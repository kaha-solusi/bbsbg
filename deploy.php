<?php
namespace Deployer;

require 'recipe/laravel.php';
require 'contrib/npm.php';
require 'contrib/rsync.php';

// Config
set('application', 'BBSBG');
set('repository', 'git@gitlab.com:kaha-solusi/bbsbg.git');
set('ssh_multiplexing', true);  // Speed up deployment
set('keep_releases', 3);

set('rsync_src', function () {
    return __DIR__; // If your project isn't in the root, you'll need to change this.
});

add('shared_files', ['.env']);
add('shared_dirs', []);
add('writable_dirs', []);

add('rsync', [
    'exclude' => [
        '.git',
        '/vendor/',
        '/node_modules/',
        '.github',
        'deploy.php',
    ],
]);

// Set up a deployer task to copy secrets to the server.
// Grabs the dotenv file from the github secret
// task('deploy:secrets', function () {
//     file_put_contents(__DIR__ . '/.env', getenv('DOT_ENV'));
//     upload('.env', get('deploy_path') . '/shared');
// });

// Hosts
host('production')
    ->setHostname('10.10.240.86')
    ->set('remote_user', 'mimin')
    ->set('branch', 'develop')
    ->set('deploy_path', '~/bbsbg');

host('staging')
    ->setHostname('104.248.158.46')
    ->set('remote_user', 'root')
    ->set('branch', 'develop')
    ->set('deploy_path', '/var/www/bbsbg');

// Hooks

after('deploy:failed', 'deploy:unlock');

desc('Start of Deploy the application');

task('deploy', [
    'deploy:prepare',
    'rsync',                // Deploy code & built assets
    // 'deploy:secrets',       // Deploy secrets
    'deploy:vendors',
    'deploy:shared',        //
    'artisan:storage:link', //
    'artisan:view:cache',   //
    'artisan:config:cache', // Laravel specific steps
    'artisan:migrate',      //
    'artisan:queue:restart',//
    'deploy:publish',       //
]);

desc('End of Deploy the application');
