@extends('layouts.adminlte')

@section('main')
    @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_FORM_CONFIRMATION_SUBMITTED)
        @include('client.permintaan-pengujian.form-confirmation.index')
    @endif

    @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_WAITING_PAYMENT)
        @include('client.permintaan-pengujian.payment-confirmation.index')
    @endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-body">
                    <form>
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label
                                for=""
                                class="text-secondary"
                            >Detail Pengujian</label>
                            <table class="table table-bordered table-detail-pesanan">
                                <tbody>
                                <tr>
                                    <td class="text-bold w-25">Nama Laboratorium</td>
                                    <td>{{ $pengujian->master_layanan_uji->pelayanan->nama }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Jenis Pengujian</td>
                                    <td>{{ $pengujian->master_layanan_uji->name }}
                                    </td>
                                </tr>
                                @if($pengujian->status === 'spk-submitted' || $pengujian->status === 'sample-submitted' || $pengujian->status === 'pengujian-submitted' || $pengujian->status === 'lhu-submitted')
                                <tr>
                                    <td class="text-bold">Tanggal Pengujian</td>
                                    <td>
                                        <div>
                                            Tanggal Mulai:
                                            {{ $pengujian->spk->tanggal_mulai }}
                                        </div>
                                        <div>
                                            Tanggal Selesai:
                                            {{ $pengujian->spk->tanggal_selesai }}
                                        </div>
                                    </td>
                                </tr>
                                @endif
                                @if($pengujian->status === 'sample-submitted' || $pengujian->status === 'pengujian-submitted' || $pengujian->status === 'lhu-submitted')
                                    <tr>
                                    <td class="text-bold">Tanggal Terima Benda Uji</td>
                                    <td>{{ $pengujian->samples->last()->updated_at }}
                                    </td>
                                </tr>
                                @endif
                                @if($pengujian->status === 'lhu-submitted')
                                    <tr>
                                    <td class="text-bold">Deskripsi Benda Uji</td>
                                    <td>
                                        {{$laporan_hasil_uji->deskripsi_benda_uji}}
                                    </td>
                                </tr>
                                @endif
                                <tr>
                                    <td class="text-bold">Produk</td>
                                    <td>{{ $pengujian->items->pluck('product_name')->unique()->join(',') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Jumlah</td>
                                    <td>{{ $pengujian->items->count() }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Dibuat untuk</td>
                                    <td>{{ $pengujian->company_name  }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Alamat</td>
                                    <td>{{ $pengujian->company_address  }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Standar Acuan/Metode Pengujian</td>
                                    <td>{{ $pengujian->items->pluck('layanan_uji_item.standard')->unique()->join(', ') }}
                                    </td>
                                </tr>
                                {{-- <tr>
                                            <td class="text-bold">Deskripsi/Kondisi Benda Uji</td>
                                            @dd($laporan_hasil_uji->permintaan_pengujian->samples->last()->items)
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->items->count() }}
                                            </td>
                                        </tr> --}}
                                </tbody>
                            </table>
                        </div>

                        {{-- <div class="form-group">
                            <label
                                for=""
                                class="text-secondary"
                            >Kondisi Benda Uji</label>
                            <table class="table table-bordered">
                                <thead class="table-custom">
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor Benda Uji</th>
                                        <th>Nama Benda Uji</th>
                                        <th>Kondisi</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($laporan_hasil_uji->permintaan_pengujian->samples->last()->items as $no => $item)
                                        <tr>
                                            <td>{{ ++$no }}</td>
                                            <td>{{ $item->pivot->kode_sampel }}</td>
                                            <td>{{ $item->product->name }}</td>
                                            <td>{{ $item->pivot->status }}</td>
                                            <td>{{ $item->pivot->keterangan }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div> --}}

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
