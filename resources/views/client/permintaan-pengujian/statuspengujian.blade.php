@extends('layouts.adminlte')
@section('main')
    {{-- @include('admin.parts.breadcrumbs', ['judul' => [['judul' => 'Permintaan Pengujian']]]) --}}
    <div class="container mt-3">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="float-left">
                                    Formulir Pengiriman Sampel
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="nama_sampel_uji">Nama Sampel Uji</label>
                                            <input
                                                type="text"
                                                class="form-control"
                                                id="nama_sampel_uji"
                                            >
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="jasa_ekspedisi">Jasa Ekspedisi</label>
                                                    <input
                                                        type="text"
                                                        class="form-control"
                                                        id="jasa_ekspedisi"
                                                    >
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="no_resi">No. Resi</label>
                                                    <input
                                                        type="text"
                                                        class="form-control"
                                                        id="no_resi"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p style="margin-top:30px;">Daftar Produk Pengiriman Sampel Uji:</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="table table-hover w-100">
                                                <thead class="bg-hijau">
                                                    <tr>
                                                        <th>Produk</th>
                                                        <th>Tipe Produk</th>
                                                        <th>Jumlah</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Tiang Pancang Kotak</td>
                                                        <td>Struktur Dasar</td>
                                                        <td>5</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tiang Pancang Bulat</td>
                                                        <td>Struktur Dasar</td>
                                                        <td>5</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tiang Jaler</td>
                                                        <td>Struktur Dasar</td>
                                                        <td>5</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Paku Baja</td>
                                                        <td>Struktur Dasar</td>
                                                        <td>5</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kepala Kambing</td>
                                                        <td>Tumbal</td>
                                                        <td>5</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <label for="keterangan_sampel">Keterangan</label>
                                            <textarea
                                                class="form-control"
                                                id="keterangan_sampel"
                                                rows="3"
                                            ></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <button
                                                type="submit"
                                                class="btn btn-primary"
                                            >Kirim Benda Uji</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
