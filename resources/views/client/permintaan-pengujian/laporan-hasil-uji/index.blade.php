@extends('layouts.adminlte')
@push('styles')
    <style>
        .table-detail-pesanan tr td:first-child {
            width: 120px;
        }

        div#print-area {
            visibility: hidden;
            position: absolute;
            left: 0;
            top: 0;
        }

        @media print {
            #non-printable {
                display: none;
            }


            body * {
                visibility: hidden;
            }

            #print-area,
            #print-area * {
                visibility: visible;
            }
        }

    </style>
@endpush

@section('main')
    <div
        id='non-printable'
        class="container"
    >
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <h3 class="card-title">Laporan Hasil Uji</h3>
                    </div>
                    <div class="card-body">
                        <form>
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label
                                    for=""
                                    class="text-secondary"
                                >Detail Pengujian</label>
                                <table class="table table-bordered table-detail-pesanan">
                                    <tbody>
                                        <tr>
                                            <td class="text-bold w-25">Nama Laboratorium</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->master_layanan_uji->pelayanan->nama }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Jenis Pengujian</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->master_layanan_uji->name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Tanggal Pengujian</td>
                                            <td>
                                                <div>
                                                    Tanggal Mulai:
                                                    {{ $laporan_hasil_uji->permintaan_pengujian->spk->tanggal_mulai }}
                                                </div>
                                                <div>
                                                    Tanggal Selesai:
                                                    {{ $laporan_hasil_uji->permintaan_pengujian->spk->tanggal_selesai }}
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Tanggal Terima Benda Uji</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->samples->last()->updated_at }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Deskripsi Benda Uji</td>
                                            <td>
                                                {{ $laporan_hasil_uji->deskripsi_benda_uji }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Produk</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->items->pluck('product_name')->unique()->join(',') }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Jumlah</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->items->count() }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Dibuat untuk</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->company_name ?? $laporan_hasil_uji->permintaan_pengujian->nama_pemohon }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Alamat</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->company_address ?? $laporan_hasil_uji->permintaan_pengujian->alamat }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Standar Acuan/Metode Pengujian</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->items->pluck('layanan_uji_item.standard')->unique()->join(', ') }}
                                            </td>
                                        </tr>
                                        {{-- <tr>
                                                    <td class="text-bold">Deskripsi/Kondisi Benda Uji</td>
                                                    @dd($laporan_hasil_uji->permintaan_pengujian->samples->last()->items)
                                                    <td>{{ $laporan_hasil_uji->permintaan_pengujian->items->count() }}
                                                    </td>
                                                </tr> --}}
                                    </tbody>
                                </table>
                            </div>

                            {{-- <div class="form-group">
                                <label
                                    for=""
                                    class="text-secondary"
                                >Kondisi Benda Uji</label>
                                <table class="table table-bordered">
                                    <thead class="table-custom">
                                        <tr>
                                            <th>No</th>
                                            <th>Nomor Benda Uji</th>
                                            <th>Nama Benda Uji</th>
                                            <th>Kondisi</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($laporan_hasil_uji->permintaan_pengujian->samples->last()->items as $no => $item)
                                            <tr>
                                                <td>{{ ++$no }}</td>
                                                <td>{{ $item->pivot->kode_sampel }}</td>
                                                <td>{{ $item->product->name }}</td>
                                                <td>{{ $item->pivot->status }}</td>
                                                <td>{{ $item->pivot->keterangan }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> --}}

                            <div class="float-right">
                                <a
                                    href="{{ route('permintaan-pengujian.download', [
                                        'path' => '/uploads/laporan_hasil_uji/' . $lhu,
                                        'model_id' => $data->id,
                                        'model' => 'PermintaanPengujian',
                                        'route' => 'permintaan-pengujian.kuisioner',
                                    ]) }}"
                                    class="btn btn-info"
                                >
                                    Download Laporan Hasil Uji
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script
        defer
        src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"
    ></script>
    <script>
        $(document).ready(function() {
            $('.btn-print').on('click', function() {
                window.print();
            });
        })
    </script>
@endpush
