@extends('layouts.adminlte')

@section('main')
    <div class="container mt-3">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="float-left">
                                    Konfirmasi Pembayaran
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    action="{{ route('client.permintaan-pengujian.payment-confirmation.post', $permintaan_pengujian->id) }}"
                                    method="POST"
                                    enctype="multipart/form-data"
                                >
                                    @csrf
                                    <div class="form-group ">
                                        <label for="how_to_pay" class="d-block">Cara Pembayaran</label>
                                        <a href="{{ $permintaan_pengujian->payments->file->getPath() }}" 
                                            class='btn btn-primary '
                                            id="how_to_pay"
                                            name="how_to_pay"
                                            target="_blank"
                                            >
                                            Unduh
                                        </a>
                                    </div>
                                    
                                    <div class="form-group ">
                                        <label for="bank_name">Nama Bank</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="bank_name"
                                            name="bank_name"
                                            required
                                        >
                                    </div>
                                    <div class="form-group ">
                                        <label for="bank_name">Nomor Rekening Bank</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="account_number_name"
                                            name="account_number_name"
                                            required
                                        >
                                    </div>
                                    <div class="form-group ">
                                        <label for="bank_name">Nama Pemegang Bank</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="account_holder_name"
                                            name="account_holder_name"
                                            required
                                        >
                                    </div>
                                    <div class="form-group ">
                                        <label for="bank_name">Nominal Pembayaran</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="payment_amount"
                                            name="payment_amount"
                                            required
                                        >
                                    </div>
                                    <div class="form-group ">
                                        <label for="payment_date">Tanggal Pembayaran</label>
                                        <input
                                            type="date"
                                            class="form-control"
                                            id="payment_date"
                                            name="payment_date"
                                            required
                                        >
                                    </div>
                                    <div class="form-group ">
                                        <label for="payment_date">Bukti Pembayaran</label>
                                        <input
                                            accept="image/*"
                                            type="file"
                                            class="form-control"
                                            id="payment_proof"
                                            name="payment_proof"
                                            required
                                        >
                                    </div>
                                    <div class="form-group ">
                                        <button
                                            type="submit"
                                            class="btn btn-primary"
                                        >Konfirmasi Pembayaran</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    {!! JsValidator::formRequest('App\Http\Requests\Client\PaymentConfirmationRequest') !!}
@endpush
