@extends('layouts.dashboard.client')

@section('main')
    @include('admin.parts.breadcrumbs', ['judul' => [['judul' => 'Permintaan
    Pengujian']]])

    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="float-left">
                            Permintaan Pengujian
                        </div>
                        <div class="float-right">
                            <a
                                href="{{ route('pengujian.registrasi') }}"
                                class="ml-1 btn btn-primary btn-sm"
                            >Daftar Pengujian</a>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush
