@extends('layouts.adminlte')
@section('main')
    {{-- @include('admin.parts.breadcrumbs', ['judul' => [['judul' => 'Permintaan Pengujian']]]) --}}
    <div class="container mt-3">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="float-left">
                                    Formulir Pengiriman Sampel
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div
                                x-data="{jenis_pengiriman: 'mandiri'}"
                                class="col-md-12"
                            >
                                <form
                                    method="POST"
                                    enctype="multipart/form-data"
                                    action="{{ route('client.permintaan-pengujian.test-sample.store', $permintaan_pengujian->id) }}"
                                >
                                    @csrf
                                    <div class="form-group">
                                        <label for="jasa_ekspedisi">Jenis Pengiriman</label>
                                        <select
                                            class="form-control"
                                            x-on:change="jenis_pengiriman = $event.target.value"
                                            name="jenis_pengiriman"
                                        >
                                            <option value="mandiri">Mandiri</option>
                                            <option value="ekspedisi">Ekspedisi</option>
                                        </select>
                                    </div>

                                    <template x-if="jenis_pengiriman == 'ekspedisi'">
                                        <div>
                                            <div class="form-group">
                                                <label for="jasa_ekspedisi">Jasa Ekspedisi</label>
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    id="jasa_ekspedisi"
                                                    name="jasa_ekspedisi"
                                                >
                                            </div>
                                            <div class="form-group">
                                                <label for="no_resi">No. Resi</label>
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    id="no_resi"
                                                    name="no_resi"
                                                >
                                            </div>
                                        </div>
                                    </template>

                                    <div class="form-group">
                                        <label for="no_resi">Bukti Pengiriman</label>
                                        <input
                                            accept="image/*"
                                            type="file"
                                            class="form-control-file"
                                            id="bukti_pengiriman"
                                            name="bukti_pengiriman"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <div class="float-right">
                                            <button
                                                type="submit"
                                                class="btn btn-primary"
                                            >Konfirmasi</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script
        defer
        src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"
    ></script>
@endpush
