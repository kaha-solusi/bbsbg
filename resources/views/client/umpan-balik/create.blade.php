@extends('layouts.dashboard.client')
@section('main')
    <div class="container">
        @if (session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session()->has('failed'))
            <div class="alert alert-danger" role="alert">
                {{ session()->get('failed') }}
            </div>
        @endif
        <div class="row h-100 mt-3">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{ route('umpan-balik.store') }}"
                                      method="POST" accept-charset="utf-8"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Nama</label>
                                                    <input type="text" name="nama"
                                                           class="form-control @error('nama') is-invalid @enderror"
                                                           value="{{ $data->name }}">
                                                    @error('nama')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Isi Umpan Balik</label>
                                                    <textarea name="keterangan"
                                                              class="form-control @error('keterangan') is-invalid @enderror"
                                                              rows="5"></textarea>
                                                    @error('keterangan')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit"
                                                    class="btn btn-success btn-sm"
                                                    style="width: 100%;">Submit</button>
                                        </div>
                                        @if (session()->has('success'))
                                            <div class="col-md-12">
                                                <p>{{ session()->get('success') }}
                                                </p>
                                            </div>
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
