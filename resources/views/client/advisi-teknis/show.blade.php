@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Akun',
    'link' => route('admin.akun.index')
    ]]
    ])
    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <h3 class="float-left">Advis Teknis</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class='col-md-12'>
                                <div class="form-group">
                                    <label for="informasiAdvisi">Informasi Advis Teknis</label>
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td width="200"><strong>Tanggal Konsultasi Awal</strong></td>
                                                <td>{{ date('d-m-Y', strtotime($data->test_date)) }}</td>
                                            </tr>
                                            @if ($data->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED && $data->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT && $data->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL)
                                                <tr>
                                                    <td width="200"><strong>Tanggal Konfirmasi</strong></td>
                                                    <td>{{ date('d-m-Y', strtotime($data->confirmation_date)) }}</td>
                                                </tr>
                                                <tr>
                                                    <td width="200"><strong>Jam Konsultasi</strong></td>
                                                    <td>{{ $data->time . ' WIB' }}</td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td><strong>Lingkup Konsultasi</strong></td>
                                                <td>{{ $data->scope }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Identitas Bangunan</strong></td>
                                                <td>{{ $data->building_identity }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Lokasi</strong></td>
                                                <td>{{ $location }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Permasalahan</strong></td>
                                                <td>{{ $data->problem }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Dokumen Pendukung</strong></td>
                                                <td>
                                                    @foreach ($data->files as $file)
                                                        <li>
                                                            <a
                                                                href="{{ route('file.download', ['path' => $file->path]) }}">
                                                                {{ $file->filename }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </td>
                                            </tr>

                                            @if ($data->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED)
                                                <tr>
                                                    <td><strong>Alasan Ditolak</strong></td>
                                                    <td>
                                                        {{ $data->approvals()->having('pivot_status', \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED)->get()->last()->pivot->reason ?? '' }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Ditolak Oleh</strong></td>
                                                    <td>
                                                        {{ $data->approvals()->having('pivot_status', \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED)->get()->last()->name ?? '' }}
                                                    </td>
                                                </tr>
                                            @endif

                                            @if ($data->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED && $data->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT && $data->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL)
                                                <tr>
                                                    <td><strong>Link Zoom</strong></td>
                                                    <td>
                                                        <a href="{{ $zoom->link_zoom }}">
                                                            {{ $zoom->link_zoom }}
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Meeting ID</strong></td>
                                                    <td>
                                                        {{ $zoom->meeting_id }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Passcode</strong></td>
                                                    <td>
                                                        {{ $zoom->passcode }}
                                                    </td>
                                                </tr>
                                            @endif

                                            @if ($data->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED || $data->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE)
                                                <tr>
                                                    <td><strong>Berita Acara</strong></td>
                                                    <td>
                                                        <a
                                                            href="{{ route('file.download', [
                                                                'path' => '/uploads/advisi-teknis/' . $data->id . '/' . $data->news_report,
                                                            ]) }}">
                                                            Unduh Berita Acara
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endif

                                            @if ($data->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED)
                                                <tr>
                                                    <td><strong>Surat Pengantar</strong></td>
                                                    <td>
                                                        <a
                                                            href="{{ route('advisi-teknis.download', [
                                                                'path' => '/uploads/advisi-teknis/' . $data->id . '/' . $data->letter_of_introduction,
                                                                'model_id' => $data->id,
                                                                'model' => 'AdvisiTeknis',
                                                                'route' => 'advisi-teknis.kuisioner',
                                                            ]) }}">
                                                            Unduh Surat Pengantar
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endif

                                            <tr>
                                                <td><strong>Status</strong></td>
                                                <td>
                                                    @if ($data->status == \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT || $data->status == \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL)
                                                        <span class="badge badge-info">Menunggu Keputusan</span>
                                                    @elseif ($data->status == \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED)
                                                        <span class="badge badge-success">Diterima</span>
                                                    @elseif ($data->status == \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED)
                                                        <span class="badge badge-danger">Ditolak</span>
                                                    @elseif ($data->status == \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE)
                                                        <span class="badge badge-warning">Menunggu Surat Pengantar</span>
                                                    @elseif ($data->status == \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED)
                                                        <span class="badge badge-success">Selesai</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
    <script>
        // Format options
        // const format = (item) => {
        //     if (!item.id) {
        //         return item.text;
        //     }

        //     var url = item.element.getAttribute('data-avatar');
        //     var img = $("<img>", {
        //         class: "rounded-circle me-2",
        //         width: 26,
        //         src: url
        //     });
        //     var span = $("<span>", {
        //         text: " " + item.text
        //     });
        //     span.prepend(img);
        //     return span;
        // }

        // function formatState(state) {
        //     if (!state.id) {
        //         return state.text;
        //     }

        //     var baseUrl = "/user/pages/images/flags";
        //     var $state = $(
        //         '<span><img class="img-flag" /> <span></span></span>'
        //     );

        //     // Use .text() instead of HTML string concatenation to avoid script injection issues
        //     $state.find("span").text(state.text);
        //     $state.find("img").attr("src", baseUrl + "/" + state.element.value.toLowerCase() + ".png");

        //     return $state;
        // };


        // // Init Select2 --- more info: https://select2.org/
        // $('.user-list').select2({
        //     templateResult: function(item) {
        //         return format(item);
        //     },
        //     templateSelection: format,
        //     theme: "bootstrap4",
        //     placeholder: 'Pilih pegawai..',
        // });

        // new ClipboardJS('.btn-clipboard');
        // $(document).ready(function() {
        //     bsCustomFileInput.init()

        //     const $rejectModal = $('#rejectModal');
        //     $rejectModal.on('show.bs.modal', function(e) {
        //         // do something...
        //     })
        // });
    </script>
@endpush
