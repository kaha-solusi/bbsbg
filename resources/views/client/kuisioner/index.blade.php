@extends('layouts.dashboard.client')

@section('main')
    @include('admin.parts.breadcrumbs', ['judul' => [['judul' => 'Kuisioner']]])

    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="float-left">
                                    Kuisioner
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form
                            action="{{ $route }}"
                            method='POST'
                        >
                            @csrf
                            @foreach ($kuisioners as $key => $kuisioner)
                                <div class="mb-3">
                                    <div class="form-group">
                                        <input
                                            type="hidden"
                                            name="data[{{ $key }}][kuisioner_id]"
                                            value="{{ $kuisioner->id }}"
                                            class="form-group"
                                        >
                                        <label for="pernyataan{{ $key }}">Pernyataan</label>
                                        <p>{{ $kuisioner->pernyataan }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="keterangan{{ $key }}">Keterangan</label>
                                        <p>{{ $kuisioner->keterangan }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label>Tingkat Kepuasan <small class="text-success"></small>
                                            @error('tingkat_kepuasan')<small
                                                class="text-danger">{{ $message }}</small>@enderror
                                        </label><br>
                                        <div class="form-row">
                                            @for ($i = 0; $i < 5; $i++)
                                                <div class="custom-control custom-radio mr-3">
                                                    <input
                                                        class="custom-control-input"
                                                        type="radio"
                                                        id="{{ $i + 1 }}star{{ $key }}"
                                                        name="data[{{ $key }}][score]"
                                                        value="{{ $i + 1 }}"
                                                    >
                                                    <label
                                                        for="{{ $i + 1 }}star{{ $key }}"
                                                        class="custom-control-label"
                                                    >{{ $i + 1 }}</label>
                                                </div>
                                            @endfor
                                        </div>
                                    </div>
                                    <hr />
                                </div>
                            @endforeach

                            <div class="form-group">
                                <div class="float-right">
                                    <button
                                        type="submit"
                                        class="btn btn-primary"
                                    >
                                        Simpan
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
