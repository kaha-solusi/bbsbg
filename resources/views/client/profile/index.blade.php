@extends('layouts.dashboard.client')
@section('main')
    {{ Breadcrumbs::render() }}
    <div class="container">
        <div class="main-body">
            <form
                action="{{ route('client.profile.update', $user->id) }}"
                method="POST"
                enctype="multipart/form-data"
            >
                @csrf
                @method('PUT')
                @include('admin.utils.alert')
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-column align-items-center text-center">
                                <img
                                    src="{{ "/uploads/foto/client/".$user->client->logo ?? 'https://bootdey.com/img/Content/avatar/avatar6.png' }}"
                                    alt="{{ $user->username }}"
                                    class="rounded-circle p-1 bg-primary"
                                    width="110"
                                >
                                <label for="form-file" style="color: dodgerblue" class="btn mt-2">Upload Foto Profil
                                    <div class="custom-tooltip">
                                        <i class="fas fa-info-circle"></i>
                                        <span
                                            class="custom-tooltiptext">
                                            Tipe file image:
                                            JPG, JPEG, PNG;
                                            Max berukuran 5mb.
                                            Klik tombol "simpan"
                                            untuk menyimpan perubahan</span>
                                    </div></label>
                                <input type="file" name="logo" id="form-file" class="hidden" hidden accept="image/*"/>
                                <div class="mt-3">
                                    <h4>{{ $user->name }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="d-flex align-items-center mb-3">Partisipasi</h5>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                    <h6 class="mb-0">Pengujian</h6>
                                    <span class="text-secondary">{{$pengujian}}</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                    <h6 class="mb-0">Bimbingan Teknis</h6>
                                    <span class="text-secondary">{{$bimtek}}</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                    <h6 class="mb-0">Advis Teknis</h6>
                                    <span class="text-secondary">{{$advis}}</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                    <h6 class="mb-0">Umpan Balik</h6>
                                    <span class="text-secondary">{{$umpanbalik}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <label
                                    for="name"
                                    class="col-sm-3 col-form-label"
                                >Nama</label>
                                <div class="col-sm-9">
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="nama"
                                        placeholder="Nama"
                                        name="nama"
                                        value="{{ old('name', $user->name) }}"
                                    >
                                </div>
                            </div>
                            <div class="form-group row">
                                    <label
                                        for="username"
                                        class="col-sm-3 col-form-label"
                                    >Username</label>
                                    <div class="col-sm-9">
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="username"
                                            placeholder="Username"
                                            name="username"
                                            value="{{ old('username', $user->username) }}"
                                        >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label
                                        for="email"
                                        class="col-sm-3 col-form-label"
                                    >Email</label>
                                    <div class="col-sm-9">
                                        <input
                                            type="email"
                                            class="form-control"
                                            id="email"
                                            placeholder="email@domain.com"
                                            name="email"
                                            value="{{ old('email', $user->email) }}"
                                        >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label
                                        for="password"
                                        class="col-sm-3 col-form-label"
                                    >Password</label>
                                    <div class="col-sm-9">
                                        <input
                                            type="password"
                                            class="form-control"
                                            id="password"
                                            placeholder="supersecretpassword"
                                            aria-describedby="passwordHelp"
                                            name="password"
                                        >
                                        <small
                                            id="passwordHelp"
                                            class="form-text text-muted"
                                        >Kosongkan jika anda tidak ingin mengubah kata sandi</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label
                                        for="phone_number"
                                        class="col-sm-3 col-form-label"
                                    >Nomor Telepon</label>
                                    <div class="col-sm-9">
                                        <input
                                            type="tel"
                                            class="form-control"
                                            id="phone_number"
                                            placeholder="628122222222"
                                            name="phone_number"
                                            value="{{ old('phone_number', $user->client->phone_number ?? '') }}"
                                        >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label
                                        for="instansi"
                                        class="col-sm-3 col-form-label"
                                    >Instansi</label>
                                    <div class="col-sm-9">
                                        <select
                                            class="form-control"
                                            id="instansi"
                                            name="instansi"
                                            disabled
                                        >
                                            <option
                                                value="1"
                                                @if (intval($user->client->group) === 1) selected @endif
                                            >Kementerian PUPR</option>
                                            <option
                                                value="2"
                                                @if (intval($user->client->group) === 2) selected @endif
                                            >Pemerintahan Lainnya</option>
                                            <option
                                                value="3"
                                                @if (intval($user->client->group) === 3) selected @endif
                                            >Non-Pemerintah</option>
                                        </select>
                                    </div>
                                </div>

                                @if (intval($user->client->group) === 1)
                                    <div x-data="profile()">
                                        @if (!is_null($user->client->instansi))
                                            <div class="form-group row">
                                                <label
                                                    for="unit_organisasi"
                                                    class="col-sm-3 col-form-label"
                                                >Unit Organisasi</label>
                                                <div class="col-sm-9">
                                                    <select
                                                        class="form-control"
                                                        id="instansi"
                                                        name="unit_organisasi"
                                                        x-on:change="setSelectedUnitOrganisasi"
                                                    >
                                                        <option value="">Silahkan pilih...</option>
                                                        <template x-for="unit in unitOrganisasi">
                                                            <option
                                                                x-text="unit.nama"
                                                                :value="unit.id"
                                                                :key="unit.id"
                                                                :selected="{{ $user->client->instansi->id }} === unit.id"
                                                            ></option>
                                                        </template>
                                                    </select>
                                                </div>
                                            </div>
                                        @endif

                                        @if (!is_null($user->client->unit_kerja))
                                            <div class="form-group row">
                                                <label
                                                    for="unit_kerja"
                                                    class="col-sm-3 col-form-label"
                                                >Unit Kerja</label>
                                                <div class="col-sm-9">
                                                    <select
                                                        class="form-control"
                                                        id="unit_kerja"
                                                        name="unit_kerja"
                                                    >
                                                        <option value="">Silahkan pilih...</option>
                                                        <template x-for="unit in unitKerja">
                                                            <option
                                                                x-text="unit.nama"
                                                                :value="unit.id"
                                                                :key="unit.id"
                                                                :selected="{{ $user->client->unit_kerja->id }} === unit.id"
                                                            ></option>
                                                        </template>

                                                    </select>

                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @else
                                    <div class="form-group row">
                                        <label
                                            for="nama_instansi"
                                            class="col-sm-3 col-form-label"
                                        >Nama Instansi</label>
                                        <div class="col-sm-9">
                                            <input
                                                type="text"
                                                class="form-control"
                                                id="nama_instansi"
                                                placeholder=""
                                                name="nama_instansi"
                                                value="{{ old('nama_instansi', $user->client->nama_perusahaan ?? '') }}"
                                            >
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group row">
                                    <label
                                        for="nip_nrp"
                                        class="col-sm-3 col-form-label"
                                    >NIP/NRP</label>
                                    <div class="col-sm-9">
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="nip_nrp"
                                            placeholder=""
                                            name="nip_nrp"
                                            value="{{ old('nip_nrp', $user->client->nomor ?? '') }}"
                                        >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label
                                        for="alamat"
                                        class="col-sm-3 col-form-label"
                                    >Alamat</label>
                                    <div class="col-sm-9">
                                        <textarea
                                            row="5"
                                            class="form-control"
                                            id="alamat"
                                            name="alamat"
                                        >{{ old('alamat', $user->client->alamat ?? '') }}</textarea>
                                    </div>
                                </div>
                                <button
                                    type="submit"
                                    class="btn btn-primary  float-right"
                                >Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script
        src="//unpkg.com/alpinejs"
        defer
    ></script>
    <script>
        function profile() {
            return {
                unitOrganisasi: {!! $unitOrganisasi !!},
                unitKerja: {!! $unitKerja !!},
                setSelectedUnitOrganisasi($event) {
                    this.getUnitKerja($event.target.value)
                },
                getUnitKerja(unitOrganisasiId) {
                    if (unitOrganisasiId) {
                        const url = '{{ route('api.instansi.get-unit-kerja', '') }}' + `/${unitOrganisasiId}`
                        fetch(url)
                            .then(res => res.json())
                            .then(data => {
                                this.unitKerja = data
                            })
                    } else {
                        this.unitKerja = []
                    }
                }
            }
        }
    </script>
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    {!! JsValidator::formRequest('App\Http\Requests\ProfileRequest') !!}
@endpush
