@php
$data = \App\Models\SettingApp::find(1);
@endphp
<!-- ======= Header ======= -->
<header
    id="header"
    class="fixed-top header bg-header text-dark "
>
    <div class="d-flex">

        <div class="logo float-left ml-3">
            {{-- <h3 class="text-light"><a href="/"><span>BBSBG</span></a></h3> --}}
            <!-- Uncomment below if you prefer to use an image logo -->
            {{-- http://ciptakarya.pu.go.id/satupintu/assets/img_app/pu-btam-white.png --}}


            {{-- <div class="ml-3 lead mb-5 d-flex text-warp align-items-center justify-items-center" style="width: 300px;"> --}}
            <a href="{{ route('home') }}">
                <div class="ml-3 d-flex justify-items-left align-item-center brand-wrapper">
                    <div class="logo-box">
                        @if (!empty($data->logo))
                            <img
                                src="{{ asset($data->logo) }}"
                                class="img-logo"
                                alt=""
                            >
                        @else
                            <img
                                src="{{ asset('/assets/default/default_logo.png') }}"
                                class="img-logo"
                                alt=""
                            >
                        @endif
                    </div>
                    <div
                        class="appname-box"
                        style="font-size: 11px !important; width: 300px !important;"
                    >
                        {{-- <a href="{{route('home')}}"><small class="text-dark"> --}}
                        {!! $data->nama !!}
                        {{-- </small></a> --}}
                    </div>
                </div>
            </a>
            {{-- </div> --}}
            </a>
        </div>

        <nav class="ml-5 nav-menu float-right d-none d-lg-block">
            <ul>
                <li><a
                        class="text-dark"
                        href="http://ciptakarya.pu.go.id/satupintu/home"
                    ><img
                            src="{{ asset('/default/img/1623306498_mini_sipandusatu.png') }}"
                            style="width: 40px;
            height: 40px;
            margin-top: -5px;"
                            alt="satupintu"
                        ></a></li>
                <li><a
                        class="font-weight-bold"
                        href="{{ route('home') }}"
                    >Beranda</a></li>
                <li class="drop-down"><a
                        href="#"
                        class="font-weight-bold"
                    >Tentang Kami</a>
                    <ul>
                        <li><a
                                class="text-dark"
                                href="{{ route('sejarah') }}"
                            >Sejarah</a></li>
                        {{-- <li><a class="text-dark" href="{{route('visimisi')}}">Visi dan Misi</a></li> --}}
                        <li><a
                                class="text-dark"
                                href="{{ route('tugasfungsi') }}"
                            >Tugas dan Fungsi</a></li>
                        <li><a
                                class="text-dark"
                                href="{{ route('struktur') }}"
                            >Struktur Organisasi</a></li>
                        <li><a
                                class="text-dark"
                                href="{{ route('pejabat') }}"
                            >Profil Pejabat dan Pegawai</a></li>
                        {{-- <li><a class="text-dark" href="{{route('maklumat')}}">Maklumat</a></li> --}}
                        <li><a
                                class="text-dark"
                                href="{{ route('maskot') }}"
                            >Maskot</a></li>
                    </ul>
                </li>

                <li class="drop-down"><a
                        href="#"
                        class="font-weight-bold"
                    >Pelayanan</a>
                    <ul>
                        <li class="drop-down"><a
                                href="#"
                                class="text-dark"
                            >Informasi</a>
                            <ul>
                                <li><a
                                        class="text-dark"
                                        href="{{ route('maklumat') }}"
                                    >Maklumat</a></li>
                                <li><a
                                        class="text-dark"
                                        href="{{ route('visimisi') }}"
                                    >Visi dan Misi</a></li>
                                <li><a
                                        class="text-dark"
                                        href="{{ route('alurpelayanan') }}"
                                    >Alur Pelayanan</a></li>
                                <li><a
                                        class="text-dark"
                                        target="__blank"
                                        href="{{ asset('/assets/file_biaya/1616315473_PP_38-2012_Tarif_PNBP_Kem_PU.pdf') }}"
                                    >{{ __('Biaya Pelayanan') }}</a></li>
                                <li><a
                                        class="text-dark"
                                        href="{{ route('kepuasan_pelanggan') }}"
                                    >Kepuasan Pelanggan</a></li>
                                <li><a
                                        class="text-dark"
                                        href="{{ route('kebijakan-pelayanan') }}"
                                    >SK Kebijakan dan Standar Pelayanan</a></li>
                            </ul>
                        </li>
                        <li class="drop-down"><a
                                href="#"
                                class="text-dark"
                            >Jenis Pelayanan</a>
                            <ul>
                                @forelse ($kategori_pelayanan as $pelayanan)
                                    @if (count($pelayanan->pelayanan) > 1)
                                        <li class="drop-down"><a
                                                href="#"
                                                class="text-dark"
                                            >{{ $pelayanan->nama }}</a>
                                            <ul>
                                                @foreach ($pelayanan->pelayanan as $item)
                                                    <li>
                                                         <a
                                                            href="{{ route('home.pelayanan.extra', $item->id) }}">{{ $item->nama }}</a>
{{--                                                        <a href="{{ route('maintenance') }}">{{ $item->nama }}</a>--}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else
                                        @foreach ($pelayanan->pelayanan as $item)
                                            <li><a
                                                    href="{{ route('home.pelayanan.extra', $item->id) }}">{{ $item->nama }}</a>
                                            </li>
                                        @endforeach
                                    @endif
                                @empty
                                    <li class="drop-down"><a
                                            href="#"
                                            class=""
                                        >Tidak ada</a>
                                @endforelse
                                <li>
                                     <a href="{{ route('home.pelayanan.advisi-teknis') }}">Advis Teknis</a>
{{--                                    <a href="{{ route('maintenance') }}">Advis Teknis</a>--}}
                                </li>
                        </li>
                    </ul>
                </li>
            </ul>
            </li>
            <li><a
                    class="font-weight-bold"
                    href="{{ route('fasilitas') }}"
                >Fasilitas Balai</a></li>
            {{-- <li class="drop-down"><a href="#" class="text-dark">Fasilitas Balai</a>
            <ul>
                <li><a class="text-dark" href="{{route('fasilitas', ['lab' => 'bahan'])}}">Lab Bahan Bangunan</a></li>
                <li><a class="text-dark" href="{{route('fasilitas', ['lab' => 'struktur'])}}">Lab Struktur Bangunan</a></li>
            </ul>
        </li> --}}
            <li class="drop-down"><a
                    href="#"
                    class="font-weight-bold"
                >Informasi Publik</a>
                <ul>
                    <li><a
                            class="text-dark"
                            href="{{ route('berita') }}"
                        >Berita</a></li>
                    <li><a
                            class="text-dark"
                            href="https://eppid.pu.go.id"
                        >e-PPID</a></li>
                    <li><a
                            class="text-dark"
                            target="__blank"
                            href="http://gol.itjen.pu.go.id/"
                        >Pelaporan Gratifikasi</a></li>
                    {{-- <li class="drop-down"><a href="#" class="text-dark">Jadwal</a>
                    <ul>
                      <li><a class="text-dark" href="{{ route('jadwal-pengujian') }}">Jadwal Pengujian</a></li>
                      <li><a class="text-dark" href="{{ route('jadwal-bimtek') }}">Jadwal Bimtek</a></li>
                    </ul>
                </li> --}}
                    {{-- <li><a class="text-dark">Kepuasan Pelanggan</a></li> --}}
                    @if (!empty($informasi))
                        @foreach ($informasi as $item)
                            <li><a
                                    class="text-dark"
                                    target="__blank"
                                    href="{{ $item->link }}"
                                >{{ $item->nama }}</a></li>
                        @endforeach
                    @endif
                    <li class="drop-down"><a
                            href="#"
                            class="text-dark"
                        >Pengaduan Masyarakat</a>
                        <ul>
                            <li><a
                                    class="text-dark"
                                    target="__blank"
                                    href="https://pengaduan.pu.go.id"
                                >Pengaduan.pu.go.id</a></li>
                            <li><a
                                    class="text-dark"
                                    target="__blank"
                                    href="https://www.lapor.go.id/"
                                >Lapor!</a></li>
                            {{-- <li><a class="text-dark" href="{{ route('pengaduan-masyarakat.create') }}">Form Pengaduan</a></li> --}}
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a
                    class="font-weight-bold"
                    href="{{ route('faq') }}"
                >FAQ</a></li>
            <li><a
                    class="font-weight-bold"
                    target="__blank"
                    href="https://i.simmer.io/@ojiardian10/virtual-tur-bbsbg-1"
                >Virtual Tour</a></li>
            {{-- <li class="drop-down"><a href="#" class="text-dark">Dokumentasi</a>
            <ul>
                <li><a class="text-dark" href="{{route('galeri', ['kategori' => 'kegiatan'])}}">Dokumentasi Kegiatan</a></li>
                <li><a class="text-dark" href="{{route('galeri', ['kategori' => 'pengujian'])}}">Dokumentasi Pengujian</a></li>
            </ul>
        </li> --}}
            @guest
                <li class="drop-down"><a
                        href="#"
                        class="font-weight-bold"
                    >Login</a>
                    <ul>
                        <li><a
                                class="text-dark"
                                href="{{ route('login') }}"
                            >Login</a></li>
                        <li><a
                                class="text-dark"
                                href="{{ route('register') }}"
                            >Register</a></li>
                    </ul>
                </li>
            @else
                <li class="drop-down"><a
                        href="#"
                        class="text-white"
                    >{{ Auth::user()->name }}</a>
                    <ul>
                        <li>
                            @role('pelanggan')
                                <a href="{{ route('client.profile.index') }}">Dashboard</a>
                            @else
                                <a href="{{ route('adminlteHome.dashboard') }}">Dashboard</a>
                            @endrole
                        </li>
                        <li>
                            <a
                                href="#"
                                onclick="event.preventDefault();
                                                                                     document.getElementById('logout-form').submit();"
                            >{{ __('Logout') }}</a>
                            <form
                                id="logout-form"
                                action="{{ route('logout') }}"
                                method="POST"
                                class="d-none"
                            >
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
            @endguest
            </ul>
        </nav><!-- .nav-menu -->

    </div>
</header><!-- End Header -->
