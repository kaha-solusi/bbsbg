@push('style')
<style>
    a.text-biru:hover{
        font-size: 110%;
        transition: all 0.1s ease-out;
        color: black;
    }
</style> 
@endpush
<!-- ======= Footer ======= -->
<footer id="footer" class="bg-dark" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">

<div class="footer-top bg-hijau text-dark" style="border: none">
    <div class="container">

        <div class="row">
            <div class="col-lg-3 col-md-6 footer-links text-dark">
                <h4 class="text-dark">Pelayanan Kami</h4>
                <ul>
                    @php
                        $i = 0;
                    @endphp
                    @if (!empty($pelayanan))
                    @foreach ($pelayanan as $item)
                        @if ($i > 4)
                            @break
                        @endif
                    {{-- <li class="text-dark"><i class="text-dark bx bx-chevron-right"></i> <a href="{{ route('home.pelayanan.extra', $item->id) }}" class="text-dark" data-toggle="modal" data-target="#{{Str::slug($item->nama)}}">{{$item->nama}}</a></li> --}}
                    <li class="text-dark"><i class="text-dark bx bx-chevron-right"></i> <a href="{{ route('home.pelayanan.extra', $item->id) }}" class="text-dark">{{$item->nama}}</a></li>
                    @php
                        $i++;
                    @endphp
                    @endforeach
                    @endif
                </ul>
            </div>

            <div class="col-lg-3 col-md-6 footer-links text-dark">
                <h4 class="text-dark">Berita terkini</h4>
                <ul>
                    @php
                        $i = 0;
                    @endphp
                    @if (!empty($berita))
                    @foreach ($berita as $item)
                        @if ($i > 4)
                            @break
                        @endif
                        <li class="text-dark"><i class="text-dark bx bx-chevron-right"></i> <a class="text-dark" href="{{route('berita.show', $item->id)}}">{{$item->judul}}</a></li>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                    @endif
                </ul>
            </div>

            <div class="col-lg-3 col-md-6 footer-links text-dark">
                <h4 class="text-dark">
                  Hubungi kami
                </h4>
                @if (!empty($berita))
                <ul>
                    <li class="text-dark"><i class="text-dark mr-2 far fa-envelope"></i>{{ $setting->email }}</li>
                    <li class="text-dark"><i class="text-dark mr-2 fas fa-phone-alt"></i>+{{ $setting->nomor_kontak }}</li>
                    <li class="text-dark"><i class="text-dark mr-2 fas fa-map-marker-alt"></i>{{ $setting->alamat }}</li>
                </ul>
                @endif
            </div>

            <div class="col-lg-3 col-md-6 footer-info">
                <h3 class="text-dark">Lokasi</h3>
                <section class="map mt-2">
                    <div class="container-fluid p-0">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4090.4613300243827!2d107.75038915764497!3d-6.950019672548883!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68c365fcada93d%3A0x56c20075ff1a165e!2sPuslitbang%20Perumahan%20dan%20Permukiman%20-%20Kementerian%20PUPR!5e1!3m2!1sen!2sid!4v1616376695692!5m2!1sen!2sid" frameborder="0" style="border:0; height:100%;" allowfullscreen=""></iframe>
                    </div>
                </section><!-- End Map Section -->
            </div>
        </div>
        <div class="col mt-5">
            <div class="d-flex d-block content-align-center justify-content-center mb-3">
                Sosial Media
            </div>
            <div class="d-flex d-block content-align-center justify-content-center">
                @if (!empty($berita))
                <div style="font-size: 2.5em">
                    <a target="__blank" href="{{ $setting->link_facebook }}" class="text-dark"><i class="fab fa-facebook mx-2"></i></a>
                    <a target="__blank" href="{{ $setting->link_ig }}" class="text-dark"><i class="fab fa-instagram mx-2"></i></a>
                    <a target="__blank" href="{{ $setting->link_yt }}" class="text-dark"><i class="fab fa-youtube mx-2"></i></a>
                    <a target="__blank" href="{{ $setting->link_twitter }}" class="text-dark"><i class="fab fa-twitter mx-2"></i></a>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="copyright">
        &copy; Copyright {{ $setting->copyright }}. All Rights Reserved
    </div>
    {{-- <div class="credits">
    <!-- All the links in the footer should remain intact. -->
    <!-- You can delete the links only if you purchased the pro version. -->
    <!-- Licensing information: https://bootstrapmade.com/license/ -->
    <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/free-bootstrap-template-corporate-moderna/ -->
    Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
    </div> --}}
</div>
</footer><!-- End Footer -->