<!-- Main Sidebar Container -->
<aside
    class="main-sidebar sidebar-dark-primary bg-dark elevation-4 sidebar-no-expand"
    style="border: none"
>
    <!-- Brand Logo -->
    <a
        href="{{ route('home') }}"
        class="brand-link"
    >
        <img
            src="{{ asset($setting->logo) }}"
            alt="AdminLTE Logo"
            class="brand-image elevation-3"
            style="opacity: .8"
        >
        <span class="brand-text font-weight-light">{!! $setting->nama !!}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul
                class="nav nav-pills nav-sidebar text-sm  flex-column"
                data-widget="treeview"
                role="menu"
                data-accordion="false"
            >
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a
                        href="{{ route('adminlteHome.dashboard') }}"
                        class="nav-link"
                    >
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a
                        href="{{ route('admin.pegawai.jadwal-kegiatan-personil.index') }}"
                        class="nav-link"
                    >
                        <i class="nav-icon fas fa-briefcase"></i>
                        <p>
                            Jadwal
                        </p>
                    </a>
                </li>

                @if (Auth::user()->hasRole(['super-admin', 'admin']))
                    <li
                        class="nav-item has-treeview
            {{ Request::is('admin/client*') ? 'menu-open' : false }}
            {{ Request::is('admin/umpan-balik*') ? 'menu-open' : false }}
            {{ Request::is('admin/kepuasan-pelanggan*') ? 'menu-open' : false }}
            {{ Request::is('admin/indeks-kepuasan*') ? 'menu-open' : false }}
            ">
                        <a
                            href="#"
                            class="nav-link"
                        >
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Manajemen Pengguna
                                <i class="nav-icon fas fa-angle-left right"></i>
                            </p>
                        </a>

                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a
                                    href="{{ route('admin.client.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Pengguna</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a
                                    href="{{ route('admin.umpan-balik') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Umpan Balik Pengguna</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a
                                    href="{{ route('kepuasan-pelanggan.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Kepuasan Pengguna</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a
                                    href="{{ route('indeks-kepuasan.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Indeks Kepuasan</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                <li
                    class="nav-item has-treeview
            {{ Request::is('admin/pelayanan*') ? 'menu-open' : false }}
            ">
                    <a
                        href="#"
                        class="nav-link"
                    >
                        <i class="nav-icon fas fa-hand-holding-heart"></i>
                        <p>
                            Pelayanan
                            <i class="nav-icon fas fa-angle-left right"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        @foreach ($kategori_pelayanan as $kategori)
                            <li class="nav-item">
                                <a
                                    href="{{ route('pelayanan.index', ['kategori' => $kategori->id]) }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{ $kategori->nama }}</p>
                                </a>
                            </li>
                        @endforeach

                        <li class="nav-item">
                            <a
                                href="{{ route('admin.advisi-teknis.index') }}"
                                class="nav-link"
                            >
                                <i class="far fa-circle nav-icon"></i>
                                <p>Advis Teknis</p>
                            </a>
                        </li>
                    </ul>
                </li>

                @if (Auth::user()->hasRole(['super-admin', 'admin']))
                    <li
                        class="nav-item has-treeview
            {{ Request::is('admin/slider*') ? 'menu-open' : false }}
            {{ Request::is('admin/videoui*') ? 'menu-open' : false }}
            {{ Request::is('admin/pelayananui*') ? 'menu-open' : false }}
            ">
                        <a
                            href="#"
                            class="nav-link"
                        >
                            <i class="nav-icon fas fa-images"></i>
                            <p>
                                Tampilan beranda depan
                            </p>
                            <i class="nav-icon fas fa-angle-left right"></i>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a
                                    href="{{ route('slider.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Banner</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a
                                    href="{{ route('videoui.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Video</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a
                                    href="{{ route('pelayananui.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Menu-menu Pelayanan</p>
                                </a>
                            </li>
                            {{-- <li class="nav-item">
                  <a href="{{route('beritaui.edit', 1)}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Berita Terkini</p>
                  </a>
                </li> --}}
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('berita.index') }}"
                            class="nav-link"
                        >
                            <i class="nav-icon fas fa-newspaper"></i>
                            <p>
                                Berita
                            </p>
                        </a>
                    </li>
                @endif
                @can('view_peralatans')
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.peralatan.index') }}"
                            class="nav-link"
                        >
                            <i class="fas fa-flask nav-icon"></i>
                            <p>Data Peralatan</p>
                        </a>
                    </li>
                @endcan
                @if (Auth::user()->hasRole(['super-admin', 'admin']))
                    <li class="nav-item">
                        <a
                            href="{{ route('galeri.index') }}"
                            class="nav-link"
                        >
                            <i class="fas fa-image nav-icon"></i>
                            <p>Dokumentasi</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('informasipublik.index') }}"
                            class="nav-link"
                        >
                            <i class="nav-icon fas fa-info-circle"></i>
                            <p>
                                Informasi Publik
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.sosial-media') }}"
                            class="nav-link"
                        >
                            <i class="nav-icon fas fa-share-alt"></i>
                            <p>
                                Data Media Sosial
                            </p>
                        </a>
                    </li>
                @endif

                <li class="nav-item has-treeview">
                    <a
                        href="#"
                        class="nav-link"
                    >
                        <i class="nav-icon fab fa-wpforms"></i>
                        <p>
                            Permintaan Pengujian
                            <i class="nav-icon fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('view_permintaan_pengujians')
                            <li class="nav-item">
                                <a
                                    href="{{ route('admin.permintaan-pengujian.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Daftar Permintaan Pengujian
                                    </p>
                                </a>
                            </li>
                        @endcan
                        @can('view_kaji_ulangs')
                            <li class="nav-item">
                                <a
                                    href="{{ route('admin.permintaan-pengujian.kaji-ulang-layanan.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Kaji Ulang Layanan
                                    </p>
                                </a>
                            </li>
                        @endcan
                        @can('view_payments')
                            <li class="nav-item">
                                <a
                                    href="{{ route('admin.permintaan-pengujian.pembayaran.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Pembayaran
                                    </p>
                                </a>
                            </li>
                        @endcan
                        @can('view_penerimaan_benda_ujis')
                            <li class="nav-item">
                                <a
                                    href="{{ route('admin.permintaan-pengujian.penerimaan-benda-uji.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Penerimaan Benda Uji
                                    </p>
                                </a>
                            </li>
                        @endcan
                        @can('view_surat_perintah_kerjas')
                            <li class="nav-item">
                                <a
                                    href="{{ route('admin.permintaan-pengujian.surat-perintah-kerja.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Surat Perintah Kerja
                                    </p>
                                </a>
                            </li>
                        @endcan
                        @can('view_proses_pengujians')
                            <li class="nav-item">
                                <a
                                    href="{{ route('admin.permintaan-pengujian.proses-pengujian.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Proses Pengujian
                                    </p>
                                </a>
                            </li>
                        @endcan
                        @can('view_laporan_hasil_ujis')
                            <li class="nav-item">
                                <a
                                    href="{{ route('admin.permintaan-pengujian.laporan-hasil-uji.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Laporan Hasil Uji
                                    </p>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
                <li class="nav-item">
                    <a
                        href="{{ route('admin.usage.create') }}"
                        class="nav-link"
                    >
                        <i class="nav-icon fas fa-tools"></i>
                        <p>
                            Kartu Alat
                        </p>
                    </a>
                </li>


                <li
                    class="nav-item has-treeview
            {{ Request::is('admin/setting-app*') ? 'menu-open' : false }}
            {{ Request::is('admin/setting-user*') ? 'menu-open' : false }}
            {{ Request::is('admin/setting-client*') ? 'menu-open' : false }}
            ">
                    <a
                        href="#"
                        class="nav-link"
                    >
                        <i class="nav-icon fas fa-cog"></i>
                        <p>
                            Pengaturan
                            <i class="nav-icon fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @if (Auth::user()->level == '1' || Auth::user()->level == '0')
                            <li class="nav-item">
                                <a
                                    href="{{ route('admin.setting-app') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pengaturan Aplikasi</p>
                                </a>
                            </li>
                        @endif
                        <li class="nav-item">
                            @if (Auth::user()->level == '2')
                                <a
                                    href="{{ route('client.setting.index') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pengaturan Client</p>
                                </a>
                            @elseif(!Auth::user()->hasRole(['super-admin', 'admin']))
                                <a
                                    href="{{ route('admin.setting.user') }}"
                                    class="nav-link"
                                >
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pengaturan User</p>
                                </a>
                            @endif
                        </li>
                    </ul>
                </li>

                @if (Auth::user()->hasRole(['super-admin', 'admin']))

                    <li class="nav-header">Data Master</li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.sejarah') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Sejarah Balai</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.visi-misi') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Visi Misi</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.alur-pelayanan') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Alur Pelayanan</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.kebijakan-pelayanan') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Kebijakan Pelayanan</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.tupoksi') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Tugas Pokok dan Fungsi</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('struktur-organisasi.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Struktur Organisasi</p>
                        </a>
                    </li>
                    {{-- <!--<li class="nav-item">-->
          <!--  <a href="{{route('pejabat.index')}}" class="nav-link">-->
          <!--    <i class="far fa-circle nav-icon"></i>-->
          <!--    <p>Data Pejabat</p>-->
          <!--  </a>-->
          <!--</li>--> --}}
                    <li class="nav-item">
                        <a
                            href="{{ route('maklumat.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Maklumat</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('maskot.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Maskot</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a
                            href="{{ route('fasilitas.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Fasilitas Balai</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a
                            href="{{ route('admin.pegawai.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Pegawai</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a
                            href="{{ route('admin.pegawai.jabatan.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Jabatan Pegawai</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.pegawai.jabatan-laboratorium.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Jabatan Laboratorim Pegawai</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a
                            href="{{ route('admin.pegawai.golongan.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Golongan Pegawai</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.akun.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Akun</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.instansi.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Instansi</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.wa') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data WhatsApp Admin</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('lab.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Lab</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('master-pelayanan-uji.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Layanan Uji</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.products.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Produk</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.kuisioners.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Kuisioner</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.faq.index') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data FAQ</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            href="{{ route('admin.front-kepuasan-pelanggan') }}"
                            class="nav-link"
                        >
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Kepuasan Pelanggan</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a
                            href="#"
                            class="nav-link"
                        >
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                Petunjuk Penggunaan
                            </p>
                        </a>
                    </li>
                @endif

                <!-- <li class="nav-item has-treeview
            {{ Request::is('admin/sejarah*') ? 'menu-open' : false }}
            {{ Request::is('admin/kebijakan-pelayanan*') ? 'menu-open' : false }}
            {{ Request::is('admin/alur-pelayanan*') ? 'menu-open' : false }}
            {{ Request::is('admin/visi-misi*') ? 'menu-open' : false }}
            {{ Request::is('admin/tupoksi*') ? 'menu-open' : false }}
            {{ Request::is('admin/struktur-organisasi*') ? 'menu-open' : false }}
            {{ Request::is('admin/pejabat*') ? 'menu-open' : false }}
            {{ Request::is('admin/maskot*') ? 'menu-open' : false }}
            {{ Request::is('admin/fasilitas*') ? 'menu-open' : false }}
            {{ Request::is('admin/pegawai*') ? 'menu-open' : false }}
            {{ Request::is('admin/akun*') ? 'menu-open' : false }}
            {{ Request::is('admin/no-whatsapp*') ? 'menu-open' : false }}
            {{ Request::is('admin/lab*') ? 'menu-open' : false }}
            {{ Request::is('admin/faq*') ? 'menu-open' : false }}
            ">
              <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-database"></i>
                <p>
                  Data Master
                  <i class="nav-icon fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">

              </ul>
            </li>
        </ul> -->
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
