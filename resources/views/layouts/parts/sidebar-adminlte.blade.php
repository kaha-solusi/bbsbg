<!-- Main Sidebar Container -->
@if (auth()->user()->hasRole('pelanggan'))
    @include('layouts.parts.client.sidebar')
@else
    @include('layouts.parts.admin.sidebar')
@endif
