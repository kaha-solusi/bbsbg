<!-- Main Sidebar Container -->
<aside
    class="main-sidebar sidebar-dark-primary bg-dark elevation-4 sidebar-no-expand"
    style="border: none"
>
    <!-- Brand Logo -->
    <a
        href="{{ route('home') }}"
        class="brand-link"
    >
        <img
            src="{{ asset($setting->logo) }}"
            alt="AdminLTE Logo"
            class="brand-image elevation-3"
            style="opacity: .8"
        >
        <span class="brand-text font-weight-light">{!! $setting->nama !!}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul
                class="nav nav-pills nav-sidebar text-sm  flex-column"
                data-widget="treeview"
                role="menu"
                data-accordion="false"
            >
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

                <li class="nav-item">
                    <a
                        href="{{ route('client.profile.index') }}"
                        class="nav-link"
                    >
                        <i class="nav-icon fab fa-wpforms"></i>
                        <p>
                            Profil
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a
                        href="{{ route('client.permintaan-pengujian.index') }}"
                        class="nav-link"
                    >
                        <i class="nav-icon fab fa-wpforms"></i>
                        <p>
                            Pendaftaran Pengujian
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a
                        href="{{ route('client.bimtek.list') }}"
                        class="nav-link"
                    >
                        <i class="nav-icon fab fa-wpforms"></i>
                        <p>
                            Bimbingan Teknis
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a
                        href="{{ route('client.advisiteknis.index') }}"
                        class="nav-link"
                    >
                        <i class="nav-icon fab fa-wpforms"></i>
                        <p>
                            Advis Teknis
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a
                        href="{{ route('umpan-balik.index') }}"
                        class="nav-link"
                    >
                        <i class="nav-icon fab fa-wpforms"></i>
                        <p>
                            Umpan Balik
                        </p>
                    </a>
                </li>
                {{-- <li class="nav-item">
                    <a
                        href="{{ route('kepuasan-pelanggan.create') }}"
                        class="nav-link"
                    >
                        <i class="nav-icon fab fa-wpforms"></i>
                        <p>
                            Kepuasan Pelanggan
                        </p>
                    </a>
                </li> --}}
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
