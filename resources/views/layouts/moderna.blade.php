@php
$data = \App\Models\SettingApp::find(1);
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta
        content="width=device-width, initial-scale=1.0"
        name="viewport"
    >

    <title>{{ $setting->nama }}</title>
    <link
        rel="shortcut icon"
        type="image/jpg"
        href="{{ asset($data->logo) }}"
    />
    <meta
        content=""
        name="description"
    >
    <meta
        content=""
        name="keywords"
    >

    {{-- My Styles --}}
    <link
        rel="stylesheet"
        href="{{ asset('/css/styles.css') }}"
    >
    <!-- Favicons -->
    <!-- Font Awesome -->
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/fontawesome-free/css/all.min.css') }}"
    >
    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,700,700i&display=swap"
        rel="stylesheet"
    >

    <!-- Vendor CSS Files -->
    <link
        href="{{ asset('/Moderna/assets/vendor/bootstrap/css/bootstrap.min.css') }}"
        rel="stylesheet"
    >
    <link
        href="{{ asset('/Moderna/assets/vendor/animate.css/animate.min.css') }}"
        rel="stylesheet"
    >
    <link
        href="{{ asset('/Moderna/assets/vendor/icofont/icofont.min.css') }}"
        rel="stylesheet"
    >
    <link
        href="{{ asset('/Moderna/assets/vendor/boxicons/css/boxicons.min.css') }}"
        rel="stylesheet"
    >
    <link
        href="{{ asset('/Moderna/assets/vendor/venobox/venobox.css') }}"
        rel="stylesheet"
    >
    <link
        href="{{ asset('/Moderna/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}"
        rel="stylesheet"
    >
    <link
        href="{{ asset('/Moderna/assets/vendor/aos/aos.css') }}"
        rel="stylesheet"
    >

    <!-- Data Table -->
    <link
        href="//cdn.datatables.net/1.11.0/css/jquery.dataTables.min.css"
        rel="stylesheet"
    >

    <!-- Template Main CSS File -->
    <link
        href="{{ asset('/Moderna/assets/css/style.css') }}"
        rel="stylesheet"
    >
    <script src="//unpkg.com/alpinejs"></script>

    <!-- page script -->
    <!-- =======================================================
  * Template Name: Moderna - v2.2.1
  * Template URL: https://bootstrapmade.com/free-bootstrap-template-corporate-moderna/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->

    {{-- Style --}}
    <style>
        #header {
            height: 90px !important;
            transition: all 0.5s;
            z-index: 997;
            transition: all 0.5s;
            padding: 20px 0;
            background: #1e4356;
        }

        #header.header-scrolled {
            background: rgba(30, 67, 86, 0.8);
            height: 70px !important;
            padding: 10px 0;
        }

        #header .logo img {
            padding: 0;
            margin: 0;
            max-height: 50px;
        }
    </style>
    @stack('style')
</head>

<body>
    @include('layouts.parts.navigation-moderna', [
        'kategori_pelayanan' => $kategori_pelayanan,
        'informasi' => $informasi,
    ])

    <!-- ======= Hero Section ======= -->
    <main id="main">
        @yield('hero')
        @yield('main')
    </main>

    @include('layouts.parts.footer-moderna', [
        'setting' => $setting,
        'berita' => $berita,
    ])

    <a
        target="__blank"
        href="https://api.whatsapp.com/send?phone={{ !empty($setting) ? $setting->nomor_wa : '' }}"
        class="float d-flex align-items-center justify-content-center"
        style="font-size: 2.5rem"
    >
        <div class="info">
            Layanan Chat
        </div>
        <img
            src="{{ !empty($maskot->foto) ? asset($maskot->foto) : asset('assets/default/default_maskot.png') }}"
            class="img-maskot"
            alt=""
        >
    </a>
    <a
        href="#"
        class="back-to-top mt-5"
    ><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{ asset('/Moderna/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/Moderna/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/Moderna/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/Moderna/assets/vendor/php-email-form/validate.js') }}"></script>
    <script src="{{ asset('/Moderna/assets/vendor/venobox/venobox.min.js') }}"></script>
    <script src="{{ asset('/Moderna/assets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('/Moderna/assets/vendor/counterup/counterup.min.js') }}"></script>
    <script src="{{ asset('/Moderna/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('/Moderna/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('/Moderna/assets/vendor/aos/aos.js') }}"></script>

    <!-- Datatable JS -->
    <script src="//cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('/Moderna/assets/js/main.js') }}"></script>

    <script>
        function modalShow(file) {
            console.log(file);
            if (file == '') {
                document.getElementById("file_pdf").removeAttribute("src");
            } else {
                document.getElementById("file_pdf").setAttribute("src", file);
            }
            console.log(document.getElementById("file_pdf"));
        }

        $(document).ready(function() {
            if ($('#advisi_teknis').length) {
                var groupColumn = 1;
                $('#advisi_teknis').dataTable({
                    "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false,
                        },
                        {
                            "targets": [groupColumn],
                            "visible": false
                        },
                        {
                            "targets": [15],
                            "visible": false
                        },
                        {
                            "targets": [17],
                            "visible": false
                        }
                    ],
                    "order": [
                        [groupColumn, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;

                        api.column(groupColumn, {
                            page: 'current'
                        }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before(
                                    '<tr class="group"><td colspan="21" style="text-transform:capitalize;background-color:#4c4c4c;font-weight:bold;color:white;">' +
                                    group +
                                    '</td></tr>'
                                );

                                last = group;
                            }
                        });
                    }
                });

                var table = $('#advisi_teknis').DataTable();
                $("#advisi_teknis_filter.dataTables_filter").append($(
                    "#yearFilter"));

                var yearIndex = 0;
                $("#advisi_teknis th").each(function(i) {
                    if ("Tahun" === $(this).html()) {
                        yearIndex = i;

                        return false;
                    }
                });

                $('#advisi_teknis tbody').on('click', 'td.nama-bimtek',
                    function() {
                        var data = table.row(this).data();

                        $('#bimtekModalLabel').html(data[0]);
                        $('#bimtekModalBody').html('<p>' + data[17] +
                            '</p>');

                        $('#bimtekModal').modal('show');
                    });

                $.fn.dataTable.ext.search.push(
                    function(settings, data, dataIndex) {
                        var selectedItem = $('#yearFilter').val();

                        var year = data[15];

                        if (selectedItem === "" || year.includes(
                                selectedItem)) {
                            return true;
                        }
                        return false;

                        console.log(year);
                    }
                );

                $("#yearFilter").change(function(e) {
                    table.draw();
                });

                table.draw();

                $('.bimtek_daftar_modal').on('click', function() {
                    $('#nama_bimtek').html($(this).data("bimtek"));
                    $('#deskripsi_bimtek').html($(this).data("deskripsi"));
                    $('#waktu_bimtek').html("Dilaksanakan pada: " + $(this).data("tanggal"));
                    $('#bimtek_id').val($(this).data("bimtekid"));

                    $('#bimtekDaftarModal').modal('show');
                });

            }
        });
    </script>
    @stack('script')
    @include('homepage.parts.modals', [
        'pelayanan' => $pelayanan,
    ])
    <script>
        $('body').on('xhr.dt', function(e, settings, data, xhr) {
            if (typeof phpdebugbar != "undefined") {
                if (xhr.getAllResponseHeaders()) {
                    phpdebugbar.ajaxHandler.handle(xhr);
                }
            }
        });
    </script>
</body>

</html>
