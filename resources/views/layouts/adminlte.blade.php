<!DOCTYPE html>
<html>

<head>
    @routes
    @if (Session::has('download.file'))
        <meta
            http-equiv="refresh"
            content="5;url={{ Session::get('download.file') }}"
        >
    @endif
    <meta charset="utf-8">
    <meta
        http-equiv="X-UA-Compatible"
        content="IE=edge"
    >
    <title>{{ $setting->nama ?? 'AdminLTE' }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
    >
    <meta
        name="csrf-token"
        content="{{ csrf_token() }}"
    >
    <!-- Font Awesome -->
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/fontawesome-free/css/all.min.css') }}"
    >
    <!-- Ionicons -->
    <link
        rel="stylesheet"
        href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
    >
    <!-- Theme style -->
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/dist/css/adminlte.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/chart.js/Chart.min.css') }}"
    >
    <!-- Google Font: Source Sans Pro -->
    <link
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700"
        rel="stylesheet"
    >
    {{-- Bootstrap --}}
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"> --}}
    <!-- DataTables -->
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/jquery-datatables-checkboxes/css/dataTables.checkboxes.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}"
    >

    <link
        href="https://unpkg.com/jquery-resizable-columns@0.2.3/dist/jquery.resizableColumns.css"
        rel="stylesheet"
    >
    <link
        rel="stylesheet"
        href="https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.css"
    >
    <link
        href="https://unpkg.com/bootstrap-table@1.18.3/dist/extensions/sticky-header/bootstrap-table-sticky-header.css"
        rel="stylesheet"
    >

    <link
        rel="stylesheet"
        href="{{ asset('/css/styles.css') }}"
    >
    <link
        rel="stylesheet"
        href="https://unpkg.com/jquery-resizable-columns@0.2.3/dist/jquery.resizableColumns.css"
    >

    <link
        rel="stylesheet"
        href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}"
    >
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger mr-2',
            },
            buttonsStyling: false,
            reverseButtons: true,
            backdrop: true,
            buttonsStyling: false
        })
    </script>
    <style type="text/css">
        .table-custom th {
            color: #101010;
            background-color: #98DCAF;
            border-color: whitesmoke;
        }

        .table-custom td {
            color: #101010;
            background-color: whitesmoke;
            border-color: whitesmoke;
        }

        .table-custom .danger td {
            background-color: rgba(236, 120, 120, 0.54);
        }

        .table .thead-custom th {
            color: #101010;
            background-color: #98DCAF;
            border-color: whitesmoke;
        }

        .table .tbody-custom td {
            color: #101010;
            background-color: whitesmoke;
            border-color: whitesmoke;
        }

        .custom-tooltip {
            position: relative;
            display: inline-block;
            cursor: pointer;
        }

        .custom-tooltip .custom-tooltiptext {
            visibility: hidden;
            width: 300px;
            background-color: black;
            color: #fff;
            font-size: 12px;
            text-align: justify;
            border-radius: 6px;
            padding: 5px;
            position: absolute;
            z-index: 1;
            bottom: 150%;
            left: 50%;
            margin-left: -60px;
        }

        .custom-tooltip .custom-tooltiptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: black transparent transparent transparent;
        }

        .custom-tooltip:hover .custom-tooltiptext {
            visibility: visible;
        }

    </style>

    <style>
        .datepicker td,
        .datepicker th {
            width: 2.5rem !important;
            height: 2.5rem !important;
            font-size: 0.85rem !important;
        }

        .datepicker-dropdown {
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1) !important;
        }

        .datepicker table tr td.active,
        .datepicker table tr td.active:hover,
        .datepicker table tr td.active.disabled,
        .datepicker table tr td.active.disabled:hover {
            background-color: var(--hijau) !important;
            border-color: var(--hijau) !important;
            background-image: var(--hijau) !important;
        }

    </style>

    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css"
    >
    @stack('styles')
</head>

<body class="hold-transition sidebar-mini bg-light layout-fixed">
    <div class="wrapper">
        {{-- Navbar --}}
        @include('layouts.parts.navigation-adminlte')
        {{-- Sidebar --}}
        @include('layouts.parts.sidebar-adminlte', [
        'setting' => $setting, // dari app service provider
        'kategori_pelayanan' => $kategori_pelayanan,
        ])
        {{-- Main --}}
        <div class="content-wrapper">
            {{-- {{ Breadcrumbs::render() }} --}}
            @yield('main')
        </div>
        <footer class="main-footer bg-hijau text-dark">
            <div class="float-right d-none d-sm-block">
                {{-- <b>Version</b> 3.0.5 --}}
            </div>
            <strong>Copyright</strong> &copy; {!! date('Y') . ' ' . str_replace('<br>', ' ', $setting->nama) !!}.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    {{-- Fontawesome --}}
    <script
        src="https://kit.fontawesome.com/6fcd9e4180.js"
        crossorigin="anonymous"
    >
    </script>
    <!-- jQuery -->
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}">
    </script>
    <!-- bs-custom-file-input -->
    <script src="{{ asset('/adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}">
    </script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/adminlte/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('/adminlte/dist/js/demo.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}">
    </script>
    <script
        src="https://cdn.jsdelivr.net/gh/jeffreydwalter/ColReorderWithResize@9ce30c640e394282c9e0df5787d54e5887bc8ecc/ColReorderWithResize.js"
    >
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/jquery-datatables-checkboxes/js/dataTables.checkboxes.min.js') }}">
    </script>

    <script src="{{ asset('/adminlte/plugins/jquery-validation/jquery.validate.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/jquery-validation/additional-methods.min.js') }}">
    </script>
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}">
    </script>

    <!-- ChartJS -->
    <script src="{{ asset('/adminlte/plugins/chart.js/Chart.min.js') }}">
    </script>

    <script
        src="//unpkg.com/alpinejs"
        defer
    ></script>
    <script type="text/javascript">
        $(document).ready(function() {
            bsCustomFileInput.init();
        });
    </script>
    {{-- <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script> --}}
    <!-- page script -->
    <script>
        $(function() {
            //--------------
            //- AREA CHART -
            //--------------

            // Get context with jQuery - using jQuery's .get() method.

            var areaChartCanvasElement = document.getElementById(
                'areaChart')
            if (areaChartCanvasElement) {
                var areaChartCanvas = areaChartCanvasElement.get(0)
                    .getContext('2d');

                var areaChartData = {
                    labels: ['January', 'February', 'March', 'April',
                        'May', 'June', 'July'
                    ],
                    datasets: [{
                            label: 'Digital Goods',
                            backgroundColor: 'rgba(60,141,188,0.9)',
                            borderColor: 'rgba(60,141,188,0.8)',
                            pointRadius: false,
                            pointColor: '#3b8bba',
                            pointStrokeColor: 'rgba(60,141,188,1)',
                            pointHighlightFill: '#fff',
                            pointHighlightStroke: 'rgba(60,141,188,1)',
                            data: [28, 48, 40, 19, 86, 27, 90]
                        },
                        {
                            label: 'Electronics',
                            backgroundColor: 'rgba(210, 214, 222, 1)',
                            borderColor: 'rgba(210, 214, 222, 1)',
                            pointRadius: false,
                            pointColor: 'rgba(210, 214, 222, 1)',
                            pointStrokeColor: '#c1c7d1',
                            pointHighlightFill: '#fff',
                            pointHighlightStroke: 'rgba(220,220,220,1)',
                            data: [65, 59, 80, 81, 56, 55, 40]
                        },
                    ]
                }

                var areaChartOptions = {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                        display: false
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false,
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                display: false,
                            }
                        }]
                    }
                }

                // This will get the first returned node in the jQuery collection.
                var areaChart = new Chart(areaChartCanvas, {
                    type: 'line',
                    data: areaChartData,
                    options: areaChartOptions
                })
            }

            //-------------
            //- LINE CHART -
            //--------------
            var lineChartCanvasElement = $('#lineChart').get(0)
            if (lineChartCanvasElement) {
                var lineChartCanvas = lineChartCanvasElement.getContext(
                    '2d')
                var lineChartOptions = jQuery.extend(true, {},
                    areaChartOptions)
                var lineChartData = jQuery.extend(true, {}, areaChartData)
                lineChartData.datasets[0].fill = false;
                lineChartData.datasets[1].fill = false;
                lineChartOptions.datasetFill = false

                var lineChart = new Chart(lineChartCanvas, {
                    type: 'line',
                    data: lineChartData,
                    options: lineChartOptions
                })
                ///-------------
                //- BAR CHART -
                //-------------
                var barChartCanvas = $('#barChart').get(0).getContext('2d')
                var barChartData = jQuery.extend(true, {}, areaChartData)
                var temp0 = areaChartData.datasets[0]
                var temp1 = areaChartData.datasets[1]
                barChartData.datasets[0] = temp1
                barChartData.datasets[1] = temp0

                var barChartOptions = {
                    responsive: true,
                    maintainAspectRatio: false,
                    datasetFill: false
                }

                var barChart = new Chart(barChartCanvas, {
                    type: 'bar',
                    data: barChartData,
                    options: barChartOptions
                })
            }
        })
    </script>
    <script src="https://unpkg.com/jquery-resizable-columns@0.2.3/dist/jquery.resizableColumns.min.js">
    </script>
    <script src="https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.js">
    </script>
    <script src="https://unpkg.com/bootstrap-table@1.18.3/dist/extensions/mobile/bootstrap-table-mobile.min.js">
    </script>
    <script src="https://unpkg.com/bootstrap-table@1.18.3/dist/extensions/resizable/bootstrap-table-resizable.min.js">
    </script>
    <script
        src="https://unpkg.com/bootstrap-table@1.18.3/dist/extensions/sticky-header/bootstrap-table-sticky-header.min.js"
    >
    </script>
    <script src="https://unpkg.com/bootstrap-table@1.18.3/dist/extensions/addrbar/bootstrap-table-addrbar.min.js">
    </script>

    <script src="{{ asset('adminlte/plugins/select2/js/select2.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(function() {
            $.fn.datepicker.dates['en'] = {
                days: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
                daysShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
                daysMin: ["Mi", "Sn", "Sl", "Ra", "Ka", "Ju", "Sa"],
                months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "July", "Agustus", "September",
                    "Oktober", "November", "Desember"
                ],
                monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov",
                    "Des"
                ],
                today: "Hari Ini",
                clear: "Hapus",
                format: "mm/dd/yyyy",
                titleFormat: "MM yyyy",
                /* Leverages same syntax as 'format' */
                weekStart: 0,
            };
            $('.datepicker').datepicker({
                // clearBtn: true,
                format: "dd/mm/yyyy",
                datesDisabled: '{{ $setting->hari_libur }}',
                daysOfWeekDisabled: [0, 6],
                autoclose: true
            });

        });
    </script>
    @stack('script')

    <script>
        $('body').on('xhr.dt', function(e, settings, data, xhr) {
            if (typeof phpdebugbar != "undefined") {
                if (xhr.getAllResponseHeaders()) {
                    phpdebugbar.ajaxHandler.handle(xhr);
                }
            }
        });
    </script>
</body>

</html>
