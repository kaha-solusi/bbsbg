<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0"
    >
    <meta
        http-equiv="X-UA-Compatible"
        content="ie=edge"
    >
    <title>{{ $setting->nama }}</title>
    <!-- Theme style -->
    <link
        href="{{ asset('/Moderna/assets/vendor/bootstrap/css/bootstrap.min.css') }}"
        rel="stylesheet"
    >
    <link
        rel="stylesheet"
        href="{{ asset('/css/styles.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/fontawesome-free/css/all.min.css') }}"
    >

    <link
        rel="stylesheet"
        href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}"
    >
</head>

<body>
    @yield('main')
    <footer
        class="justify-content-center align-items-center text-center bg-hijau text-dark"
        style="height: 50px;"
    >
        {{-- <div class="float-right d-none d-sm-block"> --}}
        {{-- <b>Version</b> 3.0.5 --}}
        {{-- </div> --}}
        <div class="d-block mt-3">
            <strong>Copyright</strong> &copy; {!! date('Y') . ' ' . str_replace('<br>', ' ', $setting->nama) !!}.
        </div>
    </footer>

    <!-- jQuery -->
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- bs-custom-file-input -->
    <script src="{{ asset('/adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/adminlte/dist/js/adminlte.min.js') }}"></script>

    <script src="{{ asset('adminlte/plugins/select2/js/select2.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('input[type=radio][name=tipe_instansi]').change(function() {
                if (this.value == 'eksternal') {
                    $('.instansi-eksternal').show();
                    $('.instansi-internal').hide();
                } else if (this.value == 'internal') {
                    $('.instansi-internal').show();
                    $('.instansi-eksternal').hide();
                }
            });
        });
    </script>

    @yield('script')
</body>

</html>
