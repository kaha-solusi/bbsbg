<section class="breadcrumbs p-4">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <h2>{{ $pageTitle ?? '' }}</h2>
            {{ Breadcrumbs::render() }}
        </div>
    </div>
</section>
