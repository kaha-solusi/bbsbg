<!-- ======= Footer ======= -->
<footer id="footer" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">

<div class="footer-top">
    <div class="container">
        {{-- <div class="row">

            <div class="col-lg-3 col-md-6 footer-links">
              <h4>Useful Links</h4>
              <ul>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
              </ul>
            </div>
  
            <div class="col-lg-3 col-md-6 footer-links">
              <h4>Our Services</h4>
              <ul>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
              </ul>
            </div>
  
            <div class="col-lg-3 col-md-6 footer-contact">
              <h4>Contact Us</h4>
              <p>
                A108 Adam Street <br>
                New York, NY 535022<br>
                United States <br><br>
                <strong>Phone:</strong> +1 5589 55488 55<br>
                <strong>Email:</strong> info@example.com<br>
              </p>
  
            </div>
  
            <div class="col-lg-3 col-md-6 footer-info">
              <h3>About Moderna</h3>
              <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus.</p>
              <div class="social-links mt-3">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
              </div>
            </div>
  
          </div> --}}

        <div class="row">
            <div class="col-lg-3 col-md-6 footer-links">
            <h4>Berita terkini</h4>
            <ul>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Incididunt incididunt non.</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Fugiat veniam.</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Eiusmod voluptate do do ullamco qui.</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Adipisicing quis laborum.</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Esse occaecat sint aute fugiat.</a></li>
            </ul>
            </div>

            <div class="col-lg-3 col-md-6 footer-links">
                <h4>Hubungi kami</h4>
                <ul>
                    <li class="hover-trigger">
                        <i class="bx bx-chevron-right"></i> 
                        <a href="#"><i class="ml-2 far fa-envelope"></i>Email</a>
                        <div class="show-when-hover">
                            <button type="button" class="btn btn-success mx-1" data-toggle="modal" data-target="#exampleModal">
                              <i class="ml-2 far fa-edit"></i>
                            </button>
                        </div>
                    </li>
                    <li><i class="far fa-phone"></i> <a href="#">08298966712</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="#">Struktur</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="#">Pejabat</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="#">Pengajar</a></li>
                </ul>
            </div>

            <div class="col-lg-6 col-md-12 footer-info">
            <h3>Lokasi</h3>
            <section class="map mt-2">
                <div class="container-fluid p-0">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3024.2219901290355!2d-74.00369368400567!3d40.71312937933185!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a23e28c1191%3A0x49f75d3281df052a!2s150%20Park%20Row%2C%20New%20York%2C%20NY%2010007%2C%20USA!5e0!3m2!1sen!2sbg!4v1579767901424!5m2!1sen!2sbg" frameborder="0" style="border:0; height:90%;" allowfullscreen=""></iframe>
                </div>
            </section><!-- End Map Section -->
            </div>
        </div>
        <div class="row">
          <div class="d-flex content-align-center justify-content-center mb-3">
            Sosial Media
          </div>
            <div class="d-flex content-align-center justify-content-center">
              
              <div style="font-size: 2.5em">
                  <i class="fab fa-facebook mx-2"></i><a href="#" class="text-white"></a>
                  <i class="fab fa-instagram mx-2"></i><a href="#" class="text-white"></a>
                  <i class="fab fa-youtube mx-2"></i><a href="#" class="text-white"></a>
                  <i class="fab fa-twitter mx-2"></i><a href="#" class="text-white"></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="copyright">
        &copy; Copyright Moderna. All Rights Reserved
    </div>
    {{-- <div class="credits">
    <!-- All the links in the footer should remain intact. -->
    <!-- You can delete the links only if you purchased the pro version. -->
    <!-- Licensing information: https://bootstrapmade.com/license/ -->
    <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/free-bootstrap-template-corporate-moderna/ -->
    Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
    </div> --}}
</div>
</footer><!-- End Footer -->