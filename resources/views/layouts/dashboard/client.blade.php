<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta
        http-equiv="X-UA-Compatible"
        content="IE=edge"
    >
    <title>{{ $setting->nama ?? 'AdminLTE' }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
    >
    <meta
        name="csrf-token"
        content="{{ csrf_token() }}"
    >
    <!-- Font Awesome -->
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/fontawesome-free/css/all.min.css') }}"
    >
    <!-- Ionicons -->
    <link
        rel="stylesheet"
        href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
    >
    <!-- Theme style -->
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/dist/css/adminlte.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/chart.js/Chart.min.css') }}"
    >
    <!-- Google Font: Source Sans Pro -->
    <link
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700"
        rel="stylesheet"
    >
    {{-- Bootstrap --}}
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"> --}}
    <!-- DataTables -->
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/jquery-datatables-checkboxes/css/dataTables.checkboxes.css') }}"
    >
    <link
        rel="stylesheet"
        href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css"
    >

    <link
        href="https://unpkg.com/jquery-resizable-columns@0.2.3/dist/jquery.resizableColumns.css"
        rel="stylesheet"
    >
    <link
        rel="stylesheet"
        href="https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.css"
    >
    <link
        href="https://unpkg.com/bootstrap-table@1.18.3/dist/extensions/sticky-header/bootstrap-table-sticky-header.css"
        rel="stylesheet"
    >

    <link
        rel="stylesheet"
        href="{{ asset('/css/styles.css') }}"
    >
    <link
        rel="stylesheet"
        href="https://unpkg.com/jquery-resizable-columns@0.2.3/dist/jquery.resizableColumns.css"
    >

    <link
        rel="stylesheet"
        href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}"
    >
    <style type="text/css">
        .table-custom th {
            color: #101010;
            background-color: #98DCAF;
            border-color: #98DCAF;
        }

        .table-custom td {
            color: #101010;
            background-color: whitesmoke;
            border-color: whitesmoke;
        }

        .table .thead-custom th {
            color: #101010;
            background-color: #98DCAF;
            border-color: #98DCAF;
        }

        .table .tbody-custom td {
            color: #101010;
            background-color: whitesmoke;
            border-color: whitesmoke;
        }

        .custom-tooltip {
            position: relative;
            display: inline-block;
            cursor: pointer;
        }

        .custom-tooltip .custom-tooltiptext {
            visibility: hidden;
            width: 300px;
            background-color: black;
            color: #fff;
            font-size: 12px;
            text-align: justify;
            border-radius: 6px;
            padding: 5px;
            position: absolute;
            z-index: 1;
            bottom: 150%;
            left: 50%;
            margin-left: -60px;
        }

        .custom-tooltip .custom-tooltiptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: black transparent transparent transparent;
        }

        .custom-tooltip:hover .custom-tooltiptext {
            visibility: visible;
        }

    </style>
    @stack('styles')
</head>

<body class="hold-transition sidebar-mini bg-light layout-fixed">
    <div class="wrapper">
        {{-- Navbar --}}
        @include('layouts.parts.navigation-adminlte')
        {{-- Sidebar --}}
        @include('layouts.parts.client.sidebar', [
        'setting' => $setting, // dari app service provider
        'kategori_pelayanan' => $kategori_pelayanan,
        ])
        {{-- Main --}}
        <div class="content-wrapper">
            @yield('main')
        </div>
        <footer class="main-footer bg-hijau text-dark">
            <div class="float-right d-none d-sm-block">
                {{-- <b>Version</b> 3.0.5 --}}
            </div>
            <strong>Copyright</strong> &copy; {!! date('Y') . ' ' . str_replace('<br>', ' ', $setting->nama) !!}.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    {{-- Fontawesome --}}
    <script
        src="https://kit.fontawesome.com/6fcd9e4180.js"
        crossorigin="anonymous"
    >
    </script>
    <!-- jQuery -->
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}">
    </script>
    <!-- bs-custom-file-input -->
    <script src="{{ asset('/adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}">
    </script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/adminlte/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('/adminlte/dist/js/demo.js') }}"></script>
    <!-- DataTables -->

    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/jquery-datatables-checkboxes/js/dataTables.checkboxes.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/jquery-validation/jquery.validate.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/jquery-validation/additional-methods.min.js') }}">
    </script>
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}">
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            bsCustomFileInput.init();
        });
    </script>
    <script src="https://unpkg.com/jquery-resizable-columns@0.2.3/dist/jquery.resizableColumns.min.js">
    </script>
    <script src="https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.js">
    </script>
    <script src="https://unpkg.com/bootstrap-table@1.18.3/dist/extensions/mobile/bootstrap-table-mobile.min.js">
    </script>
    <script src="https://unpkg.com/bootstrap-table@1.18.3/dist/extensions/resizable/bootstrap-table-resizable.min.js">
    </script>
    <script
        src="https://unpkg.com/bootstrap-table@1.18.3/dist/extensions/sticky-header/bootstrap-table-sticky-header.min.js"
    >
    </script>
    <script src="https://unpkg.com/bootstrap-table@1.18.3/dist/extensions/addrbar/bootstrap-table-addrbar.min.js">
    </script>
    @stack('script')
</body>

</html>
