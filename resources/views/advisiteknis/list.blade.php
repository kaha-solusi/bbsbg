@extends('layouts.dashboard.client')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    ['judul' => 'Pendaftaran Bimtek',
    'link' => route('umpan-balik.index')]
    ]
    ])
    @if (session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{ session()->get('success') }}
        </div>
    @endif
    @if (session()->has('failed'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('failed') }}
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="float-left">
                                    List Pendaftaran Advisi Teknis
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover w-100">
                            <thead class="bg-hijau">
                                <tr>
                                    <td>No. Advisi Teknis</td>
                                    <td>Tanggal Pelaksanaan</td>
                                    <td>Surat Pengantar</td>
                                    <td>Berita Acara</td>
                                    <td>Status Pelaksanaan</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>483-981</td>
                                    <td>21 November 2021</td>
                                    <td><a href=""><i class="fas fa-arrow-right" aria-hidden="true"></i> Dokumen Surat Pengantar.pdf</a></td>
                                    <td><a href=""><i class="fas fa-arrow-right" aria-hidden="true"></i> Dokumen Surat Pengantar.pdf</a></td>
                                    <td><a href="">Pengisian Survey Pasca Advisi Teknis</a></td>
                                </tr>
                                <tr>
                                    <td>483-945</td>
                                    <td>22 November 2021</td>
                                    <td>---</td>
                                    <td>---</td>
                                    <td><button type="button" class="btn btn-secondary">Proses Advisi Teknis</button></td>
                                </tr>
                                <tr>
                                    <td>483-573</td>
                                    <td>23 November 2021</td>
                                    <td>---</td>
                                    <td>---</td>
                                    <td><button type="button" class="btn btn-info">Pelaksanaan Advisi Teknis</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection