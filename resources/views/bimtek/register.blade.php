@extends('layouts.moderna')
@section('main')
    <!-- ======= Blog Section ======= -->
    <section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out"
             data-aos-duration="500">
        <div class="container">
            @if (session()->has('success'))
                <div class="alert alert-success" role="alert">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if (session()->has('failed'))
                <div class="alert alert-danger" role="alert">
                    {{ session()->get('failed') }}
                </div>
            @endif
            <div class="card card-register mx-auto mt-5 mb-5">
                <div class="card-header">
                    <h4 style="text-align: center;">Pendaftaran Pelatihan Bimbingan
                        Teknis</h4>
                </div>
                <div class="card-body">
                    <div id="alert"></div>
                    <div class="alert alert-info fade show">
                        <h5 style="font-weight: bold; margin-bottom: 8px;">Petunjuk
                            Bimtek {{ $bimtek->nama }}</h5>
                        <p style="margin-bottom: 0px;">Harap memilih Status Pegawai
                            terlebih dahulu, dengan ketentuan</p>
                        <ol style="margin-bottom: 0px;">
                            <li>Pegawai PNS harap mengisikan NIP</li>
                            <li>Pegawai Non PNS harap mengisikan NRP</li>
                            <li>Pegawai Lainnya (konsultan, TNI/Polri, dll) harap
                                mengisikan NRP</li>
                            <li>Jika tidak memiliki NIP/NRP, isikan 0</li>
                        </ol>
                        <h5
                            style="font-weight: bold; margin-bottom: 8px; margin-top: 10px;">
                            Syarat Peserta :</h5>
                        <p>{{ $bimtek->syarat_peserta }}</p>
                    </div>
                    <form method="POST"
                          action="{{ route('pesertabimtek.store') }}">
                        @csrf
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="font-weight: bold;">Nama
                                            Bimbingan Teknis</label>
                                        <p>{{ $bimtek->nama_bimtek }}</p>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Nama Lengkap Peserta
                                                        (beserta gelar) <span
                                                              style="color: red;">*</span></label>
                                                    <input type="text" name="nama"
                                                           id="nama"
                                                           class="form-control corner-edge"
                                                           required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label id="nip">NIP/NRP <span
                                                              style="color: red;">*</span></label>
                                                    <input type="text" name="nip"
                                                           id="nip"
                                                           class="form-control corner-edge"
                                                           required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Asal Instansi/Unit
                                                        Organisasi/Unit
                                                        Kerja</label>
                                                    <select class="custom-select"
                                                            name="instansi">
                                                        <option value="">Pilih
                                                            Instansi...</option>
                                                        @foreach ($instansis as $instansi)
                                                            <option
                                                                    value="{{ $instansi->nama_instansi }}">
                                                                {{ $instansi->nama_instansi }}
                                                            </option>
                                                        @endfor
                                                        each
                                                        <option value="lain">Lainnya
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label id="nip">Jabatan</label>
                                                    <input type="text"
                                                           name="jabatan"
                                                           id="jabatan"
                                                           class="form-control corner-edge"
                                                           required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>No. HP <span
                                                              style="color: red;">*</span></label>
                                                    <input type="text" name="no_hp"
                                                           id="no_hp"
                                                           class="form-control corner-edge"
                                                           required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label id="nip">E-Mail <span
                                                              style="color: red;">*</span></label>
                                                    <input type="text" name="email"
                                                           id="email"
                                                           class="form-control corner-edge"
                                                           required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="hidden" name="bimtek_id"
                                                   id="bimtek_id"
                                                   value="{{ $bimtek->id }}"
                                                   class="form-control corner-edge"
                                                   required="">
                                        </div>
                                        <div class="col-md-6">
                                            <button type="submit"
                                                    class="btn btn-primary btn-block corner-edge"
                                                    style="margin-bottom: 10px;">Daftar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section><!-- End Blog Section -->
@endsection
