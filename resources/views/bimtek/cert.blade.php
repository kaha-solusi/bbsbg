@extends('layouts.moderna')
@section('main')
<!-- ======= Blog Section ======= -->
<section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSf8-prBV09U1Ui50iGt_qlhQlCMvq-uPLWIVY5VXliZbmi4Kw/viewform?embedded=true" width="1000" height="600" frameborder="0" marginheight="0" marginwidth="0">Memuat…</iframe>
            </div>
        </div>
    </div>
@endsection
