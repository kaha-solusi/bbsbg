@extends('layouts.dashboard.client')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    ['judul' => 'Pendaftaran Bimtek',
    'link' => route('umpan-balik.index')]
    ]
    ])
    @if (session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{ session()->get('success') }}
        </div>
    @endif
    @if (session()->has('failed'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('failed') }}
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="float-left">
                            List Pendaftaran Bimtek
                        </div>
                        <div class="float-right">
                            <a
                                href="{{ route('home.pelayanan.extra',4) }}"
                                class="ml-1 btn btn-primary btn-sm"
                            >Daftar Bimtek</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php
                            $registered_bimtek = array();
                            foreach($pesertas as $peserta) {
                                $registered_bimtek[] = $peserta->bimtek_id;
                            }
                        ?>
                        <table class="table table-hover w-100">
                            <thead class="bg-hijau">
                                <tr>
                                    <td>No.</td>
                                    <td width="250">Nama Bimbingan Teknis</td>
                                    <td>Tanggal Pelaksanaan</td>
                                    <td>Waktu Pelaksanaan</td>
                                    <td>Status Pelaksanaan</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0; ?>
                                @foreach ($bimteks as $bimtek)
                                    <?php
                                        // relationship hack
                                        if( !in_array($bimtek->id, $registered_bimtek) ) {
                                            continue;
                                        }
                                    ?>
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $bimtek->nama_bimtek }}</td>
                                        <td>
                                            @if (is_null($bimtek->tanggal_pelaksanaan))
                                                <span
                                                    class="warning">TBA</span>
                                            @else
                                                <?php echo date('j F Y', strtotime($bimtek->tanggal_pelaksanaan)); ?>
                                        </td>
                                @endif
                                <td><?php echo $bimtek->waktu_pelaksanaan . ' WIB'; ?></td>
                                <td>{{ $bimtek->status_pelaksanaan }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
