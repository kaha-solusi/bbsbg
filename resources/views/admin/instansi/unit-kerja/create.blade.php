@extends('layouts.adminlte')
@section('main')
    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Tambah Unit Kerja
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <form
                            action="{{ route('admin.instansi.unit-kerja.store', $instansi->id) }}"
                            method="POST"
                            accept-charset="utf-8"
                            enctype="multipart/form-data"
                        >
                            @csrf
                            <div class="form-group">
                                <label>Nama Unit Kerja <small class="text-danger">*Harus
                                        diisi</small></label>
                                <input
                                    type="text"
                                    name="nama"
                                    class="form-control @error('nama') is-invalid @enderror"
                                    value="{{ old('nama') }}"
                                >
                                @error('nama')
                                    <span
                                        class="invalid-feedback"
                                        role="alert"
                                    >
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="float-right">
                                    <button
                                        type="submit"
                                        class="btn btn-success btn-sm"
                                    >Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
