@extends('layouts.adminlte')
@section('main')
    <div class="container">
        @if (session()->has('success'))
            <div
                class="alert alert-success"
                role="alert"
            >
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session()->has('failed'))
            <div
                class="alert alert-danger"
                role="alert"
            >
                {{ session()->get('failed') }}
            </div>
        @endif
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Edit Unit Kerja
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form
                            action="{{ route('admin.instansi.unit-kerja.update', [$instansi->id, $unitKerja->id]) }}"
                            method="POST"
                            accept-charset="utf-8"
                            enctype="multipart/form-data"
                        >
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label>Nama Unit Kerja <small class="text-danger">*Harus
                                        diisi</small></label>
                                <input
                                    type="text"
                                    name="nama"
                                    class="form-control @error('nama') is-invalid @enderror"
                                    value="{{ old('nama') ?? $unitKerja->nama_instansi }}"
                                >
                                @error('nama')
                                    <span
                                        class="invalid-feedback"
                                        role="alert"
                                    >
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="float-right">
                                    <button
                                        type="submit"
                                        class="btn btn-success btn-sm"
                                    >Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
