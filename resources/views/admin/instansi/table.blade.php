@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Akun',
    'link' => route('admin.akun.index')
    ]]
    ])
    <div class="container">
        @if (session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session()->has('failed'))
            <div class="alert alert-danger" role="alert">
                {{ session()->get('failed') }}
            </div>
        @endif
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div
                             class="d-flex justify-content-between align-items-center">
                            <div class="justify-self-right">
                                <a href="{{ route('instansi.create') }}"
                                   class="btn btn-sm btn-primary">Tambah Instansi</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead class="thead-custom">
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama Instansi</th>
                                        <th scope="col">Jenis</th>
                                        <th scope="col">Alamat</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1 @endphp
                                    @forelse($data as $instansi)
                                        <tr class="tbody-custom">
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $instansi->nama_instansi }}
                                            </td>
                                            <td>{{ $instansi->jenis_instansi }}
                                            </td>
                                            <td>{{ $instansi->alamat_instansi }}
                                            </td>
                                            <td>
                                                <a href="{{ route('instansi.edit', ['id' => $instansi->id]) }}"
                                                   class="btn btn-warning btn-sm">Edit</a>
                                                <form onsubmit="return confirm('Hapus data permanen ?');"
                                                      action="{{ route('instansi.destroy', $instansi->id) }}"
                                                      method="post"
                                                      class="d-inline">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit"
                                                            class="btn d-inline btn-sm btn-danger">Hapus</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="tbody-custom">
                                            <td colspan="7" class="text-center">
                                                Tidak ada data.</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
