@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Akun',
    'link' => route('admin.akun.index')
    ]]
    ])
    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="float-left">
                                    Unit Organisasi
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row float-right">
                                    <div class="col-md-12">
                                        <a
                                            href="{{ route('admin.instansi.create') }}"
                                            class="btn btn-sm btn-primary"
                                        >Tambah
                                            Unit
                                            Organisasi</a>
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush
