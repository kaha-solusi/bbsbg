@extends('layouts.adminlte')
@section('main')
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="float-left">
                                <a href="{{ route('pekerjaan.index') }}" class="btn btn-primary btn-sm">kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('pekerjaan.store') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                                @csrf
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Nama Pekerjaan <small class="text-success">*Harus diisi</small></label>
                                                <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{ old('nama') }}">
                                                @error('nama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Waktu Pelaksanaan <small class="text-success">*Harus diisi</small></label>
                                                <input type="date" name="waktu_pelaksanaan" class="form-control @error('waktu_pelaksanaan') is-invalid @enderror" value="{{ old('waktu_pelaksanaan') }}">
                                                @error('waktu_pelaksanaan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Pilih Lab <small class="text-success">*Harus diisi</small></label>
                                                <select name="lab_id" class="form-control @error('lab_id') is-invalid @enderror">
                                                    @forelse($labs as $lab)
                                                    <option value="{{ $lab->id }}">{{ substr($lab->keterangan, 0, 50) }}</option>
                                                    @empty
                                                    <option value="" disabled>-- Tidak ada data --</option>
                                                    @endforelse
                                                </select>
                                                @error('lab_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Pilih Client <small class="text-success">*Harus diisi</small></label>
                                                <select name="client_id" class="form-control @error('client_id') is-invalid @enderror">
                                                    @forelse($clients as $client)
                                                    <option value="{{ $client->id }}">{{ $client->nama }}</option>
                                                    @empty
                                                    <option value="" disabled>-- Tidak ada data --</option>
                                                    @endforelse
                                                </select>
                                                @error('client_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Penanggung Jawab <small class="text-success">*Harus diisi</small></label>
                                                <input type="text" name="penaggung_jawab" class="form-control @error('penaggung_jawab') is-invalid @enderror" value="{{ old('penanggung_jawab') }}">
                                                @error('penaggung_jawab')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Pembiayaan <small class="text-success">*Harus diisi</small></label>
                                                <input type="number" min="0" name="pembiayaan" class="form-control @error('pembiayaan') is-invalid @enderror" value="{{ old('pembiayaan') }}">
                                                @error('pembiayaan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Keterangan <small class="text-success">*Harus diisi</small></label>
                                                <textarea class="form-control @error('keterangan') is-invalid @enderror" name="keterangan" rows="5">{{ old('keterangan') }}</textarea>
                                                @error('keterangan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-3" id="btnAdd">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-success btn-sm">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">

</script>
@endpush