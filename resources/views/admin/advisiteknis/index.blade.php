@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Akun',
    'link' => route('admin.akun.index')
    ]]
    ])
    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <h3 class="card-title">Advis Teknis</h3>
                        <div class="card-tools">
                            <a
                                href="{{ route('pelayanan.edit', ['id' => 3]) }}"
                                class="btn btn-md"
                            >
                                <i class="fas fa-sliders-h"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body ">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush
