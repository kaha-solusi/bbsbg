@extends('layouts.adminlte')
@push('style')
    @can('can.upload-document.advis-teknis')
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.min.css"
            integrity="sha512-jU/7UFiaW5UBGODEopEqnbIAHOI8fO6T99m7Tsmqs2gkdujByJfkCbbfPSN4Wlqlb9TGnsuC0YgUgWkRBK7B9A=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"
        />
    @endcan
@endpush
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Akun',
    'link' => route('admin.akun.index')
    ]]
    ])
    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <h3 class="float-left">Advis Teknis</h3>
                    </div>
                    <div class="card-body">
                        <form
                            method="POST"
                            action="{{ route('admin.advisi-teknis.update', $advisiTeknis->id) }}"
                            enctype="multipart/form-data"
                        >
                            @csrf
                            @method('PUT')
                            <input
                                type="hidden"
                                name="status"
                                value="{{ $advisiTeknis->status }}"
                            />
                            <div class="row">
                                <div class='col-md-12'>
                                    <div class="form-group">
                                        <label for="informasiAdvisi">Informasi Advis Teknis</label>
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td width="200"><strong>Waktu Konsultasi Awal</strong></td>
                                                    <td>{{ date('d-m-Y', strtotime($advisiTeknis->test_date)) }}</td>
                                                </tr>
                                                @if ($advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED)
                                                        <tr>
                                                            <td width="200"><strong>Waktu Konfirmasi</strong></td>
                                                            <td>
                                                                <input
                                                                    type="text"
                                                                    class="form-control datepicker"
                                                                    name="waktu_konfirmasi"
                                                                    @if (!is_null($advisiTeknis->confirmation_date))
                                                                    value="{{ date('d-m-Y', strtotime($advisiTeknis->confirmation_date))  }}"
                                                                    @else
                                                                    value="{{ date('d-m-Y', strtotime($advisiTeknis->test_date))  }}"
                                                                    @endif
                                                                    @if ($advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT ) disabled @endif
                                                                />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="200"><strong>Jam Konsultasi</strong></td>
                                                            <td>
                                                                <input
                                                                    type="time"
                                                                    class="form-control datetimepicker-input @error('time') is-invalid @enderror"
                                                                    name="time"
                                                                    id="time"
                                                                    value="{{ $advisiTeknis->time ?? '' }}"
                                                                    @if ($advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT ) disabled @endif
                                                                />
                                                                @error('time')
                                                                <span
                                                                    class="invalid-feedback"
                                                                    role="alert"
                                                                >
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </td>
                                                        </tr>
                                                @endif
                                                <tr>
                                                    <td><strong>Lingkup Konsultasi</strong></td>
                                                    <td>{{ $advisiTeknis->scope }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Identitas Bangunan</strong></td>
                                                    <td>{{ $advisiTeknis->building_identity }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Lokasi</strong></td>
                                                    <td>{{ $location }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Permasalahan</strong></td>
                                                    <td>{{ $advisiTeknis->problem }}</td>
                                                </tr>
                                                @if ($advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED)
                                                <tr>
                                                    <td><strong>Penugasan Pegawai<small class="text-danger">
                                                                * *Harus diisi</small></strong></td>
                                                    <td>
                                                        @if ($advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT)
                                                            <select
                                                                class="form-control custom-select user-list @error('users') is-invalid @enderror"
                                                                multiple
                                                                name="users[]"
                                                                required
                                                                @if ($advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED || $advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE || $advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED) disabled @endif
                                                            >
                                                                @foreach ($pegawai as $p)
                                                                    <option
                                                                        value="{{ $p['id'] }}"
                                                                        data-avatar='{{ $p['avatar'] }}'
                                                                        @if ($advisiTeknis->members->contains($p['id'])) selected @endif
                                                                    >{{ $p['name'] }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('users')
                                                            <span
                                                                class="invalid-feedback"
                                                                role="alert"
                                                            >
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        @endif
                                                        @foreach ($pegawai as $p)
                                                            @foreach ($penugasans as $penugasan)
                                                                @if ($p['id'] == $penugasan->pegawai_id)
                                                                    <select
                                                                        class="form-control custom-select user-list"
                                                                        multiple
                                                                        name="users[]"
                                                                        @if ($advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT ) disabled @endif
                                                                    >
                                                                            <option
                                                                                value="{{ $p['id'] }}"
                                                                                data-avatar='{{ $p['avatar'] }}'
                                                                                @if ($advisiTeknis->members->contains($p['id'])) selected @endif
                                                                            >{{ $p['name'] }}</option>
                                                                    </select>
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    </td>
                                                </tr>
                                                @endif

                                                @can('can.add-member.advis-teknis')
                                                    <tr>
                                                        <td><strong>Dokumen Pendukung</strong></td>
                                                        <td>
{{--                                                            <ul>--}}
                                                                @foreach ($advisiTeknis->files as $file)
                                                                    <li>
                                                                        <a
                                                                            href="{{ route('file.download', ['path' => $file->path]) }}">
                                                                            {{ $file->filename }}
                                                                        </a>
                                                                    </li>
                                                                @endforeach
{{--                                                            </ul>--}}
                                                        </td>
                                                    </tr>
                                                @endcan

                                                @if ($advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED)
                                                    <tr>
                                                        <td><strong>Alasan Ditolak</strong></td>
                                                        <td>
                                                            {{ $advisiTeknis->approvals()->having('pivot_status', \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED)->get()->last()->pivot->reason ?? '' }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Ditolak Oleh</strong></td>
                                                        <td>
                                                            {{ $advisiTeknis->approvals()->having('pivot_status', \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED)->get()->last()->name ?? '' }}
                                                        </td>
                                                    </tr>
                                                @endif

                                                @if ($advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED || $advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE || $advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED)
                                                    <tr>
                                                        <td><strong>Berita Acara</strong></td>
                                                        <td>
                                                            <a
                                                                href="{{ '/uploads/advisi-teknis/' . $advisiTeknis->id . '/' . $advisiTeknis->news_report }}"
                                                                target="_blank"
                                                            >
                                                                {{ $advisiTeknis->news_report }}
                                                            </a>

                                                            @if (($advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED || $advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE ) && $advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED)
                                                                @can('can.upload-document.advis-teknis')
                                                                    <div class="custom-file mt-2">
                                                                        <input
                                                                            type="file"
                                                                            class="custom-file-input"
                                                                            id="news_report"
                                                                            name="news_report"
                                                                            accept="application/pdf"
                                                                            @if ($advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED) disabled @endif
                                                                        >
                                                                        <label
                                                                            class="custom-file-label"
                                                                            for="news_report"
                                                                        >Pilih file</label>
                                                                    </div>
                                                                    @error('news_report')
                                                                    <span
                                                                        class="invalid-feedback"
                                                                        role="alert"
                                                                    >
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                @endcan
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif

                                                @if ($advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED || $advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE || $advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED)
                                                    <tr>
                                                        <td><strong>Surat Pengantar</strong></td>
                                                        <td>
                                                            <a
                                                                href="{{ '/uploads/advisi-teknis/' . $advisiTeknis->id . '/' . $advisiTeknis->letter_of_introduction }}"
                                                                target="_blank"
                                                            >
                                                                {{ $advisiTeknis->letter_of_introduction }}
                                                            </a>
                                                        @if (($advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED || $advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE ) && $advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED)
                                                            <div class="custom-file mt-2">
                                                                <input
                                                                    type="file"
                                                                    class="custom-file-input"
                                                                    id="letter_of_introduction"
                                                                    name="letter_of_introduction"
                                                                    accept="application/pdf"
                                                                    @if ($advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED) disabled @endif
                                                                >
                                                                <label
                                                                    class="custom-file-label"
                                                                    for="letter_of_introduction"
                                                                >Pilih file</label>
                                                            </div>
                                                            @error('letter_of_introduction')
                                                            <span
                                                                class="invalid-feedback"
                                                                role="alert"
                                                            >
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT && $advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL)
                                                    <tr>
                                                        <td><strong>Link Zoom</strong></td>
                                                        <td>
                                                            <a
                                                                href="{{ $zoom->link_zoom }}"
                                                                target="_blank"
                                                            >
                                                                {{ $zoom->link_zoom }}
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Meeting ID</strong></td>
                                                        <td>
                                                            {{ $zoom->meeting_id }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Passcode</strong></td>
                                                        <td>
                                                            {{ $zoom->passcode }}
                                                        </td>
                                                    </tr>
                                                    @endif
                                                @if ($advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED)
                                                <tr>
                                                    <td><strong>Status</strong></td>
                                                    <td>
                                                        @if ($advisiTeknis->status == \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT)
                                                            <span class="badge badge-primary">Penugasan Personil</span>
                                                        @elseif ($advisiTeknis->status ===
                                                            \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL)
                                                            <span class="badge badge-secondary">Menunggu Persetujuan
                                                                Kepala Balai</span>
                                                        @elseif ($advisiTeknis->status ==
                                                            \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED)
                                                            <span class="badge badge-success">Disetujui</span>
                                                        @elseif ($advisiTeknis->status ==
                                                            \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE)
                                                            <span class="badge badge-warning">Menunggu Surat
                                                                Pengantar</span>
                                                        @elseif ($advisiTeknis->status ==
                                                            \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED)
                                                            <span class="badge badge-danger">Ditolak</span>
                                                        @elseif ($advisiTeknis->status ==
                                                            \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED)
                                                            <span class="badge badge-success">Selesai</span>
                                                        @endif
                                                    </td>
                                                </tr>

                                                @if ($advisiTeknis->kuisioners->isNotEmpty())
                                                    <tr>
                                                        <td><strong>Kuisioner</strong></td>
                                                        <td>
                                                            <a
                                                                href="{{ route('admin.advisi-teknis.kuisioner', $advisiTeknis->id) }}"
                                                                class="btn btn-sm btn-primary"
                                                            >Lihat</a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @if ($advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL && $advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT)
                                    @if ($advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED)
                                        <div class="col-md-12">
                                            <div class="float-right">
                                                <button
                                                    class="btn btn-success"
                                                    type="submit"
                                                >Simpan</button>
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-12">
                                            <div class="float-right">
                                                <a
                                                    href="{{ route('admin.advisi-teknis.index') }}"
                                                    class="btn btn-secondary"
                                                    type="submit"
                                                >Kembali</a>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            @endcan
                            @if ($advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT || ($advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL && $advisiTeknis->status !== \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED))
                                <div class="col-md-12">
                                    <div class="float-right">
                                        <a
                                            href="#rejectModal"
                                            class="btn btn-danger"
                                            data-toggle="modal"
                                            data-target="#rejectModal"
                                        >Tolak</a>
                                        <button
                                            class="btn btn-success"
                                            type="submit"
                                        >Setujui</button>
                                    </div>
                                </div>
                            @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT || $advisiTeknis->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL)
        <div
            class="modal fade"
            id="rejectModal"
            tabindex="-1"
            role="dialog"
            aria-labelledby="rejectModalLabel"
            aria-hidden="true"
        >
            <div
                class="modal-dialog"
                role="document"
            >
                <div class="modal-content">

                    <form
                        id="reject-form"
                        action="{{ route('admin.advisi-teknis.reject', $advisiTeknis->id) }}"
                        method="POST"
                    >
                        @csrf
                        <div class="modal-header">
                            <h5
                                class="modal-title"
                                id="exampleModalLabel"
                            >Alasan Ditolak</h5>
                            <button
                                type="button"
                                class="close"
                                data-dismiss="modal"
                                aria-label="Close"
                            >
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <textarea
                                    class="form-control"
                                    id="reject-reason"
                                    name="reject-reason"
                                    rows="3"
                                    required
                                ></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button
                                type="button"
                                class="btn btn-secondary"
                                data-dismiss="modal"
                            >Batal</button>
                            <button
                                type="submit"
                                class="btn btn-primary"
                            >Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection

@push('script')
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>--}}
    <script>
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            startDate: new Date('{{$advisiTeknis->test_date}}'),
            autoclose: true,
            daysOfWeekDisabled: [0,6]
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
    <script>
        // Format options
        const format = (item) => {
            if (!item.id) {
                return item.text;
            }

            var url = item.element.getAttribute('data-avatar');
            var img = $("<img>", {
                class: "rounded-circle me-2",
                width: 26,
                src: url
            });
            var span = $("<span>", {
                text: " " + item.text
            });
            span.prepend(img);
            return span;
        }

        function formatState(state) {
            if (!state.id) {
                return state.text;
            }

            var baseUrl = "/user/pages/images/flags";
            var $state = $(
                '<span><img class="img-flag" /> <span></span></span>'
            );

            // Use .text() instead of HTML string concatenation to avoid script injection issues
            $state.find("span").text(state.text);
            $state.find("img").attr("src", baseUrl + "/" + state.element.value.toLowerCase() + ".png");

            return $state;
        };


        // Init Select2 --- more info: https://select2.org/
        $('.user-list').select2({
            templateResult: function(item) {
                return format(item);
            },
            templateSelection: format,
            theme: "bootstrap4",
            placeholder: 'Pilih pegawai..',
        });

        new ClipboardJS('.btn-clipboard');
        $(document).ready(function() {
            bsCustomFileInput.init()

            const $rejectModal = $('#rejectModal');
            $rejectModal.on('show.bs.modal', function(e) {
                // do something...
            })
        });
    </script>

    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    {!! JsValidator::formRequest('App\Http\Requests\AdvisiTeknisRequest') !!}
@endpush
