@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [[
        'judul' => 'Data Akun',
        'link' => route('admin.akun.index')
    ]]
])
<div class="container">
    @include('admin.utils.alert')
    <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <h3 class="float-left">Advis Teknis <strong>Prayit</strong></h3>
                        <?php if( $status == 'penugasan' ) { ?>
                            <h4 class="float-right status_at_detail">Penugasan Personil</h4>
                        <?php } elseif( $status == 'unggah_dokumen' ) { ?>
                            <h4 class="float-right status_at_detail">Unggah Dokumen</h4>
                        <?php } elseif( $status == 'approval' ) { ?>
                            <h4 class="float-right status_at_detail">Persetujuan Dokumen</h4>
                        <?php } ?>
                    </div>
                    <div class="card-body ">

                        <h5 class="card-title">Ketua: <strong>Faiz Sultan</strong></h5>
                        <?php if( $status == 'penugasan' ) { ?>
                            <p class="card-text">Pilih Anggota Advis Teknis:</p>
                        <?php } elseif( $status == 'unggah_dokumen' ) { ?>
                            <p class="card-text">Unggah Dokumen Surat Pengantar dan Berita Acara</p>
                        <?php } elseif( $status == 'approval' ) { ?>
                            <p class="card-text">Persetujuan Dokumen Oleh Pimpinan</p>
                        <?php } ?>

                        <?php if( $status == 'penugasan' ) { ?>
                        <div class="personnels">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="personnel_1" value="Dadri Arbiyakto" data-nip="283749723847" data-jabatan="Pelancong" data-keterangan="Ini Keterangan tentang Dadri">
                                        <label class="form-check-label" for="personnel_1">
                                            Dadri Arbiyakto
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="personnel_2" value="Adhi Yudha Mulia" disabled>
                                        <label class="form-check-label" for="personnel_2">
                                            Adhi Yudha Mulia
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="personnel_3" value="Yoga Megantara" data-nip="574887346874" data-jabatan="Manager" data-keterangan="Ini Keterangan tentang Yoga">
                                        <label class="form-check-label" for="personnel_3">
                                            Yoga Megantara
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="personnel_4" value="Indriansi Nirwana" data-nip="2834798374" data-jabatan="Meong" data-keterangan="Ini Keterangan tentang Indriani">
                                        <label class="form-check-label" for="personnel_4">
                                            Indriansi Nirwana
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="personnel_5" value="Chiko B.M.W." data-nip="5092838675" data-jabatan="Mercedes" data-keterangan="Ini Keterangan tentang Chiko">
                                        <label class="form-check-label" for="personnel_5">
                                            Chiko B.M.W.
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="personnel_6" value="Arkhadia Rhamo" data-nip="47372831" data-jabatan="Iya" data-keterangan="Ini Keterangan tentang Rhamo">
                                        <label class="form-check-label" for="personnel_6">
                                            Arkhadia Rhamo
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="personnel_7" value="Moh. Anwar M" data-nip="438592834" data-jabatan="Penulis" data-keterangan="Ini Keterangan tentang Anwar">
                                        <label class="form-check-label" for="personnel_7">
                                            Moh. Anwar M
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="personnel_8" value="Angga Arief G.S." data-nip="239409497" data-jabatan="Herle" data-keterangan="Ini Keterangan tentang Angga">
                                        <label class="form-check-label" for="personnel_8">
                                            Angga Arief G.S.
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="personnel_9" value="Irene Dwi Mutiara" data-nip="1932477855" data-jabatan="Mbak" data-keterangan="Ini Keterangan tentang Irene">
                                        <label class="form-check-label" for="personnel_9">
                                            Irene Dwi Mutiara
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="personnel_10" value="Wahyu Dwiantoro" data-nip="23459081864587" data-jabatan="Ilahi" data-keterangan="Ini Keterangan tentang Wahyu">
                                        <label class="form-check-label" for="personnel_10">
                                            Wahyu Dwiantoro
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="personnel_11" value="Meisa Legi R" data-nip="56183758743" data-jabatan="Upin" data-keterangan="Ini Keterangan tentang Meisa">
                                        <label class="form-check-label" for="personnel_11">
                                            Meisa Legi R
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <h3 class="float-left">Personil Yang Ditugaskan</h3>
                        <h4 class="float-right">Advis Teknis No. <span class="badge badge-secondary">483-981</span></h4>
                    </div>
                    <div class="card-body ">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama</th>
                                <th scope="col">NIP/NRP</th>
                                <th scope="col">Jabatan</th>
                                <th scope="col">Keterangan</th>
                                </tr>
                            </thead>
                            <?php if( $status == 'penugasan' ) { ?>
                            <tbody class="selected_personnels">
                            </tbody>
                            <?php } elseif( $status == 'unggah_dokumen' || $status == 'approval' ) { ?>
                                <tbody class="selected_personnels">
                                    <tr class="personnel_1"><th scope="row" class="number">1</th><td>Dadri Arbiyakto</td><td>283749723847</td><td>Pelancong</td><td>Ini Keterangan tentang Dadri</td></tr><tr class="personnel_4"><th scope="row" class="number">2</th><td>Indriansi Nirwana</td><td>2834798374</td><td>Meong</td><td>Ini Keterangan tentang Indriani</td></tr><tr class="personnel_5"><th scope="row" class="number">3</th><td>Chiko B.M.W.</td><td>5092838675</td><td>Mercedes</td><td>Ini Keterangan tentang Chiko</td></tr><tr class="personnel_6"><th scope="row" class="number">4</th><td>Arkhadia Rhamo</td><td>47372831</td><td>Iya</td><td>Ini Keterangan tentang Rhamo</td></tr><tr class="personnel_8"><th scope="row" class="number">5</th><td>Angga Arief G.S.</td><td>239409497</td><td>Herle</td><td>Ini Keterangan tentang Angga</td></tr><tr class="personnel_9"><th scope="row" class="number">6</th><td>Irene Dwi Mutiara</td><td>1932477855</td><td>Mbak</td><td>Ini Keterangan tentang Irene</td></tr></tbody>
                            <?php } ?>
                        </table>
                        <br />
                        <h5 style="margin: 20px 0;font-weight:bold;">Informasi Advis Teknis:</h5>
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td width="200"><strong>Waktu Konsultasi</strong></td>
                                    <td>21 November 2021</td>
                                </tr>
                                <tr>
                                    <td><strong>Lingkup Konsultasi</strong></td>
                                    <td>Struktur Atas</td>
                                </tr>
                                <tr>
                                    <td><strong>Identitas Bangunan</strong></td>
                                    <td>Punden Berundak</td>
                                </tr>
                                <tr>
                                    <td><strong>Lokasi</strong></td>
                                    <td>Jl. Magelang km. 9, Sambisari, Candi Mbuh.</td>
                                </tr>
                                <tr>
                                    <td><strong>Permasalahan</strong></td>
                                    <td>Terkubur</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php if( $status == 'penugasan' ) { ?>
                    <div class="card card-block d-flex">
                        <form>
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="link_zoom">Link Zoom</label>
                                            <input type="text" class="form-control" id="link_zoom" aria-describedby="link_zoom">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-primary float-right" style="margin-top: 32px;"><strong>Selesaikan Penugasan</strong></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <?php } elseif( $status == 'unggah_dokumen' ) { ?>
                    <div class="card card-block d-flex">
                        <form>
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h5 style="margin-bottom: 15px;font-weight:bold;">Surat Pengantar</h5>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="surat_pengantar">
                                            <label class="custom-file-label" for="surat_pengantar">Unggah Surat Pengantar</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <h5 style="margin-bottom: 15px;font-weight:bold;">Berita Acara</h5>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="berita_acara">
                                            <label class="custom-file-label" for="berita_acara">Unggah Berita Acara</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary float-right" style="margin-top: 32px;"><strong>Unggah Dokumen Pendukung</strong></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <?php } elseif( $status == 'approval' ) { ?>
                    <div class="card card-block d-flex">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col"><h5 style="margin-bottom: 0px;font-weight:bold;">Persetujuan Pimpinan</h5></div>
                                <div class="col"><a href=""><i class="fas fa-arrow-right"></i> Dokumen Surat Pengantar.pdf</a></div>
                                <div class="col"><a href=""><i class="fas fa-arrow-right"></i> Dokumen Berita Acara.pdf</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-block d-flex">
                        <form>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title" style="font-weight:bold;">Faiz Sultan</h5>
                                        <p class="card-text">Ketua Advis Teknis</p>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio1" name="ketua_advisi_teknis" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio1">Setujui</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio2" name="ketua_advisi_teknis" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio2">Tolak</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <h5 class="card-title" style="font-weight:bold;">Muhammad Rusli</h5>
                                        <p class="card-text">Sub Koordinator Plt Pengujian</p>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio3" name="sub_koor" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio3">Setujui</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio4" name="sub_koor" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio4">Tolak</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <h5 class="card-title" style="font-weight:bold;">Ferri Eka Putra</h5>
                                        <p class="card-text">Kepala Balai</p>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio5" name="kabalai" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio5">Setujui</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio6" name="kabalai" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio6">Tolak</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary float-right" style="margin-top: 32px;"><strong>Selesaikan Persetujuan</strong></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script type="text/javascript">
        $( document ).ready(function() {
            var num = 0;
            $("input[type='checkbox']").on("change", function(e) {

                if(this.checked) {
                    num++;
                    var html = "";
                    html += "<tr class='"+ $(this).attr('id') +"'>";
                        html += "<th scope='row' class='number'>"+ num +"</th>";
                        html += "<td>"+ $(this).val() +"</td>";
                        html += "<td>" + $(this).data('nip') + "</td>";
                        html += "<td>" + $(this).data('jabatan') + "</td>";
                        html += "<td>" + $(this).data('keterangan') + "</td>";
                    html += "</tr>";

                    $(".selected_personnels").append(html);
                    console.log(html);
                } else {
                    num--;
                    $(".selected_personnels").find("."+$(this).attr('id')).remove();
                    $( "th.number" ).each(function( index ) {
                        $( this ).text(index+1);
                    });
                }
            });
        });
    </script>
@endpush