@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
	'judul' => [
        ['judul' => 'Data Dokumentasi',
		'link' => route('galeri.index')],
        ['judul' => 'Tambah Data Dokumentasi',
        'link' => route('galeri.create')]
    ]
])
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="float-left">
                                Tambah Data Dokumentasi
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('galeri.store') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Foto <small class="text-success">*Harus diisi</small></label>
                                                <input type="file" name="foto" class="form-control-file @error('foto') is-invalid @enderror" aria-describedby="detail">
                                                <small id="detail" class="form-text text-danger">Tipe file image: JPG, JPEG, PNG; Max berukuran 2mb</small>
                                                @error('foto')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Kategori Dokumentasi <small class="text-success">*Harus diisi</small></label>
                                                <select value="kegiatan" name="kategori" id="kategori" class="form-control">
                                                    <option value="">Pilih Kategori Dokumentasi</option>
                                                    <option value="kegiatan">Dokumentasi Kegiatan</option>
                                                    <option value="pengujian">Dokumentasi Pengujian</option>
                                                </select>
                                                @error('kategori')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Judul Foto <small class="text-success">*Harus diisi</small></label>
                                                <input type="text" name="judul" class="form-control @error('judul') is-invalid @enderror" value="{{ old('judul') }}">
                                                @error('judul')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-3" id="btnAdd">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-success btn-sm">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">

</script>
@endpush