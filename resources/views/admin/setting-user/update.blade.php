@extends('layouts.adminlte')
@section('main')
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="float-left">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('admin.setting.user.store') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    @if ($data->pegawai->foto)
                                        <div class="form-group">
                                            <a
                                                href="{{ '/uploads/foto/pegawai/' . $data->pegawai->foto ?? '' }}"
                                                target="_blank"
                                            >
                                                <img
                                                    width="100"
                                                    class="img-thumbnail"
                                                    src="{{ '/uploads/foto/pegawai/' . $data->pegawai->foto ?? '' }}"
                                                />
                                            </a>
                                        </div>
                                    @endif
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Foto Pegawai</label>
                                            <input
                                                type="file"
                                                name="foto"
                                                class="form-control-file"
                                                aria-describedby="detail"
                                                accept="image/*"
                                            >
                                            <small
                                                id="detail"
                                                class="form-text text-danger"
                                            >Tipe file image: JPG, JPEG, PNG; Max berukuran 5mb</small>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label>Nama <small class="text-success">*Harus diisi</small></label>
                                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ $data->name }}">
                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Username <small class="text-success">*Harus diisi</small></label>
                                                <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" value="{{ $data->username }}">
                                                @error('username')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Email <small class="text-success">*Harus diisi</small></label>
                                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ $data->email }}">
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label>NIP</label>
                                                    <input
                                                        type="number"
                                                        name="nip"
                                                        class="form-control @error('nip') is-invalid @enderror"
                                                        value="{{  $data->pegawai->nip }}"
                                                    >
                                                    @error('nip')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>No HP</label>
                                                    <input
                                                        type="tel"
                                                        name="no_hp"
                                                        class="form-control @error('no_hp') is-invalid @enderror"
                                                        value="{{ $data->pegawai->no_hp }}"
                                                        placeholder="Contoh: 62895389981992"
                                                    >
                                                    @error('no_hp')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Jenis Kelamin</label>
                                                    <select
                                                        name="gender"
                                                        class="form-control selectpicker"
                                                        value="{{ $data->pegawai->gender }}"
                                                    >
                                                        <option value="Laki-laki">Laki-laki</option>
                                                        <option value="Perempuan">Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Tempat Lahir</label>
                                                    <input
                                                        type="text"
                                                        name="tempat_lahir"
                                                        class="form-control @error('tempat_lahir') is-invalid @enderror"
                                                        value="{{ $data->pegawai->birth_place }}"
                                                    >
                                                    @error('tempat_lahir')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Tangal Lahir</label>
                                                    <input
                                                        type="date"
                                                        name="tanggal_lahir"
                                                        class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                                        value="{{ $data->pegawai->birth_date }}"
                                                    >
                                                    @error('tangal_lahir')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Pilih Lab</label>
                                                <select
                                                    name="lab_id"
                                                    class="form-control"
                                                >
                                                    <option value="">Tidak ada lab</option>
                                                    @forelse($labs as $lab)
                                                        <option
                                                            {{ $lab->id === ($data->pegawai->lab_id ?? '') ? 'selected' : '' }}
                                                            value="{{ $lab->id }}"
                                                        >{{ $lab->nama }} ||
                                                            Kategori: <span
                                                                style="text-transform: uppercase;">{{ $lab->kategori }}</span>
                                                        </option>
                                                    @empty
                                                        <option
                                                            value=""
                                                            disabled
                                                        >Tidak ada data lab</option>
                                                    @endforelse
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Pilih Jabatan</label>
                                                <select
                                                    data-live-search="true"
                                                    name="jabatan"
                                                    class="form-control selectpicker @error('jabatan') is-invalid @enderror"
                                                >
                                                    @forelse($jabatans as $jabatan)
                                                        <option
                                                            {{ $jabatan->id === ($data->pegawai->jabatan_id ?? '') ? 'selected' : '' }}
                                                            value="{{ $jabatan->id }}"
                                                        >{{ $jabatan->nama }}
                                                        </option>
                                                    @empty
                                                        <option
                                                            value=""
                                                            disabled
                                                        >Tidak ada data jabatan</option>
                                                    @endforelse
                                                </select>
                                                @error('jabatan')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label>Jabatan Laboratorium</label>
                                                <select
                                                    multiple
                                                    data-live-search="true"
                                                    name="jabatan_laboratorium[]"
                                                    class="form-control selectpicker"
                                                    data-title="Pilih jabatan laboratorium"
                                                >
                                                    @forelse($dataJabatanLab as $jabatanLab)
                                                        <option
                                                            value="{{ $jabatanLab->id }}"
                                                            @if ($data->pegawai->lab_positions->contains('id', $jabatanLab->id)) selected @endif
                                                        >{{ $jabatanLab->name }}
                                                        </option>
                                                    @empty
                                                        <option
                                                            value=""
                                                            disabled
                                                        >Tidak ada data jabatan laboratorium</option>
                                                    @endforelse

                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Pilih Golongan</label>
                                                <select
                                                    name="golongan"
                                                    class="form-control @error('golongan') is-invalid @enderror"
                                                >
                                                    @forelse($golongans as $golongan)
                                                        <option
                                                            {{ $golongan->id === ($data->pegawai->golongan_id ?? '') ? 'selected' : '' }}
                                                            value="{{ $golongan->id }}"
                                                        >{{ $golongan->nama }}
                                                        </option>
                                                    @empty
                                                        <option
                                                            value=""
                                                            disabled
                                                        >Tidak ada data golongan</option>
                                                    @endforelse
                                                </select>
                                                @error('golongan')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>Kompetensi & Keahlian</label>
                                                <select
                                                    multiple
                                                    name="kompetensi[]"
                                                    class="form-control select2-kompetensi"
                                                >
                                                    @foreach($kompetensis as $kompetensi)
                                                        <option
                                                            {{ $kompetensi->pegawai_id === ($data->pegawai->id ?? '') ? 'selected' : '' }}
                                                            value="{{ $kompetensi->nama }}"
                                                        >{{ $kompetensi->nama }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Daftar Latihan/Diklat</label>
                                                <select
                                                    multiple
                                                    name="daftar_diklat[]"
                                                    class="form-control select2-diklat"
                                                    data-title='Masukan Daftar Latihan/Diklat'
                                                >
                                                    @foreach($diklats as $diklat)
                                                        <option
                                                            {{ $diklat->pegawai_id === ($data->pegawai->id ?? '') ? 'selected' : '' }}
                                                            value="{{ $diklat->nama }}"
                                                        >{{ $diklat->nama }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                    </div>
                                    <div class="col-md-12 mb-3" id="btnAdd">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-success btn-sm">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
        <form action="{{ route('admin.setting.user.password') }}" method="post">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Password <small class="text-success">*Harus diisi</small></label>
                            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label>Password Confirmation <small class="text-success">*Harus diisi</small></label>
                            <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" value="{{ old('password_confirmation') }}">
                            @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
      </div>
    </div>
</div>
@endsection
@push('script')

<script>
    $(function() {

        $('.select2-kompetensi')
            // .prepend('<option selected></option>')
            .select2({
                allowClear: false,
                tags: true,
                theme: "bootstrap4",
                createTag: function (params) {
                    var term = $.trim(params.term);

                    if (term === '') {
                        return null;
                    }

                    return {
                        id: term,
                        text: term,
                        is_new: true
                    }
                },
                placeholder: 'Masukkan Kompetensi & Keahlian',
                minimumInputLength: 3,
            });
        $('.select2-diklat')
            // .prepend('<option selected></option>')
            .select2({
                allowClear: false,
                tags: true,
                theme: "bootstrap4",
                createTag: function (params) {
                    var term = $.trim(params.term);


                    if (term === '') {
                        return null;
                    }

                    return {
                        id: term,
                        text: term,
                        is_new: true
                    }
                },
                placeholder: 'Masukkan Daftar Latihan/Diklat',
                minimumInputLength: 3,
            });
    });
</script>

@endpush
