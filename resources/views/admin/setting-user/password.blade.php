@extends('layouts.adminlte')
@section('main')
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="float-left">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('admin.setting.user.store') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Username <small class="text-success">*Harus diisi</small></label>
                                                <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" value="{{ $data->username }}">
                                                @error('username')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Email <small class="text-success">*Harus diisi</small></label>
                                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ $data->email }}">
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-3" id="btnAdd">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-success btn-sm">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">

</script>
@endpush