@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [['judul' => 'Menu Pelayanan']
]])
<style>
.row-card .card-d{
  height: 250px;
  margin-bottom: 3em;
}
</style>
<div class="container">
    @if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
    @endif
    @if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
    @endif
    <div class="row">
        <div class="col-md-12 mb-3">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-left">
                            	Menu - Menu Pelayanan
                            </div>
                            <div class="float-right">
                                <a href="{{ route('pelayananui.create') }}" class="btn btn-primary btn-sm">Tambah Menu Pelayanan</a>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
            @forelse($pelayananUI as $item)
            <div class="col-md-4 col-sm-6 row-card">
                <div class="card-d card bg-white border-0">
                <div class="card-body">
                    <h5 class="card-title animate__animated animate__fadeInDown">{{$item->pelayanan->nama}}</h5>
                    <p class="card-text ">{{$item->keterangan}}</p>
                </div>
                <div class="card-footer bg-white border-0">
                    <div class="row">
			<div class="col">
				<a href="{{ route('pelayananui.edit', $item->id) }}" class="btn d-block btn-sm btn-success">Edit</a>
			</div>
                        <div class="col">
                            <form onsubmit="return confirm('Hapus data permanen ?');" action="{{route('pelayananui.destroy', $item->id)}}" method="post" class="d-inline">
                              @csrf
                              @method('delete')

                              <button type="submit" class="btn d-inline btn-sm btn-danger" style="width: 100%;">Hapus</button>
                            </form> 
			</div>
                    </div>
                </div>
                </div>
            </div>
            @empty
            <div class="row h-100">
                <div class="col-md-12">
                <p class="text-center">Data tidak ada</p>
                </div>
            </div>
            
            @endforelse
        </div>
        <div class="col-md-12">
        	{{ $pelayananUI->links('layouts.parts.pagination') }}
        </div>
	</div>
</div>
<form action="" id="formDelete" method="POST">
    @csrf
    @method('DELETE')
</form>
@endsection
@include('homepage.parts.modals', ['pelayanan' => $pelayanan])
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
	function deleteData(id) {
		let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

		const formDelete = document.getElementById('formDelete')
        formDelete.action = '/admin/pelayananui/'+id;

		if (r == true) {
			formDelete.submit();
		} else {
			alert('Penghapusan dibatalkan.');
		}
	}
</script>
@endpush