@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', ['judul' => [['judul' => 'Data Media Sosial']]])
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="float-left">
								Data Media Sosial
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
							<form action="{{ route('admin.sosial-media.store') }}" method="POST" accept-charset="utf-8">
								@csrf
				                <div class="row">
				                    <div class="col-md-12">
				                        <div class="form-row">
				                            <div class="form-group col-md-12">
				                                <label>YouTube <small class="text-success">*Tidak Harus diisi</small></label>
				                                <input type="text" name="link_yt" class="form-control" value="{{ $data->link_yt }}">
				                                @error('link_yt')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                            </div>
				                        </div>
				                    </div>
				                    <div class="col-md-12">
				                        <div class="form-row">
				                            <div class="form-group col-md-12">
				                                <label>Twitter <small class="text-success">*Tidak Harus diisi</small></label>
				                                <input type="text" name="link_twitter" class="form-control" value="{{ $data->link_twitter }}">
				                                @error('link_twitter')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                            </div>
				                        </div>
				                    </div>
				                    <div class="col-md-12">
				                        <div class="form-row">
				                            <div class="form-group col-md-12">
				                                <label>Instagram <small class="text-success">*Tidak Harus diisi</small></label>
				                                <input type="text" name="link_ig" class="form-control" value="{{ $data->link_ig }}">
				                                @error('link_ig')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                            </div>
				                        </div>
				                    </div>
				                    <div class="col-md-12">
				                        <div class="form-row">
				                            <div class="form-group col-md-12">
				                                <label>Facebook <small class="text-success">*Tidak Harus diisi</small></label>
				                                <input type="text" name="link_fb" class="form-control" value="{{ $data->link_facebook }}">
				                                @error('link_fb')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                            </div>
				                        </div>
				                    </div>
				                    <div class="col-md-12">
				                        <div class="float-right">
				                            <button type="submit" class="btn btn-success btn-sm">Update</button>
				                        </div>
				                    </div>
				                </div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	{{-- <div class="container">
		<div class="row justify-content-center">
			<div class="col-md-4">
				<div class="card">
					<div class="card-header">
						<p class="text-center">Total Lab</p>
					</div>
				</div>
			<div class="col-md-4">
				<div class="card">
					<div class="card-header">
						<p class="text-center">Total Lab</p>
					</div>
				</div>
			<div class="col-md-4">
				<div class="card">
					<div class="card-header">
						<p class="text-center">Total Lab</p>
					</div>
				</div>
			</div>
		</div>
	</div> --}}
@endsection