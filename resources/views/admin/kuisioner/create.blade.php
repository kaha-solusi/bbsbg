@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Pegawai',
    'link' => route('admin.pegawai.index')
    ],
    ['judul' => 'Tambah Data Pegawai']
    ]])
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Tambah Data Kuisioner
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    action="{{ route('admin.kuisioners.store') }}"
                                    method="POST"
                                    accept-charset="utf-8"
                                    enctype="multipart/form-data"
                                >
                                    @csrf
                                    <div class="form-group">
                                        <label for='kategori_pelayanan'>Kategori Pelayanan</label>
                                        <select
                                            name="kategori_pelayanan"
                                            id="kategori_pelayanan"
                                            class="form-control"
                                        >
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for='pernyataan'>Pernyataan</label>
                                        <input
                                            type="text"
                                            name="pernyataan"
                                            id="pernyataan"
                                            class="form-control"
                                            placeholder="Pernyataan"
                                            value="{{ old('pernyataan') }}"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label for='order'>Urutan</label>
                                        <input
                                            min="0"
                                            type="number"
                                            name="order"
                                            id="order"
                                            class="form-control"
                                            value="{{ old('order') }}"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label for='keterangan'>Keterangan</label>
                                        <textarea
                                            type="text"
                                            name="keterangan"
                                            id="keterangan"
                                            class="form-control"
                                        >{{ old('keterangan') }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="float-right">
                                            <button
                                                class="btn btn-primary"
                                                type="submit"
                                            >Simpan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    {!! JsValidator::formRequest('App\Http\Requests\KuisionerRequest') !!}
@endpush
