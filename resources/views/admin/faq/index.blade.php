@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
	'judul' => [[
		'judul' => 'Data faq',
		'link' => route('admin.faq.index')
	]]
])
<div class="container">
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-left">
                            	Data FAQs
                            </div>
                            <div class="float-right">
                                {{-- <button data-toggle="modal" data-target="#import" class="ml-1 btn btn-success btn-sm">Tambah Data</button> --}}
                                <a href="{{ route('admin.faq.create') }}" class="ml-1 btn btn-primary btn-sm">Tambah Data FAQ</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
							<table class="table">
								<thead class="thead-custom">
									<tr>
										<th scope="col">No</th>
										<th scope="col">Pertanyaan</th>
										<th scope="col">Jawaban</th>
										<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
									@php $i = 1 @endphp
									@forelse($data as $faq)
									<tr class="tbody-custom">
										<td>{{ $i++ }}</td>
										<td>{{ $faq->pertanyaan }}</td>
										<td>{{ $faq->jawaban }}</td>
										<td>
											<a href="{{ route('admin.faq.edit', $faq->id) }}" class="btn btn-warning btn-sm">Edit</a>
								<form onsubmit="return confirm('Hapus data permanen ?');" action="{{route('admin.faq.destroy', $faq->id)}}" method="post" class="d-inline">
                              @csrf
                              @method('delete')
                              <button type="submit" class="btn d-inline btn-sm btn-danger">Hapus</button>
                           	 </form>
										
</td>
									</tr>
									@empty
									<tr class="tbody-custom">
										<td colspan="7" class="text-center">Tidak ada data.</td>
									</tr>
									@endforelse
								</tbody>
							</table>
						</div>
                        <div class="col-md-12">
                        	{{ $data->links() }}
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<form action="" id="formDelete" method="POST">
    @csrf
    @method('DELETE')
</form>

@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
	function deleteData(id) {
		let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

		const formDelete = document.getElementById('formDelete')
        formDelete.action = '/admin/faq/'+id;

		if (r == true) {
			formDelete.submit();
		} else {
			alert('Penghapusan dibatalkan.');
		}
	}
</script>
@endpush