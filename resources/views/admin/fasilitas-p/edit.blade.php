@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
	'judul' => [
	[
		'judul' => 'Data Fasilitas Balai',
		'link' => route('fasilitas.index')
	],
	[
	    'judul' => 'Ubah Data Fasilitas'
	]
]])
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="float-left">
                               Ubah Data Fasilitas
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('fasilitas.update', $data->id) }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label>Foto <small class="text-success">*Harus diisi</small></label>
                                                <input type="file" name="foto" class="form-control-file @error('foto') is-invalid @enderror" aria-describedby="detail">
                                                <small id="detail" class="form-text text-danger">Tipe file image: JPG, JPEG, PNG; Max berukuran 2mb</small>
                                                @error('foto')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-8">
                                                <label>Fasilitas <small class="text-success">*Harus diisi</small></label>
                                                <select name="kategori" id="kategori" class="form-control  @error('kategori') is-invalid @enderror" value="{{ old('nama')}}">
                                                    <option value="">Pilih kategori fasilitas</option>
                                                    <option value="bahan">Lab Bahan</option>
                                                    <option value="struktur">Lab Struktur</option>
                                                </select>
                                                {{-- <input type="text" name="nama" class="form-control-file @error('kategori') is-invalid @enderror" value="{{ old('nama') }}"> --}}
                                                @error('kategori')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Nama Sarana / Prasarana <small class="text-success">*Harus diisi</small></label>
                                                <input type="text" name="nama" class="form-control-file @error('nama') is-invalid @enderror" value="{{ $data->nama }}">
                                                @error('nama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Keterangan <small class="text-success">*Tidak Harus diisi</small></label>
                                                <textarea class="form-control @error('keterangan') is-invalid @enderror" name="keterangan" rows="5">{{ $data->keterangan }}</textarea>
                                                @error('keterangan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-3" id="btnAdd">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-success btn-sm">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">

</script>
@endpush