@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [
        [
            'judul' => 'Berita', 
            'link' => route('berita.index'),
        ],
        [
            'judul' => 'Ubah Data Berita',
        ]
]])

<div class="container">
@if (session()->has('success'))
<div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="card">
        <div class="card-header">
            Ubah Berita
        </div>
        <form action="{{route('berita.update', $berita)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <div class="card-body">
                <div class="form-group">
                    <label for="foto">Foto</label><small class="text-success">*Tidak Harus diisi</small>
                    <input type="file" name="foto" id="foto" class="form-control-file @error('foto') is-invalid @enderror">
                    <small id="detail" class="form-text text-danger">Tipe file image: JPG, JPEG, PNG; Max berukuran 2mb</small>
                    @error('foto')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="judul">Judul</label><small class="text-success">*Harus diisi</small>
                    <input type="text" value="{{$berita->judul}}" name="judul" id="judul" class="form-control @error('judul') is-invalid @enderror">
                    @error('judul')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="isi">Isi Berita</label><small class="text-success">*Harus diisi</small>
                    <textarea name="isi" id="isi" rows="25" class="form-control @error('isi') is-invalid @enderror">{{$berita->isi}}</textarea>
                    @error('isi')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection