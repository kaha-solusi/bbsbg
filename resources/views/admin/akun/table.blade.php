@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Akun',
    'link' => route('admin.akun.index')
    ]]
    ])
    <div class="container">
        @if (session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session()->has('failed'))
            <div class="alert alert-danger" role="alert">
                {{ session()->get('failed') }}
            </div>
        @endif
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div
                             class="d-flex justify-content-between align-items-center">
                            <div class="justify-self-left">
                                <form action="{{ route('akun.index') }}"
                                      class="d-inline" method="GET"
                                      accept-charset="utf-8">
                                    @csrf
                                    <div class="input-group">
                                        <input class="form-control d-inline"
                                               type="text" placeholder="Search"
                                               aria-describedby="btn-search"
                                               name="search"
                                               value="{{ $search }}"></input>
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary d-inline"
                                                    type="submit"
                                                    id="btn-search">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="justify-self-right">
                                <a href="{{ route('akun.export') }}"
                                   class="btn btn-sm btn-warning">Export Akun</a>
                                <a href="{{ route('admin.akun.create') }}"
                                   class="btn btn-sm btn-primary">Tambah Akun</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead class="thead-custom">
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Username</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Level</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @forelse($data as $akun)
                                            <tr class="tbody-custom">
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $akun->name }}</td>
                                                <td>{{ $akun->username }}</td>
                                                <td>{{ $akun->email }}</td>
                                                <td>
                                                    @if ($akun->level == '1')
                                                        Admin
                                                    @elseif($akun->level == '2')
                                                        2
                                                    @else
                                                        3
                                                    @endif
                                                </td>
                                                <td>{{ $akun->status }}</td>
                                                <td>
                                                    <a href="{{ route('akun.edit', $akun->id) }}"
                                                       class="btn btn-warning btn-sm">Edit</a>
                                                    <form onsubmit="return confirm('Hapus data permanen ?');"
                                                          action="{{ route('akun.destroy', $akun->id) }}"
                                                          method="post"
                                                          class="d-inline">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit"
                                                                class="btn d-inline btn-sm btn-danger">Hapus</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tbody-custom">
                                                <td colspan="7"
                                                    class="text-center">Tidak ada
                                                    data.</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                {{ $data->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="" id="formDelete" method="POST">
        @csrf
        @method('DELETE')
    </form>
@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">
        function deleteData(id) {
            let r = confirm(
                "Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan."
            );

            const formDelete = document.getElementById('formDelete')
            formDelete.action = '/admin/akun/' + id;

            if (r == true) {
                formDelete.submit();
            } else {
                alert('Penghapusan dibatalkan.');
            }
        }
    </script>
@endpush
