@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Akun',
    'link' => route('admin.akun.index')
    ]]
    ])
    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="justify-self-right">
                                <a
                                    href="{{ route('akun.export') }}"
                                    class="btn btn-sm btn-warning"
                                >Export Akun</a>
                                <a
                                    href="{{ route('admin.akun.create') }}"
                                    class="btn btn-sm btn-primary"
                                >Tambah Akun</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush
