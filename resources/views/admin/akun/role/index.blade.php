@extends('layouts.adminlte')
@section('main')
    {{ Breadcrumbs::render() }}
    <div class="container">
        @include('admin.utils.alert')
        <div class="row">
            @can('add_roles')
                <div class="col-md-12 mb-4">
                    <a
                        href="{{ route('admin.roles.create') }}"
                        class="btn btn-primary float-right"
                    >Tambah Role Baru</a>

                </div>
            @endcan
            <div class="col-md-12">
                <div class="card card-primary card-outline card-outline-tabs">
                    <div class="card-header p-0 border-bottom-0">
                        <ul
                            class="nav nav-tabs"
                            id="role-tabs"
                            role="tablist"
                        >
                            @foreach ($roles as $no => $role)
                                <li class="nav-item">
                                    <a
                                        class="nav-link {{ $no === 0 ? 'active' : '' }}"
                                        id="{{ $role->name }}-tab"
                                        data-toggle="pill"
                                        href="#{{ $role->name }}"
                                        role="tab"
                                        aria-controls="custom-tabs-{{ $role->name }}"
                                        aria-selected="false"
                                    >{{ ucwords(Str::replaceArray('-', [' '], $role->name)) }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div
                        class="card-body"
                        x-data="data()"
                    >
                        <div
                            class="tab-content"
                            id="role-tabs"
                        >
                            @foreach ($roles as $no => $role)
                                <div
                                    class="tab-pane fade show {{ $no === 0 ? 'active' : '' }}"
                                    id="{{ $role->name }}"
                                    role="tabpanel"
                                    aria-labelledby="custom-tabs-{{ $role->name }}"
                                >
                                    <div class="row">
                                        @foreach ($permissions as $perm)
                                            <div class="col-md-3 mb-3">
                                                <div class="custom-control custom-checkbox">
                                                    <input
                                                        class="custom-control-input"
                                                        type="checkbox"
                                                        id="{{ $role->id }}:{{ $perm->name }}"
                                                        @if ($role->hasPermissionTo($perm->name)) checked @endif
                                                        x-on:change="updateRole"
                                                    >
                                                    <label
                                                        for="{{ $role->id }}:{{ $perm->name }}"
                                                        class="custom-control-label {{ Str::of($perm->name)->contains('delete') ? 'text-danger' : '' }}"
                                                    >{{ trans($perm->name) }}</label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    @can('delete_roles')
                                        <form
                                            onsubmit="return confirm('Hapus data permanen ?');"
                                            action="{{ route('admin.roles.destroy', $role->id) }}"
                                            method="post"
                                            class="d-inline"
                                        >
                                            @csrf
                                            @method('delete')
                                            <button
                                                type="submit"
                                                class="btn d-inline btn-sm btn-danger"
                                            >Hapus</button>
                                        </form>
                                    @endcan
                                </div>


                            @endforeach


                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        function data() {
            return {
                updateRole($event) {
                    const targetId = $event.target.getAttribute('id').split(':')
                    const roleId = targetId[0]
                    const permission = targetId[1]
                    const active = $event.target.checked;

                    const url = '{{ route('admin.roles.update', '') }}'
                    fetch(url + '/' + roleId, {
                        method: 'PUT',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}',
                            'content-type': "application/json"
                        },
                        body: JSON.stringify({
                            permission,
                            active
                        })
                    }).then(res => res.json()).then((res) => {
                        Swal.fire({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            title: res,
                            icon: 'success'
                        })
                    })
                }
            }
        }
    </script>
@endpush
