@extends('layouts.adminlte')
@section('main')
    {{ Breadcrumbs::render() }}
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Tambah Role
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    id="createForm"
                                    action="{{ route('admin.roles.store') }}"
                                    method="POST"
                                    accept-charset="utf-8"
                                    enctype="multipart/form-data"
                                >
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Nama</label>
                                                    <input
                                                        type="text"
                                                        name="name"
                                                        class="form-control-file"
                                                        value="{{ old('name') }}"
                                                        required
                                                        placeholder="Admin"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <div class="float-right">
                                                <button
                                                    type="submit"
                                                    class="btn btn-success btn-sm"
                                                >Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

@endpush
