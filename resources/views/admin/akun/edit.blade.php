@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    ['judul' => 'Data Akun',
    'link' => route('admin.akun.index')],
    ['judul' => 'Ubah Data Akun',]
    ]
    ])
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Ubah Data Akun
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    action="{{ route('admin.akun.update', $user->id) }}"
                                    method="POST"
                                    accept-charset="utf-8"
                                    enctype="multipart/form-data"
                                >
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Nama</label>
                                                    <input
                                                        type="text"
                                                        name="name"
                                                        class="form-control-file"
                                                        value="{{ $user->name }}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Username</label>
                                                    <input
                                                        type="text"
                                                        name="username"
                                                        class="form-control"
                                                        value="{{ $user->username }}"
                                                    >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Email</label>
                                                    <input
                                                        type="email"
                                                        name="email"
                                                        class="form-control"
                                                        value="{{ $user->email }}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                {{-- <div class="form-group col-md-6">
                                                    <label>Level</label>
                                                    <select
                                                        name="level"
                                                        class="form-control selectpicker"
                                                        data-title="Pilih Level"
                                                    >
                                                        @foreach ($roles as $role)
                                                            <option
                                                                @if ($role['id'] === intval($user->level)) selected @endif
                                                                value="{{ $role['id'] }}"
                                                            >
                                                                {{ ucwords(str_replace('-', ' ', $role['name'])) }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div> --}}
                                                <div class="form-group col-md-6">
                                                    <label>Status <small class="text-success">*Harus
                                                            diisi</small></label>
                                                    <select
                                                        name="status"
                                                        class="form-control"
                                                    >
                                                        <option
                                                            value="aktif"
                                                            @if ($user->status == 'aktif') selected @endif
                                                        >
                                                            Aktif</option>
                                                        <option
                                                            value="non-aktif"
                                                            @if ($user->status == 'non-aktif') selected @endif
                                                        >
                                                            Non-Aktif</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-12 mb-3"
                                            id="btnAdd"
                                        >
                                            <div class="float-right">
                                                <button
                                                    type="submit"
                                                    class="btn btn-success btn-sm"
                                                >Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    {!! JsValidator::formRequest('App\Http\Requests\AkunRequest', '#createForm') !!}
@endpush
