@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    ['judul' => 'Data Akun',
    'link' => route('admin.akun.index')],
    ['judul' => 'Tambah Data Akun',
    'link' => route('admin.akun.create')]
    ]
    ])
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Tambah Data Akun
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    id="createForm"
                                    action="{{ route('admin.akun.store') }}"
                                    method="POST"
                                    accept-charset="utf-8"
                                    enctype="multipart/form-data"
                                >
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Nama</label>
                                                    <input
                                                        type="text"
                                                        name="name"
                                                        class="form-control-file @error('name') is-invalid @enderror"
                                                        value="{{ old('name') }}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Username</label>
                                                    <input
                                                        type="text"
                                                        name="username"
                                                        class="form-control @error('username') is-invalid @enderror"
                                                        value="{{ old('username') }}"
                                                    >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Email</label>
                                                    <input
                                                        type="email"
                                                        name="email"
                                                        class="form-control @error('email') is-invalid @enderror"
                                                        value="{{ old('email') }}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                {{-- <div class="form-group col-md-6">
                                                    <label>Level</label>
                                                    <select
                                                        name="level"
                                                        data-title="Pilih Level"
                                                        class="form-control selectpicker @error('level') is-invalid @enderror"
                                                    >
                                                        @foreach ($roles as $role)
                                                            <option value="{{ $role['id'] }}">
                                                                {{ ucwords(str_replace('-', ' ', $role['name'])) }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div> --}}
                                                <div class="form-group col-md-6">
                                                    <label>Status</label>
                                                    <select
                                                        name="status"
                                                        class="form-control @error('status') is-invalid @enderror"
                                                    >
                                                        <option value="aktif">Aktif
                                                        </option>
                                                        <option value="non-aktif">
                                                            Non-Aktif</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-12 mb-3"
                                            id="btnAdd"
                                        >
                                            <div class="float-right">
                                                <button
                                                    type="submit"
                                                    class="btn btn-success btn-sm"
                                                >Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    {!! JsValidator::formRequest('App\Http\Requests\AkunRequest', '#createForm') !!}
@endpush
