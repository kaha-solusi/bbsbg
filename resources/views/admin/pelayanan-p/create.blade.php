@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [
        [
            'judul' => 'Pelayanan '.$kategori->nama,
            'link' => route('pelayanan.index', ['kategori' => $kategori->id]),
        ],
        [
            'judul' => 'Simpan Data Pelayanan '.$kategori->nama,
        ]
]])
<div class="container">
@if (session()->has('success'))
<div class="alert alert-success" role="alert">
    {{ session()->get('success') }}
</div>
@endif
@if (session()->has('failed'))
<div class="alert alert-danger" role="alert">
    {{ session()->get('failed') }}
</div>
@endif
<div class="card">
    <div class="card-header bg-biru text-white">
        <h4>{{'Simpan Data Pelayanan '.$kategori->nama}}</h4>
    </div>
    <form action="{{route('pelayanan.store')}}" method="POST" enctype="multipart/form-data" >
        @csrf
        <input type="hidden" name="kategori_id" value="{{$kategori->id}}">
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama {{'Data Pelayanan '.$kategori->nama}}  <small class="text-success">*Harus diisi</small></label>
                <input type="text" name="nama" id="nama" class="form-control @error('nama') is-invalid @enderror">
                @error('nama')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="file">File {{'Pelayanan '.$kategori->nama}}  <small class="text-success">*Tidak Harus diisi</small></label>
                <input type="file" name="file" id="file" class="form-control-file @error('file') is-invalid @enderror" aria-describedby="detail">
                <small id="detail" class="form-text text-danger">Tipe file PDF; Max berukuran 10mb</small>
                @error('file')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="file_sop">File SOP {{'Pelayanan '.$kategori->nama}}  <small class="text-success">*Tidak Harus diisi</small></label>
                <input type="file" name="file_sop" id="file_sop" class="form-control-file @error('file_sop') is-invalid @enderror" aria-describedby="detail">
                <small id="detail" class="form-text text-danger">Tipe file sop PDF; Max berukuran 10mb</small>
                @error('file_sop')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="file_panduan">File Panduan {{'Pelayanan '.$kategori->nama}}  <small class="text-success">*Tidak Harus diisi</small></label>
                <input type="file" name="file_panduan" id="file_panduan" class="form-control-file @error('file_panduan') is-invalid @enderror" aria-describedby="detail">
                <small id="detail" class="form-text text-danger">Tipe file panduan PDF; Max berukuran 10mb</small>
                @error('file_panduan')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            {{-- <div class="form-group">
                <label for="file_panduan">File Biaya Pengujian {{'Pelayanan '.$kategori->nama}}  <small class="text-success">*Tidak Harus diisi</small></label>
                <input type="file" name="file_biaya" id="file_biaya" class="form-control-file @error('file_biaya') is-invalid @enderror" aria-describedby="detail">
                <small id="detail" class="form-text text-danger">Tipe file biaya PDF; Max berukuran 10mb</small>
                @error('file_biaya')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div> --}}
            <div class="form-group">
                <label for="foto">Foto {{'Pelayanan '.$kategori->nama}}  <small class="text-success">*Tidak Harus diisi</small></label>
                <input type="file" name="foto" id="foto" class="form-control-file @error('foto') is-invalid @enderror" aria-describedby="detail">
                <small id="detail" class="form-text text-danger">Tipe file image: JPG, JPEG, PNG; Max berukuran 2mb</small>
                @error('foto')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="text">Teks {{'Pelayanan '.$kategori->nama}}  <small class="text-success">*Tidak Harus diisi</small></label>
                <textarea type="text" name="text" id="text" rows="10" class="form-control @error('text') is-invalid @enderror"></textarea>
                @error('text')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-biru">Simpan</button>
        </div>
    </form>
</div>
</div>
@endsection