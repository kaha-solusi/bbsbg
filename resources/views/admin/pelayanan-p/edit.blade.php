@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    [
    'judul' => 'Pelayanan '.$kategori->nama,
    'link' => route('pelayanan.index', ['kategori' => $kategori->id]),
    ],
    [
    'judul' => 'Ubah Data Pelayanan '.$kategori->nama,
    ]
    ]])
    <div class="container">
        @include('admin.utils.alert')
        <div class="card">
            <div class="card-header bg-biru text-white">
                <h4>{{ 'Ubah Data Pelayanan ' . $kategori->nama }}</h4>
            </div>
            <form
                action="{{ route('pelayanan.update', ['id' => $pelayanan->id]) }}"
                method="POST"
                enctype="multipart/form-data"
            >
                @csrf
                @method('patch')
                <input
                    type="hidden"
                    name="kategori_id"
                    value="{{ $kategori->id }}"
                >
                <div class="card-body">
                    <div class="form-group">
                        <label for="nama">Nama {{ 'Data Pelayanan ' . $kategori->nama }}</label>
                        <input
                            type="text"
                            value="{{ $pelayanan->nama }}"
                            name="nama"
                            id="nama"
                            class="form-control @error('nama') is-invalid @enderror"
                        >
                        @error('nama')
                            <span
                                class="invalid-feedback"
                                role="alert"
                            >
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    @if ($kategori->id === 2)
                        <div class="form-group">
                            <label for="ketua">Nama Ketua</label>
                            <select
                                class="form-control custom-select selectpicker"
                                title="Pilih ketua"
                                name="ketua"
                                data-live-search="true"
                            >
                                @foreach ($pegawai as $p)
                                    <option
                                        value="{{ $p['id'] }}"
                                        data-avatar='{{ $p['avatar'] }}'
                                        @if ($p['id'] === $pelayanan->pegawai_id) selected @endif
                                    >{{ $p['name'] }}</option>
                                @endforeach
                            </select>
                            @error('ketua')
                                <span
                                    class="invalid-feedback"
                                    role="alert"
                                >
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="ketua">Nama Sub Koor Plt Pengujian</label>
                            <select
                                class="form-control custom-select selectpicker"
                                title="Pilih Sub Koor"
                                name="subkoor"
                                data-live-search="true"
                            >
                                @foreach ($subkoor as $p)
                                    <option
                                        value="{{ $p['id'] }}"
                                        data-avatar='{{ $p['avatar'] }}'
                                        @if ($p['id'] === $pelayanan->subkoor_id) selected @endif
                                    >{{ $p['name'] }}</option>
                                @endforeach
                            </select>
                            @error('ketua')
                                <span
                                    class="invalid-feedback"
                                    role="alert"
                                >
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="file">File {{ 'Pelayanan ' . $kategori->nama }} <small class="text-success">*Tidak
                                Harus
                                diisi</small></label>
                        <input
                            type="file"
                            name="file"
                            id="file"
                            class="form-control-file @error('file') is-invalid @enderror"
                            aria-describedby="detail"
                        >
                        <small
                            id="detail"
                            class="form-text text-danger"
                        >Tipe file PDF; Max berukuran 10mb</small>
                        @error('file')
                            <span
                                class="invalid-feedback"
                                role="alert"
                            >
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="file_sop">File SOP {{ 'Pelayanan ' . $kategori->nama }} <small
                                class="text-success">*Tidak Harus diisi</small></label>
                        <input
                            type="file"
                            name="file_sop"
                            id="file_sop"
                            class="form-control-file @error('file_sop') is-invalid @enderror"
                            aria-describedby="detail"
                        >
                        <small
                            id="detail"
                            class="form-text text-danger"
                        >Tipe file sop PDF; Max berukuran 10mb</small>
                        @error('file_sop')
                            <span
                                class="invalid-feedback"
                                role="alert"
                            >
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="file_panduan">File Panduan {{ 'Pelayanan ' . $kategori->nama }} <small
                                class="text-success"
                            >*Tidak Harus diisi</small></label>
                        <input
                            type="file"
                            name="file_panduan"
                            id="file_panduan"
                            class="form-control-file @error('file_panduan') is-invalid @enderror"
                            aria-describedby="detail"
                        >
                        <small
                            id="detail"
                            class="form-text text-danger"
                        >Tipe file panduan PDF; Max berukuran 10mb</small>
                        @error('file_panduan')
                            <span
                                class="invalid-feedback"
                                role="alert"
                            >
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    {{-- <div class="form-group">
                <label for="file_panduan">File Biaya Pengujian {{'Pelayanan '.$kategori->nama}}  <small class="text-success">*Tidak Harus diisi</small></label>
                <input type="file" name="file_biaya" id="file_biaya" class="form-control-file @error('file_biaya') is-invalid @enderror" aria-describedby="detail">
                <small id="detail" class="form-text text-danger">Tipe file biaya PDF; Max berukuran 10mb</small>
                @error('file_biaya')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div> --}}
                    <div class="form-group">
                        <label for="foto">Foto {{ 'Pelayanan ' . $kategori->nama }} <small class="text-success">*Tidak
                                Harus diisi</small></label>
                        <input
                            type="file"
                            name="foto"
                            id="foto"
                            class="form-control-file @error('foto') is-invalid @enderror"
                            aria-describedby="detail"
                        >
                        <small
                            id="detail"
                            class="form-text text-danger"
                        >Tipe file image: JPG, JPEG, PNG; Max berukuran 2mb</small>
                        @error('foto')
                            <span
                                class="invalid-feedback"
                                role="alert"
                            >
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="text">Teks {{ 'Pelayanan ' . $kategori->nama }}</label>
                        <textarea
                            type="text"
                            name="text"
                            id="text"
                            rows="10"
                            class="form-control @error('text') is-invalid @enderror"
                        >{{ $pelayanan->text }}</textarea>
                        @error('text')
                            <span
                                class="invalid-feedback"
                                role="alert"
                            >
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group" id="off_days">
                        <label>Hari Tidak Melayani <small class="text-success">*Tidak Harus diisi</small></label>
                        <input
                            type="text"
                            class="form-control datepicker"
                            name="off_days"
                            value="{{$pelayanan->off_days}}"
                        />
                    </div>
                </div>
                <div
                    class="col-md-12"
                    id="zoom"
                >
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    Zoom Meeting
                                </div>
                                <div class="card-body">
                                    <div class="form-group col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Syarat Peserta<small class="text-success">*Harus
                                                        diisi</small></label>
                                                <textarea
                                                    name="syarat_peserta"
                                                    class="form-control @error('syarat_peserta') is-invalid @enderror"
                                                    rows="4"
                                                >{{ $pelayanan->syarat_peserta }}</textarea>
                                                @error('deskripsi_kegiatan')
                                                    <span
                                                        class="invalid-feedback"
                                                        role="alert"
                                                    >
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Link Zoom</label>
                                                <input
                                                    type="text"
                                                    name="link_zoom"
                                                    class="form-control @error('link_zoom') is-invalid @enderror"
                                                    value="{{ $pelayanan->link_zoom }}"
                                                >
                                                @error('link_zoom')
                                                    <span
                                                        class="invalid-feedback"
                                                        role="alert"
                                                    >
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <div class="form-row">
                                                    <div class="form-group col-sm">
                                                        <label>Meeting ID</label>
                                                        <input
                                                            type="text"
                                                            name="meeting_id"
                                                            class="form-control @error('meeting_id') is-invalid @enderror"
                                                            value="{{ $pelayanan->meeting_id }}"
                                                        >
                                                        @error('meeting_id')
                                                            <span
                                                                class="invalid-feedback"
                                                                role="alert"
                                                            >
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-sm">
                                                        <label>Passcode</label>
                                                        <input
                                                            type="text"
                                                            name="passcode"
                                                            class="form-control @error('passcode') is-invalid @enderror"
                                                            value="{{ $pelayanan->passcode }}"
                                                        >
                                                        @error('passcode')
                                                            <span
                                                                class="invalid-feedback"
                                                                role="alert"
                                                            >
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div
                    class="col-md-12"
                    id="email"
                >
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    Konten Email
                                </div>
                                <div class="card-body">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>Subjek
                                                Email</label>
                                            <input
                                                type="text"
                                                name="subjek_email"
                                                class="form-control @error('subjek_email') is-invalid @enderror"
                                                value="{{ $pelayanan->subjek_email }}"
                                            >
                                            @error('subjek_email')
                                                <span
                                                    class="invalid-feedback"
                                                    role="alert"
                                                >
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="foto">Background
                                                Video
                                                Conference</label>
                                            @if ($pelayanan->background)
                                                <div class="form-group">
                                                    <a
                                                        href="{{ '/pelayanan/background_images/' . $pelayanan->background ?? '' }}"
                                                        target="_blank"
                                                    >
                                                        <img
                                                            width="300"
                                                            class="img-thumbnail"
                                                            src="{{ '/pelayanan/background_images/' . $pelayanan->background ?? '' }}"
                                                        />
                                                    </a>
                                                </div>
                                            @endif
                                            <input
                                                type="file"
                                                name="background"
                                                id="background"
                                                class="form-control-file @error('background') is-invalid @enderror"
                                                aria-describedby="detail"
                                            >
                                            <small
                                                id="detail"
                                                class="form-text text-danger"
                                            >Tipe
                                                file image: JPG,
                                                JPEG, PNG; Max
                                                berukuran
                                                2mb</small>
                                            @error('foto')
                                                <span
                                                    class="invalid-feedback"
                                                    role="alert"
                                                >
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label>Konten
                                                Email</label>
                                            <textarea
                                                name="konten_email"
                                                class="form-control @error('konten_email') is-invalid @enderror"
                                                rows="6"
                                            >{{ $pelayanan->konten_email }}</textarea>
                                            @error('konten_email')
                                                <span
                                                    class="invalid-feedback"
                                                    role="alert"
                                                >
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button
                        type="submit"
                        class="btn btn-biru"
                    >Simpan</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('.datepicker').datepicker({
            format: 'd-m-yyyy',
            multidate: true,
            startDate: '+1d',
            daysOfWeekDisabled: [0,6]
        });
    </script>
    <script>
        $("#zoom").change(function() {
            if ($(nama).val() == "Advis Teknis") {
                $('#zoom').show();
                $('#email').show()
            } else {
                $('#zoom').hide();
                $('#email').hide();
            }
        });
        $("#zoom").trigger("change");
    </script>
    <script>
        $("#off_days").change(function() {
            if ($(nama).val() == "Bimbingan Teknis") {
                $('#off_days').hide();
            } else {
                $('#off_days').show();
            }
        });
        $("#off_days").trigger("change");
    </script>
    {!! JsValidator::formRequest('App\Http\Requests\JadwalKegiatanPersonilRequest') !!}
@endpush
