@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Pegawai',
    'link' => route('admin.pegawai.index')
    ]]
    ])
    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-left">
                                    Data Pegawai
                                </div>
                                <div class="float-right">
                                    <button
                                        data-toggle="modal"
                                        data-target="#import"
                                        class="ml-1 btn btn-success btn-sm"
                                    >Import Data Pegawai</button>
                                    <a
                                        href="{{ route('pegawai.export') }}"
                                        class="ml-1 btn btn-warning btn-sm"
                                    >Export Data Pegawai</a>
                                    <a
                                        href="{{ route('admin.pegawai.create') }}"
                                        class="ml-1 btn btn-primary btn-sm"
                                    >Tambah Data Pegawai</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead class="thead-custom">
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Foto</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Lab</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">No HP</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @forelse($data as $pegawai)
                                            <tr class="tbody-custom">
                                                <td>{{ $i++ }}</td>
                                                <td>
                                                    @if ($pegawai->foto == null)
                                                        Tidak ada foto
                                                    @else
                                                        <img
                                                            src="{{ asset($pegawai->foto) }}"
                                                            class="img-thumbnail"
                                                            alt=""
                                                            style="width: 75px;height: 75px;"
                                                        >
                                                    @endif
                                                </td>
                                                <td>{{ $pegawai->nama }}</td>
                                                <td>{{ $pegawai->lab->nama }}</td>
                                                <td>{{ $pegawai->email }}</td>
                                                <td>{{ $pegawai->no_hp }}</td>
                                                <td>
                                                    <a
                                                        href="{{ route('pegawai.edit', $pegawai->id) }}"
                                                        class="btn btn-warning btn-sm"
                                                    >Edit</a>
                                                    <form
                                                        onsubmit="return confirm('Hapus data permanen ?');"
                                                        action="{{ route('pegawai.destroy', $pegawai->id) }}"
                                                        method="post"
                                                        class="d-inline"
                                                    >
                                                        @csrf
                                                        @method('delete')
                                                        <button
                                                            type="submit"
                                                            class="btn d-inline btn-sm btn-danger"
                                                        >Hapus</button>
                                                    </form>

                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tbody-custom">
                                                <td
                                                    colspan="7"
                                                    class="text-center"
                                                >Tidak ada data.</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                {{ $data->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form
        action=""
        id="formDelete"
        method="POST"
    >
        @csrf
        @method('DELETE')
    </form>

    <!-- Modal -->
    <div
        class="modal fade"
        id="import"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="exampleModalLabel"
                    >Import file data pegawai</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form
                    action="{{ route('pegawai.import') }}"
                    method="POST"
                    enctype="multipart/form-data"
                >
                    @csrf
                    @method('post')
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="file">File data pegawai</label>
                            <input
                                type="file"
                                name="file"
                                id="file"
                                placeholder="file.xlsx"
                                class="form-control-file"
                            >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button
                            type="submit"
                            class="btn btn-primary"
                        >Import</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">
        function deleteData(id) {
            let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

            const formDelete = document.getElementById('formDelete')
            formDelete.action = '/admin/pegawai/' + id;

            if (r == true) {
                formDelete.submit();
            } else {
                alert('Penghapusan dibatalkan.');
            }
        }
    </script>
@endpush
