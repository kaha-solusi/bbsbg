@extends('layouts.adminlte')
@section('main')
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
<div class="container">
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-left">
                            	Pekerjaan
                            </div>
                            <div class="float-right">
                                <a href="{{ route('pekerjaan.create') }}" class="btn btn-primary btn-sm">Tambah Data Pekerjaan</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
							<table class="table">
								<thead class="thead-custom">
									<tr>
										<th scope="col">Timestamp</th>
										<th scope="col">Nama Pekerjaan</th>
										<th scope="col">Nama Lab</th>
										<th scope="col">Nama Client</th>
										<th scope="col">Waktu Pelaksanaan</th>
										<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
									@php $i = 1 @endphp
									@forelse($data as $pekerjaan)
									<tr class="tbody-custom">
										<td>{{ $pekerjaan->time }}</td>
										<td>{{ $pekerjaan->nama }}</td>
										<td>{{ $pekerjaan->lab->keterangan }}</td>
										<td>{{ $pekerjaan->client->nama }}</td>
										<td>{{ $pekerjaan->waktu_pelaksanaan }}</td>
										<td>
											{{-- <button type="button" onclick="sosialMedia('{{ json_decode( $pekerjaan ) }}')" class="btn btn-light btn-sm" data-toggle="modal" data-target="#sosialMediaModal">Detail</button> --}}
											<a href="{{ route('pekerjaan.edit', $pekerjaan->id) }}" class="btn btn-warning btn-sm">Edit</a>
											<button type="button" onclick="deleteData({{ $pekerjaan->id }})" class="btn btn-danger btn-sm">Hapus</button>
										</td>
									</tr>
									@empty
									<tr class="tbody-custom">
										<td colspan="5" class="text-center">Tidak ada data.</td>
									</tr>
									@endforelse
								</tbody>
							</table>
						</div>
                        <div class="col-md-12">
                        	{{ $data->links() }}
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{{-- Modal --}}
<div class="modal fade" id="sosialMediaModal" tabindex="-1" aria-labelledby="mediaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mediaModalLabel">Sosial Media</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="list-group">
        	<li class="list-group-item" id="ig"></li>
        	<li class="list-group-item" id="facebook"></li>
        	<li class="list-group-item" id="twitter"></li>
        	<li class="list-group-item" id="linkedin"></li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
{{-- End Modal --}}

<form action="" id="formDelete" method="POST">
    @csrf
    @method('DELETE')
</form>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
	function deleteData(id) {
		let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

		const formDelete = document.getElementById('formDelete')
        formDelete.action = '/admin/pekerjaan/'+id;

		if (r == true) {
			formDelete.submit();
		} else {
			alert('Penghapusan dibatalkan.');
		}
	}

	function sosialMedia(response) {
		// $('#ig').text("IG: "+ig);
		alert(response);
	}
</script>
@endpush