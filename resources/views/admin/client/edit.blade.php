@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    ['judul' => 'Data Pengguna',
    'link' => route('admin.client.index')],
    ['judul' => 'Ubah Data Pengguna']
    ]
    ])
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Ubah Data Pengguna
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    action="{{ route('admin.client.update', $data->id) }}"
                                    method="POST"
                                    accept-charset="utf-8"
                                    enctype="multipart/form-data"
                                >
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Logo</label>
                                                    <input
                                                        type="file"
                                                        name="logo"
                                                        class="form-control-file"
                                                        aria-describedby="detail"
                                                    >
                                                    <small
                                                        id="detail"
                                                        class="form-text text-danger"
                                                    >Tipe file image: JPG, JPEG, PNG; Max berukuran 2mb</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>E-mail</label>
                                                    <input
                                                        type="text"
                                                        name="email"
                                                        class="form-control"
                                                        value="{{ $data->email }}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Nama</label>
                                                    <input
                                                        type="text"
                                                        name="name"
                                                        class="form-control"
                                                        value="{{ $data->name }}"
                                                    >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Username</label>
                                                    <input
                                                        type="text"
                                                        name="username"
                                                        class="form-control"
                                                        value="{{ $data->username }}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Website</label>
                                                    <input
                                                        type="text"
                                                        name="website"
                                                        class="form-control"
                                                        value="{{ $data->client->website }}"
                                                    >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Nomor Telepon</label>
                                                    <input
                                                        type="text"
                                                        name="phone_number"
                                                        class="form-control"
                                                        value="{{ $data->client->phone_number }}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">

                                                <div class="form-group col-md-6">
                                                    <label>Nama Perusahaan</label>
                                                    <input
                                                        type="text"
                                                        name="nama_perusahaan"
                                                        class="form-control"
                                                        value="{{ $data->client->nama_perusahaan }}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Alamat</label>
                                                    <textarea
                                                        class="form-control"
                                                        name="alamat"
                                                        rows="5"
                                                    >{{ $data->client->alamat }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-12 mb-3"
                                            id="btnAdd"
                                        >
                                            <div class="float-right">
                                                <button
                                                    type="submit"
                                                    class="btn btn-success btn-sm"
                                                >Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    {!! JsValidator::formRequest('App\Http\Requests\ClientRequest') !!}
@endpush
