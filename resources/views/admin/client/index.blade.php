@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    ['judul' => 'Data Pengguna',
    'link' => route('admin.client.index')]
    ]
    ])
    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-left">
                                    Data Pengguna
                                </div>
                                <div class="float-right">
                                    <a
                                        href="{{ route('client.export') }}"
                                        class="btn btn-warning btn-sm"
                                    >Export Data Client</a>
                                    <a
                                        href="{{ route('admin.client.create') }}"
                                        class="btn btn-primary btn-sm"
                                    >Tambah Data Client</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal --}}
    <div
        class="modal fade"
        id="sosialMediaModal"
        tabindex="-1"
        aria-labelledby="mediaModalLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="mediaModalLabel"
                    >Alamat</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="keterangan"></p>
                </div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- End Modal --}}

    {{-- Modal --}}
    <div
        class="modal fade"
        id="urlModal"
        tabindex="-1"
        aria-labelledby="urlModalLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="urlModalLabel"
                    >Website</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="website"></p>
                </div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- End Modal --}}

    <form
        action=""
        id="formDelete"
        method="POST"
    >
        @csrf
        @method('DELETE')
    </form>
@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush
