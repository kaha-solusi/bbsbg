@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Pegawai',
    'link' => route('admin.pegawai.index')
    ],
    ['judul' => 'Tambah Data Pegawai']
    ]])
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Ubah Data Pegawai
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    action="{{ route('admin.pegawai.update', $data->id) }}"
                                    method="POST"
                                    accept-charset="utf-8"
                                    enctype="multipart/form-data"
                                >
                                    @csrf
                                    @method('PUT')
                                    @if ($data->pegawai->foto)
                                        <div class="form-group">
                                            <a
                                                href="{{ '/uploads/foto/pegawai/' . $data->pegawai->foto ?? '' }}"
                                                target="_blank"
                                            >
                                                <img
                                                    width="100"
                                                    class="img-thumbnail"
                                                    src="{{ '/uploads/foto/pegawai/' . $data->pegawai->foto ?? '' }}"
                                                />
                                            </a>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label>Foto Pegawai</label>
                                        <input
                                            type="file"
                                            name="foto"
                                            class="form-control-file"
                                            aria-describedby="detail"
                                            accept="image/*"
                                        >
                                        <small
                                            id="detail"
                                            class="form-text text-danger"
                                        >Tipe file image: JPG, JPEG, PNG; Max berukuran 5mb</small>

                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label>Nama</label>
                                            <input
                                                type="text"
                                                name="nama"
                                                class="form-control"
                                                value="{{ old('nama', $data->name) }}"
                                            >
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>NIP</label>
                                            <input
                                                type="number"
                                                name="nip"
                                                class="form-control"
                                                value="{{ old('nip', $data->pegawai->nip) }}"
                                            >
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>E-mail</label>
                                            <input
                                                type="text"
                                                name="email"
                                                class="form-control"
                                                value="{{ old('email', $data->email) }}"
                                            >
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>No HP</label>
                                            <input
                                                type="tel"
                                                name="no_hp"
                                                class="form-control"
                                                value="{{ old('no_hp', $data->pegawai->no_hp ?? '') }}"
                                                placeholder="Contoh: 62895389981992"
                                            >
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>Tempat Lahir</label>
                                            <input
                                                type="text"
                                                name="birth_place"
                                                class="form-control"
                                                value="{{ old('birth_place', $data->pegawai->birth_place) }}"
                                            >
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Tangal Lahir</label>
                                            <input
                                                type="date"
                                                name="birth_date"
                                                class="form-control"
                                                value="{{ old('birth_date', $data->pegawai->birth_date) }}"
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                        <select
                                            name="gender"
                                            class="form-control selectpicker"
                                            value="{{ old('gender', $data->pegawai->gender) }}"
                                        >
                                            <option value="Laki-laki">Laki-laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Kata Sandi</label>
                                        <input
                                            type="password"
                                            name="password"
                                            class="form-control"
                                            placeholder="supersecretpassword"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label>Pilih Lab</label>
                                        <select
                                            name="lab_id"
                                            class="form-control"
                                        >
                                            <option value="">Tidak ada lab</option>
                                            @forelse($labs as $lab)
                                                <option
                                                    {{ $lab->id === ($data->pegawai->lab_id ?? '') ? 'selected' : '' }}
                                                    value="{{ $lab->id }}"
                                                >{{ $lab->nama }} ||
                                                    Kategori: <span
                                                        style="text-transform: uppercase;">{{ $lab->kategori }}</span>
                                                </option>
                                            @empty
                                                <option
                                                    value=""
                                                    disabled
                                                >Tidak ada data lab</option>
                                            @endforelse
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Pilih Jabatan</label>
                                        <select
                                            data-live-search="true"
                                            name="jabatan_id"
                                            class="form-control selectpicker"
                                        >
                                            @forelse($jabatans as $jabatan)
                                                <option
                                                    {{ $jabatan->id === ($data->pegawai->jabatan_id ?? '') ? 'selected' : '' }}
                                                    value="{{ $jabatan->id }}"
                                                >{{ $jabatan->nama }}
                                                </option>
                                            @empty
                                                <option
                                                    value=""
                                                    disabled
                                                >Tidak ada data jabatan</option>
                                            @endforelse
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Jabatan Laboratorium</label>
                                        <select
                                            multiple
                                            data-live-search="true"
                                            name="jabatan_laboratorium[]"
                                            class="form-control selectpicker"
                                            data-title="Pilih jabatan laboratorium"
                                        >
                                            @forelse($dataJabatanLab as $jabatanLab)
                                                <option
                                                    value="{{ $jabatanLab->id }}"
                                                    @if ($data->pegawai->lab_positions->contains('id', $jabatanLab->id)) selected @endif
                                                >{{ $jabatanLab->name }}
                                                </option>
                                            @empty
                                                <option
                                                    value=""
                                                    disabled
                                                >Tidak ada data jabatan laboratorium</option>
                                            @endforelse

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Pilih Golongan</label>
                                        <select
                                            name="golongan_id"
                                            class="form-control"
                                        >
                                            @forelse($golongans as $golongan)
                                                <option
                                                    {{ $golongan->id === ($data->pegawai->golongan_id ?? '') ? 'selected' : '' }}
                                                    value="{{ $golongan->id }}"
                                                >{{ $golongan->nama }}
                                                </option>
                                            @empty
                                                <option
                                                    value=""
                                                    disabled
                                                >Tidak ada data golongan</option>
                                            @endforelse
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Hak Akses</label>
                                        <select
                                            name="role"
                                            class="form-control selectpicker"
                                            data-title='Pilih jabatan'
                                        >
                                            @foreach ($roles as $role)
                                                <option
                                                    @if ($data->hasRole($role['name'])) selected @endif
                                                    value="{{ $role['id'] }}"
                                                >
                                                    {{ ucwords(str_replace('-', ' ', $role['name'])) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Kompetensi & Keahlian</label>
                                        <select
                                            multiple
                                            name="kompetensi[]"
                                            class="form-control select2-kompetensi"
                                        >
                                            @forelse($kompetensis as $kompetensi)
                                                <option
                                                    {{ $kompetensi->pegawai_id === ($data->pegawai->id ?? '') ? 'selected' : '' }}
                                                    value="{{ $kompetensi->nama }}"
                                                >{{ $kompetensi->nama }}
                                                </option>
                                            @empty
                                                <option
                                                    value=""
                                                    disabled
                                                >Tidak ada data kompetensi & keahlian</option>
                                            @endforelse
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Daftar Latihan/Diklat</label>
                                        <select
                                            multiple
                                            name="daftar_diklat[]"
                                            class="form-control select2-diklat"
                                            data-title='Masukan Daftar Latihan/Diklat'
                                        >
                                            @forelse($diklats as $diklat)
                                                <option
                                                    {{ $diklat->pegawai_id === ($data->pegawai->id ?? '') ? 'selected' : '' }}
                                                    value="{{ $diklat->nama }}"
                                                >{{ $diklat->nama }}
                                                </option>
                                            @empty
                                                <option
                                                    value=""
                                                    disabled
                                                >Tidak ada data daftar latihan/diklat</option>
                                            @endforelse
                                        </select>
                                    </div>
                                    <div
                                        class="col-md-12 mb-3"
                                        id="btnAdd"
                                    >
                                        <div class="float-right">
                                            <button
                                                type="submit"
                                                class="btn btn-success btn-sm"
                                            >Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function() {

            $('.select2-kompetensi')
                // .prepend('<option selected></option>')
                .select2({
                    allowClear: false,
                    tags: true,
                    theme: "bootstrap4",
                    createTag: function(params) {
                        var term = $.trim(params.term);

                        if (term === '') {
                            return null;
                        }

                        return {
                            id: term,
                            text: term,
                            is_new: true
                        }
                    },
                    placeholder: 'Masukkan Kompetensi & Keahlian',
                    minimumInputLength: 3,
                });
            $('.select2-diklat')
                // .prepend('<option selected></option>')
                .select2({
                    allowClear: false,
                    tags: true,
                    theme: "bootstrap4",
                    createTag: function(params) {
                        var term = $.trim(params.term);


                        if (term === '') {
                            return null;
                        }

                        return {
                            id: term,
                            text: term,
                            is_new: true
                        }
                    },
                    placeholder: 'Masukkan Daftar Latihan/Diklat',
                    minimumInputLength: 3,
                });

            $('.select2-laboratorium')
                .prepend('<option selected></option>')
                .select2({
                    allowClear: false,
                    tags: true,
                    theme: "bootstrap4",
                    ajax: {
                        url: '{{ route('admin.pegawai.get-jabatan-laboratorium') }}',
                        dataType: 'json',
                        cache: true,
                        delay: 250
                    },
                    createTag: function(params) {
                        var term = $.trim(params.term);

                        if (term === '') {
                            return null;
                        }

                        return {
                            id: term,
                            text: term,
                            is_new: true
                        }
                    },
                    placeholder: 'Pilih jabatan laboratorium',
                    minimumInputLength: 3,
                })
                .append(new Option('{{ $data->pegawai->jabatan_laboratorium->name ?? '' }}',
                    '{{ $data->pegawai->jabatan_fungsional->id ?? '' }}', true, true));

            $('.select2-fungsional')
                .prepend('<option selected></option>')
                .select2({
                    allowClear: false,
                    tags: true,
                    theme: "bootstrap4",
                    ajax: {
                        url: '{{ route('admin.pegawai.get-jabatan-fungsional') }}',
                        dataType: 'json',
                        cache: true,
                        delay: 250
                    },
                    createTag: function(params) {
                        var term = $.trim(params.term);

                        if (term === '') {
                            return null;
                        }

                        return {
                            id: term,
                            text: term,
                            is_new: true
                        }
                    },
                    placeholder: 'Pilih jabatan fungsional',
                    minimumInputLength: 3,
                })
                .append(new Option('{{ $data->pegawai->jabatan_fungsional->name ?? '' }}',
                    '{{ $data->pegawai->jabatan_fungsional->id ?? '' }}', true, true));
        });
    </script>
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    {!! JsValidator::formRequest('App\Http\Requests\PegawaiRequest') !!}
@endpush
