@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Pegawai',
    'link' => route('admin.pegawai.index')
    ]]
    ])
    <div class="container">
        @alert
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-left">
                                    Data Pegawai
                                </div>
                                <div class="float-right">
                                    <button
                                        data-toggle="modal"
                                        data-target="#import"
                                        class="ml-1 btn btn-success btn-sm"
                                    >Import Data Pegawai</button>
                                    <a
                                        href="{{ route('pegawai.export') }}"
                                        class="ml-1 btn btn-warning btn-sm"
                                    >Export Data Pegawai</a>
                                    <a
                                        href="{{ route('admin.pegawai.create') }}"
                                        class="ml-1 btn btn-primary btn-sm"
                                    >Tambah Data Pegawai</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div
        class="modal fade"
        id="import"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="exampleModalLabel"
                    >Import file data pegawai</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form
                    action="{{ route('pegawai.import') }}"
                    method="POST"
                    enctype="multipart/form-data"
                >
                    @csrf
                    @method('post')
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="file">File data pegawai</label>
                            <input
                                type="file"
                                name="file"
                                id="file"
                                placeholder="file.xlsx"
                                class="form-control-file"
                            >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button
                            type="submit"
                            class="btn btn-primary"
                        >Import</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush
