@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Pegawai',
    'link' => route('admin.pegawai.index')
    ],
    ['judul' => 'Tambah Data Pegawai']
    ]])
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Edit Data Jadwal Kegiatan Personil
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    action="{{ route('admin.pegawai.jadwal-kegiatan-personil.update', $jadwal_kegiatan_personil->id) }}"
                                    method="POST"
                                    accept-charset="utf-8"
                                    enctype="multipart/form-data"
                                >
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label>Nama Personil</label>
                                        <select
                                            data-live-search="true"
                                            name="personil"
                                            class="form-control selectpicker"
                                            data-title="Pilih Personil"
                                        >
                                            @foreach ($personils as $p)
                                                <option
                                                    {{ $p['id'] === ($jadwal_kegiatan_personil->pegawai_id ?? '') ? 'selected' : '' }}
                                                    value="{{ $p['id'] }}"
                                                >{{ $p['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Tanggal Mulai</label>
                                        <input
                                            type="date"
                                            name="tanggal_mulai"
                                            class="form-control"
                                            value="{{old('tanggal_mulai',$jadwal_kegiatan_personil->tanggal_mulai)}}"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label>Tanggal Selesai</label>
                                        <input
                                            type="date"
                                            name="tanggal_selesai"
                                            class="form-control"
                                            value="{{old('tanggal_selesai',$jadwal_kegiatan_personil->tanggal_selesai)}}"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label>Penugasan</label>
                                        <select
                                            data-live-search="true"
                                            name="penugasan"
                                            class="form-control"
                                            data-title="Pilih Penugasan"
                                            id="penugasan"
                                        >
                                            <option value="lainnya">Lainnya</option>
                                            @foreach ($penugasans as $p)
                                                <option
                                                    {{ $p['nama'] === ($jadwal_kegiatan_personil->penugasan ?? '') ? 'selected' : '' }}
                                                    value="{{ $p['nama'] }}"
                                                >{{ $p['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" id="div">
                                        <label>Keterangan</label>
                                        <input
                                            type="text"
                                            id="keterangan"
                                            name="keterangan"
                                            placeholder="Keterangan penugasan lainnya"
                                            value="{{old('keterangan', $jadwal_kegiatan_personil->penugasan)}}"
                                            class="form-control w-100 @error('keterangan') is-invalid @enderror">
                                        @error('keterangan')
                                        <span class="invalid-feedback"
                                              role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                        @enderror
                                    </div>
                                    <div class="form-group float-right">
                                        <button
                                            type="submit"
                                            class="btn btn-success btn-sm"
                                        >Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    <script>
        $("#penugasan").change(function() {
            if ($(this).val() == "lainnya") {
                $('#div').show();
                $('#keterangan').attr('required','');
                $('#keterangan').attr('data-error', 'Kolom ini harus diisi');
            } else {
                $('#div').hide();
                $('#keterangan').removeAttr('required');
                $('#keterangan').removeAttr('data-error');
            }
        });
        $("#penugasan").trigger("change");
    </script>
    {!! JsValidator::formRequest('App\Http\Requests\JadwalKegiatanPersonilRequest') !!}
@endpush
