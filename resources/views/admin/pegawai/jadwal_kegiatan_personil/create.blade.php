@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Pegawai',
    'link' => route('admin.pegawai.index')
    ],
    ['judul' => 'Tambah Data Pegawai']
    ]])
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Tambah Data Kegiatan Personil
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    action="{{ route('admin.pegawai.jadwal-kegiatan-personil.store') }}"
                                    method="POST"
                                    accept-charset="utf-8"
                                    enctype="multipart/form-data"
                                >
                                    @csrf
                                    <div class="form-group" @if(auth()->user()->level != '0' && auth()->user()->level != '1' && auth()->user()->level != '2') hidden @endif>
                                        <label>Nama Personil</label>
                                        <select
                                            data-live-search="true"
                                            name="personil"
                                            class="form-control selectpicker"
                                            data-title="Pilih Personil"
                                        >
                                                @forelse ($personils as $p)
                                                    <option
                                                        {{ $p['name'] === (auth()->user()->name ?? '') ? 'selected' : '' }}
                                                        value="{{ $p['id'] }}"
                                                    >{{ $p['name'] }}</option>
                                            @empty
                                                <option
                                                    value=""
                                                    disabled
                                                >Tidak ada data personil</option>
                                                @endforelse
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Tanggal Mulai</label>
                                        <input
                                            type="date"
                                            name="tanggal_mulai"
                                            class="form-control"
                                            value="{{old('tanggal_mulai')}}"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label>Tanggal Selesai</label>
                                        <input
                                            type="date"
                                            name="tanggal_selesai"
                                            class="form-control"
                                            value="{{old('tanggal_selesai')}}"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label>Penugasan</label>
                                        <select
                                            data-live-search="true"
                                            name="penugasan"
                                            class="form-control"
                                            data-title="Pilih Penugasan"
                                            id="penugasan"
                                        >
                                            <option value="lainnya">Lainnya</option>
                                        @foreach ($penugasans as $p)
                                                <option
{{--                                                    {{ $p['nama'] === ($jadwal_kegiatan_personil->penugasan ?? '') ? 'selected' : '' }}--}}
                                                    value="{{ $p['nama'] }}"
                                                >{{ $p['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" id="div">
                                        <label>Keterangan</label>
                                        <input
                                            type="text"
                                            id="keterangan"
                                            name="keterangan"
                                            placeholder="Keterangan penugasan lainnya"
{{--                                            value="{{old('keterangan')}}--}}
                                            class="form-control w-100 @error('keterangan') is-invalid @enderror">
                                        @error('keterangan')
                                        <span class="invalid-feedback"
                                              role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                        @enderror
                                    </div>

                                    <div class="form-group float-right">
                                        <button
                                            type="submit"
                                            class="btn btn-success btn-sm"
                                        >Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script

        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"

    ></script>
    <script>
        $("#penugasan").change(function() {
            if ($(this).val() == "lainnya") {
                $('#div').show();
                $('#keterangan').attr('required','');
                $('#keterangan').attr('data-error', 'Kolom ini harus diisi');
            } else {
                $('#div').hide();
                $('#keterangan').removeAttr('required');
                $('#keterangan').removeAttr('data-error');
            }
        });
        $("#penugasan").trigger("change");
    </script>
    {!! JsValidator::formRequest('App\Http\Requests\JadwalKegiatanPersonilRequest') !!}
@endpush
