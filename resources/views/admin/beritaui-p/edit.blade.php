@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [
        [
            'judul' => 'Berita', 
            'link' => 'berita.index',
        ],
        [
            'judul' => 'Simpan Data Berita',
        ]
]])
<div class="container">
@if (session()->has('success'))
<div class="alert alert-success" role="alert">
    {{ session()->get('success') }}
</div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="card">
        <div class="card-header bg-biru text-white">
            <h3>Ubah Tampilan Menu Berita di Beranda</h3>
        </div>
        <form action="{{route('beritaui.update', $beritaUI->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label for="jumlah_berita">Jumlah</label>
                            <input type="number" value="{{$beritaUI->jumlah_berita}}" max="{{$berita->count()}}" min="1" name="jumlah_berita" id="jumlah_berita" class="form-control @error('jumlah_berita') is-invalid @enderror">
                            @error('jumlah_berita')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="berita_1_id">Panel Berita 1</label>
                            <select name="berita_1_id" id="berita_1_id" class="custom-select custom-select-sm">
                                <option value="">Pilih Berita</option>
                                <option value="0" @if($beritaUI->berita_1_id == 0) selected @endif>Acak</option>
                                @foreach ($berita as $item)
                                    <option value="{{$item->id}}" @if($item->id == $beritaUI->berita_1_id) selected @endif>{{$item->judul}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="berita_2_id">Panel Berita 2</label>
                            <select name="berita_2_id" id="berita_2_id" class="custom-select custom-select-sm">
                                <option value="">Pilih Berita</option>
                                <option value="0" @if($beritaUI->berita_2_id == 0) selected @endif>Acak</option>
                                @foreach ($berita as $item)
                                    <option value="{{$item->id}}" @if($item->id == $beritaUI->berita_2_id) selected @endif>{{$item->judul}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="berita_3_id">Panel Berita 3</label>
                            <select name="berita_3_id" id="berita_3_id" class="custom-select custom-select-sm">
                                <option value="">Pilih Berita</option>
                                <option value="0" @if($beritaUI->berita_3_id == 0) selected @endif>Acak</option>
                                @foreach ($berita as $item)
                                    <option value="{{$item->id}}" @if($item->id == $beritaUI->berita_3_id) selected @endif>{{$item->judul}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="berita_4_id">Panel Berita 4</label>
                            <select name="berita_4_id" id="berita_4_id" class="custom-select custom-select-sm">
                                <option value="">Pilih Berita</option>
                                <option value="0" @if($beritaUI->berita_4_id == 0) selected @endif>Acak</option>
                                @foreach ($berita as $item)
                                    <option value="{{$item->id}}" @if($item->id == $beritaUI->berita_4_id) selected @endif>{{$item->judul}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection