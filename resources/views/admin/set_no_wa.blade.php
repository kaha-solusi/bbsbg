@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
	'judul' => [
        ['judul' => 'Data WhatsApp Admin',
		'link' => route('admin.wa')],
    ]
])
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex mt-3">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="float-left">
								Data WhatsApp Admin
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
							<form action="{{ route('admin.wa.store') }}" method="POST" accept-charset="utf-8">
								@csrf
				                <div class="row">
				                    <div class="col-md-12">
				                        <div class="form-row">
				                            <div class="form-group col-md-12">
				                                <label>Nomor WhatsApp <small class="text-success">*Harus diisi</small></label>
				                                <input type="text" name="nomor_wa" class="form-control @error('nomor_wa') is-invalid @enderror" value="{{ $data->nomor_wa }}" placeholder="Contoh: 628*********">
				                                @error('nomor_wa')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                            </div>
				                        </div>
				                    </div>
				                    <div class="col-md-12">
				                        <div class="float-right">
				                            <button type="submit" class="btn btn-success btn-sm">Update</button>
				                        </div>
				                    </div>
				                </div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection