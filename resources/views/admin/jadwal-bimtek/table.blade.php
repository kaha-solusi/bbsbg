@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
	'judul' => [
        ['judul' => 'Data Jadwal Bimtek',
		'link' => route('jadwal-bimtek.index')],
    ]
])
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
<div class="container">
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="float-left">
                                <a href="{{route('jadwal-bimtek.export')}}" class="btn btn-sm btn-warning">Export Jadwal Bimtek</a>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row float-right">
                                <div class="col-md-6">
                                    <form action="{{ route('jadwal-bimtek.index') }}" method="GET" accept-charset="utf-8">
                                        @csrf
                                        <div class="input-group">
                                            <input class="form-control" type="text" placeholder="Search" aria-describedby="btn-search" name="search" value="{{ $search }}"></input>
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="submit" id="btn-search">Search</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('jadwal-bimtek.create') }}" class="btn btn-primary" style="width: 100%;">Tambah Jadwal Bimtek</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead class="thead-custom">
                                    <tr>
                                        <th scope="col">Timestamp</th>
                                        <th scope="col">Nama Kegiatan</th>
                                        <th scope="col">Waktu Pelaksanaan</th>
                                        <th scope="col">Tempat Pelaksanaan</th>
                                        <th scope="col">Lab</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1 @endphp
                                    @forelse($data as $d)
                                    <tr class="tbody-custom">
                                        <td>{{ $d->waktu }}</td>
                                        <td>{{ $d->nama_kegiatan }}</td>
                                        <td>{{ $d->waktu_pelaksanaan }}</td>
                                        <td>{{ $d->tempat_pelaksanaan }}</td>
                                        <td>{{ $d->lab->nama }}</td>
                                        <td>
                                            @if($d->status == 'diproses')
                                                <a href="{{ route('jadwal-bimtek.status.dilaksanakan', $d->id) }}" class="btn btn-success btn-sm">Dilaksanakan</a>
                                                <a href="{{ route('jadwal-bimtek.status.tidak', $d->id) }}" class="btn btn-danger btn-sm">Tidak Dilaksanakan</a>
                                            @elseif($d->status == 'dilaksanakan')
                                                <a href="{{ route('jadwal-bimtek.status.sedang', $d->id) }}" class="btn btn-info btn-sm" title="Klik untuk mengubah status jadwal bimtek menjadi sedang dilaksanakan">Sedang Dilaksanakan</a>
                                            @elseif($d->status == 'tidak dilaksanakan')
                                                Tidak Dilaksanakan
                                            @elseif($d->status == 'sedang dilaksanakan')
                                                <a href="{{ route('jadwal-bimtek.status.selesai', $d->id) }}" class="btn btn-primary btn-sm" title="Klik untuk mengubah status jadwal bimtek menjadi selesai dilaksanakan">Selesai</a>
                                            @elseif($d->status == 'selesai')
                                                Selesai Dilaksanakan
                                            @endif
                                        </td>
                                        <td>
                                            <button type="button" onclick="sosialMedia({{ $d }})" class="btn btn-light btn-sm" data-toggle="modal" data-target="#sosialMediaModal">Detail</button>
                                            {{-- <a href="{{ route('kegiatan.edit', $d->id) }}" class="btn btn-warning btn-sm">Edit</a> --}}
					<form onsubmit="return confirm('Hapus data permanen ?');" action="{{route('jadwal-bimtek.destroy', $d->id)}}" method="post" class="d-inline">
                              @csrf
                              @method('delete')
                              <button type="submit" class="btn d-inline btn-sm btn-danger">Hapus</button>
                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr class="tbody-custom">
                                        <td colspan="7" class="text-center">Tidak ada data.</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            {{ $data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal --}}
<div class="modal fade" id="sosialMediaModal" tabindex="-1" aria-labelledby="mediaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mediaModalLabel">Detail Jadwal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-group">
                        <li class="list-group-item" id="waktu"></li>
                        <li class="list-group-item" id="nama"></li>
                        <li class="list-group-item" id="lab"></li>
                        <li class="list-group-item" id="deskripsi_kegiatan"></li>
                        <li class="list-group-item" id="waktu_pelaksanaan"></li>
                        <li class="list-group-item" id="tempat_pelaksanaan"></li>
                        <li class="list-group-item" id="biaya_kegiatan"></li>
                        <li class="list-group-item" id="status"></li>
                    </ul>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
{{-- End Modal --}}
<form action="" id="formDelete" method="POST">
    @csrf
    @method('DELETE')
</form>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
    function deleteData(id) {
        let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

        const formDelete = document.getElementById('formDelete')
        formDelete.action = '/admin/jadwal-bimtek/'+id;

        if (r == true) {
            formDelete.submit();
        } else {
            alert('Penghapusan dibatalkan.');
        }
    }

    function sosialMedia(data) {
        $('#waktu').html("<b>Timestamp:</b><br>"+data.waktu);
        $('#nama').html("<b>Nama Kegiatan:</b><br>"+data.nama_kegiatan);
        $('#lab').html("<b>Lab:</b><br>"+data.lab.nama);
        $('#deskripsi_kegiatan').html("<b>Deskripsi Kegiatan:</b><br>"+data.deskripsi_kegiatan);
        $('#waktu_pelaksanaan').html("<b>Waktu Pelaksanaan:</b><br>"+data.waktu_pelaksanaan);
        $('#tempat_pelaksanaan').html("<b>Tempat Pelaksanaan:</b><br>"+data.tempat_pelaksanaan);
        $('#biaya_kegiatan').html("<b>Biaya Kegiatan:</b><br>Rp. "+(data.biaya_kegiatan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")));
        $('#status').html("<b>Status:</b><br>"+data.status);
    }
</script>
@endpush