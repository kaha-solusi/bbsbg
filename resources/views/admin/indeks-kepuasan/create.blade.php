@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
	'judul' => [
        ['judul' => 'Data Kepuasan Pelanggan',
        'link' => route('indeks-kepuasan.index')],
        ['judul' => 'Tambah Data Kepuasan Pelanggan'],
]])
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="row h-100 mt-3">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            Tambah Data Kepuasan Pelanggan
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('indeks-kepuasan.store') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label>Tahun <small class="text-success">*Harus diisi</small></label>
                                                <select name="tahun" class="form-control @error('tahun') is-invalid @enderror">
                                                    @for($i = 2010;$i < (integer)date('Y');$i++)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                                @error('tahun')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Jumlah Responden <small class="text-success">*Harus diisi</small></label>
                                                <input type="integer" min="0" name="jumlah_responden" class="form-control @error('jumlah_responden') is-invalid @enderror" value="{{ old('jumlah_responden') }}">
                                                @error('jumlah_responden')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Nilai IKM ( 25 - 100 ) <small class="text-success">*Harus diisi</small></label>
                                                <input type="numeric" min="25" max="100" name="nilai_ikm" class="form-control @error('nilai_ikm') is-invalid @enderror" value="25.00" placeholder="Contoh: 46.13">
                                                @error('nilai_ikm')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success btn-sm" style="width: 100%;">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection