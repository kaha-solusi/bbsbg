@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
	'judul' => [
        ['judul' => 'Data Kepuasan Pelanggan',
        'link' => route('indeks-kepuasan.index')],
    ]
])
<div class="container">
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="float-left">
	                            <a href="{{route('indeks-kepuasan.export')}}" class="btn btn-sm btn-warning">Export Indeks Kepuasan</a>
                            </div>
                        </div>
                        <div class="col-md-8">
		                    <div class="row float-right">
		                        <div class="col-md-6">
                            		<form action="{{ route('indeks-kepuasan.index') }}" method="GET" accept-charset="utf-8">
                            			@csrf
		                            	<div class="input-group">
		                            		<input class="form-control" type="text" placeholder="Search" aria-describedby="btn-search" name="search" value="{{ $search }}"></input>
		                            		<div class="input-group-append">
		                            			<button class="btn btn-outline-secondary" type="submit" id="btn-search">Search</button>
		                            		</div>
		                            	</div>
                            		</form>
	                            </div>
		                        <div class="col-md-6">
	                                <a href="{{ route('indeks-kepuasan.create') }}" class="btn btn-primary" style="width: 100%;">Tambah Data Indeks Kepuasan</a>
	                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
							<table class="table">
								<thead class="thead-custom">
									<tr>
										<th scope="col">No</th>
										<th scope="col">Tahun</th>
										<th scope="col" colspan="2">Jumlah Responden</th>
										<th scope="col">NIlai IKM</th>
										<th scope="col">Indeks</th>
										<th scope="col">Nilai Mutu</th>
										<th scope="col">Predikat</th>
										<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
									@php $i = 1 @endphp
									@forelse($data as $indeks)
									<tr class="tbody-custom">
										<td>{{ $i++ }}</td>
										<td>{{ $indeks->tahun }}</td>
										<td>{{ $indeks->jumlah_responden }}</td>
										<td>Responden</td>
										<td>{{ $indeks->nilai_ikm }}</td>
										<td>{{ $indeks->indexs }}</td>
										<td>{{ $indeks->nilai_mutu }}</td>
										<td>{{ $indeks->predikat }}</td>
										<td>
											{{-- <button type="button" onclick="sosialMedia('{{ $indeks->link_ig }}', '{{ $indeks->link_facebook }}', '{{ $indeks->link_twitter }}', '{{ $indeks->link_likedin }}')" class="btn btn-light btn-sm" data-toggle="modal" data-target="#sosialMediaModal">Sosial Media</button> --}}
											<a href="{{ route('indeks-kepuasan.edit', $indeks->id) }}" class="btn btn-warning btn-sm">Edit</a>
										
                            <form onsubmit="return confirm('Hapus data permanen ?');" action="{{route('indeks-kepuasan.destroy', $indeks->id)}}" method="post" class="d-inline">
                              @csrf
                              @method('delete')
                              <button type="submit" class="btn d-inline btn-sm btn-danger">Hapus</button>
                            </form> 
</td>
									</tr>
									@empty
									<tr class="tbody-custom">
										<td colspan="9" class="text-center">Tidak ada data.</td>
									</tr>
									@endforelse
								</tbody>
							</table>
						</div>
                        <div class="col-md-12">
                        	{{ $data->links() }}
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form action="" id="formDelete" method="POST">
    @csrf
    @method('DELETE')
</form>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
	function deleteData(id) {
		let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

		const formDelete = document.getElementById('formDelete')
        formDelete.action = '/admin/indeks-kepuasan/'+id;

		if (r == true) {
			formDelete.submit();
		} else {
			alert('Penghapusan dibatalkan.');
		}
	}

	function sosialMedia(ig, fb, twitter, linkedin) {
		$('#ig').text("IG: "+ig);
		$('#facebook').text("FB: "+fb);
		$('#twitter').text("Twitter: "+twitter);
		$('#linkedin').text("Linkedin: "+linkedin);
	}
</script>
@endpush