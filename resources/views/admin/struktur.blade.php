@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [[
        'judul' => 'Data Struktur Organisasi',
        'link' => route('struktur-organisasi.index'),
        ]]
])
<div class="container">
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="justify-self-left">
                            Data Struktur Organisasi
                        </div>
                        <div class="justify-self-right">
                            <button data-toggle="modal" data-target="#import" class="btn btn-sm btn-success">Import Struktur Organisasi</button>
                            <a href="{{route('struktur-organisasi.export')}}" class="btn btn-sm btn-warning">Export Struktur Organisasi</a>
                            <a href="{{route('struktur-organisasi.create')}}" class="btn btn-sm btn-primary">Tambah Struktur Organisasi</a>
                            <button data-toggle="modal" data-target="#gambar" class="btn btn-sm btn-info">File Gambar</button>
                            <button data-toggle="modal" data-target="#pdf" class="btn btn-sm btn-secondary">File Gambar</button>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div class="float-left">
                                Pilih untuk tampilan depan:
                                <a href="{{ route('admin.struktur-organisasi.homepage', 1) }}" class="btn btn-sm @if($organisasi->gambar_status == 1 && $organisasi->pdf_status == 0) btn-dark @else btn-light @endif">Gambar</a>
                                <a href="{{ route('admin.struktur-organisasi.homepage', 2) }}" class="btn btn-sm @if($organisasi->gambar_status == 0 && $organisasi->pdf_status == 1) btn-dark @else btn-light @endif">PDF</a>
                                <a href="{{ route('admin.struktur-organisasi.homepage', 3) }}" class="btn btn-sm @if($organisasi->gambar_status == 0 && $organisasi->pdf_status == 0) btn-dark @else btn-light @endif">Teks</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <table class="table">
                                <thead class="thead-custom">
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Foto</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">NIP</th>
                                        <th scope="col">Jabatan</th>
                                        <th scope="col">E-mail</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1 @endphp
                                    @forelse($data as $struktur)
                                    <tr class="tbody-custom">
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            <img src="{{ ($struktur->foto == null) ? asset('/assets/default/defaul_user.png') : asset($struktur->foto) }}" alt="" width="75px" height="75px">
                                        </td>
                                        <td>{{ $struktur->nama }}</td>
                                        <td>{{ $struktur->nip }}</td>
                                        <td>{{ $struktur->jabatan }}</td>
                                        <td>{{ $struktur->email }}</td>
                                        <td>
                                            <button type="button" onclick="sosialMedia('{{ $struktur->keterangan }}')" class="btn btn-light btn-sm" data-toggle="modal" data-target="#sosialMediaModal">Keterangan</button>
                                            <a href="{{ route('struktur-organisasi.edit', $struktur->id) }}" class="btn btn-warning btn-sm">Edit</a>
						<form onsubmit="return confirm('Hapus data permanen ?');" action="{{route('struktur-organisasi.destroy', $struktur->id)}}" method="post" class="d-inline">
                              @csrf
                              @method('delete')
                              <button type="submit" class="btn d-inline btn-sm btn-outline-danger">Hapus</button>
                            </form> 
                                        </td>
                                    </tr>
                                    @empty
                                    <tr class="tbody-custom">
                                        <td colspan="5" class="text-center">Tidak ada data.</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            {{ $data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="" id="formDelete" method="POST">
    @csrf
    @method('DELETE')
</form>
<!-- Modal -->
<div class="modal fade" id="import" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Import file data struktur organisasi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{route('struktur-organisasi.import')}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('post')
        <div class="modal-body">
            <div class="form-group">
                <label for="file">File data struktur organisasi</label><small class="text-success">*Harus diisi</small>
                <input type="file" name="file" id="file" placeholder="file.xlsx" class="form-control-file">
                <small id="detail" class="form-text text-danger">Tipe file excel : .xlsx </small>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Import</button>
        </div>
        </form>
        </div>
        </div>
    </div>
{{-- Modal --}}
<div class="modal fade" id="sosialMediaModal" tabindex="-1" aria-labelledby="mediaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mediaModalLabel">Keterangan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="keterangan"></p>
      </div>
    </div>
  </div>
</div>
{{-- End Modal --}}
{{-- Modal Gambar --}}
<div class="modal fade" id="gambar" tabindex="-1" aria-labelledby="mediaModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="mediaModalLabel">File Gambar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <img src="{{ asset($organisasi->gambar) }}" alt="" style="width: 100%;height: auto;">
                    </div>
                    <div class="col-md-12 mt-3">
                        <form action="{{ route('admin.struktur-organisasi.gambar') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>File Gambar</label>
                                    <input type="file" name="gambar" class="form-control-file @error('gambar') is-invalid @enderror" value="{{ old('gambar') }}">
                                    @error('gambar')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <button type="submit" class="btn btn-primary float-right">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
{{-- End Modal --}}
{{-- Modal PDF --}}
<div class="modal fade" id="pdf" tabindex="-1" aria-labelledby="mediaModalLabelPdf" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="mediaModalLabelPdf">File PDF</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <embed src="{{ asset($organisasi->pdf) }}" style="width: 100%;height: 350px;">
                        {{-- <img src="{{ asset($organisasi->gambar) }}" alt="" style="width: 100%;height: auto;"> --}}
                    </div>
                    <div class="col-md-12">
                        <form action="{{ route('admin.struktur-organisasi.pdf') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>File PDF</label>
                                    <input type="file" name="pdf" class="form-control-file @error('pdf') is-invalid @enderror" value="{{ old('pdf') }}">
                                    @error('pdf')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <button type="submit" class="btn btn-primary float-right">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
{{-- End Modal --}}
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
    function deleteData(id) {
        let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

        const formDelete = document.getElementById('formDelete')
        formDelete.action = '/admin/struktur-organisasi/'+id;

        if (r == true) {
            formDelete.submit();
        } else {
            alert('Penghapusan dibatalkan.');
        }
    }

    function sosialMedia(data) {
        $('#keterangan').text(data);
    }
</script>
@endpush