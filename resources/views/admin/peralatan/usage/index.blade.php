@extends('layouts.adminlte')
@section('main')
    <div class="container">
        <div class="row h-100">
            <div class="card w-100">
                <div class="card-header">
                    <h3 class="card-title">{{ $peralatan->nama }}</h3>
                </div>
                <div class="card-body">
                    @foreach ($usages as $usage)
                        @if($usage->kategori == 'Pemakaian')
                        <div class="post">
                            <div class="post">
                                <h3>{{$usage->pelaksana}}</h3>
                                <a class="description mb-3 mt-0">{{$usage->created_at}}</a>
                                <!-- /.user-block -->
                                <p class="mb-0 mt-1">Tanggal Pemakaian : {{ $usage->tanggal_pelaksanaan }}</p>
                                <p class="mb-1">Keterangan : {{ $usage->keterangan }}</p>
                            </div>
                        </div>
                        @elseif($usage->kategori == 'Perbaikan')
                            <div class="post">
                                <div class="post">
                                    <h3>{{$usage->pelaksana}}</h3>
                                    <a class="description mb-3 mt-0">{{$usage->created_at}}</a>
                                    <!-- /.user-block -->
                                    <p class="mb-0 mt-1">Tanggal Perbaikan : {{ $usage->tanggal_pelaksanaan }}</p>
                                    <p class="mb-1">Keterangan : {{ $usage->keterangan }}</p>
                                </div>
                            </div>
                        @else($usage->kategori == 'Pemeliharaan')
                                <div class="post">
                                    <div class="post">
                                        <h3>{{$usage->pelaksana}}</h3>
                                        <a class="description mb-3 mt-0">{{$usage->created_at}}</a>
                                        <!-- /.user-block -->
                                        <p class="mb-0 mt-1">Tanggal Perbaikan : {{ $usage->tanggal_pelaksanaan }}</p>
                                        <p class="mb-1">Keterangan : {{ $usage->keterangan }}</p>
                                    </div>
                                </div>
                        @endif
                    @endforeach
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">

    </script>
@endpush
