@extends('layouts.adminlte')
@section('main')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Kartu Alat</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="container">
        @include('admin.utils.alert')
        <div class="h-100">
            <div class="card">
                <div class="card-body">
                    <form
                        action="{{ route('admin.usage.store') }}"
                        method="POST"
                        accept-charset="utf-8"
                        enctype="multipart/form-data"
                        id="createForm"
                        novalidate="novalidate"
                    >
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="tanggal_pengujian">Alat Pengujian</label>
                                @include('admin.utils.barcode_scanner')
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Pelaksanaan</label>
                                    <input
                                        type="text"
                                        class="form-control datepicker"
                                        name="tanggal"
                                        id="tanggal"
                                        value="{{ \Carbon\Carbon::now()->format('d/m/Y') }}"
                                    />
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Kategori</label>
                                    <select
                                        name="kategori"
                                        class="form-control selectpicker"
                                        data-title="Pilih Kategori"
                                    >
                                        <option value="Pemakaian">Pemakaian</option>
                                        <option value="Pemeliharaan">Pemeliharaan</option>
                                        <option value="Perbaikan">Perbaikan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Keterangan</label>
                                <textarea
                                    type="text"
                                    name="keterangan"
                                    class="form-control @error('keterangan') is-invalid @enderror"
                                    value="{{ old('keterangan') }}"
                                ></textarea>
                                @error('keterangan')
                                    <span
                                        class="invalid-feedback"
                                        role="alert"
                                    >
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div
                                class="col-md-12 mt-3"
                                id="btnAdd"
                            >
                                <button
                                    type="submit"
                                    class="btn btn-success float-right"
                                    style="width: 15%;"
                                >Simpan</button>
                                <a
                                    href="{{ route('admin.peralatan.index') }}"
                                    class="btn btn-danger float-right mr-1"
                                >
                                    Batal
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="{{ asset('adminlte/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}">
    </script>
    <script
        src="//unpkg.com/alpinejs"
        defer
    ></script>

@endpush
