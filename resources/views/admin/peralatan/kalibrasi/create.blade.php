@extends('layouts.adminlte')
@section('main')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Kalibrasi</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">User Profile</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="container">
        <div class="row h-100">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ $peralatan->nama }}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form
                                action="{{ route('admin.calibration.store', $peralatan->id) }}"
                                method="POST"
                                accept-charset="utf-8"
                                enctype="multipart/form-data"
                                id="createForm"
                                novalidate="novalidate"
                            >
                                @csrf
                                <div class="row">
                                     <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Tanggal Terakhir
                                                        Kalibrasi
                                                        <div class="custom-tooltip">
                                                            <i
                                                               class="fas fa-info-circle"></i>
                                                            <span
                                                                  class="custom-tooltiptext">Isi
                                                                dengan tanggal
                                                                kalibrasi terakhir,
                                                                jika alat
                                                                ukur.</span>
                                                        </div>
                                                    </label>
                                                    <input type="date"
                                                           name="tanggal_terakhir_kalibrasi"
                                                           class="form-control @error('tanggal_terakhir_kalibrasi') is-invalid @enderror"
                                                           value="{{ old('tanggal_terakhir_kalibrasi') }}">
                                                    @error('tanggal_terakhir_kalibrasi')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Tanggal Kalibrasi Ulang
                                                        <div class="custom-tooltip">
                                                            <i
                                                               class="fas fa-info-circle"></i>
                                                            <span
                                                                  class="custom-tooltiptext">Isi
                                                                dengan tanggal
                                                                kalibrasi
                                                                berikutnya, jika
                                                                alat ukur.</span>
                                                        </div>
                                                    </label>
                                                    <input type="date"
                                                           name="tanggal_kalibrasi_ulang"
                                                           class="form-control @error('tanggal_kalibrasi_ulang') is-invalid @enderror"
                                                           value="{{ old('tanggal_kalibrasi_ulangg') }}">
                                                    @error('tanggal_kalibrasi_ulang')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Institusi Kalibrasi
                                                        <div class="custom-tooltip">
                                                            <i
                                                               class="fas fa-info-circle"></i>
                                                            <span
                                                                  class="custom-tooltiptext">Isi
                                                                nama laboratorium
                                                                kalibrasi eksternal
                                                                jika dikalibrasi
                                                                eksternal atau
                                                                internal jika
                                                                dikalibrasi
                                                                internal.</span>
                                                        </div>
                                                    </label>
                                                    <input type="text"
                                                           name="lembaga_pengujian_kalibrasi"
                                                           class="form-control @error('lembaga_pengujian_kalibrasi') is-invalid @enderror"
                                                           value="{{ old('lembaga_pengujian_kalibrasi') }}">
                                                    @error('lembaga_pengujian_kalibrasi')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                     <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Kondisi <small
                                                           class="text-danger">*Harus
                                                        diisi</small>
                                                    <div class="custom-tooltip">
                                                        <i
                                                           class="fas fa-info-circle"></i>
                                                        <span
                                                              class="custom-tooltiptext">Isi
                                                            dengan kondisi alat:
                                                            <br>a. Baik, jika
                                                            kondisi peralatan
                                                            baik.
                                                            <br>b. Rusak ringan,
                                                            jika kondisi
                                                            peralatan rusak
                                                            tetapi masih bisa
                                                            diperbaiki sendiri.
                                                            <br>c. Rusak sedang,
                                                            jika kondisi
                                                            peralatan rusak dan
                                                            membutuhkan
                                                            perbaikan oleh pihak
                                                            eksternal
                                                            <br>d. Rusak berat,
                                                            jika kondisi
                                                            peralatan rusak dan
                                                            membutuhkan
                                                            perbaikan oleh pihak
                                                            eksternal serta
                                                            membutuhkan
                                                            penggantian suku
                                                            cadang.</span>
                                                    </div>
                                                </label>
                                                <select name="kondisi"
                                                        class="form-control @error('kondisi') is-invalid @enderror">
                                                    <option value="baik">Baik
                                                    </option>
                                                    <option
                                                            value="rusak_ringan">
                                                        Rusak Ringan</option>
                                                    <option
                                                            value="rusak_sedang">
                                                        Rusak Sedang</option>
                                                    <option value="rusak_berat">
                                                        Rusak Berat</option>
                                                </select>
                                                @error('kondisi')
                                                    <span class="invalid-feedback"
                                                          role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                     <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Keterangan
                                                    <div class="custom-tooltip">
                                                        <i
                                                           class="fas fa-info-circle"></i>
                                                        <span
                                                              class="custom-tooltiptext">Isi
                                                            dengan keterangan kondisi peralatan.</span>
                                                    </div>
                                                </label>
                                                <input type="text"
                                                       name="keterangan"
                                                       class="form-control @error('keterangan') is-invalid @enderror"
                                                       value="{{ old('keterangan') }}">
                                                @error('keterangan')
                                                    <span class="invalid-feedback"
                                                          role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Sertifikat
                                                    <div class="custom-tooltip">
                                                        <i
                                                            class="fas fa-info-circle"></i>
                                                        <span
                                                            class="custom-tooltiptext">Isi
                                                            dengan file sertifikat peralatan. File bisa bertipe
                                                            dokumen, pdf, atau jpg.</span>
                                                    </div>
                                                </label>
                                                <div class="custom-file">
                                                <input type="file"
                                                       name="sertifikat"
                                                       class="custom-file-input form-control @error('sertifikat') is-invalid @enderror"
                                                       id="sertifikat">
                                                @error('sertifikat')
                                                <span class="invalid-feedback"
                                                      role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <label class="custom-file-label" for="sertifikat">Unggah Sertifikat</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-row">--}}
{{--                                            <div class="form-group col-md-12">--}}
{{--                                                <label>Sertifikat</label>--}}
{{--                                                    <div class="custom-file">--}}
{{--                                                        <input type="file" class="custom-file-input" id="sertifikat">--}}
{{--                                                        <label class="custom-file-label" for="sertifikat">Unggah Sertifikat</label>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                    <div
                                        class="col-md-12 mt-3"
                                        id="btnAdd"
                                    >
                                        <button
                                            type="submit"
                                            class="btn btn-success float-right"
                                            style="width: 15%;"
                                        >Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="{{ asset('adminlte/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}">
    </script>
    <script
        src="//unpkg.com/alpinejs"
        defer
    ></script>
@endpush
