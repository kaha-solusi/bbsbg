@extends('layouts.adminlte')
@section('main')
    <div class="container">
        <div class="row h-100">
            <div class="card w-100">
                <div class="card-header">
                    <h3 class="card-title">{{ $peralatan->nama }}</h3>
                </div>
                <div class="card-body">
                    @foreach ($calibrations as $calibration)
                    <div class="post">
                        <h3>{{$calibration->responsible_person}}</h3>
                        <a class="description">{{$calibration->created_at}}</a>
                        <!-- /.user-block -->
                        <p class="mb-1">Institusi Kalibrasi : {{ $calibration->examiner }}</p>
                        <p class="mb-1">Kondisi : {{ $calibration->examiner }}</p>
                        <p class="mb-1">Keterangan : {{ $calibration->description }}</p>
                        <?php $conv_last_date = strtotime($calibration->last_calibration)?>
                        <p class="mb-1">Kalibrasi Terakhir : {{ date("d/m/Y", $conv_last_date) }}</p>
                        <?php $conv_next_date = strtotime($calibration->next_calibration)?>
                        <p class="mb-1">Kalibrasi Selanjutnya : {{ date("d/m/Y", $conv_next_date) }}</p>

                        <p>
                            <a
                                href="{{ route('file.download', ['path' => 'uploads\kalibrasi/'. $calibration->certificate]) }}">
                                <i class="fas fa-link mr-1"></i> Download Sertifikat</a>
                        </p>
                    </div>
                    @endforeach
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">

    </script>
@endpush
