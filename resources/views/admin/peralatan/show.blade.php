@extends('layouts.adminlte')
@section('main')
    <div class="container">
        <div class=" h-100">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ $peralatan->nama }}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-13 col-md-13 col-lg-8 order-2 order-md-1">
                            <div class="row">
                                <div class="col-15 col-sm-3">
                                    <div
                                        class="info-box bg-light btn"
                                        id="pemakaian_btn"
                                    >
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Jumlah</span>
                                            <span class="info-box-text text-center text-muted">Pemakaian</span>
                                            <span
                                                class="info-box-number text-center text-muted mb-0">{{ $pemakaians->count() }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-15 col-sm-3">
                                    <div
                                        class="info-box bg-light btn"
                                        id="kalibrasi_btn"
                                    >
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Jumlah</span>
                                            <span class="info-box-text text-center text-muted">Kalibrasi</span>
                                            <span
                                                class="info-box-number text-center text-muted mb-0">{{ $calibrations->count() }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-15 col-sm-3">
                                    <div
                                        class="info-box bg-light btn"
                                        id="pemeliharaan_btn"
                                    >
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Jumlah</span>
                                            <span class="info-box-text text-center text-muted">Pemeliharaan</span>
                                            <span
                                                class="info-box-number text-center text-muted mb-0">{{ $pemeliharaans->count() }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-15 col-sm-3">
                                    <div
                                        class="info-box bg-light btn"
                                        id="perbaikan_btn"
                                    >
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Jumlah</span>
                                            <span class="info-box-text text-center text-muted">Perbaikan</span>
                                            <span
                                                class="info-box-number text-center text-muted mb-0">{{ $perbaikans->count() }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div
                                    class="col-12"
                                    id="kalibrasi"
                                >
                                    <h4>Riwayat Kalibrasi</h4>
                                    <div class="card-body">
                                        @foreach ($calibrations as $calibration)
                                            <div class="post">
                                                <h3>{{ $calibration->responsible_person }}</h3>
                                                <a class="description">{{ $calibration->created_at }}</a>
                                                <p class="mb-1">Institusi Kalibrasi :
                                                    {{ $calibration->examiner }}</p>
                                                    <p class="mb-1">Kondisi : {{ $calibration->condition }}</p>
                                                    <p class="mb-1">Keterangan : {{ $calibration->description }}</p>
                                                    <?php $conv_last_date = strtotime($calibration->last_calibration); ?>
                                                    <p class="mb-1">Kalibrasi Terakhir :
                                                        {{ date('d/m/Y', $conv_last_date) }}</p>
                                                    <?php $conv_next_date = strtotime($calibration->next_calibration); ?>
                                                    <p class="mb-1">Kalibrasi Selanjutnya :
                                                        {{ date('d/m/Y', $conv_next_date) }}</p>

                                                    <p>
                                                        <a
                                                            href="{{ route('file.download', ['path' => 'uploads\kalibrasi/' . $calibration->certificate]) }}">
                                                            <i class="fas fa-link mr-1"></i> Download Sertifikat</a>
                                                    </p>
                                            </div>
                                        @endforeach
                                        <div class="text-center">
                                            <a
                                                href="{{ route('admin.calibration.index', $peralatan->id) }}"
                                                class="btn btn-sm btn-default"
                                            >Lihat semua</a>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="col-12"
                                    id="pemakaian"
                                >
                                    <h4>Riwayat Pemakaian</h4>
                                    <div class="card-body">
                                        @foreach ($pemakaians as $pemakaian)
                                            <div class="post">
                                                <h3>{{ $pemakaian->pelaksana }}</h3>
                                                <a class="description mb-3 mt-0">{{ $pemakaian->created_at }}</a>
                                                <!-- /.user-block -->
                                                <p class="mb-0 mt-1">Tanggal Pemakaian :
                                                    {{ $pemakaian->tanggal_pelaksanaan }}</p>
                                                <p class="mb-1">Keterangan : {{ $pemakaian->keterangan }}</p>
                                            </div>
                                        @endforeach
                                        <div class="text-center">
                                            <a
                                                href="{{ route('admin.usage.index', [$peralatan->id, 'usage' => 'Pemakaian']) }}"
                                                class="btn btn-sm btn-default"
                                            >Lihat semua</a>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="col-12"
                                    id="pemeliharaan"
                                >
                                    <h4>Riwayat Pemeliharaan</h4>
                                    <div class="card-body">
                                        @foreach ($pemeliharaans as $pemeliharaan)
                                            <div class="post">
                                                <h3>{{ $pemeliharaan->pelaksana }}</h3>
                                                <a class="description mb-3 mt-0">{{ $pemeliharaan->created_at }}</a>
                                                <!-- /.user-block -->
                                                <p class="mb-0 mt-1">Tanggal Pemeliharaan :
                                                    {{ $pemeliharaan->tanggal_pelaksanaan }}</p>
                                                <p class="mb-1">Keterangan : {{ $pemeliharaan->keterangan }}
                                                </p>
                                            </div>
                                        @endforeach
                                        <div class="text-center">
                                            <a
                                                href="{{ route('admin.usage.index', [$peralatan->id, 'usage' => 'Pemeliharaan']) }}"
                                                class="btn btn-sm btn-default"
                                            >Lihat semua</a>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="col-12"
                                    id="perbaikan"
                                >
                                    <h4>Riwayat Perbaikan</h4>
                                    <div class="card-body">
                                        @foreach ($perbaikans as $perbaikan)
                                            <div class="post">
                                                <h3>{{ $perbaikan->pelaksana }}</h3>
                                                <a class="description mb-3 mt-0">{{ $perbaikan->created_at }}</a>
                                                <!-- /.user-block -->
                                                <p class="mb-0 mt-1">Tanggal Perbaikan :
                                                    {{ $perbaikan->tanggal_pelaksanaan }}</p>
                                                <p class="mb-1">Keterangan : {{ $perbaikan->keterangan }}</p>
                                            </div>
                                        @endforeach
                                        <div class="text-center">
                                            <a
                                                href="{{ route('admin.usage.index', [$peralatan->id, 'usage' => 'Perbaikan']) }}"
                                                class="btn btn-sm btn-default"
                                            >Lihat semua</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                            <img
                                height="450"
                                width="auto"
                                src="{{ $peralatan->foto_alat ?? 'https://via.placeholder.com/500x450?text=' . $peralatan->nama . '' }}"
                                class="product-image mb-2"
                                alt="Product Image"
                            >
                            <h3 class="text-primary">{{ $peralatan->nama }}</h3>
                            <p class="text-muted">{{ $peralatan->spesifikasi }}</p>
                            <br>
                            <div class="text-muted">
                                <p class="text-sm">Merek
                                    <b class="d-block">{{ $peralatan->merek }}</b>
                                </p>
                                <p class="text-sm">Tipe
                                    <b class="d-block">{{ $peralatan->type }}</b>
                                </p>
                                <p class="text-sm">Nomor/Kode BMN
                                    <b class="d-block">{{ $peralatan->no_bmn }}</b>
                                </p>
                                <p class="text-sm">Nomor Seri
                                    <b class="d-block">{{ $peralatan->seri }}</b>
                                </p>
                                <p class="text-sm">Lokasi Alat
                                    <b class="d-block">{{ $peralatan->lab->nama }}</b>
                                </p>
                            </div>

                            <div class="mt-5 mb-1">
                                <a
                                    href="{{ route('admin.calibration.create', $peralatan->id) }}"
                                    class="btn btn-sm btn-primary"
                                >Lakukan Kalibrasi</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">

    </script>
    <script>
        $("#pemakaian_btn").on('click', function() {
            if ($(this).on('click')) {
                $('#pemakaian').show();
                $('#kalibrasi').hide();
                $('#pemeliharaan').hide();
                $('#perbaikan').hide();
            } else {
                $('#pemakaian').hide();
            }
        });
        $("#kalibrasi_btn").on('click', function() {
            if ($(this).on('click')) {
                $('#kalibrasi').show();
                $('#pemakaian').hide();
                $('#pemeliharaan').hide();
                $('#perbaikan').hide();
            }
        });
        $("#pemeliharaan_btn").on('click', function() {
            if ($(this).on('click')) {
                $('#pemeliharaan').show();
                $('#pemakaian').hide();
                $('#kalibrasi').hide();
                $('#perbaikan').hide();

            }
        });
        $("#perbaikan_btn").on('click', function() {
            if ($(this).on('click')) {
                $('#perbaikan').show();
                $('#pemakaian').hide();
                $('#pemeliharaan').hide();
                $('#kalibrasi').hide();
            }
        });
        $("#pemakaian_btn").trigger("click");
    </script>
@endpush
