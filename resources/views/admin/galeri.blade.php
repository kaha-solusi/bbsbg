@extends('layouts.adminlte')
@section('main')@include('admin.parts.breadcrumbs', [
	'judul' => [
        ['judul' => 'Data Dokumentasi',
		'link' => route('galeri.index')],
    ]
])
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
<div class="container">
    <div class="row h-100">
        <div class="col-md-12 mb-3">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-left">
                            	Data Dokumentasi Kegiatan dan Pengujian
                            </div>
                            <div class="float-right">
                                <a href="{{ route('galeri.create') }}" class="btn btn-primary btn-sm">Tambah Data Foto</a>
                                <a href="{{ route('galeri.createVideo') }}" class="ml-1 btn btn-primary btn-sm">Tambah Data Video</a>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		@forelse($data as $galeri)
        <div class="col-md-4" style="margin-bottom: 30px;">
            <div class="card card-block d-flex" style="width: auto; height: 100%;">
				@if (!empty($galeri->video))
				@php
					parse_str(explode("?", $galeri->video)[1], $vars);
					// var_dump($vars['v']);
					$video_id = $vars['v'];
				@endphp
					<img src="http://img.youtube.com/vi/{{$video_id}}/0.jpg" alt="" srcset="" style="width: 100%;
					height: 250px;
					object-fit: cover;
					background: no-repeat center center scroll;
					background-image: cover;">
				@else
					<img src="{{ asset($galeri->foto) }}" alt="" class="card-img-top" style="width: 100%;
					height: 250px;
					object-fit: cover;
					background: no-repeat center center scroll;
					background-image: cover;">
				@endif
                <div class="card-body h-100">
					<div class="col">
						<h3 class="card-title">{{ $galeri->judul }}</h3>
					</div>
				</div>
				<div class="card-footer border-0 bg-white">
					<div class="row justify-content-between">
						<div class="col-md-6">
							<a href="{{ route('galeri.edit', $galeri->id) }}" class="btn btn-warning btn-sm" style="width: 100%;">Edit</a>
						</div>
						<div class="col-md-6">
                            <form onsubmit="return confirm('Hapus data permanen ?');" action="{{route('galeri.destroy', $galeri->id)}}" method="post" class="d-inline">
                              @csrf
                              @method('delete')
                              <button type="submit" class="btn d-inline btn-sm btn-danger" style="width: 100%;">Hapus</button>
                            </form> 
						</div>
					</div>
				</div>
			</div>
		</div>		@empty
		<div class="col-md-12">
            <p class="text-center">Data tidak ada</p>
		</div>
		@endforelse
        <div class="col-md-12">
        	{{ $data->links() }}
        </div>
	</div>
</div>
<form action="" id="formDelete" method="POST">
    @csrf
    @method('DELETE')
</form>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
	function deleteData(id) {
		let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

		const formDelete = document.getElementById('formDelete')
        formDelete.action = '/admin/galeri/'+id;

		if (r == true) {
			formDelete.submit();
		} else {
			alert('Penghapusan dibatalkan.');
		}
	}
</script>
@endpush