@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
	'judul' => [[
		'judul' => 'Data Pejabat',
		'link' => route('pejabat.index')
	]]
])
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
<div class="container">
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="float-left">
                            	Data Master \ Pejabat
                            </div>
                        </div>
                        <div class="col-md-8">
		                    <div class="row float-right">
		                        <div class="col-md-6">
                            		<form action="{{ route('pejabat.index') }}" method="GET" accept-charset="utf-8">
                            			@csrf
		                            	<div class="input-group">
		                            		<input class="form-control" type="text" placeholder="Search" aria-describedby="btn-search" name="search" value="{{ $search }}"></input>
		                            		<div class="input-group-append">
		                            			<button class="btn btn-outline-secondary" type="submit" id="btn-search">Search</button>
		                            		</div>
		                            	</div>
                            		</form>
	                            </div>
		                        <div class="col-md-6">
	                                <a href="{{ route('pejabat.create') }}" class="btn btn-primary" style="width: 100%;">Tambah Data Pejabat</a>
	                            </div>
	                        </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
							<table class="table">
								<thead class="thead-custom">
									<tr>
										<th scope="col">No</th>
										<th scope="col">Nama</th>
										<th scope="col">Jabatan</th>
										<th scope="col">Lab</th>
										<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
									@php $i = 1 @endphp
									@forelse($data as $pejabat)
									<tr class="tbody-custom">
										<td>{{ $i++ }}</td>
										<td>{{ $pejabat->nama }}</td>
										<td>{{ $pejabat->job }}</td>
										<td>{{ $pejabat->lab->nama }}</td>
										<td>
											<a href="{{ route('pejabat.edit', $pejabat->id) }}" class="btn btn-warning btn-sm">Edit</a>
											<button type="button" onclick="deleteData({{ $pejabat->id }})" class="btn btn-danger btn-sm">Hapus</button>
										</td>
									</tr>
									@empty
									<tr class="tbody-custom">
										<td colspan="5" class="text-center">Tidak ada data.</td>
									</tr>
									@endforelse
								</tbody>
							</table>
						</div>
                        <div class="col-md-12">
                        	{{ $data->links() }}
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<form action="" id="formDelete" method="POST">
    @csrf
    @method('DELETE')
</form>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
	function deleteData(id) {
		let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

		const formDelete = document.getElementById('formDelete')
        formDelete.action = '/admin/pejabat/'+id;

		if (r == true) {
			formDelete.submit();
		} else {
			alert('Penghapusan dibatalkan.');
		}
	}
</script>
@endpush