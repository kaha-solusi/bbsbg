@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', ['judul' => [
    ['judul' => 'Data Maskot',
    'link' => route('maskot.index')],
    ['judul' => 'Ubah Data Maskot']
]])
<div class="container">
    @if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
    @endif
    @if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            Ubah Data Maskot
        </div>
        <form action="{{route('maskot.update', $maskot->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <div class="card-body">
                <div class="form-group">
                    <label for="foto">Foto</label><small class="text-success"> *Tidak Harus diisi</small>
                    <input type="file" name="foto" id="foto" class="form-control-file @error('foto') is-invalid @enderror">
                    <small id="detail" class="form-text text-danger">Tipe file image: JPG, JPEG, PNG; Max berukuran 2mb</small>
                    @error('foto')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nama">Nama Maskot</label><small class="text-success">*Harus diisi</small>
                    <input type="text" value="{{$maskot->nama}}" name="nama" id="nama" class="form-control @error('nama') is-invalid @enderror">
                    @error('nama')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="deskripsi">Deskripsi Maskot</label><small class="text-success">*Harus diisi</small>
                    <input type="text" value="{{$maskot->deskripsi}}" name="deskripsi" id="deskripsi" class="form-control @error('deskripsi') is-invalid @enderror">
                    @error('deskripsi')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection