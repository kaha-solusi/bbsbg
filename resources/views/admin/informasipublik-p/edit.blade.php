@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [
        [
            'judul' => 'Informasi Publik', 
            'link' => route('informasipublik.index'),
        ],
        [
            'judul' => 'Ubah Data Informasi Publik',
        ]
]])
<div class="container">
    @if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
    @endif
    @if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            Ubah Data Informasi Publik
        </div>
        <form action="{{route('informasipublik.update', $informasipublik->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <div class="card-body">
                <!--<div class="form-group">-->
                <!--    <label for="foto">Foto</label>-->
                <!--    <input type="file" name="foto" id="foto" class="form-control-file @error('foto') is-invalid @enderror">-->
                <!--    @error('foto')-->
                <!--    <span class="invalid-feedback" role="alert">-->
                <!--        <strong>{{ $message }}</strong>-->
                <!--    </span>-->
                <!--    @enderror-->
                <!--</div>-->
                <div class="form-group">
                    <label for="nama">Nama Informasi</label>
                    <input type="text" value="{{$informasipublik->nama}}" name="nama" id="nama" class="form-control @error('nama') is-invalid @enderror">
                    @error('nama')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="link">Link Informasi Publik</label>
                    <input type="url" placeholder="http(s)://" value="{{$informasipublik->link}}" name="link" id="link" class="form-control @error('link') is-invalid @enderror">
                    @error('link')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection