@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    ['judul' => 'Data Lab',
    'link' => route('lab.index')],
    ]
    ])
    @if (session()->has('success'))
        <div
            class="alert alert-success"
            role="alert"
        >
            {{ session()->get('success') }}
        </div>
    @endif
    @if (session()->has('failed'))
        <div
            class="alert alert-danger"
            role="alert"
        >
            {{ session()->get('failed') }}
        </div>
    @endif
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12 mb-3">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="float-left">
                                    Data Master \ Lab
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row float-right">
                                    <div class="col-md-12">
                                        <form
                                            action="{{ route('lab.index') }}"
                                            method="GET"
                                            accept-charset="utf-8"
                                        >
                                            @csrf
                                            <div class="input-group">
                                                <input
                                                    class="form-control"
                                                    type="text"
                                                    placeholder="Search"
                                                    aria-describedby="btn-search"
                                                    name="search"
                                                ></input>
                                                <div class="input-group-append">
                                                    <button
                                                        class="btn btn-outline-secondary"
                                                        type="submit"
                                                        id="btn-search"
                                                    >Search</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    {{-- <div class="col-md-6">
	                                <a href="{{ route('lab.create') }}" class="btn btn-primary" style="width: 100%;">Tambah Data Lab</a>
	                            </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @forelse($data as $lab)
                <div class="col-md-6">
                    <div class="card card-block d-flex">
                        {{-- <img src="{{ asset($lab->foto) }}" alt="" class="card-img-top"> --}}
                        <div class="card-body">
                            <h3 class="card-title">{{ $lab->nama }}</h3>
                            <p class="card-text">{{ $lab->keterangan }}</p>
                            <div class="row">
                                {{-- <div class="col-md-4">
		                    <a href="{{ route('admin.peralatan.index', $lab->id) }}" class="btn btn-info btn-sm" style="width: 100%;">Peralatan</a>
						</div> --}}
                                <div class="col-md-12">
                                    <a
                                        href="{{ route('lab.edit', $lab->id) }}"
                                        class="btn btn-warning btn-sm"
                                        style="width: 100%;"
                                    >Edit</a>
                                </div>
                                {{-- <div class="col-md-4">
			    <form onsubmit="return confirm('Hapus data permanen ?');" action="{{route('lab.destroy', $lab->id)}}" method="post" class="d-inline">
                              @csrf
                              @method('delete')
                              <button type="submit" class="btn d-inline btn-sm btn-danger">Hapus</button>
                            </form> 
						</div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-md-12">
                    <p class="text-center">Data tidak ada</p>
                </div>
            @endforelse
            <div class="col-md-12">
                {{ $data->links() }}
            </div>
        </div>
    </div>
    <form
        action=""
        id="formDelete"
        method="POST"
    >
        @csrf
        @method('DELETE')
    </form>
@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">
        function deleteData(id) {
            let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

            const formDelete = document.getElementById('formDelete')
            formDelete.action = '/admin/lab/' + id;

            if (r == true) {
                formDelete.submit();
            } else {
                alert('Penghapusan dibatalkan.');
            }
        }
    </script>
@endpush
