@extends('layouts.adminlte')
@section('main')
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="float-left">
                                Setting Applikasi
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
							<form action="{{ route('admin.setting-app.store') }}" method="POST" accept-charset="utf-8"  enctype="multipart/form-data">
								@csrf
				                <div class="row">
				                    <div class="col-md-12">
				                    	<img src="{{ asset($data->logo) }}" alt="" style="width: 200px; height: auto;">
				                    </div>
				                    <div class="col-md-12">
				                        <div class="form-row">
				                            <div class="form-group col-md-12">
				                                <label>Logo <small class="text-success">*Tidak Harus diisi</small></label>
				                                <input type="file" name="logo" class="form-control-file @error('logo') is-invalid @enderror">
				                                @error('logo')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                            </div>
				                        </div>
				                    </div>
				                    <div class="col-md-12">
				                        <div class="form-row">
				                            <div class="form-group col-md-12">
				                                <label>Nama App <small class="text-success">*Harus diisi</small></label>
				                                <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{ $data->nama }}">
				                                @error('nama')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                            </div>
				                        </div>
				                    </div>
				                    <div class="col-md-12">
				                        <div class="form-row">
				                            <div class="form-group col-md-6">
				                                <label>E-mail Balai <small class="text-success">*Harus diisi</small></label>
				                                <input type="text" name="email" class="form-control" value="{{ $data->email }}">
				                                @error('email')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                            </div>
				                            <div class="form-group col-md-6">
				                                <label>Nomor Kontak <small class="text-success">*Tidak Harus diisi</small></label>
				                                <input type="text" name="nomor_kontak" class="form-control" value="{{ $data->nomor_kontak }}" placeholder="Contoh: 628*********">
				                                @error('nomor_kontak')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                            </div>
				                        </div>
				                    </div>
				                    <div class="col-md-12">
				                        <div class="form-row">
				                            <div class="form-group col-md-12">
				                                <label>Copyright <small class="text-success">*Harus diisi</small></label>
				                                <input type="text" name="copyright" class="form-control @error('copyright') is-invalid @enderror" value="{{ $data->copyright }}">
				                                @error('copyright')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                            </div>
				                        </div>
				                    </div>
				                    <div class="col-md-12">
				                        <div class="form-row">
				                            <div class="form-group col-md-12">
				                                <label>Alamat <small class="text-success">*Tidak Harus diisi</small></label>
				                                <textarea name="alamat" class="form-control" rows="5">{{ $data->alamat }}</textarea>
				                                @error('alamat')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                            </div>
				                        </div>
				                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12" hidden>
                                                <label>Hari Libur <small class="text-success">*Tidak Harus diisi</small></label>
                                                <input
                                                    type="text"
                                                    class="form-control datepicker"
                                                    name="hari_libur"
                                                    value="{{$data->hari_libur}}"
                                                />
                                            </div>
                                        </div>
                                    </div>
				                    <div class="col-md-12">
				                        <div class="float-right">
				                            <button type="submit" class="btn btn-success btn-sm">Update</button>
				                        </div>
				                    </div>
				                </div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('script')
    <script>
        $('.datepicker').datepicker({
            format: 'd-m-yyyy',
            multidate: true,
            startDate: '+1d',
            daysOfWeekDisabled: [0,6]
        });
    </script>
@endpush
