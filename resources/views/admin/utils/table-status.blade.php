@if ($status === 'pending')
    <a
        href="{{ route('admin.permintaan-pengujian.show', $id) }}"
        class="btn btn-sm btn-outline-danger"
    >
        Menunggu Tagihan
    </a>
@endif

@if ($status === 'waiting')
    <button
        type="button"
        class="btn btn-sm btn-warning"
    >
        Menunggu Pembayaran
    </button>
@endif

@if ($status === 'diproses')
    <button
        type="button"
        class="btn btn-sm btn-primary"
    >
        Sedang Diproses
    </button>
@endif

@if ($status === 'diterima')
    <button
        type="button"
        class="btn btn-sm btn-success"
    >
        Diterima
    </button>
@endif

@if ($status === 'ditolak')
    <button
        type="button"
        class="btn btn-sm btn-danger"
    >
        Ditolak
    </button>
@endif

@if ($status === 'selesai')
    <button
        type="button"
        class="btn btn-sm btn-secondary"
    >
        Selesai
    </button>
@endif
