@extends('layouts.adminlte')
@section('main')
    @include('admin.utils.alert')
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4 moksd">
                                <div class="float-left">
                                    {{ $title }}
                                </div>
                            </div>
                            @if($title == 'Peserta Bimtek')
                            <div class="col-md-8 moksd">
                                <div class="float-right">
                                    <a
                                        href="{{ route('bimtek-peserta.export') }}"
                                        class="ml-1 btn btn-warning btn-sm"
                                    >Export Data Peserta Bimtek</a>
                                </div>
                            </div>
                                @endif
                        </div>
                    </div>
                    <div class="card-body">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush
