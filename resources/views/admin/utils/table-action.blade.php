@if (isset($unit_kerja))
    <a class="btn btn-sm btn-primary" role="button"
       href="{{ $unit_kerja }} ">Lihat Unit Kerja</a>
@endif

@if (isset($detail))
    <a class="btn btn-sm btn-primary" role="button"
       href="{{ $detail }} ">Detail</a>
@endif

@if (isset($edit))
    <a href="{{ $edit }}" class="btn btn-sm btn-secondary"
       role="button">Edit</a>
@endif

@if (isset($delete))
    <form onsubmit="return confirm('Hapus data permanen ?');"
          action="{{ $delete }}" method="post" class="d-inline">
        @csrf
        @method('delete')
        <button type="submit"
                class="btn d-inline btn-sm btn-danger">Hapus</button>
    </form>
@endif
