<div class="input-group">
    <select
        name='alat_pengujian[]'
        class="form-control peralatan-list"
        multiple
    >
        <option>Pilih Alat Pengujian</option>
    </select>
    <span class="input-group-append">
        <a
            href="#barcodeModal"
            data-href="#barcodeModal"
            data-toggle="modal"
            type="button"
            class="btn btn-info btn-flat btn-open-scanner"
        ><i class="fa fa-camera"></i></a>
    </span>
</div>
<div
    class="modal fade"
    id="barcodeModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="barcodeModalLabel"
    aria-hidden="true"
>
    <div
        class="modal-dialog"
        role="document"
    >
        <div class="modal-content">
            <div class="modal-header">
                <h5
                    class="modal-title"
                    id="exampleModalLabel"
                >Scan Barcode Peralatan</h5>

            </div>
            <div class="modal-body">
                <div id="qr-reader"></div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script>
    <script>
        $(document).ready(function() {
            const format = (item) => {
                if (!item.id) {
                    return item.text;
                }

                var url = item.element.getAttribute('data-avatar');

                var ahref = '';
                if (item.foto_alat) {
                    var img = $("<img>", {
                        class: "rounded-circle me-2",
                        width: 25,
                        height: 25,
                        src: item.foto_alat
                    });
                    var ahref = $("<a>", {
                        href: item.foto_alat,
                    });
                    ahref.attr('target', '_blank');
                    ahref.append(img);
                }

                var seri = item.seri ?? '';
                var span = $("<span>", {
                    text: " " + item.text + " " + seri
                });
                // span.prepend(ahref);
                return span;
            }

            function formatState(state) {
                if (!state.id) {
                    return state.text;
                }

                var baseUrl = "/user/pages/images/flags";
                var $state = $(
                    `<span>${state.foto_alat && '<img class="img-flag mr-2" width="50" height="50" />' || ''} <span></span> <a></a></span>`
                );

                $state.find("span").text(state.text);
                $state.find("a").text(state.seri);
                if (state.foto_alat) {
                    $state.find("img").attr("src", state.foto_alat);
                }

                return $state;
            };

            $peralatans = $('.peralatan-list').select2({
                theme: "bootstrap4",
                placeholder: "Pilih Peralatan",
                ajax: {
                    url: "{{ route('admin.peralatan.get-peralatan-by-name') }}",
                    cache: true,
                    delay: 250,
                },
                minimumInputLength: 2,
                templateSelection: format,
                templateResult: formatState
            });

            function onScanSuccess(decodedText, decodedResult) {
                $('#barcodeModal').modal('hide');
                $.get(`{{ route('admin.peralatan.get-peralatan-by-id') }}/?id=${decodedText}`, function(data) {
                    var option = new Option(data.text + ' ' + data.seri, data.id, true, true);
                    $('.peralatan-list').append(option).trigger('change');

                    $peralatans.trigger({
                        type: 'select2:select',
                        params: {
                            data: data
                        }
                    });
                });
            }

            function onScanError(errorMessage) {
                // console.warn(errorMessage)
            }

            function onStartFailed(err) {
                console.log('FAILED TO START: ', err)
            }

            var html5QrCode = new Html5Qrcode("qr-reader");

            $('#barcodeModal').on('shown.bs.modal', function(e) {
                Html5Qrcode.getCameras().then(devices => {
                    if (devices && devices.length) {
                        var cameraId = devices[0].id;
                        const config = {
                            fps: 10,
                            qrbox: 250
                        };
                        html5QrCode.start({
                            facingMode: {
                                exact: "environment"
                            }
                        }, config, onScanSuccess);
                        // html5QrCode.start(
                        //             cameraId, {
                        //             fps: 10,
                        //             qrbox: 250,
                        //             facingMode: {
                        //                 exact: "environment"
                        //             }
                        //         },
                        //         onScanSuccess,
                        //         onScanError)
                        //     .catch((err) => {
                        //         onStartFailed(err)
                        //     });
                    }
                }).catch(err => {
                    console.log('FAILED TO GET CAMERA DEVICES: ', err)
                });
            })
            $('#barcodeModal').on('hide.bs.modal', function(e) {
                html5QrCode.stop();
            })
        })
    </script>
@endpush
