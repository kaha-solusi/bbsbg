@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [
        ['judul' => 'Gambar Banner', 'link' => route('slider.index')],
        ['judul' => 'Ubah Gambar Banner']
    ]
])
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="card">
        <div class="card-header">
           Ubah Banner
        </div>
        <form action="{{route('slider.update', $slider->id)}}" method="POST" enctype="multipart/form-data" >
            @csrf
            @method('patch')
            <div class="card-body">
                <div class="form-group">
                    <label for="foto">Foto Banner</label><small class="text-success">*Tidak harus diisi</small>
                    <input type="file" name="foto" id="foto" class="form-control-file @error('foto') is-invalid @enderror">
                    <small id="detail" class="form-text text-danger">Tipe file image: JPG, JPEG, PNG; Max berukuran 2mb</small>
                    @error('foto')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="judul">Judul Banner</label><small class="text-success">*Harus diisi</small>
                    <input value="{{$slider->judul}}" type="text" name="judul" id="judul" class="form-control @error('judul') is-invalid @enderror">
                    @error('judul')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="keterangan">Keterangan Banner</label><small class="text-success">*Tidak Harus diisi</small>
                    <textarea name="keterangan" id="keterangan" rows="2" class="form-control @error('keterangan') is-invalid @enderror">{{$slider->keterangan}}</textarea>
                    @error('keterangan')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection