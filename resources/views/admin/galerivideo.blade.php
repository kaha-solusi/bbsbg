@extends('layouts.adminlte')
@section('main')
<div class="container">
    @if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
    @endif
    @if (session()->has('failed'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('failed') }}
        </div>
    @endif
    <div class="row h-100">
        <div class="col-md-12 mb-3">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-left">
                            	Galeri Foto
                            </div>
                            <div class="float-right">
                                <a href="{{ route('galerivideo.create') }}" class="btn btn-primary btn-sm">Tambah Data Foto</a>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		@forelse($video as $galeri)
        <div class="col-md-4">
            <div class="card card-block d-flex">
                <img src="{{ asset($galeri->foto) }}" alt="" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-body">{{ $galeri->judul }}</h5>
				    <div class="row">
				        <div class="col-md-6">
		                    <a href="{{ route('galeri.edit', $galeri->id) }}" class="btn btn-warning btn-sm" style="width: 100%;">Edit</a>
						</div>
				        <div class="col-md-6">
							<button type="button" onclick="deleteData({{ $galeri->id }})" class="btn btn-danger btn-sm" style="width: 100%;">Hapus</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		@empty
		<div class="col-md-12">
            <p class="text-center">Data tidak ada</p>
		</div>
		@endforelse
        <div class="col-md-12">
        	{{ $data->links() }}
        </div>
	</div>
</div>
<form action="" id="formDelete" method="POST">
    @csrf
    @method('DELETE')
</form>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
	function deleteData(id) {
		let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

		const formDelete = document.getElementById('formDelete')
        formDelete.action = '/admin/galerivideo/'+id;

		if (r == true) {
			formDelete.submit();
		} else {
			alert('Penghapusan dibatalkan.');
		}
	}
</script>
@endpush