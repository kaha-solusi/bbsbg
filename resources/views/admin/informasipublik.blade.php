@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [
        [
            'judul' => 'Informasi Publik',
        ]
]])
<div class="container">
    <div class="d-flex justify-content-center">
        <div class="card w-100">
            <div class="card-header">
              <div class="d-flex content-align-between justify-content-between">
                  <a href="{{route('informasipublik.create')}}" class="btn btn-primary">Tambah Data Informasi Publik</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead class="bg-hijau">
                <tr>
                  <th>No.</th>
                  <!--<th>Foto</th>-->
                  <th>Nama</th>
                  <th>Link</th>
                  <th>Tindakan</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0;?>
                @foreach ($informasipublik as $item)
                <tr>
                    <td>{{$i++}}</td>
                    <!--<td>-->
                    <!--    @if (!empty($item->getFoto()))-->
                    <!--        <img src="{{ asset($item->getFoto()) }}" class="img-thumbnail" alt="" width="75px">                            -->
                    <!--    @endif-->
                    <!--</td>-->
                    <td>{{$item->nama}}</td>
                    <td>{{$item->link}}</td>
                    <td>
                        <div class="row">
                            <a href="{{route('informasipublik.edit', $item->id)}}" class="btn d-inline btn-sm mr-1 btn-outline-success">Ubah</a>
                            <form class="d-inline" action="{{route('informasipublik.destroy', $item->id)}}" onsubmit="return confirm('Data Akan Terhapus Permanen ! Hapus ?');" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-sm btn-outline-danger mr-1">Hapus</button>
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
    </div>
</div>
@endsection