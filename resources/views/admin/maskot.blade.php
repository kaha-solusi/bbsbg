@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [[
        'judul' => 'Data Maskot',
        'link' => route('maskot.index')
        ]]
])
<div class="container">
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
    <div class="d-flex justify-content-center">
        <div class="card w-100">
            <div class="card-header">
                <div class="d-flex content-align-center justify-content-center">
                    <img src="{{ ($maskot->foto == null) ? asset('/assets/default/default_img.png') : asset($maskot->foto) }}" alt="Foto Maskot" class="img-md" 
                    style="width: 500px; height: auto;" >
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table">
                <tr>
                    <td>Nama Maskot</td>
                    <td>{{$maskot->nama}}</td>
                </tr>
                <tr>
                    <td>Deskripsi Maskot</td>
                    <td>{{$maskot->deskripsi}}</td>
                </tr>
              </table>
            </div>
            <div class="card-footer">
                <a href="{{route('maskot.edit', $maskot->id)}}" class="btn btn-success">Ubah</a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
    </div>
</div>
@endsection