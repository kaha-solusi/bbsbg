@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [[
    'judul' => 'Data Pegawai',
    'link' => route('admin.pegawai.index')
    ],
    ['judul' => 'Tambah Data Pegawai']
    ]])
    <div class="container">
        @if (session()->has('success'))
            <div
                class="alert alert-success"
                role="alert"
            >
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session()->has('failed'))
            <div
                class="alert alert-danger"
                role="alert"
            >
                {{ session()->get('failed') }}
            </div>
        @endif
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Tambah Data Pegawai
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    action="{{ route('admin.pegawai.store') }}"
                                    method="POST"
                                    accept-charset="utf-8"
                                    enctype="multipart/form-data"
                                >
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Foto Pegawai <small class="text-success">*Tidak Harus
                                                            diisi</small></label>
                                                    <input
                                                        type="file"
                                                        name="foto"
                                                        class="form-control-file @error('foto') is-invalid @enderror"
                                                        aria-describedby="detail"
                                                    >
                                                    <small
                                                        id="detail"
                                                        class="form-text text-danger"
                                                    >Tipe file image: JPG, JPEG, PNG; Max berukuran 2mb</small>
                                                    @error('foto')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Nama <small class="text-success">*Harus diisi</small></label>
                                                    <input
                                                        type="text"
                                                        name="nama"
                                                        class="form-control @error('nama') is-invalid @enderror"
                                                        value="{{ old('nama') }}"
                                                    >
                                                    @error('nama')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>NIP <small class="text-success">*Harus diisi</small></label>
                                                    <input
                                                        type="numeric"
                                                        name="nip"
                                                        class="form-control @error('nip') is-invalid @enderror"
                                                        value="{{ old('nip') }}"
                                                    >
                                                    @error('nip')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>E-mail <small class="text-success">*Harus diisi</small></label>
                                                    <input
                                                        type="text"
                                                        name="email"
                                                        class="form-control @error('email') is-invalid @enderror"
                                                        value="{{ old('email') }}"
                                                    >
                                                    @error('email')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>No HP <small class="text-success">*Harus diisi</small></label>
                                                    <input
                                                        type="numeric"
                                                        name="no_hp"
                                                        class="form-control @error('no_hp') is-invalid @enderror"
                                                        value="{{ old('no_hp') }}"
                                                        placeholder="Contoh: 62895389981992"
                                                    >
                                                    @error('no_hp')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Pilih Lab <small class="text-success">*Harus
                                                            diisi</small></label>
                                                    <select
                                                        name="lab_id"
                                                        class="form-control @error('lab_id') is-invalid @enderror"
                                                    >
                                                        @forelse($data as $lab)
                                                            <option value="{{ $lab->id }}">{{ $lab->nama }} ||
                                                                Kategori: <span
                                                                    style="text-transform: uppercase;">{{ $lab->kategori }}</span>
                                                            </option>
                                                        @empty
                                                            <option
                                                                value=""
                                                                disabled
                                                            >Tidak ada data lab</option>
                                                        @endforelse
                                                    </select>
                                                    @error('lab_id')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-12 mb-3"
                                            id="btnAdd"
                                        >
                                            <div class="float-right">
                                                <button
                                                    type="submit"
                                                    class="btn btn-success btn-sm"
                                                >Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">

    </script>
@endpush
