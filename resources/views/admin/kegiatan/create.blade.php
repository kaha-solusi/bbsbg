@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
	'judul' => [
        ['judul' => 'Data Jadwal Pengujian',
		'link' => route('jadwal-pengujian.index')],
        ['judul' => 'Tambah Data Jadwal Pengujian',
        'link' => route('jadwal-pengujian.create')]
    ]
])
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="float-left">
                               Tambah Data Jadwal Pengujian
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('jadwal-pengujian.store') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Nama Kegiatan <small class="text-success">*Harus diisi</small></label>
                                                <input type="text" name="nama_kegiatan" class="form-control-file @error('nama_kegiatan') is-invalid @enderror" value="{{ old('nama_kegiatan') }}">
                                                @error('nama_kegiatan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Deskripsi Kegiatan <small class="text-success">*Harus diisi</small></label>
                                                <textarea name="deskripsi_kegiatan" class="form-control @error('deskripsi_kegiatan') is-invalid @enderror" rows="3">{{ old('deskripsi_kegiatan') }}</textarea>
                                                @error('deskripsi_kegiatan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Waktu Pelaksanaan <small class="text-success">*Harus diisi</small></label>
                                                <input type="date" name="waktu_pelaksanaan" class="form-control @error('waktu_pelaksanaan') is-invalid @enderror" value="{{ old('waktu_pelaksanaan') }}">
                                                @error('waktu_pelaksanaan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Biaya Kegiatan (Rp.) <small class="text-success">*Harus diisi</small></label>
                                                <input type="number" min="0" name="biaya_kegiatan" class="form-control @error('biaya_kegiatan') is-invalid @enderror" value="{{ old('biaya_kegiatan') }}">
                                                @error('biaya_kegiatan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Tempat Pelaksanaan <small class="text-success">*Harus diisi</small></label>
                                                <textarea name="tempat_pelaksanaan" class="form-control @error('tempat_pelaksanaan') is-invalid @enderror" rows="3">{{ old('tempat_pelaksanaan') }}</textarea>
                                                @error('tempat_pelaksanaan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Lab <small class="text-success">*Harus diisi</small></label>
                                                <select name="lab_id" class="form-control @error('lab_id') is-invalid @enderror">
                                                    @forelse($data as $lab)
                                                    <option value="{{ $lab->id }}">{{ $lab->nama }}</option>
                                                    @empty
                                                    <option value="" disabled>Tidak ada data</option>
                                                    @endforelse
                                                </select>
                                                @error('lab_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-3" id="btnAdd">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-success btn-sm">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">

</script>
@endpush