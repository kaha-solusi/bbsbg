@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', ['judul' => [['judul' => 'Data Berita']]])

<!------ Include the above in your HEAD tag ---------->
<div class="container mx-2 ">
<div class="card">
    <div class="card-header">
        <h4>Data berita yang akan di tampilkan di halaman utama</h4>
    </div>
    <div class="card-body">
        <table id="example1" class="table w-100 table-bordered table-striped">
          <thead class="bg-hijau">
          <tr>
            <th>Jumlah Berita Terbaru</th>
            <th>Panel Berita 1</th>
            <th>Panel Berita 2</th>
            <th>Panel Berita 3</th>
            <th>Panel Berita 4</th>
            <th>Tindakan</th>
          </tr>
          </thead>
          <tbody>
          <tr>
              <td>{{$beritaUI->jumlah_berita}}</td>
              <td>{{($beritaUI->berita_1_id == 0) ? 'acak' : $beritaUI->berita->judul}}</td>
              <td>{{($beritaUI->berita_2_id == 0) ? 'acak' : $beritaUI->berita->judul}}</td>
              <td>{{($beritaUI->berita_3_id == 0) ? 'acak' : $beritaUI->berita->judul}}</td>
              <td>{{($beritaUI->berita_4_id == 0) ? 'acak' : $beritaUI->berita->judul}}</td>
              <td>
                  <div class="row">
                      <a href="{{route('beritaui.edit', $beritaUI->id)}}" class="btn d-inline btn-sm btn-outline-success">Ubah</a>
                  </div>
              </td>
          </tr>
          </tbody>
        </table>
      </div>
</div>
<!--END SECTION-->
</div>
@endsection