 <!-- Content Header (Page header) -->
 <section class="content-header">
     <div class="container">
         <div class="row mb-2">
             <div class="col-auto">
                 <ol class="breadcrumb float-sm-right">
                     <li class="breadcrumb-item"><a href="{{ route('adminlteHome.dashboard') }}">Home</a></li>
                     @for ($i = 0; $i < count($judul); $i++)
                         @if (empty($judul[$i]['link']))
                             <li class="breadcrumb-item active">{{ $judul[$i]['judul'] }}</li>
                         @else
                             <li class="breadcrumb-item "><a href="{{ $judul[$i]['link'] }}">{{ $judul[$i]['judul'] }}</a>
                             </li>
                         @endif
                     @endfor
                 </ol>
             </div>
         </div>
     </div><!-- /.container-fluid -->
 </section>
