@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    ['judul' => 'Data Jadwal Bimtek',
    'link' => route('jadwal-bimtek.index')],
    ['judul' => 'Tambah Data Jadwal Bimtek',
    'link' => route('jadwal-bimtek.create')]
    ]
    ])
    <div class="container">
        @if (session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session()->has('failed'))
            <div class="alert alert-danger" role="alert">
                {{ session()->get('failed') }}
            </div>
        @endif
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Edit Data Peserta Bimtek
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{ route('pesertabimtek.update', ['id' => $peserta->id]) }}"
                                      method="POST" accept-charset="utf-8"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label
                                                           style="font-weight: bold;">Nama
                                                        Bimbingan Teknis</label>
                                                    <p>Ini Nama Bimtek</p>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-row">
                                                        <div class="col-md-6">
                                                            <div
                                                                 class="form-group">
                                                                <label>Nama Pegawai
                                                                    <span
                                                                          style="color: red;">*</span></label>
                                                                <input type="text"
                                                                       name="nama"
                                                                       id="nama"
                                                                       value="{{ $peserta->nama }}"
                                                                       class="form-control corner-edge"
                                                                       required="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div
                                                                 class="form-group">
                                                                <label
                                                                       id="nip">NIP/NIK
                                                                    <span
                                                                          style="color: red;">*</span></label>
                                                                <input type="text"
                                                                       name="nip"
                                                                       id="nip"
                                                                       value="{{ $peserta->nip_nik }}"
                                                                       class="form-control corner-edge"
                                                                       required="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-row">
                                                        <div class="col-md-6">
                                                            <div
                                                                 class="form-group">
                                                                <label>Asal
                                                                    Instansi</label>
                                                                <input type="text"
                                                                       name="instansi"
                                                                       id="instansi"
                                                                       value="{{ $peserta->instansi }}"
                                                                       class="form-control corner-edge"
                                                                       required="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div
                                                                 class="form-group">
                                                                <label
                                                                       id="nip">Jabatan</label>
                                                                <input type="text"
                                                                       name="jabatan"
                                                                       id="jabatan"
                                                                       value="{{ $peserta->jabatan }}"
                                                                       class="form-control corner-edge"
                                                                       required="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-row">
                                                        <div class="col-md-6">
                                                            <div
                                                                 class="form-group">
                                                                <label>No. HP <span
                                                                          style="color: red;">*</span></label>
                                                                <input type="text"
                                                                       name="no_hp"
                                                                       id="no_hp"
                                                                       value="{{ $peserta->no_hp }}"
                                                                       class="form-control corner-edge"
                                                                       required="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div
                                                                 class="form-group">
                                                                <label
                                                                       id="nip">E-Mail
                                                                    <span
                                                                          style="color: red;">*</span></label>
                                                                <input type="text"
                                                                       name="email"
                                                                       id="email"
                                                                       value="{{ $peserta->email }}"
                                                                       class="form-control corner-edge"
                                                                       required="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-row">
                                                        <div class="col-md-12">
                                                            <div
                                                                 class="form-group">
                                                                <label
                                                                       id="nip">Alamat</label>
                                                                <textarea type="text"
                                                                          name="alamat"
                                                                          id="alamat"
                                                                          rows="5"
                                                                          class="form-control ">{{ $peserta->alamat }}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="hidden"
                                                           name="bimtek_id"
                                                           id="bimtek_id" value=""
                                                           class="form-control corner-edge"
                                                           required="">
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="submit"
                                                            class="btn btn-primary btn-block corner-edge"
                                                            style="margin-bottom: 10px;">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>
@endpush
