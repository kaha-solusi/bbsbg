@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [[
        'judul' => 'Data Struktur Organisasi',
        'link' => route('struktur-organisasi.index')],
        ['judul' => 'Ubah Data Struktur Organisasi']       
]])
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="float-left">
                                Ubah Data Struktur Organisasi
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('struktur-organisasi.update', $data) }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Foto Pejabat <small class="text-success">*Tidak Harus diisi</small></label>
                                                <input type="file" name="foto" class="form-control-file @error('foto') is-invalid @enderror" aria-describedby="detail">
                                                <small id="detail" class="form-text text-danger">Tipe file image: JPG, JPEG, PNG; Max berukuran 2mb</small>
                                                @error('foto')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Nama <small class="text-success">*Harus diisi</small></label>
                                                <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{ $data->nama }}">
                                                @error('nama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>NIP <small class="text-success">*Harus diisi</small></label>
                                                <input type="text" name="nip" class="form-control @error('nip') is-invalid @enderror" value="{{ $data->nip }}">
                                                @error('nip')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Jabatan <small class="text-success">*Harus diisi</small></label>
                                                <input type="text" name="jabatan" class="form-control @error('jabatan') is-invalid @enderror" value="{{ $data->jabatan }}">
                                                @error('jabatan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Email <small class="text-success">*Harus diisi</small></label>
                                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ $data->email }}">
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Keterangan <small class="text-success">*Harus diisi</small></label>
                                                <textarea name="keterangan" class="form-control @error('keterangan') is-invalid @enderror" rows="3">{{ $data->keterangan }}</textarea>
                                                @error('jabatan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-3" id="btnAdd">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-success btn-sm">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">

</script>
@endpush