@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', ['judul' => [['judul' => 'Data Pelayanan']]])
    <div class="container">
        @if (session()->has('success'))
            <div
                class="alert alert-success"
                role="alert"
            >
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session()->has('failed'))
            <div
                class="alert alert-danger"
                role="alert"
            >
                {{ session()->get('failed') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h2>Bimbingan Teknis</h2>
                        {{ $bimtek }}
                        <div class="float-right">
                            <a
                                href="{{ route('bimtek-peserta.export') }}"
                                class="ml-1 btn btn-warning btn-sm"
                            >Export Data Peserta Bimtek</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php $i = 0; ?>
                        <table class="table table-hover w-100">
                            <thead class="bg-hijau">
                                <tr>
                                    <td>No.</td>
                                    <td>Nama</td>
                                    <td>NIP/NIK</td>
                                    <td>Instansi</td>
                                    <td>No. HP</td>
                                    <td>E-Mail</td>
                                    <td width="250">Alamat</td>
                                    <td>Tindakan</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($peserta as $p)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $p->name }}</td>
                                        <td>{{ $p->client->nomor ?? '-' }}</td>
                                        @if ($p->client ?? null)
                                            @if ($p->client->nama_perusahaan ?? null)
                                                <td>{{ $p->client->nama_perusahaan }}</td>
                                            @elseif($p->client->group === 1)
                                                <td>{{ $p->client->unit_kerja->nama_instansi ?? '-' }}</td>
                                            @else
                                                <td>{{ $p->client->nama_perusahaan }}</td>
                                            @endif
                                        @endif

                                        <td>{{ $p->client->phone_number ?? '-' }}</td>
                                        <td>{{ $p->email }}</td>
                                        <td>{{ $p->client->alamat ?? '-' }}</td>
                                        <td>
                                            <form
                                                onsubmit="return confirm('Hapus data permanen ?');"
                                                action="{{ route('pesertabimtek.destroy', $p->pivot->id) }}"
                                                method="post"
                                                class="d-inline"
                                            >
                                                @csrf
                                                @method('delete')
                                                <button
                                                    type="submit"
                                                    class="btn d-inline btn-sm btn-danger"
                                                >Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
