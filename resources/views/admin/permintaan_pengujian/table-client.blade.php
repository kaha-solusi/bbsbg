@extends('layouts.adminlte')
@section('main')
{{-- @include('admin.parts.breadcrumbs', ['judul' => [['judul' => 'Permintaan Pengujian']]]) --}}
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
<div class="container mt-3">
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="float-left">
                        Permintaan Pengujian
                    </div>
                    <div class="float-right">
                        <a href="{{route('admin.permintaan.export')}}"
                           class="ml-1 btn btn-warning"
                        >Export Data Permintaan Pengujian</a>
                        <a
                            href="{{ route('pengujian.registrasi') }}"
                            class="ml-1 btn btn-primary"
                        >Tambah Permintaan Pengujian</a>
                    </div>
                </div>

                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
							<table class="table">
								<thead class="thead-custom">
									<tr>
										<th scope="col">ID</th>
										<th scope="col">No. Sampel</th>
										<th scope="col">Tgl Permintaan</th>
										<th scope="col">Nama Pemohon</th>
										<th scope="col">Email</th>
										<th scope="col">Nomor HP</th>
										<th scope="col">Pelayanan</th>
										<th scope="col">Status</th>
									</tr>
								</thead>
								<tbody>
									<!--@php $i = 1 @endphp
									@forelse($data as $d)
									<tr class="tbody-custom">
										<td>$d->id</td>
										<td>$d->tgl_permintaan </td>
										<td>$d->nama_pemohon </td>
										<td>$d->email </td>
										<td>$d->nomor_hp_kontak </td>
										<td>$d->pelayanan->nama</td>
										<td>--</td>
										<td>
											<button type="button" onclick="sosialMedia( $d )" class="btn btn-light btn-sm" data-toggle="modal" data-target="#sosialMediaModal">Detail</button>
										</td>
										<td>
											@if($d->status == 'diproses')
												Diproses
											@elseif($d->status == 'ditolak')
												Permintaan Ditolak
											@elseif($d->status == 'diterima')
												Permintaan Diterima
											@elseif($d->status == 'selesai')
												Selesai
											@endif
										</td>
									</tr>
									@empty
									<tr class="tbody-custom">
										<td colspan="8" class="text-center">Tidak ada data.</td>
									</tr>
									@endforelse-->
									<tr class="tbody-custom">
										<td>1</td>
										<td>428</td>
										<td>22 Agustus 2021</td>
										<td>Julian</td>
										<td>julian@maildomain.com</td>
										<td>38472884</td>
										<td>--</td>
										<td><span class="badge badge-light">Menunggu Konfirmasi Pembayaran</span></td>
									</tr>
									<tr class="tbody-custom">
										<td>2</td>
										<td>427</td>
										<td>20 Agustus 2021</td>
										<td>Julian</td>
										<td>julian@maildomain.com</td>
										<td>38472884</td>
										<td>--</td>
										<td><a class="btn btn-success" href="{{ route('client.permintaan.statuspengujian', ['status' => 'kirimsampel']) }}">Pengiriman Sampel Uji</a></td>
									</tr>
									<tr class="tbody-custom">
										<td>3</td>
										<td>426</td>
										<td>18 Agustus 2021</td>
										<td>Julian</td>
										<td>julian@maildomain.com</td>
										<td>38472884</td>
										<td>--</td>
										<td><span class="badge badge-pill  badge-info">Proses Pengiriman Sampel Uji</span></td>
									</tr>
									<tr class="tbody-custom">
										<td>4</td>
										<td>425</td>
										<td>16 Agustus 2021</td>
										<td>Julian</td>
										<td>julian@maildomain.com</td>
										<td>38472884</td>
										<td>--</td>
										<td><span class="badge badge-pill badge-dark">Proses Pengujian</span></td>
									</tr>
								</tbody>
							</table>
						</div>
                        <div class="col-md-12">
                        	{{ $data->links() }}
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{{-- Modal --}}
<div class="modal fade" id="sosialMediaModal" tabindex="-1" aria-labelledby="mediaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mediaModalLabel">Detail data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="list-group">
        	<li class="list-group-item" id="tgl_permintaan"></li>
        	<li class="list-group-item" id="nama_pemohon"></li>
        	<li class="list-group-item" id="alamat"></li>
        	<li class="list-group-item" id="nama_kontak"></li>
        	<li class="list-group-item" id="nomor_hp"></li>
        	<li class="list-group-item" id="email"></li>
        	<li class="list-group-item" id="jenis"></li>
        	<li class="list-group-item" id="nama_lhu"></li>
        	<li class="list-group-item" id="email_persuratan"></li>
        	<li class="list-group-item" id="tanggal_pengujian"></li>
        	<li class="list-group-item" id="detail_layanan"></li>
        	<li class="list-group-item" id="uraian_pengujian"></li>
        	<li class="list-group-item" id="status"></li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
{{-- End Modal --}}

@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">

	function sosialMedia(data) {
		$('#tgl_permintaan').html(`<b>Tgl Permintaan:</b><br>`+data.tgl_permintaan)
		$('#nama_pemohon').html(`<b>Nama Pemohon:</b><br>`+data.nama_pemohon)
		$('#alamat').html(`<b>Alamat:</b><br>`+data.alamat)
		$('#nama_kontak').html(`<b>Nama Kontak:</b><br>`+data.nama_kontak)
		$('#nomor_hp').html(`<b>Nomor HP:</b><br>`+data.nomor_hp_kontak)
		$('#email').html(`<b>Email:</b><br>`+data.email)
		$('#jenis').html(`<b>Pelayanan:</b><br>`+data.pelayanan.nama)
		$('#nama_lhu').html(`<b>Nama LHU:</b><br>`+((data.nama_lhu == null) ? 'Tidak ada' : data.nama_lhu))
		$('#email_persuratan').html(`<b>Email Persuratan:</b><br>`+data.email_persuratan)
		$('#tanggal_pengujian').html(`<b>Tanggal Pengujian:</b><br>`+data.tanggal_pengujian)
		$('#detail_layanan').html(`<b>Detail Layanan:</b><br>`+data.detail_layanan)
		$('#uraian_pengujian').html(`<b>Uraian Pengujian:</b><br>`+data.uraian_pengujian)
		$('#status').html(`<b>Status:</b><br>`+data.status)
	}
</script>
@endpush
