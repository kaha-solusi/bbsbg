@extends('layouts.adminlte')
@section('main')
    @php
    $permintaan_pengujian = $surat_perintah_kerja->permintaan_pengujian;
    @endphp
    <div class="container mt-3">
        <div class="card card-block d-flex">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-4">
                        <div class="float-left">
                            Surat Perintah Kerja
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form
                    id="form-surat-perintah-kerja"
                    enctype="multipart/form-data"
                >
                    <div class="form-group">
                        <label for="">No. SPK</label>
                        <input
                            type="text"
                            class="form-control"
                            name="nomor"
                            readonly
                            value="{{ old('nomor', $surat_perintah_kerja->nomor) }}"
                        />
                    </div>
                    <div class="form-group">
                        <label for="">Untuk Melakukan</label>
                        <select
                            class="form-control"
                            readonly
                        >
                            <option value="set-up">Set Up dan Pengujian</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Jenis Pengujian</label>
                        <input
                            type="text"
                            class="form-control"
                            name="jenis_pengujian"
                            readonly
                            value="{{ $permintaan_pengujian->master_layanan_uji->name }}"
                        />
                    </div>
                    <div class="form-group">
                        <label for="">Metode Pengujian</label>
                        <input
                            type="text"
                            class="form-control"
                            name="metode_pengujian"
                            readonly
                            value="{{ $permintaan_pengujian->items->pluck('layanan_uji_item.standard')->unique()->join(', ') ?? '' }}"
                        />
                    </div>
                    <div class="form-group">
                        <label for="">No Sampel</label>
                        <input
                            type="text"
                            class="form-control"
                            name="no_sample"
                            readonly
                            value="{{ $surat_perintah_kerja->permintaan_pengujian->samples->last()->items->pluck('pivot.kode_sampel')->join(', ') ?? '' }}"
                        />
                    </div>

                    <div class="form-group">
                        <label for="">Tanggal Mulai</label>
                        <input
                            type="text"
                            class="form-control"
                            name="tanggal_mulai"
                            readonly
                            value="{{ old('tanggal_mulai', $surat_perintah_kerja->tanggal_mulai) }}"
                        />
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Selesai</label>
                        <input
                            type="text"
                            class="form-control"
                            name="tanggal_selesai"
                            readonly
                            value="{{ old('tanggal_selesai', $surat_perintah_kerja->tanggal_selesai) }}"
                        />
                    </div>
                    <div class="form-group">
                        <label for="">Catatan</label>
                        <textarea
                            class="form-control"
                            name="catatan"
                            readonly
                        >{{ $surat_perintah_kerja->keterangan }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <div class="row">
                            <div class="col-md-4">
                                Disusun Oleh
                                <div class="d-block">
                                    <p
                                        class="badge badge-success text-left"
                                        style="white-space: normal"
                                    >
                                        {{ $surat_perintah_kerja->creator->name }}
                                    </p>
                                </div>
                            </div>

                            @php
                                $checkedBy = $surat_perintah_kerja->approvals->filter(function ($value, $key) {
                                    return $value->creator->hasRole(['super-admin', 'subkoor']);
                                });
                                
                                $approvedBy = $surat_perintah_kerja->approvals->filter(function ($value, $key) {
                                    return $value->creator->hasRole(['super-admin', 'kepala-balai']);
                                });
                            @endphp

                            @if ($checkedBy->isNotEmpty())
                                <div class="col-md-4">
                                    Diperiksa Oleh
                                    <div class="d-block">
                                        <p
                                            class="badge {{ $checkedBy->last()->status === 'approved' ? 'badge-success' : 'badge-danger' }} text-left"
                                            style="white-space: normal"
                                        >
                                            {{ $checkedBy->last()->creator->name }}
                                        </p>
                                    </div>
                                </div>
                            @endif

                            @if ($approvedBy->isNotEmpty())
                                <div class="col-md-4">
                                    Disetujui Oleh
                                    <div class="d-block">
                                        <p
                                            class="badge {{ $checkedBy->last()->status === 'approved' ? 'badge-success' : 'badge-danger' }} text-left"
                                            style="white-space: normal"
                                        >
                                            {{ $approvedBy->last()->creator->name }}
                                        </p>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="no_resi">Alat dan Bahan Kerja</label>
                        <table
                            class="table table-hover table-bordered w-100"
                            id="table-products"
                        >
                            <thead class="bg-hijau">
                                <tr>
                                    <th width="20%">Nama Produk</th>
                                    <th width="20%">Alat</th>
                                    <th width="30%">Bahan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($surat_perintah_kerja->permintaan_pengujian->items as $item)
                                    <tr>
                                        <td>
                                            <span>{{ $item->product->name ?? '' }}</span>
                                        </td>
                                        <td>
                                            <span>{{ $item->product->alat->pluck('nama')->join(', ') ?? '' }}</span>
                                        </td>
                                        <td>
                                            <span>{{ $item->product->bahan->pluck('nama')->join(', ') ?? '' }}</span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group">
                        <label for="no_resi">Menugaskan Pengujian Kepada</label>
                        <table
                            class="table table-hover w-100"
                            id="table-sample"
                        >
                            <thead class="bg-hijau">
                                <tr>
                                    <th width="20%">Nama Pegawai</th>
                                    <th width="20%">NIP/NRP</th>
                                    <th width="30%">Jabatan</th>
                                    <th width="20%">Keterangan</th>
                                    <th width="10%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($surat_perintah_kerja->members as $member)
                                    <tr>
                                        <td>
                                            <span>{{ $member->user->name ?? '' }}</span>
                                        </td>
                                        <td>
                                            <span>{{ $member->nip ?? '' }}</span>
                                        </td>
                                        <td>
                                            <span>{{ $member->jabatan ? $member->jabatan->nama : '' }}</span>
                                        </td>
                                        <td>
                                            <span></span>
                                        </td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12">
                        @can('approve_surat_perintah_kerjas')
                            @hasanyrole('kepala-balai')
                                @if (!$surat_perintah_kerja->isApprovedBy('kepala-balai') && $surat_perintah_kerja->isApprovedBy('subkoor'))
                                    <div class="float-right">
                                        <a
                                            href="#"
                                            class="btn btn-danger reject-btn"
                                        >
                                            Tolak
                                        </a>
                                        <button
                                            type="submit"
                                            class="btn btn-primary approve-btn"
                                        >
                                            Setujui
                                        </button>
                                    </div>
                                @endif
                            @endhasanyrole

                            @hasanyrole('subkoor')
                                @if (!$surat_perintah_kerja->isApprovedBy('kepala-balai'))
                                    <div class="float-right">
                                        <a
                                            href="#"
                                            class="btn btn-danger reject-btn"
                                        >
                                            Tolak
                                        </a>
                                        <button
                                            type="submit"
                                            class="btn btn-primary approve-btn"
                                        >
                                            Setujui
                                        </button>
                                    </div>
                                @endif
                            @endhasanyrole

                            @hasrole('super-admin')
                                @if (!$surat_perintah_kerja->isApprovedBy('kepala-balai') && !$surat_perintah_kerja->isApprovedBy('subkoor'))
                                    <div class="float-right">
                                        <a
                                            href="#"
                                            class="btn btn-danger reject-btn"
                                        >
                                            Tolak
                                        </a>
                                        <button
                                            type="submit"
                                            class="btn btn-primary approve-btn"
                                        >
                                            Setujui
                                        </button>
                                    </div>
                                @endif
                            @endhasrole
                        @endcan
                        <div class="float-right mr-1">
                            <a
                                href="{{ route('admin.permintaan-pengujian.surat-perintah-kerja.index') }}"
                                class="btn btn-secondary"
                            >
                                Kembali
                            </a>
                        </div>

                    </div>

                    {{-- <div class="form-group float-right">
                        <a
                            href="{{ route('admin.permintaan-pengujian.surat-perintah-kerja.index') }}"
                            class="btn btn-secondary"
                        >Kembali</a>

                        @can('approve_surat_perintah_kerjas')
                            @hasrole('kepala-balai')
                                @if ($surat_perintah_kerja->approvals()->isCheckedBySubkoor() && !$surat_perintah_kerja->approvals()->isApprovedByKabalai())

                                    <div class="float-right">
                                        <a
                                            href="#"
                                            class="btn btn-danger reject-btn"
                                        >
                                            Tolak
                                        </a>
                                        <button
                                            type="submit"
                                            class="btn btn-primary approve-btn"
                                        >
                                            Setujui
                                        </button>
                                    </div>
                                @endif
                            @endrole

                            @hasrole('subkoor')
                                @if (!$surat_perintah_kerja->approvals()->isCheckedBySubkoor())
                                    <div class="float-right">
                                        <a
                                            href="#"
                                            class="btn btn-danger reject-btn"
                                        >
                                            Tolak
                                        </a>
                                        <button
                                            type="submit"
                                            class="btn btn-primary approve-btn"
                                        >
                                            Setujui
                                        </button>
                                    </div>
                                @endif
                            @endhasrole
                        @endcan
                        @hasanyrole('super-admin|subkoor|kepala-balai')
                            @if ($checkedBy->isEmpty() || $approvedBy->isEmpty())
                                <a
                                    href="#"
                                    class="btn btn-danger btn-reject"
                                >Tolak</a>
                                <a
                                    href="#"
                                    class="btn btn-primary btn-approve"
                                >Setujui</a>
                            @endif
                        @endhasanyrole
                    </div> --}}
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script>
        $(document).ready(function() {
            let counter = 0;
            const $table = $('#table-sample').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>

    @hasanyrole('subkoor|kepala-balai|super-admin')
        <script>
            $(document).ready(function() {
                $('.approve-btn').on('click', function(e) {
                    e.preventDefault();

                    swalWithBootstrapButtons.fire({
                        icon: 'info',
                        title: 'Apakah anda yakin?',
                        text: 'Pastikan data yang anda masukkan untuk membuat SPK sudah benar',
                        confirmButtonText: 'Ya, saya yakin!',
                        cancelButtonText: 'Batal',
                        showCancelButton: true,
                        showConfirmButton: true,
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-secondary mr-2',
                        },
                        preConfirm: () => {
                            const formData = new FormData();
                            formData.append('token', '{{ csrf_token() }}');
                            return $.ajax({
                                url: route(
                                    'admin.permintaan-pengujian.surat-perintah-kerja.approve',
                                    ['{{ $surat_perintah_kerja->id }}']),
                                dataType: 'json',
                                headers: {
                                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                                },
                                type: 'POST',
                                error: function(error) {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                }
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            swalWithBootstrapButtons.fire({
                                icon: 'success',
                                title: 'Berhasil!',
                                text: "Benda uji berhasi disetujui",
                            }).then(() => {
                                window.location.href = route(
                                    'admin.permintaan-pengujian.surat-perintah-kerja.index'
                                );
                            })
                        }
                    })
                })
            });
        </script>
    @endhasanyrole
@endpush
