@extends('layouts.adminlte')
@section('main')
    @php
    $permintaan_pengujian = $surat_perintah_kerja->permintaan_pengujian;
    @endphp
    <div class="container mt-3">
        <div class="card card-block d-flex">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-4">
                        <div class="float-left">
                            Surat Perintah Kerja
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <form
                    id="form-surat-perintah-kerja"
                    method="POST"
                    enctype="multipart/form-data"
                    action="{{ route('admin.permintaan-pengujian.surat-perintah-kerja.update', $surat_perintah_kerja->id) }}"
                >
                    @csrf
                    @method('PUT')

                    <div class="form-group">
                        <label for="">No. SPK</label>
                        <input
                            type="text"
                            class="form-control"
                            name="nomor"
                            @if ($surat_perintah_kerja->status === 'draft') required @else readonly @endif
                            value="{{ old('nomor', $surat_perintah_kerja->nomor) }}"
                        />
                    </div>
                    <div class="form-group">
                        <label for="">Untuk Melakukan</label>
                        <select
                            class="form-control"
                            @if ($surat_perintah_kerja->status === 'draft') required @else readonly @endif
                        >
                            <option value="set-up">Set Up dan Pengujian</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Jenis Pengujian</label>
                        <input
                            type="text"
                            class="form-control"
                            name="jenis_pengujian"
                            @if ($surat_perintah_kerja->status === 'draft') required @else readonly @endif
                            value="{{ $permintaan_pengujian->master_layanan_uji->name }}"
                        />
                    </div>
                    <div class="form-group">
                        <label for="">Metode Pengujian</label>
                        <input
                            type="text"
                            class="form-control"
                            name="metode_pengujian"
                            @if ($surat_perintah_kerja->status === 'draft') required @else readonly @endif
                            value="{{ $permintaan_pengujian->items->pluck('layanan_uji_item.standard')->unique()->join(', ') ?? '' }}"
                        />
                    </div>
                    <div class="form-group">
                        <label for="">No Sampel</label>
                        <input
                            type="text"
                            class="form-control"
                            name="no_sample"
                            @if ($surat_perintah_kerja->status === 'draft') required @else readonly @endif
                            value="{{ $surat_perintah_kerja->permintaan_pengujian->samples->last()->items->pluck('pivot.kode_sampel')->join(', ') ?? '' }}"
                        />
                    </div>

                    <div class="form-group">
                        <label for="">Tanggal Mulai</label>
                        <input
                            type="text"
                            class="form-control datepicker"
                            name="tanggal_mulai"
                            @if ($surat_perintah_kerja->status === 'draft') required @else readonly @endif
                            value="{{ old('tanggal_mulai', $surat_perintah_kerja->tanggal_mulai) }}"
                            autocomplete='off'
                        />
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Selesai</label>
                        <input
                            type="text"
                            class="form-control datepicker"
                            name="tanggal_selesai"
                            @if ($surat_perintah_kerja->status === 'draft') required @else readonly @endif
                            value="{{ old('tanggal_selesai', $surat_perintah_kerja->tanggal_selesai) }}"
                            autocomplete='off'
                        />
                    </div>
                    <div class="form-group">
                        <label for="">Catatan</label>
                        <textarea
                            class="form-control"
                            name="catatan"
                            @if ($surat_perintah_kerja->status === 'draft')  @else readonly @endif
                        >{{ $surat_perintah_kerja->keterangan }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <div class="row">
                            <div class="col-md-4">
                                Disusun Oleh
                                <div class="d-block">
                                    <p
                                        class="badge badge-success text-left"
                                        style="white-space: normal"
                                    >
                                        {{ $surat_perintah_kerja->creator->name }}
                                    </p>
                                </div>
                            </div>
                            {{-- <div class="col-md-4">
                                Diperiksa Oleh
                                <div class="d-block">
                                    <p
                                        class="badge badge-success text-left"
                                        style="white-space: normal"
                                    >
                                        {{ $surat_perintah_kerja->creator->name }}
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                Disetujui Oleh
                                <div class="d-block">
                                    <p
                                        class="badge badge-success text-left"
                                        style="white-space: normal"
                                    >
                                        {{ $surat_perintah_kerja->creator->name }}
                                    </p>
                                </div>
                            </div> --}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="no_resi">Alat dan Bahan Kerja</label>
                        <table
                            class="table table-hover table-bordered w-100"
                            id="table-products"
                        >
                            <thead class="bg-hijau">
                                <tr>
                                    <th width="20%">Nama Produk</th>
                                    <th width="20%">Alat</th>
                                    <th width="30%">Bahan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($surat_perintah_kerja->permintaan_pengujian->items as $item)
                                    <tr>
                                        <td>
                                            <span>{{ $item->product->name ?? '' }}</span>
                                        </td>
                                        <td>
                                            <span>{{ $item->product->alat->pluck('nama')->join(', ') ?? '' }}</span>
                                        </td>
                                        <td>
                                            <span>{{ $item->product->bahan->pluck('nama')->join(', ') ?? '' }}</span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group">
                        <label for="no_resi">Menugaskan Pengujian Kepada</label>
                        <table
                            class="table table-hover table-bordered w-100"
                            id="table-sample"
                        >
                            <thead class="bg-hijau">
                                <tr>
                                    <th width="20%">Nama Pegawai</th>
                                    <th width="20%">NIP/NRP</th>
                                    <th width="30%">Jabatan</th>
                                    <th width="20%">Keterangan</th>
                                    <th width="10%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($surat_perintah_kerja->members as $member)
                                    <tr>
                                        <td>
                                            <span>{{ $member->user->name }}</span>
                                        </td>
                                        <td>
                                            <span>{{ $member->nip ?? '' }}</span>
                                        </td>
                                        <td>
                                            @foreach ($member->lab_positions as $position)
                                                <p>{{ $position->name }}</p>
                                            @endforeach
                                        </td>
                                        <td>
                                            <span></span>
                                        </td>
                                        <td></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>
                                            <select
                                                class="form-control custom-select user-list"
                                                name="pegawai[]"
                                            >
                                                <option value="">Pilih pegawai..</option>
                                                @foreach ($pegawai as $item)
                                                    <option value="{{ $item->pegawai->id }}">{{ $item->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <span
                                                type='text'
                                                class="nip"
                                            ></span>
                                        </td>
                                        <td>
                                            <span
                                                type='text'
                                                class="jabatan"
                                            ></span>
                                        </td>
                                        <td>
                                            <span
                                                type='text'
                                                class="keterangan"
                                                disabled
                                            ></span>
                                        </td>
                                        <td></td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                    @if ($surat_perintah_kerja->status === 'draft')
                        <div class="d-flex">
                            <button
                                class="btn btn-sm btn-primary"
                                id='add-more-test'
                            >Tambah Pegawai</button>
                        </div>

                        <div class="form-group">
                            <div class="float-right">
                                <button
                                    type="submit"
                                    class="btn btn-primary"
                                >Ajukan</button>
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script>
        $(document).ready(function() {
            $pegawais = JSON.parse('{!! $pegawai !!}')
            let $filteredPegawai = [...$pegawais];

            let counter = 0;
            let selectedPegawai = [];
            const $table = $('#table-sample').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                rowCallback: function(row, data, displayNum,
                    displayIndex, dataIndex) {
                    $(row).on('change', '.user-list',
                        function(e) {
                            const selectedID = $(this)
                                .val();
                            const selected = $pegawais.find(
                                f => f.pegawai.id === Number(
                                    selectedID))

                            selectedPegawai.push(selectedID)
                            $filteredPegawai = [...$filteredPegawai].filter(f => f.pegawai.id !==
                                Number(
                                    selectedID));

                            if ($filteredPegawai.length === 0) {
                                $addButton.hide();
                            }
                            if (selected) {
                                $(row).find('.nip')
                                    .text(selected.pegawai.nip)
                                $(row).find('.jabatan')
                                    .html(selected.pegawai.lab_positions.map(e =>
                                        `<p>${e.name}</p>`))
                                // $(row).find('.keterangan')
                                //     .val(pricePerUnit)
                            } else {
                                $(row).find('.nip')
                                    .text('')
                                $(row).find('.jabatan')
                                    .text('')
                            }
                        })

                    $(row).on('change', '.product_name', function(e) {
                        const product_name = $(this).val()
                        const product = productNames.find(f => f.name === product_name)
                        if (product) {
                            $productDesc.text(product.description);
                        }
                    });
                }
            });

            const $addButton = $('#add-more-test')
            $('.user-list').select2({
                theme: "bootstrap4",
                placeholder: 'Pilih pegawai..',
            });
            $table.on('draw.dt', function() {
                $('.user-list').select2({
                    theme: "bootstrap4",
                    placeholder: 'Pilih pegawai..',
                });
            });

            $('#table-sample tbody').on('click', '.item-remove',
                function() {
                    $table
                        .row($(this).parents('tr'))
                        .remove()
                        .draw();
                    const removedId = $(this).parents('tr').find('.user-list').find(':selected').val()
                    selectedPegawai = selectedPegawai.filter(f => Number(f) !== Number(removedId))
                    $filteredPegawai = [...$pegawais].filter(f => selectedPegawai.some(i => Number(i) !== f
                        .pegawai.id));
                    if ($filteredPegawai.length !== 0) {
                        $addButton.show();
                    }
                });

            $addButton.on('click', function(e) {
                e.preventDefault();
                // $form.removeClass('was-validated');
                $table.row.add([
                    `<select class="form-control custom-select user-list" name="pegawai[]">
                        <option value="">Pilih pegawai..</option>
                        ${$filteredPegawai.map(p => `<option value="${p.pegawai.id}">${p.name}</option>`)}
                    </select>`,
                    `<span type='text' class="nip"></span>`,
                    `<span type='text' class="jabatan"></span>`,
                    `<span type='text' class="keterangan" disabled></span>`,
                    `<div class='button-group'>
                            <input type='hidden' class="form-control" disabled name="pegawai[${counter}][id]" />
                            <a 
                                href="javascript:void(0)" 
                                class='btn btn-sm btn-danger item-remove' 
                                title='remove'>Hapus</a>
                        </div>`
                ]).draw(false);
            });

            $("#form-surat-perintah-kerja").validate({
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass("is-invalid");
                },
                errorElement: "em",
                errorClass: 'invalid-feedback',
                focusInvalid: true, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                errorPlacement: function(error, element) {
                    if (element.attr("type") == "radio") {
                        error.insertAfter(element.parents('div').find('.radio-list'));
                    } else {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                },
                success: function(label) {
                    label
                        .closest('.form-group').removeClass(
                            'has-error'); // set success class to the control group
                },
                rules: {
                    nomor: {
                        required: true,
                    },
                    tanggal_mulai: {
                        required: true,
                    },
                    tanggal_selesai: {
                        required: true,
                    },
                    'pegawai[]': {
                        required: true,
                    },
                },
                messages: {
                    tanggal_selesai: 'Tanggal selesai harus diisi',
                    tanggal_mulai: 'Tanggal mulai harus diisi',
                    nomor: 'Nomor harus diisi',
                    'pegawai[]': 'Pegawai harus diisi',
                },
                submitHandler: function(form) {
                    const formData = $("#form-surat-perintah-kerja").serializeArray();
                    swalWithBootstrapButtons.fire({
                        icon: 'info',
                        title: 'Apakah anda yakin?',
                        text: 'Pastikan data yang anda masukkan untuk membuat SPK sudah benar',
                        confirmButtonText: 'Ya, saya yakin!',
                        cancelButtonText: 'Batal',
                        showCancelButton: true,
                        showConfirmButton: true,
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-secondary mr-2',
                        },
                        preConfirm: () => {
                            return $.ajax({
                                url: route(
                                    'admin.permintaan-pengujian.surat-perintah-kerja.update',
                                    ['{{ $surat_perintah_kerja->id }}']),
                                dataType: 'json',
                                type: 'PUT',
                                data: formData,
                                error: function(error) {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                }
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            swalWithBootstrapButtons.fire({
                                icon: 'success',
                                title: 'Berhasil!',
                                text: "Benda uji berhasi disetujui",
                            }).then(() => {
                                window.location.href = route(
                                    'admin.permintaan-pengujian.surat-perintah-kerja.index'
                                );
                            })
                        }
                    })
                }
            });
        });
    </script>
@endpush
