@extends('layouts.adminlte')

@section('main')
    @include('admin.parts.breadcrumbs',
    ['judul' => [
    ['judul' => 'Permintaan Pengujian'],
    ['judul' => 'Kode Billing']
    ]])
    @include('admin.utils.alert')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Main content -->
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                            <h4>
                                <i class="fas fa-globe"></i> Balai BBSBG
                                <small class="float-right">Tanggal:
                                    {{ $pembayaran->permintaan_pengujian->tgl_permintaan }}
                                </small>
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>

                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            Yang mengajukan
                            <address>
                                <strong>{{ $pembayaran->permintaan_pengujian->nama_pemohon }}</strong><br>
                                {{ $pembayaran->permintaan_pengujian->alamat }}<br>
                                Phone: {{ $pembayaran->permintaan_pengujian->nomor }} <br>
                                Email: {{ $pembayaran->permintaan_pengujian->email }}
                            </address>
                        </div>

                        <!-- /.col -->
                        @if (!is_null($pembayaran->permintaan_pengujian->invoice))
                            <div class="col-sm-6 invoice-col text-right">
                                <b>Invoice
                                    #{{ $pembayaran->permintaan_pengujian->invoice->code }}</b><br>
                                <b>Order ID:</b>
                                {{ $pembayaran->permintaan_pengujian->invoice->order_code }}<br>
                            </div>
                        @endif
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Parameter Pengujian</th>
                                        <th>Nama Produk</th>
                                        <th>Tipe Produk</th>
                                        <th>Harga Satuan</th>
                                        <th class="text-center">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total = 0;
                                    @endphp
                                    @foreach ($pembayaran->permintaan_pengujian->items as $item)
                                        <tr>
                                            <td>{{ $item->layanan_uji_item->name }}
                                            </td>
                                            <td>{{ $item->product_name }}
                                            </td>
                                            <td>{{ $item->product_type }}
                                            </td>
                                            <td>{{ formatCurrency($item->layanan_uji_item->price) }}
                                            </td>
                                            <td class="text-center">
                                                {{ formatCurrency($item->layanan_uji_item->price * $item->product_count) }}
                                            </td>
                                        </tr>

                                        @php
                                            $total += $item->layanan_uji_item->price * $item->product_count;
                                        @endphp
                                    @endforeach

                                    <tr style="background-color: whitesmoke">
                                        <td
                                            colspan="4"
                                            class="text-bold"
                                        >
                                            Total
                                        </td>
                                        <td class="text-bold text-center">
                                            {{ formatCurrency($total) }}
                                            0
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    @can('upload_layanan_konfirmasi')
                        @if ($pembayaran->permintaan_pengujian->status == \App\Models\PermintaanPengujian::STATUS_KAJI_ULANG_APPROVED_BY_KABALAI)
                            <div class="form-group">
                                <label class="text-secondary">Form Konfirmasi</label>
                                <div>
                                    <button class="btn btn-primary btn-unggah-form">
                                        Unggah Form Layanan Konfirmasi
                                    </button>
                                </div>
                            </div>
                        @endif
                    @endcan
                    @if ($pembayaran->permintaan_pengujian->dokumen->where('type', 'dokumen_konfirmasi')->isNotEmpty())
                        <div class="form-group">
                            <label class="text-secondary">Form Konfirmasi</label>
                            <div>
                                <a
                                    href="{{ $pembayaran->permintaan_pengujian->dokumen->where('type', 'dokumen_konfirmasi')->last()->dokumen->getPath() }}"
                                    target="_blank"
                                >
                                    Dokumen Konfirmasi Layanan
                                </a>
                            </div>
                        </div>
                    @endif
                    @if ($pembayaran->permintaan_pengujian->dokumen->where('type', 'dokumen_konfirmasi_reply')->isNotEmpty())
                        <div class="form-group">
                            <label class="text-secondary">Form Konfirmasi Dari Client</label>
                            <div>
                                <a
                                    target="_blank"
                                    href="{{ $pembayaran->permintaan_pengujian->dokumen->where('type', 'dokumen_konfirmasi_reply')->last()->dokumen->getPath() }}"
                                >
                                    Dokumen Konfirmasi Layanan Client
                                </a>
                            </div>
                        </div>
                    @endif
                    @if ($pembayaran->permintaan_pengujian->client->group !== 1)
                        <div class="row">
                            <!-- accepted payments column -->
                            <div class="col-6">
                                <p class="font-weight-bold">Masukkan Kode Billing:
                                </p>
                                <div class="form-group">
                                    <input
                                        name="billing_code"
                                        class="form-control"
                                        readonly
                                        value="{{ $pembayaran->billing_code }}"
                                    />
                                </div>
                            </div>
                        </div>


                        @if ($pembayaran->status === \App\Models\PermintaanPengujian::STATUS_PAYMENT_RECEIVED || $pembayaran->status === \App\Models\PermintaanPengujian::STATUS_APPROVED)
                            <div class="row">
                                <!-- accepted payments column -->
                                <div class="col-6">
                                    <p class="font-weight-bold">Bukti Pembayaran
                                    </p>
                                    <div class="form-group">
                                        <a
                                            href="{{ route('admin.permintaan-pengujian.pembayaran.download', $pembayaran->id) }}"
                                            target="_blank"
                                            class="btn btn-primary btn-block btn-show"
                                        >Bukti Pembayaran</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-12">
                            <a
                                href="{{ route('admin.permintaan-pengujian.pembayaran.index') }}"
                                class="btn btn-secondary float-right"
                            >Kembali
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /.invoice -->
            </div><!-- /.col -->
        </div>
    </div>
@endsection

@if ($pembayaran->status === \App\Models\PermintaanPengujian::STATUS_PAYMENT_RECEIVED || $pembayaran->status === \App\Models\PermintaanPengujian::STATUS_APPROVED)
    @push('script')
        <script>
            $(document).ready(function() {
                const currentStatus = '{{ $pembayaran->status }}';
                const isSubmitted = currentStatus === 'payment-received';
                const payment = {!! $pembayaran->confirmation->last() !!}
                const paymentDownloadURL = '{{ $pembayaran->confirmation->last()->file->getPath() ?? '' }}'

                $('.btn-show').click(function(e) {
                    e.preventDefault();
                    swalWithBootstrapButtons.fire({
                        title: 'Bukti Konfirmasi Pembayaran',
                        confirmButtonText: 'Ya, setujui!',
                        cancelButtonText: 'Tolak',
                        showCancelButton: isSubmitted ? true : false,
                        showConfirmButton: isSubmitted ? true : false,
                        html: `
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td class="text-bold align-middle" style="width: 150px">Nama Bank</td>
                                    <td class="align-middle">${payment.bank_name}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold align-middle" style="width: 150px">Nomor Rekening</td>
                                    <td class="align-middle">${payment.account_number}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold align-middle" style="width: 150px">Nama Pemegang Rekening</td>
                                    <td class="align-middle">${payment.account_holder}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold align-middle" style="width: 150px">Nominal</td>
                                    <td class="align-middle">${payment.payment_amount}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold align-middle" style="width: 150px">Tanggal Pembayaran</td>
                                    <td class="align-middle">${payment.payment_date}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold align-middle" style="width: 150px">Bukti Pembayaran</td>
                                    <td class='align-middle'>
                                        <a href="${paymentDownloadURL}" target="_blank" class="btn btn-sm btn-info">Lihat Bukti Pembayaran</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    `,
                        preConfirm: () => {
                            return $.ajax({
                                url: '{{ route('admin.permintaan-pengujian.pembayaran.approve', $pembayaran->id) }}',
                                dataType: 'json',
                                type: 'POST',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                },
                                error: function(response) {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                }
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            swalWithBootstrapButtons.fire({
                                icon: 'success',
                                title: 'Berhasil disetujui!',
                                text: "Konfirmasi pembayaran berhasil disetujui",
                            }).then(() => {
                                window.location.href = route(
                                    'admin.permintaan-pengujian.pembayaran.index'
                                );
                            })
                        } else if (
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            swalWithBootstrapButtons.fire({
                                titleText: 'Alasan menolak',
                                text: "Masukkan alasan anda menolak pembayaran ini",
                                input: 'textarea',
                                inputAttributes: {
                                    autocapitalize: 'off',
                                    name: 'message'
                                },
                                showCancelButton: true,
                                confirmButtonText: 'Yakin, menolak!',
                                cancelButtonText: 'Batalkan',
                                customClass: {
                                    confirmButton: 'btn btn-danger',
                                    cancelButton: 'btn btn-secondary mr-2',
                                },
                                preConfirm: (message) => {
                                    return $.ajax({
                                        url: '{{ route('admin.permintaan-pengujian.pembayaran.reject', $pembayaran->id) }}',
                                        dataType: 'json',
                                        type: 'POST',
                                        data: {
                                            _token: '{{ csrf_token() }}',
                                            message
                                        },
                                        error: function(response) {
                                            Swal.showValidationMessage(
                                                `Request failed: ${error}`
                                            )
                                        }
                                    });
                                },
                                allowOutsideClick: () => !Swal.isLoading()
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    swalWithBootstrapButtons.fire({
                                        icon: 'success',
                                        title: 'Berhasil menolak!',
                                        text: "Pembayaran berhasil ditolak",
                                    }).then(() => {
                                        window.location.href = route(
                                            'admin.permintaan-pengujian.pembayaran.index'
                                        );
                                    })
                                }
                            })
                        }
                    })
                })
            });
        </script>
    @endpush
@endif
