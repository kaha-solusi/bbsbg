@extends('layouts.adminlte')

@section('main')
    @include('admin.parts.breadcrumbs',
    ['judul' => [
    ['judul' => 'Permintaan Pengujian'],
    ['judul' => 'Kode Billing']
    ]])
    @include('admin.utils.alert')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Main content -->
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                            <h4>
                                <i class="fas fa-globe"></i> Balai BBSBG
                                <small class="float-right">Tanggal:
                                    {{ $pembayaran->permintaan_pengujian->tgl_permintaan }}
                                </small>
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>

                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            Yang mengajukan
                            <address>
                                <strong>{{ $pembayaran->permintaan_pengujian->nama_pemohon }}</strong><br>
                                {{ $pembayaran->permintaan_pengujian->company_name ?? $pembayaran->permintaan_pengujian->alamat }}<br>
                                {{ $pembayaran->permintaan_pengujian->alamat }}<br>
                                Nomor Kontak: {{ $pembayaran->permintaan_pengujian->nomor }} <br>
                                Email: {{ $pembayaran->permintaan_pengujian->email }}
                            </address>
                        </div>

                        <!-- /.col -->
                        @if (!is_null($pembayaran->permintaan_pengujian->invoice))
                            <div class="col-sm-6 invoice-col text-right">
                                <b>Invoice
                                    #{{ $pembayaran->permintaan_pengujian->invoice->code }}</b><br>
                                <b>Order ID:</b>
                                {{ $pembayaran->permintaan_pengujian->invoice->order_code }}<br>
                            </div>
                        @endif
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Parameter Pengujian</th>
                                        <th>Nama Produk</th>
                                        <th>Tipe Produk</th>
                                        <th>Harga Satuan</th>
                                        <th class="text-center">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total = 0;
                                    @endphp
                                    @foreach ($pembayaran->permintaan_pengujian->items as $item)
                                        <tr>
                                            <td>{{ $item->layanan_uji_item->name }}
                                            </td>
                                            <td>{{ $item->product_name }}
                                            </td>
                                            <td>{{ $item->product_type }}
                                            </td>
                                            <td>{{ formatCurrency($item->layanan_uji_item->price) }}
                                            </td>
                                            <td class="text-center">
                                                {{ formatCurrency($item->layanan_uji_item->price * $item->product_count) }}
                                            </td>
                                        </tr>

                                        @php
                                            $total += $item->layanan_uji_item->price * $item->product_count;
                                        @endphp
                                    @endforeach

                                    <tr style="background-color: whitesmoke">
                                        <td
                                            colspan="4"
                                            class="text-bold"
                                        >
                                            Total
                                        </td>
                                        <td class="text-bold text-center">
                                            {{ formatCurrency($total) }}
                                            0
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    @can('upload_layanan_konfirmasi')
                        @if ($pembayaran->permintaan_pengujian->status == \App\Models\PermintaanPengujian::STATUS_KAJI_ULANG_APPROVED_BY_KABALAI)
                            <div class="form-group">
                                <label class="text-secondary">Form Konfirmasi</label>
                                <div>
                                    <button class="btn btn-primary btn-unggah-form">
                                        Unggah Form Layanan Konfirmasi
                                    </button>
                                </div>
                            </div>
                        @endif
                    @endcan
                    @if ($pembayaran->permintaan_pengujian->dokumen->where('type', 'dokumen_konfirmasi')->isNotEmpty())
                        <div class="form-group">
                            <label class="text-secondary">Form Konfirmasi</label>
                            <div>
                                <a
                                    href="/{{ $pembayaran->permintaan_pengujian->dokumen->where('type', 'dokumen_konfirmasi')->last()->dokumen->path }}"
                                    target="_blank"
                                >
                                    Dokumen Konfirmasi Layanan
                                </a>
                            </div>
                        </div>
                    @endif

                    @if ($pembayaran->permintaan_pengujian->dokumen->where('type', 'dokumen_konfirmasi_reply')->isNotEmpty())
                        <div class="form-group">
                            <label class="text-secondary">Form Konfirmasi Dari Client</label>
                            <div>
                                <a
                                    class="btn btn-sm btn-primary download-reply"
                                    href="/{{ $pembayaran->permintaan_pengujian->dokumen->where('type', 'dokumen_konfirmasi_reply')->last()->dokumen->path }}"
                                >
                                    Dokumen Konfirmasi Layanan Client
                                </a>
                            </div>
                        </div>

                        <form
                            method="POST"
                            action="{{ route('admin.permintaan-pengujian.pembayaran.update', $pembayaran->id) }}"
                            enctype="multipart/form-data"
                        >
                            @csrf
                            @method('PUT')

                            <!-- this row will not appear when printing -->
                            @if ($pembayaran->permintaan_pengujian->status === 'form-konfirmasi-reply-approved')
                                @can('add_payments')

                                    @if ($pembayaran->permintaan_pengujian->client->group !== 1)
                                        <div class="row">
                                            <div class="col-6">
                                                <p class="font-weight-bold">Cara Pembayaran
                                                </p>
                                                <div class="form-group">
                                                    <input
                                                        name="how_to_pay"
                                                        class="form-control"
                                                        required
                                                        type="file"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- accepted payments column -->
                                            <div class="col-6">
                                                <p class="font-weight-bold">Masukkan Kode Billing:
                                                </p>
                                                <div class="form-group">
                                                    <input
                                                        name="billing_code"
                                                        class="form-control"
                                                        required
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.row -->
                                    @endif
                                    <div class="row no-print">
                                        <div class="col-12">

                                            <button
                                                type="submit"
                                                class="btn btn-success float-right"
                                            ><i class="far fa-credit-card"></i> Kirim Kode Billing
                                            </button>
                                        </div>
                                    </div>
                                @endcan
                            @endif
                        </form>
                    @endif

                </div>
                <!-- /.invoice -->
            </div><!-- /.col -->
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('.btn-unggah-form').click(function(e) {
                Swal.fire({
                    titleText: 'Unggah Form Layanan Konfirmasi',
                    text: "Form layanan konfirmasi akan dikirimkan ke pelanggan",
                    input: 'file',
                    inputAttributes: {
                        class: 'form-control',
                        accept: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                    },
                    reverseButtons: true,
                    showCancelButton: true,
                    showLoaderOnConfirm: true,
                    buttonsStyling: false,
                    backdrop: true,
                    confirmButtonText: 'Unggah',
                    cancelButtonText: 'Batalkan',
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-secondary mr-2',
                    },
                    preConfirm: (file) => {
                        const formData = new FormData();
                        formData.append('file', file);

                        return $.ajax({
                            url: `${route(
                        'admin.permintaan-pengujian.unggah-form-layanan-konfirmasi',
                        ['{{ $pembayaran->permintaan_pengujian->id }}'])}?_token=${$('meta[name="csrf-token"]').attr('content')}`,
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            error: function(response) {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire({
                            buttonsStyling: false,
                            icon: 'success',
                            title: 'Berhasil!',
                            text: "Form konfirmasi layanan berhasil diunggah",
                            customClass: {
                                confirmButton: 'btn btn-primary',
                            },
                        }).then(() => {
                            window.location.href = route(
                                'admin.permintaan-pengujian.pembayaran.index'
                            );
                        })
                    }
                })
            });

            $('.download-reply').click(function(e) {
                e.preventDefault();
                const url = $(this).attr('href')
                const status = '{{ $pembayaran->permintaan_pengujian->status }}'
                const isActionEnabled = status ===
                    '{{ \App\Models\PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_APPROVED }}'

                Swal.fire({
                    titleText: 'Dokumen konfirmasi layanan',
                    // text: "Apakah anda yakin ingin menyetujui dokumen konfirmasi layanan?",
                    reverseButtons: true,
                    showCancelButton: isActionEnabled ? false : true,
                    showConfirmButton: isActionEnabled ? false : true,
                    showLoaderOnConfirm: true,
                    buttonsStyling: false,
                    backdrop: true,
                    confirmButtonText: 'Ya, setuju!',
                    cancelButtonText: 'Tidak, batalkan!',
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-secondary mr-2',
                    },
                    html: `
                            <a href="${url}" target="_blank" class="btn btn-info btn-block btn-download-reply">Unduh</a>
                        `,
                    preConfirm: () => {
                        return $.ajax({
                            url: `${route(
                                    'admin.permintaan-pengujian.document-approve',
                                    ['{{ $pembayaran->permintaan_pengujian->id }}'])}?_token=${$('meta[name="csrf-token"]').attr('content')}`,
                            type: 'POST',
                            error: function(response) {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire({
                            buttonsStyling: false,
                            icon: 'success',
                            title: 'Berhasil!',
                            text: "Form konfirmasi layanan berhasil disetujui",
                            customClass: {
                                confirmButton: 'btn btn-primary',
                            },
                        }).then(() => {
                            window.location.href = route(
                                'admin.permintaan-pengujian.pembayaran.index'
                            );
                        })
                    }
                })
            });
        })
    </script>
@endpush
