@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
        'judul' => [
            [
                'judul' => 'Permintaan Pengujian',
                'link' => route('admin.permintaan-pengujian.index'),
            ],
            [
                'judul' => 'Riwayat Permintaan Pengujian',
                'link' => route('admin.permintaan-pengujian.histories.index', $order_id),
            ],
            ['judul' => 'Tambah Riwayat Permintaan Pengujian'],
        ],
    ])
    <div class="container">
        <div class="card card-block d-flex">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <div class="float-left">
                            Tambah Riwayat Permintaan Pengujian
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div
                        class="col-md-12"
                        x-data="{ step: 'step_1', steps: {{ json_encode($steps) }} }"
                    >
                        <form
                            method='POST'
                            action="{{ route('admin.permintaan-pengujian.histories.store', $order_id) }}"
                        >
                            @csrf

                            {{-- <div class="form-group">
                                <label for="message">Pesan</label>
                                <textarea
                                    required
                                    name='message'
                                    class="form-control"
                                ></textarea>
                            </div> --}}

                            <div class="form-group">
                                <label for="user_id">PIC</label>
                                <select
                                    required
                                    class="form-control"
                                    name='user_id'
                                >
                                    <option value="">Pilih PIC</option>
                                    @foreach ($users as $user)
                                        <option value='{{ $user->id }}'>{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="step">Tahapan</label>
                                <select
                                    required
                                    class="form-control"
                                    name='step'
                                    x-on:change="step = $event.target.value"
                                >
                                    @foreach (array_keys($steps) as $step)
                                        <option value='{{ $step }}'>{{ __($step) }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="message">Pesan</label>
                                <select
                                    required
                                    class="form-control"
                                    name='message'
                                >
                                    <option
                                        value='approved'
                                        x-text="steps[step].approved"
                                    ></option>
                                    <option
                                        x-show="steps[step].rejected !== ''"
                                        value='rejected'
                                        x-text="steps[step].rejected"
                                    ></option>
                                </select>
                            </div>

                            <div class="form-group float-right">
                                <button
                                    type="submit"
                                    class='btn btn-primary'
                                >Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    {!! JsValidator::formRequest('App\Http\Requests\PermintaanPengujianHistoryStoreRequest') !!}
@endpush
