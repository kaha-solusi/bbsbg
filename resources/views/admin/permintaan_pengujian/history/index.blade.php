@extends('layouts.adminlte')

@section('main')
    @include('admin.parts.breadcrumbs', [
        'judul' => [
            [
                'judul' => 'Permintaan Pengujian',
                'link' => route('admin.permintaan-pengujian.index'),
            ],
            [
                'judul' => 'Riwayat Permintaan Pengujian',
            ],
        ],
    ])
    @include('admin.utils.alert')
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col">
                                <div class="float-left">
                                    Riwayat Permintaan Pengujian
                                </div>
                            </div>
                            <div class="col-auto">
                                <a
                                    href="{{ route('admin.permintaan-pengujian.histories.create', $order_id) }}"
                                    class='btn btn-success text-white'
                                >Create New</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush
