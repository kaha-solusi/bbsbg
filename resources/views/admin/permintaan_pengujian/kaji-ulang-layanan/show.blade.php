@include('admin.parts.breadcrumbs',
['judul' => [
['judul' => 'Permintaan Pengujian'],
['judul' => 'ss']
]])
<div class="container">
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <h5>Kaji Ulang Layanan</h5>
                </div>
                <div class="card-body">
                    <form
                        action="{{ route('admin.permintaan-pengujian.update', $pengujian->id) }}"
                        method="POST"
                        id="form-kaji-ulang"
                        enctype="multipart/form-data"
                    >
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label
                                        for=""
                                        class="text-secondary"
                                    >Data Pesanan</label>
                                    <style>
                                        .table-detail-pesanan tr td:first-child {
                                            width: 200px
                                        }

                                    </style>
                                    <table class="table table-bordered table-detail-pesanan">
                                        <tbody>
                                            <tr>
                                                <td class="text-bold">No Permintaan</td>
                                                <td>
                                                    <input
                                                        type="text"
                                                        class="form-control"
                                                        value="{{ $pengujian->nomor_permintaan }}"
                                                        readonly
                                                    >
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Tanggal Permintaan</td>
                                                <td>{{ $pengujian->tgl_permintaan }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Tanggal Pelayanan</td>
                                                <td>
                                                    <input
                                                        type="date"
                                                        class="form-control"
                                                        value="{{ $pengujian->tanggal_pelayanan }}"
                                                        readonly
                                                    />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Laboratorium</td>
                                                <td>{{ $pengujian->master_layanan_uji->pelayanan->nama }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Jenis Pengujian</td>
                                                <td>{{ $pengujian->master_layanan_uji->name }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Nama Institusi</td>
                                                <td>{{ $pengujian->client->nama_perusahaan }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Alamat</td>
                                                <td>{{ $pengujian->alamat }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Nama Kontak</td>
                                                <td>{{ $pengujian->nama_pemohon }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">No HP Kontak</td>
                                                <td>{{ $pengujian->nomor }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Email</td>
                                                <td>{{ $pengujian->email }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            {{-- Detail Order --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label
                                        for=""
                                        class="text-secondary"
                                    >Order Layanan</label>

                                    <table class="table table-bordered">
                                        <thead class="thead-custom">
                                            <tr>
                                                <th>No</th>
                                                <th class="text-center">Nama Layanan</th>
                                                <th class="text-center">Nama Produk</th>
                                                <th class="text-center">Harga Satuan</th>
                                                <th class="text-center">Kuantiti</th>
                                                <th class="text-center">Jumlah</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @php
                                                $total = 0;
                                            @endphp
                                            @foreach ($pengujian->items as $key => $item)
                                                <tr>
                                                    <td class="text-center">{{ ++$key }}</td>
                                                    <td class="text-center">
                                                        {{ $item->layanan_uji_item->name }}
                                                    </td>
                                                    <td class="text-center">{{ $item->product_name }}</td>
                                                    <td class="text-center">
                                                        {{ formatCurrency($item->layanan_uji_item->price) }}</td>
                                                    <td class="text-center">{{ $item->product_count }}</td>
                                                    <td class="text-center">
                                                        {{ formatCurrency($item->layanan_uji_item->price * $item->product_count) }}
                                                    </td>

                                                    @php
                                                        $total += $item->layanan_uji_item->price * $item->product_count;
                                                    @endphp
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td
                                                    class="text-center text-bold"
                                                    colspan="5"
                                                >Total</td>
                                                <td class="text-center">{{ formatCurrency($total) }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{-- .Detail Order --}}

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label
                                        for=""
                                        class="text-secondary"
                                    >Kaji Ulang Permintaan</label>
                                    <style>
                                        .table-detail-pesanan tr td:first-child {
                                            width: 200px
                                        }

                                    </style>
                                    <table class="table table-bordered table-detail-pesanan">
                                        <thead class="table-custom">
                                            <tr>
                                                <th>Parameter</th>
                                                <th width="20%">Hasil</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($pengujian->kaji_ulang->parameter as $key => $item)
                                                <tr>
                                                    <td class="text-bold align-middle">
                                                        {{ ucwords(preg_replace('/_/', ' ', $key)) }}</td>
                                                    </td>
                                                    <td class="align-middle">
                                                        <select
                                                            class="form-control"
                                                            disabled
                                                        >
                                                            @if ($key !== 'kesimpulan_kajian_ulang')
                                                                <option
                                                                    value="1"
                                                                    @if ($item['value'] === 1) selected @endif
                                                                >Ada</option>
                                                                <option
                                                                    value="0"
                                                                    @if ($item['value'] === 1) selected @endif
                                                                >Tidak</option>
                                                            @else
                                                                <option
                                                                    value="1"
                                                                    @if ($item['value'] === 1) selected @endif
                                                                >Dapat Dikerjakan</option>
                                                                <option
                                                                    value="0"
                                                                    @if ($item['value'] === 1) selected @endif
                                                                >Tidak Dapat Dikerjakan</option>
                                                            @endif
                                                        </select>
                                                    </td>
                                                    <td class="align-middle">
                                                        {{ $item['keterangan'] }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div x-data="{subkontraktor: {{ $pengujian->kaji_ulang->subkontraktor }}}">
                                    <div class="form-group">
                                        <label>Subkontraktor</label>
                                        <select
                                            class="form-control"
                                            x-on:change="subkontraktor = $event.target.value"
                                            disabled
                                        >
                                            <option
                                                value="1"
                                                @if ($pengujian->kaji_ulang->subkontraktor === 1) selected @endif
                                            >Ya</option>
                                            <option
                                                value="0"
                                                @if ($pengujian->kaji_ulang->subkontraktor === 0) selected @endif
                                            >Tidak</option>
                                        </select>
                                    </div>

                                    <template x-if="subkontraktor == 1">
                                        <div>
                                            <div
                                                class="form-group"
                                                x-transition
                                            >
                                                <label>Nama Subkontraktor</label>
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    placeholder="Nama Subkontraktor"
                                                    value="{{ $pengujian->kaji_ulang->nama_subkontraktor }}"
                                                    readonly
                                                >
                                            </div>
                                            <div
                                                class="form-group"
                                                x-transition
                                            >
                                                <label>Alasan</label>
                                                <textarea
                                                    class="form-control"
                                                    placeholder="Alasan mensubkontraktor"
                                                    readonly
                                                >{{ $pengujian->kaji_ulang->keterangan }}</textarea>
                                            </div>
                                        </div>
                                    </template>

                                </div>

                                <div class="form-group">
                                    <label>Durasi</label>
                                    <div class="input-group">
                                        <input
                                            type="number"
                                            class="form-control"
                                            name="durasi"
                                            value="{{ $pengujian->kaji_ulang->durasi }}"
                                            readonly
                                        >
                                        <div class="input-group-append">
                                            <span class="input-group-text">Hari</span>
                                        </div>
                                    </div>
                                </div>

                                <input
                                    type="hidden"
                                    name="status"
                                    value="{{ $pengujian->status ?? 'draft' }}"
                                >
                                @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_KAJI_ULANG_SUBMITTED)
                                    <div class="form-group">
                                        <div class="float-right">
                                            <a
                                                href="#"
                                                class="btn btn-danger"
                                                id="rejectModalBtn"
                                            >Tolak</a>
                                            <button
                                                type="submit"
                                                class="btn btn-success"
                                            >Setujui</button>
                                        </div>
                                    </div>
                                @endif

                                @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_KAJI_ULANG_APPROVED)
                                    <div class="form-group">
                                        <label for="dokumenKonfirmasiLayanan">Dokumen Konfirmasi Layanan</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input
                                                    type="file"
                                                    class="custom-file-input"
                                                    id="dokumenKonfirmasiLayanan"
                                                    name="dokumen"
                                                >
                                                <label
                                                    class="custom-file-label"
                                                    for="dokumenKonfirmasiLayanan"
                                                >Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="float-right">
                                            <button
                                                type="submit"
                                                class="btn btn-success"
                                            >Simpan</button>
                                        </div>
                                    </div>
                                @endif

                                @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_FORM_CONFIRMATION_SUBMITTED)
                                    <div class="form-group">
                                        <label for="dokumenKonfirmasiLayanan">Dokumen Konfirmasi Layanan</label>
                                        <div>
                                            <a
                                                href="/"
                                                target="_blank"
                                            >Dokumen Konfirmasi Layanan</a>
                                        </div>
                                    </div>
                                @endif

                                {{-- @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_REJECTED)
                                        <div class="form-group">
                                            <label for="dokumenKonfirmasiLayanan">Dokumen Persetujuan
                                                Pelanggan</label>
                                            <div>
                                                <a
                                                    href="/"
                                                    target="_blank"
                                                >Dokumen Konfirmasi Layanan</a>
                                            </div>
                                        </div>
                                    @endif --}}

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script
        defer
        src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"
    ></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_KAJI_ULANG_SUBMITTED)
        <script>
            $(document).ready(function() {
                const url = '{{ route('admin.permintaan-pengujian.update', $pengujian->id) }}'
                const indexUrl = '{{ route('admin.permintaan-pengujian.index') }}'
                $('#form-kaji-ulang').on('submit', function(e) {
                    e.preventDefault();
                    Swal.fire({
                        title: 'Yakin menyetujui kaji ulang layanan ini?',
                        text: "Pastikan data yang anda masukkan sudah benar!",
                        icon: 'warning',
                        reverseButtons: true,
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonText: 'Ya, Setujui!',
                        cancelButtonText: 'Tidak, batalkan',
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-secondary mr-2',
                        },
                        preConfirm: () => {
                            return fetch(
                                    `${url}?_token=${$('input[name="_token"]').val()}&status=approved`, {
                                        method: 'PUT',
                                        header: {
                                            "Accept": "application/json",
                                            'Content-Type': 'application/json',
                                        }
                                    })
                                .catch(error => {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                })
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            Swal.fire({
                                buttonsStyling: false,
                                icon: 'success',
                                title: 'Berhasil!',
                                text: "Permintaan kaji ulang layanan berhasil disetujui",
                                customClass: {
                                    confirmButton: 'btn btn-primary',
                                },
                            }).then(() => {
                                window.location.href = indexUrl
                            })
                        }
                    })
                })


                $('#rejectModalBtn').on('click', function() {
                    Swal.fire({
                        titleText: 'Alasan menolak',
                        text: "Masukkan alasan anda menolak permintaan kaji ulang layanan ini",
                        input: 'textarea',
                        inputAttributes: {
                            autocapitalize: 'off',
                            name: 'message'
                        },
                        reverseButtons: true,
                        showCancelButton: true,
                        showLoaderOnConfirm: true,
                        buttonsStyling: false,
                        confirmButtonText: 'Tolak',
                        cancelButtonText: 'Batalkan',
                        customClass: {
                            confirmButton: 'btn btn-danger',
                            cancelButton: 'btn btn-secondary mr-2',
                        },
                        preConfirm: (message) => {
                            return fetch(
                                    `${url}?_token=${$('input[name="_token"]').val()}&message=${message}&status=rejected`, {
                                        method: 'PUT',
                                        header: {
                                            "Accept": "application/json",
                                            'Content-Type': 'application/json',
                                        }
                                    })
                                .catch(error => {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                })
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            Swal.fire({
                                buttonsStyling: false,
                                icon: 'success',
                                title: 'Berhasil menolak!',
                                text: "Permintaan kaji ulang layanan berhasil ditolak",
                                customClass: {
                                    confirmButton: 'btn btn-primary',
                                },
                            }).then(() => {
                                window.location.href = indexUrl
                            })
                        }
                    })
                })

            })
        </script>
    @endif
@endpush
