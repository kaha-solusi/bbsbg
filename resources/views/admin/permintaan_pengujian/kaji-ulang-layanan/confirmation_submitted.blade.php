@include('admin.parts.breadcrumbs',
['judul' => [
['judul' => 'Permintaan Pengujian'],
['judul' => 'ss']
]])
<div class="container">
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <h5>Konfirmasi Layanan</h5>
                </div>
                <div class="card-body">
                    <form
                        action="{{ route('admin.permintaan-pengujian.update', $pengujian->id) }}"
                        method="POST"
                        id="form-kaji-ulang"
                        enctype="multipart/form-data"
                    >
                        @csrf
                        @method('PUT')

                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td class="text-bold w-25">Dokumen Konfirmasi Layanan</td>
                                    <td>
                                        <a
                                            href="/{{ $pengujian->dokumen->where('type', 'dokumen_konfirmasi')->last()->dokumen->path }}"
                                            target="_blank"
                                        >Dokumen Konfirmasi Layanan</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="text-bold w-25">Dokumen Balasan Pelanggan</td>
                                    <td>
                                        @if ($pengujian->dokumen->where('type', 'dokumen_konfirmasi_reply')->last()->dokumen->path ?? null)
                                            <a
                                                href="/{{ $pengujian->dokumen->where('type', 'dokumen_konfirmasi')->last()->dokumen->path }}"
                                                target="_blank"
                                            >Dokumen Balasan Pelanggan</a>
                                        @else
                                            <span class="badge badge-info">Belum ada</span>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_SUBMITTED)
                            <div class="form-group">
                                <div class="float-right">
                                    <button
                                        type="submit"
                                        class="btn btn-success"
                                    >
                                        Lanjutkan ke Pembayaran
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="form-group">
                                <div class="float-right">
                                    <button
                                        type="submit"
                                        class="btn btn-secondary"
                                    >
                                        Kembali
                                    </button>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script
        defer
        src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"
    ></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_KAJI_ULANG_SUBMITTED)
        <script>
            $(document).ready(function() {
                const url = '{{ route('admin.permintaan-pengujian.update', $pengujian->id) }}'
                const indexUrl = '{{ route('admin.permintaan-pengujian.index') }}'
                $('#form-kaji-ulang').on('submit', function(e) {
                    e.preventDefault();
                    Swal.fire({
                        title: 'Yakin menyetujui kaji ulang layanan ini?',
                        text: "Pastikan data yang anda masukkan sudah benar!",
                        icon: 'warning',
                        reverseButtons: true,
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonText: 'Ya, Setujui!',
                        cancelButtonText: 'Tidak, batalkan',
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-secondary mr-2',
                        },
                        preConfirm: () => {
                            return fetch(
                                    `${url}?_token=${$('input[name="_token"]').val()}&status=approved`, {
                                        method: 'PUT',
                                        header: {
                                            "Accept": "application/json",
                                            'Content-Type': 'application/json',
                                        }
                                    })
                                .catch(error => {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                })
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            Swal.fire({
                                buttonsStyling: false,
                                icon: 'success',
                                title: 'Berhasil!',
                                text: "Permintaan kaji ulang layanan berhasil disetujui",
                                customClass: {
                                    confirmButton: 'btn btn-primary',
                                },
                            }).then(() => {
                                window.location.href = indexUrl
                            })
                        }
                    })
                })


                $('#rejectModalBtn').on('click', function() {
                    Swal.fire({
                        titleText: 'Alasan menolak',
                        text: "Masukkan alasan anda menolak permintaan kaji ulang layanan ini",
                        input: 'textarea',
                        inputAttributes: {
                            autocapitalize: 'off',
                            name: 'message'
                        },
                        reverseButtons: true,
                        showCancelButton: true,
                        showLoaderOnConfirm: true,
                        buttonsStyling: false,
                        confirmButtonText: 'Tolak',
                        cancelButtonText: 'Batalkan',
                        customClass: {
                            confirmButton: 'btn btn-danger',
                            cancelButton: 'btn btn-secondary mr-2',
                        },
                        preConfirm: (message) => {
                            return fetch(
                                    `${url}?_token=${$('input[name="_token"]').val()}&message=${message}&status=rejected`, {
                                        method: 'PUT',
                                        header: {
                                            "Accept": "application/json",
                                            'Content-Type': 'application/json',
                                        }
                                    })
                                .catch(error => {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                })
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            Swal.fire({
                                buttonsStyling: false,
                                icon: 'success',
                                title: 'Berhasil menolak!',
                                text: "Permintaan kaji ulang layanan berhasil ditolak",
                                customClass: {
                                    confirmButton: 'btn btn-primary',
                                },
                            }).then(() => {
                                window.location.href = indexUrl
                            })
                        }
                    })
                })

            })
        </script>
    @endif
@endpush
