@include('admin.parts.breadcrumbs',
['judul' => [
['judul' => 'Permintaan Pengujian'],
['judul' => 'ss']
]])
<div class="container">
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <h5>Kaji Ulang Layanan</h5>
                </div>
                <div class="card-body">
                    <form
                        action="{{ route('admin.permintaan-pengujian.update', $pengujian->id) }}"
                        method="POST"
                        id="form-kaji-ulang"
                        novalidate
                    >
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label
                                        for=""
                                        class="text-secondary"
                                    >Data Pesanan</label>
                                    <style>
                                        .table-detail-pesanan tr td:first-child {
                                            width: 200px
                                        }

                                    </style>

                                    <table class="table table-bordered table-detail-pesanan">
                                        <tbody>
                                            <tr>
                                                <td class="text-bold">No Permintaan</td>
                                                <td>
                                                    <div
                                                        x-data="{ editable: false }"
                                                        class="input-group"
                                                    >
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="nomor_permintaan"
                                                            x-bind:readonly="!editable"
                                                            value="{{ old('nomor_permintaan', $pengujian->nomor_permintaan) }}"
                                                        >
                                                        <span class="input-group-append">
                                                            <button
                                                                type="button"
                                                                class="btn btn-info btn-flat"
                                                                x-on:click="editable = !editable"
                                                                x-text="editable ? 'Simpan' : 'Ubah'"
                                                            >Ubah</button>
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Tanggal Permintaan</td>
                                                <td>{{ $pengujian->tgl_permintaan }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Tanggal Pelayanan</td>
                                                <td>
                                                    <input
                                                        type="date"
                                                        class="form-control"
                                                        name="tanggal_pelayanan"
                                                        required
                                                    />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Laboratorium</td>
                                                <td>{{ $pengujian->master_layanan_uji->pelayanan->nama }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Jenis Pengujian</td>
                                                <td>{{ $pengujian->master_layanan_uji->name }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Nama Institusi</td>
                                                <td>{{ $pengujian->client->nama_perusahaan }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Alamat</td>
                                                <td>{{ $pengujian->alamat }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Nama Kontak</td>
                                                <td>{{ $pengujian->nama_pemohon }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">No HP Kontak</td>
                                                <td>{{ $pengujian->nomor }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Email</td>
                                                <td>{{ $pengujian->email }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            {{-- Detail Order --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label
                                        for=""
                                        class="text-secondary"
                                    >Order Layanan</label>

                                    <table class="table table-bordered">
                                        <thead class="thead-custom">
                                            <tr>
                                                <th>No</th>
                                                <th class="text-center">Nama Layanan</th>
                                                <th class="text-center">Nama Produk</th>
                                                <th class="text-center">Harga Satuan</th>
                                                <th class="text-center">Kuantiti</th>
                                                <th class="text-center">Jumlah</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @php
                                                $total = 0;
                                            @endphp
                                            @foreach ($pengujian->items as $key => $item)
                                                <tr>
                                                    <td class="text-center">{{ ++$key }}</td>
                                                    <td class="text-center">{{ $item->layanan_uji_item->name }}
                                                    </td>
                                                    <td class="text-center">{{ $item->product_name }}</td>
                                                    <td class="text-center">
                                                        {{ formatCurrency($item->layanan_uji_item->price) }}</td>
                                                    <td class="text-center">{{ $item->product_count }}</td>
                                                    <td class="text-center">
                                                        {{ formatCurrency($item->layanan_uji_item->price * $item->product_count) }}
                                                    </td>

                                                    @php
                                                        $total += $item->layanan_uji_item->price * $item->product_count;
                                                    @endphp
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td
                                                    class="text-center text-bold"
                                                    colspan="5"
                                                >Total</td>
                                                <td class="text-center">{{ formatCurrency($total) }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{-- .Detail Order --}}

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label
                                        for=""
                                        class="text-secondary"
                                    >Kaji Ulang Permintaan</label>
                                    <style>
                                        .table-detail-pesanan tr td:first-child {
                                            width: 200px
                                        }

                                    </style>
                                    <table class="table table-bordered table-detail-pesanan">
                                        <thead class="table-custom">
                                            <tr>
                                                <th>Parameter</th>
                                                <th width="20%">Hasil</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-bold align-middle">Ketersediaan anggaran operasional
                                                </td>
                                                <td class="align-middle">
                                                    <select
                                                        class="form-control"
                                                        name="parameters[ketersediaan_anggaran_operasional][value]"
                                                    >
                                                        <option value="1">Ada</option>
                                                        <option value="0">Tidak</option>
                                                    </select>
                                                </td>
                                                <td class="align-middle">
                                                    <textarea
                                                        class="form-control"
                                                        name="parameters[ketersediaan_anggaran_operasional][keterangan]"
                                                    ></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold align-middle">Kesesuaian Lingkup Layanan</td>
                                                <td class="align-middle">
                                                    <select
                                                        class="form-control"
                                                        name="parameters[kesesuaian_lingkup_layanan][value]"
                                                    >
                                                        <option value="1">Sesuai</option>
                                                        <option value="0">Tidak</option>
                                                    </select>
                                                </td>
                                                <td class="align-middle">
                                                    <textarea
                                                        class="form-control"
                                                        name="parameters[kesesuaian_lingkup_layanan][keterangan]"
                                                    ></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold align-middle">Ketersediaan Personil</td>
                                                <td class="align-middle">
                                                    <select
                                                        class="form-control"
                                                        name="parameters[ketersediaan_personil][value]"
                                                    >
                                                        <option value="1">Ada</option>
                                                        <option value="0">Tidak</option>
                                                    </select>
                                                </td>
                                                <td class="align-middle">
                                                    <textarea
                                                        class="form-control"
                                                        name="parameters[ketersediaan_personil][keterangan]"
                                                    ></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold align-middle">Ketersediaan Material</td>
                                                <td class="align-middle">
                                                    <select
                                                        class="form-control"
                                                        name="parameters[ketersediaan_material][value]"
                                                    >
                                                        <option value="1">Ada</option>
                                                        <option value="0">Tidak</option>
                                                    </select>
                                                </td>
                                                <td class="align-middle">
                                                    <textarea
                                                        class="form-control"
                                                        name="parameters[ketersediaan_material][keterangan]"
                                                    ></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold align-middle">Ketersediaan Peralatan</td>
                                                <td class="align-middle">
                                                    <select
                                                        class="form-control"
                                                        name="parameters[ketersediaan_peralatan][value]"
                                                    >
                                                        <option value="1">Ada</option>
                                                        <option value="0">Tidak</option>
                                                    </select>
                                                </td>
                                                <td class="align-middle">
                                                    <textarea
                                                        class="form-control"
                                                        name="parameters[ketersediaan_peralatan][keterangan]"
                                                    ></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold align-middle">Ketersediaan Metode Kerja</td>
                                                <td class="align-middle">
                                                    <select
                                                        class="form-control"
                                                        name="parameters[ketersediaan_metode_kerja][value]"
                                                    >
                                                        <option value="1">Ada</option>
                                                        <option value="0">Tidak</option>
                                                    </select>
                                                </td>
                                                <td class="align-middle">
                                                    <textarea
                                                        class="form-control"
                                                        name="parameters[ketersediaan_metode_kerja][keterangan]"
                                                    ></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold align-middle">Lainnya</td>
                                                <td class="align-middle">
                                                    <select
                                                        class="form-control"
                                                        name="parameters[lainnya][value]"
                                                    >
                                                        <option value="1">Ada</option>
                                                        <option value="0">Tidak</option>
                                                    </select>
                                                </td>
                                                <td class="align-middle">
                                                    <textarea
                                                        class="form-control"
                                                        name="parameters[lainnya][keterangan]"
                                                    ></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold align-middle">Kesimpulan Kajian Ulang</td>
                                                <td class="align-middle">
                                                    <select
                                                        class="form-control"
                                                        name="parameters[kesimpulan_kajian_ulang][value]"
                                                    >
                                                        <option value="1">Dapat Dikerjakan</option>
                                                        <option value="0">Tidak Dapat Dikerjakan</option>
                                                    </select>
                                                </td>
                                                <td class="align-middle">
                                                    <textarea
                                                        class="form-control"
                                                        name="parameters[kesimpulan_kajian_ulang][keterangan]"
                                                    ></textarea>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div x-data="{subkontraktor: 0}">
                                    <div class="form-group">
                                        <label>Subkontraktor</label>
                                        <select
                                            class="form-control"
                                            x-on:change="subkontraktor = $event.target.value"
                                            name="subkontraktor"
                                        >
                                            <option value="1">Ya</option>
                                            <option
                                                value="0"
                                                selected
                                            >Tidak</option>
                                        </select>
                                    </div>

                                    <template x-if="subkontraktor == 1">
                                        <div>
                                            <div
                                                class="form-group"
                                                x-transition
                                            >
                                                <label>Nama Subkontraktor</label>
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    placeholder="Nama Subkontraktor"
                                                    name="nama_subkontraktor"
                                                >
                                            </div>
                                            <div
                                                class="form-group"
                                                x-transition
                                            >
                                                <label>Alasan</label>
                                                <textarea
                                                    class="form-control"
                                                    placeholder="Alasan mensubkontraktor"
                                                    name="alasan_subkontraktor"
                                                ></textarea>
                                            </div>
                                        </div>
                                    </template>

                                </div>

                                <div class="form-group">
                                    <label>Durasi</label>
                                    <div class="input-group">
                                        <input
                                            type="number"
                                            class="form-control"
                                            name="durasi"
                                            required
                                        >
                                        <div class="input-group-append">
                                            <span class="input-group-text">Hari</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="float-right">
                                    <button
                                        type="submit"
                                        class="btn btn-primary btn-block"
                                    >
                                        Ajukan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    <script
        defer
        src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"
    ></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(document).ready(function() {
            // $("#form-kaji-ulang").validate({
            //     highlight: function(element, errorClass, validClass) {
            //         $(element).addClass("is-invalid").removeClass("is-valid");
            //     },
            //     unhighlight: function(element, errorClass, validClass) {
            //         $(element).removeClass("is-invalid");
            //     },
            //     errorElement: "em",
            //     errorClass: 'invalid-feedback',
            //     focusInvalid: true, // do not focus the last invalid input
            //     ignore: "", // validate all fields including form hidden input
            //     errorPlacement: function(error, element) {
            //         if (element.attr("type") == "radio") {
            //             error.insertAfter(element.parents('div').find('.radio-list'));
            //         } else {
            //             if (element.parent('.input-group').length) {
            //                 error.insertAfter(element.parent());
            //             } else {
            //                 error.insertAfter(element);
            //             }
            //         }
            //     },
            //     success: function(label) {
            //         label
            //             .closest('.form-group').removeClass(
            //                 'has-error'); // set success class to the control group
            //     },
            //     rules: {
            //         durasi: {
            //             required: true
            //         },
            //         tanggal_pelayanan: {
            //             required: true
            //         }
            //     },
            //     messages: {
            //         durasi: "Durasi harus diisi",
            //         tanggal_pelayanan: "Tanggal pelayanan harus diisi",
            //     },
            //     submitHandler: function(form) {
            //         // 
            //         Swal.fire({
            //             title: 'Yakin Ajukan?',
            //             text: "Pastikan data yang anda masukkan sudah benar!",
            //             icon: 'warning',
            //             showCancelButton: true,
            //             buttonsStyling: false,
            //             confirmButtonText: 'Ya, Ajukan sekarang!',
            //             cancelButtonText: 'Tidak, batalkan',
            //             customClass: {
            //                 confirmButton: 'btn btn-primary mr-2',
            //                 cancelButton: 'btn btn-danger',
            //             },
            //             preConfirm: () => {
            //                 form.submit();
            //                 // Swal.showLoading()
            //                 // return new Promise((resolve) => {
            //                 //     setTimeout(() => {
            //                 //         resolve(true)
            //                 //     }, 5000)
            //                 // })
            //             },
            //             allowOutsideClick: () => !Swal.isLoading()
            //         }).then((result) => {
            //             if (result.isConfirmed) {
            //                 Swal.fire({
            //                     buttonsStyling: false,
            //                     icon: 'success',
            //                     title: 'Berhasil diajukan!',
            //                     text: "Permintaan kaji ulang layanan anda berhasil diajukan",
            //                     customClass: {
            //                         confirmButton: 'btn btn-primary mr-2',
            //                         cancelButton: 'btn btn-danger',
            //                     },
            //                 })
            //             }
            //         })
            //     }
            // });
        })
    </script>
@endpush
