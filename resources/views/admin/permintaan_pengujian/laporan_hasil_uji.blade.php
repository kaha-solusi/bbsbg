@extends('layouts.adminlte')
@push('styles')

    <style>
        .table-detail-pesanan tr td:first-child {
            width: 120px;
        }

        div#print-area {
            visibility: hidden;
            position: absolute;
            left: 0;
            top: 0;
        }

        @media print {
            #non-printable {
                display: none;
            }


            body * {
                visibility: hidden;
            }

            #print-area,
            #print-area * {
                visibility: visible;
            }
        }

    </style>

    <link
        rel="stylesheet"
        href="{{ asset('adminlte/plugins/summernote/summernote.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.css') }}"
    >
@endpush

@section('main')
    <div
        id='non-printable'
        class="container"
    >
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <h3 class="card-title">Laporan Hasil Uji</h3>
                    </div>
                    <div class="card-body">
                        <form
                            action="{{ route('admin.permintaan-pengujian.laporan-hasil-uji.update', $laporan_hasil_uji->id) }}"
                            method="POST"
                            enctype="multipart/form-data"
                            id="form-laporan-hasil-uji"
                            novalidate
                        >
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label
                                    for=""
                                    class="text-secondary"
                                >Detail Pengujian</label>
                                <table class="table table-bordered table-detail-pesanan">
                                    <tbody>
                                        <tr>
                                            <td class="text-bold w-25">Nomor Laporan</td>
                                            <td>
                                                <div
                                                    x-data="{ editable: false }"
                                                    class="input-group"
                                                >
                                                    <input
                                                        type="text"
                                                        class="form-control"
                                                        name="nomor_laporan"
                                                        x-bind:readonly="!editable"
                                                        value="{{ old('nomor_laporan', $laporan_hasil_uji->nomor) }}"
                                                    >
                                                    @if ($laporan_hasil_uji->status == 'draft')
                                                        <span class="input-group-append">
                                                            <button
                                                                type="button"
                                                                class="btn btn-info btn-flat"
                                                                x-on:click="editable = !editable"
                                                                x-text="editable ? 'Simpan' : 'Ubah'"
                                                            >Ubah</button>
                                                        </span>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="text-bold">Nama Laboratorium</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->master_layanan_uji->pelayanan->nama }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Jenis Pengujian</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->master_layanan_uji->name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Tanggal Pengujian</td>
                                            <td>
                                                <div>
                                                    Tanggal Mulai:
                                                    {{ $laporan_hasil_uji->permintaan_pengujian->spk->tanggal_mulai }}
                                                </div>
                                                <div>
                                                    Tanggal Selesai:
                                                    {{ $laporan_hasil_uji->permintaan_pengujian->spk->tanggal_selesai }}
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Tanggal Terima Benda Uji</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->samples->last()->updated_at }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Deskripsi Benda Uji</td>
                                            <td>
                                                <div class="form-group">
                                                    <textarea
                                                        name="deskripsi_benda_uji"
                                                        class="form-control"
                                                        rows="3"
                                                        placeholder="Deskripsi Benda Uji"
                                                        @if ($laporan_hasil_uji->status != 'draft') disabled @endif
                                                    >{{ old('deskripsi_benda_uji', $laporan_hasil_uji->deskripsi_benda_uji) }}</textarea>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Produk</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->items->pluck('product_name')->unique()->join(',') }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Jumlah</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->items->count() }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Dibuat untuk</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->company_name ?? $laporan_hasil_uji->permintaan_pengujian->nama_pemohon }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Alamat</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->company_address ?? $laporan_hasil_uji->permintaan_pengujian->alamat }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Standar Acuan/Metode Pengujian</td>
                                            <td>{{ $laporan_hasil_uji->permintaan_pengujian->items->pluck('layanan_uji_item.standard')->unique()->join(', ') }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Hasil Pengujian</td>
                                            <td>Terlampir</td>
                                        </tr>
                                        {{-- <tr>
                                                    <td class="text-bold">Deskripsi/Kondisi Benda Uji</td>
                                                    @dd($laporan_hasil_uji->permintaan_pengujian->samples->last()->items)
                                                    <td>{{ $laporan_hasil_uji->permintaan_pengujian->items->count() }}
                                                    </td>
                                                </tr> --}}
                                    </tbody>
                                </table>
                            </div>

                            {{-- <div class="form-group">
                                <label
                                    for=""
                                    class="text-secondary"
                                >Kondisi Benda Uji</label>
                                <table class="table table-bordered">
                                    <thead class="table-custom">
                                        <tr>
                                            <th>No</th>
                                            <th>Nomor Benda Uji</th>
                                            <th>Nama Benda Uji</th>
                                            <th>Kondisi</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($laporan_hasil_uji->permintaan_pengujian->samples->last()->items as $no => $item)
                                            <tr>
                                                <td>{{ ++$no }}</td>
                                                <td>{{ $item->pivot->kode_sampel }}</td>
                                                <td>{{ $item->product->name }}</td>
                                                <td>{{ $item->pivot->status }}</td>
                                                <td>{{ $item->pivot->keterangan }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> --}}

                            <div class="form-group">
                                <label
                                    for=""
                                    class="text-secondary"
                                >Hasil Pengujian</label>
                                <table class="table table-bordered">
                                    <thead class="table-custom">
                                        <tr>
                                            <th
                                                rowspan="2"
                                                class="text-center align-middle"
                                            >No</th>
                                            <th
                                                rowspan="2"
                                                class="text-center align-middle"
                                            >Tipe Benda Uji</th>
                                            <th
                                                rowspan="2"
                                                class="text-center align-middle"
                                            >Nomor Benda Uji</th>
                                            <th
                                                colspan="3"
                                                class="text-center align-middle"
                                            >Retak Pertama</th>
                                            <th
                                                colspan="3"
                                                class="text-center align-middle"
                                            >Retak 0.33mm</th>
                                            <th
                                                colspan="3"
                                                class="text-center align-middle"
                                            >Maksimum</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center align-middle">F (kN)</th>
                                            <th class="text-center align-middle">δ ½ L (mm)</th>
                                            <th class="text-center align-middle">M (kN.m)</th>

                                            <th class="text-center align-middle">F (kN)</th>
                                            <th class="text-center align-middle">δ ½ L (mm)</th>
                                            <th class="text-center align-middle">M (kN.m)</th>

                                            <th class="text-center align-middle">F (kN)</th>
                                            <th class="text-center align-middle">δ ½ L (mm)</th>
                                            <th class="text-center align-middle">M (kN.m)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($laporan_hasil_uji->permintaan_pengujian->samples->last()->items as $no => $item)
                                            @php
                                                $hasilPengujian = json_decode($item->pengujian->last()->pivot->hasil_pengujian);
                                            @endphp
                                            <tr>
                                                <td class="text-center align-middle">{{ ++$no }}</td>
                                                <td class="text-center align-middle">{{ $item->product_type }}</td>
                                                <td class="text-center align-middle">{{ $item->pivot->kode_sampel }}</td>

                                                <td class="text-center align-middle">{{ $hasilPengujian->retak_pertama }}
                                                </td>
                                                <td class="text-center align-middle">
                                                    {{ $hasilPengujian->retak_pertama_l }}</td>
                                                <td class="text-center align-middle">
                                                    {{ $hasilPengujian->retak_pertama_m }}</td>

                                                <td class="text-center align-middle">
                                                    {{ $hasilPengujian->retak_mm_compute }}</td>
                                                <td class="text-center align-middle">
                                                    {{ $hasilPengujian->retak_mm_l }}</td>
                                                <td class="text-center align-middle">
                                                    {{ $hasilPengujian->retak_mm_m_kn_compute }}</td>

                                                <td class="text-center align-middle">
                                                    {{ $hasilPengujian->maksimum_compute }}</td>
                                                <td class="text-center align-middle">
                                                    {{ $hasilPengujian->maksimum_l }}</td>
                                                <td class="text-center align-middle">
                                                    {{ $hasilPengujian->maksimum_m_kn_compute }}</td>

                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="form-group">
                                <label class="text-secondary">Catatan</label>
                                <textarea
                                    class="form-control summernote"
                                    rows="5"
                                    name="catatan"
                                    @if ($laporan_hasil_uji->status != 'draft') disabled @endif
                                >{{ old('catatan', $laporan_hasil_uji->catatan ?? $cttn) }}</textarea>
                            </div>

                            <div class="form-group">
                                <label class="text-secondary">Tempat</label>
                                <input
                                    class="form-control"
                                    name="tempat"
                                    @if ($laporan_hasil_uji->status != 'draft') disabled @endif
                                    value="{{ old('tempat', $laporan_hasil_uji->tempat) }}"
                                />
                            </div>
                            <div class="form-group">
                                <label class="text-secondary">Tanggal Terbit</label>
                                <input
                                    autocomplete="off"
                                    class="form-control datepicker"
                                    name="tanggal_terbit"
                                    @if ($laporan_hasil_uji->status != 'draft') disabled @endif
                                    value="{{ old('tanggal_terbit', $laporan_hasil_uji->tanggal_terbit) }}"
                                />
                            </div>
                            <div class="form-group">
                                <label class="text-secondary">Unggah Laporan Hasil Uji</label>
                                <div class="mb-1">
                                    <a
                                        href="{{ '/uploads/laporan_hasil_uji/' . $lhu }}"
                                        target="_blank"
                                    >
                                        Laporan Hasil Uji
                                    </a>
                                </div>
                                @if ($laporan_hasil_uji->status === 'draft')
                                    <div class="custom-file">
                                        <input
                                            type="file"
                                            name="lhu"
                                            class="custom-file-input form-control @error('lhu') is-invalid @enderror"
                                            id="lhu"
                                        >
                                        @error('lhu')
                                            <span
                                                class="invalid-feedback"
                                                role="alert"
                                            >
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <label
                                            class="custom-file-label"
                                            for="lhu"
                                        >Pilih Dokumen</label>
                                    </div>
                                @endif
                            </div>
                            <div
                                style="color: #e00b0b"
                                @if ($laporan_hasil_uji->status != 'draft') hidden @endif
                            >
                                * Simpan perubahan anda sebelum mendownload file LHU!
                            </div>
                            <div class="float-right">
                                @if ($laporan_hasil_uji->status === 'draft')
                                    <a
                                        {{-- href="{{ route('admin.permintaan-pengujian.laporan-hasil-uji.printPreview', $laporan_hasil_uji->id) }}" --}}
                                        class="btn btn-info btn-print"
                                        href="#"
                                    >
                                        Download
                                    </a>
                                    <button
                                        type="submit"
                                        class="btn btn-primary"
                                    >
                                        Simpan
                                    </button>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="print-area">
        <div
            class="container my-5"
            id="html"
        >
            <div class="row">
                <div class="col-md-12">
                    <div
                        style="display: flex; align-items: center; justify-content: center"
                        class="row no-gutters align-items-center justify-content-center"
                    >
                        <div class="col-md-2">
                            <img
                                class="img-fluid"
                                src="data:image/png;base64,{{ base64_encode(file_get_contents(public_path('/assets/img/logo-pupr.png'))) }}"
                                style="width: 9rem"
                            />
                        </div>
                        <div
                            class="col-md-10"
                            style="line-height: 1.3; border-bottom: 2px solid black"
                        >
                            <span
                                class="text-uppercase"
                                style="font-size: 16pt; font-weight: bold; letter-spacing: 4pt"
                            >kementerian pekerjaan umum
                                dan perumahan rakyat</span>
                            <div
                                class="text-uppercase"
                                style="font-size: 16pt; font-weight: bold; letter-spacing: 11.35pt;"
                            >direktorat jenderal cipta karya</div>
                            <div
                                class="text-uppercase"
                                style="font-size: 14pt;letter-spacing: 5.7pt"
                            >direktorat bina teknik permukiman dan perumahan</div>
                            <div
                                class="text-uppercase"
                                style="font-size: 14pt;letter-spacing: 7.8pt;"
                            >balai bahan dan struktur bangunan gedung</div>
                            <div style="font-size: 11pt;letter-spacing: 2.9pt;">Jl. Panyaungan Cileunyi Wetan Kabupaten
                                Bandung
                                40393. Email: ditbtpp@pu.go.id</div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 mt-3">
                    <div class="d-flex align-items-center justify-content-center">
                        <span class="text-uppercase text-bold">Laporan Hasil Uji</span>
                    </div>
                </div>

                <div class="col-md-12 mt-3">
                    <style>
                        .detail-pesanan-table td,
                        .detail-pesanan-table th {
                            border-top: none !important;
                            padding-left: 0 !important;
                        }

                    </style>
                    <table class="table table-sm detail-pesanan-table">
                        <tbody>
                            <tr>
                                <td
                                    class="text-bold"
                                    style="width: 250px"
                                >Nomor Laporan</td>
                                <td>: {{ $laporan_hasil_uji->nomor }}</td>
                            </tr>

                            <tr>
                                <td class="text-bold">Nama Laboratorium</td>
                                <td>: {{ $laporan_hasil_uji->permintaan_pengujian->master_layanan_uji->pelayanan->nama }}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-bold">Jenis Pengujian</td>
                                <td>: {{ $laporan_hasil_uji->permintaan_pengujian->master_layanan_uji->name }}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-bold">Tanggal Pengujian</td>
                                <td>
                                    <div>
                                        : Tanggal Mulai:
                                        {{ $laporan_hasil_uji->permintaan_pengujian->spk->tanggal_mulai }}
                                        - Tanggal Selesai:
                                        {{ $laporan_hasil_uji->permintaan_pengujian->spk->tanggal_selesai }}
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td class="text-bold">Tanggal Terima Benda Uji</td>
                                <td>: {{ $laporan_hasil_uji->permintaan_pengujian->samples->last()->updated_at }}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-bold">Deskripsi Benda Uji</td>
                                <td>: {{ old('deskripsi_benda_uji', $laporan_hasil_uji->deskripsi_benda_uji) }}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-bold">Produk</td>
                                <td>:
                                    {{ $laporan_hasil_uji->permintaan_pengujian->items->pluck('product_name')->unique()->join(',') }}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-bold">Jumlah</td>
                                <td>: {{ $laporan_hasil_uji->permintaan_pengujian->items->count() }}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-bold">Dibuat untuk</td>
                                <td>:
                                    {{ $laporan_hasil_uji->permintaan_pengujian->company_name ?? $laporan_hasil_uji->permintaan_pengujian->nama_pemohon }}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-bold">Alamat</td>
                                <td>:
                                    {{ $laporan_hasil_uji->permintaan_pengujian->company_address ?? $laporan_hasil_uji->permintaan_pengujian->alamat }}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-bold">Standar Acuan/Metode Pengujian</td>
                                <td>:
                                    {{ $laporan_hasil_uji->permintaan_pengujian->items->pluck('layanan_uji_item.standard')->unique()->join(', ') }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                {{-- <div class="col-md-12 mt-3">
                <p class="text-bold">Kondisi Benda Uji</p>
                <table class="table table-sm table-bordered">
                    <thead class="table-custom">
                        <tr>
                            <th width="10">No</th>
                            <th>Nomor Benda Uji</th>
                            <th>Nama Benda Uji</th>
                            <th>Kondisi</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($laporan_hasil_uji->permintaan_pengujian->samples->last()->items as $no => $item)
                            <tr>
                                <td>{{ ++$no }}</td>
                                <td>{{ $item->pivot->kode_sampel }}</td>
                                <td>{{ $item->product->name }}</td>
                                <td>{{ $item->pivot->status }}</td>
                                <td>{{ $item->pivot->keterangan }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> --}}

                <div class="col-md-12 mt-3">
                    <p class="text-bold">Hasil Pengujian</p>
                    <table class="table table-sm table-bordered">
                        <thead class="table-custom">
                            <tr>
                                <th
                                    width="10"
                                    rowspan="2"
                                    class="text-center align-middle"
                                >No</th>
                                <th
                                    rowspan="2"
                                    class="text-center align-middle"
                                >Tipe Benda Uji</th>
                                <th
                                    rowspan="2"
                                    class="text-center align-middle"
                                >Nomor Benda Uji</th>
                                <th
                                    colspan="3"
                                    class="text-center align-middle"
                                >Retak Pertama</th>
                                <th
                                    colspan="3"
                                    class="text-center align-middle"
                                >Retak 0.33mm</th>
                                <th
                                    colspan="3"
                                    class="text-center align-middle"
                                >Maksimum</th>
                            </tr>
                            <tr>
                                <th class="text-center align-middle">F (kN)</th>
                                <th class="text-center align-middle">δ ½ L (mm)</th>
                                <th class="text-center align-middle">M (kN.m)</th>

                                <th class="text-center align-middle">F (kN)</th>
                                <th class="text-center align-middle">δ ½ L (mm)</th>
                                <th class="text-center align-middle">M (kN.m)</th>

                                <th class="text-center align-middle">F (kN)</th>
                                <th class="text-center align-middle">δ ½ L (mm)</th>
                                <th class="text-center align-middle">M (kN.m)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($laporan_hasil_uji->permintaan_pengujian->samples->last()->items as $no => $item)
                                @php
                                    $hasilPengujian = json_decode($item->pengujian->last()->pivot->hasil_pengujian);
                                @endphp
                                <tr>
                                    <td class="text-center align-middle">{{ ++$no }}</td>
                                    <td class="text-center align-middle">{{ $item->product_type }}</td>
                                    <td class="text-center align-middle">{{ $item->pivot->kode_sampel }}</td>

                                    <td class="text-center align-middle">{{ $hasilPengujian->retak_pertama }}
                                    </td>
                                    <td class="text-center align-middle">
                                        {{ $hasilPengujian->retak_pertama_l }}</td>
                                    <td class="text-center align-middle">
                                        {{ $hasilPengujian->retak_pertama_m }}</td>

                                    <td class="text-center align-middle">
                                        {{ $hasilPengujian->retak_mm_compute }}</td>
                                    <td class="text-center align-middle">
                                        {{ $hasilPengujian->retak_mm_l }}</td>
                                    <td class="text-center align-middle">
                                        {{ $hasilPengujian->retak_mm_m_kn_compute }}</td>

                                    <td class="text-center align-middle">
                                        {{ $hasilPengujian->maksimum_compute }}</td>
                                    <td class="text-center align-middle">
                                        {{ $hasilPengujian->maksimum_l }}</td>
                                    <td class="text-center align-middle">
                                        {{ $hasilPengujian->maksimum_m_kn_compute }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <p class="text-bold">Catatan</p>
                    {!! $laporan_hasil_uji->catatan ?? $cttn !!}
                </div>

                <div class="col-md-12">
                    <p class="text-bold">Tempat dan Tanggal Terbit</p>
                    <p>{{ $laporan_hasil_uji->tempat }}, {{ $laporan_hasil_uji->tanggal_terbit }}</p>
                </div>

                <div class="col-md-12 text-right mb-5">
                    <p class="text-bold">Kepala Balai Bahan dan Struktur Bangunan Gedung</p>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script
        defer
        src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"
    ></script>
    <script src="{{ asset('adminlte/plugins/summernote/summernote.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.btn-print').on('click', function(e) {
                e.preventDefault();
                window.print();
            });

            $('.summernote').summernote();
        })
    </script>
@endpush
