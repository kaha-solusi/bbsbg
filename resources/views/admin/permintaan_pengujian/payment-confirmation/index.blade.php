@extends('layouts.adminlte')

@section('main')
    @php
    $confirmation = $permintaan_pengujian->confirmations->last();
    @endphp
    <div class="container mt-3">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="float-left">
                                    Konfirmasi Pembayaran
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    method='POST'
                                    action="{{ route('admin.permintaan-pengujian.payment-confirmation.approve', $permintaan_pengujian->id) }}"
                                >
                                    @csrf
                                    <div class="form-group ">
                                        <label for="bank_name">Nama Bank</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="bank_name"
                                            name="bank_name"
                                            disabled
                                            value="{{ $confirmation->bank_name }}"
                                        >
                                    </div>
                                    <div class="form-group ">
                                        <label for="bank_name">Nomor Rekening Bank</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="account_number_name"
                                            name="account_number_name"
                                            disabled
                                            value="{{ $confirmation->account_number }}"
                                        >
                                    </div>
                                    <div class="form-group ">
                                        <label for="bank_name">Nama Pemegang Bank</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="account_holder_name"
                                            name="account_holder_name"
                                            disabled
                                            value="{{ $confirmation->account_holder }}"
                                        >
                                    </div>
                                    <div class="form-group ">
                                        <label for="bank_name">Nominal Pembayaran</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="payment_amount"
                                            name="payment_amount"
                                            disabled
                                            value="{{ $confirmation->payment_amount }}"
                                        >
                                    </div>
                                    <div class="form-group ">
                                        <label for="payment_date">Tanggal Pembayaran</label>
                                        <input
                                            type="date"
                                            class="form-control"
                                            id="payment_date"
                                            name="payment_date"
                                            disabled
                                            value="{{ $confirmation->payment_date }}"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label for="payment_date">Bukti Pembayaran</label>
                                        <a
                                            href="/{{ $confirmation->payment_proof->path }}"
                                            target="_blank"
                                        >
                                            <img
                                                src="/{{ $confirmation->payment_proof->path }}"
                                                alt=""
                                                width="auto"
                                                class="img-fluid"
                                            />
                                        </a>
                                    </div>

                                    <div class="form-group mt-2">
                                        <div class="float-right">
                                            <a
                                                href="#rejectModal"
                                                class="btn btn-danger"
                                                data-toggle="modal"
                                                data-target="#rejectModal"
                                            >Tolak</a>
                                            <button
                                                class="btn btn-success"
                                                type="submit"
                                            >Setujui</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div
        class="modal fade"
        id="rejectModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="rejectModalLabel"
        aria-hidden="true"
    >
        <div
            class="modal-dialog"
            role="document"
        >
            <div class="modal-content">

                <form
                    id="reject-form"
                    action="{{ route('admin.permintaan-pengujian.payment-confirmation.reject', $permintaan_pengujian->id) }}"
                    method="POST"
                >
                    @csrf
                    <div class="modal-header">
                        <h5
                            class="modal-title"
                            id="exampleModalLabel"
                        >Alasan Ditolak</h5>
                        <button
                            type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close"
                        >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <textarea
                                class="form-control"
                                id="reject-reason"
                                name="reject-reason"
                                rows="3"
                                required
                            ></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button
                            type="button"
                            class="btn btn-secondary"
                            data-dismiss="modal"
                        >Batal</button>
                        <button
                            type="submit"
                            class="btn btn-primary"
                        >Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
