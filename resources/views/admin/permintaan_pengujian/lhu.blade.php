@include('admin.parts.breadcrumbs',
['judul' => [
['judul' => 'Permintaan Pengujian'],
['judul' => 'ss']
]])
<div class="container">
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <h5>Penerbitan LHU</h5>
                </div>
                <div class="card-body">
                    <form
                        action="{{ route('admin.permintaan-pengujian.update', $pengujian->id) }}"
                        method="POST"
                        id="form-kaji-ulang"
                        enctype="multipart/form-data"
                    >
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <style>
                                .table-detail-pesanan tr td:first-child {
                                    width: 250px
                                }

                            </style>
                            <table class="table table-bordered table-detail-pesanan">
                                <tbody>
                                    <tr>
                                        <td class="text-bold">Nomor Laporan</td>
                                        <td>
                                            <div
                                                x-data="{ editable: false }"
                                                class="input-group"
                                            >
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    name="nomor_permintaan"
                                                    x-bind:readonly="!editable"
                                                    value="{{ old('nomor_permintaan', $pengujian->nomor_permintaan) }}"
                                                >
                                                <span class="input-group-append">
                                                    <button
                                                        type="button"
                                                        class="btn btn-info btn-flat"
                                                        x-on:click="editable = !editable"
                                                        x-text="editable ? 'Simpan' : 'Ubah'"
                                                    >Ubah</button>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">Nama Laboratorium</td>
                                        <td>{{ $pengujian->master_layanan_uji->pelayanan->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">Jenis Pengujian</td>
                                        <td>{{ $pengujian->master_layanan_uji->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">Tanggal Terima Benda Uji</td>
                                        <td>
                                            {{ $pengujian->test_samples->last()->tanggal_diterima ?? '' }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">Produk</td>
                                        <td>{{ $pengujian->items->pluck('product_name')->join(', ') }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">Jumlah</td>
                                        <td>{{ $pengujian->items->count() }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">Deskripsi/Kondisi Benda Uji</td>
                                        <td>
                                            <input
                                                type="kondisi"
                                                class="form-control"
                                                name="kondisi"
                                                value="{{ old('kondisi', $pengujian->kondisi) }}"
                                            >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">Dibuat Untuk</td>
                                        <td>{{ $pengujian->nama_pemohon }} -
                                            {{ $pengujian->client->nama_perusahaan }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">Alamat</td>
                                        <td>{{ $pengujian->alamat }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">Standar Acuan/Metode Pengujian</td>
                                        <td>{{ $pengujian->items->pluck('layanan_uji_item.standard')->unique()->join(' ') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">Hasil Pengujian</td>
                                        <td>Terlampir</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script
        defer
        src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"
    ></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_KAJI_ULANG_SUBMITTED)
        <script>
            $(document).ready(function() {
                const url = '{{ route('admin.permintaan-pengujian.update', $pengujian->id) }}'
                const indexUrl = '{{ route('admin.permintaan-pengujian.index') }}'
                $('#form-kaji-ulang').on('submit', function(e) {
                    e.preventDefault();
                    Swal.fire({
                        title: 'Yakin menyetujui kaji ulang layanan ini?',
                        text: "Pastikan data yang anda masukkan sudah benar!",
                        icon: 'warning',
                        reverseButtons: true,
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonText: 'Ya, Setujui!',
                        cancelButtonText: 'Tidak, batalkan',
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-secondary mr-2',
                        },
                        preConfirm: () => {
                            return fetch(
                                    `${url}?_token=${$('input[name="_token"]').val()}&status=approved`, {
                                        method: 'PUT',
                                        header: {
                                            "Accept": "application/json",
                                            'Content-Type': 'application/json',
                                        }
                                    })
                                .catch(error => {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                })
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            Swal.fire({
                                buttonsStyling: false,
                                icon: 'success',
                                title: 'Berhasil!',
                                text: "Permintaan kaji ulang layanan berhasil disetujui",
                                customClass: {
                                    confirmButton: 'btn btn-primary',
                                },
                            }).then(() => {
                                window.location.href = indexUrl
                            })
                        }
                    })
                })


                $('#rejectModalBtn').on('click', function() {
                    Swal.fire({
                        titleText: 'Alasan menolak',
                        text: "Masukkan alasan anda menolak permintaan kaji ulang layanan ini",
                        input: 'textarea',
                        inputAttributes: {
                            autocapitalize: 'off',
                            name: 'message'
                        },
                        reverseButtons: true,
                        showCancelButton: true,
                        showLoaderOnConfirm: true,
                        buttonsStyling: false,
                        confirmButtonText: 'Tolak',
                        cancelButtonText: 'Batalkan',
                        customClass: {
                            confirmButton: 'btn btn-danger',
                            cancelButton: 'btn btn-secondary mr-2',
                        },
                        preConfirm: (message) => {
                            return fetch(
                                    `${url}?_token=${$('input[name="_token"]').val()}&message=${message}&status=rejected`, {
                                        method: 'PUT',
                                        header: {
                                            "Accept": "application/json",
                                            'Content-Type': 'application/json',
                                        }
                                    })
                                .catch(error => {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                })
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            Swal.fire({
                                buttonsStyling: false,
                                icon: 'success',
                                title: 'Berhasil menolak!',
                                text: "Permintaan kaji ulang layanan berhasil ditolak",
                                customClass: {
                                    confirmButton: 'btn btn-primary',
                                },
                            }).then(() => {
                                window.location.href = indexUrl
                            })
                        }
                    })
                })

            })
        </script>
    @endif
@endpush
