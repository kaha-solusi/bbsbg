<div class="container">
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4 moksd">
                            <div class="float-left">
                                Data Pengujian
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form
                        method="POST"
                        action="{{ route('admin.permintaan-pengujian.store') }}"
                    >
                        @csrf
                        <div class="form-group">
                            <label for="">Nomor SPK</label>
                            <input
                                type="text"
                                class="form-control"
                                value="{{ old('nomor_spk', $pengujian->spk->nomor) }}"
                                readonly
                            />
                        </div>
                        {{-- <div class="form-group">
                            <label for="">Nomor Pengujian</label>
                            <input
                                type="text"
                                class="form-control"
                            />
                        </div> --}}
                        <div class="form-group">
                            <label for="tanggal_pengujian">Tanggal Pengujian</label>
                            <input
                                type="text"
                                class="form-control"
                                id="tanggal_pengujian"
                                disabled
                                value="{{ old('tanggal_pengujian', $pengujian->spk->tanggal_mulai) }}"
                            >
                        </div>
                        <div class="form-group">
                            <label for="tanggal_pengujian">Jenis Pengujian</label>
                            <input
                                type="text"
                                class="form-control"
                                id="tanggal_pengujian"
                                disabled
                                value="{{ old('tanggal_pengujian', $pengujian->master_layanan_uji->name) }}"
                            >
                        </div>
                        <div class="form-group">
                            <label for="tanggal_pengujian">Benda Uji</label>
                            <table class="table table-bordered">
                                <thead class="thead-custom">
                                    <tr>
                                        <th width="50px">No</th>
                                        <th>Parameter Pengujian</th>
                                        <th>Nama Benda Uji</th>
                                        <th
                                            width="160px"
                                            class="text-center"
                                        >Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pengujian->items as $no => $item)
                                        <tr>
                                            <td>{{ ++$no }}</td>
                                            <td>{{ $item->layanan_uji_item->name }}</td>
                                            <td>{{ $item->product_name }}</td>
                                            <td>
                                                @if (is_null($item->hasil_pengujian))
                                                    <a
                                                        href="{{ route('admin.permintaan-pengujian.proses-pengujian.index', [$pengujian->id, $item->id]) }}"
                                                        class="btn btn-sm btn-primary text-center btn-block"
                                                    >Lakukan Pengujian</a>
                                                @else
                                                    <a
                                                        href="{{ route('admin.permintaan-pengujian.proses-pengujian.show', [$pengujian->id, $item->id]) }}"
                                                        class="btn btn-sm btn-secondary text-center btn-block"
                                                    >Lihat Hasil</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        @if (!$pengujian->items->pluck('hasil_pengujian')->contains(null))
                            <div class="form-group">
                                <div class="float-right">
                                    <a
                                        href=""
                                        class="btn btn-primary"
                                    >Buat LHU</a>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
