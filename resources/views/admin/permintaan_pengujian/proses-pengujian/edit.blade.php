@extends('layouts.adminlte')

@section('main')
    <div class="container">
        <div class="card card-block">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-4 moksd">
                        <div class="float-left">
                            Proses Pengujian
                        </div>
                    </div>
                </div>
            </div>

            <form method="POST">
                @csrf
                <div
                    x-data="{jenis_pengujian: '{{ $parameter->hasil_pengujian->jenis_pengujian ?? '' }}'}"
                    class="card-body"
                >
                    <div class="form-group">
                        <label for="tanggal_pengujian">Jenis Pengujian</label>
                        <select
                            name="jenis_pengujian"
                            class="form-control"
                            disabled
                            x-on:change="jenis_pengujian = $event.target.value"
                        >
                            <option value="">Pilih jenis pengujian</option>
                            <option
                                value="tiang-pancang-bulat"
                                x-bind:selected="jenis_pengujian == 'tiang-pancang-bulat'"
                            >Tiang Pancang Bulat</option>
                            <option
                                value="tiang-pancang-kotak"
                                x-bind:selected="jenis_pengujian == 'tiang-pancang-kotak'"
                            >Tiang Pancang Kotak</option>
                            <option
                                value="turap-beton"
                                x-bind:selected="jenis_pengujian == 'turap-beton'"
                            >Turap Beton</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tanggal_pengujian">Alat Pengujian</label>
                        <div class="input-group">
                            <select
                                name='alat_pengujian[]'
                                class="form-control peralatan-list"
                                multiple
                                disabled
                            >
                                <option>Pilih Alat Pengujian</option>
                                @foreach ($parameter->peralatan as $peralatan)
                                    <option
                                        value="{{ $peralatan->id }}"
                                        selected
                                    >
                                        <span>
                                            @if (!is_null($peralatan->foto_alat))
                                                <img
                                                    class="img-flag mr-2"
                                                    width="50"
                                                    height="50"
                                                    src="{{ $peralatan->foto_alat }}"
                                                />
                                            @endif
                                            <span>{{ $peralatan->nama }}</span>
                                        </span>
                                        {{ $peralatan->nama }}
                                    </option>
                                @endforeach
                            </select>
                            <span class="input-group-append">
                                <a
                                    href="#barcodeModal"
                                    data-href="#barcodeModal"
                                    data-toggle="modal"
                                    type="button"
                                    class="btn btn-info btn-flat"
                                ><i class="fa fa-camera"></i></a>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tanggal_pengujian">Rumus Pengujian</label>
                        <div class="table-responsive">
                            <div x-show="jenis_pengujian === 'tiang-pancang-bulat'">
                                @include('admin.permintaan_pengujian.proses-pengujian.tiang-pancang-bulat')
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script>
    <script src="{{ asset('adminlte/plugins/jsautocalc/jautocalc.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('table tr[name=line_items]').jAutoCalc({
                keyEventsFire: true,
                emptyAsZero: true,
                smartIntegers: true,
                decimalOpts: ['.', ','],
                decimalPlaces: 3,
                readOnlyResults: true,
            });

            const format = (item) => {
                if (!item.id) {
                    return item.text;
                }

                var url = item.element.getAttribute('data-avatar');

                var ahref = '';
                if (item.foto_alat) {
                    var img = $("<img>", {
                        class: "rounded-circle me-2",
                        width: 25,
                        height: 25,
                        src: item.foto_alat
                    });
                    var ahref = $("<a>", {
                        href: item.foto_alat,
                    });
                    ahref.attr('target', '_blank');
                    ahref.append(img);
                }

                var span = $("<span>", {
                    text: " " + item.text
                });
                span.prepend(ahref);
                return span;
            }

            function formatState(state) {
                if (!state.id) {
                    return state.text;
                }

                var baseUrl = "/user/pages/images/flags";
                var $state = $(
                    `<span>${state.foto_alat && '<img class="img-flag mr-2" width="50" height="50" />' || ''} <span></span></span>`
                );

                $state.find("span").text(state.text);
                if (state.foto_alat) {
                    $state.find("img").attr("src", state.foto_alat);
                }

                return $state;
            };

            $peralatans = $('.peralatan-list').select2({
                theme: "bootstrap4",
                placeholder: "Pilih Peralatan",
                ajax: {
                    url: "{{ route('admin.peralatan.get-peralatan-by-name') }}",
                    cache: true,
                    delay: 250,
                },
                minimumInputLength: 2,
                templateSelection: format,
                templateResult: formatState
            });

            // function onScanSuccess(decodedText, decodedResult) {
            //     $('#barcodeModal').modal('hide');
            //     $.get(`{{ route('admin.peralatan.get-peralatan-by-id') }}/?id=${decodedText}`, function(data) {
            //         var option = new Option(data.text, data.id, true, true);
            //         $('.peralatan-list').append(option).trigger('change');

            //         $peralatans.trigger({
            //             type: 'select2:select',
            //             params: {
            //                 data: data
            //             }
            //         });
            //     });
            // }

            // var html5QrcodeScanner = new Html5QrcodeScanner(
            //     "qr-reader", {
            //         fps: 10,
            //         qrbox: 250
            //     });

            // $('#barcodeModal').on('shown.bs.modal', function(e) {
            //     html5QrcodeScanner.render(onScanSuccess);
            // })
            // $('#barcodeModal').on('hide.bs.modal', function(e) {
            //     html5QrcodeScanner.clear();
            // })
        })
    </script>
@endpush
