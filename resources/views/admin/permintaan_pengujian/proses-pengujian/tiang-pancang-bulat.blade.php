<style>
    #tiangPancangTable tbody tr td>* {
        width: 150px
    }

</style>
<table
    id="tiangPancangTable"
    class="table table-bordered"
>
    <thead class='thead-custom'>
        <tr>
            <th
                rowspan="3"
                class="text-center align-middle"
            >No Benda Uji</th>
            <th
                rowspan="3"
                class="text-center align-middle""
            >Tipe Benda Uji</th>
            <th colspan="
                9"
                class="
                text-center align-middle"
            >Dimensi Benda Uji</th>
            <th
                rowspan="3"
                class="
            text-center align-middle"
            >Massa tiang pancang, m (tf)</th>
            <th
                colspan="2"
                class="
            text-center align-middle"
            >Konfigurasi Pembebanan</th>
            <th
                rowspan="3"
                class="
            text-center align-middle"
            >Berat alat bantu, W (tf)</th>
            <th
                rowspan="3"
                class="
            text-center align-middle"
            >Berat alat bantu, W (kN)</th>
            <th
                rowspan="3"
                class="
            text-center align-middle"
            >Grafitasi, g0 (m2/s)</th>
            <th
                colspan="18"
                class="
            text-center align-middle"
            >Hasil Pengujian</th>
            <th
                rowspan="3"
                class="
            text-center align-middle"
            >Keterangan</th>
        </tr>
        <tr>
            <th
                colspan="4"
                class="
            text-center align-middle"
            >Diameter Luar, d</th>
            <th
                colspan="4"
                class="
            text-center align-middle"
            >Tebal Tiang Pancang</th>
            <th
                rowspan="2"
                class="
            text-center align-middle"
            >Panjang</th>
            <th
                rowspan="2"
                class="
            text-center align-middle"
            >Panjang Bentang (3/5 L)</th>
            <th
                rowspan="2"
                class="
            text-center align-middle"
            >Tumpuan (1/5L)</th>
            <th
                colspan="6"
                class="
            text-center align-middle"
            >Retak Pertama</th>
            <th
                colspan="6"
                class="
            text-center align-middle"
            >Retak 0,3 mm</th>
            <th
                colspan="6"
                class="
            text-center align-middle"
            >Maksimum</th>
        </tr>
        <tr>
            <th class="text-center align-middle">1</th>
            <th class="text-center align-middle">2</th>
            <th class="text-center align-middle">3</th>
            <th class="text-center align-middle">Rata - Rata</th>
            <th class="text-center align-middle">1</th>
            <th class="text-center align-middle">2</th>
            <th class="text-center align-middle">3</th>
            <th class="text-center align-middle">Rata - Rata</th>

            <th
                colspan="4"
                class="text-center align-middle"
            >F load cell</th>
            <th class="text-center align-middle">δ ½ L (mm)</th>
            <th class="text-center align-middle">M (kN.m)</th>

            <th
                colspan="4"
                class="text-center align-middle"
            >F load cell</th>
            <th class="text-center align-middle">δ ½ L (mm)</th>
            <th class="text-center align-middle">M (kN.m)</th>

            <th
                colspan="4"
                class="text-center align-middle"
            >F load cell</th>
            <th class="text-center align-middle">δ ½ L (mm)</th>
            <th class="text-center align-middle">M (kN.m)</th>
        </tr>
    </thead>
    <tbody>
        <tr name="line_items">
            <input
                type="hidden"
                name="phi"
                value="3.14"
            />
            <td class="text-center align-middle">
                <p>{{ $parameter->no_sample }}</p>
            </td>
            <td class="text-center align-middle">
                <p>{{ $parameter->product_type }}</p>
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="d1"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->d1 ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="d2"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->d2 ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="d3"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->d3 ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="dmean"
                    jAutoCalc="({d1} + {d2} + {d3}) / 3"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="t1"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->t1 ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="t2"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->t2 ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="t3"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->t3 ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="tmean"
                    readonly
                    jAutoCalc="({t1} + {t2} + {t3}) / 3"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="panjang"
                    value="{{ $parameter->hasil_pengujian->panjang ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="massa_tiang_pancang"
                    readonly
                    jAutoCalc="2.6 * {phi} * {tmean} * ({dmean} - {tmean}) * {panjang}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="panjang-bentang"
                    readonly
                    jAutoCalc="{massa_tiang_pancang} * 3 / 5"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="tumpuan"
                    readonly
                    jAutoCalc="{massa_tiang_pancang} * 1 / 5"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="berat_alat_bantu_w_tf"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->berat_alat_bantu_w_tf ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="berat_alat_bantu_w_kn"
                    readonly
                    jAutoCalc="{berat_alat_bantu_w_tf} * {gravity}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="gravity"
                    value="9.81"
                    readonly
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="retak_pertama_f_load_cell"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->retak_pertama_f_load_cell ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <p>tf</p>
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="retak_pertama"
                    readonly
                    jAutoCalc="{retak_pertama_f_load_cell} * {gravity}"
                />
            </td>
            <td class="text-center align-middle">
                <p>kN</p>
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="retak_pertama_l"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->retak_pertama_l ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="retak_pertama_m"
                    readonly
                    jAutoCalc="1 / 40 * {gravity} * {massa_tiang_pancang} * {panjang} + ({retak_pertama} + {berat_alat_bantu_w_kn}) / 4 * ((3 / 5 * {panjang}) - 1)"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="retak_mm_f_load_cell"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->retak_mm_f_load_cell ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <p>tf</p>
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="retak_mm_compute"
                    readonly
                    jAutoCalc="{retak_mm_f_load_cell} * {gravity}"
                />
            </td>
            <td class="text-center align-middle">
                <p>kN</p>
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="retak_mm_l"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->retak_mm_l ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="retak_mm_m_kn_compute"
                    readonly
                    jAutoCalc="1 / 40 * {gravity} * {massa_tiang_pancang} * {panjang} + ({retak_mm_compute} + {berat_alat_bantu_w_kn}) / 4 * ((3 / 5 * {panjang}) - 1)"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="maksimum_f_load_cell"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->maksimum_f_load_cell ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <p>tf</p>
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="maksimum_compute"
                    readonly
                    jAutoCalc="{maksimum_f_load_cell} * {gravity}"
                />
            </td>
            <td class="text-center align-middle">
                <p>kN</p>
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control maksimum-l"
                    name="maksimum_l"
                    step="0.001"
                    value="{{ $parameter->hasil_pengujian->maksimum_l ?? '' }}"
                />
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control maksimum-m-kn-compute"
                    name="maksimum_m_kn_compute"
                    readonly
                    jAutoCalc="1 / 40 * {gravity} * {massa_tiang_pancang} * {panjang} + ({maksimum_compute} + {berat_alat_bantu_w_kn}) / 4 * ((3 / 5 * {panjang}) - 1)"
                />
            </td>
            <td class="text-center align-middle">
                <textarea
                    class="form-control keterangan"
                    name="keterangan"
                    style="width: 250px"
                >{{ $parameter->hasil_pengujian->keterangan ?? '' }}</textarea>
            </td>
        </tr>
    </tbody>
</table>

@push('script')
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
@endpush
