@extends('layouts.adminlte')
@push('style')
    <style>
        .table-detail-pesanan tr td:first-child {
            width: 200px
        }
    </style>
@endpush
@section('main')
    @include('admin.parts.breadcrumbs', [
        'judul' => [['judul' => 'Permintaan Pengujian'], ['judul' => 'ss']],
    ])
    @include('admin.utils.alert')

    @php
    $checkedBy = $kaji_ulang_layanan->approvals->filter(function ($value, $key) {
        return $value->creator->hasRole(['super-admin', 'subkoor']) && $value->creator->hasPermissionTo('approve_kaji_ulangs');
    });

    $approvedBy = $kaji_ulang_layanan->approvals->filter(function ($value, $key) {
        return $value->creator->hasRole(['super-admin', 'kepala-balai']) && $value->creator->hasPermissionTo('approve_kaji_ulangs');
    });
    @endphp
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <h5>Kaji Ulang Layanan</h5>
                    </div>
                    <div class="card-body">
                        <input
                            type="hidden"
                            value="{{ $kaji_ulang_layanan->id }}"
                            name="id"
                        />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label
                                        for=""
                                        class="text-secondary"
                                    >Data Pesanan</label>

                                    <table class="table table-bordered table-detail-pesanan">
                                        <tbody>
                                            <tr>
                                                <td class="text-bold">No Permintaan</td>
                                                <td>
                                                    <div class="input-group">
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="nomor_permintaan"
                                                            readonly
                                                            value="{{ old('nomor_permintaan', $kaji_ulang_layanan->permintaan_pengujian->order_id) }}"
                                                        >
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Tanggal Permintaan</td>
                                                <td>{{ $kaji_ulang_layanan->permintaan_pengujian->tgl_permintaan }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Tanggal Pelayanan</td>
                                                <td>
                                                    <input
                                                        type="date"
                                                        class="form-control"
                                                        name="tanggal_pelayanan"
                                                        value={{ $kaji_ulang_layanan->tanggal_pelayanan }}
                                                        readonly
                                                    />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Laboratorium</td>
                                                <td>{{ $kaji_ulang_layanan->permintaan_pengujian->master_layanan_uji->pelayanan->nama ?? '' }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Jenis Pengujian</td>
                                                <td>{{ $kaji_ulang_layanan->permintaan_pengujian->master_layanan_uji->name ?? '' }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Nama Institusi</td>
                                                <td>{{ $kaji_ulang_layanan->permintaan_pengujian->client->nama_perusahaan ?? '' }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Alamat</td>
                                                <td>{{ $kaji_ulang_layanan->permintaan_pengujian->alamat }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Nama Kontak</td>
                                                <td>{{ $kaji_ulang_layanan->permintaan_pengujian->nama_pemohon }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">No HP Kontak</td>
                                                <td>{{ $kaji_ulang_layanan->permintaan_pengujian->nomor }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Email</td>
                                                <td>{{ $kaji_ulang_layanan->permintaan_pengujian->email }}</td>
                                            </tr>
                                            <tr>
                                                <td class="align-middle text-bold">Disusun Oleh</td>
                                                <td>
                                                    <span
                                                        class="badge badge-success">{{ $kaji_ulang_layanan->creator->name ?? '' }}
                                                    </span>

                                                    <p class="mb-0">
                                                        <small>{{ $kaji_ulang_layanan->created_at ?? '' }}</small>
                                                    </p>
                                                </td>
                                            </tr>

                                            {{-- @php
                                                $submittedBy = $kaji_ulang_layanan->approvals->isEmpty()
                                                    ? $kaji_ulang_layanan->approvals
                                                    : $kaji_ulang_layanan->approvals
                                                        ->toQuery()
                                                        ->with([
                                                            'creator.permissions' => function ($query) {
                                                                return $query->where('name', 'submit_kaji_ulangs');
                                                            },
                                                        ])
                                                        ->get();
                                                
                                                $approvedBy = $kaji_ulang_layanan->approvals->isEmpty()
                                                    ? $kaji_ulang_layanan->approvals
                                                    : $kaji_ulang_layanan->approvals
                                                        ->toQuery()
                                                        ->with([
                                                            'creator.permissions' => function ($query) {
                                                                return $query->where('name', 'approve_kaji_ulangs');
                                                            },
                                                        ])
                                                        ->get();
                                            @endphp --}}
                                            @if ($kaji_ulang_layanan->isApprovedBy('subkoor'))
                                                <tr>
                                                    <td class="align-middle text-bold">Diperiksa Oleh</td>
                                                    <td>
                                                        <span class="badge badge-success">
                                                            {{ $checkedBy->last()->creator->name ?? '' }}
                                                        </span>
                                                        <p class="mb-0">
                                                            <small>{{ $checkedBy->last()->created_at ?? '' }}</small>
                                                        </p>
                                                    </td>
                                                </tr>
                                            @endif

                                            @if ($kaji_ulang_layanan->isApprovedBy('kepala-balai'))
                                                <tr>
                                                    <td class="align-middle text-bold">Disetujui Oleh</td>
                                                    <td>
                                                        <span class="badge badge-success">
                                                            {{ $approvedBy->last()->creator->name ?? '' }}
                                                        </span>

                                                        <p class="mb-0">
                                                            <small>{{ $approvedBy->last()->created_at ?? '' }}</small>
                                                        </p>
                                                    </td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td class="text-bold">Status</td>
                                                <td>
                                                    @if ($kaji_ulang_layanan->status === 'draft')
                                                        <span class="badge badge-warning">Menunggu Persetujuan</span>
                                                    @endif
                                                    @if ($kaji_ulang_layanan->status === 'submitted')
                                                        <span class="badge badge-warning">Menunggu Persetujuan Kepala
                                                            Balai</span>
                                                    @endif
                                                    @if ($kaji_ulang_layanan->status === 'approved')
                                                        <span class="badge badge-success">Disetujui</span>
                                                    @endif
                                                    @if ($kaji_ulang_layanan->status === 'rejected')
                                                        <span class="badge badge-danger">Ditolak</span>
                                                    @endif
                                                </td>
                                            </tr>
                                            @if ($kaji_ulang_layanan->status === 'rejected')
                                                <tr>
                                                    <td class="text-bold">Alasan</td>
                                                    <td>{{ $kaji_ulang_layanan->approvals->last()->reason }}</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            {{-- Detail Order --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label
                                        for=""
                                        class="text-secondary"
                                    >Order Layanan</label>

                                    <table class="table table-bordered">
                                        <thead class="thead-custom">
                                            <tr>
                                                <th>No</th>
                                                <th class="text-center">Nama Layanan</th>
                                                <th class="text-center">Nama Produk</th>
                                                <th class="text-center">Harga Satuan</th>
                                                <th class="text-center">Kuantiti</th>
                                                <th class="text-center">Jumlah</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @php
                                                $total = 0;
                                            @endphp
                                            @foreach ($kaji_ulang_layanan->permintaan_pengujian->items as $key => $item)
                                                <tr>
                                                    <td class="text-center">{{ ++$key }}</td>
                                                    <td class="text-center">{{ $item->layanan_uji_item->name }}
                                                    </td>
                                                    <td class="text-center">{{ $item->product_name }}</td>
                                                    <td class="text-center">
                                                        {{ formatCurrency($item->layanan_uji_item->price) }}</td>
                                                    <td class="text-center">{{ $item->product_count }}</td>
                                                    <td class="text-center">
                                                        {{ formatCurrency($item->layanan_uji_item->price * $item->product_count) }}
                                                    </td>

                                                    @php
                                                        $total += $item->layanan_uji_item->price * $item->product_count;
                                                    @endphp
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td
                                                    class="text-center text-bold"
                                                    colspan="5"
                                                >Total</td>
                                                <td class="text-center">{{ formatCurrency($total) }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{-- .Detail Order --}}

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label
                                        for=""
                                        class="text-secondary"
                                    >Kaji Ulang Permintaan</label>
                                    <style>
                                        .table-detail-pesanan tr td:first-child {
                                            width: 200px
                                        }
                                    </style>
                                    <table class="table table-bordered table-detail-pesanan">
                                        <thead class="table-custom">
                                            <tr>
                                                <th>Parameter</th>
                                                <th width="20%">Hasil</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($kaji_ulang_layanan->parameter as $key => $item)
                                                <tr>
                                                    <td class="text-bold align-middle">
                                                        {{ ucwords(preg_replace('/_/', ' ', $key)) }}</td>
                                                    </td>
                                                    <td class="align-middle">
                                                        <select
                                                            class="form-control"
                                                            disabled
                                                        >
                                                            @if ($key !== 'kesimpulan_kajian_ulang')
                                                                <option
                                                                    value="1"
                                                                    @if ($item['value'] === 1) selected @endif
                                                                >Ada</option>
                                                                <option
                                                                    value="0"
                                                                    @if ($item['value'] === 1) selected @endif
                                                                >Tidak</option>
                                                            @else
                                                                <option
                                                                    value="1"
                                                                    @if ($item['value'] === 1) selected @endif
                                                                >Dapat Dikerjakan</option>
                                                                <option
                                                                    value="0"
                                                                    @if ($item['value'] === 1) selected @endif
                                                                >Tidak Dapat Dikerjakan</option>
                                                            @endif
                                                        </select>
                                                    </td>
                                                    <td class="align-middle">
                                                        {{ $item['keterangan'] }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div x-data="{ subkontraktor: {{ $kaji_ulang_layanan->subkontraktor }} }">
                                    <div class="form-group">
                                        <label>Subkontraktor</label>
                                        <select
                                            class="form-control"
                                            x-on:change="subkontraktor = $event.target.value"
                                            disabled
                                        >
                                            <option
                                                value="1"
                                                @if ($kaji_ulang_layanan->subkontraktor === 1) selected @endif
                                            >Ya</option>
                                            <option
                                                value="0"
                                                @if ($kaji_ulang_layanan->subkontraktor === 0) selected @endif
                                            >Tidak</option>
                                        </select>
                                    </div>

                                    <template x-if="subkontraktor == 1">
                                        <div>
                                            <div
                                                class="form-group"
                                                x-transition
                                            >
                                                <label>Nama Subkontraktor</label>
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    placeholder="Nama Subkontraktor"
                                                    value="{{ $kaji_ulang_layanan->nama_subkontraktor }}"
                                                    readonly
                                                >
                                            </div>
                                            <div
                                                class="form-group"
                                                x-transition
                                            >
                                                <label>Alasan</label>
                                                <textarea
                                                    class="form-control"
                                                    placeholder="Alasan mensubkontraktor"
                                                    readonly
                                                >{{ $kaji_ulang_layanan->keterangan }}</textarea>
                                            </div>
                                        </div>
                                    </template>
                                </div>

                                <div class="form-group">
                                    <label>Durasi</label>
                                    <div class="input-group">
                                        <input
                                            type="number"
                                            class="form-control"
                                            name="durasi"
                                            value="{{ $kaji_ulang_layanan->durasi }}"
                                            readonly
                                        >
                                        <div class="input-group-append">
                                            <span class="input-group-text">Hari</span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            {{-- $checkedBy->isNotEmpty() --}}
                            <div class="col-md-12">
                                @can('approve_kaji_ulangs')
                                    @hasanyrole('kepala-balai')
                                        @if (!$kaji_ulang_layanan->isApprovedBy('kepala-balai') && $kaji_ulang_layanan->isApprovedBy('subkoor'))
                                            <div class="float-right">
                                                <a
                                                    href="#"
                                                    class="btn btn-danger reject-btn"
                                                >
                                                    Tolak
                                                </a>
                                                <button
                                                    type="submit"
                                                    class="btn btn-primary approve-btn"
                                                >
                                                    Setujui
                                                </button>
                                            </div>
                                        @endif
                                    @endhasanyrole

                                    @hasanyrole('subkoor')
                                        @if (!$kaji_ulang_layanan->isApprovedBy('kepala-balai'))
                                            <div class="float-right">
                                                <a
                                                    href="#"
                                                    class="btn btn-danger reject-btn"
                                                >
                                                    Tolak
                                                </a>
                                                <button
                                                    type="submit"
                                                    class="btn btn-primary approve-btn"
                                                >
                                                    Setujui
                                                </button>
                                            </div>
                                        @endif
                                    @endhasanyrole

                                    @hasrole('super-admin')
                                        @if (!$kaji_ulang_layanan->isApprovedBy('kepala-balai') && !$kaji_ulang_layanan->isApprovedBy('subkoor'))
                                            <div class="float-right">
                                                <a
                                                    href="#"
                                                    class="btn btn-danger reject-btn"
                                                >
                                                    Tolak
                                                </a>
                                                <button
                                                    type="submit"
                                                    class="btn btn-primary approve-btn"
                                                >
                                                    Setujui
                                                </button>
                                            </div>
                                        @endif
                                    @endhasrole
                                @endcan
                                <div class="float-right mr-1">
                                    <a
                                        href="{{ route('admin.permintaan-pengujian.kaji-ulang-layanan.index') }}"
                                        class="btn btn-secondary"
                                    >
                                        Kembali
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    <script
        defer
        src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"
    ></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(document).ready(function() {
            $('.reject-btn').click(function(e) {
                Swal.fire({
                    titleText: 'Alasan menolak',
                    text: "Masukkan alasan anda menolak permintaan kaji ulang layanan ini",
                    input: 'textarea',
                    inputAttributes: {
                        autocapitalize: 'off',
                        name: 'message'
                    },
                    reverseButtons: true,
                    showCancelButton: true,
                    showLoaderOnConfirm: true,
                    buttonsStyling: false,
                    backdrop: true,
                    confirmButtonText: 'Tolak',
                    cancelButtonText: 'Batalkan',
                    customClass: {
                        confirmButton: 'btn btn-danger',
                        cancelButton: 'btn btn-secondary mr-2',
                    },
                    preConfirm: (message) => {
                        return $.ajax({
                            url: `${route(
                                'admin.permintaan-pengujian.kaji-ulang-layanan.reject', ['{{ $kaji_ulang_layanan->id }}'])}?_token=${$('input[name="_token"]').val()}`,
                            dataType: 'json',
                            type: 'POST',
                            data: {
                                message
                            },
                            error: function(response) {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire({
                            buttonsStyling: false,
                            icon: 'success',
                            title: 'Berhasil menolak!',
                            text: "Permintaan kaji ulang layanan berhasil ditolak",
                            customClass: {
                                confirmButton: 'btn btn-primary',
                            },
                        }).then(() => {
                            window.location.href = route(
                                'admin.permintaan-pengujian.kaji-ulang-layanan.index'
                            );
                        })
                    }
                })
            });

            $('.approve-btn').click(function(e) {
                e.preventDefault();
                Swal.fire({
                    title: 'Yakin Setujui?',
                    text: "Pastikan data yang anda masukkan sudah benar!",
                    icon: 'info',
                    showCancelButton: true,
                    buttonsStyling: false,
                    reverseButtons: true,
                    backdrop: true,
                    confirmButtonText: 'Ya, setujui sekarang!',
                    cancelButtonText: 'Tidak, batalkan',
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-danger mr-2',
                    },
                    preConfirm: () => {
                        return $.ajax({
                            url: `${route(
                                'admin.permintaan-pengujian.kaji-ulang-layanan.approve', ['{{ $kaji_ulang_layanan->id }}'])}?_token=${$('input[name="_token"]').val()}`,
                            dataType: 'json',
                            type: 'POST',
                            error: function(response) {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire({
                            buttonsStyling: false,
                            icon: 'success',
                            title: 'Berhasil!',
                            text: "Permintaan kaji ulang layanan anda berhasil disetujui",
                            customClass: {
                                confirmButton: 'btn btn-primary mr-2',
                                cancelButton: 'btn btn-danger',
                            },
                        }).then(() => {
                            window.location.href = route(
                                'admin.permintaan-pengujian.kaji-ulang-layanan.index'
                            );
                        })
                    }
                })
            });
        })
    </script>
@endpush
