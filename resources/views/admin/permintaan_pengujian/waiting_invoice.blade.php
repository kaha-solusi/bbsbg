@include('admin.parts.breadcrumbs',
['judul' => [
['judul' => 'Permintaan Pengujian'],
['judul' => 'Kode Billing']
]])
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
                <!-- title row -->
                <div class="row">
                    <div class="col-12">
                        <h4>
                            <i class="fas fa-globe"></i> Balai BBSBG
                            <small class="float-right">Tanggal:
                                {{ $pengujian->tgl_permintaan }}
                            </small>
                        </h4>
                    </div>
                    <!-- /.col -->
                </div>

                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-6 invoice-col">
                        Yang mengajukan
                        <address>
                            <strong>{{ $pengujian->nama_pemohon }}</strong><br>
                            {{ $pengujian->alamat }}<br>
                            Phone: {{ $pengujian->nomor }} <br>
                            Email: {{ $pengujian->email }}
                        </address>
                    </div>

                    <!-- /.col -->
                    @if (!is_null($pengujian->invoice))
                        <div class="col-sm-6 invoice-col text-right">
                            <b>Invoice
                                #{{ $pengujian->invoice->code }}</b><br>
                            <b>Order ID:</b>
                            {{ $pengujian->invoice->order_code }}<br>
                        </div>
                    @endif

                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Parameter Pengujian</th>
                                    <th>Nama Produk</th>
                                    <th>Tipe Produk</th>
                                    <th>Banyak Benda Uji</th>
                                    <th>Harga Satuan</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pengujian->items as $item)
                                    <tr>
                                        <td>{{ $item->layanan_uji_item->name }}
                                        </td>
                                        <td>{{ $item->product_name }}
                                        </td>
                                        <td>{{ $item->product_type }}
                                        </td>
                                        <td>{{ $item->product_count }}
                                        </td>
                                        <td>{{ formatCurrency($item->layanan_uji_item->price) }}
                                        </td>
                                        <td>{{ formatCurrency($item->layanan_uji_item->price * $item->product_count) }}
                                        </td>
                                    </tr>
                                @endforeach

                                <tr style="background-color: whitesmoke">
                                    <td colspan="4"></td>
                                    <th>Total</th>
                                    <th>{{ formatCurrency($subTotal) }}
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <form
                    action="{{ route('admin.permintaan-pengujian.update', $pengujian->id) }}"
                    method="POST"
                >
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-6">
                            <p class="font-weight-bold">Masukkan Kode Billing:
                            </p>

                            <div class="
                           form-group">
                                <input
                                    name="billing_code"
                                    class="form-control"
                                    required
                                    value="{{ old('billing_code', $pengujian->kode_billing) }}"
                                    @if (!is_null($pengujian->kode_billing)) disabled @endif
                                />
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-12">

                            <button
                                type="submit"
                                class="btn btn-success float-right"
                            ><i class="far fa-credit-card"></i> Kirim Kode Billing
                            </button>
                        </div>
                    </div>

                </form>

            </div>
            <!-- /.invoice -->
        </div><!-- /.col -->
    </div>
</div>
