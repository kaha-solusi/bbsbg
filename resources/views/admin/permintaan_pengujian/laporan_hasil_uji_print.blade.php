<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0"
    >
    <meta
        http-equiv="Content-Type"
        content="text/html; charset=utf-8"
    />
    <meta
        http-equiv="X-UA-Compatible"
        content="ie=edge"
    >
    <title>Print Preview</title>
    <link
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700"
        rel="stylesheet"
    >
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/dist/css/adminlte.min.css') }}"
    >
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.4.0/jspdf.umd.min.js"
        integrity="sha512-V/C9Axb8EEL4ix79ERIJmpRd6Mp1rWVSxa2PIBCdCxqhEsfCBWp/R0xJ4U495czhcuDWrGOFYo8+QI3lJ9DK5g=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer"
    ></script>

    <style>
        .text-uppercase {
            text-transform: uppercase;
        }

        @media print {
            body {
                size: 330mm 210mm;
                margin: 20px;
            }
        }

    </style>
</head>

<body>
    <div
        class="container my-5"
        id="html"
    >
        <div class="row">
            <div class="col-md-12">
                <div
                    style="display: flex; align-items: center; justify-content: center"
                    class="row no-gutters align-items-center justify-content-center"
                >
                    <div class="col-md-2">
                        <img
                            class="img-fluid"
                            src="data:image/png;base64,{{ base64_encode(file_get_contents(public_path('/assets/img/logo-pupr.png'))) }}"
                            style="width: 9rem"
                        />
                    </div>
                    <div
                        class="col-md-10"
                        style="line-height: 1.3; border-bottom: 2px solid black"
                    >
                        <span
                            class="text-uppercase"
                            style="font-size: 16pt; font-weight: bold; letter-spacing: 4pt"
                        >kementerian pekerjaan umum
                            dan perumahan rakyat</span>
                        <div
                            class="text-uppercase"
                            style="font-size: 16pt; font-weight: bold; letter-spacing: 11.35pt;"
                        >direktorat jenderal cipta karya</div>
                        <div
                            class="text-uppercase"
                            style="font-size: 14pt;letter-spacing: 5.7pt"
                        >direktorat bina teknik permukiman dan perumahan</div>
                        <div
                            class="text-uppercase"
                            style="font-size: 14pt;letter-spacing: 7.8pt;"
                        >balai bahan dan struktur bangunan gedung</div>
                        <div style="font-size: 11pt;letter-spacing: 2.9pt;">Jl. Panyaungan Cileunyi Wetan Kabupaten
                            Bandung
                            40393. Email: ditbtpp@pu.go.id</div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 mt-3">
                <div class="d-flex align-items-center justify-content-center">
                    <span
                        class="text-uppercase text-bold"
                        style="border-bottom: 1px solid black"
                    >Laporan Hasil Uji</span>
                </div>
            </div>

            <div class="col-md-12 mt-3">
                <style>
                    .detail-pesanan-table td,
                    .detail-pesanan-table th {
                        border-top: none !important;
                        padding-left: 0 !important;
                    }

                </style>
                <table class="table table-sm detail-pesanan-table">
                    <tbody>
                        <tr>
                            <td
                                class="text-bold"
                                style="width: 250px"
                            >Nomor Laporan</td>
                            <td>: {{ $laporan_hasil_uji->nomor }}</td>
                        </tr>

                        <tr>
                            <td class="text-bold">Nama Laboratorium</td>
                            <td>: {{ $laporan_hasil_uji->permintaan_pengujian->master_layanan_uji->pelayanan->nama }}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-bold">Jenis Pengujian</td>
                            <td>: {{ $laporan_hasil_uji->permintaan_pengujian->master_layanan_uji->name }}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-bold">Tanggal Pengujian</td>
                            <td>
                                <div>
                                    : Tanggal Mulai:
                                    {{ $laporan_hasil_uji->permintaan_pengujian->spk->tanggal_mulai }}
                                    - Tanggal Selesai:
                                    {{ $laporan_hasil_uji->permintaan_pengujian->spk->tanggal_selesai }}
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td class="text-bold">Tanggal Terima Benda Uji</td>
                            <td>: {{ $laporan_hasil_uji->permintaan_pengujian->samples->last()->updated_at }}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-bold">Deskripsi Benda Uji</td>
                            <td>: {{ old('deskripsi_benda_uji', $laporan_hasil_uji->deskripsi_benda_uji) }}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-bold">Produk</td>
                            <td>:
                                {{ $laporan_hasil_uji->permintaan_pengujian->items->pluck('product_name')->unique()->join(',') }}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-bold">Jumlah</td>
                            <td>: {{ $laporan_hasil_uji->permintaan_pengujian->items->count() }}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-bold">Dibuat untuk</td>
                            <td>:
                                {{ $laporan_hasil_uji->permintaan_pengujian->company_name ?? $laporan_hasil_uji->permintaan_pengujian->nama_pemohon }}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-bold">Alamat</td>
                            <td>:
                                {{ $laporan_hasil_uji->permintaan_pengujian->company_address ?? $laporan_hasil_uji->permintaan_pengujian->alamat }}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-bold">Standar Acuan/Metode Pengujian</td>
                            <td>:
                                {{ $laporan_hasil_uji->permintaan_pengujian->items->pluck('layanan_uji_item.standard')->unique()->join(', ') }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            {{-- <div class="col-md-12 mt-3">
                <p class="text-bold">Kondisi Benda Uji</p>
                <table class="table table-sm table-bordered">
                    <thead class="table-custom">
                        <tr>
                            <th width="10">No</th>
                            <th>Nomor Benda Uji</th>
                            <th>Nama Benda Uji</th>
                            <th>Kondisi</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($laporan_hasil_uji->permintaan_pengujian->samples->last()->items as $no => $item)
                            <tr>
                                <td>{{ ++$no }}</td>
                                <td>{{ $item->pivot->kode_sampel }}</td>
                                <td>{{ $item->product->name }}</td>
                                <td>{{ $item->pivot->status }}</td>
                                <td>{{ $item->pivot->keterangan }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> --}}

            <div class="col-md-12 mt-3">
                <p class="text-bold">Hasil Pengujian</p>
                <table class="table table-sm table-bordered">
                    <thead class="table-custom">
                        <tr>
                            <th
                                width="10"
                                rowspan="2"
                                class="text-center align-middle"
                            >No</th>
                            <th
                                rowspan="2"
                                class="text-center align-middle"
                            >Tipe Benda Uji</th>
                            <th
                                rowspan="2"
                                class="text-center align-middle"
                            >Nomor Benda Uji</th>
                            <th
                                colspan="3"
                                class="text-center align-middle"
                            >Retak Pertama</th>
                            <th
                                colspan="3"
                                class="text-center align-middle"
                            >Retak 0.33mm</th>
                            <th
                                colspan="3"
                                class="text-center align-middle"
                            >Maksimum</th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">P</th>
                            <th class="text-center align-middle">L</th>
                            <th class="text-center align-middle">M</th>

                            <th class="text-center align-middle">P</th>
                            <th class="text-center align-middle">L</th>
                            <th class="text-center align-middle">M</th>

                            <th class="text-center align-middle">P</th>
                            <th class="text-center align-middle">L</th>
                            <th class="text-center align-middle">M</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($laporan_hasil_uji->permintaan_pengujian->samples->last()->items as $no => $item)
                            @php
                                $hasilPengujian = json_decode($item->pengujian->last()->pivot->hasil_pengujian);
                            @endphp
                            <tr>
                                <td class="text-center align-middle">{{ ++$no }}</td>
                                <td class="text-center align-middle">{{ $item->product_type }}</td>
                                <td class="text-center align-middle">{{ $item->pivot->kode_sampel }}</td>

                                <td class="text-center align-middle">{{ $hasilPengujian->retak_pertama }}
                                </td>
                                <td class="text-center align-middle">
                                    {{ $hasilPengujian->retak_pertama_l }}</td>
                                <td class="text-center align-middle">
                                    {{ $hasilPengujian->retak_pertama_m }}</td>

                                <td class="text-center align-middle">
                                    {{ $hasilPengujian->retak_mm_compute }}</td>
                                <td class="text-center align-middle">
                                    {{ $hasilPengujian->retak_mm_l }}</td>
                                <td class="text-center align-middle">
                                    {{ $hasilPengujian->retak_mm_m_kn_compute }}</td>

                                <td class="text-center align-middle">
                                    {{ $hasilPengujian->maksimum_compute }}</td>
                                <td class="text-center align-middle">
                                    {{ $hasilPengujian->maksimum_l }}</td>
                                <td class="text-center align-middle">
                                    {{ $hasilPengujian->maksimum_m_kn_compute }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="col-md-12">
                <p class="text-bold">Catatan</p>
                <p> {{ old('catatan', $laporan_hasil_uji->catatan) }} </p>
            </div>

            <div class="col-md-12">
                <p class="text-bold">Tempat dan Tanggal Terbit</p>
                <p>{{ $laporan_hasil_uji->tempat }}, {{ $laporan_hasil_uji->tanggal_terbit }}</p>
            </div>

            <div class="col-md-12 text-right mb-5">
                <p class="text-bold">Kepala Balai Bahan dan Struktur Bangunan Gedung</p>
            </div>
        </div>
    </div>

    <div
        class="d-print-none"
        style="position: fixed; bottom: 10px; right: 10px"
    >
        <a
            href="{{ route('admin.permintaan-pengujian.laporan-hasil-uji.print', $laporan_hasil_uji->id) }}"
            class="btn btn-primary"
        ><i class="fa fa-print"></i>Cetak Laporan Hasil Uji</a>
    </div>

    <script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
    <script>
        const {
            jsPDF
        } = window.jspdf;
        // var pdf = new jsPDF('p', 'pt', 'letter');
        // pdf.html(document.body, {
        //     callback: function(pdf) {
        //         var iframe = document.createElement('iframe');
        //         iframe.setAttribute('style',
        //             'position:absolute;right:0; top:0; bottom:0; height:100%; width:500px');
        //         document.body.appendChild(iframe);
        //         iframe.src = pdf.output('datauristring');
        //     }
        // });
        // html2canvas(document.querySelector("body")).then(canvas => {
        //     // var doc = new jsPDF('p', 'pt', 'a4', false);
        //     // doc.html(document.querySelector("body"))
        //     // doc.addImage(canvas, 'jpg', 0, 0, 0, 0, undefined, 'slow');
        //     // doc.save('test.pdf')
        //     document.body.appendChild(canvas)
        // });
        // var pdf = new jsPDF('p', 'pt', 'letter');
        // pdf.html(document.getElementById('html'), {
        //     callback: function(pdf) {
        //         var iframe = document.createElement('iframe');
        //         iframe.setAttribute('style',
        //             'position:absolute;right:0; top:0; bottom:0; height:100%; width:500px');
        //         document.body.appendChild(iframe);
        //         iframe.src = pdf.output('datauristring');
        //     }
        // });
    </script>
</body>

</html>
