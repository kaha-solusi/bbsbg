@extends('layouts.adminlte')
@push('style')
    <style>
        .table-detail-pesanan tr td:first-child {
            width: 200px
        }
    </style>
@endpush
@section('main')
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <h5>Detail Pesanan</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            {{-- @can('upload_layanan_konfirmasi')
                                @if ($pengujian->status == \App\Models\PermintaanPengujian::STATUS_KAJI_ULANG_APPROVED_BY_KABALAI)
                                    <div class="col-md-12 form-group">
                                        <label class="text-secondary">Form Konfirmasi</label>
                                        <div>
                                            <button class="btn btn-primary btn-unggah-form">
                                                Unggah Form Layanan Konfirmasi
                                            </button>
                                        </div>
                                    </div>
                                @endif
                            @endcan

                            @if ($pengujian->dokumen->where('type', 'dokumen_konfirmasi')->isNotEmpty())
                                <div class="col-md-12 form-group">
                                    <label class="text-secondary">Form Konfirmasi</label>
                                    <div>
                                        <a
                                            href="/{{ $pengujian->dokumen->where('type', 'dokumen_konfirmasi')->last()->dokumen->path }}"
                                            target="_blank"
                                        >
                                            Dokumen Konfirmasi Layanan
                                        </a>
                                    </div>
                                </div>
                            @endif

                            @if ($pengujian->dokumen->where('type', 'dokumen_konfirmasi_reply')->isNotEmpty())
                                <div class="col-md-12 form-group">
                                    <label class="text-secondary">Form Konfirmasi Dari Client</label>
                                    <div>
                                        <a
                                            class="btn btn-sm btn-primary download-reply"
                                            href="/{{ $pengujian->dokumen->where('type', 'dokumen_konfirmasi_reply')->last()->dokumen->path }}"
                                        >
                                            Dokumen Konfirmasi Layanan Client
                                        </a>
                                    </div>
                                </div>
                            @endif --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label
                                        for=""
                                        class="text-secondary"
                                    >Data Pesanan</label>
                                    <table class="table table-bordered table-detail-pesanan">
                                        <tbody>
                                            <tr>
                                                <td class="text-bold">No Permintaan</td>
                                                <td>
                                                    <div
                                                        x-data="{ editable: false }"
                                                        class="input-group"
                                                    >
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="nomor_permintaan"
                                                            x-bind:readonly="!editable"
                                                            value="{{ old('nomor_permintaan', $pengujian->order_id) }}"
                                                        >
                                                        @if ($pengujian->status == 'draft')
                                                            <span class="input-group-append">
                                                                <button
                                                                    type="button"
                                                                    class="btn btn-info btn-flat"
                                                                    x-on:click="editable = !editable"
                                                                    x-text="editable ? 'Simpan' : 'Ubah'"
                                                                >Ubah</button>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Tanggal Permintaan</td>
                                                <td>{{ $pengujian->tgl_permintaan }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Tanggal Pelayanan</td>
                                                <td>
                                                    <input
                                                        type="text"
                                                        class="form-control"
                                                        name="tanggal_pelayanan"
                                                        value={{ $pengujian->kaji_ulang->tanggal_pelayanan ?? '' }}
                                                        readonly
                                                    />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="text-bold">Laboratorium</td>
                                                <td>{{ $pengujian->master_layanan_uji->pelayanan->nama }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Jenis Pengujian</td>
                                                <td>{{ $pengujian->master_layanan_uji->name }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Nama Institusi</td>
                                                <td>{{ $pengujian->company_name ?? '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Alamat</td>
                                                <td>{{ $pengujian->company_address ?? $pengujian->alamat }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Nama Kontak</td>
                                                <td>{{ $pengujian->nama_pemohon }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">No HP Kontak</td>
                                                <td>{{ $pengujian->nomor }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Email</td>
                                                <td>{{ $pengujian->company_email ?? $pengujian->email }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold">Status</td>
                                                <td>{{ $pengujian->status }}</td>
                                            </tr>

                                            @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_KAJI_ULANG_REJECTED)
                                                <tr>
                                                    <td class="text-bold">Alasan</td>
                                                    <td>{{ $pengujian->kaji_ulang->approvals->last()->reason }}</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            {{-- Detail Order --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label
                                        for=""
                                        class="text-secondary"
                                    >Order Layanan</label>

                                    <table class="table table-bordered">
                                        <thead class="thead-custom">
                                            <tr>
                                                <th>No</th>
                                                <th class="text-center">Nama Layanan</th>
                                                <th class="text-center">Nama Produk</th>
                                                <th class="text-center">Harga Satuan</th>
                                                <th class="text-center">Kuantiti</th>
                                                <th class="text-center">Jumlah</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @php
                                                $total = 0;
                                            @endphp
                                            @foreach ($pengujian->items as $key => $item)
                                                <tr>
                                                    <td class="text-center">{{ ++$key }}</td>
                                                    <td class="text-center">{{ $item->layanan_uji_item->name }}
                                                    </td>
                                                    <td class="text-center">{{ $item->product_name }}</td>
                                                    <td class="text-center">
                                                        {{ formatCurrency($item->layanan_uji_item->price) }}</td>
                                                    <td class="text-center">{{ $item->product_count }}</td>
                                                    <td class="text-center">
                                                        {{ formatCurrency($item->layanan_uji_item->price * $item->product_count) }}
                                                    </td>

                                                    @php
                                                        $total += $item->layanan_uji_item->price * $item->product_count;
                                                    @endphp
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td
                                                    class="text-center text-bold"
                                                    colspan="5"
                                                >Total</td>
                                                <td class="text-center">{{ formatCurrency($total) }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{-- .Detail Order --}}


                            <div class="col-md-12">
                                <div class="form-group">
                                    <label
                                        for=""
                                        class="text-secondary"
                                    >Kaji Ulang Permintaan</label>
                                    <style>
                                        .table-detail-pesanan tr td:first-child {
                                            width: 200px
                                        }
                                    </style>
                                    <table class="table table-bordered table-detail-pesanan">
                                        <thead class="table-custom">
                                            <tr>
                                                <th>Parameter</th>
                                                <th width="20%">Hasil</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($pengujian->kaji_ulang->parameter as $key => $item)
                                                <tr>
                                                    <td class="text-bold align-middle">
                                                        {{ ucwords(preg_replace('/_/', ' ', $key)) }}</td>
                                                    </td>
                                                    <td class="align-middle">
                                                        <select
                                                            class="form-control"
                                                            disabled
                                                        >
                                                            @if ($key !== 'kesimpulan_kajian_ulang')
                                                                <option
                                                                    value="1"
                                                                    @if ($item['value'] === 1) selected @endif
                                                                >Ada</option>
                                                                <option
                                                                    value="0"
                                                                    @if ($item['value'] === 1) selected @endif
                                                                >Tidak</option>
                                                            @else
                                                                <option
                                                                    value="1"
                                                                    @if ($item['value'] === 1) selected @endif
                                                                >Dapat Dikerjakan</option>
                                                                <option
                                                                    value="0"
                                                                    @if ($item['value'] === 1) selected @endif
                                                                >Tidak Dapat Dikerjakan</option>
                                                            @endif
                                                        </select>
                                                    </td>
                                                    <td class="align-middle">
                                                        {{ $item['keterangan'] }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div x-data="{ subkontraktor: {{ $pengujian->kaji_ulang->subkontraktor }} }">
                                    <div class="form-group">
                                        <label>Subkontraktor</label>
                                        <select
                                            class="form-control"
                                            x-on:change="subkontraktor = $event.target.value"
                                            disabled
                                        >
                                            <option
                                                value="1"
                                                @if ($pengujian->kaji_ulang->subkontraktor === 1) selected @endif
                                            >Ya</option>
                                            <option
                                                value="0"
                                                @if ($pengujian->kaji_ulang->subkontraktor === 0) selected @endif
                                            >Tidak</option>
                                        </select>
                                    </div>

                                    <template x-if="subkontraktor == 1">
                                        <div>
                                            <div
                                                class="form-group"
                                                x-transition
                                            >
                                                <label>Nama Subkontraktor</label>
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    placeholder="Nama Subkontraktor"
                                                    value="{{ $pengujian->kaji_ulang->nama_subkontraktor }}"
                                                    readonly
                                                >
                                            </div>
                                            <div
                                                class="form-group"
                                                x-transition
                                            >
                                                <label>Alasan</label>
                                                <textarea
                                                    class="form-control"
                                                    placeholder="Alasan mensubkontraktor"
                                                    readonly
                                                >{{ $pengujian->kaji_ulang->keterangan }}</textarea>
                                            </div>
                                        </div>
                                    </template>
                                </div>

                                <div class="form-group">
                                    <label>Durasi</label>
                                    <div class="input-group">
                                        <input
                                            type="number"
                                            class="form-control"
                                            name="durasi"
                                            value="{{ $pengujian->kaji_ulang->durasi }}"
                                            readonly
                                        >
                                        <div class="input-group-append">
                                            <span class="input-group-text">Hari</span>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="col-md-12">
                                <div class="float-right">
                                    <a
                                        href="{{ route('admin.permintaan-pengujian.index') }}"
                                        type="submit"
                                        class="btn btn-secondary"
                                    >
                                        Kembali
                                    </a>
                                    @if ($pengujian->status == 'draft')
                                        <button
                                            type="submit"
                                            class="btn btn-primary"
                                        >
                                            Simpan dan Ajukan
                                        </button>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_DRAFT)
        @include('admin.permintaan_pengujian.kaji-ulang-layanan.index')
    @endif

    @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_KAJI_ULANG_SUBMITTED || $pengujian->status === \App\Models\PermintaanPengujian::STATUS_KAJI_ULANG_APPROVED)
        @include('admin.permintaan_pengujian.kaji-ulang-layanan.show')
    @endif

    @if ($pengujian->status === \App\Models\PermintaanPengujian::STATUS_FORM_CONFIRMATION_SUBMITTED || $pengujian->status === \App\Models\PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_SUBMITTED)
        @include('admin.permintaan_pengujian.kaji-ulang-layanan.confirmation_submitted')
    @endif

    @if (\App\Models\PermintaanPengujian::STATUS_WAITING_INVOICE == $pengujian->status)
        @include('admin.permintaan_pengujian.waiting_invoice')
    @endif

    @if (\App\Models\PermintaanPengujian::STATUS_SAMPLE_SUBMITTED == $pengujian->status)
        @include('admin.permintaan_pengujian.sample_submitted')
    @endif

    @if (\App\Models\PermintaanPengujian::STATUS_SAMPLE_RECEIVED == $pengujian->status)
        @include('admin.permintaan_pengujian.sample_received')
    @endif

    @if (\App\Models\PermintaanPengujian::STATUS_SPK_SUBMITTED == $pengujian->status)
        @include('admin.permintaan_pengujian.proses-pengujian.index')
    @endif

    @if (\App\Models\PermintaanPengujian::STATUS_PENGUJIAN_SUBMITTED == $pengujian->status)
        @include('admin.permintaan_pengujian.lhu')
    @endif --}}
@endsection


@push('script')
    <script
        defer
        src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"
    ></script>

    {{-- @if ($pengujian->status == \App\Models\PermintaanPengujian::STATUS_KAJI_ULANG_APPROVED_BY_KABALAI)
        <script>
            $(document).ready(function() {
                $('.btn-unggah-form').click(function(e) {
                    Swal.fire({
                        titleText: 'Unggah Form Layanan Konfirmasi',
                        text: "Form layanan konfirmasi akan dikirimkan ke pelanggan",
                        input: 'file',
                        inputAttributes: {
                            class: 'form-control',
                            accept: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        },
                        reverseButtons: true,
                        showCancelButton: true,
                        showLoaderOnConfirm: true,
                        buttonsStyling: false,
                        backdrop: true,
                        confirmButtonText: 'Unggah',
                        cancelButtonText: 'Batalkan',
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-secondary mr-2',
                        },
                        preConfirm: (file) => {
                            const formData = new FormData();
                            formData.append('file', file);

                            return $.ajax({
                                url: `${route(
                                'admin.permintaan-pengujian.unggah-form-layanan-konfirmasi',
                                ['{{ $pengujian->id }}'])}?_token=${$('meta[name="csrf-token"]').attr('content')}`,
                                type: 'POST',
                                data: formData,
                                contentType: false,
                                processData: false,
                                error: function(response) {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                }
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            Swal.fire({
                                buttonsStyling: false,
                                icon: 'success',
                                title: 'Berhasil!',
                                text: "Form konfirmasi layanan berhasil diunggah",
                                customClass: {
                                    confirmButton: 'btn btn-primary',
                                },
                            }).then(() => {
                                window.location.href = route(
                                    'admin.permintaan-pengujian.index'
                                );
                            })
                        }
                    })
                });
            })
        </script>
    @endif

    <script>
        $(document).ready(function() {
            $('.download-reply').click(function(e) {
                e.preventDefault();
                const url = $(this).attr('href')
                const status = '{{ $pengujian->status }}'
                const isActionEnabled = status ===
                    '{{ \App\Models\PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_APPROVED }}'

                Swal.fire({
                    titleText: 'Dokumen konfirmasi layanan',
                    // text: "Apakah anda yakin ingin menyetujui dokumen konfirmasi layanan?",
                    reverseButtons: true,
                    showCancelButton: isActionEnabled ? false : true,
                    showConfirmButton: isActionEnabled ? false : true,
                    showLoaderOnConfirm: true,
                    buttonsStyling: false,
                    backdrop: true,
                    confirmButtonText: 'Ya, setuju!',
                    cancelButtonText: 'Tidak, batalkan!',
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-secondary mr-2',
                    },
                    html: `
                            <a href="${url}" target="_blank" class="btn btn-info btn-block btn-download-reply">Unduh</a>
                        `,
                    preConfirm: () => {
                        return $.ajax({
                            url: `${route(
                                    'admin.permintaan-pengujian.document-approve',
                                    ['{{ $pengujian->id }}'])}?_token=${$('meta[name="csrf-token"]').attr('content')}`,
                            type: 'POST',
                            error: function(response) {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire({
                            buttonsStyling: false,
                            icon: 'success',
                            title: 'Berhasil!',
                            text: "Form konfirmasi layanan berhasil disetujui",
                            customClass: {
                                confirmButton: 'btn btn-primary',
                            },
                        }).then(() => {
                            window.location.href = route(
                                'admin.permintaan-pengujian.index'
                            );
                        })
                    }
                })
            });
        })
    </script> --}}
@endpush
