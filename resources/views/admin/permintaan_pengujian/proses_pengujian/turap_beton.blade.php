<table class="table table-bordered">
    <thead class='thead-custom'>
        <tr>
            <th rowspan="3">No Benda Uji</th>
            <th rowspan="3">Tipe Benda Uji</th>
            <th rowspan="2">Dimensi Benda Uji</th>
            <th
                colspan="2"
                rowspan="2"
            >Konfigurasi Pembebanan (m)</th>
            <th rowspan="3">Berat alat bantu, W (tf)</th>
            <th rowspan="3">Grafitasi, g0 (m2/s)</th>
            <th colspan="18">Hasil Pengujian</th>
            <th rowspan="3">Keterangan</th>
        </tr>
        <tr>
            <th colspan="6">Retak Pertama</th>
            <th colspan="6">Retak 0,3 mm</th>
            <th colspan="6">Maksimum</th>
        </tr>
        <tr>
            <th>Panjang, L</th>
            <th>Panjang Bentang, l(L/2)</th>
            <th>Titik Beban (L/3)</th>
            <th colspan="4">F load cell</th>
            <th>δ ½ L (mm)</th>
            <th>M (kN.m)</th>

            <th colspan="4">F load cell</th>
            <th>δ ½ L (mm)</th>
            <th>M (kN.m)</th>

            <th colspan="4">F load cell</th>
            <th>δ ½ L (mm)</th>
            <th>M (kN.m)</th>
        </tr>
    </thead>
    <tbody>
        <tr name="line_items">
            @php
                $value = json_decode($parameter->pengujian->last()->pivot->hasil_pengujian ?? null);
            @endphp
            <td>
                <p>{{ $parameter->sample->kode_sampel }}</p>
            </td>
            <td>
                <p>{{ $parameter->product_type }}</p>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="panjang"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->panjang ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="panjang_bentang"
                    jAutoCalc="{panjang} / 2"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="titk_beban"
                    jAutoCalc="{panjang_bentang} / 3"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="berat_alat_bantu"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->berat_alat_bantu ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="gravity"
                    value="9.81"
                    readonly
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_pertama_f_load_cell"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->retak_pertama_f_load_cell ?? '' }}"
                />
            </td>
            <td>
                <p>tf</p>
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="retak_pertama_f"
                    readonly
                    jAutoCalc="{retak_pertama_f_load_cell} * {gravity}"
                />
            </td>
            <td>
                <p>kN</p>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_pertama_l"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->retak_pertama_l ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_pertama_m"
                    readonly
                    jAutoCalc="1 / 6 * ({retak_pertama_f} + {berat_alat_bantu} * {gravity}) * {panjang_bentang}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_mm_f_load_cell"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->retak_mm_f_load_cell ?? '' }}"
                />
            </td>
            <td>
                <p>tf</p>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_mm_f"
                    readonly
                    jAutoCalc="{retak_mm_f_load_cell} * {gravity}"
                />
            </td>
            <td>
                <p>kN</p>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_mm_l"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->retak_mm_l ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_mm_m_kn_compute"
                    readonly
                    jAutoCalc="1 / 6 * ({retak_mm_f} + {berat_alat_bantu} * {gravity}) * {panjang_bentang}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="maksimum_f_load_cell"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->maksimum_f_load_cell ?? '' }}"
                />
            </td>
            <td>
                <p>tf</p>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="maksimum_f"
                    readonly
                    jAutoCalc="{maksimum_f_load_cell} * {gravity}"
                />
            </td>
            <td>
                <p>kN</p>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control maksimum-l"
                    name="maksimum_l"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->maksimum_l ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control maksimum-m-kn-compute"
                    name="maksimum_m_kn_compute"
                    readonly
                    jAutoCalc="1 / 6 * ({maksimum_f} + {berat_alat_bantu} * {gravity}) * {panjang_bentang}"
                />
            </td>
            <td>
                <textarea
                    class="form-control keterangan"
                    name="keterangan"
                    style="width: 250px"
                    @if (!is_null($value)) readonly @endif
                >{{ $value->keterangan ?? '' }}</textarea>
            </td>
        </tr>
    </tbody>
</table>
