@extends('layouts.adminlte')
@push('styles')
    <style>
        .table tbody tr td>* {
            width: 150px
        }

        .table tr th,
        .table tr td {
            text-align: center;
            vertical-align: middle !important;
        }

    </style>
@endpush
@section('main')
    <div class="container">
        <div class="card card-block">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-4 moksd">
                        <div class="float-left">
                            Proses Pengujian
                        </div>
                    </div>
                </div>
            </div>

            <form
                id="form-pengujian"
                method="POST"
                action="{{ route('admin.permintaan-pengujian.proses-pengujian.hasil-pengujian.update', [$proses_pengujian->id, $item->id]) }}"
            >
                @csrf
                @method('PUT')
                <div
                    x-data="{jenis_pengujian: '{{ $parameter->hasil_pengujian->jenis_pengujian ?? '' }}'}"
                    class="card-body"
                >
                    <div class="form-group">
                        <label for="tanggal_pengujian">Jenis Pengujian</label>
                        <select
                            name="jenis_pengujian"
                            class="form-control"
                            x-on:change="jenis_pengujian = $event.target.value"
                        >
                            <option value="">Pilih jenis pengujian</option>
                            <option
                                value="tiang-pancang-bulat"
                                x-bind:selected="jenis_pengujian == 'tiang-pancang-bulat'"
                            >Tiang Pancang Bulat</option>
                            <option
                                value="tiang-pancang-kotak"
                                x-bind:selected="jenis_pengujian == 'tiang-pancang-kotak'"
                            >Tiang Pancang Kotak</option>
                            <option
                                value="turap-beton"
                                x-bind:selected="jenis_pengujian == 'turap-beton'"
                            >Turap Beton</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tanggal_pengujian">Alat Pengujian</label>
                        @include('admin.utils.barcode_scanner')
                    </div>
                    <div class="form-group">
                        <label for="tanggal_pengujian">Rumus Pengujian</label>
                        <div class="table-responsive">
                            <div x-show="jenis_pengujian === 'tiang-pancang-bulat'">
                                @include('admin.permintaan_pengujian.proses_pengujian.tiang-pancang-bulat', [
                                'parameter' => $item
                                ])
                            </div>
                            <div x-show="jenis_pengujian === 'tiang-pancang-kotak'">
                                @include('admin.permintaan_pengujian.proses_pengujian.tiang_pancang_kotak', [
                                'parameter' => $item
                                ])
                            </div>
                            <div x-show="jenis_pengujian === 'turap-beton'">
                                @include('admin.permintaan_pengujian.proses_pengujian.turap_beton', [
                                'parameter' => $item
                                ])
                            </div>
                        </div>
                    </div>

                    <div class="form-group float-right">
                        <button class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('adminlte/plugins/jsautocalc/jautocalc.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('table tr[name=line_items]').jAutoCalc({
                keyEventsFire: true,
                emptyAsZero: true,
                smartIntegers: true,
                decimalOpts: ['.', ','],
                decimalPlaces: 3,
                readOnlyResults: true,
            });
            $('#form-pengujian').on('submit', function(e) {
                e.preventDefault();
                let $form = $(this).closest('form');

                swalWithBootstrapButtons.fire({
                    icon: 'info',
                    title: 'Apakah anda yakin?',
                    text: 'Pastikan data yang anda masukkan untuk melakukan pengujian sudah benar',
                    confirmButtonText: 'Ya, saya yakin!',
                    cancelButtonText: 'Batal',
                    showCancelButton: true,
                    showConfirmButton: true,
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-secondary mr-2',
                    },
                    preConfirm: () => {
                        const formData = $form.serialize();
                        return $.ajax({
                            url: $form.attr('action'),
                            dataType: 'json',
                            type: 'PUT',
                            data: formData,
                            error: function(error) {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        swalWithBootstrapButtons.fire({
                            icon: 'success',
                            title: 'Berhasil!',
                            text: "Pengujian diselesaikan",
                        }).then(() => {
                            window.location.href = route(
                                'admin.permintaan-pengujian.proses-pengujian.edit', [
                                    '{{ $proses_pengujian->id }}'
                                ]
                            );
                        })
                    }
                })
            })
        })
    </script>
@endpush
