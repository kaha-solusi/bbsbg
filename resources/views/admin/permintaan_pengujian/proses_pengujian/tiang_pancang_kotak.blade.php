<table class="table table-bordered">
    <thead class='thead-custom'>
        <tr>
            <th rowspan="3">No Benda Uji</th>
            <th rowspan="3">Tipe Benda Uji</th>
            <th colspan="
                9">Dimensi Benda Uji</th>
            <th rowspan="3">Massa tiang pancang, m (tf)</th>
            <th colspan="2">Konfigurasi Pembebanan (m)</th>
            <th rowspan="3">Berat alat bantu, W (tf)</th>
            <th rowspan="3">Berat alat bantu, W (kN)</th>
            <th rowspan="3">Grafitasi, g0 (m2/s)</th>
            <th colspan="18">Hasil Pengujian</th>
            <th rowspan="3">Keterangan</th>
        </tr>
        <tr>
            <th colspan="4">Lebar tiang pancang, b</th>
            <th colspan="4">Tinggi Tiang Pancang, h</th>
            <th rowspan="2">Panjang, L</th>
            <th rowspan="2">Panjang Bentang (3/5 L)</th>
            <th rowspan="2">Tumpuan (1/5L)</th>
            <th colspan="6">Retak Pertama</th>
            <th colspan="6">Retak 0,3 mm</th>
            <th colspan="6">Maksimum</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>Rata - Rata</th>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>Rata - Rata</th>

            <th colspan="4">F load cell</th>
            <th>δ ½ L (mm)</th>
            <th>M (kN.m)</th>

            <th colspan="4">F load cell</th>
            <th>δ ½ L (mm)</th>
            <th>M (kN.m)</th>

            <th colspan="4">F load cell</th>
            <th>δ ½ L (mm)</th>
            <th>M (kN.m)</th>
        </tr>
    </thead>
    <tbody>
        <tr name="line_items">
            @php
                $value = json_decode($parameter->pengujian->last()->pivot->hasil_pengujian ?? null);
            @endphp
            <td>
                <p>{{ $parameter->sample->kode_sampel }}</p>
            </td>
            <td>
                <p>{{ $parameter->product_type }}</p>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="d1"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->d1 ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="d2"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->d2 ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="d3"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->d3 ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="dmean"
                    jAutoCalc="({d1} + {d2} + {d3}) / 3"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="t1"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->t1 ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="t2"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->t2 ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="t3"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->t3 ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="tmean"
                    readonly
                    jAutoCalc="({t1} + {t2} + {t3}) / 3"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="panjang"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->panjang ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="massa_tiang_pancang"
                    readonly
                    jAutoCalc="2.6 * {tmean} * {dmean} * {panjang}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="panjang-bentang"
                    readonly
                    jAutoCalc="{massa_tiang_pancang} * 3 / 5"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="tumpuan"
                    readonly
                    jAutoCalc="{massa_tiang_pancang} * 1 / 5"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="berat_alat_bantu_w_tf"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->berat_alat_bantu_w_tf ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="berat_alat_bantu_w_kn"
                    readonly
                    jAutoCalc="{berat_alat_bantu_w_tf} * {gravity}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="gravity"
                    value="9.81"
                    readonly
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_pertama_f_load_cell"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->retak_pertama_f_load_cell ?? '' }}"
                />
            </td>
            <td>
                <p>tf</p>
            </td>
            <td class="text-center align-middle">
                <input
                    type="number"
                    class="form-control"
                    name="retak_pertama"
                    readonly
                    jAutoCalc="{retak_pertama_f_load_cell} * {gravity}"
                />
            </td>
            <td>
                <p>kN</p>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_pertama_l"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->retak_pertama_l ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_pertama_m"
                    readonly
                    jAutoCalc="1 / 40 * {gravity} * {massa_tiang_pancang} * {panjang} + ({retak_pertama} + {berat_alat_bantu_w_kn}) / 4 * ((3 / 5 * {panjang}) - 1)"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_mm_f_load_cell"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->retak_mm_f_load_cell ?? '' }}"
                />
            </td>
            <td>
                <p>tf</p>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_mm_compute"
                    readonly
                    jAutoCalc="{retak_mm_f_load_cell} * {gravity}"
                />
            </td>
            <td>
                <p>kN</p>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_mm_l"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->retak_mm_l ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="retak_mm_m_kn_compute"
                    readonly
                    jAutoCalc="1 / 40 * {gravity} * {massa_tiang_pancang} * {panjang} + ({retak_mm_compute} + {berat_alat_bantu_w_kn}) / 4 * ((3 / 5 * {panjang}) - 1)"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="maksimum_f_load_cell"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->maksimum_f_load_cell ?? '' }}"
                />
            </td>
            <td>
                <p>tf</p>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control"
                    name="maksimum_compute"
                    readonly
                    jAutoCalc="{maksimum_f_load_cell} * {gravity}"
                />
            </td>
            <td>
                <p>kN</p>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control maksimum-l"
                    name="maksimum_l"
                    step="0.001"
                    @if (!is_null($value)) readonly @endif
                    value="{{ $value->maksimum_l ?? '' }}"
                />
            </td>
            <td>
                <input
                    type="number"
                    class="form-control maksimum-m-kn-compute"
                    name="maksimum_m_kn_compute"
                    readonly
                    jAutoCalc="1 / 40 * {gravity} * {massa_tiang_pancang} * {panjang} + ({maksimum_compute} + {berat_alat_bantu_w_kn}) / 4 * ((3 / 5 * {panjang}) - 1)"
                />
            </td>
            <td>
                <textarea
                    class="form-control keterangan"
                    name="keterangan"
                    style="width: 250px"
                    @if (!is_null($value)) readonly @endif
                >{{ $value->keterangan ?? '' }}</textarea>
            </td>
        </tr>
    </tbody>
</table>
