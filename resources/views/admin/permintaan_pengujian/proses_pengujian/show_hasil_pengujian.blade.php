@extends('layouts.adminlte')

@section('main')
    <div class="container">
        <div class="card card-block">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-4 moksd">
                        <div class="float-left">
                            Proses Pengujian
                        </div>
                    </div>
                </div>
            </div>
            <div
                x-data="{jenis_pengujian: '{{ $item->pengujian->last()->pivot->kategori_pengujian ?? '' }}'}"
                class="card-body"
            >
                <div class="form-group">
                    <label for="tanggal_pengujian">Jenis Pengujian</label>
                    <select
                        name="jenis_pengujian"
                        class="form-control"
                        x-on:change="jenis_pengujian = $event.target.value"
                        disabled
                    >
                        <option value="">Pilih jenis pengujian</option>
                        <option
                            value="tiang-pancang-bulat"
                            x-bind:selected="jenis_pengujian == 'tiang-pancang-bulat'"
                        >Tiang Pancang Bulat</option>
                        <option
                            value="tiang-pancang-kotak"
                            x-bind:selected="jenis_pengujian == 'tiang-pancang-kotak'"
                        >Tiang Pancang Kotak</option>
                        <option
                            value="turap-beton"
                            x-bind:selected="jenis_pengujian == 'turap-beton'"
                        >Turap Beton</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="tanggal_pengujian">Alat Pengujian</label>
                    {{-- @dd($proses_pengujian->items->where('item_id', $item->id)->last()->peralatan) --}}
                    <div class="input-group">
                        <select
                            name='alat_pengujian[]'
                            class="form-control peralatan-list"
                            multiple
                            disabled
                        >
                            <option>Pilih Alat Pengujian</option>
                            @foreach ($proses_pengujian->items->where('item_id', $item->id)->last()->peralatan as $alat)
                                <option
                                    value="{{ $alat->peralatan_lab->id }}"
                                    selected
                                >
                                    <span>
                                        @if (!is_null($alat->peralatan_lab->foto_alat))
                                            <img
                                                class="img-flag mr-2"
                                                width="50"
                                                height="50"
                                                src="{{ $alat->peralatan_lab->foto_alat }}"
                                            />
                                        @endif
                                        <span>{{ $alat->peralatan_lab->nama }}</span>
                                    </span>
                                    {{ $alat->peralatan_lab->nama }}
                                </option>
                            @endforeach
                            {{-- @foreach ($parameter->peralatan as $peralatan)
                                    <option
                                        value="{{ $peralatan->id }}"
                                        selected
                                    >
                                        <span>
                                            @if (!is_null($peralatan->foto_alat))
                                                <img
                                                    class="img-flag mr-2"
                                                    width="50"
                                                    height="50"
                                                    src="{{ $peralatan->foto_alat }}"
                                                />
                                            @endif
                                            <span>{{ $peralatan->nama }}</span>
                                        </span>
                                        {{ $peralatan->nama }}
                                    </option>
                                @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tanggal_pengujian">Rumus Pengujian</label>
                    <div class="table-responsive">
                        <div x-show="jenis_pengujian === 'tiang-pancang-bulat'">
                            @include('admin.permintaan_pengujian.proses_pengujian.tiang-pancang-bulat', [
                            'parameter' => $item
                            ])
                        </div>
                    </div>
                </div>

                <div class="form-group float-right">
                    <a
                        class="btn btn-secondary"
                        href="{{ route('admin.permintaan-pengujian.proses-pengujian.edit', $proses_pengujian->id) }}"
                    >Kembali</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            const format = (item) => {
                if (!item.id) {
                    return item.text;
                }

                var url = item.element.getAttribute('data-avatar');

                var ahref = '';
                if (item.foto_alat) {
                    var img = $("<img>", {
                        class: "rounded-circle me-2",
                        width: 25,
                        height: 25,
                        src: item.foto_alat
                    });
                    var ahref = $("<a>", {
                        href: item.foto_alat,
                    });
                    ahref.attr('target', '_blank');
                    ahref.append(img);
                }

                var span = $("<span>", {
                    text: " " + item.text
                });
                span.prepend(ahref);
                return span;
            }

            function formatState(state) {
                if (!state.id) {
                    return state.text;
                }

                var baseUrl = "/user/pages/images/flags";
                var $state = $(
                    `<span>${state.foto_alat && '<img class="img-flag mr-2" width="50" height="50" />' || ''} <span></span></span>`
                );

                $state.find("span").text(state.text);
                if (state.foto_alat) {
                    $state.find("img").attr("src", state.foto_alat);
                }

                return $state;
            };

            $peralatans = $('.peralatan-list').select2({
                theme: "bootstrap4",
                placeholder: "Pilih Peralatan",
                ajax: {
                    url: "{{ route('admin.peralatan.get-peralatan-by-name') }}",
                    cache: true,
                    delay: 250,
                },
                minimumInputLength: 2,
                templateSelection: format,
                templateResult: formatState
            });
        })
    </script>
@endpush
