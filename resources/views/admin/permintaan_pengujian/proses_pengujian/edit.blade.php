@extends('layouts.adminlte')

@section('main')
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4 moksd">
                                <div class="float-left">
                                    Data Pengujian
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="">Nomor SPK</label>
                            <input
                                type="text"
                                class="form-control"
                                value="{{ old('nomor_spk', $proses_pengujian->permintaan_pengujian->spk->nomor) }}"
                                readonly
                            />
                        </div>
                        <div class="form-group">
                            <label for="tanggal_pengujian">Tanggal Pengujian</label>
                            <input
                                type="text"
                                class="form-control"
                                id="tanggal_pengujian"
                                disabled
                                value="{{ old('tanggal_pengujian', $proses_pengujian->permintaan_pengujian->spk->tanggal_mulai) }}"
                            >
                        </div>
                        <div class="form-group">
                            <label for="tanggal_pengujian">Jenis Pengujian</label>
                            <input
                                type="text"
                                class="form-control"
                                id="tanggal_pengujian"
                                disabled
                                value="{{ old('tanggal_pengujian', $proses_pengujian->permintaan_pengujian->master_layanan_uji->name) }}"
                            >
                        </div>
                        <div class="form-group">
                            <label for="tanggal_pengujian">Benda Uji</label>
                            <table class="table table-bordered">
                                <thead class="thead-custom">
                                    <tr>
                                        <th width="50px">No</th>
                                        <th>Parameter Pengujian</th>
                                        <th>Kode Benda Uji</th>
                                        <th>Nama Benda Uji</th>
                                        <th
                                            width="160px"
                                            class="text-center"
                                        >Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $isComplete = 0;
                                    @endphp
                                    @foreach ($proses_pengujian->permintaan_pengujian->samples->last()->items as $no => $item)
                                        <tr>
                                            <td>{{ ++$no }}</td>
                                            <td>{{ $item->layanan_uji_item->name }}</td>
                                            <td>
                                                {{ $item->pivot->kode_sampel }}
                                            </td>
                                            <td>{{ $item->product_name }}</td>
                                            <td>
                                                @if (is_null($item->pengujian->last()))
                                                    <a
                                                        href="{{ route('admin.permintaan-pengujian.proses-pengujian.hasil-pengujian.edit', [$proses_pengujian->id, $item->id]) }}"
                                                        class="btn btn-sm btn-primary text-center btn-block"
                                                    >Lakukan Pengujian</a>
                                                @else
                                                    @php
                                                        $isComplete++;
                                                    @endphp
                                                    <a
                                                        href="{{ route('admin.permintaan-pengujian.proses-pengujian.hasil-pengujian.show', [$proses_pengujian->id, $item->id]) }}"
                                                        class="btn btn-sm btn-secondary text-center btn-block"
                                                    >Lihat Hasil</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        @if ($proses_pengujian->permintaan_pengujian->samples->last()->items->count() === $isComplete && $proses_pengujian->status !== 'submitted')
                            <div class="form-group float-right">
                                <a
                                    href="{{ route('admin.permintaan-pengujian.laporan-hasil-uji.store') }}"
                                    class="btn btn-primary"
                                    id="create-lhu"
                                >
                                    Ajukan LHU
                                </a>
                            </div>
                        @else
                            <div class="form-group float-right">
                                <a
                                    href="{{ route('admin.permintaan-pengujian.proses-pengujian.index') }}"
                                    class="btn btn-secondary"
                                >
                                    Kembali
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('#create-lhu').on('click', function(e) {
                e.preventDefault();

                const self = $(this);
                swalWithBootstrapButtons.fire({
                    icon: 'info',
                    title: 'Apakah anda yakin?',
                    text: 'Pastikan pengujian yang anda lakukan sudah benar',
                    confirmButtonText: 'Ya, saya yakin!',
                    cancelButtonText: 'Batal',
                    showCancelButton: true,
                    showConfirmButton: true,
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-secondary mr-2',
                    },
                    preConfirm: () => {
                        return $.ajax({
                            url: self.attr('href'),
                            dataType: 'json',
                            type: 'POST',
                            data: {
                                _token: '{{ csrf_token() }}',
                                pengujian_id: '{{ $proses_pengujian->id }}',
                            },
                            error: function(error) {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        swalWithBootstrapButtons.fire({
                            icon: 'success',
                            title: 'Berhasil!',
                            text: "Pengujian diselesaikan",
                        }).then(() => {
                            window.location.href = route(
                                'admin.permintaan-pengujian.proses-pengujian.index'
                            );
                        })
                    }
                })
            })
        });
    </script>
@endpush
