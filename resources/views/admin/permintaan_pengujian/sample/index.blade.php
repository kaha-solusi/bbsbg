@extends('layouts.adminlte')
@section('main')
    @php
    $data = $permintaan_pengujian->test_samples->last();
    @endphp
    <div class="container mt-3">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="float-left">
                                    Formulir Konfirmasi Penerimaan Benda Uji
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    method="POST"
                                    enctype="multipart/form-data"
                                    action="{{ route('admin.permintaan-pengujian.penerimaan-benda-uji.store', $permintaan_pengujian->id) }}"
                                >
                                    @csrf
                                    @if ($permintaan_pengujian->status === \App\Models\PermintaanPengujian::STATUS_WAITING_SAMPLE)
                                        <div class="form-group">
                                            <label for="jasa_ekspedisi">Jasa Ekspedisi</label>
                                            <input
                                                type="text"
                                                class="form-control"
                                                id="jasa_ekspedisi"
                                                name="jasa_ekspedisi"
                                                disabled
                                                value="{{ $data->jasa_ekspedisi }}"
                                            >
                                        </div>
                                        <div class="form-group">
                                            <label for="no_resi">No. Resi</label>
                                            <input
                                                type="text"
                                                class="form-control"
                                                id="no_resi"
                                                name="no_resi"
                                                disabled
                                                value="{{ $data->no_resi }}"
                                            >
                                        </div>
                                        <div class="form-group">
                                            <label for="no_resi">Bukti Pengiriman</label>
                                            <a
                                                href="/{{ $data->bukti_pengiriman->path }}"
                                                target="_blank"
                                            >
                                                <p>{{ $data->bukti_pengiriman->filename }}</p>
                                            </a>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label for="no_resi">Daftar Benda Uji</label>
                                        <table
                                            class="table table-hover w-100"
                                            id="table-sample"
                                        >
                                            <thead class="bg-hijau">
                                                <tr>
                                                    <th>Nama Benda Uji<sup>1</sup></th>
                                                    <th>No Benda Uji<sup>2</sup></th>
                                                    <th>Tipe Benda Uji <sup>3</sup></th>
                                                    <th
                                                        width="12%"
                                                        class='text-center'
                                                    >Status <sup>5</sup></th>
                                                    <th>Keterangan <sup>6</sup></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($permintaan_pengujian->items as $key => $item)
                                                    <tr>
                                                        <td>
                                                            <input
                                                                type='hidden'
                                                                value="{{ $item->id }}"
                                                                name="test_sample[{{ $key }}][id]"
                                                            />
                                                            <input
                                                                type='text'
                                                                class="form-control"
                                                                disabled
                                                                value="{{ $item->product_name }}"
                                                            />
                                                        </td>
                                                        <td>
                                                            <input
                                                                type="text"
                                                                class="form-control"
                                                                name="test_sample[{{ $key }}][no_sampel]"
                                                                value="{{ $item->no_sample }}"
                                                            />
                                                        </td>
                                                        <td>
                                                            <input
                                                                type='text'
                                                                class="form-control"
                                                                disabled
                                                                value="{{ $item->product_type }}"
                                                            />
                                                        </td>
                                                        <td>
                                                            <select
                                                                name="test_sample[{{ $key }}][status]"
                                                                class="form-control"
                                                            >
                                                                <option value="baik">Baik</option>
                                                                <option value="cacat">Cacat</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input
                                                                type="text"
                                                                class="form-control"
                                                                name="test_sample[{{ $key }}][keterangan]"
                                                                value="{{ $item->keterangan }}"
                                                            />
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="3">Tidak ada produk</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>

                                    @if ($permintaan_pengujian->status === \App\Models\PermintaanPengujian::STATUS_SAMPLE_SUBMITTED)
                                        <div class="form-group">
                                            <div class="float-right">
                                                <a
                                                    href="#rejectModal"
                                                    class="btn btn-danger"
                                                    data-toggle="modal"
                                                    data-target="#rejectModal"
                                                >Tolak</a>
                                                <button
                                                    type="submit"
                                                    class="btn btn-primary"
                                                >Terima</button>
                                            </div>
                                        </div>
                                    @endif

                                    @if ($permintaan_pengujian->status === \App\Models\PermintaanPengujian::STATUS_SAMPLE_RECEIVED)
                                        <div class="form-group">
                                            <div class="float-right">
                                                <a
                                                    href="{{ route('admin.permintaan-pengujian.spk.index', $permintaan_pengujian->id) }}"
                                                    type="submit"
                                                    class="btn btn-primary"
                                                >Buat SPK</a>
                                            </div>
                                        </div>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div
        class="modal fade"
        id="rejectModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="rejectModalLabel"
        aria-hidden="true"
    >
        <div
            class="modal-dialog"
            role="document"
        >
            <div class="modal-content">
                <form
                    id="reject-form"
                    action="{{ route('admin.permintaan-pengujian.penerimaan-benda-uji.store', $permintaan_pengujian->id) }}"
                    method="POST"
                >
                    @csrf
                    <div class="modal-header">
                        <h5
                            class="modal-title"
                            id="exampleModalLabel"
                        >Alasan Ditolak</h5>
                        <button
                            type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close"
                        >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <textarea
                                class="form-control"
                                id="reject-reason"
                                name="reject-reason"
                                rows="3"
                                required
                            ></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button
                            type="button"
                            class="btn btn-secondary"
                            data-dismiss="modal"
                        >Batal</button>
                        <button
                            type="submit"
                            class="btn btn-primary"
                        >Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script>
        $(document).ready(function() {
            $('#table-sample').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endpush
