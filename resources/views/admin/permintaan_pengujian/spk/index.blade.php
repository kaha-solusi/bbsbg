@extends('layouts.adminlte')
@section('main')
    @php
    $data = $permintaan_pengujian->test_samples->last();
    @endphp
    <div class="container mt-3">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="float-left">
                                    Formulir Pembuatan SPK
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    method="POST"
                                    enctype="multipart/form-data"
                                    action="{{ route('admin.permintaan-pengujian.spk.store', $permintaan_pengujian->id) }}"
                                >
                                    @csrf
                                    <div class="form-group">
                                        <label for="">No. SPK</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="nomor"
                                        />
                                    </div>
                                    <div class="form-group">
                                        <label for="">Untuk Melakukan</label>
                                        <select
                                            class="form-control"
                                            name="kategori"
                                        >
                                            <option value="set-up">Set Up dan Pengujian</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Jenis Pengujian</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="jenis_pengujian"
                                            disabled
                                            value="{{ $permintaan_pengujian->master_layanan_uji->name }}"
                                        />
                                    </div>
                                    <div class="form-group">
                                        <label for="">Metode Pengujian</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="metode_pengujian"
                                            value="{{ $permintaan_pengujian->items->pluck('layanan_uji_item.standard')->join(', ') ?? '' }}"
                                        />
                                    </div>
                                    <div class="form-group">
                                        <label for="">No Sampel</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="no_sample"
                                            disabled
                                            value="{{ $permintaan_pengujian->items->pluck('no_sample')->join(', ') }}"
                                        />
                                    </div>

                                    <div class="form-group">
                                        <label for="">Tanggal Mulai</label>
                                        <input
                                            type="date"
                                            class="form-control"
                                            name="tanggal_mulai"
                                        />
                                    </div>
                                    <div class="form-group">
                                        <label for="">Tanggal Selesai</label>
                                        <input
                                            type="date"
                                            class="form-control"
                                            name="tanggal_selesai"
                                        />
                                    </div>
                                    <div class="form-group">
                                        <label for="">Catatan</label>
                                        <textarea
                                            class="form-control"
                                            name="catatan"
                                        ></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="no_resi">Menugaskan Pengujian Kepada</label>
                                        <table
                                            class="table table-hover w-100"
                                            id="table-sample"
                                        >
                                            <thead class="bg-hijau">
                                                <tr>
                                                    <th width="20%">Nama Pegawai</th>
                                                    <th width="20%">NIP/NRP</th>
                                                    <th width="30%">Jabatan</th>
                                                    <th width="20%">Keterangan</th>
                                                    <th width="10%">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>

                                        <div class="d-flex">
                                            <button
                                                class="btn btn-sm btn-primary"
                                                id='add-more-test'
                                            >Tambah Pegawai</button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="float-right">
                                            <button
                                                type="submit"
                                                class="btn btn-primary"
                                            >Buat SPK</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div
        class="modal fade"
        id="barcodeModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="barcodeModalLabel"
        aria-hidden="true"
    >
        <div
            class="modal-dialog"
            role="document"
        >
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="exampleModalLabel"
                    >Scan Barcode Peralatan</h5>

                </div>
                <div class="modal-body">
                    <div id="qr-reader"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    {{-- <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script> --}}

    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script>
        $(document).ready(function() {
            const format = (item) => {
                if (!item.id) {
                    return item.text;
                }

                var url = item.element.getAttribute('data-avatar');

                var ahref = '';
                if (item.foto_alat) {
                    var img = $("<img>", {
                        class: "rounded-circle me-2",
                        width: 25,
                        height: 25,
                        src: item.foto_alat
                    });
                    var ahref = $("<a>", {
                        href: item.foto_alat,
                    });
                    ahref.attr('target', '_blank');
                    ahref.append(img);
                }

                var span = $("<span>", {
                    text: " " + item.text
                });
                span.prepend(ahref);
                return span;
            }

            function formatState(state) {
                if (!state.id) {
                    return state.text;
                }

                var baseUrl = "/user/pages/images/flags";
                var $state = $(
                    `<span>${state.foto_alat && '<img class="img-flag mr-2" width="50" height="50" />' || ''} <span></span></span>`
                );

                $state.find("span").text(state.text);
                if (state.foto_alat) {
                    $state.find("img").attr("src", state.foto_alat);
                }

                return $state;
            };

            $peralatans = $('.peralatan-list').select2({
                theme: "bootstrap4",
                placeholder: "Pilih Peralatan",
                ajax: {
                    url: "{{ route('admin.peralatan.get-peralatan-by-name') }}",
                    cache: true,
                    delay: 250,
                },
                minimumInputLength: 2,
                templateSelection: format,
                templateResult: formatState
            });

            // function onScanSuccess(decodedText, decodedResult) {
            //     $('#barcodeModal').modal('hide');
            //     $.get(`{{ route('admin.peralatan.get-peralatan-by-id') }}/?id=${decodedText}`, function(data) {
            //         var option = new Option(data.text, data.id, true, true);
            //         $('.peralatan-list').append(option).trigger('change');

            //         $peralatans.trigger({
            //             type: 'select2:select',
            //             params: {
            //                 data: data
            //             }
            //         });
            //     });
            // }

            // var html5QrcodeScanner = new Html5QrcodeScanner(
            //     "qr-reader", {
            //         fps: 10,
            //         qrbox: 250
            //     });

            // $('#barcodeModal').on('shown.bs.modal', function(e) {
            //     html5QrcodeScanner.render(onScanSuccess);
            // })
            // $('#barcodeModal').on('hide.bs.modal', function(e) {
            //     html5QrcodeScanner.clear();
            // })

            $pegawais = JSON.parse('{!! $pegawai !!}')

            let counter = 0;
            const $table = $('#table-sample').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                rowCallback: function(row, data, displayNum,
                    displayIndex, dataIndex) {

                    $(row).on('change', '.user-list',
                        function(e) {
                            const selectedID = $(this)
                                .val();
                            const selected = $pegawais.find(
                                f => f.id === Number(
                                    selectedID))

                            if (selected) {
                                $(row).find('.nip')
                                    .text(selected.nip)
                                $(row).find('.jabatan')
                                    .text(selected.jabatan.nama)
                                // $(row).find('.keterangan')
                                //     .val(pricePerUnit)
                            }
                        })

                    $(row).on('change', '.product_name', function(e) {
                        const product_name = $(this).val()
                        const product = productNames.find(f => f.name === product_name)
                        if (product) {
                            $productDesc.text(product.description);
                        }
                    });
                }
            });

            $table.on('draw.dt', function() {
                $('.user-list').select2({
                    theme: "bootstrap4",
                    placeholder: 'Pilih pegawai..',
                });
            });

            $('#table-sample tbody').on('click', '.item-remove',
                function() {
                    $table
                        .row($(this).parents('tr'))
                        .remove()
                        .draw();
                });
            const $addButton = $('#add-more-test')
            $addButton.on('click', function(e) {
                e.preventDefault();
                // $form.removeClass('was-validated');
                $table.row.add([
                    `<select class="form-control custom-select user-list" name="pegawai[]">
                        <option value="">Pilih pegawai..</option>
                        ${$pegawais.map(pegawai => `<option value="${pegawai.id}">${pegawai.user.name}</option>`)}
                    </select>`,
                    `<span type='text' class="nip"></span>`,
                    `<span type='text' class="jabatan"></span>`,
                    `<span type='text' class="keterangan" disabled></span>`,
                    `<div class='button-group'>
                            <input type='hidden' class="form-control" disabled name="pegawai[${counter}][id]" />
                            <a 
                                href="javascript:void(0)" 
                                class='btn btn-sm btn-danger item-remove' 
                                title='remove'>Hapus</a>
                        </div>`
                ]).draw(false);
            });
        });
    </script>
@endpush
