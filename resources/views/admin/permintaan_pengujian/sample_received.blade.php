@php
$pegawai = \App\Models\Pegawai::whereHas('jabatan')
    ->with(['user:id,name', 'jabatan:id,nama'])
    ->select('id', 'nip', 'jabatan_id', 'user_id')
    ->get();
@endphp
<div class="container mt-3">
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="float-left">
                                Formulir Pembuatan SPK
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form
                                method="POST"
                                enctype="multipart/form-data"
                                action="{{ route('admin.permintaan-pengujian.update', $pengujian->id) }}"
                            >
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="">No. SPK</label>
                                    <div
                                        x-data="{ editable: false }"
                                        class="input-group"
                                    >
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="nomor"
                                            x-bind:readonly="!editable"
                                            value="{{ old('nomor', '') }}"
                                        >
                                        <span class="input-group-append">
                                            <button
                                                type="button"
                                                class="btn btn-info btn-flat"
                                                x-on:click="editable = !editable"
                                                x-text="editable ? 'Simpan' : 'Ubah'"
                                            >Ubah</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Untuk Melakukan</label>
                                    <select
                                        class="form-control"
                                        name="kategori"
                                    >
                                        <option value="set-up">Set Up dan Pengujian</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Jenis Pengujian</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        name="jenis_pengujian"
                                        disabled
                                        value="{{ $pengujian->master_layanan_uji->name }}"
                                    />
                                </div>
                                <div class="form-group">
                                    <label for="">Metode Pengujian</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        name="metode_pengujian"
                                        value="{{ $pengujian->items->pluck('layanan_uji_item.standard')->unique()->join(', ') ?? '' }}"
                                        readonly
                                    />
                                </div>
                                <div class="form-group">
                                    <label for="">No Sampel</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        name="no_sample"
                                        disabled
                                        value="{{ $pengujian->items->pluck('no_sample')->join(', ') }}"
                                    />
                                </div>

                                <div class="form-group">
                                    <label for="">Tanggal Mulai</label>
                                    <input
                                        type="date"
                                        class="form-control"
                                        name="tanggal_mulai"
                                    />
                                </div>
                                <div class="form-group">
                                    <label for="">Tanggal Selesai</label>
                                    <input
                                        type="date"
                                        class="form-control"
                                        name="tanggal_selesai"
                                    />
                                </div>
                                <div class="form-group">
                                    <label for="">Catatan</label>
                                    <textarea
                                        class="form-control"
                                        name="catatan"
                                    ></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="no_resi">Menugaskan Pengujian Kepada</label>
                                    <table
                                        class="table table-hover w-100"
                                        id="table-sample"
                                    >
                                        <thead class="bg-hijau">
                                            <tr>
                                                <th width="20%">Nama Pegawai</th>
                                                <th width="20%">NIP/NRP</th>
                                                <th width="30%">Jabatan</th>
                                                <th width="20%">Keterangan</th>
                                                <th width="10%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>

                                    <div class="d-flex">
                                        <button
                                            class="btn btn-sm btn-primary"
                                            id='add-more-test'
                                        >Tambah Pegawai</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="float-right">
                                        <button
                                            type="submit"
                                            class="btn btn-primary"
                                        >Buat SPK</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script
        defer
        src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"
    ></script>
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $pegawais = JSON.parse('{!! $pegawai !!}')

            let counter = 0;
            const $table = $('#table-sample').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                rowCallback: function(row, data, displayNum,
                    displayIndex, dataIndex) {

                    $(row).on('change', '.user-list',
                        function(e) {
                            const selectedID = $(this)
                                .val();
                            const selected = $pegawais.find(
                                f => f.id === Number(
                                    selectedID))

                            if (selected) {
                                $(row).find('.nip')
                                    .text(selected.nip)
                                $(row).find('.jabatan')
                                    .text(selected.jabatan.nama)
                                // $(row).find('.keterangan')
                                //     .val(pricePerUnit)
                            }
                        })

                    $(row).on('change', '.product_name', function(e) {
                        const product_name = $(this).val()
                        const product = productNames.find(f => f.name === product_name)
                        if (product) {
                            $productDesc.text(product.description);
                        }
                    });
                }
            });

            $table.on('draw.dt', function() {
                $('.user-list').select2({
                    theme: "bootstrap4",
                    placeholder: 'Pilih pegawai..',
                });
            });

            $('#table-sample tbody').on('click', '.item-remove',
                function() {
                    $table
                        .row($(this).parents('tr'))
                        .remove()
                        .draw();
                });
            const $addButton = $('#add-more-test')
            $addButton.on('click', function(e) {
                e.preventDefault();
                // $form.removeClass('was-validated');
                $table.row.add([
                    `<select class="form-control custom-select user-list" name="pegawai[]">
                        <option value="">Pilih pegawai..</option>
                        ${$pegawais.map(pegawai => `<option value="${pegawai.id}">${pegawai.user.name}</option>`)}
                    </select>`,
                    `<span type='text' class="nip"></span>`,
                    `<span type='text' class="jabatan"></span>`,
                    `<span type='text' class="keterangan" disabled></span>`,
                    `<div class='button-group'>
                            <input type='hidden' class="form-control" disabled name="pegawai[${counter}][id]" />
                            <a 
                                href="javascript:void(0)" 
                                class='btn btn-sm btn-danger item-remove' 
                                title='remove'>Hapus</a>
                        </div>`
                ]).draw(false);
            });
        });
    </script>
@endpush
