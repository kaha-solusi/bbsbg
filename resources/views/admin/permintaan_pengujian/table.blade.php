@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', ['judul' => [['judul' => 'Permintaan Pengujian']]])
    @if (session()->has('success'))
        <div
            class="alert alert-success"
            role="alert"
        >
            {{ session()->get('success') }}
        </div>
    @endif
    @if (session()->has('failed'))
        <div
            class="alert alert-danger"
            role="alert"
        >
            {{ session()->get('failed') }}
        </div>
    @endif
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4 moks">
                                <div class="float-left">
                                    Permintaan Pengujian
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row float-right">
                                    <div class="col-md-6">
                                        <a
                                            href="{{ route('admin.permintaan.export') }}"
                                            class="btn btn-warning"
                                        >Export Data Permintaan Pengujian</a>

                                    </div>
                                    <div class="col-md-6">
                                        <form
                                            action="{{ route('admin.permintaan-pengujian.index') }}"
                                            method="GET"
                                            accept-charset="utf-8"
                                        >
                                            @csrf
                                            <div class="input-group">
                                                <input
                                                    class="form-control"
                                                    type="text"
                                                    placeholder="Search"
                                                    aria-describedby="btn-search"
                                                    name="search"
                                                    value="{{ $search }}"
                                                ></input>
                                                <div class="input-group-append">
                                                    <button
                                                        class="btn btn-outline-secondary"
                                                        type="submit"
                                                        id="btn-search"
                                                    >Search</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead class="thead-custom">
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Tgl Permintaan</th>
                                            <th scope="col">Nama Pemohon</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Nomor HP</th>
                                            <th scope="col">Pelayanan</th>
                                            <th scope="col">Detail</th>
                                            <th scope="col">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @forelse($data as $d)
                                            <tr class="tbody-custom">
                                                <td>{{ $d->id }}</td>
                                                <td>{{ $d->tgl_permintaan }}</td>
                                                <td>{{ $d->nama_pemohon }}</td>
                                                <td>{{ $d->email }}</td>
                                                <td>{{ $d->nomor_hp_kontak }}</td>
                                                <td>{{ $d->pelayanan->nama }} ({{ $d->pelayanan->kategori->nama }})
                                                </td>
                                                <td>
                                                    <button
                                                        type="button"
                                                        onclick="sosialMedia({{ $d }})"
                                                        class="btn btn-light btn-sm"
                                                        data-toggle="modal"
                                                        data-target="#sosialMediaModal"
                                                    >Detail</button>
                                                </td>
                                                <td>
                                                    @if ($d->status == 'diproses')
                                                        <a
                                                            href="{{ route('admin.permintaan.diterima', $d->id) }}"
                                                            class="btn btn-success btn-sm"
                                                        >Terima</a>
                                                        <a
                                                            href="{{ route('admin.permintaan.ditolak', $d->id) }}"
                                                            class="btn btn-danger btn-sm"
                                                        >Tolak</a>
                                                    @elseif($d->status == 'ditolak')
                                                        Permintaan Ditolak
                                                    @elseif($d->status == 'diterima')
                                                        <a
                                                            href="{{ route('admin.permintaan.selesai', $d->id) }}"
                                                            class="btn btn-danger btn-sm"
                                                            title="Klik jika permintaan telah selesai dilaksanakan"
                                                        >Selesai</a>
                                                    @elseif($d->status == 'selesai')
                                                        Selesai
                                                    @endif
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tbody-custom">
                                                <td
                                                    colspan="8"
                                                    class="text-center"
                                                >Tidak ada data.</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                {{ $data->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal --}}
    <div
        class="modal fade"
        id="sosialMediaModal"
        tabindex="-1"
        aria-labelledby="mediaModalLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="mediaModalLabel"
                    >Detail data</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="list-group">
                        <li
                            class="list-group-item"
                            id="tgl_permintaan"
                        ></li>
                        <li
                            class="list-group-item"
                            id="nama_pemohon"
                        ></li>
                        <li
                            class="list-group-item"
                            id="alamat"
                        ></li>
                        <li
                            class="list-group-item"
                            id="nama_kontak"
                        ></li>
                        <li
                            class="list-group-item"
                            id="nomor_hp"
                        ></li>
                        <li
                            class="list-group-item"
                            id="email"
                        ></li>
                        <li
                            class="list-group-item"
                            id="jenis"
                        ></li>
                        <li
                            class="list-group-item"
                            id="nama_lhu"
                        ></li>
                        <li
                            class="list-group-item"
                            id="email_persuratan"
                        ></li>
                        <li
                            class="list-group-item"
                            id="tanggal_pengujian"
                        ></li>
                        <li
                            class="list-group-item"
                            id="detail_layanan"
                        ></li>
                        <li
                            class="list-group-item"
                            id="uraian_pengujian"
                        ></li>
                        <li
                            class="list-group-item"
                            id="status"
                        ></li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- End Modal --}}

    <form
        action=""
        id="formDelete"
        method="POST"
    >
        @csrf
        @method('DELETE')
    </form>
@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">
        function deleteData(id) {
            let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

            const formDelete = document.getElementById('formDelete')
            formDelete.action = '/admin/pejabat/' + id;

            if (r == true) {
                formDelete.submit();
            } else {
                alert('Penghapusan dibatalkan.');
            }
        }

        function sosialMedia(data) {
            $('#tgl_permintaan').html(`<b>Tgl Permintaan:</b><br>` + data.tgl_permintaan)
            $('#nama_pemohon').html(`<b>Nama Pemohon:</b><br>` + data.nama_pemohon)
            $('#alamat').html(`<b>Alamat:</b><br>` + data.alamat)
            $('#nama_kontak').html(`<b>Nama Kontak:</b><br>` + data.nama_kontak)
            $('#nomor_hp').html(`<b>Nomor HP:</b><br>` + data.nomor_hp_kontak)
            $('#email').html(`<b>Email:</b><br>` + data.email)
            $('#jenis').html(`<b>Pelayanan:</b><br>` + data.pelayanan.nama)
            $('#nama_lhu').html(`<b>Nama LHU:</b><br>` + ((data.nama_lhu == null) ? 'Tidak ada' : data.nama_lhu))
            $('#email_persuratan').html(`<b>Email Persuratan:</b><br>` + data.email_persuratan)
            $('#tanggal_pengujian').html(`<b>Tanggal Pengujian:</b><br>` + data.tanggal_pengujian)
            $('#detail_layanan').html(`<b>Detail Layanan:</b><br>` + data.detail_layanan)
            $('#uraian_pengujian').html(`<b>Uraian Pengujian:</b><br>` + data.uraian_pengujian)
            $('#status').html(`<b>Status:</b><br>` + data.status)
        }
    </script>
@endpush
