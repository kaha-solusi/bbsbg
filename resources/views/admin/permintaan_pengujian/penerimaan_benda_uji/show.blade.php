@extends('layouts.adminlte')
@section('main')
    <div class="container mt-3">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="float-left">
                                    Penerimaan Benda Uji
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @if ($penerimaan_benda_uji->status === 'rejected')
                            <div class="alert alert-danger">
                                <b>Penerimaan benda uji ditolak.</b>
                                <p>{{ $penerimaan_benda_uji->approval->last()->message ?? '' }}</p>
                            </div>
                        @endif
                        @if ($penerimaan_benda_uji->status === 'approved')
                            <div class="alert alert-success">
                                Benda uji telah diterima pada tanggal
                                {{ $penerimaan_benda_uji->approval->last()->created_at }}
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="jasa_ekspedisi">Jenis Pengiriman</label>
                            <input
                                type="text"
                                class="form-control"
                                disabled
                                value="{{ ucwords($penerimaan_benda_uji->jenis_pengiriman) }}"
                            >
                        </div>
                        @if ($penerimaan_benda_uji->jenis_pengiriman === 'ekspedisi')
                            <div class="form-group">
                                <label for="jasa_ekspedisi">Jasa Ekspedisi</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="jasa_ekspedisi"
                                    name="jasa_ekspedisi"
                                    disabled
                                    value="{{ $penerimaan_benda_uji->jasa_ekspedisi }}"
                                >
                            </div>
                            <div class="form-group">
                                <label for="no_resi">No. Resi</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="no_resi"
                                    name="no_resi"
                                    disabled
                                    value="{{ $penerimaan_benda_uji->no_resi }}"
                                >
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="no_resi">Bukti Pengiriman</label>
                            <div>
                                <a
                                    href="#"
                                    data-image="/{{ $penerimaan_benda_uji->bukti->path }}"
                                    class="btn btn-sm btn-info btn-show"
                                >
                                    Lihat Bukti Pengiriman
                                </a>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="no_resi">Daftar Benda Uji</label>
                            <table
                                class="table table-hover w-100"
                                id="table-sample"
                            >
                                <thead class="bg-hijau">
                                    <tr>
                                        <th>Nama Benda Uji<sup>1</sup></th>
                                        <th>No Benda Uji<sup>2</sup></th>
                                        <th>Tipe Benda Uji <sup>3</sup></th>
                                        <th
                                            width="12%"
                                            class='text-center'
                                        >Status <sup>5</sup></th>
                                        <th>Keterangan <sup>6</sup></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($penerimaan_benda_uji->items as $key => $item)
                                        <tr>
                                            <td>
                                                <input
                                                    type='text'
                                                    class="form-control"
                                                    disabled
                                                    value="{{ $item->product_name }}"
                                                />
                                            </td>
                                            <td>
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    disabled
                                                    value="{{ $item->pivot->kode_sampel }}"
                                                />
                                            </td>
                                            <td>
                                                <input
                                                    type='text'
                                                    class="form-control"
                                                    disabled
                                                    value="{{ $item->product_type }}"
                                                />
                                            </td>
                                            <td>
                                                <select
                                                    class="form-control"
                                                    disabled
                                                >
                                                    <option
                                                        value="baik"
                                                        @if ($item->pivot->status === 'baik') selected @endif
                                                    >Baik</option>
                                                    <option
                                                        value="cacat"
                                                        @if ($item->pivot->status === 'cacat') selected @endif
                                                    >Cacat</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    disabled
                                                    value="{{ $item->pivot->keterangan }}"
                                                />
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3">Tidak ada produk</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script>
        $(document).ready(function() {
            $('#table-sample').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });

            $('.btn-show').on('click', function() {
                const filepath = $(this).data('image');
                swalWithBootstrapButtons.fire({
                    title: 'Bukti Pengiriman Benda Uji',
                    showCancelButton: false,
                    showConfirmButton: false,
                    html: `<a target="_blank" href="${filepath}"><img src=${filepath} width="100%" /></a>`,
                    allowOutsideClick: () => !Swal.isLoading()
                })
            });
        });
    </script>
@endpush
