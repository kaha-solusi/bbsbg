@extends('layouts.adminlte')
@section('main')
    <div class="container mt-3">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="float-left">
                                    Penerimaan Benda Uji
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form
                            method="POST"
                            enctype="multipart/form-data"
                            id="form-penerimaan-benda-uji"
                            {{-- action="{{ route('admin.permintaan-pengujian.penerimaan-benda-uji.store', $permintaan_pengujian->id) }}" --}}
                        >
                            @csrf

                            <div class="form-group">
                                <label for="jasa_ekspedisi">Jenis Pengiriman</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    disabled
                                    value="{{ ucwords($penerimaan_benda_uji->jenis_pengiriman) }}"
                                >
                            </div>
                            @if ($penerimaan_benda_uji->jenis_pengiriman === 'ekspedisi')
                                <div class="form-group">
                                    <label for="jasa_ekspedisi">Jasa Ekspedisi</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="jasa_ekspedisi"
                                        name="jasa_ekspedisi"
                                        disabled
                                        value="{{ $penerimaan_benda_uji->jasa_ekspedisi }}"
                                    >
                                </div>
                                <div class="form-group">
                                    <label for="no_resi">No. Resi</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="no_resi"
                                        name="no_resi"
                                        disabled
                                        value="{{ $penerimaan_benda_uji->no_resi }}"
                                    >
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="no_resi">Bukti Pengiriman</label>
                                <div>
                                    <a
                                        href="#"
                                        data-image="/{{ $penerimaan_benda_uji->bukti->path }}"
                                        class="btn btn-sm btn-info btn-show"
                                    >
                                        Lihat Bukti Pengiriman
                                    </a>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="no_resi">Daftar Benda Uji</label>
                                <table
                                    class="table table-bordered w-100"
                                    id="table-sample"
                                >
                                    <thead class="bg-hijau">
                                        <tr>
                                            <th>Nama Benda Uji</th>
                                            <th>No Benda Uji</th>
                                            <th>Tipe Benda Uji</th>
                                            <th
                                                width="12%"
                                                class='text-center'
                                            >Status</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($penerimaan_benda_uji->permintaan_pengujian->items as $key => $item)
                                            <tr>
                                                <td>
                                                    <input
                                                        type='hidden'
                                                        value="{{ $item->id }}"
                                                        name="test_sample[{{ $key }}][id]"
                                                    />
                                                    <input
                                                        type='text'
                                                        class="form-control"
                                                        disabled
                                                        value="{{ $item->product_name }}"
                                                    />
                                                </td>
                                                <td>
                                                    <input
                                                        type="text"
                                                        class="form-control"
                                                        name="test_sample[{{ $key }}][no_sampel]"
                                                        value="{{ $item->no_sample }}"
                                                        autocomplete="off"
                                                    />
                                                </td>
                                                <td>
                                                    <input
                                                        type='text'
                                                        class="form-control"
                                                        disabled
                                                        value="{{ $item->product_type }}"
                                                    />
                                                </td>
                                                <td>
                                                    <select
                                                        name="test_sample[{{ $key }}][status]"
                                                        class="form-control"
                                                    >
                                                        <option value="baik">Baik</option>
                                                        <option value="cacat">Cacat</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input
                                                        type="text"
                                                        class="form-control"
                                                        name="test_sample[{{ $key }}][keterangan]"
                                                        value="{{ $item->keterangan }}"
                                                        autocomplete="off"
                                                    />
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="3">Tidak ada produk</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>

                            <div class="form-group float-right">
                                {{-- <a
                                    href="#"
                                    data-type="reject"
                                    class="btn btn-danger btn-reject"
                                >Tolak</a> --}}
                                <button
                                    type="submit"
                                    data-type="rejected"
                                    class="btn btn-danger"
                                >Tolak</button>
                                <button
                                    type="submit"
                                    data-type="approved"
                                    class="btn btn-primary"
                                >Terima</button>
                                {{-- <a
                                    href="#"
                                    class="btn btn-primary btn-approve"
                                >Terima</a> --}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script>
        $(document).ready(function() {
            const itemCount = '{{ $penerimaan_benda_uji->permintaan_pengujian->items->count() }}';
            const rules = {};
            const messages = {};

            for (let index = 0; index < itemCount; index += 1) {
                rules[`test_sample[${index}][no_sampel]`] = {
                    required: true,
                };
                messages[`test_sample[${index}][no_sampel]`] = 'Nomor benda uji harus diisi';
            }

            $('#table-sample').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });

            const approveBendaUji = (formData) => {
                formData.push({
                    name: '_token',
                    value: '{{ csrf_token() }}'
                });
                swalWithBootstrapButtons.fire({
                    icon: 'info',
                    title: 'Apakah anda yakin ingin menyetujui benda uji ini?',
                    text: 'Benda uji yang akan disetujui akan digunakan dalam pengujian',
                    confirmButtonText: 'Ya, setujui!',
                    cancelButtonText: 'Batal',
                    showCancelButton: true,
                    showConfirmButton: true,
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-secondary mr-2',
                    },
                    preConfirm: () => {
                        return $.ajax({
                            url: '{{ route('admin.permintaan-pengujian.penerimaan-benda-uji.approve', $penerimaan_benda_uji->id) }}',
                            dataType: 'json',
                            type: 'POST',
                            data: formData,
                            error: function(error) {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        swalWithBootstrapButtons.fire({
                            icon: 'success',
                            title: 'Berhasil!',
                            text: "Benda uji berhasi disetujui",
                        }).then(() => {
                            window.location.href = route(
                                'admin.permintaan-pengujian.penerimaan-benda-uji.index'
                            );
                        })
                    }
                })
            }

            const rejectBendaUji = (formData) => {
                formData.push({
                    name: '_token',
                    value: '{{ csrf_token() }}'
                });
                swalWithBootstrapButtons.fire({
                    icon: 'warning',
                    title: 'Apakah anda yakin ingin menolak benda uji ini?',
                    text: 'Benda uji yang ditolak akan menunggu pengiriman ulang oleh pelanggan sebelum memulai pengujian',
                    confirmButtonText: 'Ya, tolak!',
                    cancelButtonText: 'Batal',
                    showCancelButton: true,
                    showConfirmButton: true,
                    customClass: {
                        confirmButton: 'btn btn-danger',
                        cancelButton: 'btn btn-secondary mr-2',
                    },
                    input: 'textarea',
                    inputAttributes: {
                        autocapitalize: 'off',
                        name: 'message',
                        placeholder: 'Berikan alasan kenapa anda menolak benda uji ini',
                    },
                    preConfirm: (message) => {
                        formData.push({
                            name: 'message',
                            value: message
                        });
                        return $.ajax({
                            url: '{{ route('admin.permintaan-pengujian.penerimaan-benda-uji.reject', $penerimaan_benda_uji->id) }}',
                            dataType: 'json',
                            type: 'POST',
                            data: formData,
                            error: function(error) {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        swalWithBootstrapButtons.fire({
                            icon: 'success',
                            title: 'Berhasil!',
                            text: "Benda uji berhasil ditolak",
                        }).then(() => {
                            window.location.href = route(
                                'admin.permintaan-pengujian.penerimaan-benda-uji.index'
                            );
                        })
                    }
                })
            }

            $("#form-penerimaan-benda-uji").validate({
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass("is-invalid");
                },
                errorElement: "em",
                errorClass: 'invalid-feedback',
                focusInvalid: true, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                errorPlacement: function(error, element) {
                    if (element.attr("type") == "radio") {
                        error.insertAfter(element.parents('div').find('.radio-list'));
                    } else {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                },
                success: function(label) {
                    label
                        .closest('.form-group').removeClass(
                            'has-error'); // set success class to the control group
                },
                rules,
                messages,
                submitHandler: function(form) {
                    const formData = $("#form-penerimaan-benda-uji").serializeArray();
                    const submitButtonType = $(this.submitButton).data('type');
                    if (submitButtonType === 'approved') {
                        approveBendaUji(formData);
                    } else if (submitButtonType === 'rejected') {
                        rejectBendaUji(formData);
                    }
                }
            });

            $('.btn-show').on('click', function() {
                const filepath = $(this).data('image');
                swalWithBootstrapButtons.fire({
                    title: 'Bukti Pengiriman Benda Uji',
                    showCancelButton: false,
                    showConfirmButton: false,
                    html: `<a target="_blank" href="${filepath}"><img src=${filepath} width="100%" /></a>`,
                    allowOutsideClick: () => !Swal.isLoading()
                })
            });
        });
    </script>
@endpush
