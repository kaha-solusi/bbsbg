@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', ['judul' => [['judul' => 'Data Pelayanan']]])
    <div class="container">
        @if (session()->has('success'))
            <div
                class="alert alert-success"
                role="alert"
            >
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session()->has('failed'))
            <div
                class="alert alert-danger"
                role="alert"
            >
                {{ session()->get('failed') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <button
                                data-toggle="modal"
                                data-target="#import"
                                class="btn btn-sm btn-success"
                            >Import Data Pelayanan</button>
                            <a
                                href="{{ route('pelayanan.export', ['kategori' => $kategori->id]) }}"
                                class="btn btn-sm btn-warning"
                            >Export Data Pelayanan</a>
                            <a
                                href="{{ route('pelayanan.create', ['kategori' => $kategori->id]) }}"
                                class="btn btn-sm btn-primary"
                            >Tambah Data Pelayanan</a>
                            @if ($kategori->id == 3)
                                <a
                                    href="{{ route('bimtek.create') }}"
                                    class="btn btn-sm btn-primary float-right"
                                >Tambah Data Bimbingan Teknis</a>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover w-100">
                            <thead class="bg-hijau">
                                <tr>
                                    <td>No.</td>
                                    <td>Nama Pelayanan</td>
                                    <td>Link File</td>
                                    <td>Tindakan</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0; ?>
                                @foreach ($pelayanan as $item)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>
                                            @if (!empty($item->file))
                                                <a href="{{ asset($item->getFile()) }}">link</a>
                                            @else
                                                <del><a
                                                        href="#"
                                                        class="text-muted disable"
                                                    >link</a></del>
                                            @endif
                                        </td>
                                        <td>
                                            <a
                                                href="{{ route('pelayanan.edit', ['id' => $item->id]) }}"
                                                class="btn mx-1 d-inline btn-sm btn-outline-success"
                                            >Ubah</a>
                                            <form
                                                onsubmit="return confirm('Hapus data permanen ?');"
                                                action="{{ route('pelayanan.destroy', $item->id) }}"
                                                method="post"
                                                class="d-inline"
                                            >
                                                @csrf
                                                @method('delete')
                                                <button
                                                    type="submit"
                                                    class="btn d-inline btn-sm btn-danger"
                                                >Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if ($kategori->id == 2)
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-hover w-100">
                                <thead class="bg-hijau">
                                    <tr>
                                        <td>No.</td>
                                        <td width="250">Nama Pendaftar Advis Teknis</td>
                                        <td>Tanggal Pelaksanaan</td>
                                        <td>Lingkup Konsultasi</td>
                                        <td>Status Pelaksanaan</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td width="250">Prayit</td>
                                        <td>30 Oktober 2021</td>
                                        <td>Struktur Bawah</td>
                                        <td><a
                                                href="{{ route('advisiteknis.index', ['status' => 'penugasan']) }}"
                                                class="btn btn-success"
                                            >Penugasan Personil</a></td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td width="250">Dalijo</td>
                                        <td>23 Oktober 2021</td>
                                        <td>Struktur Atas</td>
                                        <td><a
                                                href="{{ route('advisiteknis.index', ['status' => 'unggah_dokumen']) }}"
                                                class="btn btn-info"
                                            >Unggah Dokumen</button></td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td width="250">Markesot</td>
                                        <td>20 Oktober 2021</td>
                                        <td>Struktur Bawah</td>
                                        <td><a
                                                href="{{ route('advisiteknis.index', ['status' => 'approval']) }}"
                                                class="btn btn-secondary"
                                            >Persetujuan Dokumen</a></td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td width="250">Markenyut</td>
                                        <td>18 Oktober 2021</td>
                                        <td>Struktur Atas</td>
                                        <td><button
                                                type="button"
                                                class="btn btn-warning"
                                            >Pelaksanaan Advis Teknis</button></td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td width="250">Parjimin</td>
                                        <td>11 Oktober 2021</td>
                                        <td>Struktur Bawah</td>
                                        <td><button
                                                type="button"
                                                class="btn btn-light"
                                            >Selesai</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if ($kategori->id == 3)
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-hover w-100">
                                <thead class="bg-hijau">
                                    <tr>
                                        <td>No.</td>
                                        <td width="250">Nama Bimtek</td>
                                        <td>Tanggal Pelaksanaan</td>
                                        <td>Waktu Pelaksanaan</td>
                                        <td>Status Pelaksanaan</td>
                                        <td>Peserta</td>
                                        <td>Tindakan</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 0; ?>
                                    @foreach ($bimteks as $bimtek)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>
                                                {{ $bimtek->nama_bimtek }}
                                                <br />
                                                <button
                                                    class="btn d-inline btn-sm btn-info"
                                                    onclick="copy_link_zoom()"
                                                >Salin Link Zoom</button>
                                                <br /><br />
                                                <button
                                                    class="btn d-inline btn-sm btn-warning"
                                                    onclick="copy_link_cert()"
                                                >Salin Link Kuisioner Sertifikat</button>
                                            </td>
                                            <td>
                                                @if (is_null($bimtek->tanggal_pelaksanaan))
                                                    <span class="warning">TBA</span>
                                                @else
                                                    <?php echo date('j F Y', strtotime($bimtek->tanggal_pelaksanaan)); ?>
                                            </td>
                                    @endif
                                    <td><?php echo $bimtek->waktu_pelaksanaan . ' WIB'; ?></td>
                                    <td>{{ $bimtek->status_pelaksanaan }}</td>
                                    <td><a
                                            href="{{ route('pesertabimtek.index', $bimtek->id) }}"
                                            class="btn mx-1 d-inline btn-sm btn-outline-success"
                                        >Daftar Peserta</a></td>
                                    <td>
                                        <a
                                            href="{{ route('bimtek.edit', ['id' => $bimtek->id]) }}"
                                            class="btn mx-1 d-inline btn-sm btn-outline-success"
                                        >Ubah</a>
                                        <form
                                            onsubmit="return confirm('Hapus data permanen ?');"
                                            action="{{ route('bimtek.destroy', $bimtek->id) }}"
                                            method="post"
                                            class="d-inline"
                                        >
                                            @csrf
                                            @method('delete')
                                            <button
                                                type="submit"
                                                class="btn d-inline btn-sm btn-danger"
                                            >Hapus</button>
                                        </form>
                                        <input
                                            type="hidden"
                                            value="{{ $bimtek->link_zoom }}"
                                            id="link_zoom"
                                        />
                                        <input
                                            type="hidden"
                                            value="{{ $bimtek->link_certificate }}"
                                            id="link_cert"
                                        />
                                        <br />
                                    </td>
                                    </tr>
        @endforeach
        </tbody>
        </table>
    </div>
    </div>
    </div>
    </div>
    @endif
    </div>

    <form
        action=""
        id="formDelete"
        method="POST"
    >
        @csrf
        @method('DELETE')
    </form>

    <!-- Modal -->
    <div
        class="modal fade"
        id="import"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="exampleModalLabel"
                    >Import file data pelayanan</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form
                    action="{{ route('pelayanan.import', ['kategori' => $kategori->id]) }}"
                    method="POST"
                    enctype="multipart/form-data"
                >
                    @csrf
                    @method('post')
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="file">File data pelayanan</label>
                                    <input
                                        type="file"
                                        name="file"
                                        id="file"
                                        placeholder="file.xlsx"
                                        class="form-control-file"
                                        aria-describedby="detail"
                                    >
                                    <small
                                        id="detail"
                                        class="form-text text-danger"
                                    >Tipe file xlx, xlsx;</small>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="zip">File-file Pelayanan </label>
                                    <input
                                        type="file"
                                        name="zip"
                                        id="zip"
                                        placeholder="file.zip"
                                        class="form-control-file"
                                        aria-describedby="detail2"
                                    >
                                    <small
                                        id="detail2"
                                        class="form-text text-danger"
                                    >Tipe file .zip; dengan isi zipnya berupa file PDF Max 10mb dan Foto Max 2mb ( Maksimal
                                        1 foto setiap baris excel )</small>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button
                            type="submit"
                            class="btn btn-primary"
                        >Import</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function deleteData(id) {
            let r = confirm("'Anda Yakin Akan Menghapus ? \nIni mungkin akan menyebabkan beberapa fungsi lain tergangggu.");

            const formDelete = document.getElementById('formDelete')
            formDelete.action = '/admin/pelayanan/' + id;

            if (r == true) {
                formDelete.submit();
            } else {
                alert('Penghapusan dibatalkan.');
            }
        }

        function copy_link_zoom() {
            /* Get the text field */
            var copyText = document.getElementById("link_zoom");

            /* Select the text field */
            copyText.select();

            /* Copy the text inside the text field */
            navigator.clipboard.writeText(copyText.value);

            /* Alert the copied text */
            alert("Link zoom berhasil disalin: " + copyText.value);
        }

        function copy_link_cert() {
            /* Get the text field */
            var copyText = document.getElementById("link_cert");

            /* Select the text field */
            copyText.select();

            /* Copy the text inside the text field */
            navigator.clipboard.writeText(copyText.value);

            /* Alert the copied text */
            alert("Link akses sertifikat berhasil disalin: " + copyText.value);
        }
    </script>
    {{-- <embed src="{{asset($item->getFile())}}" frameborder="0" width="100%" height="300px"> --}}
@endsection
