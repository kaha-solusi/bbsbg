@extends('layouts.adminlte')
@section('main')
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="row h-100 mt-3">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('kepuasan-pelanggan.store') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Pelayanan <small class="text-success">*Harus diisi</small></label>
                                                <select name="pelayanan_id" class="form-control @error('pelayanan_id') is-invalid @enderror">
                                                    @forelse($pelayanans as $pelayanan)
                                                    <option value="{{ $pelayanan->id }}">{{ $pelayanan->nama }}</option>
                                                    @empty
                                                    <option value="" disabled>Data tidak ada</option>
                                                    @endforelse
                                                </select>
                                                @error('pelayanan_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Nama <small class="text-success">*Harus diisi</small></label>
                                                <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{ Auth::user()->name }}">
                                                @error('nama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Tingkat Kepuasan <small class="text-success">*Harus diisi</small> @error('tingkat_kepuasan')<small class="text-danger">{{ $message }}</small>@enderror </label><br>
                                                @for($i = 1;$i < 6;$i++)
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input @error('tingkat_kepuasan') is-invalid @enderror" type="radio" name="tingkat_kepuasan" id="{{ $i }}star" value="{{ $i }}"></input>
                                                    <label class="form-check-label" for="{{ $i }}star">{{ $i }}</label>
                                                </div>
                                                @endfor
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Komentar <small class="text-success">*Tidak Harus diisi</small></label>
                                                <textarea name="komen" class="form-control @error('komen') is-invalid @enderror" rows="5">{{ old('komen') }}</textarea>
                                                @error('komen')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success btn-sm" style="width: 100%;">Kirim</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection