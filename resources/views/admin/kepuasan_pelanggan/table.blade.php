@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
	'judul' => [
        ['judul' => 'Kepuasan Pengguna',
		'link' => route('kepuasan-pelanggan.index')]
    ]
])
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
<div class="container">
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="float-left">
								Kepuasan Pengguna
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row float-right">
		                        <div class="col-md-6">
		                            <a href="{{route('kepuasan-pelanggan.export')}}" class="btn btn-sm btn-warning">Export Kepuasan Pengguna</a>
		                        </div>
		                        <div class="col-md-6">
                            		<form action="{{ route('kepuasan-pelanggan.index') }}" method="GET" accept-charset="utf-8">
                            			@csrf
		                            	<div class="input-group">
		                            		<input class="form-control" type="text" placeholder="Search" aria-describedby="btn-search" name="search" value="{{ $search }}"></input>
		                            		<div class="input-group-append">
		                            			<button class="btn btn-outline-secondary" type="submit" id="btn-search">Search</button>
		                            		</div>
		                            	</div>
                            		</form>
	                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
							<table class="table">
								<thead class="thead-custom">
									<tr>
										<th scope="col">No</th>
										<th scope="col">Waktu</th>
										<th scope="col">Nama</th>
										<th scope="col">Pelayanan</th>
										<th scope="col">Tingkat Kepuasan</th>
										<th scope="col">Komentar</th>
									</tr>
								</thead>
								<tbody>
									@php $i = 1 @endphp
									@forelse($data as $d)
									<tr class="tbody-custom">
										<td>{{ $i++ }}</td>
										<td>{{ $d->waktu }}</td>
										<td>{{ $d->nama }}</td>
										<td>{{ $d->pelayanan->nama }}</td>
										<td>{{ $d->tingkat_kepuasan }}</td>
										<td>{{ ($d->komen == null) ? 'Tidak ada komentar' : $d->komen }}</td>
									</tr>
									@empty
									<tr class="tbody-custom">
										<td colspan="6" class="text-center">Tidak ada data.</td>
									</tr>
									@endforelse
								</tbody>
							</table>
						</div>
                        <div class="col-md-12">
                        	{{ $data->links() }}
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection