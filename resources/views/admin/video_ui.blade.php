@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [
        [
            'judul' => 'Tampilan Video Beranda', 
        ]
]])

<div class="container">
@if (session()->has('success'))
<div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="card">
        <div class="card-header">
            Tampilan Video Beranda
        </div>
        <form action="{{route('videoui.update', $videoUI->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <div class="card-body">

                <div class="row">
                    <div class="col-md-8 col-sm-7">
                        <div class="form-group">
                            <label for="link">Link Video Youtube </label><small class="text-success">*Harus diisi</small>
                            <input type="url" value="{{$videoUI->link}}" class="form-control" name="link" id="link" placeholder="https://youtube.com/watch?v=(Video_ID)">
                            
                            @error('link')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-5">
                        <div class="form-group">
                            <label for="logo">Logo </label><small class="text-success">*Tidak harus diisi</small>
                            <input type="file" class="form-control-file" name="logo" id="logo">
                            @error('logo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                
                
                <div class="form-group">
                    <label for="judul">Judul</label><small class="text-success">*Harus diisi</small>
                    <input type="text" value="{{$videoUI->judul}}" name="judul" id="judul" class="form-control @error('judul') is-invalid @enderror">
                    @error('judul')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="deskripsi">Deskirpsi</label><small class="text-success">*Tidak Harus diisi</small>
                    <textarea name="deskripsi" id="deskripsi" rows="5" class="form-control @error('deskripsi') is-invalid @enderror">{{$videoUI->deskripsi}}</textarea>
                    @error('deskripsi')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection