@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [[
        'judul' => 'Data Maklumat Pelayanan',
        'link' => route('admin.sejarah')
        ]]
])
<div class="container">
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="float-left">
                                Data Maklumat Pelayanan
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('maklumat.store') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>File Gambar <small class="text-success">*Tidak Harus diisi</small></label>
                                                <input type="file" name="gambar" class="form-control-file @error('gambar') is-invalid @enderror" value="{{ old('gambar') }}">
                                                @error('gambar')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>File PDF <small class="text-success">*Tidak Harus diisi</small></label>
                                                <input type="file" name="pdf" class="form-control-file @error('pdf') is-invalid @enderror" value="{{ old('pdf') }}">
                                                @error('pdf')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    @php $i = 1; @endphp
                                    @forelse($maklumat as $mis)
                                    @if($mis != " " || $mis != null)
                                    @php
                                        $row  = explode("|", $mis);
                                    @endphp
                                    <div class="col-md-12 col-sm-12 isi">
                                        @php
                                            $baris = (count($row) > 1) ?  Str::length($row[1]) / 80 :  Str::length($row[0]) / 80;
                                        @endphp
                                        <div class="form-row">
                                            <div class="form-group col-sm-12 col-md-12">
                                                <label>Isi-{{ $i++ }} <small class="text-success">*Harus diisi</small></label>
                                                <textarea rows="{{$baris}}" type="text" name="isi[]" class="form-control @error('isi') is-invalid @enderror">{{ (count($row) > 1) ? $row[1] : $row[0]}}</textarea>
                                                @error('isi')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @empty
                                    <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-12">
                                            <label>Isi-{{ $i++ }} <small class="text-success">*Harus diisi</small></label>
                                            <textarea rows="{{$baris}}" type="text" name="isi[]" class="form-control @error('isi') is-invalid @enderror">{{ (count($row) > 1) ? $row[1] : $row[0]}}</textarea>
                                            @error('isi')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    @endforelse
                                    <div class="col-md-12 mb-3" id="btnAdd">
                                        <div class="float-right">
                                            <button type="button" id="deleteMisi" class="btn btn-danger btn-sm">-Delete Isi</button>
                                            <button type="button" id="tambahMisi" class="btn btn-primary btn-sm">+Tambah Isi</button>
                                            <button type="submit" class="btn btn-success btn-sm">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
    $('#tambahMisi').click(function(event) {
        /* Act on the event */
        $('#btnAdd').before(`
            <div class="col-md-12 col-sm-12 isi">
                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-12">
                        <label>Isi-${($('.misi').length + 1)}<small class="text-success">*Harus diisi</small></label>
                        <textarea type="text" name="isi[]" class="form-control @error('isi') is-invalid @enderror">{{ (count($row) > 1) ? $row[1] : $row[0]}}</textarea>
                        @error('isi')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
        `);
    });

    function deleteMiss() {
        $('#deleteMisi').click(function(event) {
            /* Act on the event */
            $('.tahun').last().remove();
            $('.isi').last().remove();
        });
    }

    deleteMiss();

</script>
@endpush