@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    ['judul' => 'Data Peralatan Lab',
    'link' => route('admin.peralatan.index')],
    ['judul' => 'Tambah Data Peralatan Lab',
    'link' => route('admin.peralatan.create')]
    ]
    ])
    <div class="container">
        <div class="row h-100 mt-3">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Tambah Data Peralatan Lab
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    action="{{ route('admin.peralatan.store') }}"
                                    method="POST"
                                    accept-charset="utf-8"
                                    enctype="multipart/form-data"
                                    id="createForm"
                                    novalidate="novalidate"
                                >
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Lokasi Alat (Lab)</label>
                                                    <select
                                                        name="lab_id"
                                                        class="form-control @error('lab_id') is-invalid @enderror"
                                                    >
                                                        @forelse($data as $lab)
                                                            <option value="{{ $lab->id }}">
                                                                {{ $lab->nama }}
                                                            </option>
                                                        @empty
                                                            <option
                                                                value=""
                                                                disabled
                                                            >Tidak
                                                                ada data</option>
                                                        @endforelse
                                                    </select>
                                                    @error('lab_id')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Nama Peralatan
                                                        <div class="custom-tooltip">
                                                            <i class="fas fa-info-circle"></i>
                                                            <span class="custom-tooltiptext">Isi
                                                                dengan nama
                                                                peralatan.</span>
                                                        </div>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        name="nama"
                                                        class="form-control @error('nama') is-invalid @enderror"
                                                        value="{{ old('nama') }}"
                                                    >
                                                    @error('nama')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label>Foto Peralatan
                                                        <div class="custom-tooltip">
                                                            <i class="fas fa-info-circle"></i>
                                                            <span class="custom-tooltiptext">Isi
                                                                dengan file foto
                                                                dengan format jpg,
                                                                jpeg, atau png dan
                                                                maksimal berukuran 2
                                                                mb.</span>
                                                        </div>
                                                    </label>
                                                    <input
                                                        type="file"
                                                        name="foto_alat"
                                                        accept="image/*"
                                                        class="form-control-file @error('foto_alat') is-invalid @enderror"
                                                        value="{{ old('foto_alat') }}"
                                                    >
                                                    @error('foto_alat')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-3">
                                                    <label>Merek
                                                        <div class="custom-tooltip">
                                                            <i class="fas fa-info-circle"></i>
                                                            <span class="custom-tooltiptext">Isi
                                                                dengan merk
                                                                peralatan.</span>
                                                        </div>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        name="merek"
                                                        class="form-control @error('merek') is-invalid @enderror"
                                                        value="{{ old('merek') }}"
                                                    >
                                                    @error('merek')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group col-md-3">
                                                    <label>Tipe
                                                        <div class="custom-tooltip">
                                                            <i class="fas fa-info-circle"></i>
                                                            <span class="custom-tooltiptext">Isi
                                                                dengan tipe
                                                                peralatan.</span>
                                                        </div>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        name="type"
                                                        class="form-control @error('type') is-invalid @enderror"
                                                        value="{{ old('type') }}"
                                                    >
                                                    @error('type')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group col-md-3">
                                                    <label>Seri
                                                        <div class="custom-tooltip">
                                                            <i class="fas fa-info-circle"></i>
                                                            <span class="custom-tooltiptext">Isi
                                                                dengan no seri
                                                                peralatan, jika
                                                                ada.</span>
                                                        </div>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        min="0"
                                                        name="seri"
                                                        class="form-control @error('seri') is-invalid @enderror"
                                                        value="{{ old('seri') }}"
                                                    >
                                                    @error('seri')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>No./Kode BMN
                                                        <div class="custom-tooltip">
                                                            <i class="fas fa-info-circle"></i>
                                                            <span class="custom-tooltiptext">Isi
                                                                dengan No
                                                                BMN.</span>
                                                        </div>
                                                    </label>
                                                    <input
                                                        type="numeric"
                                                        name="no_bmn"
                                                        class="form-control @error('no_bmn') is-invalid @enderror"
                                                        value="{{ old('no_bmn') }}"
                                                    >
                                                    @error('no_bmn')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Kapasitas
                                                        <div class="custom-tooltip">
                                                            <i class="fas fa-info-circle"></i>
                                                            <span class="custom-tooltiptext">Isi
                                                                dengan range
                                                                (kapasitas)
                                                                peralatan, jika
                                                                ada.</span>
                                                        </div>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        name="keterangan_kapasitas"
                                                        class="form-control @error('keterangan_kapasitas') is-invalid @enderror"
                                                        value="{{ old('keterangan_kapasitas') }}"
                                                    >
                                                    @error('keterangan_kapasitas')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Akurasi
                                                        <div class="custom-tooltip">
                                                            <i class="fas fa-info-circle"></i>
                                                            <span class="custom-tooltiptext">Isi
                                                                dengan akyrasi
                                                                peralatan, jika
                                                                ada.</span>
                                                        </div>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        name="akurasi"
                                                        class="form-control @error('akurasi') is-invalid @enderror"
                                                        value="{{ old('akurasi') }}"
                                                    >
                                                    @error('akurasi')
                                                    <span
                                                        class="invalid-feedback"
                                                        role="alert"
                                                    >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Jumlah
                                                        <div class="custom-tooltip">
                                                            <i class="fas fa-info-circle"></i>
                                                            <span class="custom-tooltiptext">Isi
                                                                dengan jumlah
                                                                peralatan.</span>
                                                        </div>
                                                    </label>
                                                    <input
                                                        type="number"
                                                        min="0"
                                                        name="jumlah"
                                                        class="form-control @error('jumlah') is-invalid @enderror"
                                                        value="{{ old('jumlah') }}"
                                                    >
                                                    @error('jumlah')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        {{-- <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Tanggal Terakhir
                                                        Kalibrasi
                                                        <div class="custom-tooltip">
                                                            <i
                                                               class="fas fa-info-circle"></i>
                                                            <span
                                                                  class="custom-tooltiptext">Isi
                                                                dengan tanggal
                                                                kalibrasi terakhir,
                                                                jika alat
                                                                ukur.</span>
                                                        </div>
                                                    </label>
                                                    <input type="date"
                                                           name="tanggal_terakhir_kalibrasi"
                                                           class="form-control @error('tanggal_terakhir_kalibrasi') is-invalid @enderror"
                                                           value="{{ old('tanggal_terakhir_kalibrasi') }}">
                                                    @error('tanggal_terakhir_kalibrasi')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Tanggal Kalibrasi Ulang
                                                        <div class="custom-tooltip">
                                                            <i
                                                               class="fas fa-info-circle"></i>
                                                            <span
                                                                  class="custom-tooltiptext">Isi
                                                                dengan tanggal
                                                                kalibrasi
                                                                berikutnya, jika
                                                                alat ukur.</span>
                                                        </div>
                                                    </label>
                                                    <input type="date"
                                                           name="tanggal_kalibrasi_ulang"
                                                           class="form-control @error('tanggal_kalibrasi_ulang') is-invalid @enderror"
                                                           value="{{ old('tanggal_kalibrasi_ulangg') }}">
                                                    @error('tanggal_kalibrasi_ulang')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Institusi Kalibrasi
                                                        <div class="custom-tooltip">
                                                            <i
                                                               class="fas fa-info-circle"></i>
                                                            <span
                                                                  class="custom-tooltiptext">Isi
                                                                nama laboratorium
                                                                kalibrasi eksternal
                                                                jika dikalibrasi
                                                                eksternal atau
                                                                internal jika
                                                                dikalibrasi
                                                                internal.</span>
                                                        </div>
                                                    </label>
                                                    <input type="text"
                                                           name="lembaga_pengujian_kalibrasi"
                                                           class="form-control @error('lembaga_pengujian_kalibrasi') is-invalid @enderror"
                                                           value="{{ old('lembaga_pengujian_kalibrasi') }}">
                                                    @error('lembaga_pengujian_kalibrasi')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div> --}}

                                        {{-- <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Hasil Veriifikasi Alat
                                                        <small
                                                               class="text-danger">*Harus
                                                            diisi</small>
                                                        <div class="custom-tooltip">
                                                            <i
                                                               class="fas fa-info-circle"></i>
                                                            <span
                                                                  class="custom-tooltiptext">Isi
                                                                dengan “sesuai” jika
                                                                peralatan tersebut
                                                                sesuai dengan
                                                                persyaratan yang
                                                                ditentukan di IK
                                                                peralatan/pengujian
                                                                dan “tidak sesuai”
                                                                jika peralatan
                                                                tersebut tidak
                                                                sesuai
                                                                dengan persyaratan
                                                                yang ditentukan di
                                                                IK
                                                                peralatan/pengujian.</span>
                                                        </div>
                                                    </label>
                                                    <select name="hasil_verifikasi_alat"
                                                            class="form-control @error('hasil_verifikasi_alat') is-invalid @enderror">
                                                        <option value="1">Sesuai
                                                        </option>
                                                        <option value="0">Tidak
                                                            Sesuai</option>
                                                    </select>
                                                    @error('hasil_verifikasi_alat')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div> --}}

                                        {{-- <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Kondisi <small
                                                               class="text-danger">*Harus
                                                            diisi</small>
                                                        <div class="custom-tooltip">
                                                            <i
                                                               class="fas fa-info-circle"></i>
                                                            <span
                                                                  class="custom-tooltiptext">Isi
                                                                dengan kondisi alat:
                                                                <br>a. Baik, jika
                                                                kondisi peralatan
                                                                baik.
                                                                <br>b. Rusak ringan,
                                                                jika kondisi
                                                                peralatan rusak
                                                                tetapi masih bisa
                                                                diperbaiki sendiri.
                                                                <br>c. Rusak sedang,
                                                                jika kondisi
                                                                peralatan rusak dan
                                                                membutuhkan
                                                                perbaikan oleh pihak
                                                                eksternal
                                                                <br>d. Rusak berat,
                                                                jika kondisi
                                                                peralatan rusak dan
                                                                membutuhkan
                                                                perbaikan oleh pihak
                                                                eksternal serta
                                                                membutuhkan
                                                                penggantian suku
                                                                cadang.</span>
                                                        </div>
                                                    </label>
                                                    <select name="kondisi"
                                                            class="form-control @error('kondisi') is-invalid @enderror">
                                                        <option value="baik">Baik
                                                        </option>
                                                        <option
                                                                value="rusak_ringan">
                                                            Rusak Ringan</option>
                                                        <option
                                                                value="rusak_sedang">
                                                            Rusak Sedang</option>
                                                        <option value="rusak_berat">
                                                            Rusak Berat</option>
                                                    </select>
                                                    @error('kondisi')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div> --}}

                                        {{-- <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Jadwal Pemeliharaan
                                                        <div class="custom-tooltip">
                                                            <i
                                                               class="fas fa-info-circle"></i>
                                                            <span
                                                                  class="custom-tooltiptext">Isi
                                                                dengan jadwal
                                                                pemeliharaan, misal:
                                                                setiap senin, setiap
                                                                minggu ke-2, setiap
                                                                bulan mei,
                                                                dll.</span>
                                                        </div>
                                                    </label>
                                                    <input type="text"
                                                           name="jadwal_pemeliharaan"
                                                           class="form-control @error('jadwal_pemeliharaan') is-invalid @enderror"
                                                           value="{{ old('jadwal_pemeliharaan') }}">
                                                    @error('jadwal_pemeliharaan')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div> --}}

                                        {{-- <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Akses Pengguna
                                                        <div class="custom-tooltip">
                                                            <i
                                                               class="fas fa-info-circle"></i>
                                                            <span
                                                                  class="custom-tooltiptext">Isi
                                                                dengan nama jabatan
                                                                yang bertanggung
                                                                jawab terhadap
                                                                penggunaan alat
                                                                tersebut atau yang
                                                                boleh mengakses alat
                                                                tersebut, misal:
                                                                teknisi, engineer,
                                                                seluruh
                                                                personil
                                                                laboratorium
                                                                dll.</span>
                                                        </div>
                                                    </label>
                                                    <input type="text"
                                                           name="akses_pengguna"
                                                           class="form-control @error('akses_pengguna') is-invalid @enderror"
                                                           value="{{ old('akses_pengguna') }}">
                                                    @error('akses_pengguna')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div> --}}

                                        <div
                                            class="col-md-12 mt-3"
                                            id="btnAdd"
                                        >
                                            <button
                                                type="submit"
                                                class="btn btn-success float-right"
                                                style="width: 15%;"
                                            >Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    {!! JsValidator::formRequest('App\Http\Requests\PeralatanLabStoreRequest', '#createForm') !!}

    <script type="text/javascript">
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endpush
