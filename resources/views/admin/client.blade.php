@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    ['judul' => 'Data Pengguna',
    'link' => route('admin.client.index')]
    ]
    ])
    <div class="container">
        @if (session()->has('success'))
            <div
                class="alert alert-success"
                role="alert"
            >
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session()->has('failed'))
            <div
                class="alert alert-danger"
                role="alert"
            >
                {{ session()->get('failed') }}
            </div>
        @endif
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-left">
                                    Data Pengguna
                                </div>
                                <div class="float-right">
                                    <a
                                        href="{{ route('client.export') }}"
                                        class="btn btn-warning btn-sm"
                                    >Export Data Client</a>
                                    <a
                                        href="{{ route('admin.client.create') }}"
                                        class="btn btn-primary btn-sm"
                                    >Tambah Data Client</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead class="thead-custom">
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Logo</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Nomor Telepon</th>
                                            <th scope="col">Bidang Usaha</th>
                                            <th scope="col">Alamat</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @forelse($data as $client)
                                            <tr class="tbody-custom">
                                                <td>{{ $i++ }}</td>
                                                <td>
                                                    @if ($client->logo == null)
                                                        Tidak ada logo
                                                    @else
                                                        <img
                                                            src="{{ asset($client->logo) }}"
                                                            class="img-thumbnail"
                                                            alt=""
                                                            width="75px"
                                                        >
                                                    @endif
                                                </td>
                                                <td>{{ $client->name }}</td>
                                                <td>{{ $client->user->email }}</td>
                                                <td>{{ $client->phone_number }}</td>
                                                <td>{{ $client->bidang_usaha }}</td>
                                                <td>{{ substr($client->alamat, 0, 15) }}...<button
                                                        type="button"
                                                        onclick="sosialMedia('{{ $client->alamat }}')"
                                                        class="btn btn-link"
                                                        data-toggle="modal"
                                                        data-target="#sosialMediaModal"
                                                    >[ Detail ]</button></td>

                                                <td>
                                                    <button
                                                        type="button"
                                                        onclick="website('{{ $client->website }}')"
                                                        class="btn btn-light btn-sm"
                                                        data-toggle="modal"
                                                        data-target="#urlModal"
                                                    >[ Website ]</button>
                                                    <a
                                                        href="{{ route('admin.client.edit', $client->id) }}"
                                                        class="btn btn-warning btn-sm"
                                                    >Edit</a>
                                                    <form
                                                        onsubmit="return confirm('Hapus data permanen ?');"
                                                        action="{{ route('admin.client.destroy', $client->id) }}"
                                                        method="post"
                                                        class="d-inline"
                                                    >
                                                        @csrf
                                                        @method('delete')
                                                        <button
                                                            type="submit"
                                                            class="btn d-inline btn-sm btn-danger"
                                                        >Hapus</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tbody-custom">
                                                <td
                                                    colspan="9"
                                                    class="text-center"
                                                >Tidak ada data.</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                {{ $data->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal --}}
    <div
        class="modal fade"
        id="sosialMediaModal"
        tabindex="-1"
        aria-labelledby="mediaModalLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="mediaModalLabel"
                    >Alamat</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="keterangan"></p>
                </div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- End Modal --}}

    {{-- Modal --}}
    <div
        class="modal fade"
        id="urlModal"
        tabindex="-1"
        aria-labelledby="urlModalLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="urlModalLabel"
                    >Website</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="website"></p>
                </div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- End Modal --}}

    <form
        action=""
        id="formDelete"
        method="POST"
    >
        @csrf
        @method('DELETE')
    </form>
@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">
        function deleteData(id) {
            let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

            const formDelete = document.getElementById('formDelete')
            formDelete.action = '/admin/client/' + id;

            if (r == true) {
                formDelete.submit();
            } else {
                alert('Penghapusan dibatalkan.');
            }
        }

        function sosialMedia(keterangan) {
            $('#keterangan').text(keterangan);
        }

        function website(url) {
            $('#website').text(url);
        }
    </script>
@endpush
