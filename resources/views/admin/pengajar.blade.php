@extends('layouts.adminlte')
@section('main')
<div class="container">
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="float-left">
                            	Pengajar
                            </div>
                        </div>
                        <div class="col-md-8">
		                    <div class="row float-right">
		                        <div class="col-md-6">
                            		<form action="{{ route('pengajar.index') }}" method="GET" accept-charset="utf-8">
                            			@csrf
		                            	<div class="input-group">
		                            		<input class="form-control" type="text" placeholder="Search" aria-describedby="btn-search" name="search" value="{{ $search }}"></input>
		                            		<div class="input-group-append">
		                            			<button class="btn btn-outline-secondary" type="submit" id="btn-search">Search</button>
		                            		</div>
		                            	</div>
                            		</form>
	                            </div>
		                        <div class="col-md-6">
	                                <a href="{{ route('pengajar.create') }}" class="btn btn-primary" style="width: 100%;">Tambah Data Pengajar</a>
	                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
							<table class="table">
								<thead class="thead-custom">
									<tr>
										<th scope="col">No</th>
										<th scope="col">Foto</th>
										<th scope="col">Nama</th>
										<th scope="col">Job</th>
										<th scope="col">Keterangan</th>
										<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
									@php $i = 1 @endphp
									@forelse($data as $pengajar)
									<tr class="tbody-custom">
										<td>{{ $i++ }}</td>
										<td>
											@if($pengajar->foto == null)
												Tidak ada foto
											@else
												<img src="{{ asset($pengajar->foto) }}" class="img-thumbnail" alt="" width="75px">
											@endif
										</td>
										<td>{{ $pengajar->nama }}</td>
										<td>{{ $pengajar->job }}</td>
										<td>{{ $pengajar->keterangan }}</td>
										<td>
											<button type="button" onclick="sosialMedia('{{ $pengajar->link_ig }}', '{{ $pengajar->link_facebook }}', '{{ $pengajar->link_twitter }}', '{{ $pengajar->link_likedin }}')" class="btn btn-light btn-sm" data-toggle="modal" data-target="#sosialMediaModal">Sosial Media</button>
											<a href="{{ route('pengajar.edit', $pengajar->id) }}" class="btn btn-warning btn-sm">Edit</a>
											<button type="button" onclick="deleteData({{ $pengajar->id }})" class="btn btn-danger btn-sm">Hapus</button>
										</td>
									</tr>
									@empty
									<tr class="tbody-custom">
										<td colspan="7" class="text-center">Tidak ada data.</td>
									</tr>
									@endforelse
								</tbody>
							</table>
						</div>
                        <div class="col-md-12">
                        	{{ $data->links() }}
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{{-- Modal --}}
<div class="modal fade" id="sosialMediaModal" tabindex="-1" aria-labelledby="mediaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mediaModalLabel">Sosial Media</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="list-group">
        	<li class="list-group-item" id="ig"></li>
        	<li class="list-group-item" id="facebook"></li>
        	<li class="list-group-item" id="twitter"></li>
        	<li class="list-group-item" id="linkedin"></li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
{{-- End Modal --}}

<form action="" id="formDelete" method="POST">
    @csrf
    @method('DELETE')
</form>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
	function deleteData(id) {
		let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

		const formDelete = document.getElementById('formDelete')
        formDelete.action = '/admin/pengajar/'+id;

		if (r == true) {
			formDelete.submit();
		} else {
			alert('Penghapusan dibatalkan.');
		}
	}

	function sosialMedia(ig, fb, twitter, linkedin) {
		$('#ig').text("IG: "+ig);
		$('#facebook').text("FB: "+fb);
		$('#twitter').text("Twitter: "+twitter);
		$('#linkedin').text("Linkedin: "+linkedin);
	}
</script>
@endpush