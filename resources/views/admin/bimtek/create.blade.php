@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    ['judul' => 'Data Jadwal Bimtek',
    'link' => route('jadwal-bimtek.index')],
    ['judul' => 'Tambah Data Jadwal Bimtek',
    'link' => route('jadwal-bimtek.create')]
    ]
    ])
    <div class="container">
        @if (session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session()->has('failed'))
            <div class="alert alert-danger" role="alert">
                {{ session()->get('failed') }}
            </div>
        @endif
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Tambah Data Jadwal Bimtek
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('bimtek.store') }}"
                                  method="POST" accept-charset="utf-8"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-8">
                                                <label>Nama Bimbingan Teknis <small
                                                           class="text-success">*Harus
                                                        diisi</small></label>
                                                <input type="text"
                                                       name="nama_bimtek"
                                                       class="form-control @error('nama_bimtek') is-invalid @enderror"
                                                       value="{{ old('nama_bimtek') }}">
                                                @error('nama_bimtek')
                                                    <span class="invalid-feedback"
                                                          role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Jenis Bimbingan Teknis <small
                                                           class="text-success">*Harus
                                                        diisi</small></label>
                                                <select class="custom-select"
                                                        name="jenis_bimtek">
                                                    <option value="reguler"
                                                            selected>Reguler
                                                    </option>
                                                    <option value="umum">Umum
                                                    </option>
                                                </select>
                                                @error('nama_bimtek')
                                                    <span class="invalid-feedback"
                                                          role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Deskripsi Bimbingan
                                                        Teknis <small
                                                               class="text-success">*Harus
                                                            diisi</small></label>
                                                    <textarea name="deskripsi_bimtek"
                                                              class="form-control @error('deskripsi_bimtek') is-invalid @enderror"
                                                              rows="3">{{ old('deskripsi_bimtek') }}</textarea>
                                                    @error('deskripsi_bimtek')
                                                        <span class="invalid-feedback"
                                                              role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-sm">
                                                <label>Bulan Pelaksanaan <small
                                                           class="text-success">*Harus
                                                        diisi</small></label>
                                                <input type="month"
                                                       name="bulan_pelaksanaan"
                                                       id="bulan_pelaksanaan"
                                                       class="form-control @error('bulan_pelaksanaan') is-invalid @enderror"
                                                       value="{{ old('bulan_pelaksanaan') }}">
                                                @error('bulan_pelaksanaan')
                                                    <span class="invalid-feedback"
                                                          role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-sm">
                                                <label>Tanggal Pelaksanaan
                                                </label><a
                                                   href="javascript:void(0);"
                                                   class="btn btn-danger float-right"
                                                   style="padding:0 10px;"
                                                   onclick="javascript:tanggal_pelaksanaan.value=''">clear</a>
                                                <input type="date"
                                                       name="tanggal_pelaksanaan"
                                                       id="tanggal_pelaksanaan"
                                                       class="form-control @error('tanggal_pelaksanaan') is-invalid @enderror"
                                                       value="{{ old('tanggal_pelaksanaan') }}">
                                                @error('waktu_pelaksanaan')
                                                    <span class="invalid-feedback"
                                                          role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-sm">
                                                <label>Waktu Pelaksanaan
                                                    (WIB)</label> <a
                                                   href="javascript:void(0);"
                                                   class="btn btn-danger float-right"
                                                   style="padding:0 10px;"
                                                   onclick="javascript:waktu_pelaksanaan.value=''">clear</a>
                                                <input type="time"
                                                       name="waktu_pelaksanaan"
                                                       id="waktu_pelaksanaan"
                                                       class="form-control @error('waktu_pelaksanaan') is-invalid @enderror"
                                                       value="08:00">
                                                @error('waktu_pelaksanaan')
                                                    <span class="invalid-feedback"
                                                          role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Syarat Peserta<small
                                                        class="text-success">*Harus
                                                        diisi</small></label>
                                                <textarea name="syarat_peserta"
                                                          class="form-control @error('syarat_peserta') is-invalid @enderror"
                                                          rows="6">{{ old('syarat_peserta') }}</textarea>
                                                @error('syarat_peserta')
                                                    <span class="invalid-feedback"
                                                          role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Link Zoom</label>
                                                <input type="text" name="link_zoom"
                                                       class="form-control @error('link_zoom') is-invalid @enderror"
                                                       value="{{ old('link_zoom') }}">
                                                @error('link_zoom')
                                                    <span class="invalid-feedback"
                                                          role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <div class="form-row">
                                                    <div class="form-group col-sm">
                                                <label>Meeting ID</label>
                                                <input type="text" name="meeting_id"
                                                       class="form-control @error('meeting_id') is-invalid @enderror"
                                                       value="{{ old('meeting_id') }}">
                                                @error('meeting_id')
                                                <span class="invalid-feedback"
                                                      role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                    </div>
                                                    <div class="form-group col-sm">
                                                <label>Passcode</label>
                                                <input type="text" name="passcode"
                                                       class="form-control @error('passcode') is-invalid @enderror"
                                                       value="{{ old('passcode') }}">
                                                @error('passcode')
                                                <span class="invalid-feedback"
                                                      role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                    </div>
                                                </div>
                                                <label>Link Certificate</label>
                                                <input type="text" name="link_certificate"
                                                       class="form-control @error('link_certificate') is-invalid @enderror"
                                                       value="{{ old('link_certificate') }}">
                                                @error('link_certificate')
                                                    <span class="invalid-feedback"
                                                          role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        Konten Email
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="form-row">
                                                            <div
                                                                 class="form-group col-md-6">
                                                                <label>Subjek
                                                                    Email</label>
                                                                <input type="text"
                                                                       name="bimtek_subjek_email"
                                                                       class="form-control @error('bimtek_subjek_email') is-invalid @enderror"
                                                                       value="{{ old('bimtek_subjek_email') }}">
                                                                @error('bimtek_subjek_email')
                                                                    <span class="invalid-feedback"
                                                                          role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                            <div
                                                                 class="form-group col-md-6">
                                                                <label
                                                                       for="foto">Background
                                                                    Video
                                                                    Conference</label>
                                                                <input type="file"
                                                                       name="bimtek_background"
                                                                       id="bimtek_background"
                                                                       class="form-control-file @error('bimtek_background') is-invalid @enderror"
                                                                       aria-describedby="detail">
                                                                <small id="detail"
                                                                       class="form-text text-danger">Tipe
                                                                    file image: JPG,
                                                                    JPEG, PNG; Max
                                                                    berukuran
                                                                    2mb</small>
                                                                @error('foto')
                                                                    <span class="invalid-feedback"
                                                                          role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div
                                                                 class="form-group col-md-12">
                                                                <label>Konten
                                                                    Email</label>
                                                                <textarea name="bimtek_konten_email"
                                                                          class="form-control @error('bimtek_konten_email') is-invalid @enderror"
                                                                          rows="6">{{ old('bimtek_konten_email') }}</textarea>
                                                                @error('bimtek_konten_email')
                                                                    <span class="invalid-feedback"
                                                                          role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-3" id="btnAdd">
                                            <div class="float-right">
                                                <button type="submit"
                                                        class="btn btn-success btn-sm">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">

    </script>
@endpush
