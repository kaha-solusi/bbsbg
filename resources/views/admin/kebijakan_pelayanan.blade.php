@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [[
        'judul' => 'Data Kebijakan Pelayanan',
        'link' => route('admin.kebijakan-pelayanan')
    ]]
])
<div class="container">
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="justify-self-left">
                            Kebijakan Pelayanan
                        </div>
                        <div class="justify-self-right">
                            <button data-toggle="modal" data-target="#gambar" class="btn btn-sm btn-info">File Gambar</button>
                            <button data-toggle="modal" data-target="#pdf" class="btn btn-sm btn-secondary">File PDF</button>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div class="float-left">
                                Pilih untuk tampilan depan:
                                <a href="{{ route('admin.kebijakan-pelayanan.homepage', 1) }}" class="btn btn-sm @if($data->status_img == 1 && $data->status_pdf == 0) btn-dark @else btn-light @endif">Gambar</a>
                                <a href="{{ route('admin.kebijakan-pelayanan.homepage', 2) }}" class="btn btn-sm @if($data->status_img == 0 && $data->status_pdf == 1) btn-dark @else btn-light @endif">PDF</a>
                                <a href="{{ route('admin.kebijakan-pelayanan.homepage', 3) }}" class="btn btn-sm @if($data->status_img == 0 && $data->status_pdf == 0) btn-dark @else btn-light @endif">Teks</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form action="{{ route('admin.kebijakan-pelayanan.store') }}" method="POST" accept-charset="utf-8">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Teks <small class="text-success">*Harus diisi</small></label>
                                                <textarea class="form-control @error('misi') is-invalid @enderror" name="misi" rows="5">{{ $data->text }}</textarea>
                                                @error('misi')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-success btn-sm">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Modal Gambar --}}
<div class="modal fade" id="gambar" tabindex="-1" aria-labelledby="mediaModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="mediaModalLabel">File Gambar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <img src="{{ asset($data->url_img) }}" alt="" style="width: 100%;height: auto;">
                    </div>
                    <div class="col-md-12 mt-3">
                        <form action="{{ route('admin.kebijakan-pelayanan.img') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>File Gambar</label>
                                    <input type="file" name="img" class="form-control-file @error('img') is-invalid @enderror" value="{{ old('img') }}">
                                    @error('img')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <button type="submit" class="btn btn-primary float-right">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
{{-- End Modal --}}
{{-- Modal PDF --}}
<div class="modal fade" id="pdf" tabindex="-1" aria-labelledby="mediaModalLabelPdf" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="mediaModalLabelPdf">File Gambar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <embed src="{{ asset($data->url_pdf) }}" style="width: 100%;height: 350px;">
                        {{-- <img src="{{ asset($organisasi->gambar) }}" alt="" style="width: 100%;height: auto;"> --}}
                    </div>
                    <div class="col-md-12">
                        <form action="{{ route('admin.kebijakan-pelayanan.pdf') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>File PDF</label>
                                    <input type="file" name="pdf" class="form-control-file @error('pdf') is-invalid @enderror" value="{{ old('pdf') }}">
                                    @error('pdf')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <button type="submit" class="btn btn-primary float-right">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
{{-- End Modal --}}
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
</script>
@endpush
