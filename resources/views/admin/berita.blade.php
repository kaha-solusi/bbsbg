@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', ['judul' => [['judul' => 'Data Berita']]])
<div class="container">
    <div class="d-flex justify-content-center">
        <div class="card w-100">
            <div class="card-header text-white">
              <div class="d-flex content-align-between justify-content-between">
                  <a href="{{route('berita.create')}}" class="btn btn-primary">Tambah Data Berita</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table w-100 table-bordered table-striped">
                <thead class="bg-hijau">
                <tr>
                  <th>No.</th>
                  <th>Foto</th>
                  <th>Judul</th>
                  <th>Tanggal di buat</th>
                  <th>Terakhir di update</th>
                  <th>Tindakan</th>
                </tr>
                </thead>
                <tbody class="">
                <?php $i = 0;?>
                @foreach ($berita as $item)
                <tr>
                    <td>{{++$i}}</td>
                    <td>
                      
                        <img src="{{asset($item->getFoto()) }}" class="img-thumbnail" alt="" width="75px">
                    </td>
                    <td>{{$item->judul}}</td>
                    <td>{{$item->created_at}}</td>
                    <td>{{$item->updated_at}}</td>
                    <td>
                        <div class="row">
                            <a href="{{route('berita.show', $item->id)}}" class="btn d-inline btn-sm btn-outline-info">Lihat</a>
                            <a href="{{route('berita.edit', $item->id)}}" class="btn d-inline btn-sm btn-outline-success">Ubah</a>
			    <form onsubmit="return confirm('Hapus data permanen ?');" action="{{route('berita.destroy', ['beritum' => $item->id])}}" method="post" class="d-inline">
                              @csrf
                              @method('delete')
                              <button type="submit" class="btn d-inline btn-sm btn-outline-danger">Hapus</button>
                            </form> 
                        </div>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
    </div>
</div>
<form action="" id="formDelete" method="POST">
    @csrf
    @method('DELETE')
</form>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
	function deleteData(id) {
		let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

		const formDelete = document.getElementById('formDelete')
        formDelete.action = ;

		if (r == true) {
			formDelete.submit();
		} else {
			alert('Penghapusan dibatalkan.');
		}
	}

	function sosialMedia(keterangan) {
		$('#keterangan').text(keterangan);
	}

	function website(url) {
		$('#website').text(url);
	}
</script>
@endpush