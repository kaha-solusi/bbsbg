@extends('layouts.adminlte')
@section('main')
    <div class="container px-3 mt-5">


        <div class="row justify-content-md-center">
            <div class="col-lg-3 col-12">
                <!-- small card -->
                <div class="small-box bg-dark text-white">
                    <div class="inner">
                        <h3>{{ \App\Models\PeralatanLab::whereHas('lab', function ($query) {
                            $query->where('kategori', 'bahan');
                        })->sum('jumlah') }}
                        </h3>

                        <p>Total Peralatan<br>Lab Bahan</p>
                    </div>
                    <div class="icon text-hijau">
                        <i class="fas fa-tools"></i>
                    </div>
                    <a
                        href="{{ route('admin.peralatan.index') }}"
                        class="small-box-footer text-kuning"
                    >
                        Selengkapnya <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-12">
                <!-- small card -->
                <div class="small-box bg-dark text-white">
                    <div class="inner">
                        <h3>{{ \App\Models\PeralatanLab::whereHas('lab', function ($query) {
                            $query->where('kategori', 'struktur');
                        })->sum('jumlah') }}
                        </h3>

                        <p>Total Peralatan<br>Lab Struktur</p>
                    </div>
                    <div class="icon text-hijau">
                        <i class="fas fa-tools"></i>
                    </div>
                    <a
                        href="{{ route('admin.peralatan.index') }}"
                        class="small-box-footer text-kuning"
                    >
                        Selengkapnya <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-12">
                <!-- small card -->
                <div class="small-box bg-dark text-white">
                    <div class="inner">
                        <h3>{{ \App\Models\Pegawai::count() }}</h3>

                        <p>Total Pegawai <br><br>
                        </p>
                    </div>
                    <div class="icon text-hijau">
                        <i class="fas fa-user-tie"></i>
                    </div>
                    <a
                        href="{{ route('admin.pegawai.index') }}"
                        class="small-box-footer text-kuning"
                    >
                        Selengkapnya <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-12">
                <div class="small-box bg-dark text-white">
                    <div class="inner">
                        <h3>{{ \App\Models\Client::count() }}</h3>

                        <p>Total Klien <br><br>
                        </p>
                    </div>
                    <div class="icon text-hijau">
                        <i class="fas fa-user-tie"></i>
                    </div>
                    <a
                        href="{{ route('admin.client.index') }}"
                        class="small-box-footer text-kuning"
                    >
                        Selengkapnya <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <!-- BAR CHART -->
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <h3 class="card-title">Lab Bahan</h3>
                        <div class="card-tools">
                            <button
                                type="button"
                                class="btn btn-tool text-white"
                                data-card-widget="collapse"
                            ><i class="fas fa-minus"></i></button>
                            <button
                                type="button"
                                class="btn btn-tool text-white"
                                data-card-widget="remove"
                            ><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas
                                id="labBahan"
                                style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
                            ></canvas>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-6">
                <!-- BAR CHART -->
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <h3 class="card-title">Lab Struktur</h3>
                        <div class="card-tools">
                            <button
                                type="button"
                                class="btn btn-tool text-white"
                                data-card-widget="collapse"
                            ><i class="fas fa-minus"></i></button>
                            <button
                                type="button"
                                class="btn btn-tool text-white"
                                data-card-widget="remove"
                            ><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas
                                id="labStruktur"
                                style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"
                            ></canvas>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <h3 class="card-title">Data Log Aktivitas</h3>
                    </div>
                    <!-- /.card-header -->
                    <div
                        class="card-body table-responsive p-0"
                        style="height: 300px;"
                    >
                        <table class="table table-head-fixed text-nowrap">
                            <thead class="thead-custom">
                                <tr>
                                    <th>ID</th>
                                    <th>Akun</th>
                                    <th>Tanggal</th>
                                    <th>Log</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse(\App\Models\LogAktivitas::with('user')->latest()->limit(5)->get() as $log)
                                    <tr>
                                        <td>{{ $log->id }}</td>
                                        <td>{{ $log->user->name }}</td>
                                        <td>{{ $log->date }}</td>
                                        <td>{{ $log->log }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">Tidak ada data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <h3 class="card-title">Data Log Penggunaan Peralatan</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                            <select class="custom-select filter-year">
                                <option value=''>Pilih tahun</option>
                                @foreach ($years as $year)
                                    <option value="{{ $year }}">{{ $year }}</option>
                                @endforeach
                            </select>
                        </div>
                        {!! $dataTable->table() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <h3 class="card-title">Data Tracking</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                            <select class="custom-select filter-year">
                                <option value=''>Pilih tahun</option>
                                @foreach ($years as $year)
                                    <option value="{{ $year }}">{{ $year }}</option>
                                @endforeach
                            </select>
                        </div>

                        <table
                            class="table table-bordered table-custom dataTable no-footer"
                            id="trackingTable"
                        >
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Created By</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                </tr>
                            </thead>
                            {{-- {!! $dataTable->table() !!} --}}
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>

    </div>
@endsection

@push('script')
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
    <script>
        $(function() {
            //--------------
            //- AREA CHART -
            //--------------

            // Get context with jQuery - using jQuery's .get() method.

            // var areaChartCanvas = document.getElementById('barChart').getContext('2d');

            let monthBahan = [
                '{{ $jan }}',
                '{{ $feb }}',
                '{{ $mar }}',
                '{{ $apl }}',
                '{{ $mei }}',
                '{{ $june }}',
                '{{ $july }}',
                '{{ $aug }}',
                '{{ $sep }}',
                '{{ $oct }}',
                '{{ $nov }}',
                '{{ $des }}'
            ];

            let monthStruktur = [
                '{{ $jan2 }}',
                '{{ $feb2 }}',
                '{{ $mar2 }}',
                '{{ $apl2 }}',
                '{{ $mei2 }}',
                '{{ $june2 }}',
                '{{ $july2 }}',
                '{{ $aug2 }}',
                '{{ $sep2 }}',
                '{{ $oct2 }}',
                '{{ $nov2 }}',
                '{{ $des2 }}'
            ];

            var areaChartData = {
                labels: ['Januari', 'Febuari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September',
                    'Oktober', 'November', 'Desember'
                ],
                datasets: [{
                    label: 'Jumlah Kegiatan',
                    backgroundColor: 'rgba(60,141,188,0.9)',
                    borderColor: 'rgba(60,141,188,0.8)',
                    pointRadius: false,
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(60,141,188,1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data: monthBahan
                }, ]
            }

            var areaChartDataStruktur = {
                labels: ['Januari', 'Febuari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September',
                    'Oktober', 'November', 'Desember'
                ],
                datasets: [{
                    label: 'Jumlah Kegiatan',
                    backgroundColor: 'rgba(60,141,188,0.9)',
                    borderColor: 'rgba(60,141,188,0.8)',
                    pointRadius: false,
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(60,141,188,1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data: monthStruktur
                }, ]
            }

            var areaChartOptions = {
                maintainAspectRatio: false,
                responsive: true,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                        }
                    }]
                }
            }

            //Struktur
            var barChartCanvas = $('#labStruktur').get(0).getContext('2d')
            var barChartData = jQuery.extend(true, {}, areaChartDataStruktur)
            var temp0 = areaChartDataStruktur.datasets[0]
            barChartData.datasets[0] = temp0

            var barChartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                datasetFill: false
            }

            var barChart = new Chart(barChartCanvas, {
                type: 'bar',
                data: barChartData,
                options: barChartOptions
            })

            // Lab Bahan
            var barChartCanvas1 = $('#labBahan').get(0).getContext('2d')
            var barChartData1 = jQuery.extend(true, {}, areaChartData)
            var temp1 = areaChartData.datasets[0]
            barChartData1.datasets[0] = temp1

            var barChartOptions1 = {
                responsive: true,
                maintainAspectRatio: false,
                datasetFill: false
            }

            var barChart1 = new Chart(barChartCanvas1, {
                type: 'bar',
                data: barChartData1,
                options: barChartOptions1
            })


            function trackingDataTables() {
                if (!$.fn.dataTable.isDataTable('#trackingTable')) {
                    $('#trackingTable').DataTable({
                        dom: 'R<"row d-flex"<"col-md-2 mt-2"l> <
                        "col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end" <
                        "mr-2"
                        f >>> t <
                        "bottom d-flex flex-column flex-md-row align-items-center justify-content-between"
                        ip >
                        <
                        "bg-transparent"
                        r > ',
                        processing: true,
                        serverSide: true,
                        order: [
                            [0, 'desc']
                        ],
                        buttons: [
                            'csv', 'excel', 'pdf', 'print', 'reset', 'reload'
                        ],
                        ajax: '/services/two-datatables/posts',
                        columns: [{
                                data: 'id',
                                name: 'posts.id'
                            },
                            {
                                data: 'title',
                                name: 'posts.title'
                            },
                            {
                                data: 'created_by',
                                name: 'users.name',
                                width: '110px'
                            },
                            {
                                data: 'created_at',
                                name: 'posts.created_at',
                                width: '120px'
                            },
                            {
                                data: 'updated_at',
                                name: 'posts.updated_at',
                                width: '120px'
                            },
                        ],
                        order: [
                            [0, 'desc']
                        ]
                    });
                }
            }
            trackingDataTables()
        })
    </script>
@endpush
