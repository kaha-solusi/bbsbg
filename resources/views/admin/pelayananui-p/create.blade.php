@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [
        [
            'judul' => 'Menu Pelayanan', 
            'link' => route('pelayananui.index'),
        ],
        [
            'judul' => 'Simpan Menu Pelayanan',
        ]
]])
<div class="container">
@if (session()->has('success'))
<div class="alert alert-success" role="alert">
    {{ session()->get('success') }}
</div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="card">
        <div class="card-header">
            Buat Tampilan Menu Pelayanan di Beranda
        </div>
        <form action="{{route('pelayananui.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <label for="pelayanan_id">Menu Pelayanan</label>
                        <select name="pelayanan_id" id="pelayanan_id" class="custom-select @error('pelayanan_id') is-invalid @enderror">
                            @foreach ($pelayanan as $item)
                                <option value="{{$item->id}}">{{$item->nama}}</option>
                            @endforeach
                        </select>
                        @error('pelayanan_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    {{-- <div class="col-md-6 col-sm-12">
                        <label for="info">Tipe informasi yang di tampilkan</label>
                        <select name="info" id="info" class="custom-select">
                            <option value="file">File PDF</option>
                            <option value="text">Text</option>
                        </select>
                    </div> --}}
                    <div class="col-md-12 col-sm-12">
                        <label for="keterangan">Keterangan</label>
                        <input type="text" name="keterangan" id="keterangan" class="form-control @error('keterangan') is-invalid @enderror">
                        @error('keterangan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection