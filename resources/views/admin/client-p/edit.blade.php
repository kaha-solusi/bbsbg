@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
    'judul' => [
    ['judul' => 'Data Pengguna',
    'link' => route('admin.client.index')],
    ['judul' => 'Ubah Data Pengguna']
    ]
    ])
    <div class="container">
        @if (session()->has('success'))
            <div
                class="alert alert-success"
                role="alert"
            >
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session()->has('failed'))
            <div
                class="alert alert-danger"
                role="alert"
            >
                {{ session()->get('failed') }}
            </div>
        @endif
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Ubah Data Pengguna
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <form
                                    action="{{ route('admin.client.update', $data->id) }}"
                                    method="POST"
                                    accept-charset="utf-8"
                                    enctype="multipart/form-data"
                                >
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Logo <small class="text-success">*Tidak Harus
                                                            diisi</small></label>
                                                    <input
                                                        type="file"
                                                        name="logo"
                                                        class="form-control-file @error('logo') is-invalid @enderror"
                                                        aria-describedby="detail"
                                                    >
                                                    <small
                                                        id="detail"
                                                        class="form-text text-danger"
                                                    >Tipe file image: JPG, JPEG, PNG; Max berukuran 2mb</small>
                                                    @error('logo')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>E-mail <small class="text-success">*Harus diisi</small></label>
                                                    <input
                                                        type="text"
                                                        name="email"
                                                        class="form-control @error('email') is-invalid @enderror"
                                                        value="{{ $data->user->email }}"
                                                    >
                                                    @error('email')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Nama <small class="text-success">*Harus diisi</small></label>
                                                    <input
                                                        type="text"
                                                        name="name"
                                                        class="form-control @error('name') is-invalid @enderror"
                                                        value="{{ $data->name }}"
                                                    >
                                                    @error('name')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Username <small class="text-success">*Harus
                                                            diisi</small></label>
                                                    <input
                                                        type="text"
                                                        name="username"
                                                        class="form-control @error('username') is-invalid @enderror"
                                                        value="{{ $data->user->username }}"
                                                    >
                                                    @error('username')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Website <small class="text-success">*Tidak Harus
                                                            diisi</small></label>
                                                    <input
                                                        type="text"
                                                        name="website"
                                                        class="form-control @error('website') is-invalid @enderror"
                                                        value="{{ $data->website }}"
                                                    >
                                                    @error('website')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Nomor Telepon <small class="text-success">*Harus
                                                            diisi</small></label>
                                                    <input
                                                        type="text"
                                                        name="phone_number"
                                                        class="form-control @error('phone_number') is-invalid @enderror"
                                                        value="{{ $data->phone_number }}"
                                                    >
                                                    @error('phone_number')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Bidang Usaha <small class="text-success">*Harus
                                                            diisi</small></label>
                                                    <input
                                                        type="text"
                                                        name="bidang_usaha"
                                                        class="form-control @error('bidang_usaha') is-invalid @enderror"
                                                        value="{{ $data->bidang_usaha }}"
                                                    >
                                                    @error('bidang_usaha')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Nama Perusahaan <small class="text-success">*Harus
                                                            diisi</small></label>
                                                    <input
                                                        type="text"
                                                        name="nama_perusahaan"
                                                        class="form-control @error('nama_perusahaan') is-invalid @enderror"
                                                        value="{{ $data->nama_perusahaan }}"
                                                    >
                                                    @error('nama_perusahaan')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label>Alamat <small class="text-success">*Harus diisi</small></label>
                                                    <textarea
                                                        class="form-control @error('alamat') is-invalid @enderror"
                                                        name="alamat"
                                                        rows="5"
                                                    >{{ $data->alamat }}</textarea>
                                                    @error('alamat')
                                                        <span
                                                            class="invalid-feedback"
                                                            role="alert"
                                                        >
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-12 mb-3"
                                            id="btnAdd"
                                        >
                                            <div class="float-right">
                                                <button
                                                    type="submit"
                                                    class="btn btn-success btn-sm"
                                                >Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    {{-- Chart Section --}}
    <script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">

    </script>
@endpush
