@extends('layouts.adminlte')
@section('main')
    @include('admin.parts.breadcrumbs', [
        'judul' => [['judul' => 'Data Peralatan Lab', 'link' => route('admin.peralatan.index')]],
    ])
    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div
                        class="alert-danger"
                        role="alert"
                    >
                        <a class="ml-1">* Jika alat berwarna merah maka perlu dikalibrasi!</a>
                    </div>
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-left">
                                    Data Peralatan Lab
                                </div>
                                <div class="float-right">
                                    <button
                                        data-toggle="modal"
                                        data-target="#import"
                                        class="ml-1 btn btn-success btn-sm"
                                    >Import
                                        Data Peralatan Lab</button>
                                    <a
                                        href="{{ route('peralatan-lab.export') }}"
                                        class="btn btn-warning btn-sm"
                                    >Export Data
                                        Peralatan</a>
                                    <a
                                        href="{{ route('admin.peralatan.create') }}"
                                        class="btn btn-primary btn-sm"
                                    >Tambah Data
                                        Peralatan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Detail --}}
    <div
        class="modal fade"
        id="detail-modal"
        tabindex="-1"
        aria-labelledby="mediaModalLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="mediaModalLabel"
                    >Detail Peralatan
                    </h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <table>
                        <tr>
                            <td
                                class="font-weight-bold"
                                width="50"
                            >Nama</td>
                            <td>:</td>
                            <td>Contoh nam</td>
                        </tr>
                        <tr>
                            <td
                                class="font-weight-bold"
                                width="50"
                            >Merek</td>
                            <td>:</td>
                            <td>Contoh nama</td>
                        </tr>
                        <tr>
                            <td
                                class="font-weight-bold"
                                width="50"
                            >Tipe</td>
                            <td>:</td>
                            <td>Contoh nama</td>
                        </tr>
                        <tr>
                            <td
                                class="font-weight-bold"
                                width="50"
                            >Seri</td>
                            <td>:</td>
                            <td>Contoh nama</td>
                        </tr>
                        <tr>
                            <td
                                class="font-weight-bold"
                                width="50"
                            >Jumlah</td>
                            <td>:</td>
                            <td>Contoh nama</td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- End Modal --}}

    {{-- Modal Barcode --}}
    <div
        class="modal fade"
        id="barcode-modal"
        tabindex="-1"
        aria-labelledby="mediaModalLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="mediaModalLabel"
                    >Barcode
                    </h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body text-center">

                </div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >Tutup</button>
                    <button
                        type="button"
                        class="btn btn-primary btn-save"
                    >Simpan</button>
                </div>
            </div>
        </div>
    </div>
    {{-- End Modal Barcode --}}

    <!-- Modal -->
    <div
        class="modal fade"
        id="import"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="exampleModalLabel"
                    >Import file
                        data peralatan</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form
                    action="{{ route('peralatan.import') }}"
                    method="POST"
                    enctype="multipart/form-data"
                >
                    @csrf
                    @method('post')
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="file">File data peralatan</label>
                                    <input
                                        type="file"
                                        name="file"
                                        id="file"
                                        placeholder="file.xlsx"
                                        class="form-control-file"
                                        aria-describedby="detail"
                                    >
                                    <small
                                        id="detail"
                                        class="form-text text-danger"
                                    >Tipe file
                                        excel xlsx;</small>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="file">Contoh File Import</label><br>
                                    <a
                                        href="{{ route('pegawai.import.template') }}"
                                        class="btn btn-info"
                                    >Contoh Excel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button
                            type="submit"
                            class="btn btn-primary"
                        >Import</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <form
        action=""
        id="formDelete"
        method="POST"
    >
        @csrf
        @method('DELETE')
    </form>
@endsection

@push('script')
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://superal.github.io/canvas2image/canvas2image.js"></script>
    <script>
        $(function() {
            $('.btn-save').on('click', function() {
                html2canvas($('#bcsimpan'), {
                    onrendered: function(canvas) {
                        return Canvas2Image.saveAsPNG(canvas);
                    }
                })
            })
        })
    </script>
@endpush
