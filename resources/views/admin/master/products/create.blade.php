@extends('layouts.adminlte')
@section('main')
    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    Informasi Produk
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <form
                            id="createForm"
                            action="{{ route('admin.products.store') }}"
                            method="POST"
                            accept-charset="utf-8"
                            enctype="multipart/form-data"
                        >
                            @csrf
                            <div class="form-group">
                                <label>Nama Produk</label>
                                <input
                                    type="text"
                                    name="name"
                                    class="form-control"
                                    value="{{ old('name') }}"
                                >
                            </div>
                            <div class="form-group">
                                <label>Alat Kerja</label>
                                <select
                                    multiple
                                    name="alat_kerja[]"
                                    class="form-control select2-alat"
                                    data-title='Masukan Alat kerja'
                                >
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Bahan Kerja</label>
                                <select
                                    multiple
                                    name="bahan_kerja[]"
                                    class="form-control select2-bahan"
                                    data-title='Masukan Bahan kerja'
                                >
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Deskripsi Produk</label>
                                <textarea
                                    rows="4"
                                    class="form-control"
                                    name="description"
                                >{{ old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Parameter Pengujian</label>
                                <select
                                    name="test_parameters[]"
                                    multiple
                                    data-title="Pilih Parameter Pengujian"
                                    class="form-control selectpicker"
                                    data-live-search="true"
                                >
                                    @foreach ($testParameters as $param)
                                        <option value="{{ $param['id'] }}">
                                            {{ ucwords(str_replace('-', ' ', $param['name'])) }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="float-right">
                                    <button
                                        type="submit"
                                        class="btn btn-success btn-sm"
                                    >Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $('.select2-alat')
            // .prepend('<option selected></option>')
            .select2({
                allowClear: false,
                tags: true,
                theme: "bootstrap4",
                createTag: function(params) {
                    var term = $.trim(params.term);

                    if (term === '') {
                        return null;
                    }

                    return {
                        id: term,
                        text: term,
                        is_new: true
                    }
                },
                placeholder: 'Masukkan Alat Kerja',
                minimumInputLength: 3,
            });;

        $('.select2-bahan')
            // .prepend('<option selected></option>')
            .select2({
                allowClear: false,
                tags: true,
                theme: "bootstrap4",
                createTag: function(params) {
                    var term = $.trim(params.term);

                    if (term === '') {
                        return null;
                    }

                    return {
                        id: term,
                        text: term,
                        is_new: true
                    }
                },
                placeholder: 'Masukkan Bahan kerja',
                minimumInputLength: 3,
            });;

    </script>
    <script
        type="text/javascript"
        src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"
    ></script>
    {!! JsValidator::formRequest('App\Http\Requests\ProductRequest', '#createForm') !!}
@endpush
