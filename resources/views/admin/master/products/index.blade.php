@extends('layouts.adminlte')
@section('main')
    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="float-right">
                            <a
                                href="{{ route('admin.products.create') }}"
                                class="btn btn-sm btn-primary"
                            >Tambah Produk</a>
                        </div>
                    </div>
                    <div class="card-body ">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush
