@extends('layouts.adminlte')
@section('main')
    <div class="container">
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="justify-self-left">
                                Ubah Master Data Layanan Uji
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form
                            method='POST'
                            action="{{ route('master-pelayanan-uji.update', $master_pelayanan_uji->id) }}"
                        >
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="category">Nama Laboratorium</label>

                                <select
                                    class="form-control selectpicker"
                                    name="category"
                                    id="category"
                                    data-title="Pilih Jenis Layanan Uji"
                                >
                                    @foreach ($categories as $category)
                                        <option
                                            {{ $master_pelayanan_uji->pelayanan_id === $category->id ? 'selected' : '' }}
                                            value="{{ $category->id }}"
                                        >
                                            {{ $category->nama }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="name">Nama Layanan Uji</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="name"
                                    id="name"
                                    aria-describedby="name"
                                    placeholder="Masukkan nama layanan uji"
                                    value="{{ $master_pelayanan_uji->name }}"
                                    required
                                >
                            </div>

                            <div class="form-group">
                                <div class="table-responsive">
                                    <table class='table table-custom'>
                                        <thead>
                                            <tr>
                                                <th>
                                                    Parameter Pengujian</th>
                                                <th>
                                                    Harga
                                                </th>
                                                <th>
                                                    Hari Kerja
                                                </th>
                                                <th>
                                                    Standard Acuan
                                                </th>
                                                <th>
                                                    Action
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($master_pelayanan_uji->items as $no => $item)
                                                <tr>
                                                    <td>
                                                        <input
                                                            type="hidden"
                                                            name="items[{{ $no }}][index]"
                                                            value="{{ $item->id }}"
                                                        >
                                                        <input
                                                            class="form-control"
                                                            type="text"
                                                            name="items[{{ $no }}][name]"
                                                            value="{{ old("items[$no][name]") ?? $item->name }}"
                                                        >
                                                    </td>
                                                    <td>
                                                        <input
                                                            class="form-control"
                                                            type="text"
                                                            name="items[{{ $no }}][price]"
                                                            value="{{ old("items[$no][price]") ?? $item->price }}"
                                                        >
                                                    </td>
                                                    <td>
                                                        <input
                                                            class="form-control"
                                                            type="number"
                                                            name="items[{{ $no }}][working_days]"
                                                            value="{{ old("items[$no][working_days]") ?? $item->working_days }}"
                                                        >

                                                    </td>
                                                    <td>
                                                        <input
                                                            class="form-control"
                                                            type="text"
                                                            name="items[{{ $no }}][standard]"
                                                            value="{{ old("items[$no][standard]") ?? $item->standard }}"
                                                        >
                                                    </td>
                                                    <td>

                                                        <a
                                                            href="javascript:void(0)"
                                                            class='btn btn-sm btn-outline-danger item-remove'
                                                            title='remove'
                                                        ><i class='fa fa-trash'></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach()
                                        </tbody>
                                    </table>
                                    <button
                                        id="add"
                                        class="btn btn-sm btn-primary"
                                    >
                                        <i class="fa fa-plus"></i> Tambah
                                    </button>
                                </div>
                            </div>

                            <div class="form-group float-right">
                                <button
                                    type="submit"
                                    class='btn btn-primary'
                                >Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function() {
            let counter = Number("{{ count($master_pelayanan_uji->items) }}");
            const $addButton = $('#add')
            const $table = $('.table').DataTable({
                dom: 'Rrt',
                columnDefs: [{
                    orderable: false,
                    targets: [0, -1]
                }],
                "ordering": false,
                'colReorder': {
                    'allowReorder': false
                },
            });

            $('.table tbody').on('click', '.item-remove',
                function() {
                    $table
                        .row($(this).parents('tr'))
                        .remove()
                        .draw();
                });

            $addButton.on('click', function(e) {
                e.preventDefault();
                $table.row.add([
                    `<input class="form-control" type="text" name="items[${counter}][name]">`,
                    `<input class="form-control" type="text" name="items[${counter}][price]">`,
                    `<input class="form-control" type="number" name="items[${counter}][working_days]">`,
                    `<input class="form-control" type="text" name="items[${counter}][standard]">`,
                    `<a 
                        href="javascript:void(0)" 
                        class='btn btn-sm btn-outline-danger item-remove' 
                        title='remove'><i class='fa fa-trash'></i></a>`
                ]).draw(false);

                counter++;
            });
        })
    </script>
@endpush
