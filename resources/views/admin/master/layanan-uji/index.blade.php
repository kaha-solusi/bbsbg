@extends('layouts.adminlte')
@section('main')
    <div class="container">
        @include('admin.utils.alert')
        <div class="row h-100">
            <div class="col-md-12">
                <div class="card card-block d-flex">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-left">
                                    Master Data Pelayanan
                                </div>
                                <div class="float-right">
                                    <a
                                        href="{{ route('admin.products.index') }}"
                                        class="btn btn-sm btn-primary ml-1"
                                    >Tambah
                                        Data Produk</a>
                                    <a
                                        href="{{ route('master-pelayanan-uji.create') }}"
                                        class="btn btn-sm btn-info ml-1"
                                    >Tambah
                                        Data Pelayanan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush
