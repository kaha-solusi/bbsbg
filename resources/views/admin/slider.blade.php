@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', ['judul' => [['judul' => 'Gambar Banner']]])
<div class="container">
@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
    <div class="d-flex justify-content-center">
        <div class="card w-100">
            <div class="card-header">
              <div class="d-flex content-align-between justify-content-between">
                  <a href="{{route('slider.create')}}" class="btn btn-primary">Tambah Gambar Banner</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead class="bg-hijau">
                <tr>
                  <th>No.</th>
                  <th>Foto</th>
                  <th>Judul</th>
                  <th>Keterangan</th>
                  <th>Tindakan</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;?>
                @foreach ($slider as $item)
                <tr>
                    <td>{{$i++}}</td>
                    <td>
                        <img src="{{ asset($item->getFoto()) }}" class="img-thumbnail" alt="" width="75px">
                    </td>
                    <td>{{$item->judul}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        <div class="row">
                            {{-- <a href="{{route('slide.show', $item->id)}}" class="btn d-inline btn-sm btn-outline-info">Lihat</a> --}}
                            <a href="{{route('slider.edit', $item->id)}}" class="btn d-inline btn-sm btn-outline-success">Ubah</a>
                            <form onsubmit="return confirm('Hapus data permanen ?');" action="{{route('slider.destroy', $item->id)}}" method="post" class="d-inline">
                              @csrf
                              @method('delete')
                              <button type="submit" class="btn d-inline btn-sm btn-danger">Hapus</button>
                            </form>                         
			</div>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
    </div>
</div>
<form action="" id="formDelete" method="POST">
    @csrf
    @method('DELETE')
</form>
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
	function deleteData(id) {
		let r = confirm("Apa kau yakin?\nOK untuk menghapus data atau cancel untuk membatalkan.");

		const formDelete = document.getElementById('formDelete')
        formDelete.action = '/admin/slider/'+id;

		if (r == true) {
			formDelete.submit();
		} else {
			alert('Penghapusan dibatalkan.');
		}
	}

	function sosialMedia(keterangan) {
		$('#keterangan').text(keterangan);
	}

	function website(url) {
		$('#website').text(url);
	}
</script>
@endpush