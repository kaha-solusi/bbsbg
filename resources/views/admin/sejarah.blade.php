@extends('layouts.adminlte')
@section('main')
@include('admin.parts.breadcrumbs', [
    'judul' => [[
        'judul' => 'Data Sejarah Balai',
        'link' => route('admin.sejarah')
        ]]
])
<div class="container">
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get('success') }}
	</div>
@endif
@if (session()->has('failed'))
	<div class="alert alert-danger" role="alert">
		{{ session()->get('failed') }}
	</div>
@endif
    <div class="row h-100">
        <div class="col-md-12">
            <div class="card card-block d-flex">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="justify-self-left">
                            Sejarah
                        </div>
                        <div class="justify-self-right">
                            <button data-toggle="modal" data-target="#gambar" class="btn btn-sm btn-info">File Gambar</button>
                            <button data-toggle="modal" data-target="#pdf" class="btn btn-sm btn-secondary">File PDF</button>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div class="float-left">
                                Pilih untuk tampilan depan:
                                <a href="{{ route('admin.sejarah.homepage', 1) }}" class="btn btn-sm @if($data->status_img == 1 && $data->status_pdf == 0) btn-dark @else btn-light @endif">Gambar</a>
                                <a href="{{ route('admin.sejarah.homepage', 2) }}" class="btn btn-sm @if($data->status_img == 0 && $data->status_pdf == 1) btn-dark @else btn-light @endif">PDF</a>
                                <a href="{{ route('admin.sejarah.homepage', 3) }}" class="btn btn-sm @if($data->status_img == 0 && $data->status_pdf == 0) btn-dark @else btn-light @endif">Teks</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form action="{{ route('admin.sejarah.store') }}" method="POST" accept-charset="utf-8">
                                @csrf
                                <div class="row">
                                    @php $i = 1; @endphp
                                    @foreach($sejarah as $mis)
                                    @if($mis != " " || $mis != null)
                                    @php
                                        $row  = explode("|", $mis);
                                    @endphp
                                    <div class="col-md-2 col-sm-12 tahun">
                                        <div class="form-row">
                                            <div class="form-group col-sm-12 col-md-12">
                                                <label>Tahun <small class="text-primary">*Tidak Harus diisi</small></label>
                                                <input type="text" name="tahun[]" class="form-control @error('isi') is-invalid @enderror" value="{{ (count($row) > 1) ? $row[0] : null}}">
                                                @error('isi')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-12 isi">
                                        @php
                                            $baris = (count($row) > 1) ?  Str::length($row[1]) / 80 :  Str::length($row[0]) / 80;
                                        @endphp
                                        <div class="form-row">
                                            <div class="form-group col-sm-12 col-md-12">
                                                <label>Isi-{{ $i++ }} <small class="text-success">*Harus diisi</small></label>
                                                <textarea rows="{{$baris}}" type="text" name="isi[]" class="form-control @error('isi') is-invalid @enderror">{{ (count($row) > 1) ? $row[1] : $row[0]}}</textarea>
                                                @error('isi')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                    <div class="col-md-12 mb-3" id="btnAdd">
                                        <div class="float-right">
                                            <button type="button" id="deleteMisi" class="btn btn-danger btn-sm">-Delete Isi</button>
                                            <button type="button" id="tambahMisi" class="btn btn-primary btn-sm">+Tambah Isi</button>
                                            <button type="submit" class="btn btn-success btn-sm">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Modal Gambar --}}
<div class="modal fade" id="gambar" tabindex="-1" aria-labelledby="mediaModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="mediaModalLabel">File Gambar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <img src="{{ asset($data->url_img) }}" alt="" style="width: 100%;height: auto;">
                    </div>
                    <div class="col-md-12 mt-3">
                        <form action="{{ route('admin.sejarah.img') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>File Gambar</label>
                                    <input type="file" name="img" class="form-control-file @error('img') is-invalid @enderror" value="{{ old('img') }}">
                                    @error('img')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <button type="submit" class="btn btn-primary float-right">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
{{-- End Modal --}}
{{-- Modal PDF --}}
<div class="modal fade" id="pdf" tabindex="-1" aria-labelledby="mediaModalLabelPdf" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="mediaModalLabelPdf">File Gambar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <embed src="{{ asset($data->url_pdf) }}" style="width: 100%;height: 350px;">
                        {{-- <img src="{{ asset($organisasi->gambar) }}" alt="" style="width: 100%;height: auto;"> --}}
                    </div>
                    <div class="col-md-12">
                        <form action="{{ route('admin.sejarah.pdf') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>File PDF</label>
                                    <input type="file" name="pdf" class="form-control-file @error('pdf') is-invalid @enderror" value="{{ old('pdf') }}">
                                    @error('pdf')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <button type="submit" class="btn btn-primary float-right">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
{{-- End Modal --}}
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
    $('#tambahMisi').click(function(event) {
        /* Act on the event */
        $('#btnAdd').before(`
            <div class="col-md-2 col-sm-12 tahun">
                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-12">
                        <label>Tahun <small class="text-primary">*Tidak Harus diisi</small></label>
                        <input type="text" name="tahun[]" class="form-control @error('isi') is-invalid @enderror" value="">
                        @error('isi')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-sm-12 isi">
                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-12">
                        <label>Isi-${($('.tahun').length + 1)}<small class="text-success">*Harus diisi</small></label>
                        <textarea type="text" name="isi[]" class="form-control @error('isi') is-invalid @enderror"></textarea>
                        @error('isi')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
        `);
    });

    function deleteMiss() {
        $('#deleteMisi').click(function(event) {
            /* Act on the event */
            $('.tahun').last().remove();
            $('.isi').last().remove();
        });
    }

    deleteMiss();

</script>
@endpush