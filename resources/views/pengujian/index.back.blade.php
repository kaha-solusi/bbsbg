@extends('layouts.moderna')
@push('style')
    <link rel="stylesheet"
          href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}">
    <link rel="stylesheet"
          href="https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.css">

    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet"
          type="text/css" />
@endpush

@section('main')
    @include('layouts.breadcrumbs', ['pageTitle' => 'Registrasi Pengujian'])

    <section class="contact aos-init aos-animate p-4" data-aos="fade-up"
             data-aos-easing="ease-in-out" data-aos-duration="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 mb-5 shadow p-4 rounded">
                    <div id="smartwizard">
                        <ul class="nav">
                            <li>
                                <a class="nav-link" href="#step-1">
                                    Detail Pengajuan
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="#step-2">
                                    Data Pemesan
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="#step-3">
                                    Konfirmasi Pesanan
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div id="step-1" class="tab-pane" role="tabpanel">
                                <div class="form-group">
                                    <label for="user_type">Asal Instansi
                                        Pemohon</label>
                                    <select class="form-control selectpicker"
                                            name="user_type" id="user_type">
                                        <option>Kementerian PUPR</option>
                                        <option>Lainnya</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="laboratorium_name">Nama
                                        Laboratorium</label>

                                    <select class="form-control selectpicker"
                                            name="laboratorium_name"
                                            id="laboratorium_name"
                                            data-title="Pilih Nama Laboratorium">
                                        @foreach ($layananUji as $uji)
                                            <option value="{{ $uji->id }}">
                                                {{ $uji->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="test_type">Jenis Pengujian</label>
                                    <select class="form-control selectpicker"
                                            name="test_type" id="test_type"
                                            data-live-search="true"
                                            data-title="Pilih Jenis Pengujian">
                                        <option value="">== Pilih jenis pengujian ==
                                        </option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="test_date">Waktu Pengujian</label>
                                    <input type="text"
                                           class="form-control datepicker"
                                           name="test_date" id="test_date" readonly
                                           required="">
                                </div>
                                <div class="form-group">
                                    <table id='table' data-toggle="table"
                                           data-resizeable="true"
                                           data-unique-id="id">
                                        <thead class="thead-light">
                                            <tr>
                                                <th data-sortable="true"
                                                    data-field="test_parameter"
                                                    data-formatter="nameFormatter"
                                                    data-events='testParameterEvents'>
                                                    Parameter Pengujian</th>
                                                <th data-sortable="true"
                                                    data-field="nama_produk"
                                                    data-formatter="inputFormatter">
                                                    Nama Produk</th>
                                                <th data-sortable="true"
                                                    data-field="tipe_produk"
                                                    data-formatter="inputFormatter">
                                                    Tipe Produk</th>
                                                <th data-sortable="true"
                                                    data-field="sample_count"
                                                    data-formatter="inputFormatter">
                                                    Banyak Benda Uji
                                                </th>
                                                <th data-sortable="true"
                                                    data-field="price_per_unit"
                                                    data-formatter="currencyFormatter">
                                                    Harga Satuan (Rp)</th>
                                                <th data-field="total"
                                                    data-sortable="true">Jumlah</th>
                                            </tr>
                                        </thead>
                                    </table>

                                    <div
                                         class="d-flex justify-content-between align-items-center mt-3">
                                        <small>* Isikan banyak sampel yang ingin
                                            anda uji pada kolom <b>Banyak Benda
                                                Uji</b></small>
                                        <!-- <button class="btn btn-sm btn-primary" id='add-more-test'>Tambah</button> -->
                                    </div>
                                </div>
                            </div>

                            <div id="step-2" class="tab-pane" role="tabpanel">
                                <div class="form-group">
                                    <label for="company_name">Nama</label>
                                    <input type="text" class="form-control"
                                           name="company_name" id="company_name">
                                </div>
                                <div class="form-group">
                                    <label for="company_address">Alamat</label>
                                    <input type="text" class="form-control"
                                           name="company_address"
                                           id="company_address">
                                </div>
                                <div class="form-group">
                                    <label for="company_phone">No Telepon</label>
                                    <input type="text" class="form-control"
                                           name="company_phone" id="company_phone">
                                </div>
                                <div class="form-group">
                                    <label for="billing_email">Alamat Surel</label>
                                    <input type="text" class="form-control"
                                           name="billing_email" id="billing_email">
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input"
                                           id="term_and_condition">
                                    <label class="form-check-label text-sm"
                                           name='term_and_condition'
                                           for="is_">Apakah pengujian bukan untuk
                                        pribadi?</label>
                                </div>
                                <!-- JIKA Pengujian ini untuk pribadi maka tidak perlu menampilkan field baru, 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            jika bukan untuk pribadi maka akan menampilkan nama perusahaan, alamat perusahaan, no telepon
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            -->
                                <!-- <div class="form-group">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <label for="billing_name">Nama Kontak</label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <input type="text" class="form-control" name="billing_name" id="billing_name">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <div class="form-group">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <label for="billing_address">Alamat Kontak</label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <input type="text" class="form-control" name="billing_address" id="billing_address">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <div class="form-group">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <label for="billing_phone">No HP</label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <input type="text" class="form-control" name="billing_phone" id="billing_phone">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </div> -->
                            </div>

                            <div id="step-3" class="tab-pane" role="tabpanel">
                                <span class="font-weight-bold mb-3 d-block">Laporan
                                    pengujian/sertifikasi kalibrasi dibuat
                                    untuk</span>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input"
                                           id="term_and_condition">
                                    <label class="form-check-label text-sm"
                                           name='term_and_condition'
                                           for="term_and_condition">Setelah laporan
                                        uji dan atau sertifikat diterbitkan, kami
                                        tidak akan meminta diadakan perubahan pada
                                        isi dokumen dalam bentuk apapun</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end row -->
        </div>
    </section>

    @push('script')
        <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}">
        </script>
        <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"
                type="text/javascript"></script>
        <script
                src="https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.js">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/js/jquery.smartWizard.min.js"
                type="text/javascript"></script>

        <script>
            $('.datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });

            const $table = $('#table');

            function sampleCountFormatter(sample_count) {
                return '';
            }

            let rowData = null
            window.testParameterEvents = {
                'change select': function(e, value, row, index) {
                    // const testParamId = $(e.target).val();
                    // row.test_parameter = String(testParamId);
                    const data = rowData.find(f => f.id === Number(row));
                    // console.log(row, value, testParamId);
                    // row.sample_count = $(e.target).val()
                    // const price_per_unit = row.price_per_unit.replace(/\D/g,
                    //     '');
                    // const total = parseInt(row.sample_count) * parseInt(
                    //     price_per_unit);
                    // row.total = new Intl.NumberFormat('id-ID', {
                    //     style: 'currency',
                    //     currency: 'IDR'
                    // }).format(total)

                    $table.bootstrapTable('updateRow', {
                        index: index,
                        row: row
                    })
                },
            }

            function nameFormatter(name, original, index) {
                return `
                <select class='form-control'>
                    <option value="">Pilih parameter pengujian</option>
                    ${rowData.map(r => `<option ${Number(name) === Number(r.id) ? 'selected' : ''} value='${r.id}'>${r.name}</option>`)}
                </select>
                `;
            }

            function inputFormatter(sample_count) {
                return '<input class = "form-control" />';
            }

            function currencyFormatter(value) {
                const parsedValue = value.replace(/\D/g, '');
                return new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR'
                }).format(parsedValue);
            }

            window.sampleCountEvents = {
                'change select': function(e, value, row, index) {
                    row.sample_count = $(e.target).val()
                    const price_per_unit = row.price_per_unit.replace(/\D/g,
                        '');
                    const total = parseInt(row.sample_count) * parseInt(
                        price_per_unit);
                    row.total = new Intl.NumberFormat('id-ID', {
                        style: 'currency',
                        currency: 'IDR'
                    }).format(total)
                    $table.bootstrapTable('updateRow', {
                        index: index,
                        row: row
                    })
                },
            }

            $(document).ready(function() {
                $('#smartwizard').smartWizard({
                    lang: {
                        next: 'Selanjutnya',
                        previous: 'Sebelumnya'
                    },
                    autoAdjustHeight: true,
                });

                $("#smartwizard").on("leaveStep", function(e, anchorObject,
                    currentStepIndex, nextStepIndex, stepDirection) {

                    //for "Next"
                    if (currentStepIndex === 0 && stepDirection ===
                        'forward') {
                        // return doValidation()
                    }

                    if (currentStepIndex === 1 && stepDirection ===
                        'backwards') {
                        // return doValidation()
                    }

                });

                const $laboratorium = $('#laboratorium_name')
                const $jenisPengujian = $('#test_type');

                $laboratorium.on('change', function(e) {
                    $jenisPengujian
                        .empty();
                    $table
                        .bootstrapTable(
                            'removeAll')
                    $jenisPengujian
                        .selectpicker(
                            'refresh');
                    $.ajax({
                        url: '{{ route('pengujian.get-jenis-pengujian', '') }}',
                        method: 'GET',
                        data: {
                            labId: $(this).val()
                        },
                        success: function(response) {
                            $.each(response, function(_, {
                                name,
                                id
                            }) {
                                $jenisPengujian
                                    .append(
                                        new Option(
                                            name, id
                                        ))
                                $jenisPengujian
                                    .selectpicker(
                                        'refresh');
                            })
                        }
                    })
                })

                $jenisPengujian.on('change', function(e) {
                    $.ajax({
                        url: '{{ route('pengujian.get-jenis-pengujian-item', '') }}',
                        method: 'GET',
                        data: {
                            pengujianId: $(this).val()
                        },
                        success: function(response) {
                            rowData = response
                            $.each(response, function(_,
                                res) {
                                $table
                                    .bootstrapTable(
                                        'append', {
                                            test_parameter: '',
                                            nama_produk: '',
                                            tipe_produk: '',
                                            sample_count: '',
                                            price_per_unit: '',
                                            total: 0,
                                            standard: '',
                                        }
                                    )
                            })
                        }
                    })
                })
            });
        </script>

    @endpush

@endsection
