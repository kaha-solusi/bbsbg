@extends('layouts.moderna')
@push('style')
    <link
        rel="stylesheet"
        href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}"
    >
    <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css"
    >
    <link
        href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css"
        rel="stylesheet"
        type="text/css"
    />
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}"
    >
@endpush

@section('main')
    @include('layouts.breadcrumbs', ['pageTitle' => 'Registrasi Pengujian'])

    <section
        class="contact aos-init aos-animate p-4"
        data-aos="fade-up"
        data-aos-easing="ease-in-out"
        data-aos-duration="500"
    >
        <div class="container-fluid">
            @include('admin.utils.alert')
            <div class="row">
                <div class="col-lg-12 mb-5 shadow p-4 rounded">
                    <div class="bs-stepper linear">
                        <div
                            class="bs-stepper-header"
                            role="tablist"
                        >
                            <div
                                class="step active"
                                data-target="#pengujian-part"
                            >
                                <button
                                    type="button"
                                    class="step-trigger"
                                    role="tab"
                                    aria-controls="pengujian-part"
                                    id="pengujian-part-trigger"
                                    aria-selected="true"
                                >
                                    <span class="bs-stepper-circle">1</span>
                                    <span class="bs-stepper-label">Informasi
                                        Pengujian</span>
                                </button>
                            </div>
                            <div class="line"></div>
                            <div
                                class="step"
                                data-target="#information-part"
                            >
                                <button
                                    type="button"
                                    class="step-trigger"
                                    role="tab"
                                    aria-controls="information-part"
                                    id="information-part-trigger"
                                    aria-selected="false"
                                    disabled="disabled"
                                >
                                    <span class="bs-stepper-circle">2</span>
                                    <span class="bs-stepper-label">Data
                                        Pemesan</span>
                                </button>
                            </div>
                            <div class="line"></div>
                            <div
                                class="step"
                                data-target="#summary-part"
                            >
                                <button
                                    type="button"
                                    class="step-trigger"
                                    role="tab"
                                    aria-controls="summary-part"
                                    id="summary-part-trigger"
                                    aria-selected="false"
                                    disabled="disabled"
                                >
                                    <span class="bs-stepper-circle">3</span>
                                    <span class="bs-stepper-label">Konfirmasi
                                        Pemesanan</span>
                                </button>
                            </div>
                        </div>
                        <div class="bs-stepper-content">
                            <form
                                action="{{ route('client.pengujian.store') }}"
                                class="needs-validation"
                                novalidate
                                method="POST"
                            >
                                @csrf
                                @method('POST')
                                {{-- Step 1 --}}
                                <div
                                    id="pengujian-part"
                                    class="content active dstepper-block"
                                    role="tabpanel"
                                    aria-labelledby="pengujian-part-trigger"
                                >
                                    <div class="form-group">
                                        <label for="laboratorium_name">Nama
                                            Laboratorium</label>

                                        <select
                                            class="form-control select2"
                                            name="laboratorium_name"
                                            id="laboratorium_name"
                                            data-placeholder="Pilih Nama Laboratorium"
                                            required
                                        >
                                            @foreach ($layananUji as $uji)
                                                <option value="{{ $uji->nama }}">
                                                    {{ $uji->nama }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">Nama laboratorium harus diisi</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="test_type">Jenis
                                            Pengujian</label>
                                        <select
                                            class="form-control"
                                            name="test_type"
                                            id="test_type"
                                            data-placeholder="Pilih Jenis Pengujian"
                                            required
                                        >
                                        </select>
                                        <div class="invalid-feedback">Jenis pengujian harus diisi</div>
                                    </div>

                                    {{-- <div class="form-group">
                                        <label for="test_date">Waktu
                                            Pengujian</label>
                                        <input
                                            type="text"
                                            class="form-control datepicker"
                                            name="test_date"
                                            id="test_date"
                                            readonly
                                            required
                                        >
                                    </div> --}}

                                    <div class="alert alert-info">
                                        Jika tipe produk berbeda wajib menambahkan parameter pengujian baru
                                    </div>
                                    <style>
                                        table {
                                            margin: 0 auto;
                                            width: 100%;
                                            clear: both;
                                            border-collapse: collapse;
                                            table-layout: fixed;
                                            word-wrap: break-word;
                                        }

                                    </style>
                                    <div class="table-responsive">
                                        <table
                                            id="itemPengujianTable"
                                            class="table table-striped table-bordered"
                                            style="width:100%"
                                        >
                                            <thead>
                                                <tr>
                                                    <th>Parameter Pengujian</th>
                                                    <th>Nama Produk</th>
                                                    <th>Tipe Produk</th>
                                                    <th>Banyak Benda Uji</th>
                                                    <th>Standar Acuan</th>
                                                    <th>Harga Satuan (Rp)</th>
                                                    <th>Jumlah</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="d-flex mt-2">
                                        <button
                                            class="btn btn-sm btn-primary"
                                            id='add-more-test'
                                        >Tambah</button>
                                    </div>

                                    <button class="btn btn-primary next-part mt-2">
                                        Selanjutnya
                                    </button>
                                </div>

                                {{-- Step 2 --}}
                                <div
                                    id="information-part"
                                    class="content"
                                    role="tabpanel"
                                    aria-labelledby="information-part-trigger"
                                >
                                    <div class="form-group">
                                        <label for="billing_name">Nama</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="billing_name"
                                            id="billing_name"
                                            value="{{ $user->name }}"
                                            required
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label for="billing_address">Alamat</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="billing_address"
                                            id="billing_address"
                                            required
                                            value="{{ $user->client->alamat ?? '' }}"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label for="billing_phone">No
                                            Telepon</label>
                                        <input
                                            type="tel"
                                            class="form-control"
                                            name="billing_phone"
                                            id="billing_phone"
                                            required
                                            value="{{ $user->client->phone_number ?? '' }}"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label for="billing_email">Alamat
                                            Surel</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="billing_email"
                                            id="billing_email"
                                            value="{{ $user->email }}"
                                            required
                                        >
                                    </div>

                                    <div class="form-check">
                                        <input
                                            type="checkbox"
                                            class="form-check-input"
                                            id="is_personal"
                                        >
                                        <label
                                            class="form-check-label text-sm"
                                            name='is_personal'
                                            for="is_personal"
                                        >Apakah pengujian
                                            bukan untuk pribadi?</label>
                                    </div>

                                    <div class="mt-2 d-flex">
                                        <button class="btn btn-primary previous-part">Kembali</button>
                                        <button class="btn btn-primary next-part ml-2">Selanjutnya</button>
                                    </div>
                                </div>

                                {{-- Step 3 --}}
                                <div
                                    id="summary-part"
                                    class="content"
                                    role="tabpanel"
                                    aria-labelledby="summary-part-trigger"
                                >
                                    <div class="summary-content"></div>

                                    <div class="form-check mb-3">
                                        <input
                                            type="checkbox"
                                            class="form-check-input"
                                            id="term_and_condition"
                                            required
                                        >
                                        <label
                                            class="form-check-label text-sm"
                                            name='term_and_condition'
                                            for="term_and_condition"
                                        >Setelah
                                            laporan
                                            uji dan atau sertifikat diterbitkan,
                                            kami
                                            tidak akan meminta diadakan perubahan
                                            pada
                                            isi dokumen dalam bentuk apapun</label>
                                    </div>

                                    <button class="btn btn-primary previous-part">Kembali</button>
                                    <button
                                        type="submit"
                                        class="btn btn-primary ml-2"
                                    >Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end row -->
        </div>
    </section>

    @push('script')
        <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}">
        </script>
        <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
        </script>
        <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
        </script>
        <script src="{{ asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
        </script>
        {{-- <script
                src="https://cdn.jsdelivr.net/gh/jeffreydwalter/ColReorderWithResize@9ce30c640e394282c9e0df5787d54e5887bc8ecc/ColReorderWithResize.js">
        </script> --}}
        <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}">
        </script>
        <script
                src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"
                type="text/javascript"
        ></script>
        <script src="https://cdn.jsdelivr.net/npm/bs-stepper/dist/js/bs-stepper.min.js">
        </script>

        <script src="{{ asset('adminlte/plugins/select2/js/select2.min.js') }}"></script>
        <script>
            $('select')
                .prepend('<option selected></option>')
                .select2({
                    allowClear: true,
                    theme: "bootstrap4"
                });
            var d = new Date();

            var month = d.getMonth() + 1;
            var day = d.getDate();

            var today = (day < 10 ? '0' : '') + day + '-' +
                (month < 10 ? '0' : '') + month +
                '-' + d.getFullYear();

            $('.datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                minDate: today,
                value: today,
                format: 'dd-mm-yyyy',
            });

            const $table = $('#table');
            const $addButton = $('#add-more-test')
            $addButton.hide();

            let rowData = null
            let productName = []

            function unserialize(serializedData) {
                var urlParams = new URLSearchParams(
                    serializedData); // get interface / iterator
                unserializedData = {}; // prepare result object
                for ([key, value] of urlParams) { // get pair > extract it to key/value
                    unserializedData[key] = value;
                }

                return unserializedData;
            }

            $(document).ready(function() {
                const $form = $('form')
                // BS-Stepper Init
                const stepperEl = document.querySelector('.bs-stepper')
                var stepper = new Stepper(stepperEl)

                stepperEl.addEventListener('show.bs-stepper', function(event) {
                    $form.removeClass('was-validated');

                    if (event.detail.indexStep === 2) {
                        const data = unserialize($form.serialize());

                        const companyBilling = data
                            .company_billing_name && `
                            <tr>
                                <td class='font-weight-bold'>Nama
                                    Instansi</td>
                                <td>:</td>
                                <td>${data.company_billing_name}</td>
                            </tr>
                            <tr>
                                <td class='font-weight-bold'>Alamat
                                    Instansi</td>
                                <td>:</td>
                                <td>${data.company_billing_address}</td>
                            </tr>
                            <tr>
                                <td class='font-weight-bold'>No
                                    Telepon Instansi
                                </td>
                                <td>:</td>
                                <td>${data.company_billing_phone}</td>
                            </tr>
                            <tr>
                                <td class='font-weight-bold'>Alamat
                                    Surel Instansi
                                </td>
                                <td>:</td>
                                <td>${data.company_billing_email}</td>
                            </tr>
                        ` || ''

                        const totalTestParameter = $(
                            'tr [name*="items"]').length / 6

                        let rows = '';
                        for (let index = 0; index <
                            totalTestParameter; index++) {
                            const row = $(
                                `tr [name*="items[${index}]"]`)

                            let tr = '<tr>'
                            for (let j = 0; j < row
                                .length - 1; j++) {

                                if (j === 0) {
                                    const rowD = rowData.find(f => f.id === Number(row[j].value))
                                    tr += `<td>${rowD.name}</td>`
                                } else {
                                    tr += `<td>${row[j].value}</td>`
                                }
                            }
                            tr += '</tr>'

                            rows += tr
                        }

                        const summaryContent = `
                            <table>
                                <tbody>
                                    <tr>
                                        <td class='font-weight-bold'
                                            width="200">Nama
                                            Pemesan</td>
                                        <td width="10">:</td>
                                        <td>${data.billing_name}</td>
                                    </tr>
                                    <tr>
                                        <td class='font-weight-bold'>Alamat
                                            Pemesan</td>
                                        <td>:</td>
                                        <td>${data.billing_address}</td>
                                    </tr>
                                    <tr>
                                        <td class='font-weight-bold'>Nomor
                                            Telepon</td>
                                        <td>:</td>
                                        <td>${data.billing_phone}</td>
                                    </tr>
                                    <tr>
                                        <td class='font-weight-bold'>Alamat
                                            Surel</td>
                                        <td>:</td>
                                        <td>${data.billing_email}</td>
                                    </tr>

                                    <tr class="blank_row">
                                        <td colspan="3"></td>
                                    </tr>

                                    ${companyBilling}
                                    
                                    <tr class="blank_row">
                                        <td colspan="3"></td>
                                    </tr>

                                    <tr>
                                        <td class='font-weight-bold'>Nama
                                            Laboratorium
                                        </td>
                                        <td>:</td>
                                        <td>${data.laboratorium_name}</td>
                                    </tr>
                                    <tr>
                                        <td class='font-weight-bold'>Jenis
                                            Pengujian
                                        </td>
                                        <td>:</td>
                                        <td>${data.test_type}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="mt-3 table table-bordered">
                                <thead>
                                    <tr>
                                        <td>Parameter Pengujian</td>
                                        <td>Nama Produk</td>
                                        <td>Tipe Produk</td>
                                        <td>Banyak Benda Uji</td>
                                        <td>Standar Acuan</td>
                                        <td>Harga Satuan (Rp)</td>
                                        <td>Jumlah</td>
                                    </tr>
                                </thead>

                                <tbody>
                                    ${rows}
                                </tbody>
                            </table>
                        `

                        $('.summary-content').html(summaryContent)
                    }

                    // event.preventDefault();
                    // $form.toggleClass('was-validated')
                    // console.log($form.classList)
                })

                let productNames = [];
                const $table = $('#itemPengujianTable').DataTable({
                    dom: 'rt',
                    "autoWidth": false,
                    "paging": false,
                    "ordering": false,
                    "scrollX": true,
                    columnDefs: [{
                            orderable: false,
                            targets: [0, -1]
                        },
                        {
                            "width": 200,
                            "targets": [0, 1, 2]
                        },
                        {
                            "width": 150,
                            "targets": [3, 4, 5, 6]
                        },
                        {
                            "width": 50,
                            "targets": [-1]
                        }
                    ],
                    "ordering": false,
                    'colReorder': {
                        'allowReorder': false
                    },
                    rowCallback: function(row, data, displayNum,
                        displayIndex, dataIndex) {
                        const $testParameter = $(row).find('.test_parameter');
                        const $productName = $(row).find('.product_name');
                        const $productDesc = $(row).find('.product_desc');

                        $testParameter
                            .selectpicker(
                                'refresh');
                        $productName
                            .selectpicker(
                                'refresh')
                        $(row).on("input", ".product_count",
                            function() {
                                var input = $(this);
                                const pricePerUnit = $(row)
                                    .find(
                                        '.price_per_unit')
                                    .val()
                                    .slice(0, -3)
                                    .replace(/\D/g, '')

                                const total = Number(input
                                    .val()) * Number(
                                    pricePerUnit)

                                const formattedTotal = new Intl
                                    .NumberFormat(
                                        'id-ID', {
                                            style: 'currency',
                                            currency: 'IDR'
                                        }).format(total)

                                $(row).find(
                                        '.total')
                                    .val(formattedTotal)
                            });

                        $(row).on('change', '.test_parameter',
                            function(e) {
                                const selectedID = $(this)
                                    .val();
                                const selected = rowData.find(
                                    f => f.id === Number(
                                        selectedID))

                                if (selected) {
                                    const pricePerUnit =
                                        new Intl.NumberFormat(
                                            'id-ID', {
                                                style: 'currency',
                                                currency: 'IDR'
                                            }).format(
                                            selected.price)

                                    $(row).find(
                                            '.price_per_unit')
                                        .val(pricePerUnit)
                                    // $(row).find(
                                    //         '.product_id')
                                    //     .val(selected.id)
                                    $(row).find(
                                            '.standar_acuan')
                                        .val(selected.standard)
                                }

                                productNames = [];
                                $productDesc.text('');
                                $productName
                                    .empty();
                                $.ajax({
                                    url: '{{ route('pengujian.get-product-name', '') }}',
                                    method: 'GET',
                                    data: {
                                        parameterPengujian: $(this).val()
                                    },
                                    success: function(response) {
                                        $.each(response, function(_, {
                                            name,
                                            id,
                                            description
                                        }) {
                                            productNames.push({
                                                id,
                                                name,
                                                description
                                            })
                                            $productName
                                                .append(
                                                    new Option(
                                                        name,
                                                        name
                                                    ))
                                            $productName
                                                .selectpicker(
                                                    'refresh');
                                        })
                                    }
                                })
                            })

                        $(row).on('change', '.product_name', function(e) {
                            const product_name = $(this).val()
                            const product = productNames.find(f => f.name === product_name)
                            if (product) {
                                $productDesc.text(product.description);
                                $(row).find('.product_id').val(product.id)
                            }
                        });
                    }
                });

                $('#itemPengujianTable tbody').on('click', '.item-remove',
                    function() {
                        $table
                            .row($(this).parents('tr'))
                            .remove()
                            .draw();
                    });

                let counter = 0;
                $addButton.on('click', function(e) {
                    e.preventDefault();
                    $form.removeClass('was-validated');
                    console.log(rowData)
                    $table.row.add([
                        `<select 
                            class='form-control selectpicker test_parameter' 
                            data-title='Pilih parameter pengujian' 
                            name="items[${counter}][test_parameter]"
                            required>
                            ${rowData.map(r => `<option value='${r.id}'>${r.name}</option>`)}
                        </select>`,
                        `<select 
                            class='form-control selectpicker product_name' 
                            data-title='Pilih nama produk' 
                            name="items[${counter}][product_name]
                            required">
                            ${productName.map(r => `<option value='${r.id}'>${r.name}</option>`)}
                        </select>
                        <small class='text-secondary product_desc'></small>
                        `,
                        `<textarea class="form-control product_type" name="items[${counter}][product_type]"></textarea>`,
                        `<input class="form-control product_count" type="number" name="items[${counter}][product_count]" required>
                        <div class="invalid-feedback">Banyak benda uji harus diisi</div>`,
                        `<input class="form-control standar_acuan" disabled type="text" name="items[${counter}][standar_acuan]">`,
                        `<input class="form-control price_per_unit" disabled type="text" name="items[${counter}][price_per_unit]">`,
                        `<input class="form-control total" disabled type="text" name="items[${counter}][total]">`,
                        `<div class='button-group'>
                            <input type="hidden" class="product_id" name="items[${counter}][product_id]">
                            <a 
                                href="javascript:void(0)" 
                                class='btn btn-sm btn-danger item-remove' 
                                title='remove'>Hapus</a>
                        </div>`
                    ]).draw(false);

                    counter++;
                });

                const $laboratorium = $('#laboratorium_name')
                const $jenisPengujian = $('#test_type');

                $laboratorium.on('change', function(e) {
                    $jenisPengujian.empty();
                    $table.clear().draw();
                    $.ajax({
                        url: '{{ route('pengujian.get-jenis-pengujian', '') }}',
                        method: 'GET',
                        data: {
                            labId: $(this).val()
                        },
                        success: function(response) {
                            $.each(response, function(_, {
                                name,
                                id
                            }) {
                                $jenisPengujian
                                    .append(
                                        new Option(
                                            name,
                                            name,
                                            false,
                                            false
                                        ))
                                    .trigger('change')
                            })
                        }
                    })
                })
                $jenisPengujian.on('change', function(e) {
                    $table.clear().draw();
                    $.ajax({
                        url: '{{ route('pengujian.get-jenis-pengujian-item', '') }}',
                        method: 'GET',
                        data: {
                            pengujianId: $(this).val()
                        },
                        success: function(response) {
                            rowData = response

                            $addButton.show();
                        }
                    })
                })

                $('.next-part').on('click', function(e) {
                    e.preventDefault();
                    stepper.next()
                })

                $('.previous-part').on('click', function(e) {
                    e.preventDefault();
                    stepper.previous()
                })

                $('#is_personal').on('change', function(e) {
                    const billingName = '{{ $user->client->nama_perusahaan ?? '' }}'
                    const billingAdress = '{{ $user->client->alamat ?? '' }}'
                    const billingPhone = '{{ $user->client->phone_number ?? '' }}'
                    const billingEmail = '{{ $user->email ?? '' }}'
                    if (this.checked) {
                        $('#information-part .form-check').after(`
                            <div class="form-group company mt-2">
                                <label for="company_billing_name">Nama Instansi</label>
                                <input type="text" class="form-control"
                                        name="company_billing_name"
                                        id="company_billing_name"
                                        value="${billingName}"
                                        >
                            </div>
                            <div class="form-group company">
                                <label for="company_billing_address">Alamat Instansi</label>
                                <input type="text" class="form-control"
                                        name="company_billing_address"
                                        id="company_billing_address"
                                        value="${billingAdress}">
                            </div>
                            <div class="form-group company">
                                <label for="company_billing_phone">No
                                    Telepon Instansi</label>
                                <input type="tel" class="form-control"
                                        name="company_billing_phone"
                                        id="company_billing_phone"
                                        value="${billingPhone}"
                            </div>
                            <div class="form-group company">
                                <label for="company_billing_email">Alamat
                                    Surel Instansi</label>
                                <input type="text" class="form-control"
                                        name="company_billing_email"
                                        id="company_billing_email"
                                        value="${billingEmail}">
                            </div>
                        `)
                    } else {
                        $('.company').remove()
                    }
                })


            });
        </script>

    @endpush
@endsection
