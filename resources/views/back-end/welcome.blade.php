@extends('layouts.moderna')
@section('hero')
<div id="carouselExampleIndicators" class="carousel carousel-fade slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <!-- Slide One - Set the background image for this slide in the line below -->
    <div class="carousel-item active toggle-edit" >
      <div class="carousel-img-d">
        <img src="http://ciptakarya.pu.go.id/satupintu/assets/img_app/slide/home_image.png" class="carousel-img" alt="">
      </div>
      <div class="carousel-caption d-md-block">
        <h2 class="display-4">First Slide</h2>
        <p class="lead">This is a description for the first slide.</p>
      </div>
      <div class="action">
        <button class="btn btn-primary mx-1">
          Tambah 
        </button>
        <button type="button" class="btn btn-success mx-1" data-toggle="modal" data-target="#exampleModal">
          Edit
        </button>
        <button class="btn btn-danger mx-1">
          Hapus
        </button>
      </div>
    </div>
    <!-- Slide Two - Set the background image for this slide in the line below -->
    <div class="carousel-item">
      <div class="carousel-img-d">
      <img src="http://ciptakarya.pu.go.id/satupintu/assets/img_app/2.jpg" class="carousel-img" alt="">
      </div>
      <div class="carousel-caption d-md-block">
        <h2 class="display-4">Second Slide</h2>
        <p class="lead">This is a description for the second slide.</p>
      </div>
    </div>
    <!-- Slide Three - Set the background image for this slide in the line below -->
    <div class="carousel-item">
      <div class="carousel-img-d">
      <img src="http://ciptakarya.pu.go.id/satupintu/assets/img_app/1.jpg" class="carousel-img" alt="">
      </div>
      <div class="carousel-caption d-md-block">
        <h2 class="display-4">Third Slide</h2>
        <p class="lead">This is a description for the third slide.</p>
      </div>
    </div>
  
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
  </div>
</div>
@endsection
@section('main')
<section class="blog">
  <div class="container">
  
    <div class="section-title">
      <h2>Berita Terkini</h2>
    </div>

    <div class="row">
  
      <div class="col-md-4 entries" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
        <article class="entry">

          <div class="entry-img">
            <img src="/Moderna/assets/img/blog-post-4.jpg" class="img-thumbnail" alt="">
          </div>

          <h2 class="entry-title">
            <a href="{{route('detailberita')}}">
              Non rem rerum nam.
            </a>
          </h2>

          <div class="entry-meta">
          <ul>
              <li class="d-flex align-items-center">
                  <i class="icofont-user"></i>
                  <a href="{{route('detailberita')}}" class="text-sm">
                    <small>John Doe</small> 
                  </a>
                  </li>
              <li class="d-flex align-items-center">
                <i class="icofont-wall-clock"></i>
                <a href="{{route('detailberita')}}">
                  <small><time datetime="2020-01-01">Jan 1, 2020</time></small>
                </a>
              </li>
          </ul>
          </div>

          <div class="entry-content">
          <small>
            <p class="text-justify">
              Qui proident elit amet veniam id est. Excepteur minim culpa voluptate sit est incididunt ipsum culpa velit id ipsum. Eu nostrud aliqua eu ipsum dolore Lorem ea proident sunt ipsum reprehenderit est reprehenderit. Ea deseruntt.
            </p>
          </small>
          </div>

        </article><!-- End blog entry -->
      </div>

      <div class="col-md-4 entries" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
        <article class="entry">

          <div class="entry-img">
          <img src="/Moderna/assets/img/blog-post-4.jpg" alt="" class="img-fluid">
          </div>

          <h2 class="entry-title">
            <a href="{{route('detailberita')}}">
              Non rem rerum nam.
            </a>
          </h2>

          <div class="entry-meta">
          <ul>
              <li class="d-flex align-items-center">
                  <i class="icofont-user"></i>
                  <a href="{{route('detailberita')}}" class="text-sm">
                    <small>John Doe</small> 
                  </a>
                  </li>
              <li class="d-flex align-items-center">
                <i class="icofont-wall-clock"></i>
                <a href="{{route('detailberita')}}">
                  <small><time datetime="2020-01-01">Jan 1, 2020</time></small>
                </a>
              </li>
          </ul>
          </div>

          <div class="entry-content">
          <small>
            <p class="text-justify">
              Qui proident elit amet veniam id est. Excepteur minim culpa voluptate sit est incididunt ipsum culpa velit id ipsum. Eu nostrud aliqua eu ipsum dolore Lorem ea proident sunt ipsum reprehenderit est reprehenderit. Ea deseruntt.
            </p>
          </small>
          </div>

        </article><!-- End blog entry -->
      </div>
        
      <div class="col-md-4 entries" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
        <article class="entry">

          <div class="entry-img">
          <img src="/Moderna/assets/img/blog-post-2.jpg" alt="" class="img-fluid">
          </div>

          <h2 class="entry-title">
            <a href="{{route('detailberita')}}">
              Non rem rerum nam.
            </a>
          </h2>

          <div class="entry-meta">
          <ul>
              <li class="d-flex align-items-center">
                  <i class="icofont-user"></i>
                  <a href="{{route('detailberita')}}" class="text-sm">
                    <small>John Doe</small> 
                  </a>
                  </li>
              <li class="d-flex align-items-center">
                <i class="icofont-wall-clock"></i>
                <a href="{{route('detailberita')}}">
                  <small><time datetime="2020-01-01">Jan 1, 2020</time></small>
                </a>
              </li>
          </ul>
          </div>

          <div class="entry-content">
          <small>
            <p class="text-justify">
              Qui proident elit amet veniam id est. Excepteur minim culpa voluptate sit est incididunt ipsum culpa velit id ipsum. Eu nostrud aliqua eu ipsum dolore Lorem ea proident sunt ipsum reprehenderit est reprehenderit. Ea deseruntt.
            </p>
          </small>
          </div>

        </article><!-- End blog entry -->
      </div>

      <div class="col-md-4 entries" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
        <article class="entry">

          <div class="entry-img">
          <img src="/Moderna/assets/img/blog-post-2.jpg" alt="" class="img-fluid">
          </div>

          <h2 class="entry-title">
            <a href="{{route('detailberita')}}">
              Non rem rerum nam.
            </a>
          </h2>

          <div class="entry-meta">
          <ul>
              <li class="d-flex align-items-center">
                  <i class="icofont-user"></i>
                  <a href="{{route('detailberita')}}" class="text-sm">
                    <small>John Doe</small> 
                  </a>
                  </li>
              <li class="d-flex align-items-center">
                <i class="icofont-wall-clock"></i>
                <a href="{{route('detailberita')}}">
                  <small><time datetime="2020-01-01">Jan 1, 2020</time></small>
                </a>
              </li>
          </ul>
          </div>

          <div class="entry-content">
          <small>
            <p class="text-justify">
              Qui proident elit amet veniam id est. Excepteur minim culpa voluptate sit est incididunt ipsum culpa velit id ipsum. Eu nostrud aliqua eu ipsum dolore Lorem ea proident sunt ipsum reprehenderit est reprehenderit. Ea deseruntt.
            </p>
          </small>
          </div>

        </article><!-- End blog entry -->
      </div>

      <div class="col-md-4 entries" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
        <article class="entry">

          <div class="entry-img">
          <img src="/Moderna/assets/img/blog-post-2.jpg" alt="" class="img-fluid">
          </div>

          <h2 class="entry-title">
            <a href="{{route('detailberita')}}">
              Non rem rerum nam.
            </a>
          </h2>

          <div class="entry-meta">
          <ul>
              <li class="d-flex align-items-center">
                  <i class="icofont-user"></i>
                  <a href="{{route('detailberita')}}" class="text-sm">
                    <small>John Doe</small> 
                  </a>
                  </li>
              <li class="d-flex align-items-center">
                <i class="icofont-wall-clock"></i>
                <a href="{{route('detailberita')}}">
                  <small><time datetime="2020-01-01">Jan 1, 2020</time></small>
                </a>
              </li>
          </ul>
          </div>

          <div class="entry-content">
          <small>
            <p class="text-justify">
              Qui proident elit amet veniam id est. Excepteur minim culpa voluptate sit est incididunt ipsum culpa velit id ipsum. Eu nostrud aliqua eu ipsum dolore Lorem ea proident sunt ipsum reprehenderit est reprehenderit. Ea deseruntt.
            </p>
          </small>
          </div>

        </article><!-- End blog entry -->
      </div>

      <div class="col-md-4 entries" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
        <article class="entry">

          <div class="entry-img">
          <img src="/Moderna/assets/img/blog-post-2.jpg" alt="" class="img-fluid">
          </div>

          <h2 class="entry-title">
            <a href="{{route('detailberita')}}">
              Non rem rerum nam.
            </a>
          </h2>

          <div class="entry-meta">
          <ul>
              <li class="d-flex align-items-center">
                  <i class="icofont-user"></i>
                  <a href="{{route('detailberita')}}" class="text-sm">
                    <small>John Doe</small> 
                  </a>
                  </li>
              <li class="d-flex align-items-center">
                <i class="icofont-wall-clock"></i>
                <a href="{{route('detailberita')}}">
                  <small><time datetime="2020-01-01">Jan 1, 2020</time></small>
                </a>
              </li>
          </ul>
          </div>

          <div class="entry-content">
          <small>
            <p class="text-justify">
              Qui proident elit amet veniam id est. Excepteur minim culpa voluptate sit est incididunt ipsum culpa velit id ipsum. Eu nostrud aliqua eu ipsum dolore Lorem ea proident sunt ipsum reprehenderit est reprehenderit. Ea deseruntt.
            </p>
          </small>
          </div>
        </article><!-- End blog entry -->
      </div>
    </div>
  </div>
</section><!-- End Features Section -->
<a href="#" class="float" style="font-size: 2.5rem">
  <i class="fab fa-whatsapp"></i>
</a>
@endsection