<style type="text/css">
    body,
    table,
    td {
        font-family: Arial, sans-serif !important;
    }

    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }

    table,
    tr,
    td {
        border-collapse: collapse;
        border-spacing: 0;
    }

</style>

<table
    width="700"
    cellpadding="0"
    cellspacing="0"
    align="center"
    border="0"
    bgcolor="#98dcaf"
>
    <tr>
        <td height="20"></td>
    </tr>
    <tr>
        <td>
            <table
                width="600"
                cellpadding="0"
                cellspacing="0"
                align="center"
                border="0"
            >
                <tr>
                    <td width="60">
                        <img
                            src="http://bbsbg.bmtechnology.my.id/setting/logo/1616342910_200721%20-%20Logo%20PUPR%20-%20Logogram%20Square%20-%20Primary%20Color.png"
                            width="50px"
                        />
                    </td>
                    <td>
                        <span style="font-size:12px;">Kementerian Pekerjaan Umum dan Perumahan Rakyat</span><br />
                        <span style="font-size:12px;">Direktorat Jenderal Cipta Karya</span><br />
                        <span style="font-size:12px;">Balai Bahan dan Struktur Bangunan Gedung</span>
                    </td>
                    <td>
                        <img
                            src="http://bbsbg.bmtechnology.my.id/maskot/foto/1616345377_Kang%20BaTur%20(1).png"
                            width="70px"
                        />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20"></td>
    </tr>
</table>

<table
    width="700"
    cellpadding="0"
    cellspacing="0"
    align="center"
    border="0"
    bgcolor="#dbf2e3"
>
    <tr>
        <td>
            <table
                width="600"
                cellpadding="5"
                cellspacing="0"
                align="center"
                border="0"
            >
                <tr>
                    <td colspan="3">
                        <br />
                        <h2>Halo {{ $nama }}!,</h2>
                        <h3>Pendaftaran pengujian anda sedang diverifikasi.</h3>
                        <h3>Silahkan menunggu pemberitahuan lebih lanjut mengenai jadwal pengujian pada email anda</h3>
                        <br />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table
    width="700"
    cellpadding="0"
    cellspacing="0"
    align="center"
    border="0"
    bgcolor="#98dcaf"
>
    <tr>
        <td height="20"></td>
    </tr>
    <tr>
        <td>
            <table
                width="600"
                cellpadding="0"
                cellspacing="0"
                align="center"
                border="0"
            >
                <tr>
                    <td><strong style="font-size:12px;color: grey;">Copyright &copy 2021</strong></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20"></td>
    </tr>
</table>
