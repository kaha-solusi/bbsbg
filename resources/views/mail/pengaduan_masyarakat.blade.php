<div>
    <h2>{{ $judul }}</h2>
    <h4>Pelapor: @guest
        Anonim / Tidak Login
        @else
        {{ Auth::user()->name }}
    @endguest</h4>
    <p>{{ $isi }}</p>

    <ul>
        <li>Tanggal Kejadian: {{ $tanggal }}</li>
        <li>Lokasi Kejadian: {{ $lokasi }}</li>
    </ul>
</div>
