<div>
    <table>
        <tbody>
            <tr>
                <td>Nama</td>
                <td>: {{ $data->nama }}</td>
            </tr>
            <tr>
                <td>NIP</td>
                <td>: {{ $data->nip }}</td>
            </tr>
            <tr>
                <td>No Handphone / Telp</td>
                <td>: {{ $data->no_telp }}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>: {{ $data->email }}</td>
            </tr>
            <tr>
                <td>Nama Pemberi Gratifikasi</td>
                <td>: {{ $data->nama_pemberi }}</td>
            </tr>
            <tr>
                <td>Jabatan Pemberi Gratifikasi</td>
                <td>: {{ $data->jabatan_pemberi }}</td>
            </tr>
            <tr>
                <td>Tanggal di terima Gratifikasi</td>
                <td>: {{ $data->tanggal }}</td>
            </tr>
            <tr>
                <td>Uraian Gratifikasi</td>
                <td>: {{ $data->uraian }}</td>
            </tr>
            <tr>
                <td>Taksiran Nilai Gratifikasi</td>
                <td>: {{ $data->taksiran }}</td>
            </tr>
            <tr>
                <td>Jenis Gratifikasi</td>
                <td>: {{ $data->jenis }}</td>
            </tr>
        </tbody>
    </table>
</div>
