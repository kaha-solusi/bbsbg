@component('mail::message')
# Hai {{ $name }}

Anda mendapatkan pesanan baru

Berikut adalah detail pengujian produk yang dipesan:

@component('mail::table')
| Nama Produk                  | Tipe Produk                  | Jumlah                        |
| :-----------------           | :-------------------         | :----------------------------:|
@foreach ($products as $order)
| {{ $order['product_name'] }} | {{ $order['product_type'] }} | {{ $order['product_count'] }} |
@endforeach
@endcomponent
@endcomponent
