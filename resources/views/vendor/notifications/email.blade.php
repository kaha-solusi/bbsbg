{{-- <style type="text/css">
    body,
    table,
    td {
        font-family: Arial, sans-serif !important;
    }

    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }

    table,
    tr,
    td {
        border-collapse: collapse;
        border-spacing: 0;
    }

</style>
<table
    width="700"
    cellpadding="0"
    cellspacing="0"
    align="center"
    border="0"
    bgcolor="#98dcaf"
>
    <tr>
        <td height="20"></td>
    </tr>
    <tr>
        <td>
            <table
                width="600"
                cellpadding="0"
                cellspacing="0"
                align="center"
                border="0"
            >
                <tr>
                    <td width="60">
                        <img
                            src="http://bbsbg.bmtechnology.my.id/setting/logo/1616342910_200721%20-%20Logo%20PUPR%20-%20Logogram%20Square%20-%20Primary%20Color.png"
                            width="50px"
                        />
                    </td>
                    <td>
                        <span style="font-size:12px;">Kementerian Pekerjaan Umum dan Perumahan Rakyat</span><br />
                        <span style="font-size:12px;">Direktorat Jenderal Cipta Karya</span><br />
                        <span style="font-size:12px;">Balai Bahan dan Struktur Bangunan Gedung</span>
                    </td>
                    <td>
                        <img
                            src="http://bbsbg.bmtechnology.my.id/maskot/foto/1616345377_Kang%20BaTur%20(1).png"
                            width="70px"
                        />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20"></td>
    </tr>
</table>
<table
    width="700"
    cellpadding="0"
    cellspacing="0"
    align="center"
    border="0"
    bgcolor="#dbf2e3"
>
    <tr>
        <td>
            <table
                width="600"
                cellpadding="5"
                cellspacing="0"
                align="center"
                border="0"
            >
                <tr>
                    <td colspan="3">
                        <br />
                        <h2>Halo!,</h2>
                        <h3>Anda menerima email ini karena kami menerima permintaan reset password untuk akun anda.</h3>
                        @lang('Klik tombol Reset Password dibawah dan anda akan diarahkan ke halaman reset password.')
                        <br />
                    </td>
                </tr>
                @isset($actionText)
                    <?php
                    switch ($level) {
                        case 'success':
                        case 'error':
                            $color = $level;
                            break;
                        default:
                            $color = 'primary';
                    }
                    ?>
                    @component('mail::button', ['url' => $actionUrl, 'color' => $color])
                        {{ $actionText }}
                    @endcomponent
                @endisset
                @lang('Link reset password akan kadaluwarsa dalam 60 menit.')<br>
                @lang('Jika anda tidak meminta reset password, tidak ada tindakan lebih lanjut yang diperlukan.')<br>
                <tr>
                    <td>
                        <hr>
                        @isset($actionText)
                            @lang(
                            "Jika anda memiliki masalah saat mengklik tombol \":actionText\" , copy dan paste URL
                            dibawah\n".
                            'ke dalam browser anda:',
                            [
                            'actionText' => $actionText,
                            ]
                            ) <span class="break-all">[{{ $displayableActionUrl }}]</span>
                        @endisset
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table
    width="700"
    cellpadding="0"
    cellspacing="0"
    align="center"
    border="0"
    bgcolor="#98dcaf"
>
    <tr>
        <td height="20"></td>
    </tr>
    <tr>
        <td>
            <table
                width="600"
                cellpadding="0"
                cellspacing="0"
                align="center"
                border="0"
            >
                <tr>
                    <td><strong style="font-size:12px;color: grey;">Copyright &copy 2021</strong></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20"></td>
    </tr>
</table> --}}
@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
# @lang('Hello!')
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('Terima kasih'),<br>
{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang(
    "Jika anda memiliki masalah saat mengklik tombol \":actionText\" , salin dan tempelkan URL dibawah\n".
    'pada peramban anda:',
    [
        'actionText' => $actionText,
    ]
) <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>
@endslot
@endisset
@endcomponent
