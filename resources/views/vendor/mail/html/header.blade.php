<tr>
    <td class="header">
        <a
            href="{{ $url }}"
            style="display: inline-block;"
        >
            <div style="display: flex; flex-direction: row; align-items: center; justify-content: space-between">
                <div style="display: flex; flex-direction: row; align-items: center">
                    <img
                        src="http://bbsbg.bmtechnology.my.id/setting/logo/1616342910_200721%20-%20Logo%20PUPR%20-%20Logogram%20Square%20-%20Primary%20Color.png"
                        width="50px"
                    />
                    <div style="text-align: left; margin-left: 10px">
                        <span style="font-size:12px;">Kementerian Pekerjaan Umum dan Perumahan Rakyat</span><br />
                        <span style="font-size:12px;">Direktorat Jenderal Cipta Karya</span><br />
                        <span style="font-size:12px;">Balai Bahan dan Struktur Bangunan Gedung</span>
                    </div>
                </div>
                @if (trim($slot) === 'Laravel')
                    <img
                        src="http://bbsbg.bmtechnology.my.id/setting/logo/1616342910_200721%20-%20Logo%20PUPR%20-%20Logogram%20Square%20-%20Primary%20Color.png"
                        width="50px"
                    />
                @else
                    {{ $slot }}
                @endif
            </div>

        </a>
    </td>
</tr>
