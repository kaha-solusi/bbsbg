@extends('layouts.app')

@section('main')
    <div
        class="d-flex justify-content-center align-items-center flex-column"
        style="height: 90vh !important;"
    >
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-5">
                    <div class="card shadow rounded border-0">

                        <div class="card-body">
                            @if (session('status'))
                                <div
                                    class="alert alert-success"
                                    role="alert"
                                >
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form
                                method="POST"
                                action="{{ route('password.email') }}"
                            >
                                @csrf

                                <div class="form-group">
                                    <label for="email">{{ __('Alamat E-mail') }}</label>

                                    <input
                                        id="email"
                                        type="email"
                                        class="form-control @error('email') is-invalid @enderror"
                                        name="email"
                                        value="{{ old('email') }}"
                                        required
                                        autocomplete="email"
                                        autofocus
                                    >

                                    @error('email')
                                        <span
                                            class="invalid-feedback"
                                            role="alert"
                                        >
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group mb-0">
                                    <div class="text-right">
                                        <button
                                            type="submit"
                                            class="btn btn-primary"
                                        >
                                            {{ __('Kirim E-mail') }}
                                        </button>
                                    </div>


                                </div>
                            </form>
                        </div>

                        <div class="card-footer">
                            Ingat kata sandi?<a
                                href="{{ route('login', ['return' => request()->query('return')]) }}"
                                class="ml-2"
                            >Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
