@extends('layouts.app')

@section('main')
    <div
        class="container"
        style="height: 90vh;"
    >

        <div class="row h-100 align-items-center justify-content-center">
            <div class="align-self-center justify-content-center">
                <div class="card shadow rounded border-0">
                    <div class="card-body p-4">
                        <div class="col align-self-center mb-4 justify-content-center text-center">
                            @if (!empty($setting->logo))
                                <div class="justify-content-center align-self-center mb-3 d-flex">
                                    <img
                                        src="{{ asset($setting->logo) }}"
                                        class="img-logo"
                                        alt=""
                                    >
                                </div>
                                <div class="justify-content-center align-self-center d-flex">
                                    <h5 class="text-sm">{!! $setting->nama !!}</h5>
                                </div>
                            @else
                                <div class="justify-content-center align-self-center mb-3 d-flex">
                                    <img
                                        src="{{ asset('assets/default/default_logo.png') }}"
                                        class="img-logo"
                                        alt=""
                                    >
                                </div>
                                <div class="justify-content-center align-self-center d-flex">
                                    <h5 class="text-sm">{!! $setting->nama !!}</h5>
                                </div>
                            @endif
                        </div>

                        <form
                            method="POST"
                            action="{{ route('login', ['return' => request()->query('return')]) }}"
                        >
                            @csrf

                            <div class="form-group row">
                                <label
                                    for="email"
                                    class="col-md-4 col-form-label text-md-right"
                                >Username/Email</label>

                                <div class="col-md-6">
                                    <input
                                        id="email"
                                        type="text"
                                        class="form-control @error('email') is-invalid @enderror"
                                        name="email"
                                        value="{{ old('email') }}"
                                        required
                                        autocomplete="email"
                                        autofocus
                                    >

                                    @error('email')
                                        <span
                                            class="invalid-feedback"
                                            role="alert"
                                        >
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label
                                    for="password"
                                    class="col-md-4 col-form-label text-md-right"
                                >Password</label>

                                <div class="col-md-6">
                                    <input
                                        id="password"
                                        type="password"
                                        class="form-control @error('password') is-invalid @enderror"
                                        name="password"
                                        required
                                        autocomplete="current-password"
                                    >

                                    @error('password')
                                        <span
                                            class="invalid-feedback"
                                            role="alert"
                                        >
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4 mb-2">
                                    <button
                                        type="submit"
                                        class="btn btn-primary"
                                    >
                                        {{ __('Login') }}
                                    </button>
                                </div>

                                <div class="col-md-8 offset-md-4">
                                    Belum punya akun?<a
                                        href="{{ route('register', ['return' => request()->query('return')]) }}"
                                        class="ml-2"
                                    >Daftar</a>
                                </div>

                                <div class="col-md-8 offset-md-4">
                                    <a href="{{ route('password.request', ['return' => request()->query('return')]) }}">Lupa
                                        kata sandi</a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
