@extends('layouts.app')
@section('main')
@php
    $setting = \App\Models\SettingApp::find(1);
@endphp
<div class="d-flex my-10 align-items-center justify-content-center align-self-center">
    <img src="{{asset($setting->app)}}" alt="{{$setting->nama}}">
</div>
@if (session()->has('failed'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('failed') }}
    </div>
@endif
<div class="d-flex bg-light align-items-center justify-content-center align-self-center mb-5">
    <div class="card w-50 justify-content-center">  
        <div class="card-header bg-hijau text-dark">
            Reset Password
        </div>

        <form method="POST" action="{{ route('client.password.update') }}">
        @csrf
        <div class="card-body">

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('Password') }} <small class="text-success">*Harus diisi</small></label>
                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" placeholder="New Password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label>Password Confirmation <small class="text-success">*Harus diisi</small></label>
                    <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" value="{{ old('password_confirmation') }}" placeholder="New Password Confirmation">
                    @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>  
        <div class="card-footer">
            <div class="row">
                <div class="col-md-2">
                    <button type="submit" class="btn d-block btn-warning" style="width: 100%;">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection
