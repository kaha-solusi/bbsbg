@extends('layouts.app')

@section('main')
    <div class="container p-4">

        <div class="row align-items-center justify-content-center">
            <!-- <h3>Sedang dalam pengembangan</h3> -->
            <div class="col-md-8 align-self-center justify-content-center">

                <div class="card shadow-lg">

                    <div class="card-body">
                        <div class="col align-self-center mb-4 justify-content-center text-center">
                            @if (!empty($setting->logo))
                                <div class="justify-content-center align-self-center mb-3 d-flex">
                                    <img
                                        src="{{ asset($setting->logo) }}"
                                        class="img-logo"
                                        alt=""
                                    >
                                </div>
                                <div class="justify-content-center align-self-center d-flex">
                                    <h5 class="">{!! $setting->nama !!}</h5>
                                </div>
                            @else
                                <div class="justify-content-center align-self-center mb-3 d-flex">
                                    <img
                                        src="{{ asset('assets/default/default_logo.png') }}"
                                        class="img-logo"
                                        alt=""
                                    >
                                </div>
                                <div class="justify-content-center align-self-center d-flex">
                                    <h5 class="">{!! $setting->nama !!}</h5>
                                </div>
                            @endif
                        </div>
                        <div class="alert alert-info fade show">
                            <h5 style="font-weight: bold; margin-bottom: 8px;">Pendaftaran Pengguna</h5>
                            <p style="margin-bottom: 0px;">Harap memilih Status Pegawai
                                terlebih dahulu, dengan ketentuan</p>
                            <ol style="margin-bottom: 0px;">
                                <li>Pegawai PNS harap mengisikan NIP</li>
                                <li>Pegawai Non PNS harap mengisikan NRP</li>
                                <li>Jika tidak memiliki NIP/NRP, isikan 0</li>
                            </ol>
                        </div>
                        <form
                            action="{{ route('register', ['return' => request()->query('return')]) }}"
                            method="post"
                        >
                            @csrf
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="name">Nama <small class="text-success">*Harus diisi</small></label>
                                        <input
                                            type="text"
                                            name="name"
                                            id="usernam"
                                            value="{{ old('name') }}"
                                            class="form-control @error('name') is-invalid @enderror"
                                            autofocus
                                            required
                                        >
                                        @error('name')
                                            <span
                                                class="invalid-feedback"
                                                role="alert"
                                            >
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label id="nip">NIP/NRP <small class="text-success">*Harus diisi</small></label>
                                        <input
                                            type="text"
                                            name="nip"
                                            id="nip"
                                            class="form-control corner-edge"
                                            required=""
                                        >
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Username <small class="text-success">*Harus diisi</small></label>
                                        <input
                                            type="text"
                                            name="username"
                                            class="form-control @error('username') is-invalid @enderror"
                                            value="{{ old('username') }}"
                                        >
                                        @error('username')
                                            <span
                                                class="invalid-feedback"
                                                role="alert"
                                            >
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Email <small class="text-success">*Harus diisi</small></label>
                                        <input
                                            type="email"
                                            name="email"
                                            class="form-control @error('email') is-invalid @enderror"
                                            value="{{ old('email') }}"
                                        >
                                        @error('email')
                                            <span
                                                class="invalid-feedback"
                                                role="alert"
                                            >
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Password <small class="text-success">*Harus diisi</small></label>
                                        <input
                                            type="password"
                                            name="password"
                                            class="form-control @error('password') is-invalid @enderror"
                                            value="{{ old('password') }}"
                                        >
                                        @error('password')
                                            <span
                                                class="invalid-feedback"
                                                role="alert"
                                            >
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Password Confirmation <small class="text-success">*Harus diisi</small></label>
                                        <input
                                            type="password"
                                            name="password_confirmation"
                                            class="form-control @error('password_confirmation') is-invalid @enderror"
                                            value="{{ old('password_confirmation') }}"
                                        >
                                        @error('password_confirmation')
                                            <span
                                                class="invalid-feedback"
                                                role="alert"
                                            >
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">

                                    <div class="form-group col-md-6">
                                        <label>Nomor Telepon <small class="text-success">*Harus diisi</small></label>
                                        <input
                                            type="text"
                                            name="phone_number"
                                            class="form-control @error('phone_number') is-invalid @enderror"
                                            value="{{ old('phone_number') }}"
                                            placeholder="Contoh: 6289538..."
                                        >
                                        @error('phone_number')
                                            <span
                                                class="invalid-feedback"
                                                role="alert"
                                            >
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div
                                        class="form-group col-md-6"
                                        x-data="instansi()"
                                    >
                                        <label>Instansi <small class="text-success">*Harus diisi</small></label>
                                        <select
                                            class="form-control "
                                            x-on:change="setSelectedInstansi"
                                            :value='instansi'
                                            name="group"
                                        >
                                            <option
                                                value=""
                                                hidden
                                            >Silahkan pilih...</option>

                                            <optgroup label="Pemerintah">
                                                <option value="kementerian-pupr">Kementerian PUPR</option>
                                                <option value="pemerintahan-lainnya">Pemerintahan Lainnya</option>
                                            </optgroup>
                                            <option value="nonpemerintah">Non-Pemerintah</option>
                                        </select>
                                        @error('group')
                                            <span
                                                class="invalid-feedback"
                                                role="alert"
                                            >
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <div x-show="instansi === 'kementerian-pupr'">
                                            <div class="mt-2">
                                                <label>Unit Organisasi <small class="text-success">*Harus diisi</small></label>
                                                <select
                                                    class="form-control"
                                                    x-on:change="setSelectedUnitOrganisasi"
                                                    name="unit_organisasi"
                                                >
                                                    <option value="">Silahkan pilih...</option>
                                                    <template x-for="option in unitOrganisasi">
                                                        <option
                                                            :key="option.value"
                                                            :value="option.value"
                                                            x-text="option.text"
                                                        ></option>
                                                    </template>
                                                </select>
                                                @error('unit_organisasi')
                                                    <span
                                                        class="invalid-feedback"
                                                        role="alert"
                                                    >
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="mt-2">
                                                <label>Unit Kerja</label>
                                                <select
                                                    class="form-control "
                                                    name="unit_kerja"
                                                >
                                                    <option value="">Silahkan pilih...</option>
                                                    <template x-for="option in unitKerja">
                                                        <option
                                                            :key="option.value"
                                                            :value="option.value"
                                                            x-text="option.text"
                                                        ></option>
                                                    </template>
                                                </select>
                                            </div>
                                            @error('unit_kerja')
                                                <span
                                                    class="invalid-feedback"
                                                    role="alert"
                                                >
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div x-show="instansi === 'pemerintahan-lainnya' || instansi === 'nonpemerintah'">
                                            <div class="mt-2">
                                                <label>Nama Instansi <small class="text-success">*Harus diisi</small></label>
                                                <input
                                                    type="text"
                                                    name="nama_instansi"
                                                    class="form-control "
                                                    value="{{ old('nama_instansi') }}"
                                                >
                                                @error('nama_instansi')
                                                    <span
                                                        class="invalid-feedback"
                                                        role="alert"
                                                    >
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label>Alamat <small class="text-success">*Harus diisi</small></label>
                                        <textarea
                                            class="form-control @error('alamat') is-invalid @enderror"
                                            name="alamat"
                                            rows="5"
                                        >{{ old('alamat') }}</textarea>
                                        @error('alamat')
                                            <span
                                                class="invalid-feedback"
                                                role="alert"
                                            >
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-4">
                                        <button
                                            type="submit"
                                            class="btn d-block btn-primary"
                                            style="width: 100%;"
                                        >Daftar</button>
                                    </div>
                                    <div class="col">
                                        <a
                                            href="{{ route('login', ['return' => request()->query('return')]) }}"
                                            class="btn btn-link"
                                            style="color: black;"
                                        >Sudah punya akun?</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script
        src="//unpkg.com/alpinejs"
        defer
    ></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2()
                .select2({
                    allowClear: false,
                    theme: "bootstrap4"
                });;
        });

        function instansi() {
            return {
                instansi: '',
                unitOrganisasi: [],
                unitKerja: [],
                setSelectedInstansi($event) {
                    this.instansi = $event.target.value
                    if ($event.target.value === 'kementerian-pupr') {
                        this.getUnitOrganisasi();
                    }
                },
                setSelectedUnitOrganisasi($event) {
                    this.getUnitKerja($event.target.value)
                },
                getUnitOrganisasi() {
                    fetch('{{ route('api.instansi.get-unit-organisasi') }}')
                        .then(res => res.json())
                        .then(data => {
                            const options = data.map(d => ({
                                value: d.id,
                                text: d.nama
                            }))

                            this.unitOrganisasi = options
                        })
                },
                getUnitKerja(unitOrganisasiId) {
                    if (unitOrganisasiId) {
                        const url = '{{ route('api.instansi.get-unit-kerja', '') }}' + `/${unitOrganisasiId}`
                        fetch(url)
                            .then(res => res.json())
                            .then(data => {
                                const options = data.map(d => ({
                                    value: d.id,
                                    text: d.nama
                                }))

                                this.unitKerja = options
                            })
                    } else {
                        this.unitKerja = []
                    }
                }
            }
        }
    </script>
@endsection
