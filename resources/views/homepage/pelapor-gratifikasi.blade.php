@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'Pelapor Gratifikasi'])
<!-- ======= Features Section ======= -->
<section class="features">
<div class="container">

    <div class="section-title" data-aos="fade-up">
        <h2 >Pelapor Gratifikasi</h2>
    </div>

    <div data-aos="fade-up" class="row">
    	<div class="col-md-12">
    		<form action="{{ route('pelapor-gratifikasi.store') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Nama</label>
                                <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{ old('nama') }}">
                                @error('nama')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label>NRP *jika ada</label>
                                <input type="numeric" name="nip" class="form-control @error('nip') is-invalid @enderror" value="{{ old('nip') }}">
                                @error('nip')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label>No Handphone / Telp</label>
                                <input type="numeric" placeholder="Contoh: 628*********" name="no_telp" class="form-control @error('no_telp') is-invalid @enderror" value="{{ old('no_tepl') }}">
                                @error('no_telp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label>Nama Pemberi Gratifikasi</label>
                                <input type="text" name="nama_pemberi" class="form-control @error('nama_pemberi') is-invalid @enderror" value="{{ old('nama_pemberi') }}">
                                @error('nama_pemberi')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label>Jabatan Pemberi Gratifikasi</label>
                                <input type="text" name="jabatan_pemberi" class="form-control @error('jabatan_pemberi') is-invalid @enderror" value="{{ old('jabatan_pemberi') }}">
                                @error('jabatan_pemberi')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label>Tanggal di terima gratifikasi</label>
                                <input type="date" name="tanggal" class="form-control @error('tanggal') is-invalid @enderror" value="{{ old('tanggal') }}">
                                @error('tanggal')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label>Uraian Laporan</label>
                                <textarea name="uraian" class="form-control @error('uraian') is-invalid @enderror" rows="5">{{ old('uraian') }}</textarea>
                                @error('uraian')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label>Taksiran Nilai Gratifikasi</label>
                                <input type="number" name="taksiran" class="form-control @error('taksiran') is-invalid @enderror" value="{{ old('taksiran') }}">
                                @error('taksiran')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label>Jenis Grafikasi</label>
                                <select name="jenis" class="form-control @error('jenis') is-invalid @enderror">
                                    <option value="barang">Barang</option>
                                    <option value="uang">Uang</option>
                                    <option value="setara_uang">Setara Uang</option>
                                </select>
                                @error('jenis')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success btn-sm" style="width: 100%;">Submit</button>
                    </div>
    				@if (session()->has('success'))
    	                <div class="col-md-12">
    	                    <p>{{ session()->get('success') }}</p>
    	                </div>
    				@endif
                </div>
            </form>
    	</div>
    </div>

</div>
</section><!-- End Features Section -->
@endsection
