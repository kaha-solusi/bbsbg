@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'Struktur Organisasi'])
@if ($organisasi->gambar_status == 0 && $organisasi->pdf_status == 0)
<!-- ======= Team Section ======= -->
<section class="team" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
  <div class="container">

    <div class="row justify-content-center align-items-start">
    @forelse($data as $struktur)
      <div class="col-lg-4 col-md-6 d-flex justify-content-center align-items-start">
        <div class="member">
          <div class="member-img">
            <img src="{{ ($struktur->foto == null) ? asset('/assets/default/defaul_user.png') : asset($struktur->foto) }}" class="img-fluid" alt="" style="width: 300px;
  height: 400px;
  object-fit: cover;
  background: no-repeat center center scroll;
  background-image: cover;">
          </div>
          <div class="member-info">
            <h4>{{ $struktur->nama }} || {{ $struktur->nip }}</h4>
            <span>{{ $struktur->jabatan }}</span>
            <p>{{ $struktur->keterangan }}</p>
          </div>
        </div>
      </div>

    @empty
        <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
           <h4>Data Tidak Ada</h4> 
      </div>

    @endforelse
    <div class="col-lg-12 col-md-12 d-flex justify-content-center align-items-center">
        <div class="blog-pagination">
            {{$data->links('layouts.parts.pagination')}}
        </div>
    </div>
    </div>

  </div>
</section><!-- End Team Section -->
@elseif ($organisasi->gambar_status == 1 && $organisasi->pdf_status == 0)
<section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 entries">
                <div class="portfolio-details-container">
                    <div class="owl-carousel portfolio-details-carousel">
                        <img src="{{ asset($organisasi->gambar) }}" alt=""  width="100%">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@elseif ($organisasi->gambar_status == 0 && $organisasi->pdf_status == 1)
<section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 entries">
                <div class="portfolio-details-container">
                    <div class="owl-carousel portfolio-details-carousel">
                        <iframe src="{{ asset($organisasi->pdf) }}" frameborder="0" width="100%" height="700px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
@endsection