@extends('layouts.moderna')
@push('style')
    <style>
        .row-card .card {
            height: 250px;
            margin-bottom: 3em;
        }

    </style>
@endpush
@section('hero')
    <div
        id="carouselExampleIndicators"
        class="carousel slide"
        data-ride="carousel"
    >
        <div class="carousel-inner">
            <?php $i = 0; ?>
            @if (count($slider) > 0)
                @foreach ($slider as $item)
                    <!-- Slide One - Set the background image for this slide in the line below -->
                    <div class="carousel-item @if ($i == 0) active @endif">
                        <div class="carousel-img-d">
                            <img
                                src="{{ asset($item->getFoto()) }}"
                                class="carousel-img"
                                alt=""
                            >
                        </div>
                        <div class="carousel-caption d-md-block">
                            <h2 class="">{{ $item->judul }}</h2>
                            <p class="
                                lead">{{ nl2br($item->keterangan) }}</p>
                        </div>
                    </div>
                    <?php $i++; ?>
                @endforeach
            @else
                <div class="carousel-item active">
                    {{-- <div class="carousel-img-d"> --}}
                    <img
                        src="{{ asset('/Moderna/assets/img/hero-bg.jpg') }}"
                        class="carousel-img"
                        alt=""
                    >
                    {{-- </div> --}}
                    <div class="carousel-caption d-md-block">
                        <h2 class=" ">Balai Bahan dan Struktur Bangunan
                            Gedung</h2>
                        <p class="lead">{!! nl2br("Balai Bahan dan Struktur Bangunan Gedung\nPelayanan Lab Bahan Struktur bangunan") !!}</p>
                    </div>
                </div>
                <div class="carousel-item">
                    {{-- <div class="carousel-img-d"> --}}
                    <img
                        src="{{ asset('/Moderna/assets/img/hero-bg.jpg') }}"
                        class="carousel-img"
                        alt=""
                    >
                    {{-- </div> --}}
                    <div class="carousel-caption d-md-block">
                        <h2 class=" ">Balai Bahan dan Struktur Bangunan
                            Gedung</h2>
                        <p class="lead ">{!! nl2br("Balai Bahan dan Struktur Bangunan Gedung\nPelayanan Lab Bahan Struktur bangunan") !!}</p>
                    </div>
                </div>
            @endif

            <a
                class="carousel-control-prev"
                href="#carouselExampleIndicators"
                role="button"
                data-slide="prev"
            >
                <span
                    class="carousel-control-prev-icon"
                    aria-hidden="true"
                ></span>
                <span class="sr-only">Previous</span>
            </a>
            <a
                class="carousel-control-next"
                href="#carouselExampleIndicators"
                role="button"
                data-slide="next"
            >
                <span
                    class="carousel-control-next-icon"
                    aria-hidden="true"
                ></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
@endsection
@section('main')

    <!-- ======= Layanan Section ======= -->
    <section
        class="pricing"
        data-aos="fade-up"
    >
        <div class="container">
            <div class="section-title">
                <h2>Pelayanan Kami</h2>
            </div>

            <div class="row row-card justify-content-center align-items-center">
                @forelse ($pelayananUI as $index => $pelayanan)
                    <div class="col-12 col-lg-3 col-md-6 text-center mb-2">
                        <img
                            class="rounded-circle w-50"
                            src="{{ asset('assets/img/img-' . $index . '.png') }}"
                            alt="Card image cap"
                        >
                        <div>
                            <h5
                                class="py-2 animate__animated animate__fadeInDown"
                                style="min-height: 4rem"
                            >{{ $pelayanan->pelayanan->nama }}</h5>
                            @if ($pelayanan->pelayanan->id == 4)
                                <a
                                    href="{{ route('home.pelayanan.extra', 4) }}"
                                    class="btn btn-sm d-inline mr-1 btn-primary"
                                >Daftar</a>
                            @elseif ($pelayanan->pelayanan->id === 3)
                                 <a
                                    href="{{ route('home.pelayanan.advisi-teknis') }}"
                                    class="btn btn-sm d-inline mr-1 btn-primary"
                                >Daftar</a>
{{--                                <a--}}
{{--                                    href="{{ route('maintenance') }}"--}}
{{--                                    class="btn btn-sm d-inline mr-1 btn-primary"--}}
{{--                                >Daftar</a>--}}
                            @elseif ($pelayanan->pelayanan->id === 1)
                                {{-- Change to maintenance page --}}
                                <a
                                    href="{{route('home.pelayanan.extra', 1)}}"
                                    class="btn btn-sm d-inline mr-1 btn-primary"
                                >Daftar</a>
                            @else
                                {{-- Change to maintenance page --}}
                                <a
                                    href="{{route('home.pelayanan.extra', 2)}}"
                                    class="btn btn-sm d-inline mr-1 btn-primary"
                                >Daftar</a>
                            @endif
                        </div>
                    </div>
                @empty
                    <h4>Layanan belum bisa di akses saat ini</h4>
                @endforelse

            </div>
        </div>
    </section><!-- End Layanan Section -->

    <!-- ======= Why Us Section ======= -->
    @if (!empty($videoUI->link))
        <section
            class="why-us"
            data-aos="fade-up"
            date-aos-delay="200"
        >
            <div class="container">

                <div class="row">
                    <div class="col-lg-5 justify-content-center align-items-center">
                        <iframe
                            class="video-box"
                            src="https://www.youtube.com/embed/{{ $videoUI->getVideoId() }}?playlist={{ $videoUI->getVideoId() }}&loop=1&controls=0&autoplay=1&mute=1"
                        >
                        </iframe>
                        {{-- <img src="http://img.youtube.com/vi/{{$videoUI->getVideoId()}}/0.jpg" class="img-fluid" alt="">
            <a href="https://www.youtube.com/watch?v={{$videoUI->getVideoId()}}" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> --}}
                    </div>

                    <div class="col-lg-7 d-flex flex-column justify-content-center p-5">

                        <div class="icon-box">
                            <div class="icon">
                                <img
                                    src="{{ asset($videoUI->logo) }}"
                                    class="img img-thumbnail"
                                    alt=""
                                >
                            </div>
                            <h4 class="title"><a href="">{{ $videoUI->judul }}</a></h4>
                            <p class="description">{{ $videoUI->deskripsi }}</p>
                        </div>

                    </div>
                </div>

            </div>
        </section>
    @endif

    <section
        class="blog"
        data-aos="fade-up"
    >
        <div class="container">
            <div class="section-title">
                <h2>Berita Terkini</h2>
            </div>

            @if (!empty($berita) && $berita_random)
                <!--Start code-->
                <div
                    class="row no-gutters"
                    data-aos="fade-up"
                >
                    <div class="col-12 pb-5">
                        <!--SECTION START-->
                        <section class="row">
                            <!--Start slider news-->
                            <div class="col-12 col-md-6 pb-0 pb-md-3 pt-2 pr-md-1">
                                <div
                                    id="featured"
                                    class="carousel slide carousel"
                                    data-ride="carousel"
                                >
                                    <?php $j = 0; ?>
                                    <ol class="carousel-indicators top-indicator">
                                        @foreach ($berita as $item)
                                            <li
                                                data-target="#featured"
                                                data-slide-to="{{ $j }}"
                                                class="@if ($j == 0) active @endif"
                                            >
                                            </li>
                                            <?php $j++; ?>
                                        @endforeach
                                    </ol>
                                    <!--carousel inner-->
                                    <div class="carousel-inner carousel-berita">
                                        <?php $k = 0; ?>
                                        @foreach ($berita as $item)
                                            <div class="carousel-item @if ($k == 0) active @endif">
                                                <div class="card border-0 rounded-0 text-light overflow zoom">
                                                    <div class="position-relative">
                                                        <!--thumbnail img-->
                                                        <div class="ratio_left-cover-1 image-wrapper">
                                                            <a href="{{ route('berita.show', $item) }}">
                                                                <img
                                                                    class="berita-img"
                                                                    src="{{ asset($item->getFoto()) }}"
                                                                    alt="{{ $item->judul }}"
                                                                >
                                                            </a>
                                                        </div>
                                                        <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                                            <!--title-->
                                                            <a href="{{ route('berita.show', $item) }}">
                                                                <h2 class="h3 text-white post-title my-1">
                                                                    {{ $item->judul }}
                                                                </h2>
                                                            </a>
                                                            <!-- meta title -->
                                                            <div class="news-meta">
                                                                {{-- <span class="news-author">oleh <a class= font-weight-bold" href="{{route('berita.show', $item)}}">{{$item->penulis}}</a></span> --}}
                                                                <span
                                                                    class="news-date">{{ date('F j, Y', strtotime($item->created_at)) }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $k++; ?>
                                        @endforeach
                                    </div>
                                    <!--end carousel inner-->
                                </div>

                                <!--navigation-->
                                <a
                                    class="carousel-control-prev"
                                    href="#featured"
                                    role="button"
                                    data-slide="prev"
                                >
                                    <span
                                        class="carousel-control-prev-icon"
                                        aria-hidden="true"
                                    ></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a
                                    class="carousel-control-next"
                                    href="#featured"
                                    role="button"
                                    data-slide="next"
                                >
                                    <span
                                        class="carousel-control-next-icon"
                                        aria-hidden="true"
                                    ></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <!--End slider news-->

                            <!--Start box news-->
                            <div class="col-12 col-md-6 pt-2 pl-md-1 pl-sm-0 mb-3 mb-lg-4">
                                <div class="row">
                                    @for ($i = 0; $i < count($berita_random); $i++)
                                        <!--news box-->
                                        <div class="col-md-6 col-sm-12 p-md-0 p-sm-1">
                                            <div class="card border-0 rounded-0 pl-md-1 pb-md-1 overflow zoom card-berita">
                                                <div class="position-relative overflow hidden">
                                                    <!--thumbnail img-->
                                                    <div class="ratio_right-cover-2 image-wrapper">
                                                        <a href="{{ route('berita.show', $berita_random[$i]) }}">
                                                            <img
                                                                class="thumbnail-img  overflow zoom"
                                                                src="{{ asset($berita_random[$i]->getFoto()) }}"
                                                                alt="{{ asset($berita_random[$i]->judul) }}"
                                                            >
                                                        </a>
                                                    </div>
                                                    <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                                        <!--title-->
                                                        <a href="{{ route('berita.show', $berita_random[$i]) }}">
                                                            <h2 class="h5 my-1 text-white">
                                                                {{ $berita_random[$i]->judul }}
                                                            </h2>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                            <!--End box news-->
                        </section>
                        <!--END SECTION-->
                    </div>
                </div>
                <!--end code-->
            @else
                <div
                    class="pricing"
                    data-aos="fade-up"
                >
                    <h4 class="text-center">Tidak Ada Berita</h4>
                </div>
            @endif


        </div>
    </section><!-- End Features Section -->

    <!-- Modal -->
    <div
        class="modal fade"
        id="exampleModalCenter"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
    >
        <div
            class="modal-dialog modal-xl"
            role="document"
        >
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        id="exampleModalLongTitle"
                    >Pengumuman</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <a
                        target="_blank"
                        href="{{ asset('default/img/pengumuman.png') }}"
                    >
                        <img
                            src="{{ asset('default/img/pengumuman.png') }}"
                            class="w-100"
                        />
                    </a>
                </div>
            </div>
        @endsection

        @push('script')
            <script>
                $(document).ready(function() {
                    $(window).on('load', function() {
                        $('#exampleModalCenter').modal('show');
                    });
                })
            </script>
        @endpush
    </div>
