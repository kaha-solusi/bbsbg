@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'Fasilitas Kami'])

<!-- ======= Portfolio Section ======= -->
<section class="portfolio">
    <div class="container">

    <div class="row">
        <div class="col-lg-12">
        {{-- <ul id="portfolio-flters">
            <li data-filter="*" class="filter-active">Semua</li>
            <li data-filter=".filter-bahan">Laboratorium pengujian bahan bangunan</li>
            <li data-filter=".filter-struktur">Laboratorium pengujian struktur bangunan</li> --}}
        </ul>
        </div>
    </div>

    <div class="row portfolio-container" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">

        @forelse ($fasilitas as $item)
        <div class="col-lg-4 col-md-6 filter-{{$item->kategori}}">
          <div class="portfolio-item" style="margin: 0; height: 250px !important;">
              <img src="{{asset($item->foto)}}" class="img-fluid" alt="" style="
              width: 100%;
              height: 250px;
              object-fit: cover;
              background-size: cover;
              background: no-repeat center center scroll;
              ">
              <div class="portfolio-info px-3">
                <a href="{{asset($item->foto)}}" data-gall="portfolioGallery" class="venobox" title="zoom-image" style="color: #fff;">{{ $item->nama }}</a>
                <a href="{{asset($item->foto)}}" data-gall="portfolioGallery" class="venobox" title="zoom-image"><i class="fas fa-search-plus text-white" style="font-size: small;"></i></a>
                <div class="mt-3">
                    {{-- <a href="{{asset($item->foto)}}" data-gall="portfolioGallery" class="venobox" title="zoom-image"><i class="bx bx-plus"></i></a> --}}
                    <a href="#" class="btn btn-outline-light btn-sm" title="Keterangan" onclick="website('{{ $item->keterangan }}', '{{ $item->nama }}')" data-toggle="modal" data-target="#urlModal">Keterangan</a>
                </div>
              </div>
          </div>
          <div class="name-ket lead d-flex justify-content-center align-items-center" style="padding: 0 10px; text-align: center;">
              {{ $item->nama }}
          </div>
        </div>
        @empty
        </div>
        <div class="d-flex justify-content-center align-items-center">
            <h4>Tidak Ada Fasilitas</h4>
        </div>
        @endforelse

    </div>
</section><!-- End Portfolio Section -->

{{-- Modal --}}
<div class="modal fade" id="urlModal" tabindex="-1" aria-labelledby="urlModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="urlModalLabel">Keterangan <span id="nama-gambar"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="website"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
{{-- End Modal --}}
@endsection
@push('script')
{{-- Chart Section --}}
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
	function website(url, nama) {
		$('#website').text(url);
		$('#nama-gambar').text(nama);
	}
</script>
@endpush