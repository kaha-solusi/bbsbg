@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'Kepuasan Pelanggan'])
@if ($data->status_img == 1 && $data->status_pdf == 0)
<section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 entries">
                <div class="portfolio-details-container">
                    <div class="owl-carousel portfolio-details-carousel">
                        <img src="{{ asset($data->url_img) }}" alt=""  width="100%">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@elseif ($data->status_img == 0 && $data->status_pdf == 1)
<section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 entries">
                <div class="portfolio-details-container">
                    <div class="owl-carousel portfolio-details-carousel">
                        <iframe src="{{ asset($data->url_pdf) }}" frameborder="0" width="100%" height="700px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif

<!-- ======= Team Section ======= -->
{{-- <section class="team" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
    <div class="container">
      <div class="row">
        @foreach ($datas as $data)
        <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
                <div class="member-img">
                    <img src="assets/img/team/team-1.jpg" class="img-fluid" alt="">
                    <div class="social" style="background-color: #98dcaf !important;height: 10px;">
                    </div>
                </div>
                <div class="member-info">
                    <h4>{{ $data->nama }}</h4>
                    <span>{{ $data->pelayanan->nama }}</span>
                    <span>
                        @for ($i = 0; $i < $data->tingkat_kepuasan; $i++)
                            <i class="fas fa-star" style="
                            color: #fff707;
                            text-shadow: 0px 2px 0px black;
                            "></i>
                        @endfor
                    </span>
                    <p>{{ $data->komen ?? '-' }}</p>
                </div>
            </div>
        </div>
        @endforeach
      </div>
    </div>
</section><!-- End Team Section --> --}}
@endsection
