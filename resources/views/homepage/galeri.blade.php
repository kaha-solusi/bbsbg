@extends('layouts.moderna')
@push('style')
<style>
.portfolio-item{
    height: 250px;
    width: auto;
    background: no-repeat center center scroll;
    background-size: cover;
}
</style>
@endpush
@section('main')
@include('homepage.parts.breadcrumbs', ['judul' => 'Dokumentasi '.$kategori])

<!-- ======= Portfolio Section ======= -->
<section class="portfolio">
    <div class="container">


    <div class="row">
        <div class="col-lg-12">
            <ul id="portfolio-flters">
                <li data-filter="*" class="filter-active">{{'Dokumentasi '.$kategori}}</li>
            </ul>
        </div>

        <div class="col-lg-12">
            <ul id="portfolio-flters">
            <li data-filter="*" class="filter-active">Semua</li>
            <li data-filter=".filter-foto">Foto</li>
            <li data-filter=".filter-video">Video</li>
            </ul>
        </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
        
        @foreach ($galeri as $item)
            @if ($item->foto != null)
            <div class="col-lg-4 col-md-6 filter-foto">
                <div class="portfolio-item">
                <img src="{{asset($item->foto)}}" class="img-fluid" alt="" style="
                    width: 100%; 
                    height: 300px; 
                    object-fit: cover;
                    background-image: cover;
                    background: no-repeat center center scroll">
                <div class="portfolio-info">
                    <h3><a href="{{asset($item->foto)}}" data-gall="portfolioGallery" class="venobox" title="{{$item->judul}}">{{$item->judul}}</a></h3>
                    <div>
                    <a href="{{asset($item->foto)}}" data-gall="portfolioGallery" class="venobox" title="{{$item->judul}}"><i class="bx bx-plus"></i></a>
                    </div>
                </div>
                </div>
            </div>
            @else
            @php
                parse_str(explode("?", $item->video)[1], $vars);
                $video_id = $vars['v'];
            @endphp
            <div class="col-lg-4 col-md-6 filter-video">
                <div class="portfolio-item why-us">
                    <img src="http://img.youtube.com/vi/{{$video_id}}/0.jpg" class="img-fluid" alt="" style="
                    width: 100%; 
                    height: 300px; 
                    object-fit: cover;
                    background-image: cover;
                    background: no-repeat center center scroll">
                    <div class="portfolio-info">
                        <div class="video-box">
                            <a href="https://www.youtube.com/watch?v={{$video_id}}" class="venobox play-btn mt-4" data-vbtype="video" data-autoplay="true"></a>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        @endforeach
        </div>
        
    </div>
  
    {{-- <div class="row">
        <div class="col-lg-12">
        <ul id="portfolio-flters">
            <li data-filter="*" class="filter-active">Galeri Foto</li>
        </ul>
        </div>
    </div>

    <div class="row portfolio-container" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
        @foreach ($galeri as $item)
            <div class="col-lg-4 col-md-6 filter-kesehatan">
            <div class="portfolio-item">
                <img src="{{asset($item->foto)}}" class="img-fluid img-galeri" alt="">
                <div class="portfolio-info">
                <h3><a href="{{asset($item->foto)}}" data-gall="portfolioGallery" class="venobox" title="kesehatan 1">{{$item->judul}}</a></h3>
                <div>
                    <a href="{{asset($item->foto)}}" data-gall="portfolioGallery" class="venobox" title="kesehatan 1"><i class="bx bx-plus"></i></a>
                </div>
                </div>
            </div>
            </div> 
        @endforeach
    </div> --}}
    
    <div class="d-flex justify-content-center align-items-center">
        {{$galeri->links('layouts.parts.pagination')}}
    </div>

    </div>
</section><!-- End Portfolio Section -->
@endsection