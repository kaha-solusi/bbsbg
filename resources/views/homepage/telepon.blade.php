@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul' => 'Layanan Telepon'])
<div class="container my-5">
@if (session()->has('success'))
<div class="alert alert-success">
    {{session()->get('success')}}
</div>
@endif

<div class="card">
    <div class="card-header">
        <div class="card-title">
            <h4>Formulir pelayanan</h4>
        </div>
    </div>
    <form action="{{route('telepon.store')}}" method="POST">
    @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="nama">Nama</label><small class="text-success">*Harus diisi</small>
            <input type="text" class="form-control" name="nama" id="nama">
            @error('nama')
            <small class="text-danger">
                {{$message}}
            </small>
            @enderror
        </div>
        <div class="form-group">
            <label for="nomor_telepon">Nomor Telepon</label><small class="text-success">*Harus diisi</small>
            <input type="text" name="nomor_telepon" id="nomor_telepon" class="form-control">
            @error('nomor_telepon')
            <small class="text-danger">
                {{$message}}
            </small>
            @enderror
        </div>
        <div class="form-group">
            <label for="deskripsi">Deskripsi</label><small class="text-success">*Tidak harus diisi</small>
            <textarea name="deskripsi" id="deskripsi" rows="2" class="form-control"></textarea>
            @error('deskripsi')
            <small class="text-danger">
                {{$message}}
            </small>
            @enderror
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-sm btn-primary">Kirim</button>
    </div>
    </form>
</div>
</div>

@endsection