@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'Pendaftaran Pengujian Bahan'])
<!-- ======= Features Section ======= -->
<section class="features">
<div class="container">

    <div class="section-title" data-aos="fade-up">
        <h2 >Pendaftaran Pengujian Struktur</h2>
    </div>

    <div data-aos="fade-up" class="row">
    	<div class="col-md-12">
    		<form action="{{ route('permintaan.pengujian.struktur.store') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Nama Pemohon</label>
                            <input type="text" name="nama_pemohon" class="form-control @error('nama_pemohon') is-invalid @enderror" value="{{ old('nama_pemohon') }}">
                            @error('nama_pemohon')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Alamat</label>
                            <textarea name="alamat" class="form-control @error('alamat') is-invalid @enderror" rows="5">{{ old('alamat') }}</textarea>
                            @error('alamat')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Nama Kontak</label>
                            <input type="text" name="nama_kontak" class="form-control @error('nama_kontak') is-invalid @enderror" value="{{ old('nama_kontak') }}">
                            @error('nama_kontak')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Nomor HP Kontak</label>
                            <input type="text" name="nomor_hp_kontak" class="form-control @error('nomor_hp_kontak') is-invalid @enderror" value="{{ old('nomor_hp_kontak') }}" placeholder="Contoh: 628*********">
                            @error('nomor_hp_kontak')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Email</label>
                            <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" aria-describedby="detail">
                            <small id="detail" class="form-text text-success">Status permintaan akan dikirim ke email ini.</small>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Pilih Tanggal Pengujian</label>
                            <input type="date" name="tanggal_pengujian" class="form-control @error('tanggal_pengujian') is-invalid @enderror" value="{{ old('tanggal_pengujian') }}">
                            @error('tanggal_pengujian')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Detail Pengujian</label>
                            <textarea name="detail_layanan" class="form-control @error('detail_layanan') is-invalid @enderror" rows="5"></textarea>
                            @error('detail_layanan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Uraian Pengujian</label>
                            <textarea name="uraian_pengujian" class="form-control @error('uraian_pengujian') is-invalid @enderror" rows="5"></textarea>
                            @error('uraian_pengujian')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Nama dalam LHU ( Cantumkan nama proyek jika diperlukan )</label>
                            <input type="text" name="nama_lhu" class="form-control @error('nama_lhu') is-invalid @enderror" value="{{ old('nama_lhu') }}">
                            @error('nama_lhu')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Email untuk persuratan</label>
                            <input type="text" name="email_persuratan" class="form-control @error('email_persuratan') is-invalid @enderror" value="{{ old('email_persuratan') }}">
                            @error('email_persuratan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success btn-sm" style="width: 100%;">Kirim Pendaftaran</button>
                </div>
                @if (session()->has('success'))
                    <div class="col-md-12">
                        <p>{{ session()->get('success') }}</p>
                    </div>
                @endif
            </div>
        </form>
    	</div>
    </div>
</div>
</section><!-- End Features Section -->
@endsection