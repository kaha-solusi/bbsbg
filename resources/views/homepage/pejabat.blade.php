@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'Profil Pejabat dan Pegawai'])
<!-- ======= Tetstimonials Section ======= -->
<section class="testimonials" data-aos="fade-up">
<div class="container">

    <div class="section-title">
    <h2>Profil Pejabat dan Pegawai</h2>
    </div>

    <div class="owl-carousel testimonials-carousel">

    @foreach ($pejabat as $item)
    <div class="testimonial-item">
        <img src="{{ ($item->foto == null) ? asset('/assets/default/defaul_user.png') : asset($item->foto) }}" class="testimonial-img" alt="" style="width:120px;height:120px;object-fit:cover;background:no-repeat center center scroll;backgorund-size:cover;">
        <h3>{{$item->nama}}</h3>
        <p>
        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
        {{$item->email}}
        <i class="bx bxs-quote-alt-right quote-icon-right"></i>
        </p>
    </div>
    @endforeach
    
    </div>

</div>
</section><!-- End Ttstimonials Section -->
@endsection