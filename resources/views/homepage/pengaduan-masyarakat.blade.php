@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'Pengaduan Masyarakat'])
<!-- ======= Features Section ======= -->
<section class="features">
<div class="container">

    <div class="section-title" data-aos="fade-up">
        <h2 >Pengaduan Masyarakat</h2>
    </div>

    <div data-aos="fade-up" class="row">
    	<div class="col-md-12">
    		<form action="{{ route('pengaduan-masyarakat.store') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Judul Laporan</label>
                                <input type="text" name="judul" class="form-control @error('judul') is-invalid @enderror" value="{{ old('judul') }}">
                                @error('judul')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label>Isi Laporan</label>
                                <textarea name="isi" class="form-control @error('isi') is-invalid @enderror" rows="5"></textarea>
                                @error('isi')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label>Tanggal Kejadian</label>
                                <input type="date" name="tanggal" class="form-control @error('tanggal') is-invalid @enderror" value="{{ old('tanggal') }}">
                                @error('tanggal')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label>Lokasi Kejadian</label>
                                <textarea name="lokasi" class="form-control @error('lokasi') is-invalid @enderror" rows="3"></textarea>
                                @error('lokasi')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success btn-sm" style="width: 100%;">Submit</button>
                    </div>
    				@if (session()->has('success'))
    	                <div class="col-md-12">
    	                    <p>{{ session()->get('success') }}</p>
    	                </div>
    				@endif
                </div>
            </form>
    	</div>
    </div>

</div>
</section><!-- End Features Section -->
@endsection
