@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'Maklumat Pelayanan'])
<!-- ======= Features Section ======= -->
{{-- <section class="features">
    <div class="container">

        <div data-aos="fade-up" class="section-title">
        <h2>Maklumat</h2>
            <ul>
                @php
                    $i = 1
                @endphp
                @foreach (explode(";", $maklumat->isi) as $item)
                @if ($item != "")
                    <li> {{$i++.". ".$item}} </li>
                @endif
                @endforeach
            </ul>
        </div>
        
    </div>
</section> --}}
<!-- End Features Section -->

<section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
    <div class="container">

        <div class="row">

            <div class="col-lg-9 entries">
                @if($maklumat->img != null)
                    <div class="portfolio-details-container">
                        <div class="owl-carousel portfolio-details-carousel">
                            <img src="{{ asset($maklumat->img) }}" alt=""  width="100%">
                            {{-- </iframe> --}}
                        </div>
                    </div>
                @endif

                @if($maklumat->pdf != null)
                    <div class="portfolio-details-container mt-3">
                        <div class="owl-carousel portfolio-details-carousel">
                            {{-- <img src="{{ asset($maklumat->img) }}" alt=""  width="100%"> --}}
                            {{-- </iframe> --}}
                            <iframe src="{{ asset($maklumat->pdf) }}" frameborder="0" width="100%" height="500px"></iframe>
                        </div>
                    </div>
                @endif

                @if($maklumat->isi != null || $maklumat->isi != '')
                    <div class="portfolio-description mt-5">
                        <h2>Informasi Seputar Pelayanan</h2>
                        <ul style="list-style: decimal;text-align: justify;" id="text-list">
                            @php
                                $i = 1
                            @endphp
                            @foreach (explode(";", $maklumat->isi) as $item)
                            @if ($item != "")
                                <li> {{$item}} </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>
            <div class="col-lg-3">
                <div class="sidebar">
                    <h3 class="sidebar-title">File PDF</h3>
                    @if($maklumat->pdf != null)
                    <div class="sidebar-item">
                        <div>
                            <h6><a target="__blank" href="{{ asset($maklumat->pdf) }}">LINK File</a></h4>
                        </div>
                    </div><!-- End sidebar recent post-->
                    @else
                    <div class="sidebar-item">
                        <div>
                            <h6>Tidak Ada FILE</h4>
                        </div>
                    </div><!-- End sidebar recent post-->
                    @endif

                </div><!-- End sidebar -->

            </div><!-- End blog sidebar -->
        </div>
    </div>
</section>
@endsection