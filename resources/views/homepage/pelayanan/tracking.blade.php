@extends('layouts.moderna')
@section('main')
    @include('homepage.parts.breadcrumbs', ['judul' => 'Tracking Pelayanan'])
    <style>
        ul.timeline {
            list-style-type: none;
            position: relative;
        }

        ul.timeline:before {
            content: ' ';
            background: #d4d9df;
            display: inline-block;
            position: absolute;
            left: 29px;
            width: 2px;
            height: calc(100% - 35px);
            z-index: 400;
        }

        ul.timeline>li {
            margin: 20px 0;
            padding-left: 20px;
        }

        ul.timeline>li:before {
            font-family: "Font Awesome 5 Free";
            content: "\f00c";
            background: var(--hijau);
            display: inline-flex;
            align-items: center;
            justify-content: center;
            text-align: center;
            position: absolute;
            border-radius: 50%;
            border: 3px solid var(--hijau);
            left: 20px;
            width: 20px;
            height: 20px;
            z-index: 400;

            font-size: 12px;
            font-weight: 600;
            color: #fff;
        }

        ul.timeline>li.reject:before {
            background: var(--red);
            border: 3px solid var(--red);
            content: "\f00d";
        }
    </style>

    <style>
        .horizontal-timeline {
            display: flex;
            margin-top: 24px;
            overflow-x: scroll;
            padding: 24px;
        }

        .horizontal-timeline-step {
            position: relative;
            width: 188px;
            min-width: 188px;
            margin-right: 22px;
            top: -8px;

            /* height: 150px; */
        }

        .horizontal-timeline-step-wrapper {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            text-align: center;
        }

        .horizontal-timeline-step-icon-wrapper {
            width: 40px;
            height: 40px;
            background-color: #f0f0f0;
            border-radius: 50%;
            box-shadow: inset 0px 4px 4px rgba(0, 0, 0, 0.25);

            display: flex;
            align-items: center;
            justify-content: center;

            position: relative;
            transform-style: preserve-3d;
        }

        .horizontal-timeline-step:not(:last-child) .horizontal-timeline-step-icon-wrapper::after,
        .horizontal-timeline-step:not(:first-of-type) .horizontal-timeline-step-icon-wrapper::before {
            content: ' ';
            background-color: #f5f5f5;
            position: absolute;
            width: 100px;
            height: 26px;
            transform: translateZ(-1px);
        }

        .horizontal-timeline-step:not(:last-child) .horizontal-timeline-step-complete .horizontal-timeline-step-icon-wrapper::after,
        .horizontal-timeline-step:not(:first-of-type) .horizontal-timeline-step-complete .horizontal-timeline-step-icon-wrapper::before {
            background-color: var(--hijau) !important;
        }

        .horizontal-timeline-step:not(:first-of-type) .horizontal-timeline-step-reject .horizontal-timeline-step-icon-wrapper::before {
            background-color: var(--red) !important;
        }

        .horizontal-timeline-step-icon-wrapper::after {
            left: 30px;
        }

        .horizontal-timeline-step-icon-wrapper::before {
            right: 30px;
        }

        .horizontal-timeline-step-icon {
            border-radius: 50%;
            width: 27px;
            height: 27px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .horizontal-timeline-step-text {
            margin-top: 20px;
            background-color: var(--hijau);
            border-radius: 26px;
            padding: 6px 10px;
        }

        .horizontal-timeline-step-complete .horizontal-timeline-step-icon {
            background-color: var(--hijau);
        }

        .horizontal-timeline-step-reject .horizontal-timeline-step-icon,
        .horizontal-timeline-step-reject .horizontal-timeline-step-text {
            background-color: var(--red);
        }

        .horizontal-timeline-step-next .horizontal-timeline-step-text,
        .horizontal-timeline-step-next .horizontal-timeline-step-icon {
            background-color: #BFBFBF;
            color: #fff;
        }

        .horizontal-timeline-step-active .horizontal-timeline-step-icon {
            background-color: #fff;
            border: 5px solid var(--hijau);
        }
    </style>

    <section
        class="blog"
        data-aos="fade-up"
        data-aos-easing="ease-in-out"
        data-aos-duration="500"
    >
        <div class="advisi-teknis-container">
            <div
                class="container-fluid"
                x-data="trackOrder()"
            >
                <div
                    class="alert alert-danger"
                    role="alert"
                    x-text="message"
                    x-show="!!message"
                >
                </div>
                @include('admin.utils.alert')
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <div class="card">
                            <div class="card-header">
                                Lacak Pengujian
                            </div>
                            <div class="card-body">
                                <form method="GET">
                                    <div class="form-group">
                                        <label for='code'>Kode Pengujian</label>
                                        <input
                                            name='code'
                                            class="form-control"
                                            x-model="code"
                                            required
                                        />
                                    </div>

                                    <div class="form-group">
                                        <div class="float-right">
                                            <button
                                                type="submit"
                                                @click="fetchTrackingCode"
                                                :disabled="isLoading"
                                                class="btn btn-success"
                                            >Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div
                        class="col-md-12"
                        x-show="trackings.length > 0"
                    >
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 h5">
                                        ORDER: <strong x-text="code"></strong>
                                    </div>
                                    <div class="col-md-4 text-center h5">
                                        <p>Progress</p>
                                        <span x-text="progress + '%'"></span>
                                        <div class="progress mt-2">
                                            <div
                                                {{-- :aria-valuenow="progress" --}}
                                                x-text="progress"
                                                class="rounded-pill progress-bar progress-bar-striped progress-bar-animated"
                                                role="progressbar"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                :style="{ width: progress + '%' }"
                                            ></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <p class="h5">Perkiraan Selesai</p>
                                        <strong x-text="estimation_date"></strong>
                                        <p x-text="days_count + ' Hari'"></p>
                                    </div>

                                    <div class="col-md-12 mt-5">
                                        <div class="horizontal-timeline">
                                            <template x-for="(tracking, index) in trackings">
                                                <div class="horizontal-timeline-step">
                                                    <div
                                                        class="horizontal-timeline-step-wrapper"
                                                        :class="{
                                                            'horizontal-timeline-step-complete': tracking.status ===
                                                                'completed',
                                                            'horizontal-timeline-step-active': tracking.status ===
                                                                'process',
                                                            'horizontal-timeline-step-next': tracking.status ===
                                                                'next',
                                                            'horizontal-timeline-step-reject': tracking.status ===
                                                                'rejected'
                                                        }"
                                                    >
                                                        <div class="horizontal-timeline-step-icon-wrapper">
                                                            <div class="horizontal-timeline-step-icon">
                                                                <i
                                                                    x-show="tracking.status === 'completed'"
                                                                    class="fas fa-check text-white"
                                                                ></i>
                                                                <i
                                                                    x-show="tracking.status === 'rejected'"
                                                                    class="fas fa-times text-white"
                                                                ></i>
                                                                <span
                                                                    x-show="tracking.status === 'next'"
                                                                    x-text="index + 1"
                                                                ></span>
                                                            </div>
                                                        </div>
                                                        <div class="horizontal-timeline-step-text">
                                                            <span class="text-white"><strong
                                                                    x-text="tracking.message"></strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </template>

                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <ul class="timeline">
                                            <template x-for="tracking in trackings">
                                                <li
                                                    :class="tracking.status === 'rejected' && 'reject'"
                                                    x-show="tracking.status !== 'next'"
                                                >
                                                    <p class="h6"><strong x-text="tracking.message"></strong></p>
                                                    <span
                                                        class="text-muted"
                                                        x-text="tracking.created_at"
                                                    ></span>
                                                </li>
                                            </template>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

<script>
    function trackOrder() {
        return {
            code: new URLSearchParams(location.search).get('code'),
            trackings: [],
            progress: 0,
            estimation_date: '',
            days_count: 0,
            isLoading: false,
            isSuccess: false,
            message: '',

            fetchTrackingCode() {
                this.isLoading = true;
                this.message = '';
                this.isSuccess = false;
                this.trackings = [];
                fetch(`/api/tracking?code=${this.code}`)
                    .then(res => {
                        if (res.ok) {
                            return res.json()
                        }
                        throw new Error(res);
                    })
                    .then(data => {
                        this.isLoading = false;
                        this.trackings = data.histories;
                        this.progress = data.progress;
                        this.estimation_date = data.estimation_date;
                        this.days_count = data.days_count;
                        this.isSuccess = true;
                    })
                    .catch(err => {
                        this.isLoading = false;
                        this.isSuccess = false;
                        this.message =
                            `Code ${this.code} tidak tersedia, silahkan periksa kembali kode tracking anda`
                    })
            }
        }
    }
</script>
