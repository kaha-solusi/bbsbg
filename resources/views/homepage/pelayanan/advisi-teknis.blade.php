@extends('layouts.moderna')
@push('style')
    <link
        rel="stylesheet"
        href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}"
    >

    <link
        rel="stylesheet"
        href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}"
    >
    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.min.css"
        integrity="sha512-jU/7UFiaW5UBGODEopEqnbIAHOI8fO6T99m7Tsmqs2gkdujByJfkCbbfPSN4Wlqlb9TGnsuC0YgUgWkRBK7B9A=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer"
    />
    <style>
        .datepicker td,
        .datepicker th {
            width: 2.5rem !important;
            height: 2.5rem !important;
            font-size: 0.85rem !important;
        }

        .datepicker-dropdown {
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1) !important;
        }

        .datepicker table tr td.active,
        .datepicker table tr td.active:hover,
        .datepicker table tr td.active.disabled,
        .datepicker table tr td.active.disabled:hover {
            background-color: var(--hijau) !important;
            border-color: var(--hijau) !important;
            background-image: var(--hijau) !important;
        }

    </style>

    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css"
    >
@endpush
@section('main')
    @include('homepage.parts.breadcrumbs', ['judul'=>'Pelayanan '.$data->nama])
    <!-- ======= Blog Section ======= -->
    <style>
        div#advisi_teknis_filter {
            display: flex;
            align-items: inherit;
        }

        select#yearFilter {
            width: 36%;
            height: 36px;
            margin-left: 15px;
        }

    </style>
    <section
        class="blog"
        data-aos="fade-up"
        data-aos-easing="ease-in-out"
        data-aos-duration="500"
    >
        <div class="advisi-teknis-container">
            <div class="container-fluid">
                @include('admin.utils.alert')
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-header">
                                Informasi Seputar Pelayanan
                            </div>
                            <div class="card-body">
                                <p class="card-text">{{ $data->text }}</p>
                            </div>
                        </div>
                        <br />
                        <div class="card">
                            <div class="card-header">
                                Dokumen Pendukung
                            </div>
                            <div class="card-body">
                                <ul class="list-group">
                                    @if ($data->file != null)
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            <a
                                                target="__blank"
                                                href="{{ asset($data->file) }}"
                                            >Informasi Umum {{ $data->nama }}</a>
                                            <span class="badge badge-primary badge-pill"><a
                                                    style="color:white;"
                                                    target="__blank"
                                                    href="{{ asset($data->file) }}"
                                                >unduh <i class="icofont-download"></i></a></span>
                                        </li>
                                    @endif
                                    @if ($data->file_sop != null)
                                        <!--<li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <a target="__blank" href="{{ asset($data->file_sop) }}">SOP {{ $data->nama }}</a>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <span class="badge badge-primary badge-pill"><a style="color:white;" target="__blank" href="{{ asset($data->file_sop) }}">unduh <i class="icofont-download"></i></a></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </li> -->
                                    @endif
                                    @if ($data->file_panduan != null)
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            <a
                                                target="__blank"
                                                href="{{ asset($data->file_panduan) }}"
                                            >FAQ {{ $data->nama }}</a>
                                            <span class="badge badge-primary badge-pill"><a
                                                    style="color:white;"
                                                    target="__blank"
                                                    href="{{ asset($data->file_panduan) }}"
                                                >unduh <i class="icofont-download"></i></a></span>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        @if ($data->foto != null)
                            <div class="portfolio-details-container">
                                <div class="owl-carousel portfolio-details-carousel">
                                    <img
                                        src="{{ asset($data->foto) }}"
                                        alt=""
                                        width="100%"
                                    >
                                    </iframe>
                                </div>
                            </div>

                        @endif
                    </div>

                    @guest
                        <div class="col-lg-12">
                            <a
                                href="{{ route('login', ['return' => url()->current()]) }}"
                                class='btn btn-primary'
                            >Daftar Advis Teknis</a>
                        </div>
                    @endguest
                </div>
            </div>
        </div>

        @can('submit.advisi-teknis')
            <div class="advisi-teknis-container advisi-teknis-container--slim">
                <div class="container-fluid">
                    <h3>
                        Pendaftaran Advis Teknis
                        <small class="text-muted">Isi data advis teknis</small>
                    </h3><br />
                    <form
                        id="advisi-teknis-form"
                        enctype="multipart/form-data"
                        action="{{ route('client.advisi-teknis.store') }}"
                        method="POST"
                    >
                        @csrf
                        <div
                            x-data="locations()"
                            x-init="initData()"
                        >
                            <div class="form-group">
                                <label for="input_date_at">Pilih Tanggal</label>
                                <input
                                    type="text"
                                    class="form-control datepicker"
                                    name="input_date_at"
                                />
                            </div>

                            <div class="form-group">
                                <label for="lingkup_konsultasi">Lingkup Konsultasi</label>
                                <select
                                    id="lingkup_konsultasi"
                                    name="lingkup_konsultasi"
                                    class="form-control"
                                    required
                                >
                                    <option selected>Struktur Atas</option>
                                    <option>Struktur Bawah</option>
                                    <option>Struktur Atas dan Struktur Bawah</option>
                                    <option>Mutu Bahan Bangunan</option>
                                </select>
                                @error('lingkup_konsultasi')
                                    <span
                                        class="invalid-feedback"
                                        role="alert"
                                    >
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="input_identitas_bangunan">Identitas Bangunan</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="input_identitas_bangunan"
                                    name="input_identitas_bangunan"
                                    required
                                    value="{{ old('input_identitas_bangunan', '') }}"
                                />
                                @error('input_identitas_bangunan')
                                    <span
                                        class="invalid-feedback"
                                        role="alert"
                                    >
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="province">Provinsi</label>
                                <select
                                    x-ref='province'
                                    class="form-control provinces select2"
                                    name="province"
                                    data-placeholder="Pilih Provinsi"
                                    required
                                >
                                    <option></option>
                                    <template x-for="province in provinces">
                                        <option
                                            :value="province.id"
                                            x-text="province.name"
                                        ></option>
                                    </template>
                                </select>
                                @error('province')
                                    <span
                                        class="invalid-feedback"
                                        role="alert"
                                    >
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="input_lokasi">Kabupaten</label>
                                <select
                                    x-ref='regency'
                                    class="form-control select2"
                                    name="regency"
                                    data-placeholder="Pilih kabupaten"
                                    x-bind:disabled="regencyDisabled"
                                    required
                                >
                                    <option></option>
                                    <template x-for="regency in regencies">
                                        <option
                                            :value="regency.id"
                                            x-text="regency.name"
                                        ></option>
                                    </template>
                                </select>
                                @error('regency')
                                    <span
                                        class="invalid-feedback"
                                        role="alert"
                                    >
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="input_lokasi">Kecamatan</label>
                                <select
                                    x-ref='district'
                                    class="form-control select2"
                                    name="district"
                                    data-placeholder="Pilih kecamatan"
                                    x-bind:disabled="districtDisabled"
                                    required
                                >
                                    <option></option>
                                    <template x-for="district in districts">
                                        <option
                                            :value="district.id"
                                            x-text="district.name"
                                        ></option>
                                    </template>
                                </select>
                                @error('district')
                                    <span
                                        class="invalid-feedback"
                                        role="alert"
                                    >
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="input_lokasi">Kelurahan</label>
                                <select
                                    x-ref='village'
                                    class="form-control select2"
                                    name="village"
                                    data-placeholder="Pilih desa"
                                    x-bind:disabled="villageDisabled"
                                    required
                                >
                                    <option></option>
                                    <template x-for="village in villages">
                                        <option
                                            :value="village.id"
                                            x-text="village.name"
                                        ></option>
                                    </template>
                                </select>
                                @error('village')
                                    <span
                                        class="invalid-feedback"
                                        role="alert"
                                    >
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="permasalahan">Permasalahan</label>
                                <textarea
                                    class="form-control"
                                    name="permasalahan"
                                    id="permasalahan"
                                    rows="5"
                                    required
                                    value="{{ old('permasalahan', '') }}"
                                ></textarea>
                                @error('permasalahan')
                                    <span
                                        class="invalid-feedback"
                                        role="alert"
                                    >
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="supportFile">Pilih file pendukung</label>
                                <div
                                    id="actions"
                                    class="row"
                                >
                                    <div class="col-lg-6">
                                        <div class="btn-group w-100">
                                            <span class="btn btn-success col-md-4 fileinput-button">
                                                <i class="fas fa-plus"></i>
                                                <span>Add files</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="table table-striped files"
                                    id="previews"
                                >
                                    <div
                                        id="template"
                                        class="row mt-2"
                                    >
                                        <div class="col-auto">
                                            <span class="preview"><img
                                                    src="data:,"
                                                    alt=""
                                                    data-dz-thumbnail
                                                /></span>
                                        </div>
                                        <div class="col d-flex align-items-center">
                                            <p class="mb-0">
                                                <span
                                                    class="lead"
                                                    data-dz-name
                                                ></span>
                                                (<span data-dz-size></span>)
                                            </p>
                                            <strong
                                                class="error text-danger"
                                                data-dz-errormessage
                                            ></strong>
                                        </div>
                                        <div class="col-auto d-flex align-items-center">
                                            <div class="btn-group">
                                                <button class="btn btn-primary start d-none">
                                                    <i class="fas fa-upload"></i>
                                                    <span>Start</span>
                                                </button>
                                                <button
                                                    data-dz-remove
                                                    class="btn btn-danger delete"
                                                >
                                                    <i class="fas fa-trash"></i>
                                                    <span>Delete</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button
                                type="submit"
                                class="btn btn-primary float-right"
                            >Daftar Advis Teknis</button>
                        </div>
                    </form>
                </div>
            </div>
        @endcan
    </section>
@endsection

@push('script')
    <script src="{{ asset('adminlte/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}">
    </script>
    <script
        src="//unpkg.com/alpinejs"
        defer
    ></script>

    @can('submit.advisi-teknis')
        <script
                src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/min/dropzone.min.js"
                integrity="sha512-oQq8uth41D+gIH/NJvSJvVB85MFk1eWpMK6glnkg6I7EdMqC1XVkW7RxLheXwmFdG03qScCM7gKS/Cx3FYt7Tg=="
                crossorigin="anonymous"
                referrerpolicy="no-referrer"
        ></script>
        <script>
            $(document).ready(function() {
                Dropzone.autoDiscover = false

                // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
                var previewNode = document.querySelector("#template")
                previewNode.id = ""
                var previewTemplate = previewNode.parentNode.innerHTML
                previewNode.parentNode.removeChild(previewNode)

                var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
                    url: "{{ route('client.advisi-teknis.store') }}", // Set the url
                    thumbnailWidth: 80,
                    thumbnailHeight: 80,
                    parallelUploads: 20,
                    previewTemplate: previewTemplate,
                    autoQueue: false, // Make sure the files aren't queued until manually added
                    previewsContainer: "#previews", // Define the container to display the previews
                    clickable: ".fileinput-button", // Define the element that should be used as click trigger to select files.
                    paramName: 'supportFile',
                    uploadMultiple: true,
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    success: function(file, response) {
                        location.reload();
                    }
                })

                myDropzone.on("sending", function(file, xhr, formData) {
                    var data = $('form#advisi-teknis-form').serializeArray();
                    $.each(data, function(key, el) {
                        formData.append(el.name, el.value);
                    });
                })

                $('form#advisi-teknis-form').on('submit', function(e) {
                    e.preventDefault();
                    myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
                });
            });
        </script>
    @endcan

    <script>
        $(document).ready(function() {
            bsCustomFileInput.init()

        });

        function locations() {
            return {
                provinces: [],
                regencies: [],
                districts: [],
                villages: [],
                regencyDisabled: true,
                districtDisabled: true,
                villageDisabled: true,
                selectedProvince: '',
                selectedRegency: '',
                selectedDistrict: '',
                selectedVillage: '',
                initData() {
                    this.getProvinces();

                    const selectProvince = $(this.$refs.province).select2({
                        theme: "bootstrap4"
                    });
                    selectProvince.on('select2:select', (event) => {
                        this.selectedProvince = event.target.value;
                    });

                    const selectRegency = $(this.$refs.regency).select2({
                        theme: "bootstrap4",
                    });
                    selectRegency.on('select2:select', (event) => {
                        this.selectedRegency = event.target.value;
                    });

                    const selectDistrict = $(this.$refs.district).select2({
                        theme: "bootstrap4"
                    });
                    selectDistrict.on('select2:select', (event) => {
                        this.selectedDistrict = event.target.value;
                    });

                    const selectVillage = $(this.$refs.village).select2({
                        theme: "bootstrap4"
                    });
                    selectVillage.on('select2:select', (event) => {
                        this.selectedVillage = event.target.value;
                    });

                    this.$watch('selectedProvince', (provinceId) => {
                        this.getRegencies(provinceId)
                            .then(() => {
                                selectRegency.val(null).trigger('change').select2('destroy').select2({
                                    theme: "bootstrap4",
                                })
                                selectDistrict.val(null).trigger('change').select2('destroy').select2({
                                    theme: "bootstrap4",
                                })
                                selectVillage.val(null).trigger('change').select2('destroy').select2({
                                    theme: "bootstrap4",
                                })
                            })
                    });

                    this.$watch('selectedRegency', (regencyId) => {
                        this.getDistricts(regencyId)
                            .then(() => {
                                selectDistrict.val(null).trigger('change').select2('destroy').select2({
                                    theme: "bootstrap4",
                                })
                                selectVillage.val(null).trigger('change').select2('destroy').select2({
                                    theme: "bootstrap4",
                                })
                            })
                    });

                    this.$watch('selectedDistrict', (districtId) => {
                        this.getVillages(districtId)
                            .then(() => {
                                selectVillage.val(null).trigger('change').select2('destroy').select2({
                                    theme: "bootstrap4",
                                })
                            })
                    });
                },
                getProvinces() {
                    fetch('{{ route('api.administrative-area.provinces') }}')
                        .then(res => res.json())
                        .then(({
                            data
                        }) => {
                            this.provinces = data;
                        })
                },
                getRegencies(provinceId) {
                    const url = '{{ route('api.administrative-area.province', '') }}' + `/${provinceId}`
                    return fetch(url)
                        .then(res => res.json())
                        .then(({
                            data
                        }) => {
                            this.regencies = data;
                            this.regencyDisabled = false
                        })
                },
                getDistricts() {
                    const url = '{{ route('api.administrative-area.province.district', ['', '']) }}' +
                        `/${this.selectedProvince}/${this.selectedRegency}`

                    return fetch(url)
                        .then(res => res.json())
                        .then(({
                            data
                        }) => {
                            this.districts = data;
                            this.districtDisabled = false
                        })
                },
                getVillages() {
                    const url = '{{ route('api.administrative-area.province.district.village', ['', '', '']) }}' +
                        `/${this.selectedProvince}/${this.selectedRegency}/${this.selectedDistrict}`

                    return fetch(url)
                        .then(res => res.json())
                        .then(({
                            data
                        }) => {
                            this.villages = data;
                            this.villageDisabled = false
                        })
                }
            }
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(function() {
            $.fn.datepicker.dates['en'] = {
                days: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
                daysShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
                daysMin: ["Mi", "Sn", "Sl", "Ra", "Ka", "Ju", "Sa"],
                months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "July", "Agustus", "September",
                    "Oktober", "November", "Desember"
                ],
                monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov",
                    "Des"
                ],
                today: "Hari Ini",
                clear: "Hapus",
                format: "mm/dd/yyyy",
                titleFormat: "MM yyyy",
                /* Leverages same syntax as 'format' */
                weekStart: 0,
            };
            $('.datepicker').datepicker({
                format: 'd-m-yyyy',
                startDate: '+3d',
                autoclose: true,
                datesDisabled : '{{$date->hari_libur . ',' . $date2->off_days}}',
                daysOfWeekDisabled: [0,6]
            });

        });
    </script>
@endpush
