@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'Tenaga Pengajar'])

<!-- ======= Team Section ======= -->
<section class="team" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
    <div class="container">
    <div class="row">

        @foreach ($pengajar as $item)
        <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
                <div class="member-img">
                <img src="{{asset($item->foto)}}" class="img-fluid img-galeri" alt="">
                <div class="social">
                    <a href="{{$item->link_twitter}}"><i class="icofont-twitter"></i></a>
                    <a href="{{$item->link_facebook}}"><i class="icofont-facebook"></i></a>
                    <a href="{{$item->link_instagram}}"><i class="icofont-instagram"></i></a>
                    <a href="{{$item->link_linkedin}}"><i class="icofont-linkedin"></i></a>
                </div>
                </div>
                <div class="member-info">
                <h4>{{$item->nama}}</h4>
                <span>{{$item->job}}</span>
                <p>{{$item->keterangan}}</p>
                </div>
            </div>
            </div>
        @endforeach

    </div>
    </div>
</section><!-- End Team Section -->
@endsection