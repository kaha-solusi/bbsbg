@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul' => "Berita"])
<!-- ======= Blog Section ======= -->
<section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
    <div class="container">

      <div class="row">

        <div class="col-lg-8 entries">

          <article class="entry entry-single">
            <div class="entry-img">
              <img src="{{asset($berita->foto)}}" alt="" class="img-fluid">
            </div>

            <h2 class="entry-title">
              <a href="#">{{$berita->judul}}</a>
            </h2>

            <div class="entry-meta">
              <ul>
                {{-- <li class="d-flex align-items-center"><i class="icofont-user"></i> <a href="#">{{$berita->penulis}}</a></li> --}}
                {{-- <li class="d-flex align-items-center"><i class="icofont-wall-clock"></i> <a href="#">{{date("j F, Y", strtotime($berita->created_at))}}</a></li> --}}
                {{-- <li class="d-flex align-items-center"><i class="icofont-comment"></i> <a href="#">12 Comments</a></li> --}}
              </ul>
            </div>

            <div class="entry-content">
              <p class="text-justify">
                {!!$berita->isi!!}
              </p>
            </div>

            <div class="entry-footer clearfix">

            </div>

          </article><!-- End blog entry -->

            {{-- <h3 class="sidebar-title">Berita Terbaru</h3>
            <div class="sidebar-item recent-posts">
              @foreach ($berita_terkini as $item)
              <div class="post-item clearfix">
                <img src="{{asset($item->foto)}}"  alt="">
                <h4><a href="{{route('berita.show', $item)}}">{{$item->judul}}</a></h4>
                <time> {{date("j F, Y", strtotime($item->created_at))}}</time>
              </div>
              @endforeach
            </div><!-- End sidebar recent posts--> --}}

          </div><!-- End sidebar -->

        </div><!-- End blog sidebar -->

      </div><!-- End row -->

    </div><!-- End container -->

  </section><!-- End Blog Section -->
@endsection