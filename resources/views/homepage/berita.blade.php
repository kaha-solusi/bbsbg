@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'Berita'])
<!-- ======= Blog Section ======= -->
<section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
    <div class="container">

    <div class="row">

        <div class="col-lg-8 entries">

        @forelse ($berita as $item)
        <article class="entry">

            <div class="entry-img">
            <img src="{{asset($item->foto)}}" alt="" class="img-fluid">
            </div>

            <h2 class="entry-title">
            <a href="{{route('detailberita', $item->id)}}">{{$item->judul}}</a>
            </h2>

            <div class="entry-meta">
            <ul>
                <li class="d-flex align-items-center"><i class="icofont-wall-clock"></i> <a href="{{route('detailberita', $item->id)}}">tanggal posting : {{date("F j, Y", strtotime($item->created_at))}}</a></li>
                <li class="d-flex align-items-center"><i class="icofont-wall-clock"></i> <a href="{{route('detailberita', $item->id)}}">tanggal akhir update : {{date("F j, Y", strtotime($item->updated_at))}}</a></li>
            </ul>
            </div>

            <div class="entry-content">
            <p>
                {{Str::limit($item->isi, 120, '...')}}
            </p>
            <div class="read-more">
                <a href="{{route('detailberita', $item->id)}}">Baca Lengkap</a>
            </div>
            </div>

        </article><!-- End blog entry -->
        @empty
            <div class="d-flex justify-content-center align-items-center">
                <h4>Tidak Ada Berita</h4>
            </div>
        @endforelse

        <div class="blog-pagination">
            {{$berita->links('layouts.parts.pagination')}}
        </div>

        </div><!-- End blog entries list -->

        <div class="col-lg-4">
        <div class="sidebar">

            <h3 class="sidebar-title">Search</h3>
            <div class="sidebar-item search-form">
                <form action="{{route('cariberita')}}">
                    <input type="text" name="cari">
                    <button type="submit"><i class="icofont-search"></i></button>
                </form>
            </div><!-- End sidebar search formn-->

            <h3 class="sidebar-title">Berita Terbaru</h3>
            <div class="sidebar-item recent-posts">
            @forelse ($berita as $item)
            <div class="post-item clearfix mb-3">
                <img src="{{asset($item->foto)}}" alt="" style="
                    width: 80px; 
                    height: 40px; 
                    object-fit: cover;
                    background-image: cover;
                    background: no-repeat center center scroll">
                <h4><a href="{{route('detailberita', $item->id)}}">{{$item->judul}}</a></h4>
                <a href="{{route('detailberita', $item->id)}}">tanggal posting : {{date("F j, Y", strtotime($item->created_at))}}
            </div>
            @empty
            <div class="post-item clearfix">
                <h4>Tidak Ada Berita</h4>
            </div>
            @endforelse
            </div><!-- End sidebar recent post-->

        </div><!-- End sidebar -->

        </div><!-- End blog sidebar -->

    </div><!-- End .row -->

    </div><!-- End .container -->

</section><!-- End Blog Section -->
@endsection