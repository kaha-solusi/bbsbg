@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'Alur Pelayanan'])
@if ($data->status_img == 0 && $data->status_pdf == 0)
<!-- ======= Features Section ======= -->
<section class="features">
    <div class="container">
        <div data-aos="fade-up" class="section-title">
        <h2>Alur Pelayanan</h2>
            <ul>
                @php
                    $i = 1
                @endphp
                @foreach ($misi as $item)
                @if ($item != "")
                    <li> {{$i++.". ".$item}} </li>
                @endif
                @endforeach
            </ul>
        </div>
    </div>
</section><!-- End Features Section -->
@elseif ($data->status_img == 1 && $data->status_pdf == 0)
<section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 entries">
                <div class="portfolio-details-container">
                    <div class="owl-carousel portfolio-details-carousel">
                        <img src="{{ asset($data->img) }}" alt=""  width="100%">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@elseif ($data->status_img == 0 && $data->status_pdf == 1)
<section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 entries">
                <div class="portfolio-details-container">
                    <div class="owl-carousel portfolio-details-carousel">
                        <iframe src="{{ asset($data->pdf) }}" frameborder="0" width="100%" height="700px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
@endsection
