@extends('layouts.moderna')
@push('style')
<style>
    * {
	padding: 0;
	margin: 0;
	font-family: "Lato", sans-serif;
	position: relative;
}

.timeline {
	padding: 40px 0px;
	width: 80%;
	margin-left: 10%;
	margin-bottom: 40px;
}

.timeline:before {
	content: "";
	position: absolute;
	top: 40px;
	left: 65px;
	width: 3px;
	height: calc(100% - 80px);
	background: #98DCAF;
}

.timeline .column {
	margin: 40px 40px 40px 120px;
}

.timeline .column .title h1 {
	font-size: 110px;
	color: rgba(152, 220, 175, 0.3);
	font-family: serif;
	letter-spacing: 3px;
}

.timeline .column .title h1:before {
	content: "";
	position: absolute;
	left: -62px;
	top: 86px;
	width: 20px;
	height: 20px;
	background: whitesmoke;
	border-radius: 50%;
	border: 3px solid #98DCAF;
}

.timeline .column .title h2 {
	margin-top: -60px;
	font-size: 33px;
}

.timeline .column .title p {
	margin-top: -60px;
    font-size: 1em;
	color: black;
	padding-left: 15px;
	border-left: 1px solid #000;
}

.timeline .column .description p {
	font-size: 15px;
	line-height: 20px;
	margin-left: 20px;
	margin-top: 10px;
	font-family: serif;
}

.timeline .column .description {
	border-left: 1px solid #98DCAF;
}

.main {
	width: 80%;
	margin-left: 10%;
	margin-top: 80px;
}

.main h1 {
	font-size: 80px;
	line-height: 60px;
}

.main p {
	font-size: 13px;
	line-height: 20px;
	font-family: serif;
	text-align: right;
}
.p-column {
	width: 100%;
	background: white;
	text-align: justify;
	padding-bottom: 30px;
	padding-top: 15px;
	margin-bottom: 20px;
}
</style>
@endpush
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'FAQ'])
<!-- ======= Features Section ======= -->
<section class="features">
<div class="container">
<div class="section-title" data-aos="fade-up">
	<h2 >FAQ</h2>
</div>
</div>
<p data-aos="fade-up">
<div class="timeline">
	{{-- @foreach (explode(";", $sejarah->isi) as $item)
	@php
		$row = explode("|", $item);
	@endphp


	@if (Str::length($row[0]) > 1)
	<div class="column" data-aos="fade-up">
		<div class="title">
			<h1> {{$row[0]}}</h1>
			<p class="text-justify">{{ (count($row) > 1) ? $row[1] : $row[0]}} </p>
		</div>
	</div>
	@else
	<p class="p-column" data-aos="fade-up">
		{{ isset($row[1]) ? $row[1] : $row[0]}}
	</p>
	@endif
	
	@endforeach --}}
	@foreach ($data as $faq)
		<div class="column" data-aos="fade-up">
			<div class="title">
				<h1>FAQ</h1>
				<p class="text-justify"><span class="text-primary">Pertanyaan:<br>{{ $faq->pertanyaan }}</span><br><br><span style="color: rgb(0, 160, 13)">Jawaban:<br>{{ $faq->jawaban }}</span></p>
			</div>
		</div>
	@endforeach
</div>
</p>
</section><!-- End Features Section -->
@endsection