@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul' => 'Tentang Maskot'])
<section class="features">
    <div class="container">
        <div class="row" data-aos="fade-up">
            <div class="col-md-5 order-1 order-md-1">
              <img src="{{asset($maskot->foto)}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5 order-2 order-md-2">
              <h3>{{$maskot->nama}}</h3>
              <p class="font-italic">
                Tentang maskot.
              </p>
              <p>
               {{$maskot->deskripsi}}
              </p>
            </div>
        </div>
    </div>
</section>
@endsection