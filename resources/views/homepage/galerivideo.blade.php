@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul' => 'Galeri Video'])

<!-- ======= Portfolio Section ======= -->
<section class="portfolio">
    <div class="container">

    <div class="row">
        <div class="col-lg-12">
        <ul id="portfolio-flters">
            <li data-filter="*" class="filter-active">Galeri Video</li>
        </ul>
        </div>
    </div>

    <div class="row portfolio-container" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
        @if (!empty($galeri))
        @foreach ($galeri as $item)
        @php
            parse_str(explode("?", $item->video)[1], $vars);
            $video_id = $vars['v'];
        @endphp
        <div class="col-lg-4 col-md-6 filter-pendidikan">
            <div class="portfolio-item why-us">
                <img src="http://img.youtube.com/vi/{{$video_id}}/0.jpg" class="img-fluid" alt="">
                <div class="portfolio-info">
                    <div class="video-box">
                        <a href="https://www.youtube.com/watch?v={{$video_id}}" class="venobox play-btn mt-4" data-vbtype="video" data-autoplay="true"></a>
                    </div>
                </div>
            </div>
            </div>
        @endforeach
        @endif
        
    </div>
    
    <div class="d-flex justify-content-center align-items-center">
        {{$galeri->links('layouts.parts.pagination')}}
    </div>

    </div>
</section><!-- End Portfolio Section -->
@endsection