@extends('layouts.moderna')
@section('main')
    @include('homepage.parts.breadcrumbs', ['judul'=>'Pelayanan '.$data->nama])
    <!-- ======= Blog Section ======= -->
    <style>
        div#advisi_teknis_filter {
            display: flex;
            align-items: inherit;
        }

        select#yearFilter {
            width: 36%;
            height: 36px;
            margin-left: 15px;
        }

    </style>
    <section
        class="blog"
        data-aos="fade-up"
        data-aos-easing="ease-in-out"
        data-aos-duration="500"
    >
        @if ($data->id == 3 || $data->id == 4)
            <div class="advisi-teknis-container">
                <div class="container-fluid">
                    @if (session()->has('success'))
                        <div
                            class="alert alert-success"
                            role="alert"
                        >
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if (session()->has('failed'))
                        <div
                            class="alert alert-danger"
                            role="alert"
                        >
                            {{ session()->get('failed') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    Informasi Seputar Pelayanan
                                </div>
                                <div class="card-body">
                                    <p class="card-text">{{ $data->text }}</p>
                                </div>
                            </div>
                            <br />
                            <div class="card">
                                <div class="card-header">
                                    Dokumen Pendukung
                                </div>
                                <div class="card-body">
                                    <ul class="list-group">
                                        @if ($data->file != null)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                <a
                                                    target="__blank"
                                                    href="{{ asset($data->file) }}"
                                                >Informasi Umum {{ $data->nama }}</a>
                                                <span class="badge badge-primary badge-pill"><a
                                                        style="color:white;"
                                                        target="__blank"
                                                        href="{{ asset($data->file) }}"
                                                    >unduh <i class="icofont-download"></i></a></span>
                                            </li>
                                        @endif
                                        @if ($data->file_sop != null)
                                            <!--<li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                                                <a target="__blank" href="{{ asset($data->file_sop) }}">SOP {{ $data->nama }}</a>
                                                                                                                <span class="badge badge-primary badge-pill"><a style="color:white;" target="__blank" href="{{ asset($data->file_sop) }}">unduh <i class="icofont-download"></i></a></span>
                                                                                                            </li> -->
                                        @endif
                                        @if ($data->file_panduan != null)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                <a
                                                    target="__blank"
                                                    href="{{ asset($data->file_panduan) }}"
                                                >FAQ {{ $data->nama }}</a>
                                                <span class="badge badge-primary badge-pill"><a
                                                        style="color:white;"
                                                        target="__blank"
                                                        href="{{ asset($data->file_panduan) }}"
                                                    >unduh <i class="icofont-download"></i></a></span>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            @if ($data->foto != null)
                                <div class="portfolio-details-container">
                                    <div class="owl-carousel portfolio-details-carousel">
                                        <img
                                            src="{{ asset($data->foto) }}"
                                            alt=""
                                            width="100%"
                                        >
                                        </iframe>
                                    </div>
                                </div>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @if ($data->id == 3)
                <div class="advisi-teknis-container advisi-teknis-container--slim">
                    <div class="container-fluid">
                        <h3>
                            Pendaftaran Advis Teknis
                            <small class="text-muted">Isi data advis teknis</small>
                        </h3><br />
                        <form
                            id="advisi-teknis-form"
                            enctype="multipart/form-data"
                            action="{{ route('client.advisi-teknis.store') }}"
                            method="POST"
                        >
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label for="input_date_at">Pilih Tanggal</label>
                                    <input
                                        min="{{ date('Y-m-d', strtotime(date('Y-m-d') . ' + 3 days')) }}"
                                        type="date"
                                        class="form-control"
                                        id="input_date_at"
                                        name="input_date_at"
                                    />
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label for="lingkup_konsultasi">Lingkup Konsultasi</label>
                                    <select
                                        id="lingkup_konsultasi"
                                        name="lingkup_konsultasi"
                                        class="form-control"
                                    >
                                        <option selected>Struktur Atas</option>
                                        <option>Struktur Bawah</option>
                                        <option>Struktur Atas dan Struktur Bawah</option>
                                        <option>Mutu Konstruksi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="input_identitas_bangunan">Identitas Bangunan</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="input_identitas_bangunan"
                                        name="input_identitas_bangunan"
                                    />
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="input_lokasi">Lokasi</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="input_lokasi"
                                        name="input_lokasi"
                                    />
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="permasalahan">Permasalahan</label>
                                    <textarea
                                        class="form-control"
                                        name="permasalahan"
                                        id="permasalahan"
                                        rows="5"
                                    ></textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <div class="file_pendukung">
                                        <input
                                            type="file"
                                            style="margin-top:31px;"
                                            class="file_pendukung-input"
                                            id="file_pendukung-input"
                                            name="file_pendukung"
                                        >
                                        <label
                                            class="custom-file-label"
                                            style="margin-top:31px;"
                                            for="file_pendukung-input"
                                        >Pilih File Pendukung</label>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <button
                                        type="submit"
                                        class="btn btn-primary float-right"
                                    >Daftar Advisi Teknis</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
            @if ($data->id == 4)
                <div class="advisi-teknis-container">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="portfolio-description mt-5">
                                    <div class="year-filter">
                                        <select
                                            id="yearFilter"
                                            class="form-control"
                                        >
                                            @foreach ($years_bimtek as $year)
                                                <option value="{{ $year->tahun_pelaksanaan }}">
                                                    {{ $year->tahun_pelaksanaan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="table-responsive">
                                        <table
                                            id="advisi_teknis"
                                            class="display cell-border compact stripe"
                                            style="width:100%"
                                        >
                                            <thead>
                                                <tr>
                                                    <th
                                                        rowspan="2"
                                                        width="350px;"
                                                    >Nama Bimbingan Teknis</th>
                                                    <th rowspan="2">Jenis Bimtek</th>
                                                    <th colspan="12">Pelaksanaan</th>
                                                    <th rowspan="2">Tanggal</th>
                                                    <th rowspan="2">Tahun</th>
                                                    <th rowspan="2">Waktu</th>
                                                    <th rowspan="2">Deskripsi</th>
                                                    <th
                                                        rowspan="2"
                                                        width="200px;"
                                                    >Status Pelaksanaan</th>
                                                    <th rowspan="2">Aksi</th>
                                                </tr>
                                                <tr>
                                                    <th class="month no-sort">Jan</th>
                                                    <th class="month no-sort">Feb</th>
                                                    <th class="month no-sort">Mar</th>
                                                    <th class="month no-sort">Apr</th>
                                                    <th class="month no-sort">Mei</th>
                                                    <th class="month no-sort">Jun</th>
                                                    <th class="month no-sort">Jul</th>
                                                    <th class="month no-sort">Ags</th>
                                                    <th class="month no-sort">Sep</th>
                                                    <th class="month no-sort">Okt</th>
                                                    <th class="month no-sort">Nov</th>
                                                    <th class="month no-sort">Des</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($bimteks as $bimtek)
                                                    <?php
                                                    $today = date('Y-m-d');
                                                    $status_pelaksanaan = "<span class='alert alert-primary' style='display:block;margin-bottom:0;'>Terjadwal</span>";

                                                    if (isset($bimtek->tanggal_pelaksanaan)) {
                                                        if ($bimtek->tanggal_pelaksanaan < $today) {
                                                            $status_pelaksanaan = "<span class='alert alert-secondary' style='display:block;margin-bottom:0;'>Sudah Terlaksana</span>";
                                                        } elseif ($bimtek->tanggal_pelaksanaan == $today) {
                                                            $timePlushThreshold = date("Y-m-d H:i:s", strtotime('+2 hours', strtotime($bimtek->waktu_pelaksanaan)));
                                                            if (time() > strtotime($timePlushThreshold)) {
                                                                $status_pelaksanaan = "<span class='alert alert-secondary' style='display:block;margin-bottom:0;'>Sudah Terlaksana</span>";
                                                            } else {
                                                                $status_pelaksanaan = "<span class='alert alert-success' style='display:block;margin-bottom:0;'>Pendaftaran Peserta</span>";
                                                            }
                                                        } else {
                                                            $status_pelaksanaan = "<span class='alert alert-success' style='display:block;margin-bottom:0;'>Pendaftaran Peserta</span>";
                                                        }
                                                    }

                                                    ?>
                                                    {{-- @dump($bimtek->tanggal_pelaksanaan, $today) --}}

                                                    <tr>
                                                        <td
                                                            class="nama-bimtek"
                                                            style="text-align:left;cursor:pointer;"
                                                        >{{ $bimtek->nama_bimtek }}</td>
                                                        <td>{{ $bimtek->jenis_bimtek }}</td>
                                                        <td <?php if ('01' == $bimtek->bulan_pelaksanaan) {
    echo 'class="selected"';
} ?>></td>
                                                        <td <?php if ('02' == $bimtek->bulan_pelaksanaan) {
    echo 'class="selected"';
} ?>></td>
                                                        <td <?php if ('03' == $bimtek->bulan_pelaksanaan) {
    echo 'class="selected"';
} ?>></td>
                                                        <td <?php if ('04' == $bimtek->bulan_pelaksanaan) {
    echo 'class="selected"';
} ?>></td>
                                                        <td <?php if ('05' == $bimtek->bulan_pelaksanaan) {
    echo 'class="selected"';
} ?>></td>
                                                        <td <?php if ('06' == $bimtek->bulan_pelaksanaan) {
    echo 'class="selected"';
} ?>></td>
                                                        <td <?php if ('07' == $bimtek->bulan_pelaksanaan) {
    echo 'class="selected"';
} ?>></td>
                                                        <td <?php if ('08' == $bimtek->bulan_pelaksanaan) {
    echo 'class="selected"';
} ?>></td>
                                                        <td <?php if ('09' == $bimtek->bulan_pelaksanaan) {
    echo 'class="selected"';
} ?>></td>
                                                        <td <?php if ('10' == $bimtek->bulan_pelaksanaan) {
    echo 'class="selected"';
} ?>></td>
                                                        <td <?php if ('11' == $bimtek->bulan_pelaksanaan) {
    echo 'class="selected"';
} ?>></td>
                                                        <td <?php if ('12' == $bimtek->bulan_pelaksanaan) {
    echo 'class="selected"';
} ?>></td>
                                                        <td>
                                                            @if (is_null($bimtek->tanggal_pelaksanaan))
                                                                <span class="warning">Jadwal Menyusul</span>
                                                            @else
                                                                <?php echo date('j F Y', strtotime($bimtek->tanggal_pelaksanaan)); ?>
                                                        </td>
                                                @endif
                                                </td>
                                                <td>{{ $bimtek->tahun_pelaksanaan }}</td>
                                                <?php
                                                if( !empty($bimtek->tanggal_pelaksanaan) ) { ?>
                                                <td>{{ $bimtek->waktu_pelaksanaan }} WIB
                                                </td>
                                                <?php } else { ?>
                                                <td><a
                                                        href="#"
                                                        class="btn mx-1 d-inline btn-sm"
                                                    >-</a>
                                                </td>
                                                <?php } ?>
                                                <td>{{ $bimtek->deskripsi_bimtek }}</td>
                                                <td><?php echo $status_pelaksanaan; ?></td>
                                                <?php
                                                $timePlusThreshold = date("Y-m-d H:i:s", strtotime('+2 hours', strtotime($bimtek->tanggal_pelaksanaan.' '.$bimtek->waktu_pelaksanaan)));

                                                if( $bimtek->status_pelaksanaan == 'aktif' && strtotime(date("Y-m-d H:i:s")) < strtotime($timePlusThreshold) ) { ?>
                                                <td>
                                                    <?php // {{route('bimtek.register', ['id' => $bimtek->id])}}
                                                    ?>
                                                    @auth
                                                        <a
                                                            href="javascript:void(0);"
                                                            class="aksi bimtek_daftar_modal"
                                                            data-bimtekid="{{ $bimtek->id }}"
                                                            data-bimtek="{{ $bimtek->nama_bimtek }}"
                                                            data-tanggal="<?php echo date('j F Y', strtotime($bimtek->tanggal_pelaksanaan)) . ', ' . $bimtek->waktu_pelaksanaan; ?> WIB"
                                                            data-deskripsi="{{ $bimtek->deskripsi_bimtek }}"
                                                            @if (Auth::user()->hasRole(['super-admin', 'admin'])) hidden @endif
                                                        >Daftar</a>
                                                    @else
                                                        <a
                                                            href="{{ route('login', ['return' => url()->current()]) }}"
                                                            class="aksi"
                                                        >Daftar</a>
                                                    @endauth
                                                </td>
                                                <?php } else { ?>
                                                <td><a
                                                        href="#"
                                                        class="btn mx-1 d-inline btn-sm"
                                                    >-</a>
                                                </td>
                                                <?php } ?>
                                                </tr>
            @endforeach
            </tbody>
            </table>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
        @endif
    @else
        <div class="container">
            <div class="row">

                <div class="col-lg-9 entries">
                    @if ($data->foto != null)
                        <div class="portfolio-details-container">
                            <div class="owl-carousel portfolio-details-carousel">
                                <img
                                    src="{{ asset($data->foto) }}"
                                    alt=""
                                    width="100%"
                                >
                                </iframe>
                            </div>
                        </div>
                    @endif

                    @if ($data->text != null || $data->text != '')
                        <div class="portfolio-description mt-5">
                            <h2>Informasi Seputar Pelayanan</h2>
                            <p>
                                {{ $data->text }}
                            </p>
                        </div>
                    @endif

{{--                    <a--}}
{{--                        href="http://bit.ly/bimtekbg"--}}
{{--                        target="__blank"--}}
{{--                        class="btn d-block btn-outline-primary"--}}
{{--                    >Daftar</a>--}}
                </div><!-- End sidebar -->

                <div class="col-lg-3">
                            <div class="sidebar">

                                <h3 class="sidebar-title">File PDF</h3>
                                @if ($data->file == null && $data->file_sop == null && $data->file_panduan == null && $data->file_biaya == null)
                                    <div class="sidebar-item">
                                        <div>
                                            <h6><a href="#">Tidak ada file</a></h4>
                                        </div>
                                    </div><!-- End sidebar recent post-->
                                @endif
                                @if ($data->file != null)
                                    <div class="sidebar-item">
                                        <div>
                                            <h6><a
                                                    target="__blank"
                                                    href="{{ asset($data->file) }}"
                                                >LINK
                                                    File
                                                    {{ $data->nama }}</a></h4>
                                        </div>
                                    </div><!-- End sidebar recent post-->
                                @endif
                                @if ($data->file_sop != null)
                                    <div class="sidebar-item">
                                        <div>
                                            <h6><a
                                                    target="__blank"
                                                    href="{{ asset($data->file_sop) }}"
                                                >LINK
                                                    SOP {{ $data->nama }}</a>
                                                </h4>
                                        </div>
                                    </div><!-- End sidebar recent post-->
                                @endif
                                @if ($data->file_panduan != null)
                                    <div class="sidebar-item">
                                        <div>
                                            <h6><a
                                                    target="__blank"
                                                    href="{{ asset($data->file_panduan) }}"
                                                >LINK
                                                    Panduan {{ $data->nama }}</a>
                                                </h4>
                                        </div>
                                    </div><!-- End sidebar recent post-->
                                @endif

                                <a
                                    href="{{ route('pengujian.registrasi') }}"
                                    class="btn d-block btn-outline-primary"
                                >Daftar</a>
                            </div><!-- End sidebar -->

                        </div><!-- End blog sidebar -->

            </div><!-- End blog entries list -->

        </div><!-- End .row -->

        </div><!-- End .container -->

        {{-- Modal --}}
        <div
            class="modal fade"
            id="urlModal"
            tabindex="-1"
            aria-labelledby="urlModalLabel"
            aria-hidden="true"
        >
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5
                            class="modal-title"
                            id="urlModalLabel"
                        >Keterangan <span id="nama-gambar"></span></h5>
                        <button
                            type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close"
                        >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p id="website"></p>
                    </div>
                    <div class="modal-footer">
                        <button
                            type="button"
                            class="btn btn-secondary"
                            data-dismiss="modal"
                        >Close</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}

        @endif
    @endsection
    @push('script')
        <script type="text/javascript">
            $(document).ready(function() {
                console.log('ready');
            });
        </script>
    @endpush
