@php
    $i = 1;
@endphp
@foreach ($pelayanan as $item)
<!-- Modal -->
<div class="modal fade" id="pdf{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$item->nama}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <embed src="{{asset($item->file)}}" frameborder="0" width="100%" height="1000">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
@endforeach

<!-- Modal -->
<div class="modal fade" style="z-index:1100;" id="bimtekModal" tabindex="-1" aria-labelledby="bimtekModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="bimtekModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="bimtekModalBody">
            ...
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="bimtekDaftarModal" tabindex="-1" aria-labelledby="bimtekDaftarModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="bimtek_title">Pendaftaran Bimbingan Teknis</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <h3 id="nama_bimtek"></h3>
            <p id="deskripsi_bimtek"></p><br />
            <span id="waktu_bimtek"></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <form method="POST" action="{{ route('pesertabimtek.store') }}">
            @csrf
            <input type="hidden" name="bimtek_id" id="bimtek_id" value="" class="form-control corner-edge" required="">
            <button type="submit" class="btn btn-info">Daftar <i class="icofont-check"></i></button>
            </form>
        </div>
        </div>
    </div>
</div>


</section><!-- End Blog Section -->