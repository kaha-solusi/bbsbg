<section class="breadcrumbs">
    <div class="container">

        <div class="d-flex justify-content-between align-items-center">
            <h2 class="mb-0">{{ $judul }}</h2>

            <ol>
                <li><a href="/">Home</a></li>
                <li>{{ $judul }}</li>
            </ol>
        </div>

    </div>
</section>
