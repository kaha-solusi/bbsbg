@extends('layouts.moderna')
@section('style')
    <!-- DataTables -->
<link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="/adminlte/plugins/fontawesome-free/css/all.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="/adminlte/dist/css/adminlte.min.css">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('main')
@include('homepage.parts.breadcrumbs', ['judul' => 'Jadwal Bimbingan Teknis'])
<section>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Jadwal Bimbingan Teknis</h3>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>No.</th>
                <th>Nama Kegiatan</th>
                <th>Waktu Pelaksanaan</th>
                <th>Tempat Pelaksanaan</th>
                <th>Lab</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            @php
                $i = 1
            @endphp
            @foreach ($jadwal as $item)
            <tr>
                <td>{{$i++}}</td>
                <td>{{$item->nama_kegiatan}}</td>
                <td>{{$item->waktu_pelaksanaan}}</td>
                <td>{{$item->tempat_pelaksanaan}}</td>
                <td>{{$item->lab->nama}}</td>
                <td>{{$item->status}}</td>
            </tr>
            @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>
</section>
@endsection
@push('script')
<!-- AdminLTE App -->
<script src="/adminlte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/adminlte/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="/adminlte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/adminlte/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "responsive": true,
    });
  });
</script>
@endpush