@extends('layouts.moderna')
@section('main')
@include('homepage.parts.breadcrumbs', ['judul'=>'Umpan Balik'])
<!-- ======= Features Section ======= -->
<section class="features">
<div class="container">

    <div class="section-title" data-aos="fade-up">
        <h2 >Umpan Balik</h2>
    </div>

    <div data-aos="fade-up" class="row">
    	<div class="col-md-12">
    		<form action="{{ route('umpan-balik.store') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Nama</label>
                                <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{ old('nama') }}">
                                @error('nama')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label>Email</label>
                                <input type="text" min="0" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Isi Umpan Balik</label>
                                <textarea name="keterangan" class="form-control @error('keterangan') is-invalid @enderror" rows="5"></textarea>
                                @error('keterangan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success btn-sm" style="width: 100%;">Submit</button>
                    </div>
    				@if (session()->has('success'))
    	                <div class="col-md-12">
    	                    <p>{{ session()->get('success') }}</p>
    	                </div>
    				@endif
                </div>
            </form>
    	</div>
    </div>

</div>
</section><!-- End Features Section -->
@endsection