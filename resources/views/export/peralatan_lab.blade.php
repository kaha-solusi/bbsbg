<table>
    <thead>
    <tr>
        <tr>no</tr>
        <tr>lab</tr>
        <tr>nama</tr>
        <tr>merek</tr>
        <tr>type</tr>
        <tr>seri</tr>
        <tr>spesifikasi</tr>
        <tr>no_bmn</tr>
        <tr>fungsi_alat</tr>
        <tr>tahun_pengadaan</tr>
        <tr>jumlah</tr>
        <tr>satuan</tr>
        <tr>keterangan_kapasitas</tr>
        <tr>kondisi</tr>
        <tr>foto_alat</tr>
        <tr>tanggal_terakhir_kalibrasi</tr>
        <tr>lembaga_pengujian_kalibrasi</tr>
        <tr>penanggung_jawab</tr>
    </tr>
    </thead>
    <tbody>
    @php
        $i = 1;
    @endphp
    @foreach($peralatans as $peralatan)
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $peralatan->lab->nama }}</td>
            <td>{{ $peralatan->nama }}</td>
            <td>{{ $peralatan->merek }}</td>
            <td>{{ $peralatan->type }}</td>
            <td>{{ $peralatan->seri }}</td>
            <td>{{ $peralatan->spesifikasi }}</td>
            <td>{{ $peralatan->no_bmn }}</td>
            <td>{{ $peralatan->fungsi_alat }}</td>
            <td>{{ $peralatan->tahun_pengadaan }}</td>
            <td>{{ $peralatan->jumlah }}</td>
            <td>{{ $peralatan->satuan }}</td>
            <td>{{ $peralatan->keterangan_kapasitas }}</td>
            <td>{{ $peralatan->kondisi }}</td>
            <td></td>
            <td>{{ $peralatan->tanggal_terakhir_kalibrasi }}</td>
            <td>{{ $peralatan->lembaga_pengujian_kalibrasi }}</td>
            <td>{{ $peralatan->penanggung_jawab }}</td>
        </tr>
        @php
            $i++;
        @endphp
    @endforeach
    </tbody>
</table>