const state = {
    data: []
};

const mutations = {
    SET_DATA(state, payload) {
        state.data = payload;
    }
};

const getters = {};

const actions = {
    async getData({ commit }) {
        return new Promise((resolve, reject) => {
            axios
                .get("/web-api/dashboard")
                .then(response => {
                    commit("SET_DATA", response.data.data);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions
};
