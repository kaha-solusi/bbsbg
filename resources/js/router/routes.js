import Dashboard from "../views/Dashboard.vue";
import Lab from "../views/Lab.vue";

const routes = [
    {
        path: "/back-end",
        name: "dashboard",
        meta: { layout: "SideBar", level: ["1", "2", "3"] },
        component: Dashboard
    },
    {
        path: "/back-end/lab",
        name: "lab",
        meta: { layout: "SideBar", level: ["1", "2", "3"] },
        component: Dashboard
    }
];

export default routes;
