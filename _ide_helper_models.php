<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\AdvisiTeknis
 *
 * @property int $id
 * @property string $test_date
 * @property string $scope
 * @property string $building_identity
 * @property string $problem
 * @property string $support_file
 * @property string $status
 * @property int|null $client_id
 * @property string|null $link
 * @property string|null $letter_of_introduction
 * @property string|null $news_report
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $time
 * @property int|null $province_id
 * @property int|null $regency_id
 * @property int|null $district_id
 * @property int|null $village_id
 * @property string|null $confirmation_date
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $approvals
 * @property-read int|null $approvals_count
 * @property-read \App\Models\Client|null $client
 * @property-read \App\Models\District|null $district
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\File[] $files
 * @property-read int|null $files_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kuisioner[] $kuisioners
 * @property-read int|null $kuisioners_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pegawai[] $members
 * @property-read int|null $members_count
 * @property-read \App\Models\Province|null $province
 * @property-read \App\Models\Regency|null $regency
 * @property-read \App\Models\Village|null $village
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereBuildingIdentity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereConfirmationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereLetterOfIntroduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereNewsReport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereProblem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereRegencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereScope($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereSupportFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereTestDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknis whereVillageId($value)
 */
	class AdvisiTeknis extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AdvisiTeknisApproval
 *
 * @property int $id
 * @property int|null $advisi_teknis_id
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $status
 * @property string|null $reason
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\User|null $creator
 * @property-read \App\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval whereAdvisiTeknisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisApproval whereUpdatedBy($value)
 */
	class AdvisiTeknisApproval extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AdvisiTeknisFile
 *
 * @property int $id
 * @property int $advisi_teknis_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $file_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisFile whereAdvisiTeknisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisFile whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisFile whereUpdatedAt($value)
 */
	class AdvisiTeknisFile extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AdvisiTeknisMember
 *
 * @property int $id
 * @property int $advisi_teknis_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $pegawai_id
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisMember newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisMember newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisMember query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisMember whereAdvisiTeknisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisMember whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisMember whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisMember wherePegawaiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvisiTeknisMember whereUpdatedAt($value)
 */
	class AdvisiTeknisMember extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Akun
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Akun newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Akun newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Akun query()
 * @method static \Illuminate\Database\Eloquent\Builder|Akun whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Akun whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Akun whereUpdatedAt($value)
 */
	class Akun extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AlurPelayanan
 *
 * @property int $id
 * @property string|null $text
 * @property string|null $img
 * @property string|null $pdf
 * @property int $status_img
 * @property int $status_pdf
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AlurPelayanan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AlurPelayanan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AlurPelayanan query()
 * @method static \Illuminate\Database\Eloquent\Builder|AlurPelayanan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlurPelayanan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlurPelayanan whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlurPelayanan wherePdf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlurPelayanan whereStatusImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlurPelayanan whereStatusPdf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlurPelayanan whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlurPelayanan whereUpdatedAt($value)
 */
	class AlurPelayanan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Berita
 *
 * @property int $id
 * @property string $foto
 * @property string $judul
 * @property string $isi
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Berita newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Berita newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Berita query()
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereIsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereUpdatedAt($value)
 */
	class Berita extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\BeritaUI
 *
 * @property int $id
 * @property int|null $jumlah_berita
 * @property int|null $berita_1_id
 * @property int|null $berita_2_id
 * @property int|null $berita_3_id
 * @property int|null $berita_4_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Berita|null $berita_1
 * @property-read \App\Models\Berita|null $berita_2
 * @property-read \App\Models\Berita|null $berita_3
 * @property-read \App\Models\Berita|null $berita_4
 * @method static \Illuminate\Database\Eloquent\Builder|BeritaUI newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BeritaUI newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BeritaUI query()
 * @method static \Illuminate\Database\Eloquent\Builder|BeritaUI whereBerita1Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BeritaUI whereBerita2Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BeritaUI whereBerita3Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BeritaUI whereBerita4Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BeritaUI whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BeritaUI whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BeritaUI whereJumlahBerita($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BeritaUI whereUpdatedAt($value)
 */
	class BeritaUI extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Bimtek
 *
 * @property int $id
 * @property string $nama_bimtek
 * @property string|null $jenis_bimtek
 * @property string|null $deskripsi_bimtek
 * @property string|null $tahun_pelaksanaan
 * @property string|null $bulan_pelaksanaan
 * @property string|null $tanggal_pelaksanaan
 * @property string $waktu_pelaksanaan
 * @property string $status_pelaksanaan
 * @property string $syarat_peserta
 * @property string|null $link_zoom
 * @property string|null $link_certificate
 * @property string|null $bimtek_subjek_email
 * @property string|null $bimtek_konten_email
 * @property string|null $bimtek_background
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $meeting_id
 * @property string|null $passcode
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $peserta
 * @property-read int|null $peserta_count
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek query()
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereBimtekBackground($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereBimtekKontenEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereBimtekSubjekEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereBulanPelaksanaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereDeskripsiBimtek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereJenisBimtek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereLinkCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereLinkZoom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereMeetingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereNamaBimtek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek wherePasscode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereStatusPelaksanaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereSyaratPeserta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereTahunPelaksanaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereTanggalPelaksanaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bimtek whereWaktuPelaksanaan($value)
 */
	class Bimtek extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Client
 *
 * @property int $id
 * @property int|null $unit_kerja_id
 * @property int|null $instansi_id
 * @property string $name
 * @property string|null $nomor
 * @property string|null $website
 * @property string $phone_number
 * @property string|null $logo
 * @property string $alamat
 * @property string|null $nama_perusahaan
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $group 1: Kementerian PUPR, 2: Pemerintahan Lainnya, 3: Non-Pemerintah
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Instansi|null $instansi
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pekerjaan[] $pekerjaan
 * @property-read int|null $pekerjaan_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujian[] $permintaan
 * @property-read int|null $permintaan_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UmpanBalik[] $umpan_balik
 * @property-read int|null $umpan_balik_count
 * @property-read \App\Models\Instansi|null $unit_kerja
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereAlamat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereInstansiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereNamaPerusahaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereNomor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereUnitKerjaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereWebsite($value)
 */
	class Client extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\DaftarDiklatPegawai
 *
 * @property int $id
 * @property int|null $pegawai_id
 * @property string $nama
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|DaftarDiklatPegawai newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DaftarDiklatPegawai newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DaftarDiklatPegawai query()
 * @method static \Illuminate\Database\Eloquent\Builder|DaftarDiklatPegawai whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DaftarDiklatPegawai whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DaftarDiklatPegawai whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DaftarDiklatPegawai wherePegawaiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DaftarDiklatPegawai whereUpdatedAt($value)
 */
	class DaftarDiklatPegawai extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\District
 *
 * @property int $id
 * @property int|null $regency_id
 * @property string $name
 * @property-read \App\Models\Regency|null $regency
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Village[] $villages
 * @property-read int|null $villages_count
 * @method static \Illuminate\Database\Eloquent\Builder|District newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|District newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|District query()
 * @method static \Illuminate\Database\Eloquent\Builder|District whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|District whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|District whereRegencyId($value)
 */
	class District extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Faq
 *
 * @property int $id
 * @property string $pertanyaan
 * @property string $jawaban
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Faq newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Faq newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Faq query()
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereJawaban($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq wherePertanyaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Faq whereUpdatedAt($value)
 */
	class Faq extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\File
 *
 * @property int $id
 * @property string $filename
 * @property string $filetype
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|File newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|File newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|File query()
 * @method static \Illuminate\Database\Eloquent\Builder|File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereFiletype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereUpdatedAt($value)
 */
	class File extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\FrontKepuasanPelanggan
 *
 * @property int $id
 * @property string|null $text
 * @property string|null $url_img
 * @property string|null $url_pdf
 * @property int $status_img
 * @property int $status_pdf
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|FrontKepuasanPelanggan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FrontKepuasanPelanggan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FrontKepuasanPelanggan query()
 * @method static \Illuminate\Database\Eloquent\Builder|FrontKepuasanPelanggan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrontKepuasanPelanggan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrontKepuasanPelanggan whereStatusImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrontKepuasanPelanggan whereStatusPdf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrontKepuasanPelanggan whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrontKepuasanPelanggan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrontKepuasanPelanggan whereUrlImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrontKepuasanPelanggan whereUrlPdf($value)
 */
	class FrontKepuasanPelanggan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\FunctionalPosition
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pegawai[] $pegawai
 * @property-read int|null $pegawai_count
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionalPosition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionalPosition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionalPosition query()
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionalPosition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionalPosition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionalPosition whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionalPosition whereUpdatedAt($value)
 */
	class FunctionalPosition extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Galeri
 *
 * @property int $id
 * @property string|null $foto
 * @property string|null $video
 * @property string|null $kategori
 * @property string $judul
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Galeri newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Galeri newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Galeri query()
 * @method static \Illuminate\Database\Eloquent\Builder|Galeri whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Galeri whereFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Galeri whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Galeri whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Galeri whereKategori($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Galeri whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Galeri whereVideo($value)
 */
	class Galeri extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\GaleriVideo
 *
 * @property int $id
 * @property string|null $video
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|GaleriVideo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GaleriVideo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GaleriVideo query()
 * @method static \Illuminate\Database\Eloquent\Builder|GaleriVideo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GaleriVideo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GaleriVideo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GaleriVideo whereVideo($value)
 */
	class GaleriVideo extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Golongan
 *
 * @property int $id
 * @property string $nama
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pegawai|null $pegawai
 * @method static \Illuminate\Database\Eloquent\Builder|Golongan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Golongan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Golongan query()
 * @method static \Illuminate\Database\Eloquent\Builder|Golongan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Golongan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Golongan whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Golongan whereUpdatedAt($value)
 */
	class Golongan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\IndeksKepuasan
 *
 * @property int $id
 * @property string $tahun
 * @property int $jumlah_responden
 * @property float $nilai_ikm
 * @property float $indexs
 * @property string $nilai_mutu
 * @property string $predikat
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|IndeksKepuasan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IndeksKepuasan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IndeksKepuasan query()
 * @method static \Illuminate\Database\Eloquent\Builder|IndeksKepuasan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IndeksKepuasan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IndeksKepuasan whereIndexs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IndeksKepuasan whereJumlahResponden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IndeksKepuasan whereNilaiIkm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IndeksKepuasan whereNilaiMutu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IndeksKepuasan wherePredikat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IndeksKepuasan whereTahun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IndeksKepuasan whereUpdatedAt($value)
 */
	class IndeksKepuasan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\InformasiPublik
 *
 * @property int $id
 * @property string $nama
 * @property string $link
 * @property string|null $foto
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|InformasiPublik newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InformasiPublik newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InformasiPublik query()
 * @method static \Illuminate\Database\Eloquent\Builder|InformasiPublik whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InformasiPublik whereFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InformasiPublik whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InformasiPublik whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InformasiPublik whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InformasiPublik whereUpdatedAt($value)
 */
	class InformasiPublik extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Instansi
 *
 * @property int $id
 * @property string $nama_instansi
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property-read \Kalnoy\Nestedset\Collection|Instansi[] $children
 * @property-read int|null $children_count
 * @property-read \App\User|null $creator
 * @property-read Instansi|null $parent
 * @property-read \App\User|null $updater
 * @method static \Kalnoy\Nestedset\Collection|static[] all($columns = ['*'])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi ancestorsAndSelf($id, array $columns = [])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi ancestorsOf($id, array $columns = [])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi applyNestedSetScope(?string $table = null)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi countErrors()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi createdBy($userId)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi d()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi defaultOrder(string $dir = 'asc')
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi descendantsAndSelf($id, array $columns = [])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi descendantsOf($id, array $columns = [], $andSelf = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi fixSubtree($root)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi fixTree($root = null)
 * @method static \Kalnoy\Nestedset\Collection|static[] get($columns = ['*'])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi getNodeData($id, $required = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi getPlainNodeData($id, $required = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi getTotalErrors()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi hasChildren()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi hasParent()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi isBroken()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi leaves(array $columns = [])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi makeGap(int $cut, int $height)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi moveNode($key, $position)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi newModelQuery()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi newQuery()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi orWhereAncestorOf(bool $id, bool $andSelf = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi orWhereDescendantOf($id)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi orWhereNodeBetween($values)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi orWhereNotDescendantOf($id)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi query()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi rebuildSubtree($root, array $data, $delete = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi rebuildTree(array $data, $delete = false, $root = null)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi reversed()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi root(array $columns = [])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi updatedBy($userId)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereAncestorOf($id, $andSelf = false, $boolean = 'and')
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereAncestorOrSelf($id)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereCreatedAt($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereCreatedBy($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereDeletedBy($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereDescendantOf($id, $boolean = 'and', $not = false, $andSelf = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereDescendantOrSelf(string $id, string $boolean = 'and', string $not = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereId($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereIsAfter($id, $boolean = 'and')
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereIsBefore($id, $boolean = 'and')
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereIsLeaf()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereIsRoot()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereLft($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereNamaInstansi($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereNodeBetween($values, $boolean = 'and', $not = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereNotDescendantOf($id)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereParentId($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereRgt($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereUpdatedAt($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi whereUpdatedBy($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi withDepth(string $as = 'depth')
 * @method static \Kalnoy\Nestedset\QueryBuilder|Instansi withoutRoot()
 */
	class Instansi extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Invoice
 *
 * @property int $id
 * @property string $order_code
 * @property string $code
 * @property int $permintaan_pengujian_id
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice query()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereOrderCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice wherePermintaanPengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereUpdatedBy($value)
 */
	class Invoice extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Jabatan
 *
 * @property int $id
 * @property string $nama
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pegawai|null $pegawai
 * @method static \Illuminate\Database\Eloquent\Builder|Jabatan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Jabatan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Jabatan query()
 * @method static \Illuminate\Database\Eloquent\Builder|Jabatan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jabatan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jabatan whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jabatan whereUpdatedAt($value)
 */
	class Jabatan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\JadwalKegiatanPersonil
 *
 * @property int $id
 * @property int|null $pegawai_id
 * @property string $tanggal_mulai
 * @property string $penugasan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $tanggal_selesai
 * @property-read \App\Models\Pegawai|null $pegawai
 * @method static \Illuminate\Database\Eloquent\Builder|JadwalKegiatanPersonil newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JadwalKegiatanPersonil newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JadwalKegiatanPersonil query()
 * @method static \Illuminate\Database\Eloquent\Builder|JadwalKegiatanPersonil whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JadwalKegiatanPersonil whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JadwalKegiatanPersonil wherePegawaiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JadwalKegiatanPersonil wherePenugasan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JadwalKegiatanPersonil whereTanggalMulai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JadwalKegiatanPersonil whereTanggalSelesai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JadwalKegiatanPersonil whereUpdatedAt($value)
 */
	class JadwalKegiatanPersonil extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\KategoriPelayanan
 *
 * @property int $id
 * @property string $nama
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pelayanan[] $pelayanan
 * @property-read int|null $pelayanan_count
 * @method static \Illuminate\Database\Eloquent\Builder|KategoriPelayanan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|KategoriPelayanan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|KategoriPelayanan query()
 * @method static \Illuminate\Database\Eloquent\Builder|KategoriPelayanan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KategoriPelayanan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KategoriPelayanan whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KategoriPelayanan whereUpdatedAt($value)
 */
	class KategoriPelayanan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\KebijakanPelayanan
 *
 * @property int $id
 * @property string|null $text
 * @property string|null $url_img
 * @property string|null $url_pdf
 * @property int $status_img
 * @property int $status_pdf
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|KebijakanPelayanan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|KebijakanPelayanan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|KebijakanPelayanan query()
 * @method static \Illuminate\Database\Eloquent\Builder|KebijakanPelayanan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KebijakanPelayanan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KebijakanPelayanan whereStatusImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KebijakanPelayanan whereStatusPdf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KebijakanPelayanan whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KebijakanPelayanan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KebijakanPelayanan whereUrlImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KebijakanPelayanan whereUrlPdf($value)
 */
	class KebijakanPelayanan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Kegiatan
 *
 * @property int $id
 * @property string $waktu
 * @property int|null $lab_id
 * @property string $nama_kegiatan
 * @property string $deskripsi_kegiatan
 * @property string $waktu_pelaksanaan
 * @property string $tempat_pelaksanaan
 * @property string $biaya_kegiatan
 * @property string $status
 * @property string $kategori
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Lab|null $lab
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan query()
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan whereBiayaKegiatan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan whereDeskripsiKegiatan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan whereKategori($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan whereLabId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan whereNamaKegiatan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan whereTempatPelaksanaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan whereWaktu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kegiatan whereWaktuPelaksanaan($value)
 */
	class Kegiatan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\KepuasanPelanggan
 *
 * @property int $id
 * @property string $waktu
 * @property int $user_id
 * @property int $pelayanan_id
 * @property string $nama
 * @property string $tingkat_kepuasan
 * @property string|null $komen
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pelayanan $pelayanan
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|KepuasanPelanggan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|KepuasanPelanggan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|KepuasanPelanggan query()
 * @method static \Illuminate\Database\Eloquent\Builder|KepuasanPelanggan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KepuasanPelanggan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KepuasanPelanggan whereKomen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KepuasanPelanggan whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KepuasanPelanggan wherePelayananId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KepuasanPelanggan whereTingkatKepuasan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KepuasanPelanggan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KepuasanPelanggan whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KepuasanPelanggan whereWaktu($value)
 */
	class KepuasanPelanggan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\KompetensiPegawai
 *
 * @property int $id
 * @property int|null $pegawai_id
 * @property string $nama
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|KompetensiPegawai newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|KompetensiPegawai newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|KompetensiPegawai query()
 * @method static \Illuminate\Database\Eloquent\Builder|KompetensiPegawai whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KompetensiPegawai whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KompetensiPegawai whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KompetensiPegawai wherePegawaiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KompetensiPegawai whereUpdatedAt($value)
 */
	class KompetensiPegawai extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Kuisioner
 *
 * @property int $id
 * @property string $pernyataan
 * @property string|null $keterangan
 * @property int $order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $kategori_pelayanan_id
 * @property-read \App\Models\KategoriPelayanan|null $kategori_pelayanan
 * @method static \Illuminate\Database\Eloquent\Builder|Kuisioner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kuisioner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kuisioner query()
 * @method static \Illuminate\Database\Eloquent\Builder|Kuisioner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kuisioner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kuisioner whereKategoriPelayananId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kuisioner whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kuisioner whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kuisioner wherePernyataan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kuisioner whereUpdatedAt($value)
 */
	class Kuisioner extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Lab
 *
 * @property int $id
 * @property string|null $foto
 * @property string $kategori
 * @property string $nama
 * @property string $keterangan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kegiatan[] $kegiatan
 * @property-read int|null $kegiatan_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pegawai[] $pegawai
 * @property-read int|null $pegawai_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pejabat[] $pejabat
 * @property-read int|null $pejabat_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pekerjaan[] $pekerjaan
 * @property-read int|null $pekerjaan_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PeralatanLab[] $peralatan
 * @property-read int|null $peralatan_count
 * @method static \Illuminate\Database\Eloquent\Builder|Lab newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Lab newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Lab query()
 * @method static \Illuminate\Database\Eloquent\Builder|Lab whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lab whereFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lab whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lab whereKategori($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lab whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lab whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lab whereUpdatedAt($value)
 */
	class Lab extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\LabPosition
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pegawai|null $pegawai
 * @method static \Illuminate\Database\Eloquent\Builder|LabPosition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LabPosition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LabPosition query()
 * @method static \Illuminate\Database\Eloquent\Builder|LabPosition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LabPosition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LabPosition whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LabPosition whereUpdatedAt($value)
 */
	class LabPosition extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\LaporanHasilUji
 *
 * @property int $id
 * @property int $pengujian_id
 * @property string $nomor
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deskripsi_benda_uji
 * @property string|null $tempat
 * @property string|null $tanggal_terbit
 * @property string|null $catatan
 * @property string|null $file
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\PermintaanPengujian $permintaan_pengujian
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji query()
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji whereCatatan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji whereDeskripsiBendaUji($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji whereNomor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji wherePengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji whereTanggalTerbit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji whereTempat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUji whereUpdatedAt($value)
 */
	class LaporanHasilUji extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\LaporanHasilUjiApproval
 *
 * @property int $id
 * @property int $laporan_hasil_uji_id
 * @property string $status
 * @property string|null $message
 * @property int $created_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\LaporanHasilUji $laporan
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUjiApproval createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUjiApproval newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUjiApproval newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUjiApproval query()
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUjiApproval updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUjiApproval whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUjiApproval whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUjiApproval whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUjiApproval whereLaporanHasilUjiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUjiApproval whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUjiApproval whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaporanHasilUjiApproval whereUpdatedAt($value)
 */
	class LaporanHasilUjiApproval extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\LogAktivitas
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $date
 * @property string $log
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|LogAktivitas newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LogAktivitas newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LogAktivitas query()
 * @method static \Illuminate\Database\Eloquent\Builder|LogAktivitas whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LogAktivitas whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LogAktivitas whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LogAktivitas whereLog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LogAktivitas whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LogAktivitas whereUserId($value)
 */
	class LogAktivitas extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Maklumat
 *
 * @property int $id
 * @property string|null $isi
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Maklumat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Maklumat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Maklumat query()
 * @method static \Illuminate\Database\Eloquent\Builder|Maklumat whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maklumat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maklumat whereIsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maklumat whereUpdatedAt($value)
 */
	class Maklumat extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Maskot
 *
 * @property int $id
 * @property string|null $nama
 * @property string|null $deskripsi
 * @property string|null $foto
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Maskot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Maskot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Maskot query()
 * @method static \Illuminate\Database\Eloquent\Builder|Maskot whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maskot whereDeskripsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maskot whereFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maskot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maskot whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maskot whereUpdatedAt($value)
 */
	class Maskot extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MasterLayananUji
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property int $pelayanan_id
 * @property-read \App\User|null $creator
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MasterLayananUjiItem[] $items
 * @property-read int|null $items_count
 * @property-read \App\Models\Pelayanan $pelayanan
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujian[] $permintaan
 * @property-read int|null $permintaan_count
 * @property-read \App\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji query()
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji wherePelayananId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUji whereUpdatedBy($value)
 */
	class MasterLayananUji extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MasterLayananUjiItem
 *
 * @property int $id
 * @property string $name
 * @property string $price
 * @property string $working_days
 * @property string $standard
 * @property int $master_layanan_uji_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\MasterLayananUji $layanan_uji
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUjiItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUjiItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUjiItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUjiItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUjiItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUjiItem whereMasterLayananUjiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUjiItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUjiItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUjiItem whereStandard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUjiItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MasterLayananUjiItem whereWorkingDays($value)
 */
	class MasterLayananUjiItem extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Pegawai
 *
 * @property int $id
 * @property string|null $nip
 * @property int|null $lab_id
 * @property string|null $no_hp
 * @property string|null $foto
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $user_id
 * @property int|null $functional_position_id
 * @property int|null $lab_position_id
 * @property int|null $jabatan_id
 * @property int|null $golongan_id
 * @property string|null $gender
 * @property string|null $birth_place
 * @property string|null $birth_date
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AdvisiTeknis[] $advisi_teknis_approval
 * @property-read int|null $advisi_teknis_approval_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AdvisiTeknis[] $advisi_teknis_members
 * @property-read int|null $advisi_teknis_members_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DaftarDiklatPegawai[] $diklat
 * @property-read int|null $diklat_count
 * @property-read \App\Models\Golongan|null $golongan
 * @property-read \App\Models\Jabatan|null $jabatan
 * @property-read \App\Models\FunctionalPosition|null $jabatan_fungsional
 * @property-read \App\Models\LabPosition $jabatan_laboratorium
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KompetensiPegawai[] $kompetensi
 * @property-read int|null $kompetensi_count
 * @property-read \App\Models\Lab|null $lab
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LabPosition[] $lab_positions
 * @property-read int|null $lab_positions_count
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai query()
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereBirthPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereFunctionalPositionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereGolonganId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereJabatanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereLabId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereLabPositionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereNip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereNoHp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pegawai whereUserId($value)
 */
	class Pegawai extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PegawaiLabPosition
 *
 * @property int $id
 * @property int|null $lab_position_id
 * @property int|null $pegawai_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PegawaiLabPosition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PegawaiLabPosition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PegawaiLabPosition query()
 * @method static \Illuminate\Database\Eloquent\Builder|PegawaiLabPosition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PegawaiLabPosition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PegawaiLabPosition whereLabPositionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PegawaiLabPosition wherePegawaiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PegawaiLabPosition whereUpdatedAt($value)
 */
	class PegawaiLabPosition extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Pejabat
 *
 * @property int $id
 * @property int|null $lab_id
 * @property string|null $foto
 * @property string $nama
 * @property string $job
 * @property string $quote
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Lab|null $lab
 * @method static \Illuminate\Database\Eloquent\Builder|Pejabat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pejabat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pejabat query()
 * @method static \Illuminate\Database\Eloquent\Builder|Pejabat whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pejabat whereFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pejabat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pejabat whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pejabat whereLabId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pejabat whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pejabat whereQuote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pejabat whereUpdatedAt($value)
 */
	class Pejabat extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Pekerjaan
 *
 * @property int $id
 * @property int|null $lab_id
 * @property int|null $client_id
 * @property string $nama
 * @property string $time
 * @property string $waktu_pelaksanaan
 * @property string $penaggung_jawab
 * @property string $status_pelaksanaan
 * @property int $pembiayaan
 * @property string|null $keterangan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Client|null $client
 * @property-read \App\Models\Lab|null $lab
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan query()
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan whereLabId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan wherePembiayaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan wherePenaggungJawab($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan whereStatusPelaksanaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pekerjaan whereWaktuPelaksanaan($value)
 */
	class Pekerjaan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Pelayanan
 *
 * @property int $id
 * @property string|null $nama
 * @property int|null $kategori_id
 * @property string|null $file
 * @property string|null $file_sop
 * @property string|null $file_panduan
 * @property string|null $file_biaya
 * @property string|null $text
 * @property string|null $foto
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $pegawai_id
 * @property int|null $subkoor_id
 * @property string $syarat_peserta
 * @property string|null $link_zoom
 * @property string|null $meeting_id
 * @property string|null $passcode
 * @property string|null $subjek_email
 * @property string|null $konten_email
 * @property string|null $background
 * @property string|null $off_days
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MasterLayananUji[] $dataPelayanan
 * @property-read int|null $data_pelayanan_count
 * @property-read \App\Models\KategoriPelayanan|null $kategori
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KepuasanPelanggan[] $kepuasan_pelanggan
 * @property-read int|null $kepuasan_pelanggan_count
 * @property-read \App\Models\Pegawai|null $pegawai
 * @property-read \App\Models\PelayananUI|null $pelayanan_ui
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujian[] $permintaan
 * @property-read int|null $permintaan_count
 * @property-read \App\User|null $subkoor
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan query()
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereBackground($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereFileBiaya($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereFilePanduan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereFileSop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereKategoriId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereKontenEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereLinkZoom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereMeetingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereOffDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan wherePasscode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan wherePegawaiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereSubjekEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereSubkoorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereSyaratPeserta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pelayanan whereUpdatedAt($value)
 */
	class Pelayanan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PelayananUI
 *
 * @property int $id
 * @property int $pelayanan_id
 * @property string|null $keterangan
 * @property string $info
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pelayanan $pelayanan
 * @method static \Illuminate\Database\Eloquent\Builder|PelayananUI newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PelayananUI newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PelayananUI query()
 * @method static \Illuminate\Database\Eloquent\Builder|PelayananUI whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PelayananUI whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PelayananUI whereInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PelayananUI whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PelayananUI wherePelayananId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PelayananUI whereUpdatedAt($value)
 */
	class PelayananUI extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Pengajar
 *
 * @property int $id
 * @property string|null $foto
 * @property string $nama
 * @property string $job
 * @property string $keterangan
 * @property string|null $link_twitter
 * @property string|null $link_facebook
 * @property string|null $link_ig
 * @property string|null $link_likedin
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar query()
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar whereFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar whereLinkFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar whereLinkIg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar whereLinkLikedin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar whereLinkTwitter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengajar whereUpdatedAt($value)
 */
	class Pengajar extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PeralatanLab
 *
 * @property int $id
 * @property int|null $lab_id
 * @property string $nama
 * @property string $merek
 * @property string $type
 * @property string $seri
 * @property string|null $spesifikasi
 * @property string|null $no_bmn
 * @property string|null $fungsi_alat
 * @property string|null $tahun_pengadaan
 * @property int|null $jumlah
 * @property string|null $satuan
 * @property string|null $keterangan_kapasitas
 * @property string|null $kondisi
 * @property string|null $foto_alat
 * @property string|null $tanggal_terakhir_kalibrasi
 * @property string|null $lembaga_pengujian_kalibrasi
 * @property string|null $penanggung_jawab
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property string|null $akurasi
 * @property-read \App\User|null $creator
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PeralatanLabCalibration[] $history
 * @property-read int|null $history_count
 * @property-read \App\Models\Lab|null $lab
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianPengujian[] $pengujian
 * @property-read int|null $pengujian_count
 * @property-read \App\User|null $updater
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PeralatanLabUsage[] $usage
 * @property-read int|null $usage_count
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab query()
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereAkurasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereFotoAlat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereFungsiAlat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereJumlah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereKeteranganKapasitas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereKondisi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereLabId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereLembagaPengujianKalibrasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereMerek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereNoBmn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab wherePenanggungJawab($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereSatuan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereSeri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereSpesifikasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereTahunPengadaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereTanggalTerakhirKalibrasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLab whereUpdatedBy($value)
 */
	class PeralatanLab extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PeralatanLabCalibration
 *
 * @property int $id
 * @property int $peralatan_lab_id
 * @property string $examiner
 * @property string $condition
 * @property string $description
 * @property string $responsible_person
 * @property string $last_calibration
 * @property string $next_calibration
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $certificate
 * @property-read \App\User|null $creator
 * @property-read \App\Models\Lab $peralatan
 * @property-read \App\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration query()
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereCondition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereExaminer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereLastCalibration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereNextCalibration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration wherePeralatanLabId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereResponsiblePerson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabCalibration whereUpdatedBy($value)
 */
	class PeralatanLabCalibration extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PeralatanLabUsage
 *
 * @property int $id
 * @property string $tanggal_pelaksanaan
 * @property string $kategori
 * @property string $keterangan
 * @property int $peralatan_lab_id
 * @property string $pelaksana
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\PeralatanLab $peralatan
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabUsage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabUsage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabUsage query()
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabUsage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabUsage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabUsage whereKategori($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabUsage whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabUsage wherePelaksana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabUsage wherePeralatanLabId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabUsage whereTanggalPelaksanaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PeralatanLabUsage whereUpdatedAt($value)
 */
	class PeralatanLabUsage extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujian
 *
 * @property int $id
 * @property int $client_id
 * @property string|null $order_id
 * @property int|null $pelayanan_id
 * @property string $tgl_permintaan
 * @property string|null $tanggal_pelayanan
 * @property string|null $nomor_permintaan
 * @property string $nama_pemohon
 * @property string $alamat
 * @property string $nomor
 * @property string $email
 * @property string $status
 * @property \datetime|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $company_email
 * @property string|null $company_address
 * @property string|null $company_phone
 * @property string|null $company_name
 * @property int|null $master_layanan_uji_id
 * @property string|null $kode_billing
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Client $client
 * @property-read \App\User|null $creator
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianFile[] $dokumen
 * @property-read int|null $dokumen_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianHistory[] $history
 * @property-read int|null $history_count
 * @property-read \App\Models\Invoice $invoice
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianItem[] $items
 * @property-read int|null $items_count
 * @property-read \App\Models\PermintaanPengujianKajiUlang|null $kaji_ulang
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kuisioner[] $kuisioners
 * @property-read int|null $kuisioners_count
 * @property-read \App\Models\LaporanHasilUji|null $laporan_hasil_uji
 * @property-read \App\Models\MasterLayananUji|null $master_layanan_uji
 * @property-read \App\Models\PermintaanPengujianPayment|null $payments
 * @property-read \App\Models\PermintaanPengujianPengujian|null $pengujian
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianReport[] $reports
 * @property-read int|null $reports_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianSample[] $samples
 * @property-read int|null $samples_count
 * @property-read \App\Models\PermintaanPengujianSpk|null $spk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianTestSample[] $test_samples
 * @property-read int|null $test_samples_count
 * @property-read \App\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereAlamat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereCompanyAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereCompanyEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereCompanyPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereKodeBilling($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereMasterLayananUjiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereNamaPemohon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereNomor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereNomorPermintaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian wherePelayananId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereTanggalPelayanan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereTglPermintaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujian whereUpdatedBy($value)
 */
	class PermintaanPengujian extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianFile
 *
 * @property int $id
 * @property int $permintaan_pengujian_id
 * @property int|null $file_id
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\File|null $dokumen
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianFile whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianFile wherePermintaanPengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianFile whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianFile whereUpdatedAt($value)
 */
	class PermintaanPengujianFile extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianHistory
 *
 * @property int $id
 * @property int $permintaan_pengujian_id
 * @property string $message
 * @property string $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \datetime|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\PermintaanPengujian $order
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianHistory whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianHistory whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianHistory whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianHistory wherePermintaanPengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianHistory whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianHistory whereUpdatedBy($value)
 */
	class PermintaanPengujianHistory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianItem
 *
 * @property int $id
 * @property int|null $product_id
 * @property string $product_name
 * @property string $product_type
 * @property string $product_count
 * @property int $master_layanan_uji_item_id
 * @property int $permintaan_pengujian_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $status
 * @property string|null $keterangan
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\MasterLayananUjiItem $layanan_uji_item
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianPengujian[] $pengujian
 * @property-read int|null $pengujian_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PeralatanLab[] $peralatan
 * @property-read int|null $peralatan_count
 * @property-read \App\Models\PermintaanPengujian $permintaan_pengujian
 * @property-read \App\Models\Product|null $product
 * @property-read \App\Models\PermintaanPengujianSampleItem|null $sample
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem whereMasterLayananUjiItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem wherePermintaanPengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem whereProductCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem whereProductName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem whereProductType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItem whereUpdatedAt($value)
 */
	class PermintaanPengujianItem extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianItemPeralatan
 *
 * @property int $id
 * @property int $permintaan_pengujian_item_id
 * @property int $peralatan_lab_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItemPeralatan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItemPeralatan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItemPeralatan query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItemPeralatan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItemPeralatan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItemPeralatan wherePeralatanLabId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItemPeralatan wherePermintaanPengujianItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianItemPeralatan whereUpdatedAt($value)
 */
	class PermintaanPengujianItemPeralatan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianKajiUlang
 *
 * @property int $id
 * @property int $permintaan_pengujian_id
 * @property array|null $parameter
 * @property int|null $subkontraktor
 * @property string|null $nama_subkontraktor
 * @property string|null $keterangan
 * @property string|null $durasi
 * @property \datetime|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $file_id
 * @property string $status
 * @property \datetime|null $tanggal_pelayanan
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianKajiUlangApproval[] $approvals
 * @property-read int|null $approvals_count
 * @property-read \App\User|null $creator
 * @property-read \App\Models\PermintaanPengujian $permintaan_pengujian
 * @property-read \App\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereDurasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereNamaSubkontraktor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereParameter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang wherePermintaanPengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereSubkontraktor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereTanggalPelayanan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlang whereUpdatedBy($value)
 */
	class PermintaanPengujianKajiUlang extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianKajiUlangApproval
 *
 * @property int $id
 * @property int $permintaan_pengujian_kaji_ulang_id
 * @property string $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $reason
 * @property \datetime|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\User|null $creator
 * @property-read \App\Models\PermintaanPengujianKajiUlang $kaji_ulang
 * @property-read \App\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval wherePermintaanPengujianKajiUlangId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianKajiUlangApproval whereUpdatedBy($value)
 */
	class PermintaanPengujianKajiUlangApproval extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianPayment
 *
 * @property int $id
 * @property int $permintaan_pengujian_id
 * @property string|null $billing_code
 * @property int|null $file_id
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianPaymentConfirmation[] $confirmation
 * @property-read int|null $confirmation_count
 * @property-read \App\User|null $creator
 * @property-read \App\Models\File|null $file
 * @property-read \App\Models\PermintaanPengujian $permintaan_pengujian
 * @property-read \App\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment whereBillingCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment wherePermintaanPengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPayment whereUpdatedBy($value)
 */
	class PermintaanPengujianPayment extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianPaymentConfirmation
 *
 * @property int $id
 * @property int $permintaan_pengujian_payment_id
 * @property string $bank_name
 * @property string $account_number
 * @property string $account_holder
 * @property string $payment_amount
 * @property mixed $payment_date
 * @property string $status
 * @property int $file_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianPaymentConfirmationApproval[] $approval
 * @property-read int|null $approval_count
 * @property-read \App\Models\File $file
 * @property-read \App\Models\PermintaanPengujianPayment $payment
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation whereAccountHolder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation wherePaymentAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation wherePaymentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation wherePermintaanPengujianPaymentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmation whereUpdatedAt($value)
 */
	class PermintaanPengujianPaymentConfirmation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianPaymentConfirmationApproval
 *
 * @property int $id
 * @property int $permintaan_pengujian_payment_confirmation_id
 * @property string $status
 * @property string|null $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\PermintaanPengujianPaymentConfirmation $payment_confirmation
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmationApproval newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmationApproval newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmationApproval query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmationApproval whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmationApproval whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmationApproval whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmationApproval wherePermintaanPengujianPaymentConfirmationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmationApproval whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPaymentConfirmationApproval whereUpdatedAt($value)
 */
	class PermintaanPengujianPaymentConfirmationApproval extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianPengujian
 *
 * @property int $id
 * @property int $permintaan_pengujian_id
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianPengujianItem[] $items
 * @property-read int|null $items_count
 * @property-read \App\Models\PermintaanPengujian $permintaan_pengujian
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujian newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujian newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujian query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujian whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujian whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujian wherePermintaanPengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujian whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujian whereUpdatedAt($value)
 */
	class PermintaanPengujianPengujian extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianPengujianItem
 *
 * @property int $id
 * @property int $permintaan_pengujian_pengujian_id
 * @property int $item_id
 * @property string $hasil_pengujian
 * @property string $kategori_pengujian
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\PermintaanPengujianPengujian $pengujian
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianPengujianItemPeralatan[] $peralatan
 * @property-read int|null $peralatan_count
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItem whereHasilPengujian($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItem whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItem whereKategoriPengujian($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItem wherePermintaanPengujianPengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItem whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItem whereUpdatedAt($value)
 */
	class PermintaanPengujianPengujianItem extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianPengujianItemPeralatan
 *
 * @property int $id
 * @property int $pengujian_item_id
 * @property int $peralatan_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\PeralatanLab $peralatan_lab
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItemPeralatan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItemPeralatan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItemPeralatan query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItemPeralatan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItemPeralatan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItemPeralatan wherePengujianItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItemPeralatan wherePeralatanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianPengujianItemPeralatan whereUpdatedAt($value)
 */
	class PermintaanPengujianPengujianItemPeralatan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianReport
 *
 * @property int $id
 * @property int $permintaan_pengujian_id
 * @property string $nomor
 * @property string $tanggal_pengujian
 * @property string $tempat_terbit
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianReport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianReport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianReport query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianReport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianReport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianReport whereNomor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianReport wherePermintaanPengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianReport whereTanggalPengujian($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianReport whereTempatTerbit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianReport whereUpdatedAt($value)
 */
	class PermintaanPengujianReport extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianSample
 *
 * @property int $id
 * @property int $permintaan_pengujian_id
 * @property string|null $jasa_ekspedisi
 * @property string|null $no_resi
 * @property int|null $file_id
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $tanggal_diterima
 * @property string $jenis_pengiriman
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianSampleApproval[] $approval
 * @property-read int|null $approval_count
 * @property-read \App\Models\File|null $bukti
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianItem[] $items
 * @property-read int|null $items_count
 * @property-read \App\Models\PermintaanPengujian $permintaan_pengujian
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample whereJasaEkspedisi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample whereJenisPengiriman($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample whereNoResi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample wherePermintaanPengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample whereTanggalDiterima($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSample whereUpdatedAt($value)
 */
	class PermintaanPengujianSample extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianSampleApproval
 *
 * @property int $id
 * @property int $permintaan_pengujian_sample_id
 * @property string $status
 * @property string|null $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\PermintaanPengujianSample $sample
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleApproval newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleApproval newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleApproval query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleApproval whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleApproval whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleApproval whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleApproval wherePermintaanPengujianSampleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleApproval whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleApproval whereUpdatedAt($value)
 */
	class PermintaanPengujianSampleApproval extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianSampleItem
 *
 * @property int $id
 * @property int $permintaan_pengujian_sample_id
 * @property int $sample_id
 * @property string $kode_sampel
 * @property string $status
 * @property string|null $keterangan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleItem whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleItem whereKodeSampel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleItem wherePermintaanPengujianSampleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleItem whereSampleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleItem whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSampleItem whereUpdatedAt($value)
 */
	class PermintaanPengujianSampleItem extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianSpk
 *
 * @property int $id
 * @property int $permintaan_pengujian_id
 * @property string|null $nomor
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property string|null $status
 * @property string $keterangan
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PermintaanPengujianSpkApproval[] $approvals
 * @property-read int|null $approvals_count
 * @property-read \App\User|null $creator
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pegawai[] $members
 * @property-read int|null $members_count
 * @property-read \App\Models\PermintaanPengujian $permintaan_pengujian
 * @property-read \App\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk whereNomor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk wherePermintaanPengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk whereTanggalMulai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk whereTanggalSelesai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpk whereUpdatedBy($value)
 */
	class PermintaanPengujianSpk extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianSpkApproval
 *
 * @property int $id
 * @property int $permintaan_pengujian_spk_id
 * @property string|null $status
 * @property string|null $reason
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\User|null $creator
 * @property-read \App\Models\PermintaanPengujianSpk $spk
 * @property-read \App\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval isApprovedByKabalai()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval isCheckedBySubkoor()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval wherePermintaanPengujianSpkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkApproval whereUpdatedBy($value)
 */
	class PermintaanPengujianSpkApproval extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianSpkMember
 *
 * @property int $id
 * @property int $pegawai_id
 * @property int $permintaan_pengujian_spk_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\PermintaanPengujianSpk $permintaan_spk
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkMember newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkMember newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkMember query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkMember whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkMember whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkMember wherePegawaiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkMember wherePermintaanPengujianSpkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianSpkMember whereUpdatedAt($value)
 */
	class PermintaanPengujianSpkMember extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PermintaanPengujianTestSample
 *
 * @property int $id
 * @property int $permintaan_pengujian_id
 * @property string|null $jasa_ekspedisi
 * @property string|null $no_resi
 * @property int|null $file_id
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $tanggal_diterima
 * @property string $jenis_pengiriman
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\File $bukti_pengiriman
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample whereJasaEkspedisi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample whereJenisPengiriman($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample whereNoResi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample wherePermintaanPengujianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample whereTanggalDiterima($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermintaanPengujianTestSample whereUpdatedAt($value)
 */
	class PermintaanPengujianTestSample extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Permission
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PesertaBimtek
 *
 * @property int $id
 * @property int $bimtek_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $user_id
 * @property-read \App\Models\Bimtek $bimtek
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|PesertaBimtek newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PesertaBimtek newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PesertaBimtek query()
 * @method static \Illuminate\Database\Eloquent\Builder|PesertaBimtek whereBimtekId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PesertaBimtek whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PesertaBimtek whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PesertaBimtek whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PesertaBimtek whereUserId($value)
 */
	class PesertaBimtek extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Product
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductAlatKerja[] $alat
 * @property-read int|null $alat_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductBahanKerja[] $bahan
 * @property-read int|null $bahan_count
 * @property-read \App\User|null $creator
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MasterLayananUjiItem[] $parameters
 * @property-read int|null $parameters_count
 * @property-read \App\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|Product createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedBy($value)
 */
	class Product extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProductAlatKerja
 *
 * @property int $id
 * @property string $nama
 * @property int $product_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAlatKerja newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAlatKerja newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAlatKerja query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAlatKerja whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAlatKerja whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAlatKerja whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAlatKerja whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAlatKerja whereUpdatedAt($value)
 */
	class ProductAlatKerja extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProductBahanKerja
 *
 * @property int $id
 * @property string $nama
 * @property int $product_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ProductBahanKerja newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductBahanKerja newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductBahanKerja query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductBahanKerja whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductBahanKerja whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductBahanKerja whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductBahanKerja whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductBahanKerja whereUpdatedAt($value)
 */
	class ProductBahanKerja extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Province
 *
 * @property int $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Regency[] $regencies
 * @property-read int|null $regencies_count
 * @method static \Illuminate\Database\Eloquent\Builder|Province newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Province newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Province query()
 * @method static \Illuminate\Database\Eloquent\Builder|Province whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Province whereName($value)
 */
	class Province extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Regency
 *
 * @property int $id
 * @property int|null $province_id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\District[] $districts
 * @property-read int|null $districts_count
 * @property-read \App\Models\Province|null $province
 * @method static \Illuminate\Database\Eloquent\Builder|Regency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Regency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Regency query()
 * @method static \Illuminate\Database\Eloquent\Builder|Regency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regency whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regency whereProvinceId($value)
 */
	class Regency extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SaranaPrasarana
 *
 * @property int $id
 * @property string|null $foto
 * @property string $nama
 * @property string $kategori
 * @property string|null $keterangan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SaranaPrasarana newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SaranaPrasarana newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SaranaPrasarana query()
 * @method static \Illuminate\Database\Eloquent\Builder|SaranaPrasarana whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SaranaPrasarana whereFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SaranaPrasarana whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SaranaPrasarana whereKategori($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SaranaPrasarana whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SaranaPrasarana whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SaranaPrasarana whereUpdatedAt($value)
 */
	class SaranaPrasarana extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Sejarah
 *
 * @property int $id
 * @property string|null $isi
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Sejarah newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sejarah newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sejarah query()
 * @method static \Illuminate\Database\Eloquent\Builder|Sejarah whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sejarah whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sejarah whereIsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sejarah whereUpdatedAt($value)
 */
	class Sejarah extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SettingApp
 *
 * @property int $id
 * @property string|null $logo
 * @property string|null $nama
 * @property string|null $copyright
 * @property string|null $nomor_wa
 * @property string|null $nomor_kontak
 * @property string|null $email
 * @property string|null $alamat
 * @property string|null $lokasi
 * @property string|null $link_ig
 * @property string|null $link_yt
 * @property string|null $link_facebook
 * @property string|null $link_twitter
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $hari_libur
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp query()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereAlamat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereCopyright($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereHariLibur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereLinkFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereLinkIg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereLinkTwitter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereLinkYt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereLokasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereNomorKontak($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereNomorWa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingApp whereUpdatedAt($value)
 */
	class SettingApp extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Slider
 *
 * @property int $id
 * @property string|null $foto
 * @property string|null $judul
 * @property string|null $keterangan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Slider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Slider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Slider query()
 * @method static \Illuminate\Database\Eloquent\Builder|Slider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slider whereFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slider whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slider whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slider whereUpdatedAt($value)
 */
	class Slider extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\StrukturOrganisasi
 *
 * @property int $id
 * @property string|null $foto
 * @property string|null $nama
 * @property string|null $nip
 * @property string|null $jabatan
 * @property string $email
 * @property string|null $keterangan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasi newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasi newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasi query()
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasi whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasi whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasi whereFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasi whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasi whereJabatan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasi whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasi whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasi whereNip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasi whereUpdatedAt($value)
 */
	class StrukturOrganisasi extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\StrukturOrganisasiFile
 *
 * @property int $id
 * @property string|null $gambar
 * @property string|null $pdf
 * @property int $gambar_status
 * @property int $pdf_status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasiFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasiFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasiFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasiFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasiFile whereGambar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasiFile whereGambarStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasiFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasiFile wherePdf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasiFile wherePdfStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StrukturOrganisasiFile whereUpdatedAt($value)
 */
	class StrukturOrganisasiFile extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Telepon
 *
 * @property int $id
 * @property string|null $nama
 * @property string|null $nomor_telepon
 * @property string|null $deskripsi
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Telepon newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Telepon newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Telepon query()
 * @method static \Illuminate\Database\Eloquent\Builder|Telepon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Telepon whereDeskripsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Telepon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Telepon whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Telepon whereNomorTelepon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Telepon whereUpdatedAt($value)
 */
	class Telepon extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TugasFungsi
 *
 * @property int $id
 * @property string|null $tugas
 * @property string|null $fungsi
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $url_pdf
 * @property string|null $url_img
 * @property int $status_pdf
 * @property int $status_img
 * @method static \Illuminate\Database\Eloquent\Builder|TugasFungsi newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TugasFungsi newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TugasFungsi query()
 * @method static \Illuminate\Database\Eloquent\Builder|TugasFungsi whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TugasFungsi whereFungsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TugasFungsi whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TugasFungsi whereStatusImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TugasFungsi whereStatusPdf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TugasFungsi whereTugas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TugasFungsi whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TugasFungsi whereUrlImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TugasFungsi whereUrlPdf($value)
 */
	class TugasFungsi extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\UmpanBalik
 *
 * @property int $id
 * @property int $client_id
 * @property string $nama
 * @property string $keterangan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Client $client
 * @method static \Illuminate\Database\Eloquent\Builder|UmpanBalik newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UmpanBalik newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UmpanBalik query()
 * @method static \Illuminate\Database\Eloquent\Builder|UmpanBalik whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UmpanBalik whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UmpanBalik whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UmpanBalik whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UmpanBalik whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UmpanBalik whereUpdatedAt($value)
 */
	class UmpanBalik extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\VideoUI
 *
 * @property int $id
 * @property string|null $judul
 * @property string|null $logo
 * @property string|null $deskripsi
 * @property string|null $link
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|VideoUI newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VideoUI newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VideoUI query()
 * @method static \Illuminate\Database\Eloquent\Builder|VideoUI whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VideoUI whereDeskripsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VideoUI whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VideoUI whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VideoUI whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VideoUI whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VideoUI whereUpdatedAt($value)
 */
	class VideoUI extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Village
 *
 * @property int $id
 * @property int|null $district_id
 * @property string $name
 * @property-read \App\Models\District|null $district
 * @method static \Illuminate\Database\Eloquent\Builder|Village newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Village newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Village query()
 * @method static \Illuminate\Database\Eloquent\Builder|Village whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Village whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Village whereName($value)
 */
	class Village extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\VisiMisi
 *
 * @property int $id
 * @property string|null $visi
 * @property string|null $misi
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|VisiMisi newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VisiMisi newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VisiMisi query()
 * @method static \Illuminate\Database\Eloquent\Builder|VisiMisi whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VisiMisi whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VisiMisi whereMisi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VisiMisi whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VisiMisi whereVisi($value)
 */
	class VisiMisi extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $two_factor_secret
 * @property string|null $two_factor_recovery_codes
 * @property string $level
 * @property string $status
 * @property string|null $remember_token
 * @property int|null $current_team_id
 * @property string|null $profile_photo_path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AdvisiTeknis[] $advisi_teknis_approval
 * @property-read int|null $advisi_teknis_approval_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AdvisiTeknis[] $advisi_teknis_members
 * @property-read int|null $advisi_teknis_members_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Bimtek[] $bimtek
 * @property-read int|null $bimtek_count
 * @property-read \App\Models\Client|null $client
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KepuasanPelanggan[] $kepuasan_pelanggan
 * @property-read int|null $kepuasan_pelanggan_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LogAktivitas[] $log_aktivitas
 * @property-read int|null $log_aktivitas_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\Pegawai|null $pegawai
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCurrentTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProfilePhotoPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorRecoveryCodes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUsername($value)
 */
	class User extends \Eloquent implements \Illuminate\Contracts\Auth\MustVerifyEmail {}
}

