<?php


use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\Models\KategoriPelayanan;
use App\Models\Pelayanan;
use App\Models\Maskot;
use App\Models\InformasiPublik;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesAndPermissionsSeeder::class,
            UserSeeder::class,      
            JabatanSeeder::class,      
            JabatanLaboratoriumSeeder::class,      
            GolonganSeeder::class,      
        ]);
        // \App\Models\User::factory(1)->create();

        \App\Models\SettingApp::create([
            'nama' => 'Balai Bahan dan Struktur Bangunan Gedung',
            'nomor_kontak' => '6281332535009',
            'nomor_wa' => '6281332535009',
            'email' => 'ditbtpp.bbsbg@pu.go.id',
            'alamat' => 'Komplek Dit.BtPP, Jl. Panyauangan, CIleuniy Wetan, Kab. Bandung 40393',
        	'copyright' => 'Balai Bahan dan Struktur Bangunan Gedung'
        ]);


        //Lab 
        \App\Models\Lab::create([
        	'kategori' => 'bahan',
        	'nama' => 'Lab Bahan',
        	'keterangan' => 'Tempat Bahan'
        ]);

        \App\Models\Lab::create([
        	'kategori' => 'struktur',
        	'nama' => 'Lab Struktur',
        	'keterangan' => 'Tempat Struktur'
        ]);

        //Sejarah 
        \App\Models\Sejarah::create([
        	'isi' => '
            _|Balai Bahan dan Struktur Bangunan Gedung merupakan salah satu unit kerja teknis di bawah Direktorat Jenderal Cipta Karya – Kementerian Pekerjaan Umum dan Perumahan Rakyat, diawali dari:;
            1975–1983|Sub Direktorat Bahan Bangunan dan Konstruksi Direktorat Penyelidikan Masalah Bangunan (DPMB) di bawah Direktorat Jendral Cipta Karya, Departemen Pekerjaan Umum;
            1984–1995|Bidang Bahan Bangunan dan Bidang Struktur & Konstruksi Bangunan Pusat Penelitian dan Pengembangan Permukiman (PUSKIM) di bawah Badan Penelitian dan Pengembangan Departemen Pekerjaan Umum;
            1995-2000|Balai Bahan Bangunan dan Balai Struktur & Konstruksi Bangunan Pusat Penelitian dan Pengembangan Permukiman (PUSKIM) di bawah Badan Penelitian dan Pengembangan Departemen Pekerjaan Umum;
            2001–2004|Balai Bahan Bangunan dan Balai Struktur & Konstruksi Pusat Penelitian dan Pengembangan Permukiman (PUSKIM) di bawah Badan Penelitian dan Pengembangan Departemen Permukiman dan Prasarana Wilayah;
            2005–2009|Balai Bahan Bangunan dan Balai Struktur & Konstruksi Pusat Penelitian dan Pengembangan Permukiman (PUSKIM) di bawah Badan Penelitian dan Pengembangan Departemen Pekerjaan Umum;
            2010–2014|Balai Bahan Bangunan dan Balai Struktur & Konstruksi Bangunan Pusat Penelitian dan Pengembangan Permukiman (PUSKIM) di bawah Badan Penelitian dan Pengembangan Kementerian Pekerjaan Umum;
            2015|Balai Bahan Bangunan dan Balai Struktur & Konstruksi Bangunan Pusat Penelitian dan Pengembangan Perumahan dan Permukiman di bawah Badan Penelitian dan Pengembangan Kementerian Pekerjaan Umum dan Perumahan Rakyat;
            2016-2020|Balai Litbang Bahan dan Struktur Bangunan Pusat Penelitian dan Pengembangan Perumahan dan Permukiman di bawah Badan Penelitian dan Pengembangan Kementerian Pekerjaan Umum dan Perumahan Rakyat;
            2020-sekarang|Balai Bahan dan Struktur Bangunan Gedung (BBSBG) Direktorat Bina Teknik Permukiman dan Perumahan di bawah Direktorat Jenderal Cipta Karya Kementerian Pekerjaan Umum dan Perumahan Rakyat;
            _|Balai Bahan dan Struktur Bangunan Gedung merupakan salah satu unit kerja baru di lingkungan Direktorat Jenderal Cipta Karya dibentuk pada tahun 2020 melalui Peraturan Menteri PUPR Nomor 16 Tahun 2020 tentang Struktur Organisasi dan Tata Kerja Unit Pelaksana Teknis Kementerian PUPR. Pada tahun 2021, Balai Bahan dan Struktur Bangunan Gedung merupakan Satuan Kerja mandiri. Dalam pelaksanaannya, Balai Bahan dan Struktur Bangunan Gedung telah melaksanakan beberapa kegiatan seperti layanan pengujian laboratorium lingkup bahan dan struktur bangunan gedung, advis teknis bidang keandalan struktur bangunan gedung, bimbingan teknis, kerekayasaan di bidang teknologi bahan bangunan dan struktur bangunan, serta kegiatan dukungan lainnya. Untuk menjalankan tugas dan fungsinya dengan baik, Balai Bahan dan Struktur Bangunan Gedung selalu berupaya melayani pihak-pihak yang menjadi mitra kerjanya, baik pihak Internal Kementerian PUPR maupun pihak eksternal. Pihak internal Kementerian PUPR yang menjadi mitra kerja seperti Direktorat Bina Teknik Permukiman dan Perumahan, Direktorat Bina Penataan Bangunan, Direktorat Prasarana Strategis, Balai Prasarana Permukiman Wilayah seluruh Indonesia, Direktorat Jenderal Penyediaan Perumahan, dan unit kerja lainnya. Sedangkan pihak eksternal yang menjadi mitra kerja seperti Kementerian/Lembaga pusat, Pemerintah daerah, Kontraktor BUMN/swasta, Produsen, Konsultan, Perguruan Tinggi, dan lainnya.;'
        ]);

        // visi misi
        \App\Models\VisiMisi::create([
        	'visi' => 'Terwujudnya Balai Bahan dan Struktur Bangunan Gedung sebagai pusat layanan unggulan advis teknis, pengujian, bimbingan teknis, serta pengkajian bidang bahan dan struktur bangunan gedung dengan mengikuti dinamika perkembangan teknologi.',
        	'misi' => '
            Melaksanakan advis teknis keandalan bangunan gedung serta layanan pengujian laboratorium sesuai kebutuhan mitra kerja dengan pelayanan prima;
            Meningkatkan kualitas Sumber Daya Manusia yang profesional bidang bahan dan struktur bangunan gedung;
            Melaksanakan pengkajian dan penerapan teknologi untuk menghasilkan kliring dan layanan teknologi;
            Mewujudkan lembaga terakreditasi yang mendapatkan pengakuan nasional dan internasional secara berkelanjutan
            '
        ]);

        //tugas fungsi
        \App\Models\TugasFungsi::create([
        	'tugas' => 'Berdasarakan Permen PUPR No. 16 tahun 2020 Balai Bahan dan Struktur Bangunan Gedung mempunyai tugas melaksanakan pelayanan pengujian, inspeksi, dan sertifikasi serta pengkajian teknologi bahan dan struktur bangunan gedung.',
        	'fungsi' => '
            Penyusunan rencana, program, dan anggaran;
            Pelaksanaan pengujian bahan dan struktur bangunan di laboratorium dan lapangan;
            Pengelolaan laboratorium bahan dan struktur bangunan;
            Pengelolaan sistem manajemen mutu laboratorium;
            Pelaksanaan inspeksi dan sertifikasi bahan dan struktur bangunan;
            Pelaksanaan bimbingan teknis dan diseminasi bidang bahan dan struktur bangunan gedung;
            Pelaksanaan audit teknologi serta penilaian keandalan struktur bangunan pascakonstruksi dan pascabencana;
            Pelaksanaan perekayasaan bahan dan struktur bangunan;
            Pelaksanaan kliring teknologi bahan dan struktur bangunan; dan
            Pelaksanaan urusan tata usaha dan rumah tangga balai
            '
        ]);

        \App\Models\StrukturOrganisasi::create([
        	'nama' => 'Ketua',
        	'nip' => '12345678912345678',
            'jabatan' => 'ketua',
            'email' => 'ketua1@gmail.com',
        	'keterangan' => 'Autem ipsum nam porro corporis rerum. Quis eos dolorem eos itaque inventore commodi labore quia quia. Exercitationem repudiandae officiis neque suscipit non officia eaque itaque enim. Voluptatem officia accusantium nesciunt est omnis tempora consectetur dignissimos. Sequi nulla at esse enim cum deserunt eius.'
        ]);
        $kategori = [
            ['nama' => 'Pengujian'],
            ['nama' => 'Advis Teknis'],
            ['nama' => 'Bimbingan Teknis'],
        ];
        KategoriPelayanan::insert($kategori);

        $pelayanan = [
            [
                'nama' => 'Lab Bahan Bangunan',
                'kategori_id' => 1,    
            ],
            [
                'nama' => 'Lab struktur Bangunan',
                'kategori_id' => '1',
            ],
            [
                'nama' => 'Advis Teknis',
                'kategori_id' => '2',
            ],
            [
                'nama' => 'Bimbingan Teknis',
                'kategori_id' => '3',
            ],
        ];
        Pelayanan::insert($pelayanan);

        Maskot::create([
            'nama' => 'Kang BaTur',
            'deskripsi' => 'Laborum magna quis ipsum aliqua laborum do aliqua occaecat ad excepteur do id dolore.Voluptate esse nulla occaecat anim ut voluptate non ullamco. Pariatur ipsum reprehenderit eiusmod quis culpa sunt est ea cupidatat qui. Ea culpa veniam commodo laboris quis exercitation qui laborum. Et ex pariatur quis enim eu ad velit in. Aliqua ipsum ut ad ipsum elit.'
        ]);

        $informasi = [
            [
                'nama' => 'JIDH',
                'link' => 'http://jdih.pu.go.id',
            ],
            [
                'nama' => 'PPID',
                'link' => 'http://eppid.pu.go.id',
            ],
            [
                'nama' => 'Whistle Blowing System',
                'link' => 'http://wispu.go.id',
            ],
            [
                'nama' => 'Pengaduan Masyaratak',
                'link' => 'http://pengaduan.pu.go.id',
            ],
            [
                'nama' => 'Pelaporan Greatifikasi',
                'link' => 'http://gol.itjen.pu.go.id',
            ]
        ];

        InformasiPublik::insert($informasi);

        \App\Models\BeritaUI::create([
            'jumlah_berita' => '0',
        ]);

        \App\Models\VideoUI::create([
            'judul' => '-',
        ]);

        \App\Models\Maklumat::create([
            'isi' => 'maklumat 1;maklumat 2;maklumat 3;maklumat 4',
        ]);
    }
}
