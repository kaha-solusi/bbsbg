<?php

use App\Models\LabPosition;
use Illuminate\Database\Seeder;

class JabatanLaboratoriumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sampleData = [
            'Kepala Balai Bahan dan Struktur Bangunan ',
            'Kepala Sub Bagian Umum & Tata Usaha', 
            'Sub Koordinator PLT Pengujian', 
            'Sub Koordinator PLT Pengkajian', 
            'Teknisi Laboratorium Struktur', 
            'Engineer Laboratorium Bahan', 
            'Penyelia Laboratorium Bahan', 
            'Ketua Tim', 
            'Tim SMM Balai Bahan dan Struktur Bangunan', 
            'Teknisi Laboratorium Bahan', 
            'Petugas Pengelola Peralatan', 
            'Penyelia Laboratorium Struktur', 
            'Engineer Laboratorium Struktur', 
            'Bendahara pengeluaran', 
            'Petugas Pengelola Sampel', 
            'Petugas Administrasi Umum', 
            'Petugas Layanan'
        ];

        $data = [];
        foreach ($sampleData as $sample) {
            $data[] = [
                'name' => $sample,
            ];
        }

        LabPosition::insert($data);
    }
}
