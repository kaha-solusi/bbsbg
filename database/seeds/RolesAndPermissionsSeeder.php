<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        Role::firstOrCreate(['name' => 'pelanggan']);
        Role::firstOrCreate(['name' => 'direktur']);
        Role::firstOrCreate(['name' => 'kepala-balai']);
        Role::firstOrCreate(['name' => 'subkoor']);
        Role::firstOrCreate(['name' => 'penyelia']);
        Role::firstOrCreate(['name' => 'engineer']);
        Role::firstOrCreate(['name' => 'teknisi']);
        Role::firstOrCreate(['name' => 'admin']);
        
        $role = Role::firstOrCreate(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());
    }
}
