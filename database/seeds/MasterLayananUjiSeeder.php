<?php

use App\Models\MasterLayananUji;
use App\Models\MasterLayananUjiItem;
use Illuminate\Database\Seeder;

class MasterLayananUjiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(MasterLayananUji::class, 3)
            ->create()
            ->each(function ($master_layanan_uji) {
                $master_layanan_uji->items()->createMany(
                    factory(MasterLayananUjiItem::class, 3)->make()->toArray()
                );
            });
    }
}
