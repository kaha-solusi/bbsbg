<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        
        Permission::firstOrCreate(['name' => 'can.add-member.advis-teknis']);
        Permission::firstOrCreate(['name' => 'can.upload-document.advis-teknis']);
        Permission::firstOrCreate(['name' => 'submit.advisi-teknis']);

        $abilities = ['view', 'add', 'edit', 'delete', 'approve'];
        $names = [
            'master',
            'role',
            'advisi_teknis',
            'permintaan_pengujian',
            'payment',
            'kaji_ulang',
            'penerimaan_benda_uji',
            'surat_perintah_kerja',
            'proses_pengujian',
            'laporan_hasil_uji',
            'peralatan'
        ];

        foreach ($names as $name) {
            foreach ($abilities as $ability) {
                $name = strtolower(Str::plural($name));
                Permission::firstOrCreate(['name' => $ability.'_'.$name ]);
            }
        }
        
        Permission::firstOrCreate(['name' => 'upload_layanan_konfirmasi']);
        
        $role = Role::firstOrCreate(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());
    }
}
