<?php

use App\Models\Jabatan;
use Illuminate\Database\Seeder;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'REKAYASA AHLI MADYA',
            'KEPALA  SUBBAGIAN UMUM DAN TATA USAHA',
            'PEREKAYASA AHLI MUDA',
            'ANALIS BMN',
            'TEKNISI SARANA DAN PRASARANA',
            'PEREKAYASA AHLI PERTAMA',
            'PENGELOLA PEMELIHARAAN LABORATORIUM',
            'TEKNIK PENYEHATAN LINGKUNGAN AHLI PERTAMA',
            'ARSIPARIS AHLI PERTAMA',
            'TEKNIK TATA BANGUNAN DAN PERUMAHAN AHLI PERTAMA',
            'PENELAAH BANGUNAN GEDUNG DAN PERMUKIMAN',
            'PENYUSUN RENCANA KEGIATAN DAN ANGGARAN',
            'BENDAHARA PENGELUARAN',
            'PENGELOLA MONITORING DAN EVALUASI',
            'TEKNIK TATA BANGUNAN DAN PERUMAHAN TERAMPIL',
            'PENGELOLA SARANA DAN PRASARANA KANTOR',
            'JURU OPERASI DAN PEMELIHARAAN',
            'PENATA KEUANGAN',
            'PENATA TEKNIK',
            'PENATA BMN',
            'SEKRETARIS',
            'PENGADMINISTRASI UMUM',
            'PETUGAS OPERASI DAN PEMELIHARAAN',
            'PENGEMUDI',
            'PRAMUBAKTI',
            'AHLI PERTAMA TEKNIK TATA BANGUNAN DAN PERUMAHAN',
            'PELAKSANA / TERAMPIL TEKNIK TATA BANGUNAN',
            'PENELITI AHLI PERTAMA',
            'KONSULTAN INDIVIDU (KI)',
            'KONSULTAN INDIVIDU (KI KEHUMASAN)',
            'KONSULTAN INDIVIDU (KI PRANATA KOMPUTER)',
            'KEPALA  BALAI BAHAN DAN STRUKTUR BANGUNAN GEDUNG, BINA TEKNIK PERMUKIMAN DAN PERUMAHAN, DIREKTORAT JENDERAL CIPTA KARYA, KEMENTERIAN PUPR',
        ];

        $createData = [];

        foreach ($data as $d) {
           $createData[] = [
               'nama' => $d,
           ];
        }
        Jabatan::insert($createData);
    }
}
