<?php

use App\Models\Golongan;
use Illuminate\Database\Seeder;

class GolonganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'PEMBINA IV/A',
            'PENATA TK.I III/D',
            'PENATA III/C',
            'PENATA MUDA TINGKAT I III/B',
            'PENATA MUDA III/A',
            'PENGATUR TK I II/D',
            'PENGATUR II/C',
            'PENGATUR MUDA TK I II/B',
            'PENGATUR MUDA II/A',
        ];

        $golongans = [];
        foreach ($data as $d) {
            $golongans[] = [
                'nama' => $d,
            ];
        }

        Golongan::insert($golongans);
    }
}
