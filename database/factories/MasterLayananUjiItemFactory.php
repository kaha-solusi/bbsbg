<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\MasterLayananUjiItem;
use App\Models\MasterLayananUji;
use Faker\Generator as Faker;

$factory->define(MasterLayananUjiItem::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => $faker->randomFloat(),
        'working_days' => $faker->randomDigitNotNull,
        'master_layanan_uji_id' => factory(MasterLayananUji::class)
    ];
});
