<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\MasterLayananUji;
use Faker\Generator as Faker;

$factory->define(MasterLayananUji::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->address
    ];
});
