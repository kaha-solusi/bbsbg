<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKategoriPengujianToPermintaanPengujianSpkHasilPengujiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujian_spk_hasil_pengujians', function (Blueprint $table) {
            $table->string('kategori_pengujian')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujian_spk_hasil_pengujians', function (Blueprint $table) {
            $table->dropColumn('kategori_pengujian');
        });
    }
}
