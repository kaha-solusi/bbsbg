<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKepuasanPelanggansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kepuasan_pelanggans', function (Blueprint $table) {
            $table->id();
            $table->string('waktu');
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->foreignId('pelayanan_id')->constrained('pelayanans')->onDelete('cascade');
            $table->string('nama');
            $table->enum('tingkat_kepuasan', [1,2,3,4,5]);
            $table->text('komen')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kepuasan_pelanggans');
    }
}
