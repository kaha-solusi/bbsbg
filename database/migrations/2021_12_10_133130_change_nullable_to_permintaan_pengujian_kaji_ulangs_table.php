<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeNullableToPermintaanPengujianKajiUlangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujian_kaji_ulangs', function (Blueprint $table) {
            $table->longText('parameter')->nullable()->change();
            $table->boolean('subkontraktor')->nullable()->change();
            $table->string('durasi')->nullable()->change();
            $table->string('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujian_kaji_ulangs', function (Blueprint $table) {
            $table->longText('parameter')->nullable(false)->change();
            $table->boolean('subkontraktor')->nullable(false)->change();
            $table->string('durasi')->nullable(false)->change();
            $table->dropColumn('status');
        });
    }
}
