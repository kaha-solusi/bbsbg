<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTableStrukturOrganisasiFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('struktur_organisasi_files', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('gambar')->nullable();
            $table->string('pdf')->nullable();
            $table->boolean('gambar_status')->default(0);
            $table->boolean('pdf_status')->default(0);            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('struktur_organisasi_files');
    }
}
