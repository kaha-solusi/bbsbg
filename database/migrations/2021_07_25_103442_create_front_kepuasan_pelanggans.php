<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFrontKepuasanPelanggans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('front_kepuasan_pelanggans', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->text('text')->nullable();
            $table->string('url_img')->nullable();
            $table->string('url_pdf')->nullable();
            $table->boolean('status_img')->default(0);
            $table->boolean('status_pdf')->default(0);            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('front_kepuasan_pelanggans');
    }
}
