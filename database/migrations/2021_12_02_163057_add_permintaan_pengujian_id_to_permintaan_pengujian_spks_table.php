<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPermintaanPengujianIdToPermintaanPengujianSpksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujian_spks', function (Blueprint $table) {
            $table->foreignId('permintaan_pengujian_id')
                ->after('id')
                ->constrained('permintaan_pengujians');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujian_spks', function (Blueprint $table) {
            $table->dropForeign(['permintaan_pengujian_id']);
            $table->dropColumn('permintaan_pengujian_id');
        });
    }
}
