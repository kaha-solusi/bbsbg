<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCreatedByToAdvisiTeknisApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('advisi_teknis_approvals', function (Blueprint $table) {
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);
            
            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL');
        
            $table->foreign('updated_by')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advisi_teknis_approvals', function (Blueprint $table) {
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);
            $table->foreign('created_by')
                    ->references('id')->on('pegawais')
                    ->onDelete('SET NULL');
        
            $table->foreign('updated_by')
                ->references('id')->on('pegawais')
                ->onDelete('SET NULL');
        });
    }
}
