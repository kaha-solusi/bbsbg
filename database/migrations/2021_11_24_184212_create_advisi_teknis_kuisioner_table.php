<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvisiTeknisKuisionerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advisi_teknis_kuisioners', function (Blueprint $table) {
            $table->id();
            $table->foreignId('advisi_teknis_id')->constrained();
            $table->foreignId('kuisioner_id')->constrained();
            $table->string('score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advisi_teknis_kuisioners');
    }
}
