<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterLayananUjiItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'master_layanan_uji_items', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->string('price');
                $table->string('working_days');
                $table->foreignId('master_layanan_uji_id')->constrained()
                    ->onDelete('CASCADE');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_layanan_uji_items');
    }
}
