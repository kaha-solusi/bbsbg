<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvisiTeknisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advisi_teknis', function (Blueprint $table) {
            $table->id();
            $table->date('test_date');
            $table->string('scope');
            $table->string('building_identity');
            $table->string('location');
            $table->text('problem');
            $table->string('support_file');
            $table->string('status');

            $table->foreignId('client_id')
                ->nullable()
                ->constrained()
                ->onDelete('SET NULL')
                ->onUpdate('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advisi_teknis');
    }
}
