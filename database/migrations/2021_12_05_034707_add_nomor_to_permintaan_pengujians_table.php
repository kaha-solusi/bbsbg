<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNomorToPermintaanPengujiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujians', function (Blueprint $table) {
            $table->string('nomor_permintaan')->after('tgl_permintaan')->nullable();
            $table->string('tanggal_pelayanan')->after('tgl_permintaan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujians', function (Blueprint $table) {
            $table->dropColumn('nomor_permintaan');
            $table->dropColumn('tanggal_pelayanan');
        });
    }
}
