<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdPasscodeZoomToBimbinganTeknisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bimbingan_teknis', function (Blueprint $table) {
            $table->string('meeting_id')->nullable();
            $table->string('passcode')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bimbingan_teknis', function (Blueprint $table) {
            $table->dropColumn('meeting_id');
            $table->dropColumn('passcode');
        });
    }
}
