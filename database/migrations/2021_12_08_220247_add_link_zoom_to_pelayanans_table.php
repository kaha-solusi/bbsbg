<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLinkZoomToPelayanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pelayanans', function (Blueprint $table) {
            $table->text('syarat_peserta');
            $table->string('link_zoom')->nullable();
            $table->string('meeting_id')->nullable();
            $table->string('passcode')->nullable();
            $table->string('subjek_email')->nullable();
            $table->text('konten_email')->nullable();
            $table->text('background')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pelayanans', function (Blueprint $table) {
            $table->dropColumn('syarat_peserta');
            $table->dropColumn('link_zoom');
            $table->dropColumn('meeting_id');
            $table->dropColumn('passcode');
            $table->dropColumn('subjek_email');
            $table->dropColumn('konten_email');
            $table->dropColumn('background');
        });
    }
}
