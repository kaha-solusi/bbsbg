<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKegiatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kegiatans', function (Blueprint $table) {
            $table->id();
            $table->string('waktu');
            $table->foreignId('lab_id')->nullable()->constrained('labs')->onDelete('cascade');
            $table->string('nama_kegiatan');
            $table->text('deskripsi_kegiatan');
            $table->string('waktu_pelaksanaan');
            $table->text('tempat_pelaksanaan');
            $table->string('biaya_kegiatan');
            $table->enum('status', ['diproses', 'dilaksanakan', 'tidak dilaksanakan', 'sedang dilaksanakan', 'selesai']);
            $table->enum('kategori', ['bimtek', 'pengujian']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kegiatans');
    }
}
