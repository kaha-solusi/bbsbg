<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianPaymentConfirmationApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujian_payment_confirmation_approvals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('permintaan_pengujian_payment_confirmation_id');
            $table->foreign('permintaan_pengujian_payment_confirmation_id', 'fk_payment_confirmation_approval')
                ->references('id')
                ->on('permintaan_pengujian_payment_confirmations')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->string('status');
            $table->string('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_payment_confirmation_approvals');
    }
}
