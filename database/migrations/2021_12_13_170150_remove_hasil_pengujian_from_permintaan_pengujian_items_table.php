<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveHasilPengujianFromPermintaanPengujianItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujian_items', function (Blueprint $table) {
            $table->dropColumn('hasil_pengujian');
            $table->dropColumn('no_sample');
            $table->dropColumn('jenis_pengujian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujian_items', function (Blueprint $table) {
            //
        });
    }
}
