<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianKajiUlangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujian_kaji_ulangs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('permintaan_pengujian_id')
                ->constrained()
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->json('parameter');
            $table->boolean('subkontraktor');
            $table->string('nama_subkontraktor')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('durasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_kaji_ulangs');
    }
}
