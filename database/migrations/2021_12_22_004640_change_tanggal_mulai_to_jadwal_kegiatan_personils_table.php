<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTanggalMulaiToJadwalKegiatanPersonilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jadwal_kegiatan_personils', function (Blueprint $table) {
            $table->date('tanggal_mulai')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jadwal_kegiatan_personils', function (Blueprint $table) {
            $table->string('tanggal_mulai')->change();
        });
    }
}
