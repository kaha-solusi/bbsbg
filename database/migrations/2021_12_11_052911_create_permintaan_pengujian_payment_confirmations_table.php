<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianPaymentConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujian_payment_confirmations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('permintaan_pengujian_payment_id');
            $table->foreign('permintaan_pengujian_payment_id', 'fk_pp_payment_confirmation')
                ->references('id')
                ->on('permintaan_pengujian_payments')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->string('bank_name');
            $table->string('account_number');
            $table->string('account_holder');
            $table->string('payment_amount');
            $table->string('payment_date');
            $table->string('status');
            $table->foreignId('file_id')->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_payment_confirmations');
    }
}
