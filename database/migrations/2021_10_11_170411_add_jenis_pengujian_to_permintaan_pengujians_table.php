<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJenisPengujianToPermintaanPengujiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujians', function (Blueprint $table) {
            $table->foreignId('pelayanan_id')
                ->nullable()
                ->after('client_id')
                ->constrained()
                ->onDelete('SET NULL')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujians', function (Blueprint $table) {
            $table->dropForeign(['pelayanan_id']);
            $table->dropColumn('pelayanan_id');
        });
    }
}
