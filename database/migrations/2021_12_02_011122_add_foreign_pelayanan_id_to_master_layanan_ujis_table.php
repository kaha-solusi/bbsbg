<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignPelayananIdToMasterLayananUjisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_layanan_ujis', function (Blueprint $table) {
            $table->bigInteger('pelayanan_id')->unsigned()->change();

            $table->foreign('pelayanan_id')
                ->references('id')
                ->on('pelayanans')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_layanan_ujis', function (Blueprint $table) {
            $table->dropForeign(['pelayanan_id']);
            $table->bigInteger('pelayanan_id')->unsigned(false)->change();
        });
    }
}
