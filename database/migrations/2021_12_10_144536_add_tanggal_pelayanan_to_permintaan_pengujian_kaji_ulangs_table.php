<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTanggalPelayananToPermintaanPengujianKajiUlangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujian_kaji_ulangs', function (Blueprint $table) {
            $table->date('tanggal_pelayanan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujian_kaji_ulangs', function (Blueprint $table) {
            $table->dropColumn('tanggal_pelayanan');
        });
    }
}
