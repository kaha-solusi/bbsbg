<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianPengujianItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujian_pengujian_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('permintaan_pengujian_pengujian_id');
            $table->foreign('permintaan_pengujian_pengujian_id', 'fk_permintaan_proses_pengujian')
                ->references('id')
                ->on('permintaan_pengujian_pengujians')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->unsignedBigInteger('item_id');
            $table->foreign('item_id')
                ->references('id')
                ->on('permintaan_pengujian_items')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->json('hasil_pengujian');
            $table->string('kategori_pengujian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_pengujian_items');
    }
}
