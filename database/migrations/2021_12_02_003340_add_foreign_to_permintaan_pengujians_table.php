<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToPermintaanPengujiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujians', function (Blueprint $table) {
            // $table->dropColumn('master_layanan_id');
            $table->unsignedBigInteger('master_layanan_uji_id')->nullable();
            $table->foreign('master_layanan_uji_id')
                ->references('id')
                ->on('master_layanan_ujis')
                ->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujians', function (Blueprint $table) {
            $table->bigInteger('master_layanan_id')->unsigned();
            $table->dropForeign(['master_layanan_uji_id']);
        });
    }
}
