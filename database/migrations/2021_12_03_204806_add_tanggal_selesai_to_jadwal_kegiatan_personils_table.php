<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTanggalSelesaiToJadwalKegiatanPersonilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jadwal_kegiatan_personils', function (Blueprint $table) {
            $table->date('tanggal_selesai')->nullable();
            $table->date('jadwal')->change();
            $table->renameColumn('jadwal','tanggal_mulai');

            $table->dropColumn('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jadwal_kegiatan_personils', function (Blueprint $table) {
            $table->dropColumn('jadwal_selesai');
            $table->renameColumn('tanggal_mulai','jadwal');
            $table->string('jadwal')->change();
            $table->string('status');
        });
    }
}
