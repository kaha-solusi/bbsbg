<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBimbinganTeknisPesertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bimbingan_teknis_peserta', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('bimtek_id')->unsigned();
            $table->foreign('bimtek_id')
                ->references('id')
                ->on('bimbingan_teknis')
                ->onDelete('CASCADE');

            $table->string('nama');
            $table->string('nip_nik');
            $table->string('instansi')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('no_hp');
            $table->string('email');
            $table->text('alamat')->nullable();
            $table->string('status')->nullable();
            $table->string('link_video_conf')->nullable();
            $table->string('link_certificate')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bimbingan_teknis_peserta');
    }
}
