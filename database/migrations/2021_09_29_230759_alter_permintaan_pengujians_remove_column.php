<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPermintaanPengujiansRemoveColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'permintaan_pengujians', function (Blueprint $table) {
                $table->string('company_email')->nullable();
                $table->string('company_address')->nullable();
                $table->string('company_phone')->nullable();
                $table->string('company_name')->nullable();
                $table->string('tanggal_pengujian')->nullable()->change();

                
                $table->renameColumn('nomor_hp_kontak', 'nomor');
                
                $table->dropForeign(['pelayanan_id']);
                $table->dropColumn(['uraian_pengujian', 'email_persuratan', 'detail_layanan', 'nama_lhu', 'nama_kontak', 'pelayanan_id']);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'permintaan_pengujians', function (Blueprint $table) {
                $table->dropColumn('company_email');
                $table->dropColumn('company_address');
                $table->dropColumn('company_phone');
                $table->dropColumn('company_email');

                $table->text('uraian_pengujian');
                $table->text('email_persuratan');
                $table->text('detail_layanan');
                $table->string('nama_lhu')->nullable();
                $table->string('tanggal_pengujian')->change();

                $table->renameColumn('nomor', 'nomor_hp_kontak');

                $table->foreignId('pelayanan_id')
                    ->constrained()
                    ->onUpdate('restrict')
                    ->onDelete('cascade');
            }
        );
    }
}
