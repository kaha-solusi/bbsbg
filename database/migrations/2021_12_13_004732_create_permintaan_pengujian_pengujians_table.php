<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianPengujiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujian_pengujians', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('permintaan_pengujian_id');
            $table->foreign('permintaan_pengujian_id')
                ->references('id')
                ->on('permintaan_pengujians')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_pengujians');
    }
}
