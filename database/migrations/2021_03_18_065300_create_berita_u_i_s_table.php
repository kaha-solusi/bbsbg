<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeritaUISTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('berita_u_i_s', function (Blueprint $table) {
            $table->id();
            $table->integer('jumlah_berita')->nullable();
            $table->foreignId('berita_1_id')->nullable();
            $table->foreignId('berita_2_id')->nullable();
            $table->foreignId('berita_3_id')->nullable();
            $table->foreignId('berita_4_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('berita_u_i_s');
    }
}
