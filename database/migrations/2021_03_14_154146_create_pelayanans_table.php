<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePelayanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelayanans', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
            $table->foreignId('kategori_id')->nullable()->constrained('kategori_pelayanans')->onDelete('set null');
            $table->text('file')->nullable();
            $table->string('file_sop')->nullable();
            $table->string('file_panduan')->nullable();
            $table->string('file_biaya')->nullable();
            $table->longText('text')->nullable();
            $table->string('foto')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelayanans');
    }
}
