<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujian_confirmations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('permintaan_pengujian_id')->unsigned();
            $table->foreign('permintaan_pengujian_id', 'permintaan_pengujian_confirmations_foreign')
                ->references('id')
                ->on('permintaan_pengujians')
                ->constrained()
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->string('bank_name');
            $table->string('account_number');
            $table->string('account_holder');
            $table->string('payment_amount');
            $table->string('payment_date');
            $table->foreignId('file_id')
                ->nullable()
                ->constrained()
                ->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_confirmations');
    }
}
