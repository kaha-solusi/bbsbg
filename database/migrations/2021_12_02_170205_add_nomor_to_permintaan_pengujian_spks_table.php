<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNomorToPermintaanPengujianSpksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujian_spks', function (Blueprint $table) {
            $table->string('nomor')->after('permintaan_pengujian_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujian_spks', function (Blueprint $table) {
            $table->dropColumn('nomor');
        });
    }
}
