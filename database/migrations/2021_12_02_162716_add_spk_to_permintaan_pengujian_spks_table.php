<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSpkToPermintaanPengujianSpksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujian_spks', function (Blueprint $table) {
            $table->string('metode_uji');
            $table->string('tanggal_mulai');
            $table->string('tanggal_selesai');
            $table->string('keterangan');
        });

        Schema::dropIfExists('spks');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujian_spks', function (Blueprint $table) {
            $table->dropColumn('metode_uji');
            $table->dropColumn('tanggal_mulai');
            $table->dropColumn('tanggal_selesai');
            $table->dropColumn('keterangan');
        });

        Schema::create('spks', function (Blueprint $table) {
            $table->id();
            $table->string('metode_uji');
            $table->string('tanggal_mulai');
            $table->string('tanggal_selesai');
            $table->string('keterangan');
            $table->timestamps();
        });
    }
}
