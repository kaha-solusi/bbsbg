<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTableAlurPelayanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alur_pelayanans', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->text('text')->nullable();
            $table->string('img')->nullable();
            $table->string('pdf')->nullable();
            $table->boolean('status_img')->default(0);
            $table->boolean('status_pdf')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alur_pelayanans');
    }
}
