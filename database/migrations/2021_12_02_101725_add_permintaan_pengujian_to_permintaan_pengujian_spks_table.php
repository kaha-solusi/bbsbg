<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPermintaanPengujianToPermintaanPengujianSpksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujian_spks', function (Blueprint $table) {
            $table->bigInteger('permintaan_pengujian_sample_id')->unsigned()->nullable();
            $table->foreign('permintaan_pengujian_sample_id', 'permintaan_pengujian_sample_foreign')
                ->references('id')
                ->on('permintaan_pengujian_samples')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujian_spks', function (Blueprint $table) {
            $table->dropForeign('permintaan_pengujian_sample_foreign');
            $table->dropColumn('permintaan_pengujian_sample_id');
        });
    }
}
