<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLayananIdToKuisionersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kuisioners', function (Blueprint $table) {
            $table->unsignedBigInteger('kategori_pelayanan_id')->nullable();
            $table->foreign('kategori_pelayanan_id')->references('id')->on('kategori_pelayanans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kuisioners', function (Blueprint $table) {
            $table->dropForeign(['kategori_pelayanan_id']);
            $table->dropColumn('kategori_pelayanan_id');
        });
    }
}
