<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvisiTeknisApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advisi_teknis_approvals', function (Blueprint $table) {
            $table->id();

            $table->foreignId('advisi_teknis_id')
                ->nullable()
                ->constrained()
                ->onDelete('SET NULL')
                ->onUpdate('CASCADE');

            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->bigInteger('deleted_by')->unsigned()->nullable();

            $table->string('status');
            $table->text('reason')->nullable();
            
            $table->foreign('created_by')
                    ->references('id')->on('users')
                    ->onDelete('SET NULL');
        
            $table->foreign('updated_by')
                ->references('id')->on('users')
                ->onDelete('SET NULL');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advisi_teknis_approvals');
    }
}
