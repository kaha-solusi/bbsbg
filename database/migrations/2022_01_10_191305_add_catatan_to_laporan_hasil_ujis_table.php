<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCatatanToLaporanHasilUjisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laporan_hasil_ujis', function (Blueprint $table) {
            $table->string('catatan')->nullable();
            $table->string('file')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laporan_hasil_ujis', function (Blueprint $table) {
            $table->dropColumn('catatan');
            $table->dropColumn('file');
        });
    }
}
