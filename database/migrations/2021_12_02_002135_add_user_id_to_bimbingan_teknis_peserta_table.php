<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdToBimbinganTeknisPesertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bimbingan_teknis_peserta', function (Blueprint $table) {
            $table->dropColumn('nama');
            $table->dropColumn('nip_nik');
            $table->dropColumn('instansi');
            $table->dropColumn('jabatan');
            $table->dropColumn('no_hp');
            $table->dropColumn('email');
            $table->dropColumn('alamat');
            $table->dropColumn('status');
            $table->dropColumn('link_video_conf');
            $table->dropColumn('link_certificate');
            $table->unsignedBigInteger('user_id')->nullable();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bimbingan_teknis_peserta', function (Blueprint $table) {
            // $table->dropColumn('user_id');
            $table->string('nama');
            $table->string('nip_nik');
            $table->string('instansi');
            $table->string('jabatan');
            $table->string('no_hp');
            $table->string('email');
            $table->text('alamat');
            $table->string('status');
            $table->string('link_video_conf');
            $table->string('link_certificate');
        });
    }
}
