<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToPermintaanPengujianPengujianItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujian_pengujian_items', function (Blueprint $table) {
            $table->string('status')->after('kategori_pengujian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujian_pengujian_items', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
