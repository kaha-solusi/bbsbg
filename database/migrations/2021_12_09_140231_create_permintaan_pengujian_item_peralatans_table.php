<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianItemPeralatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujian_item_peralatans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('permintaan_pengujian_item_id');
            $table->foreign('permintaan_pengujian_item_id', 'fk_pengujian_item')
                ->references('id')
                ->on('permintaan_pengujian_items')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->unsignedBigInteger('peralatan_lab_id');
            $table->foreign('peralatan_lab_id', 'fk_peralatan_peralatan_lab')
                ->references('id')
                ->on('peralatan_labs')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_item_peralatans');
    }
}
