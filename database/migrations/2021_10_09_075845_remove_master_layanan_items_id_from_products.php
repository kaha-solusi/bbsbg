<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveMasterLayananItemsIdFromProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['master_layanan_uji_item_id']);
            $table->dropColumn('master_layanan_uji_item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->foreignId('master_layanan_uji_item_id')
                ->constrained()
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');
        });
    }
}
