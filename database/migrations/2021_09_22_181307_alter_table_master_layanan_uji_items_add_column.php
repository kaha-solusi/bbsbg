<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableMasterLayananUjiItemsAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'master_layanan_uji_items', function (Blueprint $table) {
                $table->string('standard')->after('working_days');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'master_layanan_uji_items', function (Blueprint $table) {
                $table->dropColumn('standard');
            }
        );
    }
}
