<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujian_payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('permintaan_pengujian_id');
            $table->foreign('permintaan_pengujian_id')
                ->references('id')
                ->on('permintaan_pengujians')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->string('billing_code');
            $table->string('status');
            $table->unsignedBigInteger('file_id')->nullable();
            $table->foreign('file_id')
                ->references('id')
                ->on('files')
                ->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_payments');
    }
}
