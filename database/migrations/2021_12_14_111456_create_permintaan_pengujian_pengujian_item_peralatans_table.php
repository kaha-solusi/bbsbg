<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianPengujianItemPeralatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujian_pengujian_item_peralatans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pengujian_item_id');
            $table->foreign('pengujian_item_id', 'fk_item_pengujian')
                ->references('id')
                ->on('permintaan_pengujian_pengujian_items')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->unsignedBigInteger('peralatan_id');
            $table->foreign('peralatan_id', 'fk_peralatan_item_pengujian')
                ->references('id')
                ->on('peralatan_labs')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_pengujian_item_peralatans');
    }
}
