<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeralatanLabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peralatan_labs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('lab_id')->nullable()->constrained('labs')->onDelete('cascade');
            $table->string('nama');
            $table->string('merek');
            $table->string('type');
            $table->string('seri');
            $table->text('spesifikasi')->nullable();
            $table->string('no_bmn')->nullable();
            $table->text('fungsi_alat')->nullable();
            $table->string('tahun_pengadaan')->nullable();
            $table->integer('jumlah')->nullable();
            $table->string('satuan')->nullable();
            $table->text('keterangan_kapasitas')->nullable();
            $table->string('kondisi')->nullable();
            $table->string('foto_alat')->nullable();
            $table->string('tanggal_terakhir_kalibrasi')->nullable();
            $table->string('lembaga_pengujian_kalibrasi')->nullable();
            $table->string('penanggung_jawab')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peralatan_labs');
    }
}
