<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIndeksKepuasansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indeks_kepuasans', function (Blueprint $table) {
            $table->id();
            $table->year('tahun');
            $table->bigInteger('jumlah_responden');
            $table->float('nilai_ikm', 8, 2);
            $table->float('indexs', 8, 2);
            $table->enum('nilai_mutu', ['A', 'B', 'C', 'D', 'E']);
            $table->enum('predikat', ['Sangat Puas', 'Puas', 'Biasa', 'Tidak Puas', 'Sangat Tidak Puas']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indeks_kepuasans');
    }
}
