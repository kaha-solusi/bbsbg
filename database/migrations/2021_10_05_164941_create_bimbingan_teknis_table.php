<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBimbinganTeknisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bimbingan_teknis', function (Blueprint $table) {
            $table->id();
            $table->string('nama_bimtek');
            $table->string('jenis_bimtek')->nullable();
            $table->text('deskripsi_bimtek')->nullable();
            $table->string('tahun_pelaksanaan')->nullable();
            $table->string('bulan_pelaksanaan')->nullable();
            $table->date('tanggal_pelaksanaan')->nullable();
            $table->string('waktu_pelaksanaan');
            $table->string('status_pelaksanaan');
            $table->text('syarat_peserta');
            $table->string('link_zoom')->nullable();
            $table->string('link_certificate')->nullable();
            $table->string('bimtek_subjek_email')->nullable();
            $table->text('bimtek_konten_email')->nullable();
            $table->text('bimtek_background')->nullable();

            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->bigInteger('deleted_by')->unsigned()->nullable();

            $table->foreign('created_by')
                ->references('id')->on('users')
                ->onDelete('SET NULL');

            $table->foreign('updated_by')
                ->references('id')->on('users')
                ->onDelete('SET NULL');

            $table->foreign('deleted_by')
                ->references('id')->on('users')
                ->onDelete('SET NULL');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bimbingan_teknis');
    }
}
