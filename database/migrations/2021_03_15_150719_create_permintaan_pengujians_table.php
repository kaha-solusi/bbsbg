<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujians', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_id')->constrained('clients')->onDelete('cascade');
            $table->string('tgl_permintaan');
            $table->string('nama_pemohon');
            $table->text('alamat');
            $table->string('nama_kontak');
            $table->string('nomor_hp_kontak');
            $table->string('email');
            $table->foreignId('pelayanan_id')->constrained('pelayanans')->onDelete('cascade');
            $table->string('nama_lhu')->nullable();
            $table->string('email_persuratan');
            $table->string('tanggal_pengujian');
            $table->text('detail_layanan');
            $table->text('uraian_pengujian');
            $table->enum('status', ['diproses', 'diterima', 'ditolak', 'selesai']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujians');
    }
}
