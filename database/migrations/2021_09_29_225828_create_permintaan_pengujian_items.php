<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'permintaan_pengujian_items', function (Blueprint $table) {
                $table->id();
                $table->string('product_name');
                $table->string('product_type');
                $table->string('product_count');

                $table->foreignId('master_layanan_uji_item_id')
                    ->constrained()
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
                $table->foreignId('permintaan_pengujian_id')
                    ->constrained()
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_items');
    }
}
