<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianSpkHasilPengujiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujian_spk_hasil_pengujians', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('permintaan_pengujian_spk_id');
            $table->foreign('permintaan_pengujian_spk_id', 'permintaan_pengujian_spk_hasil_foreign')
                ->references('id')
                ->on('permintaan_pengujian_spks')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->string('no_benda_uji');
            $table->json('hasil_pengujian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_spk_hasil_pengujians');
    }
}
