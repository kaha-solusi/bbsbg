<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPdfAndImgToTugasFungsi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tugas_fungsis', function (Blueprint $table) {
            $table->string('url_pdf')->nullable()->default(null);
            $table->string('url_img')->nullable()->default(null);
            $table->tinyInteger('status_pdf')->default(0);
            $table->tinyInteger('status_img')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tugas_fungsis', function (Blueprint $table) {
            //
        });
    }
}
