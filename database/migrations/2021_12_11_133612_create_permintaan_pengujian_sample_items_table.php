<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianSampleItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujian_sample_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('permintaan_pengujian_sample_id');
            $table->foreign('permintaan_pengujian_sample_id', 'fk_sample_item_id')
                ->references('id')
                ->on('permintaan_pengujian_samples')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->unsignedBigInteger('sample_id');
            $table->foreign('sample_id', 'fk_sample_item_sample_id')
                ->references('id')
                ->on('permintaan_pengujian_items')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->string('kode_sampel');
            $table->string('status');
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_sample_items');
    }
}
