<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAdvisiTeknisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('advisi_teknis', function (Blueprint $table) {
            $table->string('link')->after('client_id')->nullable();
            $table->string('letter_of_introduction')->after('link')->nullable();
            $table->string('news_report')->after('letter_of_introduction')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advisi_teknis', function (Blueprint $table) {
            $table->dropColumn('link');
            $table->dropColumn('letter_of_introduction');
            $table->dropColumn('news_report');
        });
    }
}
