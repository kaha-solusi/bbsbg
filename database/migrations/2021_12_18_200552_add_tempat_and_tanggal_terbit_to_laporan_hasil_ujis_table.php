<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTempatAndTanggalTerbitToLaporanHasilUjisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laporan_hasil_ujis', function (Blueprint $table) {
            $table->string('tempat', 100)->nullable();
            $table->string('tanggal_terbit', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laporan_hasil_ujis', function (Blueprint $table) {
            $table->dropColumn('tempat');
            $table->dropColumn('tanggal_terbit');
        });
    }
}
