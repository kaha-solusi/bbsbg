<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNoSampleToPermintaanPengujianItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pengujian_items', function (Blueprint $table) {
            $table->string('no_sample')->nullable();
            $table->string('status')->nullable();  
            $table->string('keterangan')->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pengujian_items', function (Blueprint $table) {
            $table->dropColumn('no_sample');
            $table->dropColumn('status');
            $table->dropColumn('keterangan');
        });
    }
}
