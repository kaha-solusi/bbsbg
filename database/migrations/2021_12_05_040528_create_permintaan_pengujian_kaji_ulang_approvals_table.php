<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPengujianKajiUlangApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_pengujian_kaji_ulang_approvals', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('permintaan_pengujian_kaji_ulang_id')->unsigned();
            $table->foreign('permintaan_pengujian_kaji_ulang_id', 'kaji_ulang_permintaan_foreign')
                ->references('id')
                ->on('permintaan_pengujian_kaji_ulangs')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->string('status', 20);

            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();

            $table->text('reason')->nullable();
            
            $table->foreign('created_by')
                    ->references('id')
                    ->on('users')
                    ->onDelete('SET NULL');
        
            $table->foreign('updated_by')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pengujian_kaji_ulang_approvals');
    }
}
