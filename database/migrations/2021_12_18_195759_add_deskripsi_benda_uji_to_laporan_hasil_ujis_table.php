<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeskripsiBendaUjiToLaporanHasilUjisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laporan_hasil_ujis', function (Blueprint $table) {
            $table->mediumText('deskripsi_benda_uji')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laporan_hasil_ujis', function (Blueprint $table) {
            $table->dropColumn('deskripsi_benda_uji');
        });
    }
}
