<?php

namespace App\Notifications;

use App\Models\PermintaanPengujian;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PermintaanPengujianReceivedNotification extends Notification implements ShouldQueue
{
    use Queueable;
    
    private $pengujian;
    public $name;
    public $products;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(PermintaanPengujian $pengujian, array $products)
    {
        $this->pengujian = $pengujian;
        $this->products = $products;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Permintaan Pengujian Baru')
                    ->markdown('mail.pengujian.received', [
                        'products' => $this->products,
                        'name' => $this->name,
                    ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
