<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PermintaanPengujianRegisteredNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $nama;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(string $nama)
    {
        $this->nama = $nama;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Permintaan Pengujian')
                    ->greeting('Hai, '.$this->nama)
                    ->line('Permintaan pengujian anda sedang dilakukan verifikasi oleh tim kami')
                    ->line('Silahkan menunggu pemberitahuan lebih lanjut mengenai jadwal pengujian pada email anda');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
