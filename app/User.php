<?php

namespace App;

use App\Models\AdvisiTeknis;
use App\Models\Bimtek;
use App\Models\Client;
use App\Models\Pegawai;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable implements MustVerifyEmail
{

    use Notifiable, HasRoles, LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'username',
        'level',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static $logFillable = true;

    /**
     * User has many Log_aktivitas.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function log_aktivitas()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = user_id, localKey = id)
        return $this->hasMany('App\Models\LogAktivitas');
    }

    /**
     * User has many Client.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function client()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = user_id, localKey = id)
        return $this->hasOne(Client::class);
    }

    /**
     * User has one pegawai.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pegawai()
    {
        return $this->hasOne(Pegawai::class);
    }

    /**
     * User has many Kepuasan_pelanggan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kepuasan_pelanggan()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = user_id, localKey = id)
        return $this->hasMany('App\Models\KepuasanPelanggan');
    }

    public function bimtek()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = user_id, localKey = id)
        return $this->belongsToMany(Bimtek::class, 'bimbingan_teknis_peserta', 'users_id', 'bimtek_id');
    }

    public function advisi_teknis_approval()
    {
        return $this->belongsToMany(AdvisiTeknis::class, 'advisi_teknis_approvals', 'advisi_teknis_id', 'id');
    }

    public function advisi_teknis_members()
    {
        return $this->belongsToMany(AdvisiTeknis::class, 'advisi_teknis_members', 'advisi_teknis_id', 'id');
    }
}
