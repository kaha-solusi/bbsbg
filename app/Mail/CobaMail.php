<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CobaMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subjek;
    public $content;
    public $background;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $content, $background)
    {
        $this->subjek = $subject;
        $this->content = $content;
        $this->background = $background;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
            ->with(
                [
                'password' => $this->subjek
                ]
            )
            ->view('mail.akun');
    }
}
