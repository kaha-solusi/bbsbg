<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PermintaanPengujian extends Mailable
{
    use Queueable, SerializesModels;

    public $date;
    public $jenis;
    public $pemohon;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($date, $jenis, $pemohon, $email)
    {
        $this->date = $date;
        $this->jenis = $jenis;
        $this->pemohon = $pemohon;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.permintaan');
    }
}
