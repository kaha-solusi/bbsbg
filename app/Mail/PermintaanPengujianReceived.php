<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PermintaanPengujianReceived extends Mailable
{
    use Queueable, SerializesModels;

    private $nama;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $nama)
    {
        $this->nama = $nama;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Permintaan Pengujian')
            ->with([
                'nama' => $this->nama,
            ])
            ->view('mail.permintaan-pengujian');
    }
}
