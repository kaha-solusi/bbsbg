<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PermintaanStatus extends Mailable
{
    use Queueable, SerializesModels;

    public $id;
    public $tgl_permintaan;
    public $nama_pemohon;
    public $status;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id, $tgl_permintaan, $nama_pemohon, $status)
    {
        $this->id = $id;
        $this->tgl_permintaan = $tgl_permintaan;
        $this->nama_pemohon = $nama_pemohon;
        $this->status = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.status');
    }
}
