<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PengaduanMasyarakatMail extends Mailable
{
    use Queueable, SerializesModels;

    public $judul;
    public $isi;
    public $tanggal;
    public $lokasi;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($judul, $isi, $tanggal, $lokasi)
    {
        $this->judul = $judul;
        $this->isi = $isi;
        $this->tanggal = $tanggal;
        $this->lokasi = $lokasi;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.pengaduan_masyarakat');
    }
}
