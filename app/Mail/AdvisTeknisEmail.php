<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdvisTeknisEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $nama;
    public $test_date;
    public $time;
    public $scope;
    public $building_identity;
    public $problem;
    public $syarat_peserta;
    public $link_zoom;
    public $meeting_id;
    public $passcode;
    public $deskripsi;
    public $subjek;
    public $konten;
    public $background;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        $nama,
        $test_date,
        $time,
        $scope,
        $building_identity,
        $problem,
        $syarat_peserta,
        $link_zoom,
        $meeting_id,
        $passcode,
        $subjek,
        $konten
    )
    {
        $this->nama = $nama;
        $this->test_date = $test_date;
        $this->time = $time;
        $this->scope = $scope;
        $this->building_identity = $building_identity;
        $this->problem = $problem;
        $this->syarat_peserta = $syarat_peserta;
        $this->link_zoom = $link_zoom;
        $this->meeting_id = $meeting_id;
        $this->passcode = $passcode;
        $this->subjek = $subjek;
        $this->konten = $konten;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('pengirim@bbsbg.com')
            ->subject($this->subjek)
            ->view('mail.advis_teknis')
            ->with(
                [
                    'nama'   => $this->nama,
                    'test_date'   => $this->test_date,
                    'time'   => $this->time,
                    'scope'   => $this->scope,
                    'building_identity' => $this->building_identity,
                    'problem' => $this->problem,
                    'syarat_peserta'   => $this->syarat_peserta,
                    'link_zoom'   => $this->link_zoom,
                    'meeting_id'     => $this->meeting_id,
                    'passcode'    => $this->passcode,
                    'konten' => $this->konten,
                ]
            );
    }
}
