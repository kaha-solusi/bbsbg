<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PesertaBimtekEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $nama;
    public $nama_bimtek;
    public $tanggal_bimtek;
    public $waktu_bimtek;
    public $syarat_bimtek;
    public $link_zoom;
    public $meeting_id;
    public $passcode;
    public $deskripsi;
    public $subjek;
    public $konten;
    public $background;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        $nama,
        $nama_bimtek,
        $tanggal_bimtek,
        $waktu_bimtek,
        $syarat_bimtek,
        $link_zoom,
        $meeting_id,
        $passcode,
        $deskripsi,
        $subjek,
        $konten,
        $background
    )
    {
        $this->nama = $nama;
        $this->nama_bimtek = $nama_bimtek;
        $this->tanggal_bimtek = $tanggal_bimtek;
        $this->waktu_bimtek = $waktu_bimtek;
        $this->syarat_bimtek = $syarat_bimtek;
        $this->link_zoom = $link_zoom;
        $this->meeting_id = $meeting_id;
        $this->passcode = $passcode;
        $this->deskripsi = $deskripsi;
        $this->subjek = $subjek;
        $this->konten = $konten;
        $this->background = $background;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('pengirim@bbsbg.com')
            ->subject($this->subjek)
            ->view('mail.pesertabimtek')
            ->with(
                [
                    'nama'   => $this->nama,
                    'nama_bimtek'   => $this->nama_bimtek,
                    'tanggal_bimtek'   => $this->tanggal_bimtek,
                    'waktu_bimtek'   => $this->waktu_bimtek,
                    'syarat_bimtek'   => $this->syarat_bimtek,
                    'link_zoom'   => $this->link_zoom,
                    'meeting_id'     => $this->meeting_id,
                    'passcode'    => $this->passcode,
                    'deskripsi'   => $this->deskripsi,
                    'konten' => $this->konten,
                ])
            ->attach(public_path($this->background), [
                'as' => 'background_zoom.jpg',
                'mime' => 'image/jpeg',
            ]);
    }
}
