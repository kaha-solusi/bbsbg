<?php

namespace App\Imports;

use App\Models\StrukturOrganisasi;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StrukturImport implements ToModel, WithHeadingRow
{
    use Importable;
    public function model(array $row)
    {
        return new StrukturOrganisasi([
            'nama' => $row['nama'],
            'nip' => $row['nip'],
            'jabatan' => $row['jabatan'],
            'email' => $row['email'],
            'keterangan' => $row['keterangan'],
        ]);
    }
}
