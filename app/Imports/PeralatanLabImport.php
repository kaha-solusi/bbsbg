<?php

namespace App\Imports;

use App\Models\PeralatanLab;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Lab;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;

class PeralatanLabImport implements WithHeadingRow, SkipsOnError, ToModel, WithBatchInserts
{
    use Importable, SkipsErrors;

    private $rowIndex = 2; // start form row 2
    
    public function model(array $row)
    {
        $tool = null;
        if (!is_null($row['nama_laboratorium'])) {
            $tool = new PeralatanLab([
                'lab_id' => Lab::where('nama', $row['nama_laboratorium'])->first()->id,
                'nama' => $row['nama_alat'],
                'merek' => $row['merek'],
                'type' => $row['tipe'],
                'seri' => $row['seri'],
                'no_bmn' => $row['nokode_bmn'],
                'keterangan_kapasitas' => $row['kapasitas'],
                'jumlah' => $row['jumlah'],
                'akurasi' => $row['akurasi'],
                'foto_alat' => $this->importImage($this->rowIndex),
            ]);
            $this->rowIndex++;
        }

        return $tool;
    }

    private function importImage($rowIndex)
    {
        $spreadsheet = IOFactory::load(request()->file('file'));
        $image = null;
        foreach ($spreadsheet->getActiveSheet()->getDrawingCollection() as $drawing) {
            if ($drawing->getCoordinates() === "J".$rowIndex) {
                if ($drawing instanceof MemoryDrawing) {
                    ob_start();
                    call_user_func(
                        $drawing->getRenderingFunction(),
                        $drawing->getImageResource()
                    );
                    $imageContents = ob_get_contents();
                    ob_end_clean();
                    switch ($drawing->getMimeType()) {
                        case MemoryDrawing::MIMETYPE_PNG:
                            $extension = 'png';
                            break;
                        case MemoryDrawing::MIMETYPE_GIF:
                            $extension = 'gif';
                            break;
                        case MemoryDrawing::MIMETYPE_JPEG:
                            $extension = 'jpg';
                            break;
                    }
                } else {
                    $zipReader = fopen($drawing->getPath(), 'r');
                    $imageContents = '';
                    while (!feof($zipReader)) {
                        $imageContents .= fread($zipReader, 1024);
                    }
                    fclose($zipReader);
                    $extension = $drawing->getExtension();
                }

                $myFileName = sha1(now() .$this->rowIndex). '.' . $extension;
                $pathname = '/uploads/foto_alat/';
                file_put_contents(base_path('public'.$pathname) . $myFileName, $imageContents);

                $image = $pathname . $myFileName;
                break;
            }
        }

        return $image;
    }

    public function onErrors()
    {
        //
    }

    public function batchSize(): int
    {
        return 100;
    }
}
