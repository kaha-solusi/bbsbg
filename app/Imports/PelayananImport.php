<?php

namespace App\Imports;

use App\Models\Pelayanan;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Throwable;

class PelayananImport implements ToModel, WithHeadingRow
{
    use Importable, SkipsErrors;
    public function __construct($kategori)
    {
        $this->kategori = $kategori;
        $this->path = 'pelayanan/'.strval($kategori)."/";
        // dd($this->path);
    }
    public function model(array $row)
    {
        if(!empty($row['nama']))
        {
            return new Pelayanan([
                'nama' => $row['nama'],
                'text' => $row['text'],
                'kategori_id' => $this->kategori,
                'file' => (!empty($row['file'])) ? $this->path.$row['file'] : null,
                'file_sop' => (!empty($row['file_sop'])) ? $this->path.$row['file_sop'] : null,
                'file_panduan' => (!empty($row['file_panduan'])) ? $this->path.$row['file_panduan'] : null,
                'file_biaya' => (!empty($row['file_biaya'])) ? $this->path.$row['file_biaya'] : null,
                'foto' => (!empty($row['foto'])) ? $this->path.$row['foto'] : null,
            ]);
        }
    }
    // public function onErrors(Throwable $e)
    // {
    //     //
    // }
}
