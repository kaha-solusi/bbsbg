<?php

namespace App\Imports;

use App\Models\Lab;
use App\Models\Pegawai;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Throwable;

class PegawaiImport implements ToModel, WithHeadingRow
{
    use Importable;
    public function model(array $row)
    {
        return new Pegawai([
            'nama' => $row['nama'],
            'nip' => $row['nip'],
            'lab_id' => Lab::findOrFail($row['lab_id'])->value('id'),
            'email' => $row['email'],
            'no_hp' => $row['no_hp'],
        ]);
    }
    // public function onError(Throwable $error)
    // {
    //     return back()->with('success', 'Import File Berhasil !');
    // }

}