<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndeksKepuasan extends Model
{
    // use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'indeks_kepuasans';
    protected $guarded = [];
}
