<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianPengujianItem extends Model
{
    use LogsActivity;
    protected $fillable = ['hasil_pengujian', 'kategori_pengujian', 'status', 'item_id'];

    public function pengujian()
    {
        return $this->belongsTo(PermintaanPengujianPengujian::class, 'permintaan_pengujian_pengujian_id');
    }

    public function peralatan()
    {
        return $this->hasMany(
            PermintaanPengujianPengujianItemPeralatan::class,
            'pengujian_item_id'
        );
    }
}
