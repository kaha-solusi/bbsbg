<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class PeralatanLab extends Model
{
    // use HasFactory;
    use BlameableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'peralatan_labs';
    protected $guarded = [];

    /**
     * PeralatanLab belongs to Lab.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lab()
    {
        // belongsTo(RelatedModel, foreignKey = lab_id, keyOnRelatedModel = id)
        return $this->belongsTo('App\Models\Lab');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(PeralatanLabCalibration::class);
    }

    public function usage()
    {
        return $this->hasMany(PeralatanLabUsage::class);
    }

    public function pengujian()
    {
        return $this->belongsToMany(
            PermintaanPengujianPengujian::class,
            'permintaan_pengujian_pengujian_item_peralatans',
            'peralatan_id',
            'pengujian_item_id'
        );
    }
}
