<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianFile extends Model
{
    use LogsActivity;
    protected $logFillable = true;
    protected $fillable = ['permintaan_pengujian_id', 'file_id', 'type'];

    public function dokumen()
    {
        return $this->belongsTo(File::class, 'file_id');
    }
}
