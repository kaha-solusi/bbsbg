<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermintaanPengujianSpkMember extends Model
{
    protected $fillable = ['pegawai_id'];
    public function permintaan_spk()
    {
        return $this->belongsTo(PermintaanPengujianSpk::class, 'permintaan_pengujian_spk_id');
    }
}
