<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
    // use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pekerjaans';
    protected $guarded = [];

    /**
     * Pekerjaan belongs to Client.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
    	// belongsTo(RelatedModel, foreignKey = client_id, keyOnRelatedModel = id)
    	return $this->belongsTo('App\Models\Client');
    }

    /**
     * Pekerjaan belongs to Lab.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lab()
    {
    	// belongsTo(RelatedModel, foreignKey = lab_id, keyOnRelatedModel = id)
    	return $this->belongsTo('App\Models\Lab');
    }
}
