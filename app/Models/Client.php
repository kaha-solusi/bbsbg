<?php

namespace App\Models;

use App\Models\AdvisiTeknis;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Client extends Model
{
    use LogsActivity;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clients';
    protected $guarded = [];

    protected static $logFillable = true;

    /**
     * Client has many Pekerjaan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pekerjaan()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = client_id, localKey = id)
        return $this->hasMany('App\Models\Pekerjaan');
    }

    /**
     * Client belongs to User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        // belongsTo(RelatedModel, foreignKey = user_id, keyOnRelatedModel = id)
        return $this->belongsTo('App\User');
    }

    /**
     * Client has many Permintaan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permintaan()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = client_id, localKey = id)
        return $this->hasMany('App\Models\PermintaanPengujian');
    }

    /**
     * Client has many Umpan_balik.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function umpan_balik()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = client_id, localKey = id)
        return $this->hasMany('App\Models\UmpanBalik');
    }

    public function advisi_teknis()
    {
        return $this->hasMacro(AdvisiTeknis::class);
    }

    public function unit_kerja()
    {
        return $this->belongsTo('App\Models\Instansi');
    }

    public function instansi()
    {
        return $this->belongsTo('App\Models\Instansi');
    }
}
