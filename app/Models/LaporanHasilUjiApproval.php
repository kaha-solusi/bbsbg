<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class LaporanHasilUjiApproval extends Model
{
    use LogsActivity, BlameableTrait;

    protected $fillable = ['status', 'message', 'created_by'];
    protected static $blameable = [
        'user' => User::class,
        'createdBy' => 'created_by',
        'updatedBy' => 'created_by',
    ];

    public function laporan()
    {
        return $this->belongsTo(LaporanHasilUji::class, 'laporan_hasil_uji_id');
    }
}
