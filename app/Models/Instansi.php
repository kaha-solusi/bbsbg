<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class Instansi extends Model
{
    use NodeTrait, BlameableTrait;

    protected $table = 'instansi';
    protected $fillable = [
        'nama_instansi',
    ];
}
