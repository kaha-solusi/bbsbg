<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Product extends Model
{
    use LogsActivity, BlameableTrait;
    protected static $logFillable = true;

    protected $fillable = [
        'name',
        'description',
    ];

    public function parameters()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = user_id, localKey = id)
        return $this->belongsToMany(MasterLayananUjiItem::class, 'product_has_items');
    }

    public function alat()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = product_id, localKey = id)
        return $this->hasMany(ProductAlatKerja::class);
    }

    public function bahan()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = product_id, localKey = id)
        return $this->hasMany(ProductBahanKerja::class);
    }
}
