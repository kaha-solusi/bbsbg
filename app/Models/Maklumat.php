<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Maklumat extends Model
{
    // use HasFactory;
    protected $table = 'maklumats';
    protected $guarded = [];
}
