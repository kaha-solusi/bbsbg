<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianPengujianItemPeralatan extends Model
{
    use LogsActivity;
    protected $fillable = ['pengujian_item_id', 'peralatan_id'];

    public function peralatan_lab()
    {
        return $this->belongsTo(PeralatanLab::class, 'peralatan_id');
    }
}
