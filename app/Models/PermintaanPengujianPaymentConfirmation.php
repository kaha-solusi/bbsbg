<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermintaanPengujianPaymentConfirmation extends Model
{
    protected $fillable = [
        'bank_name',
        'account_number',
        'account_holder',
        'payment_amount',
        'payment_date',
        'status',
        'file_id',
    ];
    protected $casts = [
        'payment_date' => 'date:d-m-Y',
    ];

    public function payment()
    {
        return $this->belongsTo(PermintaanPengujianPayment::class);
    }

    public function file()
    {
        return $this->belongsTo(File::class);
    }

    public function approval()
    {
        return $this->hasMany(PermintaanPengujianPaymentConfirmationApproval::class);
    }
}
