<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class PeralatanLabCalibration extends Model
{
    use BlameableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'peralatan_lab_calibration_histories';
    protected $guarded = [];

    /**
     * PeralatanLab belongs to Lab.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function peralatan()
    {
        // belongsTo(RelatedModel, foreignKey = lab_id, keyOnRelatedModel = id)
        return $this->belongsTo('App\Models\Lab');
    }
}
