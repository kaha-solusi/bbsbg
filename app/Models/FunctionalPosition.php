<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FunctionalPosition extends Model
{
    protected $fillable = ['name'];

    /**
     * FunctionalPosition has many Pegawai.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pegawai()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = lab_id, localKey = id)
        return $this->hasMany(Pegawai::class);
    }
}
