<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AdvisiTeknisFile extends Model
{
    use LogsActivity;

    protected $fillable = ['file_name', 'file_path', 'advisi_teknis_id'];
}
