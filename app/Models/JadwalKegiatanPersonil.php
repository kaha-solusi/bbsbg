<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JadwalKegiatanPersonil extends Model
{
    const JADWAL_KEGIATAN_PERSONIL_STATUS_SCHEDULED = 'scheduled';
    const JADWAL_KEGIATAN_PERSONIL_STATUS_ONGOING = 'ongoing';
    const JADWAL_KEGIATAN_PERSONIL_STATUS_COMPLETED = 'completed';

    protected $table = 'jadwal_kegiatan_personils';
    protected $guarded = [];

    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class);
    }
}
