<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianSampleItem extends Model
{
    use LogsActivity;
    protected $fillable = ['kode_sampel', 'status', 'keterangan'];
}
