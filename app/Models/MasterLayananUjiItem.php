<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class MasterLayananUjiItem extends Model
{
    // use QueryCacheable;
    // public $cacheFor = 3600;
    // public $cachePrefix = 'master_layanan_uji_items';
    // protected static $flushCacheOnUpdate = true;

    protected $touches = ['layanan_uji'];
    protected $guarded = [];
    protected $fillable = ['name', 'price', 'working_days', 'standard', 'master_layanan_uji_id'];

    public function layanan_uji()
    {
        return $this->belongsTo(MasterLayananUji::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'product_has_items');
    }
}
