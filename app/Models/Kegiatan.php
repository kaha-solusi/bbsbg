<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    // use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'kegiatans';
    protected $guarded = [];

    /**
     * Kegiatan belongs to Lab.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lab()
    {
    	// belongsTo(RelatedModel, foreignKey = lab_id, keyOnRelatedModel = id)
    	return $this->belongsTo('App\Models\Lab');
    }
}
