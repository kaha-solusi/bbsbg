<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductBahanKerja extends Model
{
    protected $table = 'product_bahan_kerjas';
    protected $guarded = [];
}
