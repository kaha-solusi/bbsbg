<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StrukturOrganisasiFile extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'struktur_organisasi_files';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
