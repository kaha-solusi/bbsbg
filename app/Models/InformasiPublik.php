<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class InformasiPublik extends Model
{
    // use HasFactory;
    protected $table = 'informasipubliks';
    protected $guarded = [];
    public function getFoto()
    {
        return !empty($this->foto) ? Storage::url($this->foto) : null;
    }
}
