<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VideoUI extends Model
{
    // use HasFactory;
    protected $table = 'video_u_i_s';
    protected $guarded = [];
    public function getVideoId()
    {
        $id = null;
        if(!empty($this->link))
        {
            parse_str(explode("?", $this->link)[1], $res);
            $id = $res['v'];
        }
        return $id;
    }
}
