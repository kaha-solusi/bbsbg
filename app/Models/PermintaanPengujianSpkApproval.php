<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianSpkApproval extends Model
{
    use LogsActivity, BlameableTrait;
    protected $logFillable = true;
    protected $fillable = [
        'status',
        'reason',
    ];

    public function spk()
    {
        return $this->belongsTo(PermintaanPengujianSpk::class);
    }

    public function scopeIsCheckedBySubkoor()
    {
        $lastApproval = $this->get()->filter(function ($value) {
            return $value->creator->hasRole(['subkoor']) &&
            $value->creator->hasPermissionTo('approve_kaji_ulangs');
        })->last();

        return ($lastApproval->status ?? '') === 'approved';
    }

    public function scopeIsApprovedByKabalai()
    {
        $lastApproval = $this->get()->filter(function ($value) {
            return $value->creator->hasRole(['kepala-balai']) &&
            $value->creator->hasPermissionTo('approve_kaji_ulangs');
        })->last();

        return ($lastApproval->status ?? '') === 'approved';
    }
}
