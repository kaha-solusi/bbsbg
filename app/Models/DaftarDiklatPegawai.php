<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DaftarDiklatPegawai extends Model
{
    protected $table = 'daftar_diklat_pegawais';
    protected $guarded = [];

}
