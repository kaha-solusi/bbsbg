<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lab extends Model
{
    // use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'labs';
    protected $guarded = [];

    /**
     * Lab has many Peralatan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function peralatan()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = lab_id, localKey = id)
    	return $this->hasMany('App\Models\PeralatanLab');
    }

    /**
     * Lab has many Pekerjaan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pekerjaan()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = lab_id, localKey = id)
    	return $this->hasMany('App\Models\Pekerjaan');
    }

    /**
     * Lab has many Pegawai.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pegawai()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = lab_id, localKey = id)
    	return $this->hasMany('App\Models\Pegawai');
    }

    /**
     * Lab has many Pejabat.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pejabat()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = lab_id, localKey = id)
        return $this->hasMany('App\Models\Pejabat');
    }

    /**
     * Lab has many Kegiatan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kegiatan()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = lab_id, localKey = id)
        return $this->hasMany('App\Models\Kegiatan');
    }
}
