<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianPaymentConfirmationApproval extends Model
{
    use LogsActivity;

    protected $fillable = [
        'status',
        'message',
    ];
    protected $logFillable = true;

    public function payment_confirmation()
    {
        return $this->belongsTo(PermintaanPengujianPaymentConfirmation::class);
    }
}
