<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Pelayanan extends Model
{
    use QueryCacheable;
    public $cacheFor = 3600;
    public $cachePrefix = 'pelayanan';
    protected static $flushCacheOnUpdate = true;

    // use HasFactory;
    protected $table = 'pelayanans';
    protected $guarded = [];

    protected $with = ['kategori'];
    
    public function dataPelayanan()
    {
        return $this->hasMany(MasterLayananUji::class);
    }
    
    public function kategori()
    {
        return $this->belongsTo(KategoriPelayanan::class, 'kategori_id');
    }

    public function getFile()
    {
        return !empty($this->file) ? $this->file : null;
    }

    /**
     * Pelayanan has one Pelayanan_ui.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pelayanan_ui()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = pelayanan_id, localKey = id)
        return $this->hasOne('App\Models\PelayananUI');
    }

    /**
     * Pelayanan has many Permintaan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permintaan()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = pelayanan_id, localKey = id)
        return $this->hasMany('App\Models\PermintaanPengujian');
    }

    /**
     * Pelayanan has many Kepuasan_pelanggan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kepuasan_pelanggan()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = pelayanan_id, localKey = id)
        return $this->hasMany('App\Models\KepuasanPelanggan');
    }

    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class);
    }

    public function subkoor()
    {
        return $this->belongsTo(\App\User::class, 'subkoor_id');
    }
}
