<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianSpk extends Model
{
    use LogsActivity, BlameableTrait;

    protected $logFillable = true;
    protected $fillable = [
        'metode_uji',
        'tanggal_mulai',
        'tanggal_selesai',
        'keterangan',
        'nomor',
        'status',
    ];
    protected $with = ['permintaan_pengujian'];

    public function permintaan_pengujian()
    {
        return $this->belongsTo(PermintaanPengujian::class);
    }

    public function members()
    {
        return $this->belongsToMany(
            Pegawai::class,
            'permintaan_pengujian_spk_members',
            'permintaan_pengujian_spk_id',
            'pegawai_id'
        );
    }

    public function approvals()
    {
        return $this->hasMany(PermintaanPengujianSpkApproval::class);
    }

    public function isApprovedBy($role)
    {
        $lastApproval = $this->approvals()
                            ->getQuery()
                            ->whereHas('creator.roles', function ($query) use ($role) {
                                $query
                                    ->where('name', $role)
                                    ->orWhere('name', 'super-admin');
                            })->get()->last();
        return ($lastApproval->status ?? '') === 'approved';
    }
}
