<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianItem extends Model
{
    // use HasFactory;
    use LogsActivity;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permintaan_pengujian_items';
    protected $guarded = [];

    /**
     * PermintaanPengujian belongs to Client.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function permintaan_pengujian()
    {
        // belongsTo(RelatedModel, foreignKey = client_id, keyOnRelatedModel = id)
        return $this->belongsTo('App\Models\PermintaanPengujian');
    }

    /**
     * PermintaanPengujian belongs to MasterLayananUjiItem.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function layanan_uji_item()
    {
        return $this->belongsTo(
            'App\Models\MasterLayananUjiItem',
            'master_layanan_uji_item_id',
            'id'
        );
    }

    public function peralatan()
    {
        return $this->belongsToMany(
            PeralatanLab::class,
            'permintaan_pengujian_item_peralatans',
            'permintaan_pengujian_item_id'
        );
    }

    public function sample()
    {
        return $this->hasOne(
            PermintaanPengujianSampleItem::class,
            'sample_id'
        );
    }

    public function pengujian()
    {
        return $this->belongsToMany(
            PermintaanPengujianPengujian::class,
            'permintaan_pengujian_pengujian_items',
            'item_id',
            'permintaan_pengujian_pengujian_id'
        )
        ->withPivot('hasil_pengujian', 'kategori_pengujian', 'status');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
