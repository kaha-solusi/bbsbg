<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KompetensiPegawai extends Model
{
    protected $table = 'kompetensi_pegawais';
    protected $guarded = [];

}
