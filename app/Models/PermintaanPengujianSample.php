<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianSample extends Model
{
    use LogsActivity;
    protected $logFillable = true;
    protected $fillable = [
        'jasa_ekspedisi',
        'no_resi',
        'file_id',
        'status',
        'tanggal_diterima',
    ];
    protected $with = ['permintaan_pengujian'];

    public function permintaan_pengujian()
    {
        return $this->belongsTo(PermintaanPengujian::class);
    }

    public function bukti()
    {
        return $this->belongsTo(File::class, 'file_id');
    }

    public function approval()
    {
        return $this->hasMany(PermintaanPengujianSampleApproval::class);
    }

    public function items()
    {
        return $this->belongsToMany(
            PermintaanPengujianItem::class,
            'permintaan_pengujian_sample_items',
            'permintaan_pengujian_sample_id',
            'sample_id'
        )
        ->withPivot('kode_sampel')
        ->withPivot('status')
        ->withPivot('keterangan');
    }
}
