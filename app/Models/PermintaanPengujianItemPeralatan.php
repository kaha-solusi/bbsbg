<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianItemPeralatan extends Model
{
    use LogsActivity;

    protected $fillable = [
        'permintaan_pengujian_id',
        'peralatan_lab_id',
    ];
}
