<?php

namespace App\Models;

use App\Models\Client;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AdvisiTeknis extends Model
{
    const ADVISI_TEKNIS_STATUS_DRAFT = 'draft';
    const ADVISI_TEKNIS_STATUS_UPLOAD_DOCUMENT = 'upload-document';
    const ADVISI_TEKNIS_STATUS_AWAITING_KETUA_APPROVAL = 'awaiting-ketua-approval';
    const ADVISI_TEKNIS_STATUS_AWAITING_SUBKOOR_APPROVAL = 'awaiting-subkoor-approval';
    const ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL = 'awaiting-kepala-approval';
    const ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE = 'awaiting-letter-of-introduce'; // surat pengantar
    const ADVISI_TEKNIS_STATUS_APPROVED = 'approved';
    const ADVISI_TEKNIS_STATUS_REJECTED = 'rejected';
    const ADVISI_TEKNIS_STATUS_WAITING = 'waiting';
    const ADVISI_TEKNIS_STATUS_COMPLETED = 'completed';

    use LogsActivity;

    protected $logFillable = true;
    protected $guarded = ['id'];
    
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function members()
    {
        return $this->belongsToMany(Pegawai::class, 'advisi_teknis_members');
    }

    public function files()
    {
        return $this->belongsToMany(File::class, 'advisi_teknis_files', 'advisi_teknis_id');
    }

    public function approvals()
    {
        return $this->belongsToMany(
            User::class,
            'advisi_teknis_approvals',
            'advisi_teknis_id',
            'created_by'
        )
        ->withPivot('status')
        ->withPivot('created_by')
        ->withPivot('reason');
    }

    public function kuisioners()
    {
        return $this->belongsToMany(
            Kuisioner::class,
            'advisi_teknis_kuisioners'
        )
        ->withPivot('score');
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function village()
    {
        return $this->belongsTo(Village::class);
    }
}
