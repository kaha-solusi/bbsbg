<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianTestSample extends Model
{
    use LogsActivity;
    protected $logFillable = true;
    
    const STATUS_APPROVED = 'Approved';
    const STATUS_REJECTED = 'Rejected';
    const STATUS_PENDING = 'Pending';

    protected $table = 'permintaan_pengujian_samples';
    protected $fillable = [
        'jasa_ekspedisi',
        'no_resi',
        'file_id',
        'status',
        'jenis_pengiriman',
    ];

    public function bukti_pengiriman()
    {
        return $this->belongsTo('App\Models\File', 'id');
    }
}
