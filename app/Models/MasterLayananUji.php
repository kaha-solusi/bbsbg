<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class MasterLayananUji extends Model
{
    use BlameableTrait;
   
    protected $fillable = ['name', 'pelayanan_id'];
    protected $touches = ['items'];

    public function items()
    {
        return $this->hasMany(MasterLayananUjiItem::class);
    }

    public function permintaan()
    {
        return $this->hasMany(PermintaanPengujian::class);
    }

    public function pelayanan()
    {
        return $this->belongsTo(Pelayanan::class);
    }
}
