<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PelayananUI extends Model
{
    // use HasFactory;

    protected $table = 'pelayanan_u_i_s';
    protected $guarded = [];
    protected $with = ['pelayanan'];
    public function pelayanan()
    {
        return $this->belongsTo(Pelayanan::class, 'pelayanan_id');
    }
}
