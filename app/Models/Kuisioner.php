<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kuisioner extends Model
{
    protected $guarded = [];

    public function kategori_pelayanan()
    {
        return $this->belongsTo(KategoriPelayanan::class);
    }
}
