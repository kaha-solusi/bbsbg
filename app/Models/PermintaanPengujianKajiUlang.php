<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianKajiUlang extends Model
{
    use LogsActivity, BlameableTrait;

    protected $logFillable = true;
    protected $casts = [
        'parameter' => 'array',
        'created_at' => 'datetime:d-m-Y',
        'tanggal_pelayanan' => 'datetime:d-m-Y',
    ];

    protected $fillable = [
        'parameter',
        'keterangan',
        'subkontraktor',
        'nama_subkontraktor',
        'durasi',
        'file_id',
        'status',
        'tanggal_pelayanan',
        'permintaan_pengujian_id',
    ];

    protected $with = ['permintaan_pengujian'];
    
    public function permintaan_pengujian()
    {
        return $this->belongsTo(PermintaanPengujian::class);
    }

    public function approvals()
    {
        return $this->hasMany(PermintaanPengujianKajiUlangApproval::class);
    }

    public function isApprovedBy($role)
    {
        $lastApproval = $this->approvals()
                            ->getQuery()
                            ->whereHas('creator.roles', function ($query) use ($role) {
                                $query
                                    ->where('name', $role)
                                    ->orWhere('name', 'super-admin');
                            })->get()->last();
        return ($lastApproval->status ?? '') === 'approved';
    }
}
