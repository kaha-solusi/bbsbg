<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class AdvisiTeknisApproval extends Model
{
    use LogsActivity, BlameableTrait;

    protected $fillable = ['status', 'created_by', 'reason'];
    protected static $blameable = [
        'user' => User::class,
        'createdBy' => 'created_by',
        'updatedBy' => 'created_by',
    ];
}
