<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianKajiUlangApproval extends Model
{
    use LogsActivity, BlameableTrait;
    protected $fillable = [
        'status',
        'created_by',
        'reason',
        'permintaan_pengujian_kaji_ulang_id',
    ];
    protected static $logFillable = true;
    protected static $logAttributes = [
        'name',
        'text',
        'permintaan_pengujian_kaji_ulang_id.status',
    ];
    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
    ];

    public function kaji_ulang()
    {
        return $this->belongsTo(PermintaanPengujianKajiUlang::class);
    }
}
