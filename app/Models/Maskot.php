<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Maskot extends Model
{
    // use HasFactory;
    protected $table = 'maskots';
    protected $guarded = [];
    public function getFoto()
    {
        return $this->foto;
    }
}
