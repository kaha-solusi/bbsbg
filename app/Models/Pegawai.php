<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Pegawai extends Model
{
    use LogsActivity;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pegawais';
    protected $guarded = [];
    protected static $logFillable = true;

    /**
     * Pegawai belongs to Lab.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lab()
    {
        // belongsTo(RelatedModel, foreignKey = lab_id, keyOnRelatedModel = id)
        return $this->belongsTo('App\Models\Lab');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function jabatan_fungsional()
    {
        return $this->belongsTo(FunctionalPosition::class, 'functional_position_id', 'id');
    }

    public function jabatan_laboratorium()
    {
        return $this->belongsTo(LabPosition::class);
    }

    public function advisi_teknis_approval()
    {
        return $this->belongsToMany(AdvisiTeknis::class, 'advisi_teknis_approvals', 'advisi_teknis_id', 'id');
    }

    public function advisi_teknis_members()
    {
        return $this->belongsToMany(AdvisiTeknis::class, 'advisi_teknis_members', 'advisi_teknis_id', 'id');
    }

    public function jabatan()
    {
        return $this->belongsTo(Jabatan::class);
    }

    public function golongan()
    {
        return $this->belongsTo(Golongan::class);
    }

    public function lab_positions()
    {
        return $this->belongsToMany(LabPosition::class, 'pegawai_lab_positions');
    }

    /**
     * Pegawai has many Kompetensi.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kompetensi()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = pegawai_id, localKey = id)
        return $this->hasMany(KompetensiPegawai::class);
    }

    /**
     * Pegawai has many Daftar Diklat.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function diklat()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = pegawai_id, localKey = id)
        return $this->hasMany(DaftarDiklatPegawai::class);
    }
}
