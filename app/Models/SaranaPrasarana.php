<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class SaranaPrasarana extends Model
{
    // use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sarana_prasaranas';
    protected $guarded = [];
    public function getFoto()
    {
        return Storage::url($this->foto);
    }
}
