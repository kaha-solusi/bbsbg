<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianPayment extends Model
{
    use LogsActivity, BlameableTrait;
    protected $fillable = ['billing_code', 'status', 'file_id'];
    protected $logFillable = true;

    protected $with = ['permintaan_pengujian'];

    public function permintaan_pengujian()
    {
        return $this->belongsTo(PermintaanPengujian::class);
    }

    public function confirmation()
    {
        return $this->hasMany(PermintaanPengujianPaymentConfirmation::class);
    }

    public function file()
    {
        return $this->belongsTo(File::class);
    }
}
