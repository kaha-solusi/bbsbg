<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LabPosition extends Model
{
    protected $fillable = ['name'];

    /**
     * LabPosition has many Pegawai.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pegawai()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = lab_id, localKey = id)
        return $this->hasOne(Pegawai::class);
    }
}
