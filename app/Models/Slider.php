<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Slider extends Model
{
    // use HasFactory;

    protected $table = 'sliders';
    protected $guarded = [];
    public function getFoto()
    {
        return !empty($this->foto) ? $this->foto: null;
    }
}
