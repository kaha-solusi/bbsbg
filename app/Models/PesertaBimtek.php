<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class PesertaBimtek extends Model
{
    public $timestamps = false;

    // use HasFactory;
    protected $table = 'bimbingan_teknis_peserta';
    protected $fillable = ['bimtek_id', 'user_id'];

    public function bimtek()
    {
        return $this->belongsTo(Bimtek::class, 'bimtek_id');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
