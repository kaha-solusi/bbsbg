<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianPengujian extends Model
{
    use LogsActivity;
    protected $fillable = [
        'status'
    ];
    protected $with = ['permintaan_pengujian'];
    
    public function permintaan_pengujian()
    {
        return $this->belongsTo(PermintaanPengujian::class);
    }

    public function items()
    {
        return $this->hasMany(
            PermintaanPengujianPengujianItem::class,
            'permintaan_pengujian_pengujian_id'
        );
    }
}
