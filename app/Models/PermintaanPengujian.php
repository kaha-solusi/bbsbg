<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujian extends Model
{
    const STATUS_DRAFT = 'draft';
    const STATUS_REJECTED = 'rejected';
    const STATUS_APPROVED = 'approved';
    const STATUS_APPROVED_BY_SUBKOOR = 'approved-by-subkoor';
    const STATUS_APPROVED_BY_KABALAI = 'approved-by-kabalai';
    const STATUS_COMPLETED = 'completed';
    const STATUS_SUBMITTED = 'submitted';

    const STATUS_KAJI_ULANG_CHECKED_BY_SUBKOOR = 'kaji-ulang-checked-by-subkoor';
    const STATUS_KAJI_ULANG_SUBMITTED = 'kaji-ulang-submitted';
    const STATUS_KAJI_ULANG_APPROVED = 'kaji-ulang-approved';
    const STATUS_KAJI_ULANG_APPROVED_BY_KABALAI = 'kaji-ulang-approved-by-kabalai';
    const STATUS_KAJI_ULANG_REJECTED = 'kaji-ulang-rejected';

    const STATUS_FORM_CONFIRMATION_SUBMITTED = 'form-konfirmasi-submitted';
    const STATUS_FORM_CONFIRMATION_APPROVED = 'form-konfirmasi-approved';
    const STATUS_FORM_CONFIRMATION_REJECTED = 'form-konfirmasi-rejected';

    const STATUS_FORM_CONFIRMATION_REPLY_SUBMITTED = 'form-konfirmasi-reply-submitted';
    const STATUS_FORM_CONFIRMATION_REPLY_DOWNLOADED = 'form-konfirmasi-reply-downloaded';
    const STATUS_FORM_CONFIRMATION_REPLY_APPROVED = 'form-konfirmasi-reply-approved';

    const STATUS_WAITING_INVOICE = 'waiting-invoice';
    const STATUS_WAITING_PAYMENT = 'waiting-payment';

    const STATUS_WAITING_PAYMENT_CONFIRMED = 'waiting-payment-confirmed';
    const STATUS_PAYMENT_DECLINED = 'payment-declined';
    const STATUS_PAYMENT_APPROVED = 'payment-approved';
    const STATUS_PAYMENT_SUBMITTED = 'payment-submitted';
    const STATUS_PAYMENT_RECEIVED = 'payment-received';
    const STATUS_PAYMENT_DOWNLOADED = 'payment-downloaded';

    const STATUS_SAMPLE_SUBMITTED = 'sample-submitted';
    const STATUS_WAITING_SAMPLE = 'waiting-sample';
    const STATUS_SAMPLE_RECEIVED = 'sample-received';
    const STATUS_SAMPLE_REJECTED = 'sample-rejected';

    const STATUS_SPK_SUBMITTED = 'spk-submitted';

    const STATUS_PENGUJIAN_SUBMITTED = 'pengujian-submitted';

    const STATUS_LHU_SUBMITTED = 'lhu-submitted';


    // use HasFactory;
    use LogsActivity, BlameableTrait;
    protected $logUnguarded = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permintaan_pengujians';
    protected $guarded = [];
    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
    ];

    /**
     * PermintaanPengujian belongs to Client.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        // belongsTo(RelatedModel, foreignKey = client_id, keyOnRelatedModel = id)
        return $this->belongsTo('App\Models\Client');
    }

    /**
     * PermintaanPengujian belongs to Client.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invoice()
    {
        // belongsTo(RelatedModel, foreignKey = client_id, keyOnRelatedModel = id)
        return $this->hasOne('App\Models\Invoice');
    }

    /**
     * PermintaanPengujianItem hasMany items
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(PermintaanPengujianItem::class);
    }

    /**
     * PermintaanPengujianItem hasMany items
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function master_layanan_uji()
    {
        return $this->belongsTo(MasterLayananUji::class);
    }

    public function test_samples()
    {
        return $this->hasMany(PermintaanPengujianTestSample::class);
    }

    public function spk()
    {
        return $this->hasOne(PermintaanPengujianSpk::class);
    }

    public function reports()
    {
        return $this->hasMany(PermintaanPengujianReport::class);
    }

    public function kaji_ulang()
    {
        return $this->hasOne(PermintaanPengujianKajiUlang::class);
    }

    public function dokumen()
    {
        return $this->hasMany(PermintaanPengujianFile::class);
    }

    public function payments()
    {
        return $this->hasOne(PermintaanPengujianPayment::class);
    }

    public function samples()
    {
        return $this->hasMany(PermintaanPengujianSample::class);
    }

    public function pengujian()
    {
        return $this->hasOne(PermintaanPengujianPengujian::class);
    }

    public function laporan_hasil_uji()
    {
        return $this->hasOne(LaporanHasilUji::class, 'pengujian_id');
    }

    public function history()
    {
        return $this->hasMany(PermintaanPengujianHistory::class, 'permintaan_pengujian_id');
    }

    public function kuisioners()
    {
        return $this->belongsToMany(
            Kuisioner::class,
            'permintaan_pengujian_kuisioners'
        )
        ->withPivot('score');
    }
}
