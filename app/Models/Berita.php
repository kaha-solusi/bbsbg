<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Berita extends Model
{
    // use HasFactory;
    protected $table = 'beritas';
    protected $fillable = ['judul', 'foto', 'isi'];
    public function getFoto()
    {
        return $this->foto;
    }
}
