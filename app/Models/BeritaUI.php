<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BeritaUI extends Model
{
    // use HasFactory;
    protected $table = 'berita_u_i_s';
    protected $guarded = [];
    public function berita_1()
    {
        return $this->hasOne(Berita::class, 'berita_1_id');
    }
    public function berita_2()
    {
        return $this->hasOne(Berita::class, 'berita_2_id');
    }
    public function berita_3()
    {
        return $this->hasOne(Berita::class, 'berita_3_id');
    }
    public function berita_4()
    {
        return $this->hasOne(Berita::class, 'berita_4_id');
    }
}
