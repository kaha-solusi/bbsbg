<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriPelayanan extends Model
{
    // use HasFactory;
    protected $table = 'kategori_pelayanans';
    protected $guarded = [];
    public function pelayanan()
    {
        return $this->hasMany(Pelayanan::class, 'kategori_id');
    }
}
