<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class LaporanHasilUji extends Model
{
    use LogsActivity;
    protected $fillable = ['pengujian_id', 'status', 'nomor', 'deskripsi_benda_uji', 'tempat', 'tanggal_terbit','catatan','file'];

    public function permintaan_pengujian()
    {
        return $this->belongsTo(PermintaanPengujian::class, 'pengujian_id');
    }
}
