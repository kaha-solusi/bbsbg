<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PeralatanLabUsage extends Model
{
    protected $guarded = [];

    public function peralatan()
    {
        return $this->belongsTo(PeralatanLab::class, 'peralatan_lab_id');
    }
}
