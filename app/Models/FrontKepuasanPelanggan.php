<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FrontKepuasanPelanggan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'front_kepuasan_pelanggans';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
