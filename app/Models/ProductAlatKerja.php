<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAlatKerja extends Model
{
    protected $table = 'product_alat_kerjas';
    protected $guarded = [];
}
