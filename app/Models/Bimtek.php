<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\User;

class Bimtek extends Model
{
    public $timestamps = false;

    // use HasFactory;
    protected $table = 'bimbingan_teknis';
    protected $fillable = [
        'nama_bimtek',
        'jenis_bimtek',
        'deskripsi_bimtek',
        'tahun_pelaksanaan',
        'bulan_pelaksanaan',
        'tanggal_pelaksanaan',
        'waktu_pelaksanaan',
        'status_pelaksanaan',
        'syarat_peserta',
        'link_zoom',
        'link_certificate',
        'bimtek_subjek_email',
        'bimtek_konten_email',
        'bimtek_background',
        'meeting_id',
        'passcode',
    ];

    /**
     * Get the peserta for the bimtek.
     */
    public function peserta()
    {
        return $this
            ->belongsToMany(User::class, 'bimbingan_teknis_peserta', 'bimtek_id', 'user_id')
            ->withPivot('id');
    }
}
