<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class PermintaanPengujianHistory extends Model
{
    use BlameableTrait;
    
    const APPROVED = 'approved';
    const REJECTED = 'rejected';
    
    protected $casts = [
        'created_at' => 'datetime:d F Y H:m:s',
    ];
    
    protected $fillable = [
        'message',
        'status',
        'user_id',
        'step',
    ];

    public function order()
    {
        return $this->belongsTo(PermintaanPengujian::class, 'permintaan_pengujian_id');
    }

    public function pic()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
