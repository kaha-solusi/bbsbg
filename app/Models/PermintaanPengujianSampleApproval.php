<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermintaanPengujianSampleApproval extends Model
{
    use LogsActivity;

    protected $fillable = ['permintaan_pengujian_sample_id', 'message', 'status'];
    protected $logFillable = true;

    public function sample()
    {
        return $this->belongsTo(PermintaanPengujianSample::class, 'permintaan_pengujian_sample_id');
    }
}
