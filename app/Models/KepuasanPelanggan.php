<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KepuasanPelanggan extends Model
{
    // use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'kepuasan_pelanggans';
    protected $guarded = [];

    /**
     * KepuasanPelanggan belongs to User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
    	// belongsTo(RelatedModel, foreignKey = user_id, keyOnRelatedModel = id)
    	return $this->belongsTo('App\User');
    }

    /**
     * KepuasanPelanggan belongs to Pelayanan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pelayanan()
    {
    	// belongsTo(RelatedModel, foreignKey = pelayanan_id, keyOnRelatedModel = id)
    	return $this->belongsTo('App\Models\Pelayanan');
    }
}
