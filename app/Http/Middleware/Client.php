<?php

namespace App\Http\Middleware;

use Closure;

class Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->level == '2') {
            return redirect()->back()->with('failed', 'Anda bukan client/anda harus login lebih dulu');
        }
        return $next($request);
    }
}
