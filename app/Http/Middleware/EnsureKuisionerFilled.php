<?php

namespace App\Http\Middleware;

use Closure;

class EnsureKuisionerFilled
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->has('model')) {
            new \Exception('Model not found');
        }

        if (!$request->has('model_id')) {
            new \Exception('Model id not found');
        }

        $modelName = $request->input('model');
        $modelId = $request->input('model_id');

        $classes = "\\App\\Models\\$modelName";
        $model = new $classes();
        $data = $model->find($modelId);

        if ($data->kuisioners->isEmpty()) {
            return redirect()->route($request->route, $modelId);
        }

        return $next($request);
    }
}
