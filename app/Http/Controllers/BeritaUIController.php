<?php

namespace App\Http\Controllers;

use App\Models\BeritaUI;
use App\Models\Berita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BeritaUIController extends Controller
{

    public function index()
    {
        return view('admin.berita_ui', [
            'berita' => Berita::get(),
            'beritaUI' => BeritaUI::first(),
        ]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('admin.beritaui-p.edit', [
            'berita' => Berita::get(),
            'beritaUI' => BeritaUI::first(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'jumlah_berita' => 'min:1|max:'.Berita::count(),
            'berita_1_id' => 'min:0|max:'.Berita::count(),
            'berita_2_id' => 'min:0|max:'.Berita::count(),
            'berita_3_id' => 'min:0|max:'.Berita::count(),
            'berita_4_id' => 'min:0|max:'.Berita::count(),
        ]);
        if ($validation->fails()) {
            dd($validation);
            return back()->withErrors($validation)->withInput();
        }
        $berita_ui = BeritaUI::findOrFail($id);
        $berita_ui->update($request->all());
        return back()->with('success', 'Berhasil di Update !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
