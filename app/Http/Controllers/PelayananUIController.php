<?php

namespace App\Http\Controllers;

use App\Models\PelayananUI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Pelayanan;

class PelayananUIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pelayanan_ui', [
            'pelayananUI' => PelayananUI::paginate(9),
            'pelayanan' => Pelayanan::get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pelayananui-p.create', [
            'pelayanan' => Pelayanan::get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'keterangan' => 'nullable|min:3|max:125',
            'pelayanan_id' => 'required',
        ]);
        if($validator->fails()){
            return back()->withInput()->withErrors($validator);
        }
        PelayananUI::create($request->all());
        return redirect()->back()->with('success', 'Berhasil di Update !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PelayananUI  $pelayananUI
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PelayananUI  $pelayananUI
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.pelayananui-p.edit', [
            'pelayananUI' => PelayananUI::findOrFail($id),
            'pelayanan' => Pelayanan::get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PelayananUI  $pelayananUI
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'keterangan' => 'nullable|min:3|max:125',
            'pelayanan_id' => 'required',
        ]);
        if($validator->fails()){
            return back()->withInput()->withErrors($validator);
        }
        $pelayananUI = PelayananUI::findOrFail($id);
        $pelayananUI->update($request->all());
        return redirect()->back()->with('success', 'Berhasil di Update !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PelayananUI  $pelayananUI
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pelayananUI = PelayananUI::findOrFail($id);
        $pelayananUI->delete();
        return redirect()->back()->with('success', 'Berhasil di Hapus !');
    }
}
