<?php

namespace App\Http\Controllers;

use App\Models\Maklumat;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\LogAktivitas;

class MaklumatController extends Controller
{
    public function index()
    {
        return view('admin.maklumat.table', [
            'maklumat' => !empty(Maklumat::first()) ? explode(";", Maklumat::first()->isi) : [" " => " "],
        ]);
    }
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'isi*' => 'required|min:3', 
        ]);
        if($validation->fails()){
            return back()->withErrors($validation)->withInput();
        }
        $row = [];
        for($i= 0; $i < count($request->isi); $i++) {
            array_push($row, $request->isi[$i]); 
        }
        
        $data = Maklumat::first();

        $data->update([
            'isi' => (!empty($request->isi)) ? join(';', $row) : null,
        ]);

        if ($request->gambar != null) {
            $name = $request->file('gambar');
            $logo = time()."_".$name->getClientOriginalName();
            $request->gambar->move("gambar/maklumat/", $logo);

            Maklumat::find($data->id)->update([
                'img' => '/gambar/maklumat/'.$logo
            ]);
        }

        if ($request->file('pdf') != null) {
            $name = $request->file('pdf');
            $logo = time()."_".$name->getClientOriginalName();
            $request->pdf->move("pdf/maklumat/", $logo);

            Maklumat::find($data->id)->update([
                'pdf' => '/pdf/maklumat/'.$logo
            ]);
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Maklumat"
        ]); 

        return back()->with('success', 'Data Berhasil di Update !');
    }
}
