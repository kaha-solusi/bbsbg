<?php

namespace App\Http\Controllers;

use App\DataTables\Admin\InstansiDatatable;
use App\Http\Requests\InstansiRequest;
use App\Models\Instansi;
use App\Services\InstansiService;
use Illuminate\Http\Request;

class InstansiUnitKerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param App\Models\Instansi $instansi
     * @param App\DataTables\Admin\InstansiDatatable $dataTable
     *
     * @return Yajra\DataTables\Services\DataTable::render
     */
    public function index(Instansi $instansi, InstansiDatatable $dataTable)
    {
        return $dataTable->render(
            'admin.instansi.unit-kerja.index',
            compact('instansi')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Instansi $instansi)
    {
        return view('admin.instansi.unit-kerja.create', compact('instansi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(
        Instansi $instansi,
        InstansiRequest $request,
        InstansiService $service
    )
    {
        try {
            $service->store($request->nama, $instansi);
            return redirect()
                ->route('admin.instansi.unit-kerja.index', $instansi->id)
                ->with('success', 'Berhasil menambahkan unit kerja');
        } catch (\Throwable $th) {
            report($th);
            return redirect()
                ->back()
                ->withErrors($th->getMessage())
                ->withInput()
                ->with('failed', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Instansi $instansi, Instansi $unitKerja)
    {
        return view(
            'admin.instansi.unit-kerja.edit',
            compact('instansi', 'unitKerja')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(
        InstansiRequest $request,
        Instansi $instansi,
        Instansi $unitKerja,
        InstansiService $service
    )
    {
        try {
            $service->update($unitKerja, $request->nama);
            return redirect()
                ->route('admin.instansi.unit-kerja.index', $instansi->id)
                ->with('success', 'Berhasil memperbarui unit kerja');
        } catch (\Throwable $th) {
            report($th);
            return redirect()
                ->back()
                ->withErrors($th->getMessage())
                ->withInput()
                ->with('failed', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Instansi  $instansi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instansi $instansi, Instansi $unitKerja, InstansiService $service)
    {
        try {
            $service->destroy($unitKerja);
            return redirect()
                ->route('admin.instansi.unit-kerja.index', $instansi->id)
                ->with('success', 'Berhasil menghapus unit kerja');
        } catch (\Throwable $th) {
            report($th);
            return redirect()
                ->back()
                ->withErrors($th->getMessage())
                ->withInput()
                ->with('failed', $th->getMessage());
        }
    }
}
