<?php

namespace App\Http\Controllers;

use App\Models\Maskot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class MaskotController extends Controller
{

    public function index()
    {
        return view('admin.maskot',[
            'maskot' => Maskot::first(),
        ]);
    }

    public function create()
    {
        return view('admin.maskot-p.create');
    }

    public function store(Request $request)
    {
        //validasi
        $validator = Validator::make($request->all(), [
            'foto' => 'required|image|mimes:jpg,png,jpeg|max:2048',
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }

        // Attribute yg akan di input
        // $foto = $request->file('foto'); 
        // $attr = $request->all();
        $data = Maskot::create($request->only(['nama', 'deskripsi']));
        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("maskot/foto/", $logo);

            Maskot::find($data->id)->update([
                'foto' => 'maskot/foto/'.$logo
            ]);         
        }
        // //Masukan foto dengan nama + timestamp
        // $attr['foto'] = Storage::putFileAs(
        //     'maskot/foto', 
        //     $foto, 
        //     time()."_".$foto->getClientOriginalName()
        // );
        // Maskot::create($attr);
        return back()->with('success', 'Berhasil di Update !');
    }

    public function show($id)
    {
        //   
    }

    public function edit($id)
    {
        return view('admin.maskot-p.edit', [
            'maskot' => Maskot::findOrFail($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        //validasi
        $validator = Validator::make($request->all(), [
            'foto' => 'image|mimes:jpg,png,jpeg|max:2048',
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }


        $data = Maskot::findOrFail($id);
        $data->update($request->only(['nama', 'deskripsi']));
        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("maskot/foto/", $logo);

            Maskot::find($data->id)->update([
                'foto' => 'maskot/foto/'.$logo
            ]);         
        }

        // // Attribute yg akan di input
        // $maskot = Maskot::findOrFail($id);
        // $foto = $request->file('foto'); 
        // $attr = $request->all();
        // //Masukan foto dengan nama + timestamp
        // if(!empty($foto))
        // {
        //     Storage::delete($maskot->foto);
        //     $attr['foto'] = Storage::putFileAs(
        //         'maskot/foto', 
        //         $foto, 
        //         time()."_".$foto->getClientOriginalName()
        //     );
        // }else{
        //     $attr['foto'] = $maskot->foto;
        // }
        
        // $maskot->update($attr);
        return back()->with('success', 'Berhasil di Update !');
    }

    public function destroy($id)
    {
        $maskot = Maskot::findOrFail($id);
        $maskot->delete();
        return back()->with('success', 'Berhasil di Hapus !');
    }
}
