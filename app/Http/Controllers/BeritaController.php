<?php

namespace App\Http\Controllers;

use App\Models\Berita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class BeritaController extends Controller
{

    public function index()
    {
        return view('admin.berita', [
            'berita' => Berita::get(),
        ]);
    }

    public function create()
    {
        return view('admin.berita-p.create');
    }

    public function store(Request $request)
    {
        //validasi
        $validator = Validator::make($request->all(), [
            'foto' => 'required|image|mimes:jpg,png,jpeg|max:2048',
            'isi' => 'required',
            'judul' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $data = Berita::create($request->all());
        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("pelayanan/foto/", $logo);

            Berita::find($data->id)->update([
                'foto' => 'pelayanan/foto/'.$logo,
            ]);
        }

        // // Attribute yg akan di input
        // $foto = $request->file('foto');
        // $attr = $request->all();
        // //Masukan foto dengan nama + timestamp
        // $attr['foto'] = Storage::putFileAs(
        //     'berita/foto',
        //     $foto,
        //     time()."_".$foto->getClientOriginalName()
        // );
        // Berita::create($attr);
        return back()->with('success', 'Berhasil di Update !');
    }

    public function show($id)
    {
        return view('homepage.detailberita', [
            'berita' => Berita::findOrFail($id),
            'berita_terbaru' => Berita::orderBy('updated_at', 'desc')->get(),
        ]);
    }


    public function edit($id)
    {
        return view('admin.berita-p.edit', [
            'berita' => Berita::findOrFail($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        //valiadasi
        $validator = Validator::make($request->all(), [
            'foto' => 'image|mimes:jpg,png,jpeg|max:2048',
            'isi' => 'required',
            'judul' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }
        //Attribute yg akan di input
        $data = Berita::findOrFail($id);
        $data->update($request->all());
        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("pelayanan/foto/", $logo);

            Berita::find($data->id)->update([
                'foto' => 'pelayanan/foto/'.$logo,
            ]);
        }
        // $foto = $request->file('foto');
        // $attr = $request->all();
        // // cek apakah ada foto
        // if(!empty($foto)){
        //     // Jika ada foto lama di hapus dan di ganti yg baru
        //     Storage::delete($berita->foto);
        //     $attr['foto'] = Storage::putFileAs(
        //         'berita/foto',
        //         $foto,
        //         time()."_".$foto->getClientOriginalName()
        //     );
        // }else{
        //     // jika tidak ada pakai foto sebelumnya
        //     $attr['foto'] = $berita->foto;
        // }
        // $berita->update($attr);
        return back()->with('success', 'Berhasil di Update !');
    }

    public function destroy($id)
    {
        $berita = Berita::findOrFail($id);
        Storage::delete($berita->foto);
        $berita->delete();
        return back()->with('success', 'Berhasil di Delete !');
    }
}
