<?php

namespace App\Http\Controllers;

use App\Models\KategoriPelayanan;
use Illuminate\Http\Request;

class KategoriPelayananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KategoriPelayanan  $kategoriPelayanan
     * @return \Illuminate\Http\Response
     */
    public function show(KategoriPelayanan $kategoriPelayanan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KategoriPelayanan  $kategoriPelayanan
     * @return \Illuminate\Http\Response
     */
    public function edit(KategoriPelayanan $kategoriPelayanan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KategoriPelayanan  $kategoriPelayanan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KategoriPelayanan $kategoriPelayanan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KategoriPelayanan  $kategoriPelayanan
     * @return \Illuminate\Http\Response
     */
    public function destroy(KategoriPelayanan $kategoriPelayanan)
    {
        //
    }
}
