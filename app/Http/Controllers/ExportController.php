<?php

namespace App\Http\Controllers;

use App\Exports\BimtekPesertaExport;
use App\Exports\ClientExport;
use App\Exports\PegawaiExport;
use App\Exports\PelayananExport;
use App\Exports\PermintaanPengujianExport;
use App\Exports\StrukturOrganisasiExport;
use App\Exports\JadwalPengujianExport;
use App\Exports\JadwalBimtekExport;
use App\Exports\UmpanBalikExport;
use App\Exports\KepuasanPelangganExport;
use App\Exports\IndeksKepuasanExport;
use App\Exports\UserExport;
use App\Exports\PeralatanLabExport;
use App\Models\Pelayanan;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class ExportController extends Controller
{
    public function pegawai()
    {
        return Excel::download(new PegawaiExport, time().'-pegawai.xlsx');
    }
    public function bimtek_peserta()
    {
        return Excel::download(new BimtekPesertaExport(), time().'-bimtek-peserta.xlsx');
    }
    public function client()
    {
        return Excel::download(new ClientExport, time().'-client.xlsx');
    }
    public function permintaan_pengujian()
    {
        return Excel::download(new PermintaanPengujianExport, time().'-permintaan-pengujian.xlsx');
    }
    public function struktur_organisasi()
    {
        return Excel::download(new StrukturOrganisasiExport, time().'-struktur-organisasi.xlsx');
    }
    public function akun()
    {
        return Excel::download(new UserExport, time().'-akun.xlsx');
    }
    public function pelayanan($kategori)
    {
        return Excel::download(new PelayananExport($kategori), time().'-pelayanan.xlsx');
    }
    public function jadwal_pengujian()
    {
        return Excel::download(new JadwalPengujianExport, time().'-jadwal-pengujian.xlsx');
    }
    public function jadwal_bimtek()
    {
        return Excel::download(new JadwalBimtekExport, time().'-jadwal-bimtek.xlsx');
    }
    public function umpan_balik()
    {
        return Excel::download(new UmpanBalikExport, time().'-umpan-balik.xlsx');
    }
    public function kepuasan_pelanggan()
    {
        return Excel::download(new KepuasanPelangganExport, time().'-kepuasan-pelanggan.xlsx');
    }
    public function indeks_kepuasan()
    {
        return Excel::download(new IndeksKepuasanExport, time().'-indeks-kepuasan.xlsx');
    }

    public function peralatan_lab()
    {
        $excel_name = time().'_'.now('Asia/Jakarta')->format('Y_m_d').'_peralatan-lab.xlsx';
        return Excel::download(new PeralatanLabExport, $excel_name);

        // Storage::disk('public')->put($excel_name, $excel);
        // // dd($excel);
        // $zip_file = 'export_excel_and_picture.zip';
        // $zip = new \ZipArchive();
        // $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        // $files = \App\Models\PeralatanLab::all();
        // foreach ($files as $file)
        // {
        //     if ($file->foto_alat != null) {
        //         // extracting filename with substr/strlen

        //         $zip->addFile(public_path($file->foto_alat), $file->foto_alat);
        //     }
        // }

        // $zip->addFile(Excel::store(new PeralatanLabExport(), $excel_name, 'public'), $excel_name);
        // $zip->close();
        // return response()->download($zip_file);
    }
}
