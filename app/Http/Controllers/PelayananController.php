<?php

namespace App\Http\Controllers;

use App\Models\KategoriPelayanan;
use App\Models\Pelayanan;
use App\Models\Bimtek;
use App\Services\PegawaiService;
use App\Services\PelayananService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class PelayananController extends Controller
{

    public function index($kategori)
    {
        $k = KategoriPelayanan::findOrFail($kategori);

        return view('admin.pelayanan', [
            'pelayanan' => $k->pelayanan,
            'kategori'  => $k,
            'bimteks'   => Bimtek::all(),
        ]);
    }

    public function create($kategori)
    {
        return view('admin.pelayanan-p.create', [
            'kategori' => KategoriPelayanan::findOrFail($kategori),
        ]);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        //validasi
        $validator = Validator::make($request->all(), [
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'file' => 'nullable|mimes:pdf|max:10240',
            'file_sop' => 'nullable|mimes:pdf|max:10240',
            'file_panduan' => 'nullable|mimes:pdf|max:10240',
            // 'file_biaya' => 'nullable|mimes:pdf|max:10240',
            'nama' => 'required|string|max:25',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $data = Pelayanan::create($request->only('nama', 'kategori_id', 'text'));

        if ($request->file('file')) {
            // File::delete($data->file);

            $name = $request->file('file');
            $logo = time()."_".$name->getClientOriginalName();
            $request->file->move("pelayanan/file/", $logo);

            Pelayanan::find($data->id)->update([
                'file' => 'pelayanan/file/'.$logo,
            ]);
        }

        if ($request->file('file_sop')) {
            // File::delete($data->file);

            $name = $request->file('file_sop');
            $logo = time()."_".$name->getClientOriginalName();
            $request->file_sop->move("pelayanan/sop/", $logo);

            Pelayanan::find($data->id)->update([
                'file_sop' => 'pelayanan/sop/'.$logo,
            ]);
        }

        if ($request->file('file_panduan')) {
            // File::delete($data->file);

            $name = $request->file('file_panduan');
            $logo = time()."_".$name->getClientOriginalName();
            $request->file_panduan->move("pelayanan/panduan/", $logo);

            Pelayanan::find($data->id)->update([
                'file_panduan' => 'pelayanan/panduan/'.$logo,
            ]);
        }

        // if ($request->file('file_biaya')) {
        //     // File::delete($data->file);

        //     $name = $request->file('file_biaya');
        //     $logo = time()."_".$name->getClientOriginalName();
        //     $request->file_biaya->move("pelayanan/biaya/", $logo);

        //     Pelayanan::find($data->id)->update([
        //         'file_biaya' => 'pelayanan/biaya/'.$logo
        //     ]);
        // }

        if ($request->file('foto')) {
            // File::delete($data->file);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("pelayanan/foto/", $logo);

            Pelayanan::find($data->id)->update([
                'foto' => 'pelayanan/foto/'.$logo,
            ]);
        }
        // Attribute yg akan di input
        // $pdf = $request->file('file');
        // $attr = $request->all();
        // $attr['file'] = Storage::putFileAs(
        //     'pelayanan/pdf',
        //     $pdf,
        //     time()."_".$pdf->getClientOriginalName()
        // );
        // Pelayanan::create($attr);
        return back()->with('success', 'Berhasil di buat !');
    }

    public function edit($id)
    {
        $pegawai = (new PegawaiService())->getAll();

        $subkoor = User::role('subkoor')->get();
        return view('admin.pelayanan-p.edit', [
            'pelayanan' => Pelayanan::findOrFail($id),
            'kategori' => Pelayanan::findOrFail($id)->kategori,
            'pegawai' => $pegawai,
            'subkoor' => $subkoor
        ]);
    }

    public function update(Request $request, $id)
    {
        //validasi
        $validator = Validator::make($request->all(), [
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'file' => 'nullable|mimes:pdf|max:10240',
            'file_sop' => 'nullable|mimes:pdf|max:10240',
            'file_panduan' => 'nullable|mimes:pdf|max:10240',
            // 'file_biaya' => 'nullable|mimes:pdf|max:10240',
            'nama' => 'required'

        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('failed', 'Gagal di Update !');
        }
        $data = Pelayanan::findOrFail($id);

//        if (!is_null($request->file('background'))) {
//            $name = $request->file('background');
//            $bg_image = hash('sha256', $name->getClientOriginalName()).".".$name->getClientOriginalExtension();
//            $request->background->move("pelayanan/background_images/", $bg_image);
//            $request->background = $bg_image;
//
//            File::delete("pelayanan/background_images/" . $data->background);
//
//        }

        $data->update([
            'pegawai_id'            => $request->ketua,
            'subkoor_id'            => $request->subkoor,
            "nama"                  => $request->nama,
            "kategori_id"           => $request->kategori_id,
            "text"                  => $request->text,
            "syarat_peserta"        => $request->syarat_peserta,
            "link_zoom"             => $request->link_zoom,
            "meeting_id"            => $request->meeting_id,
            "passcode"              => $request->passcode,
            "subjek_email"          => $request->subjek_email,
            "konten_email"          => $request->konten_email,
            "off_days"              => $request->off_days
//            'background' => is_null($request->background) ? $data->background : $request->background
        ]);

        if ($request->file('file')) {
            // File::delete($data->file);

            $name = $request->file('file');
            $logo = time()."_".$name->getClientOriginalName();
            $request->file->move("pelayanan/file/", $logo);

            Pelayanan::find($data->id)->update([
                'file' => 'pelayanan/file/'.$logo,
            ]);
        }

        if ($request->file('file_sop')) {
            // File::delete($data->file);

            $name = $request->file('file_sop');
            $logo = time()."_".$name->getClientOriginalName();
            $request->file_sop->move("pelayanan/sop/", $logo);

            Pelayanan::find($data->id)->update([
                'file_sop' => 'pelayanan/sop/'.$logo,
            ]);
        }

        if ($request->file('file_panduan')) {
            // File::delete($data->file);

            $name = $request->file('file_panduan');
            $logo = time()."_".$name->getClientOriginalName();
            $request->file_panduan->move("pelayanan/panduan/", $logo);

            Pelayanan::find($data->id)->update([
                'file_panduan' => 'pelayanan/panduan/'.$logo,
            ]);
        }

        // if ($request->file('file_biaya')) {
        //     // File::delete($data->file);

        //     $name = $request->file('file_biaya');
        //     $logo = time()."_".$name->getClientOriginalName();
        //     $request->file_biaya->move("pelayanan/biaya/", $logo);

        //     Pelayanan::find($data->id)->update([
        //         'file_biaya' => 'pelayanan/biaya/'.$logo
        //     ]);
        // }

        if ($request->file('foto')) {
            // File::delete($data->file);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("pelayanan/foto/", $logo);

            Pelayanan::find($data->id)->update([
                'foto' => 'pelayanan/foto/'.$logo,
            ]);
        }

        return back()->with('success', 'Berhasil di Update !');
    }
    public function destroy($id)
    {
        $pelayanan = Pelayanan::findOrFail($id);
        // File::delete($pelayanan->file);
        $pelayanan->delete();
        return back()->with('success', 'Berhasil di Hapus !');
    }
    public function show()
    {
        //
    }

    public function pelayanan($id)
    {
        $data = Pelayanan::findOrFail($id);
        $bimteks = Bimtek::all();
        $years_bimtek = Bimtek::orderBy('tahun_pelaksanaan', 'desc')
                            ->groupBy('tahun_pelaksanaan')
                            ->get();
        return view('homepage.pelayanan-page', compact('data', 'bimteks', 'years_bimtek'));
    }

    public function storeAdvisiTeknis(Request $request, PelayananService $pelayananService)
    {
        $data = collect([
            'test_date' => $request->input_date_at,
            'scope' => $request->lingkup_konsultasi,
            'building_identity' => $request->input_identitas_bangunan,
            'location' => $request->input_lokasi,
            'problem' => $request->permasalahan,
            'support_file' => $request->file_pendukung
        ]);

        $pelayananService->store($data, auth()->user()->id);
    }
}
