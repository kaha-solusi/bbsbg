<?php

/**
 * Ssss *
 * php version 7.4.0
 *
 * @category Controller
 * @package  PermintaanPengujian
 * @author   Agung Kurniawan <agungkes95@gmail.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @version  GIT: adskahdkadhskj
 * @link     http://url.com
 */
namespace App\Http\Controllers;

use App\DataTables\Admin\PermintaanPengujianDatatable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PermintaanPengujian as Pendaftaran;
use App\Models\SettingApp;
use App\Models\Pelayanan;
use Illuminate\Support\Facades\Validator;

/**
 * PermintaanPengujian
 *
 * @category Controller
 * @package  PermintaanPengujian
 * @author   Agung Kurniawan <agungkes95@gmail.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @version  Release: 0.0.1
 * @access   public
 * @link     http://url.com
 */
class PermintaanPengujian extends Controller
{
    /**
     * Index function
     *
     * @param PermintaanPengujianDatatable $dataTable
     *
     * @return mixed
     */
    public function index(PermintaanPengujianDatatable $dataTable)
    {
        return $dataTable->render('admin.permintaan_pengujian.index');
    }

    /**
     * Show function
     *
     * @param Pendaftaran $pengujian
     *
     * @return mixed
     */
    public function show(Pendaftaran $pengujian)
    {
        $subTotal = collect($pengujian->items)->reduce(
            function ($prev, $current) {
                return $prev +
                    $current->layanan_uji_item->price *
                    $current->product_count;
            },
            0
        );

        return view(
            'admin.permintaan_pengujian.show',
            compact(
                'pengujian',
                'subTotal'
            )
        );
    }

    public function listPermintaan(Request $request)
    {
        $data = Pendaftaran::where('client_id', auth()->user()->client->id)->orderBy('id', 'desc')->paginate(10);

        foreach (Pendaftaran::all() as $value) {
            if (date('Y-m-d', strtotime($value->tgl_permintaan)) > date('Y-m-d', strtotime(now('Asia/Jakarta')))) {
                Pendaftaran::find($value->id)->update(
                    [
                    'status' => 'selesai',
                    ]
                );
            }
        }

        $search = $request->query('search', null);

        if ($search != null) {
            $data = Pendaftaran::where('nama_pemohon', 'like', '%'.$search.'%')
                ->where('client_id', auth()->user()->client->id)
                ->orWhere(
                    function ($query) use ($search) {
                        $query->where('email', 'like', '%'.$search.'%')
                            ->where('client_id', auth()->user()->client->id);
                    }
                )
            ->orWhere(
                function ($query) use ($search) {
                    $query->where('tgl_permintaan', 'like', '%'.$search.'%')
                        ->where('client_id', auth()->user()->client->id);
                }
            )
            ->orWhere(
                function ($query) use ($search) {
                    $query->where('nomor_hp_kontak', 'like', '%'.$search.'%')
                        ->where('client_id', auth()->user()->client->id);
                }
            )
            ->orWhere(
                function ($query) use ($search) {
                    $query->where('status', 'like', '%'.$search.'%')
                        ->where('client_id', auth()->user()->client->id);
                }
            )
            ->orWhere(
                function ($query) use ($search) {
                    $query->whereHas(
                        'pelayanan',
                        function ($q) use ($search) {
                            $q->where('nama', 'like', '%'.$search.'%');
                        }
                    )
                    ->where('client_id', auth()->user()->client->id);
                }
            )
            ->orderBy('id', 'desc')
            ->paginate(10);
        }

        return view('admin.permintaan_pengujian.table-client', compact('data', 'search'));
    }

    public function terimaPermintaan($id)
    {
        $data = Pendaftaran::findOrFail($id);

        $data->update(
            [
            'status' => 'diterima',
            ]
        );

        // Mail::to($data->email)->send(new PermintaanStatus($data->id, $data->tgl_permintaan, $data->nama_pemohon, 'diterima'));

        return back()->with('success', __('Status Diterima.'));
    }

    public function tolakPermintaan($id)
    {
        $data = Pendaftaran::findOrFail($id);

        $data->update(
            [
            'status' => 'ditolak',
            ]
        );

        // Mail::to($data->email)->send(new PermintaanStatus($data->id, $data->tgl_permintaan, $data->nama_pemohon, 'ditolak'));

        return back()->with('success', __('Status Diterima.'));
    }

    public function selesaiPermintaan($id)
    {
        $data = Pendaftaran::findOrFail($id);

        $data->update(
            [
            'status' => 'selesai',
            ]
        );

        // Mail::to($data->email)
            // ->send(new PermintaanStatus(
            //     $data->id,
            //     $data->tgl_permintaan,
            //     $data->nama_pemohon,
            //     'selesai'
            // ));

        return back()->with('success', __('Status Diterima.'));
    }

    public function create($id)
    {
        $data = Pelayanan::with('kategori')->findOrFail($id);
        $email = auth()->user()->email;

        return view('homepage.pendaftaran-client', compact('data', 'email'));
    }

    public function store(Request $request, $id)
    {
        $v = Validator::make(
            $request->all(),
            [
            'nama_pemohon' => 'required|string|max:250',
            'alamat' => 'required|string|max:65534',
            'nama_kontak' => 'required|string|max:250',
            'nomor_hp_kontak' => 'required|string|regex:/(628)[0-9]{9}/',
            'email' => 'required|string|email',
            'tanggal_pengujian' => 'required|date',
            'uraian_pengujian' => 'required|string|max:65534',
            'detail_layanan' => 'required|string|max:65534',
            'nama_lhu' => 'nullable|string|max:250',
            'email_persuratan' => 'required|string|email',
            ]
        );

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $pelayanan = Pelayanan::findOrFail($id);

        Pendaftaran::create(
            $request->only(
                'nama_pemohon',
                'email',
                'nama_kontak',
                'nomor_hp_kontak',
                'alamat',
                'nama_lhu',
                'email_persuratan',
                'tanggal_pengujian',
                'uraian_pengujian',
                'detail_layanan'
            ) + [
            'pelayanan_id' => $id,
            'tgl_permintaan' => date('d-m-Y', strtotime(now('Asia/Jakarta'))),
            'status' => 'diproses',
            'client_id' => auth()->user()->client->id
                ]
        );

        $setting = SettingApp::find(1);

        $plus = '';

        if ($setting->email != null) {
            // Mail::to($setting->email)
                // ->send(new PermintaanMail(
                //     now('Asia/Jakarta'),
                //     'Bahan',
                //     $request->nama_pemohon,
                //     $request->email
                // ));
        }

        return back()->with('success', __('Pendaftaran dikirim. '.$plus));
    }

    public function statuspengujian(Request $status)
    {
        return view(
            'client.permintaan-pengujian.statuspengujian',
            compact(
                'status'
            )
        );
    }
}
