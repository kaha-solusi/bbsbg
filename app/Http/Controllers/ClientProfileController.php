<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class ClientProfileController extends Controller
{
    public function index()
    {
        return view('client.profile.index');
    }
}