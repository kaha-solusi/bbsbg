<?php

namespace App\Http\Controllers;
use App\Models\InformasiPublik;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class InformasiPublikController extends Controller
{

    public function index()
    {
        return view('admin.informasipublik',[
            'informasipublik' => InformasiPublik::get(),
        ]);
    }

    public function create()
    {
        return view('admin.informasipublik-p.create');
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'link' => 'required|url',
            'nama' => 'required',
            'foto' => 'image|mimes:jpg,png,jpeg|max:2048',
        ]);
        if($validation->fails())
        {
            return back()->withInput()->withErrors($validation);
        }
        $attr = $request->all();
        if(!empty($request->file('foto')))
        {
            $foto = $request->file('foto');
            //Masukan foto dengan nama + timestamp
            $attr['foto'] = Storage::putFileAs(
                'public/informasiPublik/foto', 
                $foto, 
                time()."_".$foto->getClientOriginalName()
            );
        }
        InformasiPublik::create($attr);
        return back()->with('success', 'Berhasil di Update !');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        return view('admin.informasipublik-p.edit', [
            'informasipublik' => InformasiPublik::findOrFail($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[
            'link' => 'required|url',
            'nama' => 'required',
            'foto' => 'image|mimes:jpg,png,jpeg|max:2048',
        ]);
        if($validation->fails())
        {
            return back()->withInput()->withErrors($validation);
        }

        $informasiPublik = InformasiPublik::findOrFail($id);
        $attr = $request->all();
        $foto = $request->file('foto');
        // cek apakah ada foto 
        if(!empty($foto)){
            // Jika ada foto lama di hapus dan di ganti yg baru
            Storage::delete($informasiPublik->foto);
            $attr['foto'] = Storage::putFileAs(
                'public/informasiPublik/foto', 
                $foto, 
                time()."_".$foto->getClientOriginalName()
            );
        }else{
            // jika tidak ada pakai foto sebelumnya
            $attr['foto'] = $informasiPublik->foto;
        }
        $informasiPublik->update($attr);
        return back()->with('success', 'Berhasil di Update !');
    }

    public function destroy($id)
    {
        $informasiPublik = InformasiPublik::findOrFail($id);
        $informasiPublik->delete();
        return back()->with('success', 'Berhasil di Hapus !');
    }
}
