<?php

namespace App\Http\Controllers;

use App\Models\AlurPelayanan;
use App\Models\Berita;
use App\Models\Galeri;
use App\Models\Kegiatan;
use App\Models\Maskot;
use App\Models\Pejabat;
use App\Models\Pegawai as Budak;
use App\Models\Pelayanan;
use App\Models\Pengajar;
use App\Models\SaranaPrasarana;
use Illuminate\Http\Request;
use App\Models\Sejarah;
use App\Models\Slider;
use App\Models\StrukturOrganisasi;
use App\Models\StrukturOrganisasiFile;
use App\Models\TugasFungsi;
use App\Models\VisiMisi;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Models\VideoUI;
use App\Models\PelayananUI;
use App\Models\SettingApp;
use App\Models\Maklumat;
use App\Models\Faq;
use App\Models\FrontKepuasanPelanggan;
use App\Models\KebijakanPelayanan;
use App\Models\KepuasanPelanggan;

class HomeController extends Controller
{

    public function home()
    {
        $berita = Berita::get();
        $berita_random = [];
        if (count($berita) > 0) {
            for ($i = 0; $i < 4; $i++) {
                array_push($berita_random, $berita[rand(0, count($berita) - 1)]);
            }
        }
        return view('homepage.welcome', [
            'pelayanan' => Pelayanan::get(),
            'berita' => Berita::limit(5)->get(),
            'berita_random' => $berita_random,
            'slider' => Slider::get(),
            'videoUI' => VideoUI::first(),
            'pelayananUI' => PelayananUI::get(),
            'setting' => SettingApp::first(),
        ]);
    }
    public function faq()
    {
        $data = Faq::get();

        return view('homepage.faq', compact('data'));
    }
    public function sejarah()
    {
        return view('homepage.sejarah', [
            'sejarah' => Sejarah::first(),
        ]);
    }
    public function visimisi()
    {
        $visimisi = VisiMisi::first();
        $misi = explode(";", $visimisi->misi);
        array_pop($misi);
        return view('homepage.visimisi', [
            'visi' => $visimisi->visi,
            'misi' => $misi,
            'data' => $visimisi,
        ]);
    }
    public function kepuasan_pelanggan()
    {
        $datas = FrontKepuasanPelanggan::first();

        return view('homepage.kepuasan_pelanggan', [
            'data' => $datas,
        ]);
    }
    public function kebijakanpelayanan()
    {
        $visimisi = KebijakanPelayanan::first();
        return view('homepage.kebijakan_pelayanan', [
            'text' => $visimisi->text,
            'data' => $visimisi,
        ]);
    }
    public function alurpelayanan()
    {
        $visimisi = AlurPelayanan::first();
        $misi = explode(";", $visimisi->text);
        // dd($misi);
        // array_pop($misi);
        return view('homepage.alur-pelayanan', [
            'misi' => $misi,
            'data' => $visimisi,
        ]);
    }
    public function tugasfungsi()
    {
        $tugasfungsi = TugasFungsi::first();
        $fungsi = explode(";", $tugasfungsi->fungsi);
        array_pop($fungsi);
        return view('homepage.tugasfungsi', [
            'tugas' => $tugasfungsi->tugas,
            'fungsi' => $fungsi,
            'data'  => $tugasfungsi
        ]);
    }
    public function struktur()
    {
        return view('homepage.struktur', [
            'data' => StrukturOrganisasi::paginate(10),
            'organisasi' => StrukturOrganisasiFile::first(),
        ]);
    }
    public function maklumat()
    {
        return view('homepage.maklumat', [
            'maklumat' => Maklumat::first(),
        ]);
    }
    public function pejabat()
    {
        return view('homepage.pejabat', [
            'pejabat' => Budak::get(),
        ]);
    }
    public function pengajar()
    {
        return view('homepage.pengajar', [
            'pengajar' => Pengajar::get(),
        ]);
    }
    public function pelayanan()
    {
        return view('homepage.pelayanan');
    }
    public function pelayananTracking()
    {
        return view('homepage.pelayanan.tracking');
    }
    public function fasilitas()
    {
        return view('homepage.fasilitas', [
            'fasilitas' => SaranaPrasarana::paginate(9),
            'kategori' => SaranaPrasarana::value('kategori'),
        ]);
    }
    public function jadwalbimtek()
    {
        return view('homepage.jadwalbimtek');
    }
    public function galeri($kategori)
    {
        return view('homepage.galeri', [
            'galeri' => Galeri::where('kategori', $kategori)->paginate(9),
            'kategori' => $kategori,
        ]);
    }

    // galeri video gak dipakai
    public function galerivideo()
    {
        return view('homepage.galerivideo', [
            'galeri' => Galeri::where('video', '!=', null)->paginate(9),
        ]);
    }
    public function berita()
    {
        return view('homepage.berita', [
            'berita' => Berita::orderBy('created_at', 'desc')->paginate(5),
        ]);
    }
    public function detailberita($id)
    {
        return view('homepage.detailberita', [
            'berita' => Berita::find($id),
        ]);
    }
    public function maskot()
    {
        return view('homepage.maskot', [
            'maskot' => Maskot::first(),
        ]);
    }
    public function jadwal_pengujian()
    {
        return view('homepage.jadwal-pengujian', [
            'jadwal' => Kegiatan::get(),
        ]);
    }
    public function cariberita(Request $request)
    {
        $berita = Berita::query()
        ->where('judul', 'LIKE', "%{$request->cari}%")
        ->orWhere('isi', 'LIKE', "%{$request->cari}%")
        ->get();
        return view('homepage.berita', [
            'berita' => $this->paginate($berita, 5, null, ['path' => '/home/berita/']),
        ]);
    }
    // paginate collection
    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
