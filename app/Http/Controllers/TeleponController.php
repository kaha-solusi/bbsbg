<?php

namespace App\Http\Controllers;

use App\Models\Telepon;
use Illuminate\Http\Request;

class TeleponController extends Controller
{
    public function index()
    {
        return  view('admin.permintaan_telepon', [
            'telepon' => Telepon::get(),
        ]);
    }
    public function newTicket(Request $request)
    {

        $this->validate(
            $request, [
                'nama' => 'required',
                'nomor_telepon' => 'required',
                'deskripsi' => 'required'
            ]
        );

        Telepon::create($request->all());

        return back()->with('success', 'Berhasil mengirim permintaan, mohon tunggu operator untuk menelepon anda');
    
    }
    public function create()
    {
        return view('homepage.telepon');
    }
}
