<?php

namespace App\Http\Controllers;

use App\Models\VideoUI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class VideoUIController extends Controller
{
    public function index()
    {
        return view('admin.video_ui', [
            'videoUI' => VideoUI::first(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'judul' => 'required|',
            'link' => 'required|url',
            'logo' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'keterangan' => 'nullable',
        ]);
        if($validation->fails()){
            return back()->withErrors($validation)->withInput();
        }
        $videoUI = VideoUI::findOrFail($id);
        $videoUI->update($request->all());
        // dd($request->logo);
        if ($request->file('logo')) {
            // (!empty($this->logo)) ?? File::delete($videoUI->logo);

            $name = $request->file('logo');
            $logo = time()."_".$name->getClientOriginalName();
            $request->logo->move("videoui/logo", $logo);
            VideoUI::find($videoUI->id)->update([
                'logo' => 'videoui/logo/'.$logo
            ]);         
        }
        return redirect()->back()->with('success', 'Berhasil di Update !');
    }
}
