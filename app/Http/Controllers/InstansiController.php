<?php

namespace App\Http\Controllers;

use App\DataTables\Admin\InstansiDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\InstansiRequest;
use App\Models\Instansi;
use App\Services\InstansiService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InstansiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(InstansiDatatable $dataTable)
    {
        return $dataTable->render('admin.instansi.index');
    }

    public function create()
    {
        return view('admin.instansi.create');
    }

    public function store(InstansiRequest $request, InstansiService $service)
    {
        try {
            $service->store($request->nama);
            return redirect()
                ->route('admin.instansi.index')
                ->with('success', 'Berhasil menambahkan unit organisasi baru');
        } catch (\Throwable $th) {
            report($th);
            return redirect()
                ->back()
                ->withErrors($th->getMessage())
                ->withInput()
                ->with('failed', $th->getMessage());
        }
    }

    public function edit(Instansi $instansi)
    {
        return view('admin.instansi.edit', compact('instansi'));
    }

    public function update(
        InstansiRequest $request,
        Instansi $instansi,
        InstansiService $service
    )
    {
        try {
            $service->update($instansi, $request->nama);
            return redirect()
                ->route('admin.instansi.index')
                ->with('success', 'Berhasil memperbarui unit organisasi');
        } catch (\Throwable $th) {
            report($th);
            return redirect()
                ->back()
                ->withErrors($th->getMessage())
                ->withInput()
                ->with('failed', $th->getMessage());
        }
    }

    public function show($id)
    {
    }
    public function destroy(Instansi $instansi, InstansiService $service)
    {
        $service->destroy($instansi);
        return back()->with('success', 'Berhasil menghapus unit organisasi');
    }
}
