<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if ($data['group'] == 'pemerintahan-lainnya') {
            return Validator::make($data, [
                'name' => ['required', 'string'],
                'username' => ['required', 'string', 'unique:users'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'alamat' => 'required',
                'group' => 'required',
                'phone_number' => ['required','unique:clients','regex:/(628)[0-9]{9,}/'],
                'nama_instansi' => 'required'
            ]);
        } elseif ($data['group'] == 'nonpemerintah') {
            return Validator::make($data, [
                'name' => ['required', 'string'],
                'username' => ['required', 'string','unique:users'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'alamat' => 'required',
                'phone_number' => ['required','unique:clients','regex:/(628)[0-9]{9,}/'],
                'group' => 'required',
                'nama_instansi' => 'required'
            ]);
        } else {
            return Validator::make($data, [
                'name' => ['required', 'string'],
                'username' => ['required', 'string','unique:users'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'alamat' => 'required',
                'group' => 'required',
                'phone_number' => ['required','unique:clients','regex:/(628)[0-9]{9,}/'],
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $group = 3;
        if ($data['group'] === 'kementerian-pupr') {
            $group = 1;
        } elseif ($data['group'] === 'pemerintahan-lainnya') {
            $group = 2;
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
            'level' => 1,
            'status' => 'aktif'
        ]);

        $user->assignRole('pelanggan');
        $user->client()->create([
            'name' => $data['name'],
            'alamat' => $data['alamat'],
            'phone_number' => $data['phone_number'],
            'nama_perusahaan' => $data['nama_instansi'] ?? null,
            'group' => $group,
            'nomor' => $data['nip'],
            'instansi_id' => $data['unit_organisasi'],
            'unit_kerja_id' => $data['unit_kerja']
        ]);

        if (intval($group) === 1 || intval($group) === 2) {
            $user->givePermissionTo('submit.advisi-teknis');
        }

        return $user;
    }

    protected function registered(Request $request, $user)
    {
        if ($request->has('return')) {
            return redirect()->to($request->query('return'));
        }

        return redirect()->to($this->redirectTo);
    }
}
