<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\KebijakanPelayanan;
use App\Models\LogAktivitas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KebijakanPelayananController extends Controller
{
    public function create()
    {
    	$data = KebijakanPelayanan::find(1);

    	return view('admin.kebijakan_pelayanan', compact('data'));
    }

    public function store(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'misi' => 'required|string|max:65534',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        // $text = "";

        // foreach ($request->misi as $key => $value) {
        // 	$text = $text.$value.";";
        // }

        KebijakanPelayanan::find(1)->update([
            'text'  => $request->misi
        	// 'text' => (!empty($request->misi)) ? join(";", $request->misi) : null,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Kebijakan Pelayanan"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function store_img(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'img' => 'required|image|mimes:png,jpg,jpeg'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $name = $request->file('img');
        $logo = time()."_".$name->getClientOriginalName();
        $request->img->move("gambar/kebijakan_pelayanan/", $logo);

        KebijakanPelayanan::first()->update([
            'url_img' => '/gambar/kebijakan_pelayanan/'.$logo,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update IMG Kebijakan Pelayanan"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function store_pdf(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'pdf' => 'required|file|mimes:pdf'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $name = $request->file('pdf');
        $logo = time()."_".$name->getClientOriginalName();
        $request->pdf->move("pdf/kebijakan_pelayanan/", $logo);

        KebijakanPelayanan::first()->update([
            'url_pdf' => '/pdf/kebijakan_pelayanan/'.$logo,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update PDF Kebijakan Pelayanan"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function status_homepage($id)
    {
        $inArray = [1,2,3];

        if (in_array($id, $inArray) != true) {
            return back()->with('failed', __('404 URL tidak ada!'));
        }

        if ($id == 1) {
            KebijakanPelayanan::first()->update([
                'status_img' => 1,
                'status_pdf' => 0
            ]);
        }elseif($id == 2) {
            KebijakanPelayanan::first()->update([
                'status_img' => 0,
                'status_pdf' => 1
            ]);
        } elseif ($id == 3) {
            KebijakanPelayanan::first()->update([
                'status_img' => 0,
                'status_pdf' => 0
            ]);
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Change Homepage Kebijakan Pelayanan"
        ]);

        return back()->with('success', __('Berhasil update'));
    }
}
