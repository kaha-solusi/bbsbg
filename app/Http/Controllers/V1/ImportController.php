<?php

namespace App\Http\Controllers;

use App\Imports\PegawaiImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    public function pegawai(Request $request)
    {
        Excel::import(new PegawaiImport, $request->file('file'));
        return back()->with('success', 'Import File Berhasil !');
    }

    // public function peralatan_lab(Request $request, $id)
    // {
    //     Excel::import(new )
    // }
}
