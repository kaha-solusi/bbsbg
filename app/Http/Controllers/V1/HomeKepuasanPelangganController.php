<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\FrontKepuasanPelanggan;
use App\Models\LogAktivitas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeKepuasanPelangganController extends Controller
{
    public function create()
    {
    	$data = FrontKepuasanPelanggan::find(1);

    	return view('admin.kepuasan_pelanggan', compact('data'));
    }

    public function store_img(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'img' => 'required|image|mimes:png,jpg,jpeg'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $name = $request->file('img');
        $logo = time()."_".$name->getClientOriginalName();
        $request->img->move("gambar/kepuasan_pelanggan/", $logo);

        FrontKepuasanPelanggan::first()->update([
            'url_img' => '/gambar/kepuasan_pelanggan/'.$logo,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update IMG Kepuasan Pelanggan"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function store_pdf(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'pdf' => 'required|file|mimes:pdf'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $name = $request->file('pdf');
        $logo = time()."_".$name->getClientOriginalName();
        $request->pdf->move("pdf/kepuasan_pelanggan/", $logo);

        FrontKepuasanPelanggan::first()->update([
            'url_pdf' => '/pdf/kepuasan_pelanggan/'.$logo,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update PDF Kepuasan Pelanggan"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function status_homepage($id)
    {
        $inArray = [1,2,3];

        if (in_array($id, $inArray) != true) {
            return back()->with('failed', __('404 URL tidak ada!'));
        }

        if ($id == 1) {
            FrontKepuasanPelanggan::first()->update([
                'status_img' => 1,
                'status_pdf' => 0
            ]);
        }elseif($id == 2) {
            FrontKepuasanPelanggan::first()->update([
                'status_img' => 0,
                'status_pdf' => 1
            ]);
        } elseif ($id == 3) {
            FrontKepuasanPelanggan::first()->update([
                'status_img' => 0,
                'status_pdf' => 0
            ]);
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Change Homepage Kepuasan Pelanggan"
        ]);

        return back()->with('success', __('Berhasil update'));
    }
}
