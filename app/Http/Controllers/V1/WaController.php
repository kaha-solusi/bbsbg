<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\SettingApp;
use App\Models\LogAktivitas;

class WaController extends Controller
{
    public function create()
    {
    	$data = SettingApp::find(1);

    	return view('admin.set_no_wa', compact('data'));
    }

    public function store(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'nomor_wa' => 'required|string|regex:/(628)[0-9]{9}/'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        SettingApp::find(1)->update($request->only('nomor_wa'));

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Nomor WhatsApp"
        ]); 

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }
}
