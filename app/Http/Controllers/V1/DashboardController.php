<?php

namespace App\Http\Controllers\V1;

use App\DataTables\Admin\PeralatanUsageDatatable;
use App\Http\Controllers\Controller;
use App\Models\Kegiatan;
use App\Models\PeralatanLabUsage;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PeralatanUsageDatatable $dataTable)
    {
        $jan = Kegiatan::where('lab_id', 1)->whereMonth('waktu_pelaksanaan', '01')->count();
        $feb = Kegiatan::where('lab_id', 1)->whereMonth('waktu_pelaksanaan', '02')->count();
        $mar = Kegiatan::where('lab_id', 1)->whereMonth('waktu_pelaksanaan', '03')->count();
        $apl = Kegiatan::where('lab_id', 1)->whereMonth('waktu_pelaksanaan', '04')->count();
        $mei = Kegiatan::where('lab_id', 1)->whereMonth('waktu_pelaksanaan', '05')->count();
        $june = Kegiatan::where('lab_id', 1)->whereMonth('waktu_pelaksanaan', '06')->count();
        $july = Kegiatan::where('lab_id', 1)->whereMonth('waktu_pelaksanaan', '07')->count();
        $aug = Kegiatan::where('lab_id', 1)->whereMonth('waktu_pelaksanaan', '08')->count();
        $sep = Kegiatan::where('lab_id', 1)->whereMonth('waktu_pelaksanaan', '09')->count();
        $oct = Kegiatan::where('lab_id', 1)->whereMonth('waktu_pelaksanaan', '10')->count();
        $nov = Kegiatan::where('lab_id', 1)->whereMonth('waktu_pelaksanaan', '11')->count();
        $des = Kegiatan::where('lab_id', 1)->whereMonth('waktu_pelaksanaan', '12')->count();
        $jan2 = Kegiatan::where('lab_id', 2)->whereMonth('waktu_pelaksanaan', '01')->count();
        $feb2 = Kegiatan::where('lab_id', 2)->whereMonth('waktu_pelaksanaan', '02')->count();
        $mar2 = Kegiatan::where('lab_id', 2)->whereMonth('waktu_pelaksanaan', '03')->count();
        $apl2 = Kegiatan::where('lab_id', 2)->whereMonth('waktu_pelaksanaan', '04')->count();
        $mei2 = Kegiatan::where('lab_id', 2)->whereMonth('waktu_pelaksanaan', '05')->count();
        $june2 = Kegiatan::where('lab_id', 2)->whereMonth('waktu_pelaksanaan', '06')->count();
        $july2 = Kegiatan::where('lab_id', 2)->whereMonth('waktu_pelaksanaan', '07')->count();
        $aug2 = Kegiatan::where('lab_id', 2)->whereMonth('waktu_pelaksanaan', '08')->count();
        $sep2 = Kegiatan::where('lab_id', 2)->whereMonth('waktu_pelaksanaan', '09')->count();
        $oct2 = Kegiatan::where('lab_id', 2)->whereMonth('waktu_pelaksanaan', '10')->count();
        $nov2 = Kegiatan::where('lab_id', 2)->whereMonth('waktu_pelaksanaan', '11')->count();
        $des2 = Kegiatan::where('lab_id', 2)->whereMonth('waktu_pelaksanaan', '12')->count();
        // dd($maret);

        // return view('admin.dashboard', );

        $years = PeralatanLabUsage::pluck('created_at')->map(function ($item) {
            return $item->format('Y');
        })->unique()->toArray();
        return $dataTable->render('admin.dashboard', compact([
            'jan',
            'feb',
            'mar',
            'apl',
            'mei',
            'june',
            'july',
            'aug',
            'sep',
            'oct',
            'nov',
            'des',
            'jan2',
            'feb2',
            'mar2',
            'apl2',
            'mei2',
            'june2',
            'july2',
            'aug2',
            'sep2',
            'oct2',
            'nov2',
            'des2',
            'years',
        ]));
    }

    public function getTrackingTables()
    {
        return '';
    }
}
