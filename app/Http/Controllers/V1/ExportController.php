<?php

namespace App\Http\Controllers;

use App\Exports\ClientExport;
use App\Exports\PegawaiExport;
use App\Exports\PermintaanPengujianExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function pegawai()
    {
        return Excel::download(new PegawaiExport, time().'-pegawai.xlsx');
    }
    public function client()
    {
        return Excel::download(new ClientExport, time().'-client.xlsx');
    }
    public function permintaan_pengujian()
    {
        return Excel::download(new PermintaanPengujianExport, time().'-permintaan-pengujian.xlsx');
    }
}
