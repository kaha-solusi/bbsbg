<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\TugasFungsi;
use App\Models\LogAktivitas;

class TugasFungsiController extends Controller
{
    public function create()
    {
    	$data = TugasFungsi::find(1);

    	$fungsi = explode(";", $data->fungsi);

    	return view('admin.tupoksi', compact('data', 'fungsi'));
    }

    public function store(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'tugas' => 'required|string|max:65534',
            'fungsi.*' => 'required|string|max:65534',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $text = "";

        foreach ($request->fungsi as $key => $value) {
        	$text = $text.$value.";";
        }

        TugasFungsi::find(1)->update([
        	'tugas' => $request->tugas,
        	'fungsi' => $text
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Tupoksi"
        ]); 

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    

    public function store_img(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'img' => 'required|image|mimes:png,jpg,jpeg'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $name = $request->file('img');
        $logo = time()."_".$name->getClientOriginalName();
        $request->img->move("gambar/tupoksi/", $logo);

        TugasFungsi::first()->update([
            'url_img' => '/gambar/tupoksi/'.$logo,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update IMG Tupoksi"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function store_pdf(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'pdf' => 'required|file|mimes:pdf'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $name = $request->file('pdf');
        $logo = time()."_".$name->getClientOriginalName();
        $request->pdf->move("pdf/tupoksi/", $logo);

        TugasFungsi::first()->update([
            'url_pdf' => '/pdf/tupoksi/'.$logo,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update PDF Tupoksi"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function status_homepage($id)
    {
        $inArray = [1,2,3];

        if (in_array($id, $inArray) != true) {
            return back()->with('failed', __('404 URL tidak ada!'));
        }

        if ($id == 1) {
            TugasFungsi::first()->update([
                'status_img' => 1,
                'status_pdf' => 0
            ]);
        }elseif($id == 2) {
            TugasFungsi::first()->update([
                'status_img' => 0,
                'status_pdf' => 1
            ]);
        } elseif ($id == 3) {
            TugasFungsi::first()->update([
                'status_img' => 0,
                'status_pdf' => 0
            ]);
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Change Homepage Tupoksi"
        ]);

        return back()->with('success', __('Berhasil update'));
    }
}
