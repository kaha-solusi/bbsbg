<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Sejarah;
use App\Models\LogAktivitas;

class SejarahController extends Controller
{
    public function create()
    {
    	$data = Sejarah::first();

        $sejarah = explode(";", $data->isi);

    	return view('admin.sejarah', compact('data', 'sejarah'));
    }

    public function store(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'isi*' => 'required|string|max:65534'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }
        $row = [];
        for($i= 0; $i < count($request->isi); $i++) {
            array_push($row, $request->tahun[$i] . "|". $request->isi[$i]);
        }
        Sejarah::first()->update([
            'isi' => (!empty($request->isi)) ? join(';', $row) : null,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Sejarah"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function store_img(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'img' => 'required|image|mimes:png,jpg,jpeg'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $name = $request->file('img');
        $logo = time()."_".$name->getClientOriginalName();
        $request->img->move("gambar/sejarah/", $logo);

        Sejarah::first()->update([
            'url_img' => '/gambar/sejarah/'.$logo,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update IMG Sejarah"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function store_pdf(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'pdf' => 'required|file|mimes:pdf'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $name = $request->file('pdf');
        $logo = time()."_".$name->getClientOriginalName();
        $request->pdf->move("pdf/sejarah/", $logo);

        Sejarah::first()->update([
            'url_pdf' => '/pdf/sejarah/'.$logo,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update PDF Sejarah"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function status_homepage($id)
    {
        $inArray = [1,2,3];

        if (in_array($id, $inArray) != true) {
            return back()->with('failed', __('404 URL tidak ada!'));
        }

        if ($id == 1) {
            Sejarah::first()->update([
                'status_img' => 1,
                'status_pdf' => 0
            ]);
        }elseif($id == 2) {
            Sejarah::first()->update([
                'status_img' => 0,
                'status_pdf' => 1
            ]);
        } elseif ($id == 3) {
            Sejarah::first()->update([
                'status_img' => 0,
                'status_pdf' => 0
            ]);
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Change Homepage Sejarah"
        ]);

        return back()->with('success', __('Berhasil update'));
    }
}
