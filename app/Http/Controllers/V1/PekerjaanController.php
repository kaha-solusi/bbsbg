<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Pekerjaan;
use App\Models\Lab;
use App\Models\Client;
use App\Models\Pejabat;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class PekerjaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pekerjaan::with('lab', 'client')->orderBy('id', 'asc')->paginate(10);

        return view('admin.pekerjaan', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $labs = Lab::all();
        $clients = Client::all();

        return view('admin.pekerjaan-p.create', compact('labs', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'nama' => 'required|string',
            'waktu_pelaksanaan' => 'required',
            'lab_id' => 'required|exists:labs,id',
            'client_id' => 'required|exists:clients,id',
            'penaggung_jawab' => 'required|string',
            'pembiayaan' => 'required|integer|min:0',
            'keterangan' => 'required|string|max:65534'
        ]);

        if ($v->fails()) {
            // dd($v);
            return back()->withErrors($v)->withInput();
        }

        $data = Pekerjaan::create($request->only('nama', 'waktu_pelaksanaan', 'lab_id', 'client_id', 'penaggung_jawab', 'pembiayaan', 'keterangan')+[
            'time' => now('Asia/Jakarta'),
            'status_pelaksanaan' => '0'
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Pejabat::findOrFail($id);

        return view('admin.pejabat-p.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(),[
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'nama' => 'required|string',
            'job' => 'required|string',
            'quote' => 'required|string|max:65534'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = Pejabat::findOrFail($id);

        $data->update($request->only('nama', 'job', 'quote'));

        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("upload/foto/pejabat", $logo);

            $data->update([
                'foto' => '/upload/foto/pejabat/'.$logo
            ]);         
        }


        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pejabat::findOrFail($id);

        // File::delete($data->foto);

        $data->delete();

        return back()->with('success', __( 'Data berhasil dihapus.' ));
    }
}
