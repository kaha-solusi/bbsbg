<?php

namespace App\Http\Controllers;

use App\Models\GaleriVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GaleriVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.galerivideo', [
            'video' => GaleriVideo::get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.galerivideo-p.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'video' => 'required|url',
        ]);
        if($validation->fails()){
            return back()->withErrors($validation)->withInput();
        }
        GaleriVideo::create($request->all());
        return back()->with('success', 'Berhasil di Update !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GaleriVideo  $galeriVideo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GaleriVideo  $galeriVideo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.galerivideo-p.edit', [
            'video' => GaleriVideo::findOrFail($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GaleriVideo  $galeriVideo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'video' => 'required|url',
        ]);
        if($validation->fails()){
            return back()->withErrors($validation)->withInput();
        }
        $video = GaleriVideo::findOrFail($id);
        $video->update($request->all());
        return back()->with('success', 'Berhasil di Update !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GaleriVideo  $galeriVideo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = GaleriVideo::findOrFail($id);
        $video->delete();
        return back()->with('success', 'Berhasil di Delete !');
    }
}
