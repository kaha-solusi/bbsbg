<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kegiatan;
use App\Models\Lab;
use App\Models\LogAktivitas;
use Illuminate\Support\Facades\Validator;

class JadwalBimtekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Kegiatan::with('lab')
        ->where('kategori', 'bimtek')
        ->orderBy('id', 'desc')
        ->paginate(10);

        $search = $request->query('search', null);

        if ($search != null) {
            $data = Kegiatan::where('nama_kegiatan', 'like', '%'.$search.'%')
            ->where('kategori', 'bimtek')
            ->orWhere(function($query)use($search){
                $query->where('waktu_pelaksanaan', 'like', '%'.$search.'%')
                    ->where('kategori', 'bimtek');
            })
            ->orWhere(function($query)use($search){
                $query->where('tempat_pelaksanaan', 'like', '%'.$search.'%')
                    ->where('kategori', 'bimtek');
            })
            ->orWhere(function($query)use($search){
                $query->where('biaya_kegiatan', 'like', '%'.$search.'%')
                    ->where('kategori', 'bimtek');
            })
            ->orWhere(function($query)use($search){
                $query->where('status', 'like', '%'.$search.'%')
                    ->where('kategori', 'bimtek');
            })
            ->orderBy('id', 'desc')
            ->paginate(10);
        }

        // foreach (Kegiatan::all() as $value) {
        //     if (date('Y-m-d', strtotime($value->waktu)) == date('Y-m-d', strtotime(now('Asia/Jakarta')))) {
        //         Kegiatan::find($value->id)->update([
        //             'status' => 'sedang dilaksanakan'
        //         ]);
        //     }elseif (date('Y-m-d', strtotime($value->waktu)) > date('Y-m-d', strtotime(now('Asia/Jakarta')))) {
        //         Kegiatan::find($value->id)->update([
        //             'status' => 'selesai'
        //         ]);
        //     }
        // }   

        return view('admin.jadwal-bimtek.table', compact('data', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Lab::all();
        return view('admin.jadwal-bimtek.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'nama_kegiatan' => 'required|string',
            'waktu_pelaksanaan' => 'required',
            'deskripsi_kegiatan' => 'required|string|max:65534',
            'tempat_pelaksanaan' => 'required|string|max:65534',
            'biaya_kegiatan' => 'required|integer|min:0',
            'lab_id' => 'required|exists:labs,id'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        Kegiatan::create($request->only([
            'nama_kegiatan',
            'waktu_pelaksanaan',
            'deskripsi_kegiatan',
            'tempat_pelaksanaan',
            'biaya_kegiatan',
            'lab_id'
        ])+[
            'status' => 'diproses',
            'kategori' => 'bimtek',
            'waktu' => now('Asia/Jakarta')
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Create Jadwal Bimtek"
        ]);

        return back()->with('success', __( 'Berhasil dibuat!' ));
    }

    public function statusDilaksanakan($id)
    {
        $data = Kegiatan::findOrFail($id);

        $data->update([
            'status' => 'dilaksanakan'
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Change Status Kegiatan"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function statusTidakDilaksanakan($id)
    {
        $data = Kegiatan::findOrFail($id);

        $data->update([
            'status' => 'tidak dilaksanakan'
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Change Status Kegiatan"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function statusSedangDilaksanakan($id)
    {
        $data = Kegiatan::findOrFail($id);

        $data->update([
            'status' => 'sedang dilaksanakan'
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Change Status Kegiatan"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function statusSelesaiDilaksanakan($id)
    {
        $data = Kegiatan::findOrFail($id);

        $data->update([
            'status' => 'selesai'
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Change Status Kegiatan"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Kegiatan::findOrFail($id);

        $data->delete();

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Delete Jadwal Bimtek"
        ]);

        return back()->with('success', __( 'Berhasil dihapus!' ));
    }

    public function home()
    {
        $jadwal = Kegiatan::with('lab')
        ->where('kategori', 'bimtek')
        ->orderBy('id', 'desc')
        ->paginate(10);

        return view('homepage.jadwalbimtek', compact('jadwal'));
    }
}
