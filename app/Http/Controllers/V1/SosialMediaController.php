<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SettingApp;
use Illuminate\Support\Facades\Validator;
use App\Models\LogAktivitas;

class SosialMediaController extends Controller
{
    public function create()
    {
    	$data = SettingApp::find(1);

    	return view('admin.media_sosial', compact('data'));
    }

    public function store(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'link_twitter' => 'nullable|string',
            'link_fb' => 'nullable|string',
            'link_yt' => 'nullable|string',
            'link_ig' => 'nullable|string'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $fb = ($request->link_fb == null) ? '#' : $request->link_fb ;
        $twitter = ($request->link_twitter == null) ? '#' : $request->link_twitter ;
        $yt = ($request->link_yt == null) ? '#' : $request->link_yt ;
        $ig = ($request->link_ig == null) ? '#' : $request->link_ig ;


        SettingApp::find(1)->update([
        	'link_ig' => $ig,
        	'link_yt' => $yt,
        	'link_twitter' => $twitter,
        	'link_facebook' => $fb
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Sosial Media"
        ]); 

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }
}
