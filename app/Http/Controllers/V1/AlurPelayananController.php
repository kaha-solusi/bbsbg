<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\AlurPelayanan;
use App\Models\LogAktivitas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AlurPelayananController extends Controller
{
    public function create()
    {
    	$data = AlurPelayanan::find(1);
    	$misi = explode(";", $data->text);

    	return view('admin.alur-pelayanan', compact('data', 'misi'));
    }

    public function store(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'misi.*' => 'required|string|max:65534',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        // $text = "";

        // foreach ($request->misi as $key => $value) {
        // 	$text = $text.$value.";";
        // }

        AlurPelayanan::find(1)->update([
        	'text' => (!empty($request->misi)) ? join(";", $request->misi) : null,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Visi Misi"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function store_img(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'img' => 'required|image|mimes:png,jpg,jpeg'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $name = $request->file('img');
        $logo = time()."_".$name->getClientOriginalName();
        $request->img->move("gambar/alur_pelayanan/", $logo);

        AlurPelayanan::first()->update([
            'img' => '/gambar/alur_pelayanan/'.$logo,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update IMG Visi Misi"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function store_pdf(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'pdf' => 'required|file|mimes:pdf'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $name = $request->file('pdf');
        $logo = time()."_".$name->getClientOriginalName();
        $request->pdf->move("pdf/alur_pelayanan/", $logo);

        AlurPelayanan::first()->update([
            'pdf' => '/pdf/alur_pelayanan/'.$logo,
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update PDF Visi Misi"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function status_homepage($id)
    {
        $inArray = [1,2,3];

        if (in_array($id, $inArray) != true) {
            return back()->with('failed', __('404 URL tidak ada!'));
        }

        if ($id == 1) {
            AlurPelayanan::first()->update([
                'status_img' => 1,
                'status_pdf' => 0
            ]);
        }elseif($id == 2) {
            AlurPelayanan::first()->update([
                'status_img' => 0,
                'status_pdf' => 1
            ]);
        } elseif ($id == 3) {
            AlurPelayanan::first()->update([
                'status_img' => 0,
                'status_pdf' => 0
            ]);
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Change Homepage Visi Misi"
        ]);

        return back()->with('success', __('Berhasil update'));
    }
}
