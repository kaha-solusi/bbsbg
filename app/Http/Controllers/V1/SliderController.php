<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    public function index()
    {
        return view('admin.slider', [
            'slider' => Slider::get(),
        ]);
    }
    public function create()
    {
        return view('admin.slider-p.create');
    }
    public function edit($id)
    {
        return view('admin.slider-p.edit', [
            'slider' => Slider::findOrFail($id),
        ]);
    }
    public function store(Request $request)
    {
        //validasi
        $validator = Validator::make($request->all(), [
            'foto' => 'required|image|mimes:jpg,png,jpeg|max:2048',
            'keterangan' => 'required',
            'judul' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = Slider::create($request->all());
        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("slider/foto/", $logo);

            Slider::find($data->id)->update([
                'foto' => 'slider/foto/'.$logo
            ]); 
        }
        return back()->with('success', 'Berhasil di Update !');
    }
    public function update(Request $request, $id)
    {
        //validasi
        $validator = Validator::make($request->all(), [
            'foto' => 'image|mimes:jpg,png,jpeg|max:2048',
            'keterangan' => 'required',
            'judul' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }

        // Attribute yg akan di input
        $data = Slider::findOrFail($id);
        $data->update($request->all());
        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("slider/foto/", $logo);

            Slider::find($data->id)->update([
                'foto' => 'slider/foto/'.$logo
            ]); 
        }
        return back()->with('success', 'Berhasil di Update !');
    }
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);
        $slider->delete();
        return back()->with('success', 'Berhasil di Hapus !');
    }
}
