<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Galeri;
use App\Models\GaleriVideo;
use App\Models\LogAktivitas;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class GaleriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Galeri::orderBy('id', 'desc')->paginate(10);

        return view('admin.galeri', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.galeri-p.create');
    }
    public function createVideo()
    {
        return view('admin.galerivideo-p.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'foto' => Rule::requiredIf($request->video == null).'|image|mimes:jpg,png,jpeg|max:2048',
            'video' => Rule::requiredIf($request->foto == null).'|url',
            'kategori' => 'required',
            'judul' => 'required|string',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = Galeri::create($request->all());

            // File::delete($data->foto);

        if(!empty($request->file('foto')))
        {
            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("galeri/foto", $logo);
    
            Galeri::find($data->id)->update([
                'foto' => '/galeri/foto/'.$logo
            ]);             
        }
        
        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Create Galeri Foto"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Galeri::findOrFail($id);

        return view('admin.galeri-p.edit', compact('data'));
    }
    public function editVideo($id)
    {
        $data = Galeri::findOrFail($id);
        return view('admin.galerivideo-p.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(),[
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'video' => 'nullable|url',
            'kategori' => 'required|not_in:0',
            'judul' => 'required|string',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = Galeri::findOrFail($id);

        $data->update($request->all());

        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("galeri/foto", $logo);

            $data->update([
                'foto' => '/galeri/foto/'.$logo
            ]);        
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Galeri Foto"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Galeri::findOrFail($id);

        // File::delete($data->foto);

        $data->delete();

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Delete Galeri Foto"
        ]);

        return back()->with('success', __( 'Data berhasil dihapus.' ));
    }
}
