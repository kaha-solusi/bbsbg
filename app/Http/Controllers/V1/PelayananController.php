<?php

namespace App\Http\Controllers;

use App\Models\Pelayanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class PelayananController extends Controller
{

    public function index()
    {
        return view('admin.pelayanan', [
            'pelayanan' => Pelayanan::get(),
        ]);
    }

    public function create()
    {
        return view('admin.pelayanan-p.create');
    }

    public function store(Request $request)
    {
        dd($request->all());
        //validasi
        $validator = Validator::make($request->all(), [
            'file' => 'nullable|mimes:pdf|max:10240',
            'nama' => 'nullable',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = Pelayanan::create($request->only(['nama', 'keterangan']));
        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("pelayanan/foto/", $logo);

            Pelayanan::find($data->id)->update([
                'foto' => 'pelayanan/foto/'.$logo,
            ]);
        }
        // Attribute yg akan di input
        // $pdf = $request->file('file');
        // $attr = $request->all();
        // $attr['file'] = Storage::putFileAs(
        //     'pelayanan/pdf',
        //     $pdf,
        //     time()."_".$pdf->getClientOriginalName()
        // );
        // Pelayanan::create($attr);
        return back()->with('success', 'Berhasil di Update !');
    }

    public function edit($id)
    {
        return view('admin.pelayanan-p.edit', [
            'pelayanan' => Pelayanan::findOrFail($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        //validasi
        $validator = Validator::make($request->all(), [
            'file' => 'mimes:pdf|max:10240',
            'nama' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = Pelayanan::findOrFail($id);
        $data->update($request->all());
        if ($request->file('file')) {
            // File::delete($data->file);

            $name = $request->file('file');
            $logo = time()."_".$name->getClientOriginalName();
            $request->file->move("pelayanan/file/", $logo);

            Pelayanan::find($data->id)->update([
                'file' => 'pelayanan/file/'.$logo,
            ]);
        }
        return back()->with('success', 'Berhasil di Update !');
    }
    public function destroy($id)
    {
        $pelayanan = Pelayanan::findOrFail($id);
        Storage::delete($pelayanan->file);
        $pelayanan->delete();
        return back()->with('success', 'Berhasil di Hapus !');
    }
}
