<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Pengajar;
use App\Models\LogAktivitas;
use Illuminate\Support\Facades\File;

class PengajarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Pengajar::orderBy('nama', 'asc')->paginate(10);

        $search = $request->query('search', null);

        if ($search != null) {
            $data = Pengajar::where('nama', 'like', '%'.$search.'%')
            ->orWhere('job', 'like', '%'.$search.'%')
            ->orderBy('id', 'desc')
            ->paginate(10);
        }

        return view('admin.pengajar', compact('data', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pengajar-p.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'nama' => 'required|string',
            'job' => 'required|string',
            'keterangan' => 'required|string|max:65534',
            'link_ig' => 'nullable|url',
            'link_likedin' => 'nullable|url',
            'link_facebook' => 'nullable|url',
            'link_twitter' => 'nullable|url'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = Pengajar::create($request->only('nama', 'job', 'keterangan', 'link_ig', 'link_likedin', 'link_facebook', 'link_twitter'));

        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("upload/foto/pengajar", $logo);

            Pengajar::find($data->id)->update([
                'foto' => '/upload/foto/pengajar/'.$logo
            ]);         
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Create Pengajar"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Pengajar::findOrFail($id);

        return view('admin.pengajar-p.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(),[
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'nama' => 'required|string',
            'job' => 'required|string',
            'keterangan' => 'required|string|max:65534',
            'link_ig' => 'nullable|url',
            'link_likedin' => 'nullable|url',
            'link_facebook' => 'nullable|url',
            'link_twitter' => 'nullable|url'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = Pengajar::findOrFail($id);

        $data->update($request->only('nama', 'job', 'keterangan', 'link_ig', 'link_likedin', 'link_facebook', 'link_twitter'));

        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("upload/foto/pengajar", $logo);

            $data->update([
                'foto' => '/upload/foto/pengajar/'.$logo
            ]);         
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Pengajar"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pengajar::findOrFail($id);

        // File::delete($data->foto);

        $data->delete();

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Delete Pengajar"
        ]);

        return back()->with('success', __( 'Data berhasil dihapus.' ));
    }
}
