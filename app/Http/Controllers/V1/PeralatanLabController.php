<?php

namespace App\Http\Controllers\V1;

use App\DataTables\Admin\PeralatanLabDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\PeralatanLabStoreRequest;
use App\Models\PeralatanLabCalibration;
use App\Models\PeralatanLabUsage;
use Illuminate\Http\Request;
use App\Models\PeralatanLab;
use App\Models\Lab;
use App\Models\LogAktivitas;

class PeralatanLabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PeralatanLabDatatable $dataTable)
    {
        return $dataTable->render('admin.peralatan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Lab::all();

        return view('admin.peralatan-p.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PeralatanLabStoreRequest $request)
    {
        try {
            $data = PeralatanLab::create($request->except([
                'id',
                'foto_alat',
                'created_at',
                'updated_at',
                'tanggal_kalibrasi_ulang',
                'tanggal_kalibrasi_ulang',
                'hasil_verifikasi_alat',
                'jadwal_pemeliharaan',
                'akses_pengguna',
                'proengsoft_jsvalidation',
            ]));

            if ($request->file('foto_alat')) {
                // File::delete($data->foto);

                $name = $request->file('foto_alat');
                $logo = time()."_".$name->getClientOriginalName();
                $request->foto_alat->move("upload/foto_alat", $logo);

                PeralatanLab::find($data->id)->update([
                    'foto_alat' => '/upload/foto_alat/'.$logo,
                ]);
            }

            LogAktivitas::create([
                'user_id' => auth()->user()->id,
                'date' => now('Asia/Jakarta'),
                'log' => "Create Peralatan Lab"
            ]);

            return redirect()
                ->route('admin.peralatan.index')
                ->with('success', __('Berhasil menambahkan peralatan baru!'));
        } catch (\Throwable $th) {
            return back()->withErrors($th->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, PeralatanLab $peralatan)
    {
        $calibrations = PeralatanLabCalibration::where('peralatan_lab_id', $peralatan->id)
            ->orderByDesc('created_at')
            ->limit(3)
            ->get();

        $pemakaians = PeralatanLabUsage::where([['peralatan_lab_id', '=', $peralatan->id ], [ 'kategori', '=', 'Pemakaian']])
            ->orderByDesc('created_at')
            ->limit(5)
            ->get();
        $perbaikans = PeralatanLabUsage::where([['peralatan_lab_id', '=', $peralatan->id ], [ 'kategori', '=', 'Perbaikan']])
            ->orderByDesc('created_at')
            ->limit(5)
            ->get();
        $pemeliharaans = PeralatanLabUsage::where([['peralatan_lab_id', '=', $peralatan->id ], [ 'kategori', '=', 'Pemeliharaan']])
            ->orderByDesc('created_at')
            ->limit(5)
            ->get();
        $user = auth()->user();

        if ($request->ajax()) {
            return "<table>
                        <tr>
                            <td class='font-weight-bold' width='50'>Nama</td>
                            <td>:</td>
                            <td>$peralatan->nama</td>
                        </tr>
                        <tr>
                            <td class='font-weight-bold' width='50'>Merek</td>
                            <td>:</td>
                            <td>$peralatan->merek</td>
                        </tr>
                        <tr>
                            <td class='font-weight-bold' width='50'>Tipe</td>
                            <td>:</td>
                            <td>$peralatan->type</td>
                        </tr>
                        <tr>
                            <td class='font-weight-bold' width='50'>Seri</td>
                            <td>:</td>
                            <td>$peralatan->seri</td>
                        </tr>
                        <tr>
                            <td class='font-weight-bold' width='50'>Jumlah</td>
                            <td>:</td>
                            <td>$peralatan->jumlah</td>
                        </tr>
                    </table>";
        }

        return view('admin.peralatan.show', compact('peralatan', 'calibrations', 'user', 'pemakaians', 'pemeliharaans', 'perbaikans'));
    }

    public function showBarcode(PeralatanLab $alat)
    {
        return '<div class="text-center d-inline-block p-4" id="bcsimpan">'.generateBarcode($alat->seri, 10, 10).'<p>'.$alat->nama.' - '.$alat->seri.'</p></div>';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $labs = Lab::all();
        $data = PeralatanLab::findOrFail($id);

        return view('admin.peralatan-p.edit', compact('data', 'labs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PeralatanLabStoreRequest $request, $id)
    {
        try {
            $data = PeralatanLab::findOrFail($id);

            $data->update($request->except([
                'id',
                'foto_alat',
                'created_at',
                'updated_at',
                'tanggal_kalibrasi_ulang',
                'tanggal_kalibrasi_ulang',
                'hasil_verifikasi_alat',
                'jadwal_pemeliharaan',
                'akses_pengguna',
            ]));

            if ($request->file('foto_alat')) {
                // File::delete($data->foto);

                $name = $request->file('foto_alat');
                $logo = time()."_".$name->getClientOriginalName();
                $request->foto_alat->move("upload/foto_alat", $logo);

                PeralatanLab::find($data->id)->update([
                    'foto_alat' => '/upload/foto_alat/'.$logo,
                ]);
            }

            LogAktivitas::create([
                'user_id' => auth()->user()->id,
                'date' => now('Asia/Jakarta'),
                'log' => "Update Peralatan Lab"
            ]);

            return redirect()
                ->route('admin.peralatan.index')
                ->with('success', __('Berhasil menambahkan peralatan baru!'));
        } catch (\Throwable $th) {
            report($th);
            dd($th);
            return back()->withErrors($th->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PeralatanLab::findOrFail($id);

        $data->delete();

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Delete Peralatan Lab"
        ]);

        return back()->with('success', __('Data berhasil dihapus.'));
    }

    public function getPeralatanByName(Request $request)
    {
        $data = PeralatanLab::where('nama', 'LIKE', '%'.$request->term.'%')
            ->orWhere('seri', 'LIKE', '%'.$request->term.'%')
            ->select('id', 'nama as text', 'foto_alat', 'seri')
            ->limit(10)
            ->get()
            ->toArray();
        return response()->json([
            'results' => $data,
            'pagination' => [
                'more' => false,
            ],
        ]);
    }

    public function getPeralatanById(Request $request)
    {
        $data = PeralatanLab::where('seri', $request->id)->select([
            'id', 'nama as text', 'foto_alat', 'seri',
        ])->first();
        // $data = PeralatanLab::find($request->id, [
        //     'id', 'nama as text', 'foto_alat', 'seri',
        // ]);
        return response()->json($data);
    }
}
