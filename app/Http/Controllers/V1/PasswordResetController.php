<?php

namespace App\Http\Controllers\V1;

use Illuminate\Auth\Events\PasswordReset;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{
    public function forgot()
    {
    	$status = Password::sendResetLink([
    		'email' => auth()->user()->email
    	]);

    	return $status === Password::RESET_LINK_SENT
    				? back()->with(['success' => __( $status )]) 
    				: back()->withErrors(['failed' => __( $status )]);
    }

    public function reset(Request $request)
    {
    	$request->validate([
    		'token' => 'required',
    		'password' => 'required|min:8|confirmed'
    	]);
    	// dd($request->all());

    	$status = Password::reset([
	    		'email' => auth()->user()->email,
	    		'password' => $request->password,
	    		'password_confirmation' => $request->password_confirmation,
	    		'token' => $request->token
	    	],
    		function($user, $password)use($request){
    			$user->forceFill([
    				'password' => Hash::make($password)
    			])->save();

    			$user->setRememberToken(Str::random(60));

    			event(new PasswordReset($user));
    		}
    	);
    	return $status == Password::PASSWORD_RESET
    				? redirect()->route('login')->with('status', __( $status ))
    				: back()->withErrors(['failed', __( $status )]);
    }
}
