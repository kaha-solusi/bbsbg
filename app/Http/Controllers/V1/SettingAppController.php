<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Mail\PelaporanGrafikasiMail;
use App\Mail\PengaduanMasyarakatMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\SettingApp;
use Illuminate\Support\Facades\File;
use App\Models\LogAktivitas;
use Illuminate\Support\Facades\Mail;

class SettingAppController extends Controller
{
    public function create()
    {
        $data = SettingApp::find(1);
        // dd($data);
        return view('admin.setting_app', compact('data'));
    }

    public function store(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'logo' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'email' => 'required|string|email',
            'copyright' => 'required|string',
            'nama' => 'required|string',
            'nomor_kontak' => 'required|string',
            'alamat' => 'required|string|max:65534'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = SettingApp::find(1);

        $data->update($request->only('email', 'copyright', 'nomor_kontak', 'alamat', 'nama', 'hari_libur'));

        if ($request->file('logo')) {
	        // File::delete($data->logo);

	        $name = $request->file('logo');
	        $logo = time()."_".$name->getClientOriginalName();
	        $request->logo->move("setting/logo", $logo);

			SettingApp::find(1)->update([
				'logo' => '/setting/logo/'.$logo
			]);
	    }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Setting App"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function form_pengaduan(Request $request)
    {
        return view('homepage.pengaduan-masyarakat');
    }

    public function laporan_masyarakat(Request $request)
    {
        $v = Validator::make($request->all(),[
            'judul' => 'required|string|max:250',
            'isi' => 'required|string|max:1255',
            'tanggal' => 'required|string',
            'lokasi' => 'required|string|max:1255'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        if ($request->query('debug', null) == null) {
            return back()->with('success', __('Pengaduan masih dalam tahap perbaikan!'));
        }

        // $mail = new PHPMailer();
        // $mail->IsSMTP();
        // $mail->Mailer = "smtp";

        // $mail->SMTPDebug  = 1;
        // $mail->SMTPAuth   = TRUE;
        // $mail->SMTPSecure = "tls";
        // $mail->Port       = 587;
        // $mail->Host       = "makerindo.com";
        // $mail->Username   = "al@makerindo.com";
        // $mail->Password   = "47MqLcUnUicbRT7";

        // $mail->IsHTML(true);
        // $mail->AddAddress("filok5267@gmail.com", "Felix");
        // $mail->SetFrom("al@makerindo.com", "from-name");
        // $mail->AddReplyTo("al@makerindo.com", "reply-to-name");
        // $mail->AddCC("al@makerindo.com", "cc-recipient-name");
        // $mail->Subject = "Test is Test Email sent via Gmail SMTP Server using PHP Mailer";
        // $content = "<b>This is a Test Email sent via Gmail SMTP Server using PHP mailer class.</b>";

        // $mail->MsgHTML($content);
        // if(!$mail->Send()) {
        // echo "Error while sending Email.";
        //     var_dump($mail);
        // } else {
        //     echo "Email sent successfully";
        // }

        // Mail::to('filok5267@gmail.com')->send(new PengaduanMasyarakatMail($request->judul, $request->isi, $request->tanggal, $request->lokasi));

        // echo shell_exec('sudo service apache2 restart');

        // the message
        // $msg = "First line of text\nSecond line of text";

        // use wordwrap() if lines are longer than 70 characters
        // $msg = wordwrap($msg,70);

        // send email
        // mail("filok5267@gmail.com","My subject",$msg);

        return back()->with('success', __('Berhasil dikirim.'));
    }

    public function form_pelaporan()
    {
        return view('homepage.pelapor-gratifikasi');
    }

    public function pelaporan_grafikasi(Request $request)
    {
        $v = Validator::make($request->all(),[
            'nama' => 'required|string|max:250',
            'nip' => 'nullable|numeric|max:255',
            'no_telp' => 'required|numeric',
            'email' => 'required|string|email|max:255',
            'nama_pemberi' => 'required|string|max:255',
            'jabatan_pemberi' => 'required|string|max:255',
            'tanggal' => 'required|string',
            'uraian' => 'required|string|max:1255',
            'taksiran' => 'required|integer|min:0',
            'jenis' => 'required|string',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        Mail::to('filok5267@gmail.com')->send(new PelaporanGrafikasiMail($request));

        return back()->with('success', __('Berhasil dikirim.'));
    }
}
