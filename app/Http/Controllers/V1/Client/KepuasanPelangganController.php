<?php

namespace App\Http\Controllers\V1\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\KepuasanPelanggan;
use App\Models\Pelayanan;

class KepuasanPelangganController extends Controller
{
    public function index(Request $request)
    {
        $data = KepuasanPelanggan::with('pelayanan', 'user')->orderBy('id', 'desc')->paginate(10);

        $search = $request->query('search', null);

        if ($search != null) {
            $data = KepuasanPelanggan::where('nama', 'like', '%'.$search.'%')
            ->orWhere(function ($q) use ($search) {
                $q->whereHas('pelayanan', function ($query) use ($search) {
                    $query->where('nama', 'like', '%'.$search.'%');
                });
            })
            ->orWhere('tingkat_kepuasan', 'like', '%'.$search.'%')
            ->orWhere('waktu', 'like', '%'.$search.'%')
            ->orWhere('komen', 'like', '%'.$search.'%')
            ->orderBy('id', 'desc')
            ->paginate(10);
        }

        return view('client.kepuasan-pelanggan.index', compact('data', 'search'));
    }

    public function create()
    {
        $pelayanans = Pelayanan::all();
        return view('client.kepuasan-pelanggan.create', compact('pelayanans'));
    }

    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required|string|max:30',
            'pelayanan_id' => 'required|exists:pelayanans,id',
            'tingkat_kepuasan' => 'required',
            'komen' => 'nullable|string|max:65534'
        ]);

        if ($v->fails()) {
            // dd($v);
            return back()->withErrors($v)->withInput();
        }

        $data = KepuasanPelanggan::create($request->only('nama', 'pelayanan_id', 'tingkat_kepuasan', 'komen') + [
            'waktu' => now('Asia/Jakarta'),
            'user_id' => auth()->user()->id,
        ]);

        return back()->with('success', __('Berhasil dikirim!'));
    }
}
