<?php

namespace App\Http\Controllers\V1\Client;

use App\Http\Controllers\Controller;
use App\Models\PermintaanPengujian;
use App\Models\Kuisioner;
use App\Services\AdvisiTeknisService;
use App\Services\PermintaanPengujianService;
use Illuminate\Http\Request;

class PermintaanPengujianKuisionerController extends Controller
{
    private $permintaanPengujianService;

    public function __construct(PermintaanPengujianService $permintaanPengujianService)
    {
        $this->permintaanPengujianService = $permintaanPengujianService;
    }

    public function index(PermintaanPengujian $pengujian)
    {
        $kuisioners = Kuisioner::where('kategori_pelayanan_id', 1)->get();
        $route = route('permintaan-pengujian.kuisioner.store', $pengujian->id);
        return view('client.kuisioner.index', compact('kuisioners', 'route'));
    }

    public function store(Request $request)
    {
        $pengujian = PermintaanPengujian::find(request('pengujian'));
        $this->permintaanPengujianService->store_kuisioner($request->data, $pengujian);

        return redirect()->route('client.permintaan-pengujian.laporan-hasil-uji.get', request('pengujian'));
    }
}
