<?php

namespace App\Http\Controllers\V1\Client;

use App\DataTables\Client\PermintaanPengujianDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Client\PaymentConfirmationRequest;
use App\Models\File;
use App\Models\LaporanHasilUji;
use Illuminate\Http\Request;
use App\Models\PermintaanPengujian as Pendaftaran;
use App\Models\SettingApp;
use App\Models\Pelayanan;
use App\Notifications\PaymentReceivedNotification;
use Illuminate\Support\Facades\Validator;
use App\Services\PermintaanPengujianService;
use App\Services\UploadService;

/**
 * PermintaanPengujian
 *
 * @category Client
 * @package  PermintaanPengujian
 * @author   Agung Kurniawan <agungkes95@gmail.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @version  Release: 0.0.1
 * @access   public
 * @link     http://url.com
 */
class PermintaanPengujian extends Controller
{
    private $service;
    public function __construct(PermintaanPengujianService $service)
    {
        $this->service = $service;
    }

    /**
     * Index function
     *
     * @param PermintaanPengujianDatatable $dataTable
     *
     * @return mixed
     */
    public function index(PermintaanPengujianDatatable $dataTable)
    {
        return $dataTable->render('client.permintaan-pengujian.index');
    }

    /**
     * Show function
     *
     * @param Pendaftaran $pengujian
     *
     * @return mixed
     */
    public function show(Pendaftaran $permintaan_pengujian)
    {
        $pengujian = $permintaan_pengujian;
        $subTotal = collect($pengujian->items)->reduce(
            function ($prev, $current) {
                return $prev +
                    $current->layanan_uji_item->price *
                    $current->product_count;
            },
            0
        );
        $laporan_hasil_uji = LaporanHasilUji::firstWhere('pengujian_id',$pengujian->id);

        return view(
            'client.permintaan-pengujian.show',
            compact(
                'pengujian',
                'subTotal',
                'laporan_hasil_uji',
                'permintaan_pengujian'
            )
        );
    }

    public function update(Request $request, Pendaftaran $permintaan_pengujian)
    {
        $this->service->clientUnggahKonfirmasiLayanan($request, $permintaan_pengujian);
        return redirect()
            ->route('client.permintaan-pengujian.index')
            ->with('success', 'Permintaan pengujian berhasil diperbaharui');
    }

    public function listPermintaan(Request $request)
    {
        $data = Pendaftaran::where('client_id', auth()->user()->client->id)->orderBy('id', 'desc')->paginate(10);

        foreach (Pendaftaran::all() as $value) {
            if (date('Y-m-d', strtotime($value->tgl_permintaan)) > date('Y-m-d', strtotime(now('Asia/Jakarta')))) {
                Pendaftaran::find($value->id)->update(
                    [
                    'status' => 'selesai',
                    ]
                );
            }
        }

        $search = $request->query('search', null);

        if ($search != null) {
            $data = Pendaftaran::where('nama_pemohon', 'like', '%'.$search.'%')
                ->where('client_id', auth()->user()->client->id)
                ->orWhere(
                    function ($query) use ($search) {
                        $query->where('email', 'like', '%'.$search.'%')
                            ->where('client_id', auth()->user()->client->id);
                    }
                )
            ->orWhere(
                function ($query) use ($search) {
                    $query->where('tgl_permintaan', 'like', '%'.$search.'%')
                        ->where('client_id', auth()->user()->client->id);
                }
            )
            ->orWhere(
                function ($query) use ($search) {
                    $query->where('nomor_hp_kontak', 'like', '%'.$search.'%')
                        ->where('client_id', auth()->user()->client->id);
                }
            )
            ->orWhere(
                function ($query) use ($search) {
                    $query->where('status', 'like', '%'.$search.'%')
                        ->where('client_id', auth()->user()->client->id);
                }
            )
            ->orWhere(
                function ($query) use ($search) {
                    $query->whereHas(
                        'pelayanan',
                        function ($q) use ($search) {
                            $q->where('nama', 'like', '%'.$search.'%');
                        }
                    )
                    ->where('client_id', auth()->user()->client->id);
                }
            )
            ->orderBy('id', 'desc')
            ->paginate(10);
        }

        return view('admin.permintaan_pengujian.table-client', compact('data', 'search'));
    }

    public function terimaPermintaan($id)
    {
        $data = Pendaftaran::findOrFail($id);

        $data->update(
            [
            'status' => 'diterima',
            ]
        );

        // Mail::to($data->email)->send(new PermintaanStatus($data->id, $data->tgl_permintaan, $data->nama_pemohon, 'diterima'));

        return back()->with('success', __('Status Diterima.'));
    }

    public function tolakPermintaan($id)
    {
        $data = Pendaftaran::findOrFail($id);

        $data->update(
            [
            'status' => 'ditolak',
            ]
        );

        // Mail::to($data->email)->send(new PermintaanStatus($data->id, $data->tgl_permintaan, $data->nama_pemohon, 'ditolak'));

        return back()->with('success', __('Status Diterima.'));
    }

    public function selesaiPermintaan($id)
    {
        $data = Pendaftaran::findOrFail($id);

        $data->update(
            [
            'status' => 'selesai',
            ]
        );

        // Mail::to($data->email)
            // ->send(new PermintaanStatus(
            //     $data->id,
            //     $data->tgl_permintaan,
            //     $data->nama_pemohon,
            //     'selesai'
            // ));

        return back()->with('success', __('Status Diterima.'));
    }

    public function create($id)
    {
        $data = Pelayanan::with('kategori')->findOrFail($id);
        $email = auth()->user()->email;

        return view('homepage.pendaftaran-client', compact('data', 'email'));
    }

    public function store(Request $request, $id)
    {
        dd('PP');
        $v = Validator::make(
            $request->all(),
            [
            'nama_pemohon' => 'required|string|max:250',
            'alamat' => 'required|string|max:65534',
            'nama_kontak' => 'required|string|max:250',
            'nomor_hp_kontak' => 'required|string|regex:/(628)[0-9]{9}/',
            'email' => 'required|string|email',
            'tanggal_pengujian' => 'required|date',
            'uraian_pengujian' => 'required|string|max:65534',
            'detail_layanan' => 'required|string|max:65534',
            'nama_lhu' => 'nullable|string|max:250',
            'email_persuratan' => 'required|string|email',
            ]
        );

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $pelayanan = Pelayanan::findOrFail($id);

        Pendaftaran::create(
            $request->only(
                'nama_pemohon',
                'email',
                'nama_kontak',
                'nomor_hp_kontak',
                'alamat',
                'nama_lhu',
                'email_persuratan',
                'tanggal_pengujian',
                'uraian_pengujian',
                'detail_layanan'
            ) + [
            'pelayanan_id' => $id,
            'tgl_permintaan' => date('d-m-Y', strtotime(now('Asia/Jakarta'))),
            'status' => 'diproses',
            'client_id' => auth()->user()->client->id
                ]
        );

        $setting = SettingApp::find(1);

        $plus = '';

        if ($setting->email != null) {
            // Mail::to($setting->email)
                // ->send(new PermintaanMail(
                //     now('Asia/Jakarta'),
                //     'Bahan',
                //     $request->nama_pemohon,
                //     $request->email
                // ));
        }

        return back()->with('success', __('Pendaftaran dikirim. '.$plus));
    }

    public function paymentConfirmation(Pendaftaran $permintaan_pengujian)
    {
        return view(
            'client.permintaan-pengujian.payment-confirmation.index',
            compact('permintaan_pengujian')
        );
    }

    public function paymentConfirmationPost(PaymentConfirmationRequest $request, Pendaftaran $permintaan_pengujian)
    {
        $uploadService = new UploadService();
        $paymentProof = $uploadService->saveFileBulk([$request->file('payment_proof')]);

        $permintaan_pengujian->update(
            [
                'status' => Pendaftaran::STATUS_PAYMENT_RECEIVED,
            ]
        );

        $permintaan_pengujian->payments->update([
            'status' => Pendaftaran::STATUS_PAYMENT_RECEIVED,
        ]);
        $permintaan_pengujian->payments->confirmation()->create([
            'bank_name' => $request->bank_name,
            'account_number' => $request->account_number_name,
            'account_holder' => $request->account_holder_name,
            'payment_amount' => $request->payment_amount,
            'payment_date' => $request->payment_date,
            'file_id' => $paymentProof[0],
            'status' => Pendaftaran::STATUS_DRAFT,
        ]);

        $permintaan_pengujian->payments->creator->notify(new PaymentReceivedNotification());

        return redirect()
            ->route('client.permintaan-pengujian.index')
            ->with('success', __('Pembayaran berhasil dikonfirmasi.'));
    }

    public function laporanHasilUji(Pendaftaran $permintaan_pengujian)
    {
        $data = $permintaan_pengujian;
        $laporan_hasil_uji=LaporanHasilUji::firstWhere('pengujian_id',$permintaan_pengujian->id);
        $file=File::find($laporan_hasil_uji->file);
        $lhu=$file->filename;
        return view(
            'client.permintaan-pengujian.laporan-hasil-uji.index',
            compact('laporan_hasil_uji','lhu', 'data')
        );
    }
}
