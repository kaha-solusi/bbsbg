<?php

namespace App\Http\Controllers\V1\Client;

use App\Http\Controllers\Controller;
use App\Models\AdvisiTeknis;
use App\Models\Kuisioner;
use App\Services\AdvisiTeknisService;
use Illuminate\Http\Request;

class AdvisTeknisKuisionerController extends Controller
{
    private $advisiTeknisService;

    public function __construct(AdvisiTeknisService $advisiTeknisService)
    {
        $this->advisiTeknisService = $advisiTeknisService;
    }

    public function index(AdvisiTeknis $advisiTeknis)
    {
        $kuisioners = Kuisioner::where('kategori_pelayanan_id', 2)->get();
        $route = route('advisi-teknis.kuisioner.store', $advisiTeknis->id);
        return view('client.kuisioner.index', compact('kuisioners', 'route'));
    }

    public function store(Request $request)
    {
        $advisiTeknis = AdvisiTeknis::find(request('advisiTeknis'));
        $this->advisiTeknisService->store_kuisioner($request->data, $advisiTeknis);

        return redirect()->route('client.advisi-teknis.show', request('advisiTeknis'));
    }
}
