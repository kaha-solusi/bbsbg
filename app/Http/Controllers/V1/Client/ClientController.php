<?php

namespace App\Http\Controllers\V1\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Client;
use App\User;
use App\Models\LogAktivitas;
use Illuminate\Support\Facades\Hash;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Client::with('user')->orderBy('id', 'asc')->paginate(10);

        return view('admin.client', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.client-p.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'logo' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'name' => 'required|string|max:25',
            'username' => 'required|string|max:25',
            'website' => 'nullable|url',
            'alamat' => 'required|string|max:65534',
            'email' => 'required|string|email|unique:users,email',
            'phone_number' => 'required|string|regex:/(628)[0-9]{9}/',
            'nama_perusahaan' => 'required|string',
            'bidang_usaha' => 'required|string',
            'password' => 'required|string|min:8|confirmed'
        ]);

        if ($v->fails()) {
            // dd($v);
            return back()->withErrors($v)->withInput();
        }

        $user = User::create($request->only([
            'name',
            'email',
            'username',
        ]) + [
            'password' => Hash::make($request->password),
            'level' => '2',
            'status' => 'aktif'
        ]);

        $user->assignRole('pelanggan');

        $data = Client::create($request->only('name', 'website', 'alamat', 'phone_number', 'bidang_usaha', 'nama_perusahaan') + [
            'user_id' => $user->id,
        ]);

        if ($request->file('logo')) {
            // File::delete($data->foto);

            $name = $request->file('logo');
            $logo = time()."_".$name->getClientOriginalName();
            $request->logo->move("upload/logo/client", $logo);

            Client::find($data->id)->update([
                'logo' => '/upload/logo/client/'.$logo,
            ]);
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Create Client"
        ]);

        return back()->with('success', __('Berhasil diupdate!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Client::with('user')->findOrFail($id);

        return view('admin.client-p.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Client::findOrFail($id);

        $v = Validator::make($request->all(), [
            'logo' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'name' => 'required|string|max:25',
            'username' => 'required|string|max:25',
            'website' => 'nullable|url',
            'alamat' => 'required|string|max:65534',
            'email' => 'required|string|email|unique:users,email,'.$data->user_id,
            'phone_number' => 'required|string|regex:/(628)[0-9]{9}/',
            'nama_perusahaan' => 'required|string',
            'bidang_usaha' => 'required|string'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $user = User::find($data->user_id);

        $user->update($request->only([
            'name',
            'email',
            'username',
        ]));

        $data->update($request->only('name', 'website', 'alamat', 'phone_number', 'bidang_usaha', 'nama_perusahaan'));

        if ($request->file('logo')) {
            // File::delete($data->logo);

            $name = $request->file('logo');
            $logo = time()."_".$name->getClientOriginalName();
            $request->logo->move("upload/logo/client", $logo);

            $data->update([
                'logo' => '/upload/logo/client/'.$logo,
            ]);
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Client"
        ]);

        return back()->with('success', __('Berhasil diupdate!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Client::findOrFail($id);

        // File::delete($data->logo);

        $data->delete();

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Delete Client"
        ]);

        return back()->with('success', __('Data berhasil dihapus.'));
    }

    public function settingClient()
    {
        $id = auth()->user()->client->id;
        $data = Client::with('user')->findOrFail($id);

        return view('admin.setting-clent.edit', compact('data'));
    }


    public function settingClientUpdate(Request $request)
    {
        $id = auth()->user()->client->id;
        $data = Client::findOrFail($id);

        $v = Validator::make($request->all(), [
            'logo' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'name' => 'required|string|max:25',
            'username' => 'required|string|max:25',
            'website' => 'nullable|url',
            'alamat' => 'required|string|max:65534',
            'email' => 'required|string|email|unique:users,email,'.$data->user_id,
            'phone_number' => 'required|string|regex:/(628)[0-9]{9}/',
            'nama_perusahaan' => 'required|string',
            'bidang_usaha' => 'required|string'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $user = User::find($data->user_id);

        $user->update($request->only([
            'name',
            'email',
            'username',
        ]));

        $data->update($request->only('name', 'website', 'alamat', 'phone_number', 'bidang_usaha', 'nama_perusahaan'));

        if ($request->file('logo')) {
            // File::delete($data->logo);

            $name = $request->file('logo');
            $logo = time()."_".$name->getClientOriginalName();
            $request->logo->move("upload/logo/client", $logo);

            $data->update([
                'logo' => '/upload/logo/client/'.$logo,
            ]);
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Client"
        ]);

        return back()->with('success', __('Berhasil diupdate!'));
    }

    public function password(Request $request)
    {
        $v = Validator::make($request->all(), [
            'password' => 'required|string|min:8|confirmed',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $id = auth()->user()->id;

        $data = User::find($id);

        $data->update([
            'password' => Hash::make($request->password),
        ]);

        return back()->with('success', __('Berhasil direset!'));
    }
}
