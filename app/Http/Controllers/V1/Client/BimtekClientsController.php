<?php

namespace App\Http\Controllers\V1\Client;

use App\Http\Controllers\Controller;
use App\Models\Bimtek;
use App\Models\Pelayanan;
use App\Models\PesertaBimtek;
use App\Models\Instansi;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class BimtekClientsController extends Controller
{
    public function list() {
        $currentuserid = Auth::user()->id;

        $bimteks = Bimtek::all();
        $pesertas = PesertaBimtek::where('user_id', $currentuserid)->get();
        return view('bimtek.list', compact( 'bimteks', 'pesertas'));
    }
}
