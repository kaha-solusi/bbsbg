<?php

namespace App\Http\Controllers\V1\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermintaanPengujianTestSampleStoreRequest;
use App\Models\PermintaanPengujian;
use App\Services\UploadService;

class PermintaanPengujianTestSampleController extends Controller
{
    public function index(PermintaanPengujian $permintaan_pengujian)
    {
        return view('client.permintaan-pengujian.sample.create', compact('permintaan_pengujian'));
    }

    public function store(PermintaanPengujianTestSampleStoreRequest $request, PermintaanPengujian $permintaan_pengujian)
    {
        $uploadService = new UploadService();
        $paymentProof = $uploadService->saveFileBulk([$request->file('bukti_pengiriman')]);

        $permintaan_pengujian->update([
            'status' => PermintaanPengujian::STATUS_SAMPLE_SUBMITTED,
        ]);

        $permintaan_pengujian->test_samples()->create([
            'jasa_ekspedisi' => $request->jasa_ekspedisi,
            'no_resi' => $request->no_resi,
            'status' => PermintaanPengujian::STATUS_DRAFT,
            'file_id' => $paymentProof[0],
            'jenis_pengiriman' => $request->jenis_pengiriman,
        ]);


        return redirect()
            ->route('client.permintaan-pengujian.index')
            ->with('success', 'Permintaan pengujian sampel berhasil dikirim');
    }
}
