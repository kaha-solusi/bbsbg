<?php

namespace App\Http\Controllers\V1\Client;

use App\DataTables\Client\AdvisiTeknisDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Client\AdvisiTeknisRequest;
use App\Models\AdvisiTeknis;
use App\Models\Pelayanan;
use App\Services\AdvisiTeknisService;

class AdvisiTeknisController extends Controller
{
    private $service;
    public function __construct(AdvisiTeknisService $advisiTeknisService)
    {
        $this->service = $advisiTeknisService;
    }

    public function index(AdvisiTeknisDatatable $advisiTeknisDatatable)
    {
        return $advisiTeknisDatatable->render('client.advisi-teknis.index');
    }

    public function show(AdvisiTeknis $advisi_tekni)
    {
        $data = $advisi_tekni;
        $zoom = Pelayanan::where('nama', 'Advis Teknis')->first();
        $province = $data->province->name;
        $regency = $data->regency->name;
        $district = $data->district->name;
        $village = $data->village->name;
        $location = $province.', '.$regency.', '.$district.', '.$village;

        return view('client.advisi-teknis.show', compact('data', 'location', 'zoom'));
    }

    public function store(AdvisiTeknisRequest $request)
    {
        $tanggal = date('Y-m-d', strtotime($request->input_date_at));
        $data = collect([
            'test_date' => $tanggal,
            'scope' => $request->lingkup_konsultasi,
            'building_identity' => $request->input_identitas_bangunan,
            'problem' => $request->permasalahan,
            'support_file' => $request->supportFile,
            'province' => $request->province,
            'regency' => $request->regency,
            'district' => $request->district,
            'village' => $request->village,
        ]);

        $this->service->store($data, auth()->user());

        $request->session()->flash('success', 'Berhasil mendaftarkan advis teknis');
        return response()->json(['status' => 'successfully created advis teknis']);
    }
}
