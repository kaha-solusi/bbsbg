<?php

namespace App\Http\Controllers\V1\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Models\AdvisiTeknis;
use App\Models\PesertaBimtek;
use App\Models\UmpanBalik;
use App\Services\InstansiService;
use App\User;
use Illuminate\Support\Facades\File;

class ProfileController extends Controller
{
    private $instansiService;
    public function __construct(InstansiService $instansiService)
    {
        $this->instansiService = $instansiService;
    }

    public function index()
    {
        $user = User::find(auth()->user()->id);
        $unitOrganisasi = $this->instansiService->getUnitOrganisasi();
        $unitKerja = $this->instansiService->getUnitKerja($user->client->instansi);

        $advis = AdvisiTeknis::where('client_id', $user->client->id)->count();
        $bimtek = PesertaBimtek::where('user_id', $user->id)->count();
        $pengujian = \App\Models\PermintaanPengujian::where('client_id', $user->client->id)->count();
        $umpanbalik = UmpanBalik::where('client_id', $user->client->id)->count();


        return view('client.profile.index', compact(
            'user',
            'unitOrganisasi',
            'unitKerja',
            'advis',
            'bimtek',
            'pengujian',
            'umpanbalik'
        ));
    }

    public function update(ProfileRequest $request, User $user)
    {
        try {
            $user->update([
                'name' => $request->nama,
                'username' => $request->username,
                'email' => $request->email,
            ]);
            if ($request->file('logo')) {
                $foto = $request->file('logo');
                $name = hash('sha256', $foto->getClientOriginalName()).".".$foto->getClientOriginalExtension();

                $request->logo->move("uploads/foto/client", $name);

                if ($user->client->logo) {
                    File::delete("uploads/foto/client/" . $user->client->logo);
                }
                $user->client->update([
                    'logo' => $name
                ]);
            }

            $user->client->update([
                'phone_number' => $request->phone_number,
                'unit_kerja_id' => $request->unit_kerja,
                'instansi_id' => $request->unit_organisasi,
                'nama_perusahaan' => $request->nama_instansi,
                'nomor' => $request->nip_nrp,
                'alamat' => $request->alamat
            ]);

            $user->save();
            return redirect()->route('client.profile.index')->with('success', 'Data berhasil diubah');
        } catch (\Throwable $th) {
            report($th);
            return redirect()->route('client.profile.index')->with('error', 'Data gagal diubah');
        }
    }

    public function query(AdvisiTeknis $model)
    {
        $count = $model->newQuery()
            ->whereClientId(auth()->user()->client->id)
            ->select('advisi_teknis.*');
        return $count->count();
    }
}
