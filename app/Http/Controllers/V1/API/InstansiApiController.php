<?php

namespace App\Http\Controllers\V1\API;

use App\Http\Controllers\Controller;
use App\Models\Instansi;
use App\Services\InstansiService;
use Illuminate\Http\Request;

class InstansiApiController extends Controller
{
    private $service;

    public function __construct(InstansiService $instansiService)
    {
        $this->service = $instansiService;
    }
    
    public function getUnitOrganisasi()
    {
        return response()->json($this->service->getUnitOrganisasi());
    }
    
    public function getUnitKerja(Instansi $unitOrganisasi)
    {
        return response()->json($this->service->getUnitKerja($unitOrganisasi));
    }
}
