<?php

namespace App\Http\Controllers\V1\API;

use App\Http\Controllers\Controller;
use App\Models\PermintaanPengujian;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Undocumented function
     *
     * @param string $order
     */
    public function getOrderByCode(Request $request)
    {
        try {
            $code = $request->input("code");
            $steps = config('tracking');
            $orderHistories = PermintaanPengujian::whereOrderId($code)
                ->with('history:id,permintaan_pengujian_id,step,message,status,created_at')
                ->with('kaji_ulang:id,permintaan_pengujian_id,durasi')
                ->select('id', 'order_id')
                ->firstOrFail();

            $data = [];
            $success = 0;
            foreach ($steps as $key => $value) {
                if ($orderHistories->history->where('step', $key)->isNotEmpty()) {
                    foreach ($orderHistories->history->where('step', $key) as $history) {
                        $success += 1;
                        $data[] = [
                            'message' => $history->message,
                            'status' => $history->status === 'approved' ? 'completed' : 'rejected',
                            'created_at' => $history->created_at->format('d F Y H:m:s'),
                        ];
                    }
                } else {
                    $status = 'next';
                    if ($data[count($data) - 1]['status'] === 'completed') {
                        $status = 'process';
                    }
                    $data[]  = [
                        'message' => __($key),
                        'status' => $status
                    ];
                }
            }

            $estimationDate = Carbon::now()->addDays($orderHistories->kaji_ulang->durasi);
            $estimationDate = $estimationDate->format('d F Y');
            return response()->json([
                'histories' => $data,
                'progress' => round(($success / count($steps)) * 100, 2),
                'estimation_date' => $estimationDate,
                'days_count' => $orderHistories->kaji_ulang->durasi
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'code' => $th->getCode(),
                'error' => "Code ".$request->input("code")." tidak tersedia, silahkan periksa kembali kode tracking anda",
            ], 500);
        }
    }
}
