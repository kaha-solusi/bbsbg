<?php

namespace App\Http\Controllers\V1\API;

use App\Http\Controllers\Controller;
use App\Models\District;
use App\Models\Province;
use App\Models\Regency;
use App\Services\ProvinceService;

class ProvinceController extends Controller
{
    private $service;
    public function __construct(ProvinceService $provinceService)
    {
        $this->service = $provinceService;
    }
    
    public function index()
    {
        return $this->service->findAll();
    }
    
    public function regencies(Province $province)
    {
        return $this->service->getRegencies($province, $province->regencies);
    }
    
    public function district(Province $province, Regency $regency)
    {
        return $this->service->getDistricts($province, $regency, $regency->districts);
    }
    
    public function village(Province $province, Regency $regency, District $district)
    {
        return $this->service->getVillages($province, $regency, $district, $district->villages);
    }
}
