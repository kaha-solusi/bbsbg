<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\StrukturOrganisasi;
use App\Models\StrukturOrganisasiFile;
use App\Models\LogAktivitas;
use Illuminate\Support\Facades\File;

class StrukturController extends Controller
 {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = StrukturOrganisasi::orderBy('id', 'asc')->paginate(10);
        $organisasi = StrukturOrganisasiFile::first();

        $search = $request->query('search', null);

        if ($search != null) {
            $data = StrukturOrganisasi::where('nama', 'like', '%'.$search.'%')
            ->orWhere('jabatan', 'like', '%'.$search.'%')
            ->orWhere('nip', 'like', '%'.$search.'%')
            ->orderBy('id', 'desc')
            ->paginate(10);
        }

        return view('admin.struktur', compact('data', 'search', 'organisasi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.struktur-p.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'nama' => 'required|string|max:50',
            'email' => 'required|string|email|unique:struktur_organisasis,email',
            'nip' => 'required|numeric|digits:18',
            'jabatan' => 'required|string|max:75',
            'keterangan' => 'nullable|string|max:65534'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = StrukturOrganisasi::create($request->only('nama', 'nip', 'jabatan', 'keterangan', 'email'));

        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("upload/foto/struktur", $logo);

            StrukturOrganisasi::find($data->id)->update([
                'foto' => '/upload/foto/struktur/'.$logo
            ]);         
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Create Struktur"
        ]);

        return back()->with('success', __( 'Berhasil dibuat!' ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = StrukturOrganisasi::findOrFail($id);

        return view('admin.struktur-p.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(),[
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'nama' => 'required|string|max:50',
            'email' => 'required|string|email|unique:struktur_organisasis,email,'.$id,
            'nip' => 'required|numeric|digits:18',
            'jabatan' => 'required|string|max:75',
            'keterangan' => 'nullable|string|max:65534'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = StrukturOrganisasi::findOrFail($id);

        $data->update($request->only('nama', 'nip', 'jabatan', 'keterangan', 'email'));

        if ($request->file('foto')) {
            File::delete(public_path($data->foto));

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("upload/foto/struktur", $logo);

            $data->update([
                'foto' => '/upload/foto/struktur/'.$logo
            ]);         
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Struktur Organisasi"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = StrukturOrganisasi::findOrFail($id);

        // File::delete($data->foto);

        $data->delete();

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Delete Struktur Organisasi"
        ]);

        return back()->with('success', __( 'Data berhasil dihapus.' ));
    }

    public function store_gambar(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'gambar' => 'required|image|mimes:png,jpg,jpeg'
        ]);

        if($validation->fails()){
            return back()->withErrors($validation)->withInput();
        }

        $name = $request->file('gambar');
        $logo = time()."_".$name->getClientOriginalName();
        $request->gambar->move("gambar/struktur_organisasi/", $logo);

        StrukturOrganisasiFile::first()->update([
            'gambar' => '/gambar/struktur_organisasi/'.$logo
        ]);

        return back();
    }

    public function store_pdf(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'pdf' => 'required|file|mimes:pdf'
        ]);

        if($validation->fails()){
            return back()->withErrors($validation)->withInput();
        }

        $name = $request->file('pdf');
        $logo = time()."_".$name->getClientOriginalName();
        $request->pdf->move("pdf/struktur_organisasi/", $logo);

        StrukturOrganisasiFile::first()->update([
            'pdf' => '/pdf/struktur_organisasi/'.$logo
        ]);

        return back()->with('success', __('Berhasil Update PDF'));
    }

    public function status_homepage($id)
    {
        $inArray = [1,2,3];

        if (in_array($id, $inArray) != true) {
            return back()->with('failed', __('URL salah'));
        }

        if ($id == 1) {
            StrukturOrganisasiFile::first()->update([
                'gambar_status' => '1',
                'pdf_status' => '0'
            ]);
        }elseif($id == 2) {
            StrukturOrganisasiFile::first()->update([
                'gambar_status' => '0',
                'pdf_status' => '1'
            ]);
        } elseif ($id == 3) {
            StrukturOrganisasiFile::first()->update([
                'gambar_status' => '0',
                'pdf_status' => '0'
            ]);
        }

        return back()->with('success', __('Berhasil update'));
    }
}
