<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Pelayanan;
use App\Models\SettingApp;
use Illuminate\Http\Request;

class AdvisiTeknisController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $date = SettingApp::findOrFail(1);
        $date2 = Pelayanan::where('nama', 'Advis Teknis')->first();
        $data = Pelayanan::findOrFail(3);
        return view('homepage.pelayanan.advisi-teknis', compact('data','date','date2'));
    }
}
