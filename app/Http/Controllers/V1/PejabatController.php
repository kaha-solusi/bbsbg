<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Pejabat;
use App\Models\Lab;
use App\Models\LogAktivitas;
use Illuminate\Support\Facades\File;

class PejabatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Pejabat::orderBy('nama', 'asc')->paginate(10);

        $search = $request->query('search', null);

        if ($search != null) {
            $data = Pejabat::where('nama', 'like', '%'.$search.'%')
            ->orWhere('job', 'like', '%'.$search.'%')
            ->orWhere(function($q)use($search){
                $q->whereHas('lab', function($query)use($search){
                    $query->where('nama', 'like', '%'.$search.'%');
                });
            })
            ->orderBy('id', 'desc')
            ->paginate(10);
        }

        return view('admin.pejabat', compact('data', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Lab::all();

        return view('admin.pejabat-p.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'nama' => 'required|string',
            'lab_id' => 'required|exists:labs,id',
            'job' => 'required|string'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = Pejabat::create($request->only('nama', 'job', 'lab_id')+[
            'quote' => ''
        ]);

        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("upload/foto/pejabat", $logo);

            Pejabat::find($data->id)->update([
                'foto' => '/upload/foto/pejabat/'.$logo
            ]);         
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Create Pejabat"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Pejabat::findOrFail($id);
        $labs = Lab::all();

        return view('admin.pejabat-p.edit', compact('data', 'labs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(),[
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'lab_id' => 'required|exists:labs,id',
            'nama' => 'required|string',
            'job' => 'required|string'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = Pejabat::findOrFail($id);

        $data->update($request->only('nama', 'job', 'lab_id')+[
            'quote' => ''
        ]);

        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("upload/foto/pejabat", $logo);

            $data->update([
                'foto' => '/upload/foto/pejabat/'.$logo
            ]);         
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Pejabat"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pejabat::findOrFail($id);

        // File::delete($data->foto);

        $data->delete();

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Delete Pejabat"
        ]);

        return back()->with('success', __( 'Data berhasil dihapus.' ));
    }
}
