<?php

/**
 * Ssss *
 * php version 7.4.0
 *
 * @category Controller
 * @package  PengujianController
 * @author   Agung Kurniawan <agungkes95@gmail.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @version  GIT: adskahdkadhskj
 * @link     http://url.com
 */
namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Services\MasterLayananUjiService;
use App\Services\PengujianService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * PengujianController
 *
 * @category Controller
 * @package  PengujianController
 * @author   Agung Kurniawan <agungkes95@gmail.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @version  Release: 0.0.1
 * @access   public
 * @link     http://url.com
 */
class PengujianController extends Controller
{
    /**
     * MasterLayananUjiService
     *
     * @var MasterLayananUjiService
     */
    private $masterLayananUjiService;

    /**
     * Service
     *
     * @var PengujianService
     */
    private $service;

    /**
     * __construct function
     *
     * @param MasterLayananUjiService $masterLayananUjiService s
     * @param PengujianService        $pengujianService        s
     *
     * @return void
     */
    public function __construct(
        MasterLayananUjiService $masterLayananUjiService,
        PengujianService $pengujianService
    )
    {
        $this->masterLayananUjiService = $masterLayananUjiService;
        $this->service = $pengujianService;
    }

    /**
     * Index function
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $layananUji = $this->masterLayananUjiService->getServices();
        $user = auth()->user();
        
        return view('pengujian.index', compact('layananUji', 'user'));
    }

    /**
     * Index function
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function show()
    {
        $layananUji = $this->masterLayananUjiService->getServices();
        $user = auth()->user();
        
        return view('pengujian.index', compact('layananUji', 'user'));
    }

    /**
     * Store function
     *
     * @param Request $request s
     *
     * @return view
     */
    public function store(Request $request)
    {
        try {
            $this->service->store($request->input());
            return redirect()
                ->route('pengujian.registrasi')
                ->with('success', 'Registrasi pengujian berhasil, silahkan periksa email anda');
        } catch (\Throwable $th) {
            report($th);
            dd($th);
            return redirect()
                ->route('pengujian.registrasi')
                ->with('error', 'Terjadi kesalahan, silahkan coba lagi nanti');
        }
    }

    /**
     * GetJenisPengujian function
     *
     * @param Request $request s
     *
     * @return JSON
     */
    public function getJenisPengujian(Request $request)
    {
        $labId = $request->labId;
        $jenis_pengujian = $this
            ->masterLayananUjiService
            ->getJenisPengujianByLab($labId);

        return response()->json($jenis_pengujian);
    }


    /**
     * GetJenisPengujianItem function
     *
     * @param Request $request s
     *
     * @return JSON
     */
    public function getJenisPengujianItem(Request $request)
    {
        $jenis_pengujian_items = $this
            ->masterLayananUjiService
            ->getPengujianItemByJenisPengujian($request->pengujianId);

        return response()->json($jenis_pengujian_items->items);
    }

    public function getProductName(Request $request)
    {
        if (!$request->ajax()) {
            abort(403);
        }

        if (is_null($request->parameterPengujian)) {
            return response()->json([]);
        }

        $products = Product::select('name', 'id', 'description')
            ->whereHas('parameters', function (Builder $query) use ($request) {
                $query->where('master_layanan_uji_item_id', $request->parameterPengujian);
            })->get();

        return response()->json($products);
    }
}
