<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UmpanBalik;
use Illuminate\Support\Facades\Validator;

class UmpanBalikController extends Controller
{
	public function index()
	{
		$data = UmpanBalik::orderBy('id', 'desc')->paginate(10);

		return view('admin.umpan-balik', compact('data')); 
	}

    public function create()
    {
        $data = auth()->user();
    	return view('admin.client-umpan-balik.create', compact('data'));
    }

    public function store(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'nama' => 'required|string|max:250',
            'keterangan' => 'required|string|max:65534',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        UmpanBalik::create($request->only('nama', 'keterangan')+[
            'client_id' => auth()->user()->client->id
        ]);

        return back()->with('success', __( 'Umpan Balik dikirim.' ));
    }
}
