<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdvisiTeknis;
use App\Models\Kuisioner;

class AdvisTeknisKuisionerController extends Controller
{
    public function index(AdvisiTeknis $advisiTeknis)
    {
        $kuisioners = Kuisioner::where('kategori_pelayanan_id', 2)->get();
        return view('admin.advisiteknis.kuisioner', compact('kuisioners', 'advisiTeknis'));
    }
}
