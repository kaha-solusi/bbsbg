<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\MasterLayananDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\MasterLayananUjiStoreRequest;
use App\Models\MasterLayananUji;
use App\Services\MasterLayananUjiService;

class MasterLayananUjiController extends Controller
{
    private $service;
    public function __construct(MasterLayananUjiService $masterLayananUjiService)
    {
        $this->service = $masterLayananUjiService;
    }

    public function index(MasterLayananDatatable $dataTable)
    {
        return $dataTable->render('admin.master.layanan-uji.index');
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function create()
    {
        $categories = $this->service->getCategory();
        return view('admin.master.layanan-uji.create', compact('categories'));
    }

    public function edit(MasterLayananUji $master_pelayanan_uji)
    {
        $categories = $this->service->getCategory();
        return view('admin.master.layanan-uji.edit', compact('master_pelayanan_uji', 'categories'));
    }

    public function show(MasterLayananUji $master_pelayanan_uji)
    {
        $categories = $this->service->getCategory();
        return view(
            'admin.master.layanan-uji.edit',
            compact(
                'master_pelayanan_uji',
                'categories'
            )
        );
    }

    /**
     * Undocumented function
     *
     * @param MasterLayananUjiStoreRequest $request r
     *
     * @return void
     */
    public function store(MasterLayananUjiStoreRequest $request)
    {
        $this->service
            ->store(
                $request->name,
                $request->category,
                $request->items
            );
        return redirect()
            ->route('master-pelayanan-uji.index')
            ->with('success', 'Berhasil menambahkan data baru!');
    }

    public function update(
        MasterLayananUjiStoreRequest $request,
        MasterLayananUji $master_pelayanan_uji
    )
    {
        $this->service
            ->update(
                $master_pelayanan_uji,
                $request->name,
                $request->category,
                $request->items
            );
        return redirect()
            ->route('master-pelayanan-uji.index')
            ->with('success', 'Berhasil mengubah data!');
    }

    public function destroy(MasterLayananUji $master_pelayanan_uji)
    {
        $this->service->destroy($master_pelayanan_uji->id);
        return back()->with('success', 'Berhasil menghapus pengguna');
    }
}
