<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\KuisionerDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\KuisionerRequest;
use App\Models\KategoriPelayanan;
use App\Models\Kuisioner;
use App\Services\KuisionerService;

class KuisionerController extends Controller
{
    private $service;
    public function __construct(KuisionerService $kuisionerService)
    {
        $this->service = $kuisionerService;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(KuisionerDatatable $dataTable)
    {
        return $dataTable->render('admin.kuisioner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = KategoriPelayanan::all();
        return view('admin.kuisioner.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KuisionerRequest $request)
    {
        $this->service->store(collect([
            'pernyataan' => $request->pernyataan,
            'keterangan' => $request->keterangan,
            'order' => $request->order,
            'kategori' => $request->kategori_pelayanan,
        ]));

        return redirect()->route('admin.kuisioners.index')->with('success', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kuisioner $kuisioner)
    {
        $categories = KategoriPelayanan::all();
        return view('admin.kuisioner.edit', compact('kuisioner', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KuisionerRequest $request, Kuisioner $kuisioner)
    {
        $this->service->update($kuisioner, collect([
            'pernyataan' => $request->pernyataan,
            'keterangan' => $request->keterangan,
            'order' => $request->order,
            'kategori' => $request->kategori_pelayanan,
        ]));

        return redirect()->route('admin.kuisioners.index')->with('success', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kuisioner $kuisioner)
    {
        $this->service->destroy($kuisioner);

        return redirect()->route('admin.kuisioners.index')->with('success', 'Data berhasil dihapus');
    }
}
