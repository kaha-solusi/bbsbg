<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\Models\PeralatanLab;
use App\Models\PeralatanLabUsage;
use App\Services\PegawaiService;
use App\Services\PeralatanLabUsageService;
use Illuminate\Http\Request;

class PeralatanLabUsageController extends Controller
{
    private $service;
    public function __construct(PeralatanLabUsageService $peralatanLabUsageService)
    {
        $this->service = $peralatanLabUsageService;
    }

    public function index(Request $request, PeralatanLab $peralatan)
    {
        $usages = PeralatanLabUsage::where([['peralatan_lab_id','=', $peralatan->id],['kategori','=',$request->usage]])->orderByDesc('created_at')->get();
        return view('admin.peralatan.usage.index', compact('peralatan','usages'));
    }

    public function create()
    {
        $alats = PeralatanLab::all();
        return view('admin.peralatan.usage.create', compact('alats'));
    }

    public function store(Request $request)
    {
        $tanggal = date('Y-m-d', strtotime(str_replace('/','-',$request->tanggal)));
        $v = \Validator::make($request->all(),[
            'alat_pengujian' => 'required',
            'tanggal' => 'required',
            'kategori' => 'required',
            'keterangan' => 'required',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = collect([
            'tanggal' => $tanggal,
            'kategori' => $request->kategori,
            'keterangan' => $request->keterangan,
            'peralatan' => $request->alat_pengujian,
            'pelaksana' => auth()->user()->name
        ]);

        $this->service->store($data);
        return redirect()
            ->route('admin.usage.create')
            ->with('success', __('Berhasil menambahkan kartu alat baru!'));
    }

    public function edit($id)
    {
        $data=PeralatanLabUsage::find($id);
        $alat = PeralatanLab::find($data->peralatan_lab_id);
        return view('admin.peralatan.usage.edit', compact('data','alat'));
    }

    public function update(Request $request, PeralatanLabUsage $usage)
    {
        $v = \Validator::make($request->all(),[
            'tanggal' => 'required',
            'kategori' => 'required',
            'keterangan' => 'required',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = collect([
            'tanggal' => $request->tanggal,
            'kategori' => $request->kategori,
            'keterangan' => $request->keterangan,
            'pelaksana' => auth()->user()->name
        ]);

        $this->service->update($usage, collect($data));
        return redirect()
            ->route('admin.peralatan.show',$usage->peralatan_lab_id);
    }
}
