<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\PermintaanPengujianDatatable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PermintaanPengujian as Pendaftaran;
use App\Services\PermintaanPengujianService;

class PermintaanPengujianController extends Controller
{
    private $service;
    public function __construct(PermintaanPengujianService $service)
    {
        $this->service = $service;
    }
    
    /**
     * Index function
     *
     * @param PermintaanPengujianDatatable $dataTable
     *
     * @return mixed
     */
    public function index(PermintaanPengujianDatatable $dataTable)
    {
        return $dataTable->render('admin.permintaan_pengujian.index');
    }

    /**
     * Show function
     *
     * @param Pendaftaran                      $pengujian
     *
     * @return mixed
     */
    public function show(Pendaftaran $permintaan_pengujian)
    {
        $pengujian = $permintaan_pengujian;
        $subTotal = collect($permintaan_pengujian->items)->reduce(
            function ($prev, $current) {
                return $prev +
                    $current->layanan_uji_item->price *
                    $current->product_count;
            },
            0
        );

        return view(
            'admin.permintaan_pengujian.show',
            compact(
                'pengujian',
                'subTotal'
            )
        );
    }

    /**
     * Show function
     *
     * @param Pendaftaran                      $pengujian
     *
     * @return mixed
     */
    public function edit(Pendaftaran $permintaan_pengujian)
    {
        $pengujian = $permintaan_pengujian;
        return view(
            'admin.permintaan_pengujian.edit',
            compact(
                'pengujian'
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pendaftaran $permintaan_pengujian)
    {
        try {
            $this->service->ajukanKajiUlang($request, $permintaan_pengujian);
            return redirect()
                ->route('admin.permintaan-pengujian.index')
                ->with('success', 'Permintaan pengujian berhasil diperbaharui');
        } catch (\Throwable $th) {
            report($th);
            return redirect()
                ->back()
                ->with('error', $th->getMessage());
        }
    }

    public function upload(Request $request, Pendaftaran $permintaan_pengujian)
    {
        try {
            $this->service->unggahKonfirmasiLayanan($request, $permintaan_pengujian);
            return response()->json([
                'success' => true,
                'message' => 'Upload berhasil',
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                'status' => 'error',
                'message' => $th->getMessage(),
            ], 500);
        }
    }

    public function downloadReply(Pendaftaran $permintaan_pengujian)
    {
        try {
            $this->service->unduhKonfirmasiLayanan($permintaan_pengujian);
            return redirect()
                ->back()
                ->with(
                    'download.file',
                    '/'.$permintaan_pengujian->dokumen->where('type', 'dokumen_konfirmasi_reply')->last()->dokumen->path
                );
        } catch (\Throwable $th) {
            report($th);
            return redirect()->back()->with('error', $th->getMessage());
        }
    }

    public function documentApprove(Pendaftaran $permintaan_pengujian)
    {
        try {
            $this->service->setujuiDokumen($permintaan_pengujian);
            return response()->json([
                'success' => true,
                'message' => 'Dokumen berhasil disetujui',
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                'status' => 'error',
                'message' => $th->getMessage(),
            ], 500);
        }
    }
}
