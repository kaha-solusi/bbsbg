<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\AkunDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\AkunRequest;
use App\User;
use App\Services\UserService;
use Spatie\Permission\Models\Role;

class AkunController extends Controller
{
    private $service;

    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AkunDatatable $dataTable)
    {
        return $dataTable->render('admin.akun.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::whereNotIn('name', ['super-admin', 'pelanggan'])
            ->orderBy('name', 'asc')
            ->get()
            ->toArray();
        return view('admin.akun.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AkunRequest $request)
    {
        try {
            $this->service->store(collect($request->except([
                '_token',
                'proengsoft_jsvalidation',
            ])));
            return redirect()->route('admin.akun.index')->with('success', __('Berhasil dibuat!'));
        } catch (\Throwable $th) {
            report($th);
            return back()
                ->withErrors($th->getMessage())
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $akun)
    {
        $user = $akun;
        $roles = Role::whereNotIn('name', ['super-admin', 'pelanggan'])
            ->orderBy('name', 'asc')
            ->get()
            ->toArray();
        return view('admin.akun.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AkunRequest $request, User $akun)
    {
        try {
            $this->service->update($akun, collect($request->except([
                '_token',
                'proengsoft_jsvalidation',
            ])));
            return redirect()->route('admin.akun.index')->with('success', __('Berhasil diperbarui!'));
        } catch (\Throwable $th) {
            report($th);
            return back()
                ->withErrors($th->getMessage())
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $akun)
    {
        $this->service->destroy($akun);
        return back()->with('success', 'Berhasil menghapus pengguna');
    }
}
