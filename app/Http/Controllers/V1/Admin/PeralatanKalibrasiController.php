<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\Models\PeralatanLab;
use App\Models\PeralatanLabCalibration;
use App\Services\UploadService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PeralatanKalibrasiController extends Controller
{
    private $uploadService;
    public function __construct(UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PeralatanLab $peralatan)
    {
        $calibrations = PeralatanLabCalibration::where('peralatan_lab_id', $peralatan->id)->orderByDesc('created_at')->get();
        return view('admin.peralatan.kalibrasi.index', compact('peralatan','calibrations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PeralatanLab $peralatan)
    {
        $user = auth()->user();

        return view('admin.peralatan.kalibrasi.create', compact('peralatan','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PeralatanLab $peralatan)
    {
        $all=$request->all();

        $v = \Validator::make($request->all(),[
            'tanggal_terakhir_kalibrasi' => 'required',
            'tanggal_kalibrasi_ulang' => 'required',
            'kondisi' => 'required',
            'keterangan' => 'required',
            'lembaga_pengujian_kalibrasi' => 'required',
            'sertifikat' => 'required'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }
        dd($all);

        $file = $this->uploadService->saveFile($all['sertifikat'], 'uploads/kalibrasi');

        PeralatanLabCalibration::create([
            'peralatan_lab_id' => $peralatan->id,
            'examiner' => $request->get('lembaga_pengujian_kalibrasi'),
            'condition' => $request->get('kondisi'),
            'description' => $request->get('keterangan'),
            'responsible_person' => auth()->user()->name,
            'last_calibration' => $request->get('tanggal_terakhir_kalibrasi'),
            'next_calibration' => $request->get('tanggal_kalibrasi_ulang'),
            'certificate' => $file,
        ]);

        return redirect()
            ->route('admin.peralatan.index')
            ->with('success', __('Berhasil melakukan kalibrasi!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
