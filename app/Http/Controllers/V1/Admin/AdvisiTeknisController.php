<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\AdvisiTeknisDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdvisiTeknisRequest;
use App\Mail\AdvisTeknisEmail;
use App\Models\AdvisiTeknis;
use App\Models\AdvisiTeknisMember;
use App\Models\Pelayanan;
use App\Models\SettingApp;
use App\Services\AdvisiTeknisService;
use App\Services\JadwalKegiatanPersonilService;
use App\Services\PegawaiService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AdvisiTeknisController extends Controller
{
    private $pegawaiService;
    private $service;
    private $jadwalKegiatanPersonilService;
    public function __construct(PegawaiService $pegawaiService, AdvisiTeknisService $advisiTeknisService, JadwalKegiatanPersonilService $jadwalKegiatanPersonilService)
    {
        $this->pegawaiService = $pegawaiService;
        $this->service = $advisiTeknisService;
        $this->jadwalKegiatanPersonilService = $jadwalKegiatanPersonilService;
    }

    public function home(AdvisiTeknisDatatable $advisiTeknisDatatable)
    {
        return $advisiTeknisDatatable->render('admin.advisiteknis.index');
    }

    public function oldIndex($status)
    {
        return view('admin.advisiteknis.index-bak', compact('status'));
    }

    public function index(AdvisiTeknisDatatable $advisiTeknisDatatable)
    {
        return $advisiTeknisDatatable->render('admin.advisiteknis.index');
    }

    public function show(AdvisiTeknis $advisi_tekni)
    {
        $advisiTeknis = $advisi_tekni;
        $pelayanan = Pelayanan::findOrFail(3);
        $zoom = Pelayanan::where('nama', 'Advis Teknis')->first();
        $pegawai = $this->pegawaiService->getAll()->filter(function ($e) use ($pelayanan) {
            if ($pelayanan->pegawai_id === $e['id']) {
                return false;
            }
            return true;
        });
        $penugasans = AdvisiTeknisMember::where('advisi_teknis_id', $advisiTeknis->id)->get();


        $ketua = $pelayanan->pegawai ?? 'Belum didefinisikan';
        $subkoor = $pelayanan->subkoor ?? 'Belum didefinisikan';

        $province = $advisiTeknis->province->name;
        $regency = $advisiTeknis->regency->name;
        $district = $advisiTeknis->district->name;
        $village = $advisiTeknis->village->name;
        $location = $province.', '.$regency.', '.$district.', '.$village;

        $date = SettingApp::findOrFail(1);

        return view(
            'admin.advisiteknis.show',
            compact(
                'advisiTeknis',
                'pegawai',
                'location',
                'ketua',
                'subkoor',
                'zoom',
                'penugasans',
                'date'
            )
        );
    }

    public function update(AdvisiTeknisRequest $request, AdvisiTeknis $advisi_tekni)
    {
        $tanggal = date('Y-m-d', strtotime($request->waktu_konfirmasi));
        if ($advisi_tekni->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT) {
            $data = collect([
                'members' => $request->users,
                'time' => $request->time,
                'confirmation_date' => $tanggal,
                'status' => AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL
            ]);
            $this->service->approve(
                $advisi_tekni,
                $data
            );
        } elseif ($advisi_tekni->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL) {
            $zoom = Pelayanan::where('kategori_id', 2)->first();
            $client_email = $advisi_tekni->client->user->email;
            $client_name = $advisi_tekni->client->user->name;
            $date = date('d M Y', strtotime($advisi_tekni->test_date));
            $time = $advisi_tekni->time.' WIB';

            Mail::to($client_email)->send(new AdvisTeknisEmail(
                $client_name,
                $date,
                $time,
                $advisi_tekni->scope,
                $advisi_tekni->building_identity,
                $advisi_tekni->problem,
                $zoom->syarat_peserta,
                $zoom->link_zoom,
                $zoom->meeting_id,
                $zoom->passcode,
                $zoom->subjek_email,
                $zoom->konten_email
            ));
            $advisiTeknisMember = AdvisiTeknisMember::where('advisi_teknis_id', $advisi_tekni->id)->get();
            $this->service->approve_kepala($advisi_tekni);
            $data = collect([
                'personil' => $advisiTeknisMember,
                'tanggal_mulai' => $advisi_tekni->test_date,
                'tanggal_selesai' => $advisi_tekni->test_date,
                'penugasan' => 'Advis Teknis',
                'keterangan' => null
            ]);
            $this->jadwalKegiatanPersonilService->store(
                $data
            );
        } elseif ($advisi_tekni->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED || $advisi_tekni->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE) {
            $this->service->uploadDocument($advisi_tekni, $request->file('letter_of_introduction'), $request->file('news_report'));
        } else {
            $userId = auth()->user()->id;
            $this->service->approve($advisi_tekni, $request->status, $userId);
        }

        return redirect()->route('admin.advisi-teknis.index')
            ->with('success', 'Berhasil!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdvisiTeknis $advisi_tekni)
    {
        $this->service->destroy($advisi_tekni);
        return redirect()->back()->with('success', 'Berhasil menghapus advis teknis');
    }

    public function reject(AdvisiTeknis $advisiTeknis, Request $request)
    {
        $this->service->reject($advisiTeknis, auth()->user()->id, $request->input('reject-reason'));
        return redirect()->route('admin.advisi-teknis.index')->with('success', 'Berhasil menolak advis teknis');
    }
}
