<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\GolonganDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\GolonganRequest;
use App\Models\Golongan;
use App\Services\GolonganService;

class GolonganController extends Controller
{
    private $service;

    public function __construct(GolonganService $service)
    {
        $this->service = $service;
    }

    public function index(GolonganDatatable $dataTable)
    {
        return $dataTable->render('admin.pegawai.golongan.index');
    }

    /**
     * Menampilkan halaman untuk menambah data.
     */
    public function create()
    {
        return view('admin.pegawai.golongan.create');
    }

    /**
     * Menyimpan data baru.
     */
    public function store(GolonganRequest $request)
    {
        $this->service->store(collect([
            'nama' => $request->nama,
        ]));

        return redirect()
            ->route('admin.pegawai.golongan.index')
            ->with('success', 'Berhasil menambahkan golongan baru');
    }

    /**
     * Menampilkan halaman untuk edit
     *
     * @param Golongan $golongan
     */
    public function edit(Golongan $golongan)
    {
        return view('admin.pegawai.golongan.edit', compact('golongan'));
    }

    /**
     * Menyimpan perubahan data
     *
     * @param Golongan $golongan
     * @param GolonganRequest $request
     */
    public function update(Golongan $golongan, GolonganRequest $request)
    {
        $this->service->update($golongan, collect([
            'nama' => $request->nama,
        ]));

        return redirect()
            ->route('admin.pegawai.golongan.index')
            ->with('success', 'Berhasil mengubah golongan');
    }

    public function destroy(Golongan $golongan)
    {
        $this->service->destroy($golongan);
        return back()->with('success', 'Berhasil menghapus golongan');
    }
}
