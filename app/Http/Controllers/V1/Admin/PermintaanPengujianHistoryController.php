<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\PermintaanPengujianHistoryDatatable;
use App\Http\Controllers\Controller;

use App\Http\Requests\PermintaanPengujianHistoryStoreRequest;
use App\Http\Requests\PermintaanPengujianHistoryUpdateRequest;

use App\Models\PermintaanPengujian;
use App\Models\PermintaanPengujianHistory;
use App\Services\UserService;

class PermintaanPengujianHistoryController extends Controller
{
    /**
     * UserService
     *
     * @var UserService
     */
    protected $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(
        PermintaanPengujian $permintaanPengujian,
        PermintaanPengujianHistoryDatatable $historyDatatable
    )
    {
        return $historyDatatable->render(
            'admin.permintaan_pengujian.history.index',
            ['order_id' => $permintaanPengujian->id]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PermintaanPengujian $permintaanPengujian)
    {
        $users = $this->userService->findAll();
        $steps = config('tracking');
        return view(
            'admin.permintaan_pengujian.history.create',
            [
                'order_id' => $permintaanPengujian->id,
                'users' => $users,
                'steps' => $steps
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\PermintaanPengujianHistoryStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(
        PermintaanPengujian $permintaanPengujian,
        PermintaanPengujianHistoryStoreRequest $request
    )
    {
        try {
            $validate = $request->validated();
            $status = $validate['message'];
            $step = $validate['step'];
            $steps = config('tracking')[$step];

            $permintaanPengujian->history()->create([
                'user_id' => $validate['user_id'],
                'message' => $steps[$status],
                'status' => $status,
                'step' => $step
            ]);
            return redirect()
                ->route('admin.permintaan-pengujian.histories.index', $permintaanPengujian->id)
                ->with('success', 'Riwayat berhasil ditambahkan');
        } catch (\Throwable $th) {
            report($th);
            dd($th);
            return redirect()
                ->back()
                ->with('error', $th->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param App\Models\PermintaanPengujian  $permintaanPengujian
     * @param  App\Models\PermintaanPengujianHistory  $history
     * @return \Illuminate\Http\Response
     */
    public function edit(
        PermintaanPengujian $permintaanPengujian,
        PermintaanPengujianHistory $history
    )
    {
        $users = $this->userService->findAll();
        $steps = config('tracking');
        return view(
            'admin.permintaan_pengujian.history.edit',
            [
                'order_id' => $permintaanPengujian->id,
                'history' => $history,
                'users' => $users,
                'steps' => $steps
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PermintaanPengujianHistory  $history
     * @return \Illuminate\Http\Response
     */
    public function update(
        PermintaanPengujian $permintaanPengujian,
        PermintaanPengujianHistory $history,
        PermintaanPengujianHistoryUpdateRequest $request
    )
    {
        try {
            $validate = $request->validated();
            $status = $validate['message'];
            $step = $validate['step'];
            $steps = config('tracking')[$step];

            $permintaanPengujian->history()->where('id', '=', $history->id)->update([
                'message' => $steps[$status],
                'status' => $status,
                'user_id' => $validate['user_id'],
                'step' => $step
            ]);
            return redirect()
                ->route('admin.permintaan-pengujian.histories.index', $permintaanPengujian->id)
                ->with('success', 'Riwayat berhasil ditambahkan');
        } catch (\Throwable $th) {
            report($th);
            return redirect()
                ->back()
                ->with('error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PermintaanPengujianHistory  $history
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        PermintaanPengujian $permintaanPengujian,
        PermintaanPengujianHistory $history
    )
    {
        $permintaanPengujian->history()->where('id', '=', $history->id)->delete();
        return back()->with('success', 'Berhasil riwayat');
    }
}
