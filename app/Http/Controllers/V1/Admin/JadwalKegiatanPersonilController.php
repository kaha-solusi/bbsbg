<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\JadwalKegiatanPeronilDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\JadwalKegiatanPersonilRequest;
use App\Models\JadwalKegiatanPersonil;
use App\Models\Pegawai;
use App\Models\Pelayanan;
use App\Services\JadwalKegiatanPersonilService;
use App\Services\PegawaiService;
use Illuminate\Database\Eloquent\Builder;

class JadwalKegiatanPersonilController extends Controller
{
    private $service;
    private $pegawaiService;
    public function __construct(JadwalKegiatanPersonilService $service, PegawaiService $pegawaiService)
    {
        $this->service= $service;
        $this->pegawaiService= $pegawaiService;
    }

    public function index(JadwalKegiatanPeronilDatatable $datatable)
    {
        return $datatable->render('admin.pegawai.jadwal_kegiatan_personil.index');
    }

    public function create()
    {
        $personils = $this->pegawaiService->getAll();
        $penugasans = Pelayanan::all();
        return view('admin.pegawai.jadwal_kegiatan_personil.create', compact('personils','penugasans'));
    }

    public function store(JadwalKegiatanPersonilRequest $request)
    {
        if ($request->get('penugasan') === "lainnya") {
            $v = \Validator::make($request->all(),[
                'keterangan' => 'required'
            ]);

            if ($v->fails()) {
                return back()->withErrors($v)->withInput();
            }
        }


        $this->service->store(collect($request->except([
            '_token',
            'proengsoft_jsvalidation',
        ])));

        return redirect()
            ->route('admin.pegawai.jadwal-kegiatan-personil.index')
            ->with('success', 'Berhasil menambahkan kegiatan personil baru');
    }

    public function edit(JadwalKegiatanPersonil $jadwal_kegiatan_personil)
    {
        $data = Pegawai::with(['user'])
            ->whereHas('user')->select('id','user_id')->get();
        $personils = $data->map(function ($q) {
            return [
                'id' => $q->id,
                'name' => $q->user->name
            ];
        });

        $penugasans = Pelayanan::all();
        return view('admin.pegawai.jadwal_kegiatan_personil.edit', compact('jadwal_kegiatan_personil','personils','penugasans'));
    }

    public function update(JadwalKegiatanPersonil $jadwal_kegiatan_personil,JadwalKegiatanPersonilRequest $request)
    {
        try {
            $this->service->update($jadwal_kegiatan_personil, collect($request->except([
                '_token',
                'proengsoft_jsvalidation',
                '_method',
            ])));
            return redirect()
                ->route('admin.pegawai.jadwal-kegiatan-personil.index')
                ->with('success', __('Berhasil diperbarui!'));
        } catch (\Throwable $th) {
            report($th);
            return back()
                ->withErrors($th->getMessage())
                ->withInput();
        }

    }

    public function destroy(JadwalKegiatanPersonil $jadwal_kegiatan_personil)
    {
        $this->service->destroy($jadwal_kegiatan_personil);
        return back()->with('success', 'Berhasil menghapus jadwal kegiatan');
    }
}
