<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\Models\PermintaanPengujian;
use App\Models\PermintaanPengujianTestSample;
use Illuminate\Http\Request;

class PermintaanPengujianTestSampleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PermintaanPengujian $permintaan_pengujian)
    {
        return view('admin.permintaan_pengujian.sample.index', compact('permintaan_pengujian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PermintaanPengujian $permintaan_pengujian)
    {
        foreach ($request->test_sample as $sample) {
            $permintaan_pengujian->items()->updateOrCreate([
                'id' => $sample['id'],
            ], [
                "no_sample" => $sample['no_sampel'],
                "status" => $sample['status'],
                "keterangan" => $sample['keterangan'],
            ]);
        }

        $permintaan_pengujian->update([
            'status' => PermintaanPengujian::STATUS_SAMPLE_RECEIVED,
        ]);
        $permintaan_pengujian->test_samples()->update([
            'tanggal_diterima' => date('Y-m-d'),
            'status' => PermintaanPengujian::STATUS_SAMPLE_RECEIVED,
        ]);

        return redirect()
            ->route('admin.permintaan-pengujian.index')
            ->with('success', 'Data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
