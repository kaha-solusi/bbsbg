<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use App\Models\PermintaanPengujian;
use Illuminate\Http\Request;

class PermintaanPengujianSpkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PermintaanPengujian $permintaan_pengujian)
    {
        $pegawai = Pegawai::whereHas('jabatan')
            ->with(['user:id,name', 'jabatan:id,nama'])
            ->select('id', 'nip', 'jabatan_id', 'user_id')
            ->get();

        return view(
            'admin.permintaan_pengujian.spk.index',
            compact(
                'permintaan_pengujian',
                'pegawai'
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PermintaanPengujian $permintaan_pengujian)
    {
        $pegawai = [];
        foreach ($request->pegawai as $p) {
            $pegawai[]['pegawai_id'] = $p;
        }

        $permintaan_pengujian->update([
            'status' => PermintaanPengujian::STATUS_SPK_SUBMITTED,
        ]);
        $permintaan_pengujian->spk()->create([
            'nomor' => $request->nomor,
            'metode_uji' => $request->metode_pengujian,
            'tanggal_mulai' => $request->tanggal_mulai,
            'tanggal_selesai' => $request->tanggal_selesai,
            'keterangan' => $request->catatan,
        ])->members()->createMany($pegawai);
        
        return redirect()->route('admin.permintaan-pengujian.index')->with('success', 'SPK berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
