<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\Models\Kuisioner;
use App\Models\PermintaanPengujian;

class PermintaanPengujianKuisionerController extends Controller
{
    public function index(PermintaanPengujian $permintaan_pengujian)
    {
        $kuisioners = Kuisioner::where('kategori_pelayanan_id', 1)->get();
        return view('admin.permintaan_pengujian.kuisioner', compact('kuisioners', 'permintaan_pengujian'));
    }
}
