<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\JabatanDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\JabatanRequest;
use App\Models\Jabatan;
use App\Services\JabatanService;

class JabatanController extends Controller
{
    private $service;

    public function __construct(JabatanService $service)
    {
        $this->service = $service;
    }

    public function index(JabatanDatatable $dataTable)
    {
        return $dataTable->render('admin.pegawai.jabatan.index');
    }

    /**
     * Menampilkan halaman untuk menambah data.
     */
    public function create()
    {
        return view('admin.pegawai.jabatan.create');
    }

    /**
     * Menyimpan data baru.
     */
    public function store(JabatanRequest $request)
    {
        $this->service->store(collect([
            'nama' => $request->nama,
        ]));

        return redirect()
            ->route('admin.pegawai.jabatan.index')
            ->with('success', 'Berhasil menambahkan jabatan baru');
    }

    /**
     * Menampilkan halaman untuk edit
     *
     * @param Jabatan $jabatan
     */
    public function edit(Jabatan $jabatan)
    {
        return view('admin.pegawai.jabatan.edit', compact('jabatan'));
    }

    /**
     * Menyimpan perubahan data
     *
     * @param Jabatan $jabatan
     * @param JabatanRequest $request
     */
    public function update(Jabatan $jabatan, JabatanRequest $request)
    {
        $this->service->update($jabatan, collect([
            'nama' => $request->nama,
        ]));

        return redirect()
            ->route('admin.pegawai.jabatan.index')
            ->with('success', 'Berhasil mengubah jabatan');
    }

    public function destroy(Jabatan $jabatan)
    {
        $this->service->destroy($jabatan);
        return back()->with('success', 'Berhasil menghapus jabatan');
    }
}
