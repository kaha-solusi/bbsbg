<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\PegawaiDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\PegawaiRequest;
use App\Models\DaftarDiklatPegawai;
use App\Models\Golongan;
use App\Models\Jabatan;
use App\Models\KompetensiPegawai;
use App\Models\LabPosition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Lab;
use App\Models\Faq;
use App\Models\LogAktivitas;
use App\Services\PegawaiService;
use App\User;
use Spatie\Permission\Models\Role;

class PegawaiController extends Controller
{
    private $service;
    public function __construct(PegawaiService $pegawaiService)
    {
        $this->service = $pegawaiService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PegawaiDatatable $pegawaiDatatable)
    {
        return $pegawaiDatatable->render('admin.pegawai.index');
    }
    public function indexfaq()
    {
        $data = Faq::orderBy('id', 'desc')->paginate(10);

        return view('admin.faq.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dataLab = Lab::all();
        $dataJabatan = Jabatan::all();
        $dataGolongan = Golongan::all();
        $dataJabatanLab = LabPosition::all();
        $roles = $this->getRoles();

        return view(
            'admin.pegawai.create',
            compact(
                'dataLab',
                'dataJabatan',
                'dataGolongan',
                'dataJabatanLab',
                'roles'
            )
        );
    }

    public function createfaq()
    {
        return view('admin.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storefaq(Request $request)
    {
        $v = Validator::make($request->all(), [
            'pertanyaan' => 'required|string',
            'jawaban' => 'required|string',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = Faq::create($request->only('pertanyaan', 'jawaban'));


        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Create Faq"
        ]);

        return back()->with('success', __('Berhasil dibuat!'));
    }

    public function store(PegawaiRequest $request)
    {
        $this->service->store(collect($request->except([
            '_token',
            'proengsoft_jsvalidation',
        ])));
        return redirect()
            ->route('admin.pegawai.index')
            ->with('success', __('Berhasil menambahkan pegawai baru!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $pegawai)
    {
        $data = $pegawai;
        $labs = Lab::all();
        $jabatans = Jabatan::all();
        $golongans = Golongan::all();
        $roles = $this->getRoles();
        $dataLab = Lab::all();
        $dataJabatanLab = LabPosition::all();
        $kompetensis = KompetensiPegawai::where('pegawai_id', $data->pegawai->id);
        $diklats = DaftarDiklatPegawai::where('pegawai_id', $data->pegawai->id);

        return view(
            'admin.pegawai.edit',
            compact(
                'data',
                'labs',
                'roles',
                'jabatans',
                'golongans',
                'dataJabatanLab',
                'kompetensis',
                'diklats'
            )
        );
    }
    public function editfaq($id)
    {
        $data = Faq::findOrFail($id);

        return view('admin.faq.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PegawaiRequest $request, User $pegawai)
    {
        try {
            if ($pegawai->pegawai->kompetensi()) {
                $pegawai->pegawai->kompetensi()->delete();
            }

            if ($request->get('kompetensi')) {
                $kompetensis = $request->get('kompetensi');
                foreach ($kompetensis as $kompetensi) {
                    $pegawai->pegawai->kompetensi()->create([
                        'nama' => $kompetensi,
                        'pegawai_id' => $pegawai->pegawai->id,
                    ]);
                }
            }

            if ($pegawai->pegawai->diklat()) {
                $pegawai->pegawai->diklat()->delete();
            }

            if ($request->get('daftar_diklat')) {
                $diklats = $request->get('daftar_diklat');
                foreach ($diklats as $diklat) {
                    $pegawai->pegawai->diklat()->create([
                        'nama' => $diklat,
                        'pegawai_id' => $pegawai->pegawai->id,
                    ]);
                }
            }

            $this->service->update($pegawai, collect($request->except([
                '_token',
                '_method',
                '_jsvalidation',
                '_jsvalidation_validate_all',
                'proengsoft_jsvalidation',
                'kompetensi',
                'daftar_diklat',
            ])));
            return redirect()
                ->route('admin.pegawai.index')
                ->with('success', __('Berhasil diperbarui!'));
        } catch (\Throwable $th) {
            report($th);
            return back()
                ->withErrors($th->getMessage())
                ->withInput();
        }
    }

    public function updatefaq(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'pertanyaan' => 'required|string',
            'jawaban' => 'required|string',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = Faq::findOrFail($id);

        $data->update($request->only('pertanyaan', 'jawaban'));


        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Faq"
        ]);

        return back()->with('success', __('Berhasil diupdate!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $pegawai)
    {
        $this->service->destroy($pegawai);
        return back()->with('success', 'Berhasil menghapus pengguna');
    }
    public function destroyfaq($id)
    {
        $data = Faq::findOrFail($id);

        $data->delete();

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Delete Faq"
        ]);

        return back()->with('success', __('Data berhasil dihapus.'));
    }

    private function getRoles()
    {
        return Role::whereNotIn('name', ['super-admin', 'pelanggan', 'admin'])
            ->orderBy('name', 'asc')
            ->get()
            ->toArray();
    }

    public function getLaboratoriumPositions(Request $request)
    {
        $positions = $this->service->getLaboratoriumPositionsByName($request->term);

        return response()->json(['results' => $positions]);
    }

    public function getFunctionalPositions(Request $request)
    {
        $positions = $this->service->getFunctionalPositionByName($request->term);

        return response()->json(['results' => $positions]);
    }
}
