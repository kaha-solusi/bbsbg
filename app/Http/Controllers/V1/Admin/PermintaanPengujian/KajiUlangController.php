<?php

namespace App\Http\Controllers\V1\Admin\PermintaanPengujian;

use App\DataTables\Admin\KajiUlangDatatable;
use App\Http\Controllers\Controller;
use App\Models\PermintaanPengujianKajiUlang;
use Illuminate\Http\Request;

class KajiUlangController extends Controller
{
    private $service;
    public function __construct()
    {
        $this->service = new \App\Services\KajiUlangService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(KajiUlangDatatable $dataTable)
    {
        $title = 'Kaji Ulang';
        return $dataTable->render('admin.utils.table-list', compact('title'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PermintaanPengujianKajiUlang $kaji_ulang_layanan)
    {
        return view('admin.permintaan_pengujian.kaji_ulang.show', compact('kaji_ulang_layanan'));
    }

    public function approve(PermintaanPengujianKajiUlang $kaji_ulang_layanan)
    {
        try {
            $this->service->approve($kaji_ulang_layanan);
            return response()->json([
                'status' => 'success',
                'message' => 'Kaji Ulang Layanan Berhasil Disetujui',
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json(['status' => 'error', 'message' => $th->getMessage()], $th->getCode());
        }
    }

    public function reject(Request $request, PermintaanPengujianKajiUlang $kaji_ulang_layanan)
    {
        try {
            $this->service->reject($kaji_ulang_layanan, $request->message);
            return response()->json([
                'status' => 'success',
                'message' => 'Kaji Ulang Layanan Berhasil Ditolak',
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json(['status' => 'error', 'message' => $th->getMessage()], $th->getCode());
        }
    }
}
