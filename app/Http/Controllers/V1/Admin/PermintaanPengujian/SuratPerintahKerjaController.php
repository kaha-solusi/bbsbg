<?php

namespace App\Http\Controllers\V1\Admin\PermintaanPengujian;

use App\DataTables\Admin\SuratPerintahKerjaDatatable;
use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use App\Models\PermintaanPengujianSpk;
use App\Services\SuratPerintahKerjaService;
use App\User;
use Illuminate\Http\Request;

class SuratPerintahKerjaController extends Controller
{
    private $service;
    public function __construct(SuratPerintahKerjaService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SuratPerintahKerjaDatatable $suratPerintahKerjaDatatable)
    {
        return $suratPerintahKerjaDatatable->render('admin.utils.table-list', [
            'title' => 'Surat Perintah Kerja',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PermintaanPengujianSpk $surat_perintah_kerja)
    {
        return view('admin.permintaan_pengujian.surat_perintah_kerja.show', compact('surat_perintah_kerja'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PermintaanPengujianSpk $surat_perintah_kerja)
    {
        $pegawai = User::whereHas('pegawai')
            ->whereHas('roles', function ($query) {
                return $query->where('name', '!=', 'direktur')
                    ->where('name', '!=', 'kepala-balai');
            })
            ->with(['pegawai:id,nip,user_id,jabatan_id', 'pegawai.lab_positions:id,name'])
            ->select('id', 'name', 'username', 'email')
            ->get();
        return view('admin.permintaan_pengujian.surat_perintah_kerja.edit', compact('surat_perintah_kerja', 'pegawai'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PermintaanPengujianSpk $surat_perintah_kerja)
    {
        try {
            $this->service->update($request, $surat_perintah_kerja);
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json($th->getMessage(), 500);
        }
    }

    public function approve(PermintaanPengujianSpk $surat_perintah_kerja)
    {
        try {
            $this->service->approve($surat_perintah_kerja);
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json($th->getMessage(), 500);
        }
    }

    public function reject(Request $request, PermintaanPengujianSpk $surat_perintah_kerja)
    {
        try {
            $this->service->reject($surat_perintah_kerja, $request->message);
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json($th->getMessage(), 500);
        }
    }
}
