<?php

namespace App\Http\Controllers\V1\Admin\PermintaanPengujian;

use App\DataTables\Admin\PembayaranDatatable;
use App\Http\Controllers\Controller;
use App\Models\PermintaanPengujianPayment;
use App\Services\PembayaranService;
use Illuminate\Http\Request;

class PembayaranController extends Controller
{
    private $service;
    public function __construct(PembayaranService $pembayaranService)
    {
        $this->service = $pembayaranService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PembayaranDatatable $dataTable)
    {
        return $dataTable->render('admin.utils.table-list', [
            'title' => 'Pembayaran',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PermintaanPengujianPayment $pembayaran)
    {
        return view('admin.permintaan_pengujian.pembayaran.show', compact('pembayaran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PermintaanPengujianPayment $pembayaran)
    {
        return view('admin.permintaan_pengujian.pembayaran.edit', compact('pembayaran'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PermintaanPengujianPayment $pembayaran)
    {
        try {
            $this->service->submitKodeBilling($pembayaran, $request);
            return redirect()
                ->route('admin.permintaan-pengujian.pembayaran.index')
                ->with('success', 'Kode billing berhasil diubah');
        } catch (\Throwable $th) {
            report($th);
            return redirect()->back()->with('error', 'Terjadi kesalahan saat mengubah data');
        }
    }
    
    public function download(PermintaanPengujianPayment $pembayaran)
    {
        try {
            $downloadURL = $this->service->downloadPaymentConfirmation($pembayaran);
            return response()->json([
                'success' => true,
                'downloadURL' => $downloadURL,
            ]);
        } catch (\Throwable $th) {
            report($th);
            return redirect()->back()->with('error', $th->getMessage());
        }
    }
    
    public function approve(PermintaanPengujianPayment $pembayaran)
    {
        try {
            $this->service->approve($pembayaran);
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }
    
    public function reject(Request $request, PermintaanPengujianPayment $pembayaran)
    {
        try {
            $this->service->reject($pembayaran, $request->message);
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }
}
