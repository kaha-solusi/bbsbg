<?php

namespace App\Http\Controllers\V1\Admin\PermintaanPengujian;

use App\DataTables\Admin\PenerimaanSampleDatatable;
use App\Http\Controllers\Controller;
use App\Models\PermintaanPengujianSample;
use App\Services\PenerimaanBendaUjiService;
use Illuminate\Http\Request;

class PenerimaanBendaUjiController extends Controller
{
    private $service;
    public function __construct(PenerimaanBendaUjiService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PenerimaanSampleDatatable $dataTable)
    {
        return $dataTable->render('admin.utils.table-list', [
            'title' => 'Penerimaan Benda Uji',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PermintaanPengujianSample $penerimaan_benda_uji)
    {
        return view('admin.permintaan_pengujian.penerimaan_benda_uji.show', compact('penerimaan_benda_uji'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PermintaanPengujianSample $penerimaan_benda_uji)
    {
        return view('admin.permintaan_pengujian.penerimaan_benda_uji.edit', compact('penerimaan_benda_uji'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function approve(Request $request, PermintaanPengujianSample $sample)
    {
        try {
            $this->service->approve($request, $sample);
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }
    
    public function reject(Request $request, PermintaanPengujianSample $sample)
    {
        try {
            $this->service->reject($request, $sample);
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }
}
