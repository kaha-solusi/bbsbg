<?php

namespace App\Http\Controllers\V1\Admin\PermintaanPengujian;

use App\DataTables\Admin\HasilPengujianDatatable;
use App\Http\Controllers\Controller;
use App\Models\PermintaanPengujian;
use App\Models\PermintaanPengujianItem;
use App\Models\PermintaanPengujianPengujian;
use App\Models\PermintaanPengujianSample;
use App\Models\PermintaanPengujianSampleItem;
use App\Models\PermintaanPengujianSpk;
use App\Services\PeralatanLabUsageService;
use Illuminate\Http\Request;

class HasilPengujianController extends Controller
{
    private $peralatanService;
    public function __construct(PeralatanLabUsageService $peralatanLabUsageService)
    {
        $this->peralatanService = $peralatanLabUsageService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HasilPengujianDatatable $hasilPengujianDatatable)
    {
        return $hasilPengujianDatatable->render('admin.utils.table-list', [
            'title' => 'Proses Pengujian',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PermintaanPengujianPengujian $proses_pengujian)
    {
        return view('admin.permintaan_pengujian.proses_pengujian.edit', compact('proses_pengujian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function editHasilPengujian(PermintaanPengujianPengujian $proses_pengujian, PermintaanPengujianItem $item)
    {
        return view(
            'admin.permintaan_pengujian.proses_pengujian.edit_hasil_pengujian',
            compact(
                'proses_pengujian',
                'item'
            )
        );
    }

    public function updateHasilPengujian(
        Request $request,
        PermintaanPengujianPengujian $proses_pengujian,
        PermintaanPengujianItem $item
    )
    {
        $spk = PermintaanPengujianSpk::where('permintaan_pengujian_id', $proses_pengujian->permintaan_pengujian_id)->first();
        $tanggal= date('Y-m-d',strtotime(str_replace('/','-',$spk->tanggal_mulai)));

        try {
            $pengujianItem = $proses_pengujian->items()->create([
                'item_id' => $item->id,
                'kategori_pengujian' => $request->jenis_pengujian,
                'hasil_pengujian' => json_encode($request->except('_token', '_method', 'jenis_pengujian', 'alat_pengujian')),
                'status' => PermintaanPengujian::STATUS_SUBMITTED
            ]);
            $pengujianItem->peralatan()->createMany(array_map(function ($item) {
                return ['peralatan_id' => $item];
            }, $request->alat_pengujian));
            $this->peralatanService->store(collect([
                'tanggal' => $tanggal,
                'kategori' => 'Pemakaian',
                'keterangan' => $spk->permintaan_pengujian->master_layanan_uji->name .' '.
                                $spk->permintaan_pengujian->master_layanan_uji->pelayanan->nama .' '.
                                $request->jenis_pengujian .' '. $item->product_type .' '.
                                $item->sample->kode_sampel,
                'peralatan' => $request->alat_pengujian,
                'pelaksana' => auth()->user()->name
            ]));

            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function showHasilPengujian(
        PermintaanPengujianPengujian $proses_pengujian,
        PermintaanPengujianItem $item
    )
    {
        return view(
            'admin.permintaan_pengujian.proses_pengujian.show_hasil_pengujian',
            compact(
                'proses_pengujian',
                'item'
            )
        );
    }
}
