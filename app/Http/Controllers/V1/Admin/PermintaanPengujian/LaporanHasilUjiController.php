<?php

namespace App\Http\Controllers\V1\Admin\PermintaanPengujian;

use App\DataTables\Admin\LaporanHasilUjiDatatable;
use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\LaporanHasilUji;
use App\Models\LaporanHasilUjiApproval;
use App\Models\PermintaanPengujian;
use App\Services\LaporanHasilUjiService;
use App\Services\UploadService;
use Illuminate\Http\Request;

class LaporanHasilUjiController extends Controller
{
    private $service;
    private $uploadService;
    public function __construct(LaporanHasilUjiService $service,UploadService $uploadService)
    {
        $this->service = $service;
        $this->uploadService = $uploadService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(LaporanHasilUjiDatatable $dataTable)
    {
        return $dataTable->render('admin.utils.table-list', [
            'title' => 'Laporan Hasil Uji',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->service->createLHU($request->pengujian_id);
            return response()->json([
                'status' => 'success',
                'message' => 'Data berhasil disimpan',
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json(['status' => 'error', 'message' => $th->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LaporanHasilUji $laporan_hasil_uji)
    {
        $cttn = 'Pengujian dilakukan sampai benda uji hancur,
F: beban luar (sudah termasuk berat load cell dan berat perata beban),
M: Moment lentur akibat P dan berat sendiri benda uji,
δ:Lendutan,
L: Panjang Bentang/Benda Uji';
        $lhu='';
        if ($laporan_hasil_uji->file) {
            $file=File::find($laporan_hasil_uji->file);
            $lhu=$file->filename;
        }
        return view('admin.permintaan_pengujian.laporan_hasil_uji', compact('laporan_hasil_uji','cttn','lhu'));
    }

    public function printPreview(LaporanHasilUji $laporan_hasil_uji)
    {
        return view('admin.permintaan_pengujian.laporan_hasil_uji_print', compact('laporan_hasil_uji'));
    }

    public function print(LaporanHasilUji $laporan_hasil_uji)
    {
        $pdf = \PDF::loadView('admin.permintaan_pengujian.laporan_hasil_uji_print', compact('laporan_hasil_uji'));
        return $pdf->download('invoice.pdf');
        // return view('admin.permintaan_pengujian.laporan_hasil_uji_print', compact('laporan_hasil_uji'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LaporanHasilUji $laporan_hasil_uji)
    {
        $all=$request->all();
        try {
            $laporan_hasil_uji->update([
                'deskripsi_benda_uji' => $request->deskripsi_benda_uji,
                'tempat' => $request->tempat,
                'tanggal_terbit' => $request->tanggal_terbit,
                'catatan' => $request->catatan,
            ]);

            if ($request->file() != null) {
                if ($laporan_hasil_uji->file) {
                    $file = File::find($laporan_hasil_uji->file);
                    $lhu = $file->filename;
                    $this->uploadService->removeFile('uploads/laporan_hasil_uji' , $lhu);
                }
                $lhu = $this->uploadService->saveFile($all['lhu'], 'uploads/laporan_hasil_uji');
                $laporan_hasil_uji->update([
                    'file' => $lhu,
                    'status' => PermintaanPengujian::STATUS_COMPLETED,
                ]);
                $pengujian = PermintaanPengujian::find($laporan_hasil_uji->pengujian_id);
                $pengujian->update([
                    'status' => PermintaanPengujian::STATUS_LHU_SUBMITTED
                ]);
            }
            return redirect()
                ->route('admin.permintaan-pengujian.laporan-hasil-uji.index')
                ->with('success', 'Berhasil mengubah data');
        } catch (\Throwable $th) {
            report($th);
            return redirect()
                ->route('admin.permintaan-pengujian.laporan-hasil-uji.index')
                ->with('error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
