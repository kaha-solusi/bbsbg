<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\LabPositionDatatable;
use App\Http\Controllers\Controller;
use App\Models\LabPosition;
use App\Services\LabPositionService;
use Illuminate\Http\Request;

class LabPositionController extends Controller
{
    private $service;
    public function __construct(LabPositionService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(LabPositionDatatable $labPositionDatatable)
    {
        return $labPositionDatatable->render('admin.pegawai.jabatan_laboratorium.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pegawai.jabatan_laboratorium.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->service->store(collect([
            'name' => $request->nama,
        ]));

        return redirect()
            ->route('admin.pegawai.jabatan-laboratorium.index')
            ->with('success', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LabPosition $jabatan_laboratorium)
    {
        // return view('admin.pegawai.jabatan_laboratorium.show', compact('jabatan_laboratorium'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LabPosition $jabatan_laboratorium)
    {
        $data = $jabatan_laboratorium;
        return view('admin.pegawai.jabatan_laboratorium.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LabPosition $jabatan_laboratorium)
    {
        $this->service->update(collect([
            'name' => $request->nama,
        ]), $jabatan_laboratorium);

        return redirect()
            ->route('admin.pegawai.jabatan-laboratorium.index')
            ->with('success', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LabPosition $jabatan_laboratorium)
    {
        $this->service->destroy($jabatan_laboratorium);
        return redirect()
            ->route('admin.pegawai.jabatan-laboratorium.index')
            ->with('success', 'Data berhasil dihapus');
    }
}
