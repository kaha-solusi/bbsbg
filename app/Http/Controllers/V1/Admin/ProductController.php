<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\ProductDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\ProductAlatKerja;
use App\Models\ProductBahanKerja;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $service;
    public function __construct(ProductService $productService)
    {
        $this->service = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductDatatable $dataTable)
    {
        return $dataTable->render('admin.master.products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $testParameters = $this->service->getParameterPengujian();
        return view('admin.master.products.create', compact('testParameters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        try {
            $this->service->store(collect([
                'name' => $request->name,
                'alat_kerja' => $request->alat_kerja,
                'bahan_kerja' => $request->bahan_kerja,
                'description' => $request->description,
                'testParameters' => $request->test_parameters,
            ]));
            return redirect()
                ->route('admin.products.index')
                ->with('success', 'Berhasil menambahkan produk baru!');
        } catch (\Throwable $th) {
            report($th);
            return redirect()->back()->withErrors($th->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $testParameters = $this->service->getParameterPengujian();
        $alats = ProductAlatKerja::all();
        $bahans = ProductBahanKerja::all();
        return view('admin.master.products.edit', compact('product', 'testParameters','alats','bahans'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        try {
            if ($product->alat()->get()) {
                $product->alat()->delete();
            }
            if ($product->bahan()->get()) {
                $product->bahan()->delete();
            }

            $this->service->update($product, collect([
                'name' => $request->name,
                'alat_kerja' => $request->alat_kerja,
                'bahan_kerja' => $request->bahan_kerja,
                'description' => $request->description,
                'parameters' => $request->test_parameters
            ]));
            return redirect()
                ->route('admin.products.index')
                ->with('success', 'Berhasil mengubah produk!');
        } catch (\Throwable $th) {
            report($th);
            return redirect()->back()->withErrors($th->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->service->destroy($product);
        return redirect()->back()->with('success', 'Berhasil menghapus produk');
    }
}
