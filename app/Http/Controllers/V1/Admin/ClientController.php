<?php

namespace App\Http\Controllers\V1\Admin;

use App\DataTables\Admin\ClientDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Client;
use App\User;
use App\Models\LogAktivitas;
use App\Services\ClientService;
use Illuminate\Support\Facades\Hash;

class ClientController extends Controller
{
    private $service;
    public function __construct(ClientService $clientService)
    {
        $this->service = $clientService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ClientDatatable $clientDatatable)
    {
        return $clientDatatable->render('admin.client.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        try {
            $this->service->store(collect($request->except([
                '_token',
                'proengsoft_jsvalidation',
            ])));
            return redirect()
                ->route('admin.client.index')
                ->with('success', __('Berhasil menambahkan client baru!'));
        } catch (\Throwable $th) {
            report($th);
            return back()
                ->withErrors($th->getMessage())
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $client)
    {
        $data = $client;
        return view('admin.client.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientRequest $request, User $client)
    {
        try {
            $this->service->update($client, collect($request->except([
                '_token',
                'proengsoft_jsvalidation',
                '_method',
            ])));
            return redirect()
                ->route('admin.client.index')
                ->with('success', __('Berhasil diperbarui!'));
        } catch (\Throwable $th) {
            report($th);
            return back()
                ->withErrors($th->getMessage())
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $client)
    {
        $this->service->destroy($client);
        return back()->with('success', 'Berhasil menghapus pengguna');
    }

    public function settingClient()
    {
        $id = auth()->user()->client->id;
        $data = Client::with('user')->findOrFail($id);

        return view('admin.setting-clent.edit', compact('data'));
    }


    public function settingClientUpdate(Request $request)
    {
        $id = auth()->user()->client->id;
        $data = Client::findOrFail($id);

        $v = Validator::make($request->all(), [
            'logo' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'name' => 'required|string|max:25',
            'username' => 'required|string|max:25',
            'website' => 'nullable|url',
            'alamat' => 'required|string|max:65534',
            'email' => 'required|string|email|unique:users,email,'.$data->user_id,
            'phone_number' => 'required|string|regex:/(628)[0-9]{9}/',
            // 'nama_perusahaan' => 'required|string',
            // 'bidang_usaha' => 'required|string'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $user = User::find($data->user_id);

        $user->update($request->only([
            'name',
            'email',
            'username',
        ]));

        $data->update($request->only('name', 'website', 'alamat', 'phone_number'));

        if ($request->file('logo')) {
            // File::delete($data->logo);

            $name = $request->file('logo');
            $logo = time()."_".$name->getClientOriginalName();
            $request->logo->move("upload/logo/client", $logo);

            $data->update([
                'logo' => '/upload/logo/client/'.$logo,
            ]);
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Client"
        ]);

        return back()->with('success', __('Berhasil diupdate!'));
    }

    public function password(Request $request)
    {
        $v = Validator::make($request->all(), [
            'password' => 'required|string|min:8|confirmed',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $id = auth()->user()->id;

        $data = User::find($id);

        $data->update([
            'password' => Hash::make($request->password),
        ]);

        return back()->with('success', __('Berhasil direset!'));
    }
}
