<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\Models\PermintaanPengujian;
use App\Models\PermintaanPengujianItem;
use App\Services\ProsesPengujianService;
use Illuminate\Http\Request;

class ProsesPengujianController extends Controller
{
    private $service;
    public function __construct(ProsesPengujianService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PermintaanPengujian $permintaan_pengujian, PermintaanPengujianItem $parameter)
    {
        $pengujian = $permintaan_pengujian;
        return view(
            'admin.permintaan_pengujian.proses-pengujian.create',
            compact('pengujian', 'parameter')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PermintaanPengujian $permintaan_pengujian, PermintaanPengujianItem $parameter, Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermintaanPengujian $permintaan_pengujian, PermintaanPengujianItem $parameter, Request $request)
    {
        try {
            $this->service->store($request, $permintaan_pengujian, $parameter);
            return redirect()
                ->route('admin.permintaan-pengujian.show', [
                    $permintaan_pengujian->id,
                ])
                ->with('success', 'Data berhasil disimpan');
        } catch (\Throwable $th) {
            report($th);
            return redirect()->back()->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PermintaanPengujian $permintaan_pengujian, PermintaanPengujianItem $parameter, Request $request)
    {
        $pengujian = $permintaan_pengujian;
        return view(
            'admin.permintaan_pengujian.proses-pengujian.edit',
            compact('pengujian', 'parameter')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
