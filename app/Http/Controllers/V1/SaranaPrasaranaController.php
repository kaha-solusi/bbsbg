<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\SaranaPrasarana;
use App\Models\LogAktivitas;
use Illuminate\Support\Facades\File;

class SaranaPrasaranaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SaranaPrasarana::orderBy('id', 'desc')->paginate(10);

        return view('admin.fasilitas', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.fasilitas-p.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'foto' => 'required|image|mimes:jpg,png,jpeg|max:2048',
            'nama' => 'required|string',
            'kategori' => 'required',
            'keterangan' => 'nullable|string|max:65534'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = SaranaPrasarana::create($request->only('nama', 'keterangan', 'kategori'));

        // File::delete($data->foto);

        $name = $request->file('foto');
        $logo = time()."_".$name->getClientOriginalName();
        $request->foto->move("fasilitas/foto", $logo);

        SaranaPrasarana::find($data->id)->update([
            'foto' => '/fasilitas/foto/'.$logo
        ]);        

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Create Sarana Prasarana"
        ]); 

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SaranaPrasarana::findOrFail($id);

        return view('admin.fasilitas-p.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(),[
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'nama' => 'required|string',
            'keterangan' => 'nullable|string|max:65534',
            'kategori' => 'required',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = SaranaPrasarana::findOrFail($id);

        $data->update($request->only(['keterangan', 'nama', 'kategori']));

        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("fasilitas/foto", $logo);

            $data->update([
                'foto' => '/fasilitas/foto/'.$logo
            ]);        
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Sarana Prasarana"
        ]); 

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = SaranaPrasarana::findOrFail($id);

        // File::delete($data->foto);

        $data->delete();

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Delete Sarana Prasarana"
        ]); 

        return back()->with('success', __( 'Data berhasil dihapus.' ));
    }
}
