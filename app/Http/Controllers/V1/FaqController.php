<?php

namespace App\Http\Controllers\V1;

use App\Exports\PegawaiExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Pegawai as Budak;
use App\Models\Lab;
use App\Models\Faq;
use App\Models\LogAktivitas;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Faq::orderBy('id', 'desc')->paginate(10);

        return view('admin.faq.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Lab::all();

        return view('admin.pegawai-p.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'nama' => 'required|string|max:20',
            'email' => 'required|string|email|unique:pegawais,email',
            'nip' => 'required|numeric',
            'lab_id' => 'required|exists:labs,id',
            'no_hp' => 'required|string|regex:/(628)[0-9]{9}/'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = Budak::create($request->only('nama', 'email', 'nip', 'lab_id', 'no_hp'));

        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("upload/foto/pegawai", $logo);

            Budak::find($data->id)->update([
                'foto' => '/upload/foto/pegawai/'.$logo
            ]);         
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Create Pegawai"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Budak::findOrFail($id);
        $labs = Lab::all();

        return view('admin.pegawai-p.edit', compact('data', 'labs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(),[
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'nama' => 'required|string|max:20',
            'email' => 'required|string|email|unique:pegawais,email,'.$id,
            'nip' => 'required|numeric',
            'lab_id' => 'required|exists:labs,id',
            'no_hp' => 'required|string|regex:/(628)[0-9]{9}/'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $data = Budak::findOrFail($id);

        $data->update($request->only('nama', 'email', 'nip', 'lab_id', 'no_hp'));

        if ($request->file('foto')) {
            // File::delete($data->foto);

            $name = $request->file('foto');
            $logo = time()."_".$name->getClientOriginalName();
            $request->foto->move("upload/foto/pegawai", $logo);

            $data->update([
                'foto' => '/upload/foto/pegawai/'.$logo
            ]);         
        }

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Pegawai"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Budak::findOrFail($id);

        // File::delete($data->foto);

        $data->delete();

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Delete Pegawai"
        ]);

        return back()->with('success', __( 'Data berhasil dihapus.' ));
    }

}
