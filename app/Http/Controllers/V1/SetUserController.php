<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\DaftarDiklatPegawai;
use App\Models\Golongan;
use App\Models\Jabatan;
use App\Models\KompetensiPegawai;
use App\Models\Lab;
use App\Models\LabPosition;
use App\Services\PegawaiService;
use Illuminate\Http\Request;
use App\User;
use App\Models\LogAktivitas;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class SetUserController extends Controller
{
    private $service;
    public function __construct(PegawaiService $pegawaiService)
    {
        $this->service = $pegawaiService;
    }
    public function create()
    {
    	$data = auth()->user();
        $labs = Lab::all();
        $jabatans = Jabatan::all();
        $golongans = Golongan::all();
        $dataJabatanLab = LabPosition::all();
        $kompetensis = KompetensiPegawai::all();
        $diklats = DaftarDiklatPegawai::all();

    	return view('admin.setting-user.update', compact('data',
            'labs',
            'jabatans',
            'golongans',
            'dataJabatanLab',
            'kompetensis',
            'diklats'));
    }

    public function store(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'name' => 'required|string|max:50',
            'username' => 'required|string|max:50',
            'email' => 'required|string|email|unique:users,email,'.auth()->user()->id,
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:5048',
            'nip' => 'required|numeric',
            'jabatan' => 'required|exists:jabatans,id',
            'golongan' => 'required|exists:golongans,id',
            'no_hp' => 'required|string|regex:/(628)[0-9]{9,}/',
            'gender' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|before:today'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $id = auth()->user()->id;
        $data = User::find($id);

        if ($data->pegawai->kompetensi()) {
            $data->pegawai->kompetensi()->delete();
        }

        if ($request->get('kompetensi')) {
            $kompetensis = $request->get('kompetensi');
            foreach ($kompetensis as $kompetensi) {
                $data->pegawai->kompetensi()->create([
                    'nama' => $kompetensi,
                    'pegawai_id' => $data->pegawai->id
                ]);
            }
        }

        if ($data->pegawai->diklat()) {
            $data->pegawai->diklat()->delete();
        }

        if ($request->get('daftar_diklat')) {
            $diklats = $request->get('daftar_diklat');
            foreach ($diklats as $diklat) {
                $data->pegawai->diklat()->create([
                    'nama' => $diklat,
                    'pegawai_id' => $data->pegawai->id
                ]);
            }
        }

        $this->service->update($data, collect([
            'nama' => $request->name,
            'username' => $request->username,
            'nip' => $request->nip,
            'lab_id' => $request->lab_id,
            'jabatan_id' => $request->jabatan,
            'jabatan_laboratorium' => $request->jabatan_laboratorium,
            'golongan_id' => $request->golongan,
            'no_hp' => $request->no_hp,
            'gender' => $request->gender,
            'role' => $data->level,
            'birth_place' => $request->tempat_lahir,
            'birth_date' => $request->tanggal_lahir
        ]));
        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    public function password(Request $request)
    {
        $v = Validator::make($request->all(),[
            'password' => 'required|string|min:8|confirmed'
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        $id = auth()->user()->id;

        $data = User::find($id);

        $data->update([
            'password' => Hash::make($request->password)
        ]);

        return back()->with('success', __( 'Berhasil direset!' ));
    }
}
