<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PermintaanPengujian as Pendaftaran;
use App\Models\SettingApp;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\PermintaanPengujian as PermintaanMail;

class PermintaanPengujianStruktur extends Controller
{
	public function index()
	{
		# code...
	}

    public function create()
    {
    	return view('homepage.pendaftaran-pengujian-struktur');
    }

    public function store(Request $request)
    {
    	$v = Validator::make($request->all(),[
            'nama_pemohon' => 'required|string|max:250',
            'alamat' => 'required|string|max:65534',
            'nama_kontak' => 'required|string|max:250',
            'nomor_hp_kontak' => 'required|string|regex:/(628)[0-9]{9}/',
            'email' => 'required|string|email',
            'tanggal_pengujian' => 'required|date',
            'uraian_pengujian' => 'required|string|max:65534',
            'detail_layanan' => 'required|string|max:65534',
            'nama_lhu' => 'nullable|string|max:250',
            'email_persuratan' => 'required|string|email',
        ],[
        	'nama_pemohon.required' => 'Harap isi form ini',
        	'alamat.required' => 'Harap isi form ini',
        	'nomor_hp_kontak.required' => 'Harap isi form ini',
        	'email.required' => 'Harap isi form ini',
        	'tanggal_pengujian.required' => 'Harap isi form ini',
        	'uraian_pengujian.required' => 'Harap isi form ini',
        	'detail_layanan.required' => 'Harap isi form ini',
        	'email_persuratan.required' => 'Harap isi form ini',
        ]);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }

        Pendaftaran::create($request->only('nama_pemohon', 'email', 'nama_kontak', 'nomor_hp_kontak', 'alamat', 'nama_lhu', 'email_persuratan', 'tanggal_pengujian', 'uraian_pengujian', 'detail_layanan')+[
        	'jenis_layanan' => 'struktur',
        	'tgl_permintaan' => date('d-m-Y', strtotime(now('Asia/Jakarta'))),
        	'status' => 'diproses'
        ]);

        $setting = SettingApp::find(1);

		$plus = '';

		if ($setting->email != null) {
			// Mail::to($setting->email)->send(new PermintaanMail(now('Asia/Jakarta'), 'Struktur', $request->nama_pemohon, $request->email));
		}

        return back()->with('success', __( 'Pendaftaran dikirim.' ));
    }
}
