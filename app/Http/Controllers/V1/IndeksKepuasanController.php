<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\IndeksKepuasan;
use App\Models\LogAktivitas;

class IndeksKepuasanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = IndeksKepuasan::orderBy('id', 'asc')->paginate(10);

        $search = $request->query('search', null);

        if ($search != null) {
            $data = IndeksKepuasan::where('tahun', 'like', '%'.$search.'%')
            ->orWhere('jumlah_responden', 'like', '%'.$search.'%')
            ->orWhere('nilai_ikm', 'like', '%'.$search.'%')
            ->orWhere('indexs', 'like', '%'.$search.'%')
            ->orWhere('nilai_mutu', 'like', '%'.$search.'%')
            ->orWhere('predikat', 'like', '%'.$search.'%')
            ->orderBy('id', 'desc')
            ->paginate(10);
        }

        return view('admin.indeks-kepuasan.table', compact('data', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.indeks-kepuasan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'tahun' => 'required|integer|regex:/[0-9]{4}/',
            'jumlah_responden' => 'required|integer',
            'nilai_ikm' => 'required|numeric|min:25|max:100'
        ]);

        if ($v->fails()) {
            dd($v);
            return back()->withErrors($v)->withInput();
        }

        $ikm = $request->nilai_ikm;

        $index = (float)$ikm / 25;

        $index = round($index, 2, PHP_ROUND_HALF_UP);

        if ($index >= 1.00 && $index <= 1.75) {
            $mutu = 'D';
            $predikat = 'Sangat Tidak Puas';
        } elseif ($index >= 1.76 && $index <= 2.50) {
            $mutu = 'C';
            $predikat = 'Tidak Puas';
        } elseif ($index >= 2.51 && $index <= 3.25) {
            $mutu = 'B';
            $predikat = 'Puas';
        } elseif ($index >= 3.26 && $index <= 4.00) {
            $mutu = 'A';
            $predikat = 'Sangat Puas';
        }
        

        $data = IndeksKepuasan::create($request->only('tahun', 'jumlah_responden', 'nilai_ikm')+[
            'indexs' => $index,
            'nilai_mutu' => $mutu,
            'predikat' => $predikat
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Create Indeks Kepuasan"
        ]);

        return back()->with('success', __( 'Berhasil dibuat!' ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = IndeksKepuasan::findOrFail($id);

        return view('admin.indeks-kepuasan.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(),[
            'tahun' => 'required|integer|regex:/[0-9]{4}/',
            'jumlah_responden' => 'required|integer',
            'nilai_ikm' => 'required|numeric|min:25|max:100'
        ]);

        if ($v->fails()) {
            dd($v);
            return back()->withErrors($v)->withInput();
        }

        $ikm = $request->nilai_ikm;

        $index = (float)$ikm / 25;

        $index = round($index, 2, PHP_ROUND_HALF_UP);

        if ($index >= 1.00 && $index <= 1.75) {
            $mutu = 'D';
            $predikat = 'Sangant Tidak Puas';
        } elseif ($index >= 1.76 && $index <= 2.50) {
            $mutu = 'C';
            $predikat = 'Tidak Puas';
        } elseif ($index >= 2.51 && $index <= 3.25) {
            $mutu = 'B';
            $predikat = 'Puas';
        } elseif ($index >= 3.26 && $index <= 4.00) {
            $mutu = 'A';
            $predikat = 'Sangat Puas';
        }
        
        $data = IndeksKepuasan::findOrFail($id);

        $data->update($request->only('tahun', 'jumlah_responden', 'nilai_ikm')+[
            'indexs' => $index,
            'nilai_mutu' => $mutu,
            'predikat' => $predikat
        ]);

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Update Indeks Kepuasan"
        ]);

        return back()->with('success', __( 'Berhasil diupdate!' ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = IndeksKepuasan::findOrFail($id);

        $data->delete();

        LogAktivitas::create([
            'user_id' => auth()->user()->id,
            'date' => now('Asia/Jakarta'),
            'log' => "Delete Indeks Kepuasan"
        ]);

        return back()->with('success', __( 'Data berhasil dihapus.' ));
    }
}
