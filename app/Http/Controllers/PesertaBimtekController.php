<?php

namespace App\Http\Controllers;

use App\DataTables\Admin\PesertaBimtekDatatable;
use App\Models\Client;
use App\Models\Bimtek;
use App\Models\PesertaBimtek;
use App\Mail\PesertaBimtekEmail;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class PesertaBimtekController extends Controller
{
    public function index(PesertaBimtekDatatable $pesertaBimtekDatatable)
    {
        // dd($pesertaBimtekDatatable);
        return $pesertaBimtekDatatable->render('admin.utils.table-list', [
            'title' => 'Peserta Bimtek',
        ]);
        // return view('admin.pesertabimtek', [
        //     'bimtek' => $bimtek->nama_bimtek,
        //     'peserta' => $bimtek->peserta,
        // ]);
    }
    public function store(Request $request)
    {
        $currentuserid = Auth::user()->id;
        $currentusername = Auth::user()->name;
        $currentuseremail = Auth::user()->email;

        //validasi
        $validator = Validator::make($request->all(), [
            'bimtek_id' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $exist = PesertaBimtek::where('bimtek_id', $request->bimtek_id)
                ->where('user_id', $currentuserid)
                ->get();

        $bimtek = Bimtek::findOrFail($request->bimtek_id);

        if ($exist->isEmpty()) {
            PesertaBimtek::create(
                array(
                    "bimtek_id"    => $request->bimtek_id,
                    "user_id"     => $currentuserid,
                )
            );

            if ($bimtek->jenis_bimtek == 'reguler') {
                Mail::to($currentuseremail)->send(new PesertaBimtekEmail(
                    $request->nama,
                    $bimtek->nama_bimtek,
                    $bimtek->tanggal_pelaksanaan,
                    $bimtek->waktu_pelaksanaan,
                    $bimtek->syarat_peserta,
                    $bimtek->link_zoom,
                    $bimtek->meeting_id,
                    $bimtek->passcode,
                    $bimtek->deskripsi_bimtek,
                    $bimtek->bimtek_subjek_email,
                    $bimtek->bimtek_konten_email,
                    $bimtek->bimtek_background
                ));

                return redirect('/home/pelayanan/4')
                    ->with(
                        'success',
                        'Pendaftaran Bimtek '.
                        $bimtek->nama_bimtek.
                        ' atas nama '.
                        $currentusername.
                        ' berhasil. Informasi Pendaftaran Telah Dikirim ke Email '.$currentuseremail.'!'
                    );
            } else {
                return redirect('/home/pelayanan/4')
                    ->with('success', 'Pendaftaran berhasil. Menunggu Persetujuan Dari Administrator.');
            }
        } else {
            return redirect('/home/pelayanan/4')
                ->with('success', 'Anda sudah terdaftar sebagai peserta Bimbingan Teknis '.$bimtek->nama_bimtek.'.');
        }
    }

    public function edit($id)
    {
        return view('admin.pesertabimtek.edit', [
            'peserta' => PesertaBimtek::findOrFail($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        //validasi
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'nip' => 'required',
            'no_hp' => 'required',
            'email' => 'required',
            'alamat' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = PesertaBimtek::findOrFail($id);
        $data->update($request->only('status', 'link_video_conf'));

        return back()->with('success', 'Berhasil di Update !');
    }

    public function destroy($id)
    {
        $bimtek = PesertaBimtek::findOrFail($id);
        $bimtek->delete();
        return back()->with('success', 'Berhasil di Hapus !');
    }

    public function cert()
    {

        return view('bimtek.cert');
    }
}
