<?php

namespace App\Http\Controllers;

use App\Imports\PegawaiImport;
use App\Imports\PelayananImport;
use App\Imports\PeralatanLabImport;
use App\Imports\StrukturImport;
use App\Models\Lab;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use ZanySoft\Zip\Zip;
use Illuminate\Support\Facades\File;
use ZipArchive;

class ImportController extends Controller
{
    public function pegawai(Request $request)
    {
        try {
            Excel::import(new PegawaiImport, $request->file('file'));
        } catch (\Throwable $th) {
            return back()->with('failed', $th->getMessage());
        }
        return back()->with('success', 'Import File Berhasil !');
    }

    public function peralatan(Request $request)
    {
        try {
            Excel::import(new PeralatanLabImport, $request->file('file'));
            return back()->with('success', 'Import File Berhasil !');
        } catch (\Throwable $th) {
            return back()->with('failed', $th->getMessage());
        }
    }

    public function struktur_organisasi(Request $request)
    {
        try {
            Excel::import(new StrukturImport, $request->file('file'));
        } catch (\Throwable $th) {
            return back()->with('failed', $th->getMessage());
        }
        return back()->with('success', 'Import File Berhasil !');
    }

    public function pelayanan(Request $request, $kategori)
    {
        // dd($request->all());
        try {
            Excel::import(new PelayananImport($kategori), $request->file('file'));
            if (!empty($request->file('zip'))) {
                try {
                    $zip = new ZipArchive;
                    $name = $request->file('zip');
                    $filename = $name->getClientOriginalName();
                    $path = public_path('pelayanan/'.strval($kategori));
                    $location = $path.$filename;
                    if (move_uploaded_file($request->file('zip'), $location)) {
                        if ($zip->open($location)) {
                            $zip->extractTo($path);
                            $zip->close();
                            File::delete($location);
                        }
                    }
                    $file = scandir($path);
                    // dd($file);
                } catch (\Throwable $th) {
                    throw $th;
                }
            }
        } catch (\Throwable $th) {
            return back()->with('failed', $th->getMessage());
        }
        return back()->with('success', 'Import File Berhasil !');
    }
}
