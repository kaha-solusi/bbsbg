<?php

namespace App\Http\Controllers;

use App\Models\Bimtek;
use App\Models\Instansi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class BimtekController extends Controller
{
    public function create()
    {
        return view('admin.bimtek.create');
    }

    public function store(Request $request)
    {
        //validasi
        $validator = Validator::make($request->all(), [
            'nama_bimtek' => 'required',
            'bulan_pelaksanaan' => 'required',
            'syarat_peserta' => 'required',
            'deskripsi_bimtek' => 'required',
            'waktu_pelaksanaan' => 'required'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $month = explode("-", $request->bulan_pelaksanaan);

        if ($request->tanggal_pelaksanaan === '') {
            $date = explode("-", $request->tanggal_pelaksanaan);
        }

        $data = Bimtek::create(
            array(
                "nama_bimtek"           => $request->nama_bimtek,
                "jenis_bimtek"          => $request->jenis_bimtek,
                "deskripsi_bimtek"      => $request->deskripsi_bimtek,
                "tahun_pelaksanaan"     => $month[0],
                "bulan_pelaksanaan"     => $month[1],
                "tanggal_pelaksanaan"   => $request->tanggal_pelaksanaan,
                "waktu_pelaksanaan"     => $request->waktu_pelaksanaan,
                "status_pelaksanaan"    => "aktif",
                "syarat_peserta"        => $request->syarat_peserta,
                "link_zoom"             => $request->link_zoom,
                "meeting_id"            => $request->meeting_id,
                "passcode"              => $request->passcode,
                "link_certificate"      => $request->link_certificate,
                "bimtek_subjek_email"   => $request->bimtek_subjek_email,
                "bimtek_konten_email"   => $request->bimtek_konten_email
            )
        );

        if ($request->file('bimtek_background')) {
            $name = $request->file('bimtek_background');
            $bg_image = time()."_".$name->getClientOriginalName();
            $request->bimtek_background->move("bimtek/background_images/", $bg_image);

            Bimtek::find($data->id)->update([
                'bimtek_background' => 'bimtek/background_images/'.$bg_image,
            ]);
        }

        return redirect('/admin/pelayanan/3')->with('success', 'Berhasil di Tambah !');
    }

    public function register($id)
    {
        return view('bimtek.register', [
            'bimtek' => Bimtek::findOrFail($id),
            'instansis' => Instansi::all(),
        ]);
    }

    public function edit($id)
    {
        return view('admin.bimtek.edit', [
            'bimtek' => Bimtek::findOrFail($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        //validasi
        $validator = Validator::make($request->all(), [
            'nama_bimtek' => 'required',
            'bulan_pelaksanaan' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $month = explode("-", $request->bulan_pelaksanaan);

        if ($request->tanggal_pelaksanaan === '') {
            $date = explode("-", $request->tanggal_pelaksanaan);
        }

        $data = Bimtek::findOrFail($id);

        if (!is_null($request->file('bimtek_background'))) {
            $name = $request->file('bimtek_background');
            $bg_image = time()."_".$name->getClientOriginalName();
            $request->bimtek_background->move("bimtek/background_images/", $bg_image);
            $request->bimtek_background = 'bimtek/background_images/'.$bg_image;
        }

        $data->update(
            array(
                "nama_bimtek"           => $request->nama_bimtek,
                "jenis_bimtek"          => $request->jenis_bimtek,
                "deskripsi_bimtek"      => $request->deskripsi_bimtek,
                "tahun_pelaksanaan"     => $month[0],
                "bulan_pelaksanaan"     => $month[1],
                "tanggal_pelaksanaan"   => $request->tanggal_pelaksanaan,
                "waktu_pelaksanaan"     => $request->waktu_pelaksanaan,
                "status_pelaksanaan"    => "aktif",
                "syarat_peserta"        => $request->syarat_peserta,
                "link_zoom"             => $request->link_zoom,
                "meeting_id"            => $request->meeting_id,
                "passcode"              => $request->passcode,
                "link_certificate"      => $request->link_certificate,
                "bimtek_subjek_email"   => $request->bimtek_subjek_email,
                "bimtek_konten_email"   => $request->bimtek_konten_email,
                'bimtek_background' => is_null($request->bimtek_background) ? $data->bimtek_background : $request->bimtek_background
            )
        );

        return redirect('/admin/pelayanan/3')->with('success', 'Data Bimbingan Teknis Berhasil Diperbarui.');
    }

    public function destroy($id)
    {
        $bimtek = Bimtek::findOrFail($id);
        $bimtek->delete();
        return back()->with('success', 'Berhasil di Hapus !');
    }
}
