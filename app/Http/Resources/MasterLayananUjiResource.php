<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MasterLayananUjiResource extends JsonResource
{
    public $preserveKeys = true;
    public static $wrap = 'rows';

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * 
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'pelayanan_id' => $this->pelayanan->nama
        ];
    }
}
