<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PegawaiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'foto' => 'nullable|image|mimes:jpg,png,jpeg|max:5048',
            'nama' => 'required|string|max:50',
            'email' => 'required|string|email|unique:users,email',
            'nip' => 'nullable|numeric',
            // 'lab_id' => 'exists:labs,id',
//            'jabatan_id' => 'nullable|exists:jabatans,id',
//            'jabatan_laboratorium_id' => 'required',
//            'golongan_id' => 'nullable|exists:golongans,id',
            'no_hp' => 'nullable|string|regex:/(628)[0-9]{9,}/',
            'role' => 'required|exists:roles,id',
            'password' => 'required|string|min:6',
            'birth_date' => 'nullable|before:today'
        ];

        if ($this->method() === 'POST') {
            $rules['password'] = ['required', 'string', 'min:6'];
        };

        if ($this->method() === 'PUT') {
            $rules['email'] = [
                'required',
                'string',
                'email',
                Rule::unique('users', 'email')->ignore(request('pegawai')),
            ];
            $rules['password'] = ['nullable', 'string', 'min:6'];
        };
        return $rules;
    }
}
