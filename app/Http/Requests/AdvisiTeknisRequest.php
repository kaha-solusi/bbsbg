<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdvisiTeknisRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->status === 'waiting-member') {
            return [
                'link' => 'required|url',
                'users' => 'required|array',
            ];
        } elseif ($this->status === 'upload-document') {
            return [
                'suratPengantar' => 'required',
                'beritaAcara' => 'required'
            ];
        } elseif ($this->status === 'draft') {
            return [
                'users' => 'required',
                'waktu_konfirmasi' => 'required',
                'time' => 'required'
            ];
        };

        return [];
    }
}
