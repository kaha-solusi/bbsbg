<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'logo'=>'nullable|image|mimes:jpg,png,jpeg|max:5048',
            'email' => 'required|string|email|unique:users,email',
            'username' => 'required|string',
            'password' => 'sometimes',
        ];

        if ($this->method() === 'PUT') {
            $rules['email'] = [
                'required',
                'string',
                'email',
                Rule::unique('users', 'email')->ignore(request('user')),
            ];
        }
        return $rules;
    }
}
