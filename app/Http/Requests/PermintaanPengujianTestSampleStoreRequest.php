<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PermintaanPengujianTestSampleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jasa_ekspedisi' => 'required|string',
            'no_resi' => 'required|string',
            'status' => 'required',
            'bukti_pengiriman' => 'required|image',
        ];
    }
}
