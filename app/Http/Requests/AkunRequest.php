<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AkunRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users,email',
            'username' => 'required|string',
            'status' => [
                'required',
                Rule::in(['aktif', 'non-aktif']),
            ]
        ];

        if ($this->method() === 'PUT') {
            $rules['email'] = [
                'required',
                'string',
                'email',
                Rule::unique('users', 'email')->ignore(request('akun')),
            ];
        }
        return $rules;
    }
}
