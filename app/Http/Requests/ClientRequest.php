<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'logo' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'name' => 'required|string|max:25',
            'username' => 'required|string|max:25',
            'alamat' => 'required|string|max:65534',
            'email' => 'required|string|email|unique:users,email',
            'phone_number' => 'required|string|regex:/(628)[0-9]{9,}/',
            'password' => 'required|string|min:8|confirmed'
        ];

        if ($this->method() === 'PUT') {
            $rules['email'] = [
                'required',
                'string',
                'email',
                Rule::unique('users', 'email')->ignore(request('client')),
            ];

            unset($rules['password']);
        };

        return $rules;
    }
}
