<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PeralatanLabStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lab_id' => 'required|exists:labs,id',
            'nama' => 'required|string|max:250',
            'foto_alat' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'merek' => 'required|string|max:250',
            'type' => 'required|string|max:250',
            'seri' => 'required|string|max:250',
            'no_bmn' => 'nullable|string',
            'jumlah' => 'nullable|integer',
            // 'tanggal_terakhir_kalibrasi' => 'nullable|string|max:250',
            // 'tanggal_kalibrasi_ulang' => 'nullable|string|max:250',
            // 'lembaga_pengujian_kalibrasi' => 'nullable|string|max:250',
            // 'hasil_verifikasi_alat' => [
            //     'required',
            //     Rule::in(['1', '0']),
            // ],
            // 'kondisi' => [
            //     'required',
            //     Rule::in([
            //         'baik',
            //         'rusak_ringan',
            //         'rusak_sedang',
            //         'rusak_berat',
            //     ]),
            // ],
            // 'jadwal_pemeliharaan' => 'nullable|string|max:250',
            // 'penanggung_jawab' => 'nullable|string|max:250'
        ];
    }
}
