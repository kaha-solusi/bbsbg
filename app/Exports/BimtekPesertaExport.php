<?php

namespace App\Exports;

use App\Models\Client;
use App\Models\Pegawai;
use App\Models\PesertaBimtek;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class BimtekPesertaExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;

    public function __construct()
    {
        $this->i = 0;
    }

    public function increment()
    {
        return $this->i += 1;
    }

    public function collection()
    {
        return PesertaBimtek::with('user')->whereHas('user.client')->get();
    }

    public function map($peserta) : array
    {
        $perusahaan = $peserta->user->client->nama_perusahaan;

        if ($peserta->user->client->unit_kerja_id != null){
            $perusahaan = $peserta->user->client->unit_kerja->nama_instansi;
        }

        return [
            $this->increment(),
            $peserta->user->client->name,
            $peserta->user->client->nomor,
            $perusahaan,
            $peserta->user->client->phone_number,
            $peserta->user->email,
            $peserta->user->client->alamat
        ];
    }

    public function headings(): array
    {
        return [
            'No.',
            'Nama',
            'NIP/NRP',
            'Nama Perusahaan',
            'No HP',
            'E-mail',
            'Alamat'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER,
            'E' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
