<?php

namespace App\Exports;

use App\Models\UmpanBalik;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class UmpanBalikExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithColumnFormatting
{
	use Exportable;

    public function __construct()
    {
        $this->i = 1;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return UmpanBalik::get();
    }

    public function map($umpan) : array
    {
        return [
            $this->i++,
            $umpan->nama,
            $umpan->client->user->email,
            $umpan->keterangan,
        ];
    }

    public function headings() : array
    {
        return [
            'No.',
            'nama',
            'email',
            'keterangan',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
