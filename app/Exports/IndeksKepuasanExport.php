<?php

namespace App\Exports;

use App\Models\IndeksKepuasan;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class IndeksKepuasanExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithColumnFormatting
{
    use Exportable;

    public function __construct()
    {
        $this->i = 1;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return IndeksKepuasan::all();
    }

    public function map($jadwal) : array
    {
        return [
            $this->i++,
            $jadwal->tahun,
            $jadwal->jumlah_responden,
            $jadwal->nilai_ikm,
            $jadwal->indexs,
            $jadwal->nilai_mutu,
            $jadwal->predikat,
        ];
    }

    public function headings() : array
    {
        return [
            'No.',
            'tahun',
            'jumlah_responden',
            'nilai_ikm',
            'indeks',
            'nilai_mutu',
            'predikat',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
