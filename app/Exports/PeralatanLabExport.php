<?php

namespace App\Exports;

use App\Models\PeralatanLab;
// use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class PeralatanLabExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;

    public function __construct()
    {
        $this->i = 0;
    }

    public function increment()
    {
        return $this->i += 1;
    }

    public function collection()
    {
        return \App\Models\PeralatanLab::all();
    }

    public function map($peralatan) : array
    {
//        dd(!$peralatan->usage->where([['kategori','=', 'Pemeliharaan'],['kategori','=','Pemakaian']])->isEmpty());
        if (!$peralatan->usage->where('kategori', 'Pemeliharaan')->isEmpty()) {
            $pemeliharaan = $peralatan->usage->where('kategori', 'Pemeliharaan')->first();
            $tgl_pemeliharaan = date('d-m-Y', strtotime($pemeliharaan->tanggal_pelaksanaan));
        } else {$tgl_pemeliharaan="";}

        if (!$peralatan->usage->where('kategori', 'Pemakaian')->isEmpty()) {
            $pemakaian = $peralatan->usage->where('kategori', 'Pemakaian')->count();
        } else {$pemakaian="";}

        if (!$peralatan->history->isEmpty()) {
            $tgl_last_kalibrasi = date('d-m-Y', strtotime($peralatan->history->last()->last_calibration));
            $tgl_next_kalibrasi = date('d-m-Y', strtotime($peralatan->history->last()->next_calibration));
            $kondisi = $peralatan->history->last()->condition;
            $akses_pengguna = $peralatan->history->last()->responsible_person;
            $instansi = $peralatan->history->last()->examiner;
        } else {$tgl_last_kalibrasi="";$tgl_next_kalibrasi="";$kondisi="";$akses_pengguna=""; $instansi = "";}
        return [
            $this->increment(),
            $peralatan->lab->nama,
            $peralatan->nama,
            $peralatan->merek,
            $peralatan->type,
            $peralatan->seri,
            $peralatan->no_bmn,
            $peralatan->keterangan_kapasitas,
            $peralatan->akurasi,
            $peralatan->jumlah,
            $tgl_last_kalibrasi,
            $tgl_next_kalibrasi,
            $instansi,
//            $peralatan->hasil_verifikasi_alat == 0 ? 'Tidak Sesuai' : 'Sesuai',
            $kondisi,
            $tgl_pemeliharaan,
            $akses_pengguna,
            $pemakaian
        ];
    }

    public function headings(): array
    {
        return [
            'No.',
            'Nama Lab',
            'Nama Alat',
            'Merek',
            'Tipe',
            'Seri',
            'No. BMN',
            'Kapasitas',
            'Akurasi',
            'Jumlah',
            'Tanggal Terakhir Kalibrasi',
            'Tanggal Kalibrasi Ulang',
            'Lembaga Pengujian Kalibrasi',
//            'hasil_verifikasi_alat',
            'Kondisi',
            'jadwal_pemeliharaan',
            'Akses Pengguna',
            'Jumlah Pemakaian'
        ];
    }

    public function columnFormats(): array
    {
        return [
        //     'F' => NumberFormat::FORMAT_NUMBER,
 	    // 'C' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
