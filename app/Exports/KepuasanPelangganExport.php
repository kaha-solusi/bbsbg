<?php

namespace App\Exports;

use App\Models\KepuasanPelanggan;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class KepuasanPelangganExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithColumnFormatting
{
	use Exportable;

    public function __construct()
    {
        $this->i = 1;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return KepuasanPelanggan::all();
    }

    public function map($jadwal) : array
    {
        return [
            $this->i++,
            $jadwal->waktu,
            $jadwal->nama,
            $jadwal->pelayanan->nama,
            $jadwal->tingkat_kepuasan,
            $jadwal->komen,
        ];
    }

    public function headings() : array
    {
        return [
            'No.',
            'waktu',
            'nama',
            'pelayanan',
            'tingkat_kepuasan',
            'komentari',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
