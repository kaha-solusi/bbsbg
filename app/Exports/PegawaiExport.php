<?php

namespace App\Exports;

use App\Models\Pegawai;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class PegawaiExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;

    public function __construct()
    {
        $this->i = 0;
    }

    public function increment()
    {
        return $this->i += 1;
    }

    public function collection()
    {
        return \App\Models\Pegawai::all();
    }

    public function map($pegawai) : array
    {
        return [
            $this->increment(),
            $pegawai->nama,
            $pegawai->nip,
            $pegawai->lab->id,
            $pegawai->email,
            $pegawai->no_hp,
        ];
    }

    public function headings(): array
    {
        return [
            'no',
            'nama',
            'nip',
            'lab_id',
            'email',
            'no_hp',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'F' => NumberFormat::FORMAT_NUMBER,
 	    'C' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
