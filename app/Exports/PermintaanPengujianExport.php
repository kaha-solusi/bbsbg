<?php

namespace App\Exports;

use App\Models\PermintaanPengujian;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class PermintaanPengujianExport implements FromCollection, WithColumnFormatting, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;
    
    public function __construct()
    {
        $this->i = 0;
    }

    public function increment()
    {
        return $this->i += 1;
    }

    public function collection()
    {
        return PermintaanPengujian::all();
    }

    public function map($data): array
    {
        return [
            $this->increment(),
            $data->nama_pemohon,
            $data->alamat,
            $data->email,
            $data->nama_kontak,
            $data->nomor_hp_kontak,
            $data->tgl_permintaan,
            $data->pelayanan->nama,
            $data->nama_lhu,
            $data->email_persuratan,
            $data->tanggal_pengujian,
            $data->detail_layanan,
        ];
    }
    public function headings(): array
    {
        return [
            'No.',
            'nama_pemohon',
            'alamat',
            'email',
            'nama_kontak',
            'nomor_hp_kontak',
            'tgl_permintaan',
            'jenis_layanan',
            'nama_lhu',
            'email_persuratan',
            'tanggal_pengujian',
            'detail_layanan',
        ];
    }
    public function columnFormats(): array
    {
        return [
            'E' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
