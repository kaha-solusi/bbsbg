<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UserExport implements FromCollection, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;
    public function __construct()
    {
        $this->i = 1;
    }
    public function collection()
    {
        return User::all();
    }
    public function map($user) : array
    {
        return [
            $this->i++,
            $user->name,
            $user->username,
            $user->email,
            $user->level,
            $user->status,
        ];
    }
    public function headings() : array
    {  
        return [
            'No.',
            'name',
            'username',
            'email',
            'level',
            'status',
        ];
    }
}
