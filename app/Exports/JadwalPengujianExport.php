<?php

namespace App\Exports;

use App\Models\Kegiatan;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class JadwalPengujianExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithColumnFormatting
{
	use Exportable;

    public function __construct()
    {
        $this->i = 1;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Kegiatan::where('kategori', 'pengujian')->get();
    }

    public function map($jadwal) : array
    {
        return [
            $this->i++,
            $jadwal->waktu,
            $jadwal->lab->nama,
            $jadwal->nama_kegiatan,
            $jadwal->deskripsi_kegiatan,
            $jadwal->waktu_pelaksanaan,
            $jadwal->tempat_pelaksanaan,
            $jadwal->biaya_kegiatan,
            $jadwal->status,
            $jadwal->kategori,
        ];
    }

    public function headings() : array
    {
        return [
            'No.',
            'waktu',
            'lab_id',
            'nama_kegiatan',
            'deskripsi_kegiatan',
            'waktu_pelaksanaan',
            'tempat_pelaksanaan',
            'biaya_kegiatan',
            'status',
            'kategori',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
