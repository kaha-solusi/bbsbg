<?php

namespace App\Exports;

use App\Models\Pelayanan;
use App\Models\KategoriPelayanan;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use function PHPSTORM_META\map;

class PelayananExport implements FromCollection, WithMapping, WithHeadings
{
    use Exportable;
    public function __construct($kategori)
    {
        $this->kategori = $kategori;
        $this->i = 1;
    }
    public function collection()
    {
        return Pelayanan::where('kategori_id', $this->kategori)->get();
    }
    public function map($pelayanan) : array
    {
        return  [  
            $this->i++,
            $pelayanan->nama,
            $pelayanan->text,
        ];
    }
    public function headings() : array
    {
        return [
            'No.',
            'nama',
            'text',
        ];
    }
}
