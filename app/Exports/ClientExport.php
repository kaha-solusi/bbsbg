<?php

namespace App\Exports;

use App\Models\Client;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ClientExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    public function __construct()
    {
        $this->i = 0;
    }

    public function increment()
    {
        return $this->i += 1;
    }

    public function collection()
    {
        return \App\Models\Client::all();
    }

    public function map($client) : array
    {
        return [
            $this->increment(),
            $client->name,
            $client->user->email,
            $client->phone_number,
            $client->bidang_usaha,
            $client->alamat,
        ];
    }

    public function headings(): array
    {
        return [
            'no',
            'nama',
            'email',
            'phone_number',
            'bidang_usaha',
            'alamat',
        ];
    }
}
