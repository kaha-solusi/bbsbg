<?php

namespace App\Exports;

use App\Models\StrukturOrganisasi;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class StrukturOrganisasiExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithColumnFormatting
{
    use Exportable;
    public function __construct()
    {
        $this->i = 1;
    }
    public function collection()
    {
        return StrukturOrganisasi::all();
    }
    public function map($struktur) : array
    {
        return [
            $this->i++,
            $struktur->nama,
            $struktur->nip,
            $struktur->jabatan,
            $struktur->email,
            $struktur->keterangan,
        ];
    }
    public function headings() : array
    {
        return [
            'No.',
            'nama',
            'nip',
            'jabatan',
            'email',
            'keterangan',
        ];
    }
    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
