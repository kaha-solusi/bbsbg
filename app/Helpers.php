<?php

/**
 * PengujianController
 * php version: 7.4.0
 *
 * @category Helpers
 * @package  Helpers
 * @author   Agung Kurniawan <agungkes95@gmail.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @version  GIT: asdlad123
 * @access   public
 * @link     https://agungkes.github.com
 */

use App\Models\AdvisiTeknis;
use App\Models\PermintaanPengujian;

if (!function_exists('formatCurrency')) {
    /**
     * FormatCurrency function
     *
     * @param float $price
     *
     * @return string
     */
    function formatCurrency($price)
    {
        return 'Rp. ' . number_format($price, 2, ',', '.');
    }
}

if (!function_exists('generateBarcode')) {
    /**
     * GenerateBarcode function
     *
     * @param float $price
     *
     * @return string
     */
    function generateBarcode($content, $width = 3, $height = 100, $color = [0, 0, 0], $showText = true)
    {
        return DNS2D::getBarcodeHTML($content, 'QRCODE', $width, $height);
        // return '<img src="data:image/png;base64,'
        //     .DNS1D::getBarcodePNG(strval($content), 'QRCODE', $width, $height, $color, $showText)
        //     .'" alt="barcode"  />';
    }
}

if (!function_exists('getUserByRole')) {
    function getUserByRole($role, $first = false)
    {
        $user = App\User::whereHas('roles', function ($q) use ($role) {
            $q->where('name', $role);
        })->get();

        if ($first) {
            $user = $user->first();
        }
        return $user;
    }
}

if (!function_exists('getUserByPosition')) {
    function getUserByPosition($position)
    {
        $user = App\User::whereHas('roles', function ($q) {
            $q->where('name', 'subkoor');
        })->get();

        dd($user[0]->pegawai);

        return $user;
    }
}


if (!function_exists('getAdvisiTeknisApprovalStatus')) {
    function getAdvisiTeknisApprovalStatus(AdvisiTeknis $advisiTeknis, $userId, $html = true)
    {
        $advisiTeknis = $advisiTeknis->approvals->where('id', $userId);
        if ($advisiTeknis->isNotEmpty()) {
            if ($advisiTeknis->last()->pivot->status === \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED) {
                return $html ? '<span class="badge badge-success">Menyetujui</span>' : \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED;
            }

            return $html ? '<span class="badge badge-danger">Menolak</span>' : \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED;
        }
        return $html ? '<span class="badge badge-secondary">Menunggu</span>' : \App\Models\AdvisiTeknis::ADVISI_TEKNIS_STATUS_WAITING;
    }
}

if (!function_exists('getNameFromNumber')) {
    function getNameFromNumber($num)
    {
        $numeric = ($num - 1) % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval(($num - 1) / 26);
        if ($num2 > 0) {
            return getNameFromNumber($num2) . $letter;
        } else {
            return $letter;
        }
    }
}
