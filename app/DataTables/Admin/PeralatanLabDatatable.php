<?php

namespace App\DataTables\Admin;

use App\Models\PeralatanLab;
use Carbon\Carbon;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PeralatanLabDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->setRowAttr([
                'class' => function ($query) {
                    if ($query->history->isNotEmpty()) {
                        $to = Carbon::createFromFormat('Y-m-d H:s:i', now());
                        $from = Carbon::createFromFormat('Y-m-d H:s:i', $query->history->last()->next_calibration);

                        $diff_in_days = $to->diffInDays($from);
                        if ($diff_in_days < 30) {
                            return 'danger';
                        }
                    }
                    return '';
                },
            ])
            ->editColumn('nama', function ($query) {
                return '<a
                href="'.route("admin.peralatan.show", $query->id).'"
                role="button">'.$query->nama.'</a>';
            })
            ->editColumn('history.condition', function ($query) {
                return ucwords(preg_replace('/_/', ' ', $query->history->last()->condition ?? '-'));
            })
            ->addColumn('action', function ($query) {
                $action = [
                    'edit' => route("admin.peralatan.edit", $query->id),
                    'delete' => route('admin.peralatan.destroy', $query->id),
                    'detail' => route("admin.peralatan.show", $query->id),
                ];

                return view('admin.utils.table-action', $action)->render();
            })
            ->addColumn('barcode', function ($query) {
                return '<a href="'.route('peralatan.barcode', $query->id).'"
                    data-toggle="modal" data-target="#barcode-modal" class="btn btn-link barcode">'
                .generateBarcode(strtolower($query->id), 2, 2).'</a>';
            })
            ->rawColumns(['action', 'barcode', 'nama']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Admin\PeralatanLab $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PeralatanLab $model)
    {
        return $model->newQuery()
            ->with([
                'history'
            ])
            ->select(
                'peralatan_labs.*'
            );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-peralatanlabdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->orderBy(2)
                    ->drawCallback("function() {
                        // const tableId = $('#admin-peralatanlabdatatable-table');
                        const detailModal = $('#detail-modal');
                        const barcodeModal = $('#barcode-modal');

                        $('a.barcode').on('click', function(e) {
                            e.preventDefault();
                            const url = $(this).attr('href');

                            $.get(url, function(res) {

                                $('#barcode-modal .modal-body').html(res);
                                barcodeModal.modal('show');
                            })
                            barcodeModal.modal('show');
                        });

                        // $('a.detail').on('click', function(e) {
                        //     e.preventDefault();
                        //     const url = $(this).attr('href');
                        //     $.get(url, function(res) {

                        //         $('#detail-modal .modal-body').html(res);
                        //         detailModal.modal('show');
                        //     })
                        // })
                    }")
                    ->addRowNumber('id');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('nama')->title('Nama Alat'),
            Column::make('merek')->title('Merek'),
            Column::make('type')->title('Tipe'),
            Column::make('seri')->title('Seri'),
            Column::make('jumlah')->title('Jumlah'),
            Column::make('history.condition')
                ->title('Kondisi'),
            Column::computed('barcode', 'QR Code')
                ->exportable(false)
                ->printable(false)
                ->width(80)
                ->addClass('text-center'),
            Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->width(180)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PeralatanLab_' . date('YmdHis');
    }
}
