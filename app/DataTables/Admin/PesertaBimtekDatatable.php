<?php

namespace App\DataTables\Admin;

use App\Models\Bimtek;
use App\Models\PesertaBimtek;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PesertaBimtekDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('user.client.group', function ($query) {
                if ($query->user->client->group === "1") {
                    $nama = '';
                    if ($query->user->client->instansi) {
                        if ($query->user->client->instansi->isRoot()) {
                            $nama .= $query->user->client->instansi->nama_instansi;
                        }

                        if ($query->user->client->instansi->hasParent()) {
                            $parent = $query->user->client->instansi->hasParent()->first()->nama_instansi;
                            $nama = $nama.' - '.$parent;
                        }
                    }
                    return $nama;
                } elseif ($query->user->client->group === "2" || $query->user->client->group === "3") {
                    return $query->user->client->nama_perusahaan;
                };

                return '-';
            })
            ->addColumn('action', 'admin-pesertabimtekdatatable.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Bimtek $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PesertaBimtek $model)
    {
        return $model->newQuery()
            ->whereBimtekId(request('bimtek'))
            ->whereHas('user.client')
            ->with(['user', 'user.client', 'user.client.instansi']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-pesertabimtekdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->orderBy(2, 'asc')
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('user.name')
                ->title('Nama'),
            Column::make('user.client.nomor')
                ->title('NIP/NIK'),
            Column::make('user.client.group')
                ->title('Instansi'),
             Column::make('user.client.phone_number')
                 ->title('No. HP'),
             Column::make('user.email')
                 ->title('Email'),
             Column::make('user.client.alamat')
                 ->title('Alamat')
                 ->addClass('center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin\PesertaBimtek_' . date('YmdHis');
    }
}
