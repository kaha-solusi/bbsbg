<?php

namespace App\DataTables\Admin;

use App\Models\MasterLayananUji;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class MasterLayananDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($query) {
                $action = [
                    'delete' => route('master-pelayanan-uji.destroy', $query->id),
                    'edit' => route('master-pelayanan-uji.edit', $query->id),
                ];

                return view('admin.utils.table-action', $action)->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\MasterLayananUji $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MasterLayananUji $model)
    {
        return $model->newQuery()
            ->select('master_layanan_ujis.id', 'name', 'pelayanans.nama as jenis', 'master_layanan_ujis.created_at')
            ->join('pelayanans', 'pelayanans.id', 'pelayanan_id');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-masterlayanandatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->orderBy(4)
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name')
                ->title('Nama Layanan'),
            Column::make('jenis')
                ->title('Jenis Layanan'),
            Column::make('created_at')
                ->title('')
                ->hidden(),
            Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin\MasterLayanan_' . date('YmdHis');
    }
}
