<?php

namespace App\DataTables\Admin;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PegawaiDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('level', function ($query) {
                return ucwords($query->roles->first()->name);
            })
            ->editColumn('pegawai.lab.nama', function ($query) {
                if (is_null($query->pegawai)) {
                    return null;
                }

                if (!is_null($query->pegawai->lab)) {
                    return $query->pegawai->lab->nama;
                }

                return '-';
            })
            ->editColumn('pegawai.jabatan.nama', function ($query) {
                if (is_null($query->pegawai)) {
                    return null;
                }

                if (!is_null($query->pegawai->jabatan)) {
                    return $query->pegawai->jabatan->nama;
                }

                return '-';
            })
//            ->editColumn('pegawai.jabatan_laboratorium.name', function ($query) {
//                if (is_null($query->pegawai)) {
//                    return null;
//                }
//
//                if (!is_null($query->pegawai->jabatan_laboratorium)) {
//                    return $query->pegawai->jabatan_laboratorium->name;
//                }
//
//                return '-';
//            })
            ->editColumn('pegawai.golongan.nama', function ($query) {
                if (is_null($query->pegawai)) {
                    return null;
                }

                if (!is_null($query->pegawai->golongan)) {
                    return $query->pegawai->golongan->nama;
                }

                return '-';
            })
            ->editColumn('pegawai.no_hp', function ($query) {
                if (is_null($query->pegawai)) {
                    return null;
                }

                return $query->pegawai->no_hp;
            })
            ->editColumn('pegawai.foto', function ($query) {
                if (is_null($query->pegawai)) {
                    return null;
                }
                return '<img
                    width="50"
                    class="img-fluid"
                    src="/uploads/foto/pegawai/'.$query->pegawai->foto.'" />';
            })
            ->addColumn('action', function ($query) {
                $action = [
                    'delete' => route('admin.pegawai.destroy', $query->id),
                    'edit' => route('admin.pegawai.edit', $query->id),
                ];

                return view('admin.utils.table-action', $action)->render();
            })
            ->rawColumns(['action', 'pegawai.foto']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery()
            ->with([
                'pegawai' => function ($query) {
                        $query->select('id', 'foto', 'no_hp', 'lab_id', 'user_id', 'jabatan_id', 'golongan_id');
                },
                'pegawai.lab' => function ($query) {
                        $query->select('id', 'nama');
                },
                'pegawai.jabatan' => function ($query) {
                    $query->select('id', 'nama');
                },
                'pegawai.golongan' => function ($query) {
                    $query->select('id', 'nama');
                },
//                'pegawai.jabatan_laboratorium' => function ($query) {
//                    $query->select('id', 'name');
//                },
            ])
            ->with([
                'roles' => function ($query) {
                        $query->select('id', 'name');
                },
            ])
            ->whereHas('roles', function (Builder $query) {
                $query->where('name', '!=', 'super-admin')
                    ->where('name', '!=', 'pelanggan');
            })
            ->select('users.*');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-pegawai-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->autoFillHorizontal(true)
                    ->autoWidth(true)
                    ->orderBy(9, 'desc')
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('pegawai.foto', 'Foto')
                ->addClass('text-center'),
            Column::make('name')
                ->title('Nama'),
            Column::make('pegawai.lab.nama')
                ->title('Laboratorium'),
            Column::make('pegawai.jabatan.nama')
                ->title('Jabatan'),
            Column::make('pegawai.golongan.nama')
                ->title('Golongan'),
            Column::make('email')
                ->title('Email'),
            Column::make('pegawai.no_hp')
                ->title('Nomor Telepon')
                ->addClass('center'),
            Column::make('created_at', '')->hidden(),
            Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Pegawai_' . date('YmdHis');
    }
}
