<?php

namespace App\DataTables\Admin;

use App\Models\Jabatan;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class JabatanDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($query) {
                $action = [
                    'delete' => route('admin.pegawai.jabatan.destroy', $query->id),
                    'edit' => route('admin.pegawai.jabatan.edit', $query->id),
                ];

                return view('admin.utils.table-action', $action)->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Admin\Jabatan $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Jabatan $model)
    {
        return $model->newQuery()
            ->select('id', 'nama', 'created_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('jabatandatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->orderBy(3)
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('nama'),
            Column::make('created_at')->hidden(),
            Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->width(150)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Jabatan_' . date('YmdHis');
    }
}
