<?php

namespace App\DataTables\Admin;

use App\Models\Instansi;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class InstansiDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($query) {
                $action = [
                    'delete' => route('admin.instansi.destroy', $query->id),
                    'edit' => route('admin.instansi.edit', $query->id),
                    'unit_kerja' => route('admin.instansi.unit-kerja.index', $query->id),
                ];

                if (request('instansi')) {
                    $action['delete'] = route('admin.instansi.unit-kerja.destroy', [$query->parent_id, $query->id]);
                    $action['edit'] = route('admin.instansi.unit-kerja.edit', [$query->parent_id, $query->id]);
                    unset($action['unit_kerja']);
                }
                return view('admin.utils.table-action', $action)->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Admin\Instansi $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Instansi $model)
    {
        return $model->newQuery()
            ->select([
                'id',
                'nama_instansi',
                'parent_id',
            ])
            ->when(request('instansi'), function ($query, Instansi $instansi) {
                $query->where('parent_id', $instansi->id);
            })
            ->when(!request('instansi'), function ($query) {
                $query->whereNull('parent_id');
            });
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-instansidatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->orderBy(2)
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $title = request('instansi') ? 'Nama Unit Kerja' : 'Nama Unit Organisasi';
        $actionWidth = request('instansi') ? 120 : 220;
        return [
            Column::make('nama_instansi')
                ->title($title),
            Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->width($actionWidth)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Instansi_' . date('YmdHis');
    }
}
