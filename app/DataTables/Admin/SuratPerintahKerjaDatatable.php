<?php

namespace App\DataTables\Admin;

use App\Models\PermintaanPengujian;
use App\Models\PermintaanPengujianSpk;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class SuratPerintahKerjaDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('status', function ($query) {
                if ($query->status == PermintaanPengujian::STATUS_DRAFT) {
                    return '<span class="badge badge-warning">Belum Di Proses</span>';
                }

                if ($query->status == PermintaanPengujian::STATUS_REJECTED) {
                    return '<span class="badge badge-danger">Ditolak</span>';
                }

                if ($query->status == PermintaanPengujian::STATUS_APPROVED_BY_SUBKOOR) {
                    return '<span class="badge badge-success">Disetujui Subkoor</span>';
                }

                if ($query->status == PermintaanPengujian::STATUS_APPROVED_BY_KABALAI) {
                    return '<span class="badge badge-success">Disetujui Kepala Balai</span>';
                }

                return $query->status;
            })
            ->editColumn('permintaan_pengujian.order_id', function ($query) {
                $action = [
                    'route' => $query->status === PermintaanPengujian::STATUS_DRAFT
                        ? route('admin.permintaan-pengujian.surat-perintah-kerja.edit', $query->id)
                        : route('admin.permintaan-pengujian.surat-perintah-kerja.show', $query->id),
                    'name' => $query->permintaan_pengujian->order_id,
                ];

                return view('admin.utils.link', $action)->render();
            })
            ->rawColumns(['permintaan_pengujian.order_id', 'status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PermintaanPengujianSpk $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PermintaanPengujianSpk $model)
    {
        return $model->newQuery()
            ->with([
                'permintaan_pengujian.master_layanan_uji' => function ($query) {
                    $query->select('id', 'name', 'pelayanan_id');
                },
                'permintaan_pengujian.master_layanan_uji.pelayanan' => function ($query) {
                    $query->select('id', 'nama');
                },
            ])
            ->when(auth()->user()->hasRole('kepalai-balai'), function ($query) {
                return $query->where('permintaan_pengujian_spks.status', PermintaanPengujian::STATUS_APPROVED_BY_KABALAI);
            })
            ->when(auth()->user()->hasRole('subkoor'), function ($query) {
                return $query->where('permintaan_pengujian_spks.status', '!=', PermintaanPengujian::STATUS_DRAFT);
            })
            ->select(
                'permintaan_pengujian_spks.id',
                'permintaan_pengujian_spks.status',
                'permintaan_pengujian_spks.nomor',
                'permintaan_pengujian_spks.permintaan_pengujian_id'
            );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-suratperintahkerjadatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->orderBy(2)
                    ->columnDefs([
                        [
                            "targets" => 0,
                            "checkboxes" => [
                                "selectRow" => true,
                            ],
                        ],
                    ])
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('permintaan_pengujian.order_id')
                ->title('ID Pesanan')
                ->addClass('text-center'),
            Column::make('nomor')
                ->title('Nomor')
                ->addClass('text-center'),
            Column::make('permintaan_pengujian.master_layanan_uji.pelayanan.nama')
                ->title('Nama Laboratorium')
                ->addClass('text-center'),
            Column::make('permintaan_pengujian.master_layanan_uji.name')
                ->title('Jenis Pengujian')
                ->addClass('text-center'),
            Column::make('permintaan_pengujian.created_at')
                ->title('Tanggal Permintaan')
                ->addClass('text-center'),
            Column::make('status')
                ->title('Status')
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin\SuratPerintahKerja_' . date('YmdHis');
    }
}
