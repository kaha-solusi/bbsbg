<?php

namespace App\DataTables\Admin;

use App\Models\LaporanHasilUji;
use App\Models\PermintaanPengujian;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class LaporanHasilUjiDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('status', function ($query) {
                if ($query->status == PermintaanPengujian::STATUS_DRAFT) {
                    return '<span class="badge badge-warning">Belum diproses</span>';
                }
                if ($query->status == PermintaanPengujian::STATUS_SUBMITTED) {
                    return '<span class="badge badge-info">Diajukan</span>';
                }
                if ($query->status == PermintaanPengujian::STATUS_COMPLETED) {
                    return '<span class="badge badge-success">Selesai</span>';
                }

                return $query->status;
            })
            ->editColumn('nomor', function ($query) {
                $action = [
                    'route' => $query->status === PermintaanPengujian::STATUS_DRAFT
                        ? route('admin.permintaan-pengujian.laporan-hasil-uji.show', $query->id)
                        : route('admin.permintaan-pengujian.laporan-hasil-uji.show', $query->id),
                    'name' => $query->nomor,
                ];

                return view('admin.utils.link', $action)->render();
            })
            ->rawColumns(['nomor', 'status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\LaporanHasilUji $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(LaporanHasilUji $model)
    {
        return $model->newQuery()
            ->with([
                'permintaan_pengujian' => function ($query) {
                    $query->select('id', 'order_id', 'master_layanan_uji_id');
                },
                'permintaan_pengujian.spk' => function ($query) {
                    $query->select('id', 'nomor', 'permintaan_pengujian_id');
                },
                'permintaan_pengujian.master_layanan_uji' => function ($query) {
                    $query->select('id', 'name', 'pelayanan_id');
                },
                'permintaan_pengujian.master_layanan_uji.pelayanan' => function ($query) {
                    $query->select('id', 'nama');
                },
            ])
            ->select(
                'laporan_hasil_ujis.id',
                'laporan_hasil_ujis.nomor',
                'laporan_hasil_ujis.pengujian_id',
                'laporan_hasil_ujis.status'
            );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-laporanhasilujidatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->orderBy(2)
                    ->columnDefs([
                        [
                            "targets" => 0,
                            "checkboxes" => [
                                "selectRow" => true,
                            ],
                        ],
                    ])
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('nomor')
                ->title('Nomor Laporan')
                ->addClass('text-center'),
            Column::make('permintaan_pengujian.order_id')
                ->title('Nomor Pesanan')
                ->addClass('text-center'),
            Column::make('permintaan_pengujian.master_layanan_uji.pelayanan.nama')
                ->title('Nama Laboratorium')
                ->addClass('text-center'),
            Column::make('permintaan_pengujian.master_layanan_uji.name')
                ->title('Jenis Pengujian')
                ->addClass('text-center'),
            Column::make('status')
                ->title('Status')
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin\LaporanHasilUji_' . date('YmdHis');
    }
}
