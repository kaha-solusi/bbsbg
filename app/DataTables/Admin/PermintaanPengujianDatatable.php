<?php

namespace App\DataTables\Admin;

use App\Models\PermintaanPengujian;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PermintaanPengujianDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', '{{ date("Y-m-d", strtotime($created_at)) }}')
            ->editColumn('updated_at', '{{ date("Y-m-d H:m:s", strtotime($created_at)) }}')
            ->editColumn('status', function ($query) {
                if ($query->status === PermintaanPengujian::STATUS_DRAFT) {
                    return '<span class="badge badge-warning">Pesanan Baru</span>';
                }
                if ($query->status === PermintaanPengujian::STATUS_KAJI_ULANG_SUBMITTED) {
                    return '<span class="badge badge-info">Menunggu Persetujuan</span>';
                }
                if ($query->status === PermintaanPengujian::STATUS_KAJI_ULANG_REJECTED) {
                    return '<span class="badge badge-danger">Kaji Ulang Ditolak</span>';
                }
                if ($query->status === PermintaanPengujian::STATUS_KAJI_ULANG_APPROVED) {
                    return '<span class="badge badge-success">Kaji Ulang Disetujui</span>';
                }
                if ($query->status === PermintaanPengujian::STATUS_KAJI_ULANG_CHECKED_BY_SUBKOOR) {
                    return '<span class="badge badge-info">Kaji Ulang Diperiksa Kepala Balai</span>';
                }
                if ($query->status === PermintaanPengujian::STATUS_KAJI_ULANG_APPROVED_BY_KABALAI) {
                    return '<span class="badge badge-success">Kaji Ulang Disetujui Kepala Balai</span>';
                }
                if ($query->status === PermintaanPengujian::STATUS_FORM_CONFIRMATION_SUBMITTED) {
                    return '<span class="badge badge-info">Menunggu Persetujuan Pelanggan</span>';
                }
                if ($query->status === PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_SUBMITTED) {
                    return '<span class="badge badge-info">Pelanggan Telah Menyetujui</span>';
                }
                if ($query->status === PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_APPROVED) {
                    return '<span class="badge badge-info">Form Konfirmasi Telah Disetujui</span>';
                }
                // if ($query->status === PermintaanPengujian::STATUS_WAITING_INVOICE) {
                //     return '<a href="'.route('admin.permintaan-pengujian.show', $query->id).'"
                //             class="btn btn-sm btn-warning btn-block">Menunggu Kode Billing</a>';
                // }
                // if ($query->status === PermintaanPengujian::STATUS_WAITING_PAYMENT) {
                //     return '<a href="#" class="btn btn-sm btn-info btn-block">Menunggu Pembayaran</a>';
                // }
                // if ($query->status === PermintaanPengujian::STATUS_WAITING_PAYMENT_CONFIRMED) {
                //     return '<a href="'.route('admin.permintaan-pengujian.payment-confirmation', $query->id).'"
                //             class="btn btn-sm btn-info btn-block">Telah Dibayar</a>';
                // }
                // if ($query->status === PermintaanPengujian::STATUS_PAYMENT_APPROVED) {
                //     return '<a href="#"
                //             class="btn btn-sm btn-success btn-block">Pembayaran Diterima</a>';
                // }

                // if ($query->status === PermintaanPengujian::STATUS_SAMPLE_SUBMITTED) {
                //     return '<a href="'.route('admin.permintaan-pengujian.show', $query->id).'"
                //             class="btn btn-sm btn-info btn-block">Benda Uji Dikirim</a>';
                // }

                // if ($query->status === PermintaanPengujian::STATUS_SAMPLE_RECEIVED) {
                //     return '<a href="'.route('admin.permintaan-pengujian.show', $query->id).'"
                //             class="btn btn-sm btn-info btn-block">Benda Uji Diterima</a>';
                // }

                // if ($query->status === PermintaanPengujian::STATUS_SAMPLE_REJECTED) {
                //     return '<a href="'.route('admin.permintaan-pengujian.penerimaan-benda-uji.index', $query->id).'"
                //             class="btn btn-sm btn-danger btn-block">Benda Uji Ditolak</a>';
                // }

                // if ($query->status === PermintaanPengujian::STATUS_SPK_SUBMITTED) {
                //     return '<a href="'.route('admin.permintaan-pengujian.show', $query->id).'"
                //             class="btn btn-sm btn-info btn-block">SPK Telah Dibuat</a>';
                // }

                // if ($query->status === PermintaanPengujian::STATUS_PENGUJIAN_SUBMITTED) {
                //     return '<a href="'.route('admin.permintaan-pengujian.show', $query->id).'"
                //             class="btn btn-sm btn-info btn-block">Penerbitan LHU</a>';
                // }

                return $query->status;
            })
            ->editColumn('order_id', function ($query) {
                $action = [
                    'route' => $query->status === PermintaanPengujian::STATUS_DRAFT ?
                        route('admin.permintaan-pengujian.edit', $query->id) : route('admin.permintaan-pengujian.show', $query->id),
                    'name' => $query->order_id,
                ];

                return view('admin.utils.link', $action)->render();
            })
            ->addColumn('action', function ($query) {
                return view('admin.permintaan_pengujian.table-action.history', ['order_id' => $query->id])->render();
            })
            ->rawColumns(['action', 'status', 'order_id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Admin\PermintaanPengujian $model Model
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PermintaanPengujian $model)
    {
        return $model->newQuery()
            ->with([
                'client' => function ($query) {
                    $query->select('id', 'name');
                },
                'master_layanan_uji' => function ($query) {
                    $query->select('id', 'name', 'pelayanan_id');
                },
                'master_layanan_uji.pelayanan' => function ($query) {
                    $query->select('id', 'nama');
                },
            ])
            ->select(
                'permintaan_pengujians.id',
                'permintaan_pengujians.order_id',
                'permintaan_pengujians.client_id',
                'permintaan_pengujians.master_layanan_uji_id',
                'tgl_permintaan',
                'permintaan_pengujians.created_at',
                'status'
            );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('admin-permintaanpengujiandatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
            <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
            <"mr-2"f>>>t
            <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
            <"bg-transparent"r>')
            ->orderBy(2)
            ->columnDefs([
                [
                    "targets" => 0,
                    "checkboxes" => [
                        "selectRow" => true,
                    ],
                ],
            ])
            ->addRowNumber('id', [
                'checkbox' => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('order_id')->title('ID Pesanan')
                ->addClass('text-center'),
            Column::make('client.name')->title('Nama Pemohon')
                ->addClass('text-center'),
            Column::make('master_layanan_uji.pelayanan.nama')->title('Nama Laboratorium')
                ->addClass('text-center'),
            Column::make('master_layanan_uji.name')->title('Jenis Pengujian')
                ->addClass('text-center'),
            Column::make('tgl_permintaan')->title('Tanggal Permintaan')
                ->addClass('text-center'),
            Column::make('status')
                ->title('Status')
                ->addClass('text-center'),
            Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PermintaanPengujian_' . date('YmdHis');
    }
}
