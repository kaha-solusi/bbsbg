<?php

namespace App\DataTables\Admin;

use App\Models\AdvisiTeknis;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class AdvisiTeknisDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('test_date', '{{ date("Y-m-d H:m:s", strtotime($test_date)) }}')
            ->editColumn('created_at', '{{ date("Y-m-d H:m:s", strtotime($created_at)) }}')
            ->addColumn('status', function ($query) {
                if ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT) {
                    return '<a 
                        href="'.route('admin.advisi-teknis.show', $query->id).'"
                        class="btn btn-sm btn-primary btn-block">Petugasan Personil</a>';
                } elseif ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_UPLOAD_DOCUMENT) {
                    return '<a 
                        href="'.route('admin.advisi-teknis.show', $query->id).'"
                        class="btn btn-sm btn-info btn-block">Unggah Dokumen</a>';
                } elseif ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KETUA_APPROVAL) {
                    return '<a 
                        href="'.route('admin.advisi-teknis.show', $query->id).'" 
                        class="btn btn-sm btn-secondary btn-block">Menunggu Persetujuan Ketua</a>';
                } elseif ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_SUBKOOR_APPROVAL) {
                    return '<a 
                        href="'.route('admin.advisi-teknis.show', $query->id).'" 
                        class="btn btn-sm btn-secondary btn-block">Menunggu Persetujuan Subkoor</a>';
                } elseif ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL) {
                    return '<a 
                        href="'.route('admin.advisi-teknis.show', $query->id).'" 
                        class="btn btn-sm btn-secondary btn-block">Menunggu Persetujuan Kepala Balai</a>';
                } elseif ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED) {
                    return '<a 
                        href="'.route('admin.advisi-teknis.show', $query->id).'" 
                        class="btn btn-sm btn-success btn-block">Disetujui</a>';
                } elseif ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE) {
                    return '<a 
                        href="'.route('admin.advisi-teknis.show', $query->id).'" 
                        class="btn btn-sm btn-warning btn-block">Menunggu Surat Pengantar</a>';
                } elseif ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED) {
                    return '<a href="'.route('admin.advisi-teknis.show', $query->id).'" 
                        class="btn btn-sm btn-danger btn-block">Ditolak</a>';
                } else {
                    return '<a href="'.route('admin.advisi-teknis.show', $query->id).'"  
                        class="btn btn-sm btn-success btn-block text-white" disabled>Selesai</a>';
                }
            })
            ->rawColumns(['status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Admin\AdvisiTeknisDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(AdvisiTeknis $model)
    {
        return $model->newQuery()
        ->whereHas('client')
        ->with([
            'client' => function ($query) {
                $query->select('id', 'name', 'user_id');
            },
            'client.user' => function ($query) {
                $query->select('id', 'name');
            },
        ])->select('advisi_teknis.*');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-advisiteknis-datatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->orderBy(6, 'desc')
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns = [
            Column::make('client.user.name')
                ->title('Nama Pendaftar'),
            Column::make('test_date')
                ->title('Tanggal Pelaksanaan'),
            Column::make('scope')
                ->title('Lingkup Konsultasi'),
            Column::make('status', 'Status')
                ->addClass('text-center'),
            Column::make('created_at')
                ->title('Tanggal Dibuat')
                ->hidden(true),
        ];
        
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'AdvisiTeknis_' . date('YmdHis');
    }
}
