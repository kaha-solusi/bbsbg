<?php

namespace App\DataTables\Admin;

use App\Models\JadwalKegiatanPeronil;
use App\Models\JadwalKegiatanPersonil;
use App\Models\Pegawai;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class JadwalKegiatanPeronilDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $date = Carbon::today()->format('Y-m-d');
        return datatables()
            ->eloquent($query)
            ->addColumn('status', function ($query) use ($date) {
                if ($query->tanggal_selesai < $date) {
                    return '<a
                    class="btn btn-sm btn-primary btn-block disabled">Selesai</a>';
                } elseif ($query->tanggal_mulai > $date) {
                    return '<a
                    class="btn btn-sm btn-kuning btn-block disabled">Terjadwal</a>';
                } else {
                    return '<a
                    class="btn btn-sm btn-hijau btn-block disabled">Berlangsung</a>';
                }
            })
            ->addColumn('action', function ($query) {
                $action = [
                    'delete' => route('admin.pegawai.jadwal-kegiatan-personil.destroy', $query->id),
                    'edit' => route("admin.pegawai.jadwal-kegiatan-personil.edit", $query->id),
                ];

                return view('admin.utils.table-action', $action)->render();
            })
            ->rawColumns(['status','action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\JadwalKegiatanPeronil $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(JadwalKegiatanPersonil $model)
    {
        if (auth()->user()->level == '1' || auth()->user()->level == '2' || auth()->user()->level == '0') {
            return $model->newQuery()
                ->with(['pegawai' => function ($query) {
                    $query->select('id', 'user_id');
                }, 'pegawai.user' => function ($query) {
                    $query->select('id', 'name');
                },])
                ->select('jadwal_kegiatan_personils.*');
        } else {
            $user = auth()->user()->id;
            return $model->newQuery()
                ->with(['pegawai' => function ($query) {
                    $query->select('id', 'user_id');
                    },
                ])
                ->whereHas('pegawai', function (Builder $query) use ($user) {
                    $query->where('user_id','=', $user);
                })
            ->select('jadwal_kegiatan_personils.*');
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-jadwalkegiatanperonildatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
        <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
        <"mr-2"f>>>t
        <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
        <"bg-transparent"r>')
                    ->orderBy(3, 'desc')
                    ->addRowNumber('id', [
                            'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $col = [
            Column::make('tanggal_mulai')
            ->title('Tanggal Mulai'),
            Column::make('tanggal_selesai')
                ->title('Tangal Selesai'),
            Column::make('penugasan')
                ->title('Penugasan'),
            Column::computed('status', 'Status')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center')
        ];
        if (auth()->user()->hasRole(['super-admin', 'admin','direktur'])) {
            array_unshift($col,Column::make('pegawai.user.name')
                ->title('Nama Personil'));
            $col[]=Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->width(120)
                ->addClass('text-center');
        }
        return $col;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin/JadwalKegiatanPeronil_' . date('YmdHis');
    }
}
