<?php

namespace App\DataTables\Admin;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class AkunDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('status', '<span class="badge badge-success">{{ ucfirst($status) }}</span>')
            ->addColumn('action', function ($query) {
                $action = [
                    'delete' => route('admin.akun.destroy', $query->id),
                    'edit' => route('admin.akun.edit', $query->id),
                ];

                return view('admin.utils.table-action', $action)->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Admin\AkunDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery()
            ->with('roles')
            ->whereHas('roles', function (Builder $query) {
                $query->where('name', '=', 'pelanggan');
            })
            ->select('id', 'name', 'username', 'email', 'status');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-akundatatable-table')
                    ->columns($this->getColumns())
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->columnDefs([
                        [
                            "targets" => 0,
                            "checkboxes" => [
                                "selectRow" => true,
                            ],
                        ],
                    ])
                    ->minifiedAjax()
                    ->orderBy(2)
                    // ->buttons(
                    //     Button::make('create'),
                    // )
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name')
                ->title('Nama'),
            Column::make('username')
                ->title('Username'),
            Column::make('email')
                ->title('Email'),
            Column::make('status')
                ->title('Status')
                ->addClass('center'),
            Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Akun_' . date('YmdHis');
    }
}
