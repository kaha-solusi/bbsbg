<?php

namespace App\DataTables\Admin;

use App\Models\Client;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ClientDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('group', function ($query) {
                if ($query->client->group === "1") {
                    $nama = '';
                    if ($query->client->instansi) {
                        if ($query->client->instansi->isRoot()) {
                            $nama .= $query->client->instansi->nama_instansi;
                        }

                        if ($query->client->instansi->hasParent()) {
                            $parent = $query->client->instansi->hasParent()->first()->nama_instansi;
                            $nama = $nama.' - '.$parent;
                        }
                    }
                    return $nama;
                } elseif ($query->client->group === "2" || $query->client->group === "3") {
                    return $query->client->nama_perusahaan;
                };

                return '-';
            })
            ->addColumn('action', function ($query) {
                $action = [
                    'delete' => route('admin.client.destroy', $query->id),
                    'edit' => route('admin.client.edit', $query->id),
                ];

                return view('admin.utils.table-action', $action)->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Client $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery()
            ->with('client')
            ->whereHas('client')
            ->whereHas('roles', function (Builder $query) {
                $query->where('name', 'pelanggan');
            })
            ->select('users.*');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-clientdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->orderBy(2)
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name')
                ->title('Nama'),
            Column::make('email')
                ->title('Email'),
            Column::make('client.phone_number')
                ->title('Nomor Telepon')
                ->addClass('center'),
            Column::make('client.alamat')
                ->title('Alamat')
                ->addClass('center'),
            Column::make('client.group')
                ->data('group')
                ->title('Instansi')
                ->addClass('center'),
            Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin\Client_' . date('YmdHis');
    }
}
