<?php

namespace App\DataTables\Admin;

use App\Models\PeralatanLab;
use App\Models\PeralatanLabUsage;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PeralatanUsageDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('peralatan.nama', function ($query) {
                return '<a href="'.route('admin.peralatan.show', $query->peralatan->id).'">'.$query->peralatan->nama.'</a>';
            })
            ->editColumn('created_at', function ($query) {
                return date('d-m-Y', strtotime($query->created_at));
            })
            ->rawColumns(['action', 'peralatan.nama']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PeralatanLabUsage $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PeralatanLabUsage $model)
    {
        return $model->newQuery()
            ->with('peralatan')
            ->select('peralatan_lab_usages.*');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-peralatanusagedatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->orderBy(7)
                    ->initComplete("function() {
                        const table = window['" . config('datatables-html.namespace', 'LaravelDataTables') . "']['admin-peralatanusagedatatable-table'];

                        $('.filter-year').on('change', function(e){
                            const selectedYear = e.target.value;
                            if (selectedYear) {
                                table.columns(7).search(selectedYear).draw();
                            } else {
                                table.columns(7).search('').draw();
                            }
                        })
                    }");
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('peralatan.nama')->title('Nama Alat'),
            Column::make('peralatan.merek')->title('Merek'),
            Column::make('peralatan.type')->title('Tipe'),
            Column::make('peralatan.seri')->title('Seri'),
            Column::make('kategori')->title('Kategori'),
            Column::make('keterangan')->title('Keterangan'),
            Column::make('pelaksana')->title('Pelaksana'),
            Column::make('created_at')->title('Tanggal'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin\PeralatanUsage_' . date('YmdHis');
    }
}
