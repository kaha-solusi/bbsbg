<?php

namespace App\DataTables\Admin;

use App\Models\PermintaanPengujianHistory;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PermintaanPengujianHistoryDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', '{{ date("Y-m-d h:m:s", strtotime($created_at)) }}')
            ->editColumn('status', function ($query) {
                if ($query->status === PermintaanPengujianHistory::APPROVED) {
                    return '<span class="badge badge-success">Disetujui</span>';
                }

                return '<span class="badge badge-danger">Ditolak</span>';
            })
            ->editColumn('pic.name', function ($query) {
                if (is_null($query->pic)) {
                    return 'User dihapus';
                }
                return $query->pic->name;
            })
            ->addColumn('action', function ($query) {
                $action = [
                    'delete' => route('admin.permintaan-pengujian.histories.destroy', [$query->permintaan_pengujian_id, $query->id]),
                    'edit' => route('admin.permintaan-pengujian.histories.edit', [$query->permintaan_pengujian_id, $query->id]),
                ];
                
                return view('admin.utils.table-action', $action)->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PermintaanPengujianHistoryDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PermintaanPengujianHistory $model)
    {
        return $model->newQuery()
                ->when(request('permintaan_pengujian'), function ($query, $permintaanPengujian) {
                    return $query->whereHas('order', function ($query) use ($permintaanPengujian) {
                        return $query->where('permintaan_pengujian_id', $permintaanPengujian->id);
                    });
                })
                ->with('pic');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-permintaanpengujianhistorydatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->orderBy(1, 'asc');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns = [
            Column::make('message')
                ->orderable(false),
            Column::make('created_at')
                ->title('Tanggal')
                ->width(130),
            Column::make('pic.name')
                ->title('PIC')
                ->orderable(false)
                ->addClass('text-center')
                ->width(70),
            Column::make('status')
                ->orderable(false)
                ->addClass('text-center')
                ->width(70),
        ];

        if (auth()->user()->hasRole('super-admin')) {
            array_push(
                $columns,
                Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center')
            );
        };
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PermintaanPengujianHistory_' . date('YmdHis');
    }
}
