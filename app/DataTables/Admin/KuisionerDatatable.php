<?php

namespace App\DataTables\Admin;

use App\Models\Kuisioner;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class KuisionerDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($query) {
                $action = [
                    'delete' => route('admin.kuisioners.destroy', $query->id),
                    'edit' => route('admin.kuisioners.edit', $query->id),
                ];

                return view('admin.utils.table-action', $action)->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Admin\Kuisioner $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Kuisioner $model)
    {
        return $model->newQuery()
            ->with('kategori_pelayanan')
            ->select('kuisioners.*');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-kuisionerdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->orderBy(4, 'asc')
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
        ;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('pernyataan')->title('Pernyataan'),
            Column::make('keterangan')->title('Keterangan'),
            Column::make('order')->title('Urutan'),
            Column::make('kategori_pelayanan.nama')->title('Kategori'),
            Column::make('created_at')->hidden(),
            Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->width(150)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin\Kuisioner_' . date('YmdHis');
    }
}
