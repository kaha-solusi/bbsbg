<?php

namespace App\DataTables\Client;

use App\Models\AdvisiTeknis;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class AdvisiTeknisDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('test_date', function ($query) {
                if ($query->status !== AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT &&
                    $query->status !== AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED &&
                    $query->status !== AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL) {
                    $date = date("d/m/Y", strtotime($query->confirmation_date)) . " " . $query->time;
                    return $date;
                } else {
                    $date = date("d/m/Y", strtotime($query->test_date));
                    return $date;
                }
            })
            ->editColumn('created_at', '{{ date("d/m/Y", strtotime($created_at)) }}')
            ->addColumn('status', function ($query) {
                if ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_DRAFT
                || $query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_KEPALA_BALAI_APPROVAL) {
                    return '<span class="badge badge-info">Menunggu Keputusan</span>';
                } elseif ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED) {
                    return '<span class="badge badge-success">Diterima</span>';
                } elseif ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED) {
                    return '<span class="badge badge-danger">Ditolak</span>';
                } elseif ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED) {
                    return '<span class="badge badge-dark">Selesai</span>';
                } elseif ($query->status === AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE) {
                    return '<span class="badge badge-warning">Menunggu Surat Pengantar</span>';
                } else {
                    return '<span class="badge badge-dark">Selesai</span>';
                }
            })
            ->addColumn('action', function ($query) {
                $action = [
                    'detail' => route('client.advisi-teknis.show', $query->id),
                ];

                return view('admin.utils.table-action', $action)->render();
            })
            ->rawColumns(['status', 'action', 'letter_of_introduction', 'news_report']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Admin\AdvisiTeknisDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(AdvisiTeknis $model)
    {
        return $model->newQuery()
            ->whereClientId(auth()->user()->client->id)
            ->select(
                'advisi_teknis.*'
            );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin-advisiteknisdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->orderBy(6, 'desc')
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {

        // <td>No. Advisi Teknis</td> // Tidak tau
        // <td>Tanggal Pelaksanaan</td>
        // <td>Surat Pengantar</td>
        // <td>Berita Acara</td>
        // <td>Status Pelaksanaan</td>
        return [
            Column::make('test_date')
                ->title('Tanggal Pelaksanaan'),
            Column::make('building_identity')
                ->title('Identitas Bangunan'),
            Column::make('scope')
                ->title('Lingkup Konsultasi'),
            // Column::make('letter_of_introduction')
            //     ->title('Surat Pengantar'),
            // Column::make('news_report')
            //     ->title('Berita Acara'),

            Column::make('status', 'Status')
                ->addClass('text-center'),
            Column::make('created_at')
                ->title('Tanggal Dibuat')
                ->hidden(),
            Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'AdvisiTeknis_' . date('YmdHis');
    }
}
