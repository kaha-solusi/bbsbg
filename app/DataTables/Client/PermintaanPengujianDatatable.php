<?php

namespace App\DataTables\Client;

use App\Models\PermintaanPengujian;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PermintaanPengujianDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('status', function ($query) {
                if ($query->status === PermintaanPengujian::STATUS_DRAFT) {
                    return '<a href="#"
                            class="badge badge-warning">Permintaan Diajukan</a>';
                }
                if ($query->status === PermintaanPengujian::STATUS_KAJI_ULANG_SUBMITTED ||
                    $query->status === PermintaanPengujian::STATUS_KAJI_ULANG_APPROVED) {
                    return '<a href="#" class="badge badge-info">Sedang diproses</a>';
                }
                if ($query->status === PermintaanPengujian::STATUS_KAJI_ULANG_REJECTED) {
                    return '<a href="#" class="badge badge-danger">Ditolak</a>';
                }
                if ($query->status === PermintaanPengujian::STATUS_FORM_CONFIRMATION_SUBMITTED) {
                    return '<a href="'.route('client.permintaan-pengujian.show', $query->id).'"
                            class="badge badge-info">Kirim dokumen persetujuan</a>';
                }
                if ($query->status === PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_SUBMITTED) {
                    return '<a href="#"
                            class="badge badge-info">Menunggu Persetujuan</a>';
                }
                if ($query->status === PermintaanPengujian::STATUS_WAITING_INVOICE) {
                    return '<a href="#"
                            class="badge badge-warning">Menunggu Informasi Pembayaran</a>';
                }
                if ($query->status === PermintaanPengujian::STATUS_WAITING_PAYMENT) {
                    return '<a href="'.route('client.permintaan-pengujian.payment-confirmation.get', $query->id) .'"
                            class="badge badge-warning">Konfirmasi Pembayaran</a>';
                }
                if ($query->status === PermintaanPengujian::STATUS_WAITING_PAYMENT_CONFIRMED) {
                    return '<a href="#"
                            class="badge badge-warning">Menunggu Pembayaran Dikonfirmasi</a>';
                }
                if ($query->status === PermintaanPengujian::STATUS_PAYMENT_APPROVED) {
                    return '<a href="#"
                            class="badge badge-success">Pembayaran Diterima</a>';
                }

                if ($query->status === PermintaanPengujian::STATUS_SAMPLE_SUBMITTED) {
                    return '<a href="#"
                            class="badge badge-info">Benda Uji Telah Dikirim</a>';
                }

                if ($query->status === PermintaanPengujian::STATUS_SAMPLE_RECEIVED) {
                    return '<a href="#"
                            class="badge badge-info">Benda Uji Diterima</a>';
                }

                if ($query->status === PermintaanPengujian::STATUS_SAMPLE_REJECTED) {
                    return '<a href="#"
                            class="badge badge-danger">Benda Uji Ditolak</a>';
                }

                if ($query->status === PermintaanPengujian::STATUS_SPK_SUBMITTED
                    || $query->status === PermintaanPengujian::STATUS_PENGUJIAN_SUBMITTED) {
                    return '<a href="#"
                            class="badge badge-info">Sedang diproses</a>';
                }

                if ($query->status === PermintaanPengujian::STATUS_LHU_SUBMITTED) {
                    return '<a href="#"
                            class="badge badge-success">Selesai</a>';
                }
            })
            // ->editColumn('order_id', function ($query) {
            //     $action = [
            //         'route' => $query->status === PermintaanPengujian::STATUS_DRAFT ?
            //             route('admin.permintaan-pengujian.edit', $query->id) : route('admin.permintaan-pengujian.show', $query->id),
            //         'name' => $query->order_id,
            //     ];

            //     return view('admin.utils.link', $action)->render();
            // })
            ->addColumn('action', function ($query) {
                if ($query->status === PermintaanPengujian::STATUS_PAYMENT_APPROVED) {
                    return '<a href="'.route('client.permintaan-pengujian.test-sample.index', $query->id).'"
                            class="btn btn-sm btn-primary btn-block">Kirim Benda Uji</a>';
                }

                if ($query->status === PermintaanPengujian::STATUS_LHU_SUBMITTED) {
                    return '<a href="'.route('client.permintaan-pengujian.laporan-hasil-uji.get', $query->id).'"
                            class="btn btn-sm btn-primary ">Detail</a>';
                }

                if ($query->status === PermintaanPengujian::STATUS_SAMPLE_SUBMITTED) {
                    return '';
                }

                $action = [
                    'detail' => route('client.permintaan-pengujian.show', $query->id),
                ];

                return view('admin.utils.table-action', $action)->render();
            })
            ->rawColumns(['status', 'order_id', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Client\PermintaanPengujianDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PermintaanPengujian $model)
    {
        return $model->newQuery()
            ->with([
                'client' => function ($query) {
                    $query->select('id', 'name');
                },
                'master_layanan_uji' => function ($query) {
                    $query->select('id', 'name', 'pelayanan_id');
                },
                'master_layanan_uji.pelayanan' => function ($query) {
                    $query->select('id', 'nama');
                },
                'payments' => function ($query) {
                    $query->select('id', 'status');
                },
            ])
            ->where('permintaan_pengujians.client_id', auth()->user()->client->id)
            ->select(
                'permintaan_pengujians.id',
                'permintaan_pengujians.client_id',
                'permintaan_pengujians.master_layanan_uji_id',
                'tgl_permintaan',
                'permintaan_pengujians.status',
                'permintaan_pengujians.order_id'
            );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('client-permintaanpengujiandatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('R<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"
                    <"mr-2"f>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->orderBy(4)
                    ->addRowNumber('id', [
                        'checkbox' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('order_id')
                ->title('ID Pesanan'),
            Column::make('master_layanan_uji.pelayanan.nama')
                ->title('Nama Laboratorium'),
            Column::make('master_layanan_uji.name')
                ->title('Jenis Pengujian'),
            Column::make('tgl_permintaan')
                ->title('Tanggal Permintaan'),
            Column::make('status')
                ->title('Status')
                ->addClass('center'),
            Column::computed('action', '')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PermintaanPengujian_' . date('YmdHis');
    }
}
