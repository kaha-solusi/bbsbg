<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Column;

class MacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        Builder::macro('addRowNumber', function (string $checkbox_data, array $attributes = [
            'checkbox' => true,
        ]) {
            if ($attributes['checkbox']) {
                $this->addColumnDef([
                    'targets' => 0,
                    'checkboxes' => [
                        'selectRow' => true
                    ],
                ]);
            }


            $this->collection->prepend(
                Column::computed('')
                    ->content('')
                    ->data(null)
                    ->title('No')
                    ->width(10)
                    ->addClass('text-center')
            );
            
            $this->addColumnDef([
                'targets' => $attributes['checkbox'] ? 1 : 0,
                'render' => "function(_data, _type, _row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }"
            ]);

            if ($attributes['checkbox']) {
                $this->collection->prepend(
                    Column::computed('')
                        ->data($checkbox_data)
                        ->width(10)
                );
            }

            return $this;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
// 