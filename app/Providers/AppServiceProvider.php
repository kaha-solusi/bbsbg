<?php

namespace App\Providers;

use App\Models\InformasiPublik;
use App\Models\Pelayanan;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Berita;
use App\Models\KategoriPelayanan;
use App\Models\Maskot;
use App\Models\PelayananUI;
use App\Models\SettingApp;
use App\Models\Slider;
use App\Models\VideoUI;
use Illuminate\Pagination\Paginator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Blade;
use Spatie\Menu\Laravel\Html;
use Spatie\Menu\Laravel\Link;
use Spatie\Menu\Laravel\Menu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	    if (config('app.env') === 'production') {
            $this->app['request']->server->set('HTTPS','on');
        }

        View::composer('layouts.adminlte', function ($view) {
            $view->with([
                'setting' => SettingApp::first(),
                'kategori_pelayanan' => KategoriPelayanan::where('id', '!=', 2)->get(),
            ]);
        });

        View::composer('layouts.dashboard.client', function ($view) {
            $view->with([
                'setting' => SettingApp::first(),
                'kategori_pelayanan' => KategoriPelayanan::where('id', '!=', 2)->get(),
            ]);
        });

        View::composer('auth.login', function ($view) {
            $view->with([
                'setting' => SettingApp::first(),
            ]);
        });

        View::composer('auth.register', function ($view) {
            $view->with([
                'setting' => SettingApp::first(),
            ]);
        });

        View::composer('layouts.app', function ($view) {
            $view->with([
                'setting' => SettingApp::first(),
            ]);
        });

        View::composer('layouts.moderna', function ($view) {
            $view->with([
                'setting' => SettingApp::first(),
                'berita' => Berita::get(),
                'informasi' => InformasiPublik::get(),
                'pelayanan' => Pelayanan::get(),
                'kategori_pelayanan' => KategoriPelayanan::where('id', '!=', 2)->get(),
                'maskot' => Maskot::first(),
            ]);
        });

        config(['app.locale' => 'id']);
        Carbon::setLocale('id');
        date_default_timezone_set('Asia/Jakarta');

        Blade::directive('alert', function () {
            if (session()->has('success')) {
                return '<div class="alert alert-success"
                            role="alert">'.session()->get('success').'</div>';
            }

            if (session()->has('error') || session()->has('failed')) {
                return '<div class="alert alert-danger"
                        role="alert">'.session()->get('failed').'</div>';
            }
            
            return '';
        });
    }
}
