<?php

namespace App\Services;

use App\Models\JadwalKegiatanPersonil;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use App\User;

class JadwalKegiatanPersonilService
{
    public function store(Collection $data)
    {
        if ($data->get('penugasan') === "lainnya") {
            $penugasan = $data->get('keterangan');
        } else {
            $penugasan = $data->get('penugasan');
        }

        if (is_string($data->get('personil'))) {
            JadwalKegiatanPersonil::create([
                'pegawai_id' => $data->get('personil'),
                'tanggal_mulai' => $data->get('tanggal_mulai') ?? date('Y-m-d'),
                'tanggal_selesai' => $data->get('tanggal_selesai') ?? date('Y-m-d'),
                'penugasan' => $penugasan,
            ]);
        } else {
            $personils = $data->get('personil');
            foreach ($personils as $personil) {
                JadwalKegiatanPersonil::create([
                    'pegawai_id' => $personil->pegawai_id,
                    'tanggal_mulai' => $data->get('tanggal_mulai') ?? date('Y-m-d'),
                    'tanggal_selesai' => $data->get('tanggal_selesai') ?? date('Y-m-d'),
                    'penugasan' => $penugasan,
                ]);
            }
        }
    }

    public function destroy(JadwalKegiatanPersonil $jadwalKegPersonil)
    {
        $jadwalKegPersonil->delete();
    }

    public function update(JadwalKegiatanPersonil $jadwalKegPersonil, Collection $data)
    {
        if ($data->get('penugasan') === "lainnya") {
            $penugasan = $data->get('keterangan');
        } else {
            $penugasan = $data->get('penugasan');
        }

        $jadwalKegPersonil->update([
            'pegawai_id' => $data->get('personil'),
            'tanggal_mulai' => $data->get('tanggal_mulai') ?? date('Y-m-d'),
            'tanggal_selesai' => $data->get('tanggal_selesai') ?? date('Y-m-d'),
            'penugasan' => $penugasan,
        ]);
        $jadwalKegPersonil->save();
    }
}
