<?php

namespace App\Services;

use App\Models\LabPosition;
use Illuminate\Support\Collection;

class LabPositionService
{
    public function store(Collection $data)
    {
        LabPosition::create([
            'name' => $data->get('name'),
        ]);
    }

    public function update(Collection $data, LabPosition $labPosition)
    {
        $labPosition->update([
            'name' => $data->get('name'),
        ]);
        $labPosition->save();
    }

    public function destroy(LabPosition $labPosition)
    {
        $labPosition->delete();
    }
}
