<?php

namespace App\Services;

use App\Models\FunctionalPosition;
use App\Models\LabPosition;
use App\Models\Pegawai;
use App\User;
use Carbon\Carbon;
use File;
use Hash;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Storage;
use Str;

class PegawaiService
{
    public function getAll()
    {
        $pegawais = Pegawai::with(['user'])
        ->whereHas('user', function (Builder $query) {
            $query->whereHas('roles', function ($q) {
                $q->whereNotIn('name', [
                    'super-admin',
                    'pelanggan',
                    'Admin',
                    'direktur',
                    'kepala-balai',
                ]);
            });
        })
        ->select('id', 'foto', 'user_id')
        ->get();

        $pegawais = $pegawais->map(function ($q) {
            return [
            'id' => $q->id,
            'name' => $q->user->name,
            'avatar' => asset("uploads/foto/pegawai" . DIRECTORY_SEPARATOR . $q->foto),
            ];
        });
        return $pegawais;
    }

    public function store(Collection $data)
    {
        if (request()->file('foto')) {
            $name = request()->file('foto');
            $foto = hash('sha256', $name->getClientOriginalName()).".".$name->getClientOriginalExtension();

            request()->foto->move("uploads/foto/pegawai", $foto);

            $data->put('foto', $foto);
        }

        $user = User::create([
            'name' => $data->get('nama'),
            'email' => $data->get('email'),
            'username'  => str_replace(' ', '', $data->get('nama')),
            'password'  => Hash::make($data->get('password')),
            'level' => $data->get('role'),
            'status' => 'aktif',
        ]);
        $user->assignRole($data->get('role'));

        $pegawai = $user->pegawai()->create([
            'nip' => $data->get('nip'),
            'lab_id' => $data->get('lab_id'),
            'jabatan_id' => $data->get('jabatan_id'),
            'golongan_id' => $data->get('golongan_id'),
            'no_hp' => $data->get('no_hp'),
            'foto' => $data->get('foto'),
            'gender' => $data->get('gender'),
            'birth_place' => $data->get('birth_place'),
            'birth_date' => $data->get('birth_date')
        ]);

        if ($data->get('jabatan_laboratorium')) {
            $pegawai->lab_positions()->sync($data->get('jabatan_laboratorium'));
        }

        if ($data->get('kompetensi')) {
            $kompetensis = $data->get('kompetensi');
            foreach ($kompetensis as $kompetensi) {
                $pegawai->kompetensi()->create([
                    'nama' => $kompetensi,
                    'pegawai_id' => $pegawai->id,
                ]);
            }
        }

        if ($data->get('daftar_diklat')) {
            $daftar_diklats = $data->get('daftar_diklat');
            foreach ($daftar_diklats as $daftar_diklat) {
                $pegawai->diklat()->create([
                    'nama' => $daftar_diklat,
                    'pegawai_id' => $pegawai->id,
                ]);
            }
        }
    }

    public function destroy(User $user)
    {
        if (!is_null($user->pegawai->foto)) {
            Storage::delete($user->pegawai->foto);
        }

        $user->delete();
    }

    public function update(User $user, Collection $data)
    {
        if (request()->file('foto')) {
            $name = request()->file('foto');
            $foto = hash('sha256', $name->getClientOriginalName()).".".$name->getClientOriginalExtension();
            request()->foto->move("uploads/foto/pegawai", $foto);

            $data->put('foto', $foto);

            File::delete("uploads/foto/pegawai/" . $user->pegawai->foto);
        }

        $user->syncRoles($data->get('role'));
        $user->name = $data->pull('nama');
        $user->level = $data->pull('role');
        if ($data->pull('jabatan_laboratorium')) {
            $user->pegawai->lab_positions()->sync($data->pull('jabatan_laboratorium'));
        }
        $data->each(function ($value, $key) use ($user) {
            if (!in_array($key, array_keys($user->getAttributes()))) {
                $user->pegawai->{$key} = $value;
            } else {
                if ($key === 'password' && $value) {
                    $user->{$key} = Hash::make($value);
                } else {
                    $user->{$key} = $value;
                }
            }
        });
        $user->pegawai->save();
        $user->save();
    }

    public function getLaboratoriumPositionsByName(mixed $name = null)
    {
        return LabPosition::where('name', 'like', '%'.$name.'%')->select('id', 'name as text')->get();
    }

    public function getFunctionalPositionByName(mixed $name = null)
    {
        return FunctionalPosition::where('name', 'like', '%'.$name.'%')->select('id', 'name as text')->get();
    }
}
