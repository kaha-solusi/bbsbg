<?php

namespace App\Services;

use App\Models\AdvisiTeknis;
use App\Models\File;
use App\Notifications\AdvisiTeknisRegistered;
use App\Notifications\AdvisTeknisRejectedNotification;
use Illuminate\Support\Collection;
use App\User;

class AdvisiTeknisService
{
    private $uploadService;
    public function __construct(UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }

    public function store(Collection $data, User $user)
    {
        $fileIds = [];
        if ($data->get('support_file')) {
            $fileIds = $this->uploadService->saveFileBulk($data->get('support_file'), 'advisi_teknis');
        }

        $advisTeknis = AdvisiTeknis::create([
            'test_date' => $data->get('test_date'),
            'scope' => $data->get('scope'),
            'building_identity' => $data->get('building_identity'),
            'problem' => $data->get('problem'),
            'support_file' => '',
            'client_id' => $user->client->id,
            'province_id' => $data->get('province'),
            'regency_id' => $data->get('regency'),
            'district_id' => $data->get('district'),
            'village_id' => $data->get('village'),
            'status' => 'draft'
        ]);

        $advisTeknis->files()->attach($fileIds);

        $user->notify(new AdvisiTeknisRegistered());
    }

    public function destroy(AdvisiTeknis $advisiTeknis)
    {
        $advisiTeknis->delete();
    }

    public function addMembers(AdvisiTeknis $advisiTeknis, array $members, string $link)
    {
        $advisiTeknis->status = AdvisiTeknis::ADVISI_TEKNIS_STATUS_UPLOAD_DOCUMENT;
        $advisiTeknis->link = $link;
        $advisiTeknis->members()->attach($members);

        $advisiTeknis->save();
    }

    public function uploadDocument(AdvisiTeknis $advisiTeknis, $suratPengantar, $beritaAcara)
    {
        if ($suratPengantar) {
            $fileId = $this->uploadService
            ->saveFile($suratPengantar, 'uploads/advisi-teknis/' . $advisiTeknis->id);
            $advisiTeknis->letter_of_introduction = File::find($fileId)->filename;
            $advisiTeknis->status = AdvisiTeknis::ADVISI_TEKNIS_STATUS_COMPLETED;
        }

        if ($beritaAcara) {
            $fileId = $this->uploadService
            ->saveFile($beritaAcara, 'uploads/advisi-teknis/' . $advisiTeknis->id);
            $advisiTeknis->news_report = File::find($fileId)->filename;
            $advisiTeknis->status = AdvisiTeknis::ADVISI_TEKNIS_STATUS_AWAITING_LETTER_OF_INTRODUCE;
        }

        $advisiTeknis->save();
    }

    public function approve(AdvisiTeknis $advisiTeknis, Collection $data)
    {
        $advisiTeknis->status = $data->get('status');
        $advisiTeknis->time = $data->get('time');
        $advisiTeknis->confirmation_date = $data->get('confirmation_date');
        $advisiTeknis->approvals()->attach(auth()->user()->id, [
            'status' => AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED,
        ]);
        $advisiTeknis->members()->sync($data->get('members'));
        $advisiTeknis->save();
    }

    public function approve_kepala(AdvisiTeknis $advisiTeknis)
    {
        $advisiTeknis->status = AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED;
        $advisiTeknis->approvals()->attach(auth()->user()->id, [
            'status' => AdvisiTeknis::ADVISI_TEKNIS_STATUS_APPROVED,
        ]);
        $advisiTeknis->save();
    }

    public function reject(AdvisiTeknis $advisiTeknis, $userId, $reason)
    {
        $advisiTeknis->status = AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED;
        $advisiTeknis->approvals()->attach($userId, [
            'status' => AdvisiTeknis::ADVISI_TEKNIS_STATUS_REJECTED,
            'reason' => $reason,
        ]);
        $advisiTeknis->save();

        $userClient = $advisiTeknis->client->user;
        $userClient->notify(new AdvisTeknisRejectedNotification($advisiTeknis, $reason));
    }

    public function store_kuisioner(array $data, $advisiTeknis)
    {
        $advisiTeknis->kuisioners()->attach($data);
    }
}
