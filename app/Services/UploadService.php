<?php

namespace App\Services;

use App\Models\File as ModelsFile;
use File;

class UploadService
{
    public static function saveFile(\Illuminate\Http\UploadedFile $file, $path = '')
    {
        $filename = hash('sha256', time().$file->getClientOriginalName()).".".$file->getClientOriginalExtension();
        $filepath = 'uploads/'.$path.'/'.$filename;
        $file->move(public_path('uploads/'.$path), $filename);

        $fileId = ModelsFile::create([
            'filename' => $filename,
            'filetype' => $file->getClientMimeType(),
            'path' => $filepath
        ])->id;
        return $fileId;
    }

    public function saveFileBulk(array $files, $directory = '')
    {
        $path = $directory !== '' ? 'uploads'.DIRECTORY_SEPARATOR.$directory : 'uploads';
        $dataFiles = [];
        foreach ($files as $file) {
            $filename = sha1(time().time()).".".$file->getClientOriginalExtension();
            $filepath = $path.'/'.$filename;
            $file->move(public_path($path), $filename);
            $fileId = ModelsFile::create([
                'filename' => $filename,
                'filetype' => $file->getClientMimeType(),
                'path' => $filepath
            ])->id;

            $dataFiles[] = $fileId;
        }

        return $dataFiles;
    }

    public static function removeFile($path, $filename)
    {
        File::delete($path . $filename);
    }

    public static function updateFile($filename, \Illuminate\Http\UploadedFile $file, $path)
    {
        if (File::exists($path . DIRECTORY_SEPARATOR . $filename)) {
            self::removeFile($path, $filename);
        }
        
        return self::saveFile($file, $path);
    }
}
