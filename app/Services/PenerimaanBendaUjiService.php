<?php

namespace App\Services;

use App\Models\PermintaanPengujian;
use App\Models\PermintaanPengujianSample;
use Illuminate\Http\Request;

/**
 * Class PenerimaanBendaUjiService
 *
 * @package App\Services
 */
class PenerimaanBendaUjiService
{
    public function approve(Request $request, PermintaanPengujianSample $sample)
    {
        $this->updateOrCreateSampleItem($sample, $request->test_sample);
        $this->updateStatus(
            $sample,
            PermintaanPengujian::STATUS_APPROVED,
            PermintaanPengujian::STATUS_SAMPLE_RECEIVED
        );

        $spkService = new SuratPerintahKerjaService();
        $sample->permintaan_pengujian->spk()->create([
            'nomor' => $spkService->generateNomorSPK(),
            'status' => PermintaanPengujian::STATUS_DRAFT,
        ]);
    }

    public function reject(Request $request, PermintaanPengujianSample $sample)
    {
        $this->updateOrCreateSampleItem($sample, $request->test_sample);
        $this->updateStatus(
            $sample,
            PermintaanPengujian::STATUS_REJECTED,
            PermintaanPengujian::STATUS_SAMPLE_REJECTED,
            $request->message
        );
    }

    private function updateStatus(
        PermintaanPengujianSample $sample,
        $sampleStatus,
        $permintaanPengujianStatus,
        $message = ''
    )
    {
        $sample->update([
            'status' => $sampleStatus,
        ]);
        $sample->permintaan_pengujian->update([
            'status' => $permintaanPengujianStatus,
        ]);
        $sample->approval()->create([
            'status' => $sampleStatus,
            'message' => $message,
        ]);
    }

    private function updateOrCreateSampleItem(PermintaanPengujianSample $permintaanPengujianSample, $samples)
    {
        $items = [];
        foreach ($samples as $sample) {
            $items[$sample['id']] = [
                'kode_sampel' => $sample['no_sampel'],
                'status' => $sample['status'],
                'keterangan' => $sample['keterangan'],
            ];
        }
        $permintaanPengujianSample->items()->sync($items);
    }
}
