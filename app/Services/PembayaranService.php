<?php

namespace App\Services;

use App\Jobs\PaymentExpiredJob;
use App\Jobs\PaymentReminderJob;
use App\Models\PermintaanPengujianPayment;
use App\Models\PermintaanPengujian;
use App\Notifications\PaymentConfirmedNotification;
use App\Notifications\PaymentReminderNotification;
use Illuminate\Http\Request;

/**
 * Class PembayaranService
 *
 * @package App\Services
 */
class PembayaranService
{
    private $uploadService;
    public function __construct(UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }
    
    public function submitKodeBilling(PermintaanPengujianPayment $pembayaran, Request $request)
    {
        $kodeBilling = $request->billing_code;

        if (isset($request->how_to_pay)) {
            $fileId = $this->uploadService->saveFile($request->file('how_to_pay'), 'how_to_pay');
        }

        $pembayaran->permintaan_pengujian->update([
            'status' => PermintaanPengujian::STATUS_WAITING_PAYMENT,
        ]);
        $pembayaran->update([
            'billing_code' => $kodeBilling,
            'status' => PermintaanPengujian::STATUS_SUBMITTED,
            'file_id' => $fileId,
        ]);

        $client = $pembayaran->permintaan_pengujian->client->user;
        $client->notify(new PaymentReminderNotification());
        PaymentReminderJob::dispatch($client)->delay(now()->addDay(3));
        PaymentExpiredJob::dispatch($client)->delay(now()->addDay(5));
    }

    public function downloadPaymentConfirmation(PermintaanPengujianPayment $pembayaran)
    {
        $latestConfirmation = $pembayaran->confirmation->where('status', PermintaanPengujian::STATUS_DRAFT)->last();
        $downloadURL = $latestConfirmation->file->getPath();

        $latestConfirmation->update([
            'status' => PermintaanPengujian::STATUS_PAYMENT_DOWNLOADED,
        ]);
        
        return $downloadURL;
    }

    public function approve(PermintaanPengujianPayment $pembayaran)
    {
        $pembayaran->update([
            'status' => PermintaanPengujian::STATUS_APPROVED,
        ]);
        $pembayaran->permintaan_pengujian->update([
            'status' => PermintaanPengujian::STATUS_PAYMENT_APPROVED,
        ]);
        $pembayaran->confirmation->last()->approval()->create([
            'status' => PermintaanPengujian::STATUS_APPROVED,
        ]);
        $pembayaran->permintaan_pengujian->client->user->notify(new PaymentConfirmedNotification());
    }

    public function reject(PermintaanPengujianPayment $pembayaran, $message)
    {
        $pembayaran->update([
            'status' => PermintaanPengujian::STATUS_REJECTED,
        ]);
        $pembayaran->permintaan_pengujian->update([
            'status' => PermintaanPengujian::STATUS_PAYMENT_DECLINED,
        ]);
        $pembayaran->confirmation->last()->approval()->create([
            'status' => PermintaanPengujian::STATUS_REJECTED,
            'message' => $message,
        ]);
    }
}
