<?php

namespace App\Services;

use App\Models\Jabatan;
use Illuminate\Support\Collection;

class JabatanService
{
    public function store(Collection $data)
    {
        Jabatan::create([
            'nama' => $data->get('nama'),
        ]);
    }
    
    public function update(Jabatan $jabatan, Collection $data)
    {
        $jabatan->update([
            'nama' => $data->get('nama'),
        ]);
        $jabatan->save();
    }

    public function destroy(Jabatan $jabatan)
    {
        $jabatan->delete();
    }
}
