<?php

namespace App\Services;

use App\Models\Instansi;

class InstansiService
{
    public function store(string $nama, Instansi $parent = null)
    {
        $node = new Instansi([
            'nama_instansi' => $nama,
        ]);

        if ($parent) {
            $node->appendToNode($parent);
        }

        $node->save();
    }

    public function destroy(Instansi $instansi)
    {
        $instansi->delete();
    }

    public function update(Instansi $instansi, string $nama)
    {
        $instansi->nama_instansi = $nama;
        $instansi->save();
    }

    public function getUnitOrganisasi()
    {
        $unitOrganisasi = Instansi::select('id', 'nama_instansi as nama')
            ->whereNull('parent_id')
            ->get();

        return $unitOrganisasi;
    }

    public function getUnitKerja($unitOrganisasi)
    {
        if (is_null($unitOrganisasi)) {
            return collect();
        }

        $unitKerja = Instansi::select('id', 'nama_instansi as nama')
            ->where('parent_id', $unitOrganisasi->id)
            ->get();

        return $unitKerja;
    }
}
