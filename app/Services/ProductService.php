<?php

namespace App\Services;

use App\Models\MasterLayananUjiItem;
use App\Models\Product;
use Illuminate\Support\Collection;

class ProductService
{
    public function store(Collection $data)
    {
        $product = Product::create([
            'name' => $data->get('name'),
            'description' => $data->get('description'),
        ]);

        if ($data->get('alat_kerja')) {
            $alats = $data->get('alat_kerja');
            foreach ($alats as $alat) {
                $product->alat()->create([
                    'nama' => $alat,
                    'product_id' => $product->id
                ]);
            }
        }

        if ($data->get('bahan_kerja')) {
            $bahans = $data->get('bahan_kerja');
            foreach ($bahans as $bahan) {
                $product->bahan()->create([
                    'nama' => $bahan,
                    'product_id' => $product->id
                ]);
            }
        }

        $product->parameters()->attach($data->get('testParameters'));
    }

    public function destroy(Product $product)
    {
        $product->delete();
    }

    public function update(Product $product, Collection $data)
    {
        $product->name = $data->get('name');
        $product->description = $data->get('description');

        if ($data->get('alat_kerja')) {
            $alats = $data->get('alat_kerja');
            foreach ($alats as $alat) {
                $product->alat()->create([
                    'nama' => $alat,
                    'product_id' => $product->id
                ]);
            }
        }

        if ($data->get('bahan_kerja')) {
            $bahans = $data->get('bahan_kerja');
            foreach ($bahans as $bahan) {
                $product->bahan()->create([
                    'nama' => $bahan,
                    'product_id' => $product->id
                ]);
            }
        }
        $product->save();
        $product->parameters()->sync($data->get('parameters'));
    }

    public function getParameterPengujian()
    {
        return MasterLayananUjiItem::select('id', 'name')->get()->toArray();
    }
}
