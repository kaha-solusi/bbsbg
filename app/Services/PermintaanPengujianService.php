<?php

namespace App\Services;

use App\Models\PermintaanPengujian;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PermintaanPengujianService
{
    public function ajukanKajiUlang(Request $request, PermintaanPengujian $permintaanPengujian)
    {
        $permintaanPengujian->update([
            'order_id' => $request->nomor_permintaan,
            'status' => PermintaanPengujian::STATUS_KAJI_ULANG_SUBMITTED,
        ]);

        $permintaanPengujian->kaji_ulang()->create([
            'status' => PermintaanPengujian::STATUS_DRAFT,
            'parameter' => $request->parameters,
            'subkontraktor' => $request->subkontraktor['use_subkontraktor'],
            'nama_subkontraktor' => $request->subkontraktor['nama'] ?? null,
            'keterangan' => $request->subkontraktor['alasan'] ?? null,
            'durasi' => $request->durasi,
            'tanggal_pelayanan' => $request->tanggal_pelayanan
        ]);
    }

    public function unggahKonfirmasiLayanan(Request $request, PermintaanPengujian $permintaanPengujian)
    {
        $uploadService = new UploadService();
        $file = $uploadService->saveFile($request->file, 'uploads/dokumen_konfirmasi');

        $permintaanPengujian->update([
            'status' => PermintaanPengujian::STATUS_FORM_CONFIRMATION_SUBMITTED,
        ]);

        $permintaanPengujian->dokumen()->create([
            'file_id' => $file,
            'type' => 'dokumen_konfirmasi',
        ]);
    }

    public function clientUnggahKonfirmasiLayanan(Request $request, PermintaanPengujian $permintaanPengujian)
    {
        $uploadService = new UploadService();
        $file = $uploadService->saveFile($request->dokumen, 'uploads/dokumen_konfirmasi_reply');

        $permintaanPengujian->update([
            'status' => PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_SUBMITTED,
        ]);

        $permintaanPengujian->dokumen()->create([
            'file_id' => $file,
            'type' => 'dokumen_konfirmasi_reply',
        ]);
    }

    public function unduhKonfirmasiLayanan(PermintaanPengujian $permintaanPengujian)
    {
        $permintaanPengujian->update([
            'status' => PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_DOWNLOADED,
        ]);
    }

    public function setujuiDokumen(PermintaanPengujian $permintaanPengujian)
    {
        $permintaanPengujian->update([
            'status' => PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_APPROVED,
        ]);

        // $permintaanPengujian->payments()->create([
        //     'status' => PermintaanPengujian::STATUS_DRAFT,
        // ]);
    }
    

    // public function update(Request $request, PermintaanPengujian $permintaanPengujian)
    // {
    //     if ($permintaanPengujian->status === PermintaanPengujian::STATUS_DRAFT) {
    //         $permintaanPengujian->update([
    //             'nomor_permintaan' => $request->nomor_permintaan,
    //             'tanggal_pelayanan' => $request->tanggal_pelayanan,
    //             'status' => PermintaanPengujian::STATUS_KAJI_ULANG_SUBMITTED,
    //         ]);
    //         $this->submitKajiUlang($permintaanPengujian, collect([
    //             'parameters' => $request->parameters,
    //             'subkontraktor' => $request->subkontraktor,
    //             'nama_subkontraktor' => $request->nama_subkontraktor,
    //             'alasan_subkontraktor' => $request->alasan_subkontraktor,
    //             'durasi' => $request->durasi,
    //         ]));
    //     } elseif ($permintaanPengujian->status === PermintaanPengujian::STATUS_KAJI_ULANG_SUBMITTED) {
    //         if ($request->status === PermintaanPengujian::STATUS_APPROVED) {
    //             $this->kajiUlangApproved($permintaanPengujian);
    //         } else {
    //             $this->kajiUlangRejected($permintaanPengujian, $request->message);
    //         }
    //     } elseif ($permintaanPengujian->status === PermintaanPengujian::STATUS_KAJI_ULANG_APPROVED) {
    //         $this->submitDokumenKonfirmasi($permintaanPengujian, $request->dokumen);
    //     } elseif ($permintaanPengujian->status === PermintaanPengujian::STATUS_FORM_CONFIRMATION_SUBMITTED) {
    //         $this->submitDokumentKonfirmasiReply($permintaanPengujian, $request->dokumen);
    //     } elseif ($permintaanPengujian->status === PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_SUBMITTED) {
    //         $permintaanPengujian->update([
    //             'status' => PermintaanPengujian::STATUS_WAITING_INVOICE,
    //         ]);
    //     } elseif ($permintaanPengujian->status === PermintaanPengujian::STATUS_WAITING_INVOICE) {
    //         $permintaanPengujian->update([
    //             'status' => PermintaanPengujian::STATUS_WAITING_PAYMENT,
    //             'kode_billing' => $request->billing_code,
    //         ]);
    //     } elseif ($permintaanPengujian->status === PermintaanPengujian::STATUS_SAMPLE_SUBMITTED) {
    //         $this->approveSample($permintaanPengujian, $request->test_sample);
    //     } elseif ($permintaanPengujian->status === PermintaanPengujian::STATUS_SAMPLE_RECEIVED) {
    //         $this->createSPK($permintaanPengujian, collect([
    //             'employes' => $request->pegawai,
    //             'nomor' => $request->nomor,
    //             'metode_uji' => $request->metode_pengujian,
    //             'tanggal_mulai' => $request->tanggal_mulai,
    //             'tanggal_selesai' => $request->tanggal_selesai,
    //             'keterangan' => $request->catatan,
    //         ]));
    //     }
    // }

    private function submitKajiUlang(PermintaanPengujian $permintaanPengujian, Collection $data)
    {
        $permintaanPengujian->kaji_ulang()->create([
            'parameter' => $data->get('parameters'),
            'subkontraktor' => $data->get('subkontraktor'),
            'nama_subkontraktor' => $data->get('nama_subkontraktor'),
            'keterangan' => $data->get('alasan_subkontraktor'),
            'durasi' => $data->get('durasi'),
        ]);
    }

    private function kajiUlangApproved(PermintaanPengujian $permintaanPengujian)
    {
        $permintaanPengujian->update([
            'status' => PermintaanPengujian::STATUS_KAJI_ULANG_APPROVED,
        ]);
        $permintaanPengujian->kaji_ulang->approvals()->create([
            'status' => PermintaanPengujian::STATUS_APPROVED,
        ]);
    }

    private function kajiUlangRejected(PermintaanPengujian $permintaanPengujian, $reason)
    {
        $permintaanPengujian->update([
            'status' => PermintaanPengujian::STATUS_KAJI_ULANG_REJECTED,
        ]);
        $permintaanPengujian->kaji_ulang->approvals()->create([
            'status' => PermintaanPengujian::STATUS_REJECTED,
            'reason' => $reason,
        ]);
    }

    private function submitDokumenKonfirmasi(PermintaanPengujian $permintaanPengujian, $dokumen)
    {
        $uploadService = new UploadService();
        $file = $uploadService->saveFile($dokumen, 'uploads/dokumen_konfirmasi');

        $permintaanPengujian->update([
            'status' => PermintaanPengujian::STATUS_FORM_CONFIRMATION_SUBMITTED,
        ]);

        $permintaanPengujian->dokumen()->create([
            'file_id' => $file,
            'type' => 'dokumen_konfirmasi',
        ]);
    }

    private function submitDokumentKonfirmasiReply(PermintaanPengujian $permintaanPengujian, $dokumen)
    {
        $uploadService = new UploadService();
        $file = $uploadService->saveFile($dokumen, 'uploads/dokumen_konfirmasi_reply');

        $permintaanPengujian->update([
            'status' => PermintaanPengujian::STATUS_FORM_CONFIRMATION_REPLY_SUBMITTED,
        ]);

        $permintaanPengujian->dokumen()->create([
            'file_id' => $file,
            'type' => 'dokumen_konfirmasi_reply',
        ]);
    }

    private function approveSample(PermintaanPengujian $permintaanPengujian, $samples)
    {
        foreach ($samples as $sample) {
            $permintaanPengujian->items()->updateOrCreate([
                'id' => $sample['id'],
            ], [
                "no_sample" => $sample['no_sampel'],
                "status" => $sample['status'],
                "keterangan" => $sample['keterangan'],
            ]);
        }

        $permintaanPengujian->update([
            'status' => PermintaanPengujian::STATUS_SAMPLE_RECEIVED,
        ]);
        $permintaanPengujian->test_samples()->update([
            'tanggal_diterima' => date('Y-m-d'),
            'status' => PermintaanPengujian::STATUS_SAMPLE_RECEIVED,
        ]);
    }
    
    private function createSPK(PermintaanPengujian $permintaanPengujian, Collection $data)
    {
        $employes = [];
        foreach ($data->get('employes') as $employe) {
            $employes[]['pegawai_id'] = $employe;
        }

        $permintaanPengujian->update([
            'status' => PermintaanPengujian::STATUS_SPK_SUBMITTED,
        ]);
        
        $permintaanPengujian->spk()->create([
            'nomor' => $data->get('nomor'),
            'metode_uji' => $data->get('metode_uji'),
            'tanggal_mulai' => $data->get('tanggal_mulai'),
            'tanggal_selesai' => $data->get('tanggal_selesai'),
            'keterangan' => $data->get('keterangan'),
        ])->members()->createMany($employes);
    }

    public function store_kuisioner(array $data, $pengujian)
    {
        $pengujian->kuisioners()->attach($data);
    }
}
