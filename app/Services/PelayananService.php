<?php

namespace App\Services;

use App\Models\AdvisiTeknis;
use App\Models\Client;
use App\Models\PermintaanPengujian;
use App\Models\PermintaanPengujianItem;
use App\Notifications\AdvisiTeknisRegistered;
use App\User;
use Illuminate\Support\Collection;

/**
 * Pelayanan Service
 *
 * @category Description
 * @package  Client\Pengujian
 * @author   Agung Kurniawan <agungkes95@gmail.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @version  Release: 0.0.1
 * @access   public
 * @link     http://url.com
 */
class PelayananService
{
    private $uploadService;
    public function __construct(UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }

    public function store(Collection $data, User $user)
    {
        $file = $this->uploadService->saveFile($data->get('support_file'), 'uploads/advisi-teknis');
        $data->put('support_file', $file);
        
        AdvisiTeknis::create([
            'test_date' => $data->get('test_date'),
            'scope' => $data->get('scope'),
            'building_identity' => $data->get('building_identity'),
            'problem' => $data->get('problem'),
            'support_file' => $data->get('support_file'),
            'client_id' => $user->client->id,
            'province_id' => $data->get('province_id'),
            'regency_id' => $data->get('regency_id'),
            'district_id' => $data->get('district_id'),
            'village_id' => $data->get('village_id'),
        ]);

        $user->notify(new AdvisiTeknisRegistered());
    }
}
