<?php

namespace App\Services;

use App\Models\MasterLayananUji;
use App\Models\PermintaanPengujian;
use App\Notifications\PermintaanPengujianReceivedNotification;
use App\Notifications\PermintaanPengujianRegisteredNotification;
use App\User;
use Carbon\Carbon;

/**
 * PengujianService
 *
 * @category Description
 * @package  Client\Pengujian
 * @author   Agung Kurniawan <agungkes95@gmail.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @version  Release: 0.0.1
 * @access   public
 * @link     http://url.com
 */
class PengujianService
{

    public function store($data)
    {
        $master = MasterLayananUji::firstWhere('name', $data['test_type']);
        $permintaan = PermintaanPengujian::create(
            [
                'order_id' => $this->generateOrderID($data),
                'client_id' => auth()->user()->client->id,
                'nama_pemohon' => $data['billing_name'],
                'alamat' => $data['billing_address'],
                'nomor' => $data['billing_phone'],
                'email' => $data['billing_email'],
                'tgl_permintaan' => $data['test_date'] ?? date('Y-m-d'),
                'status' => PermintaanPengujian::STATUS_DRAFT,
                'company_email' => $data['company_billing_email'] ?? null,
                'company_address' => $data['company_billing_address'] ?? null,
                'company_name' => $data['company_billing_name'] ?? null,
                'company_phone' => $data['company_billing_phone'] ?? null,
                'master_layanan_uji_id' => $master->id,
            ]
        );
        
        // Create history on create
        $permintaan->history()->create([
            'message' => "Permohonan layanan pengujian telah tercatat",
        ]);

        $products = [];
        foreach ($data['items'] as $item) {
            for ($i = 0; $i < intval($item['product_count']); $i += 1) {
                $products[] = [
                    'product_id' => $item['product_id'],
                    'product_name' => $item['product_name'],
                    'product_type' => $item['product_type'],
                    'product_count' => 1,
                    'master_layanan_uji_item_id' => intval($item['test_parameter']),
                ];
            }
        }
        $permintaan->items()->createMany($products);
        $users = User::role(['penyelia', 'subkoor'])
            ->whereHas('pegawai.lab', function ($query) use ($master) {
                $query->where('id', $master->pelayanan->id);
            })->get();


        /**
         * Send email to users
         *
         * @var App\User $user
         */

        foreach ($users as $user) {
            $notification = new PermintaanPengujianReceivedNotification($permintaan, $products);
            $notification->name = $user->name;

            $user->notify($notification);
        }
        auth()->user()->notify(new PermintaanPengujianRegisteredNotification($data['billing_name']));
    }

    private function generateOrderID($data)
    {
        $labCode = strpos(strtolower($data['laboratorium_name']), "bahan bangunan") !== false ? "B" : "S";
        $currentDate = Carbon::now();
        $startOfYear = $currentDate->copy()->startOfYear();
        $endOfYear = $currentDate->copy()->endOfYear();
        
        $lastOrderId = PermintaanPengujian::select('order_id')
            ->where('order_id', 'like', '%' . $labCode . '%')
            ->whereBetween('created_at', [$startOfYear, $endOfYear])
            ->orderBy('created_at', 'desc')
            ->first();

        $counter = 0;
        $orderID = [];
        $orderID[] = $labCode;
        $orderID[] = $currentDate->format("Ymd");
        if (!is_null($lastOrderId)) {
            $lastOrderId = explode('/', $lastOrderId->order_id);
            $orderID[] = str_pad($lastOrderId[2] + 1, 3, "0", STR_PAD_LEFT);
        } else {
            $orderID[] = str_pad($counter + 1, 3, "0", STR_PAD_LEFT);
        }
        return implode("/", $orderID);
    }
}
