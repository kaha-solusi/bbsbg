<?php

namespace App\Services;

use App\Models\LaporanHasilUji;
use App\Models\PermintaanPengujian;
use App\Models\PermintaanPengujianPengujian;

/**
 * Class LaporanHasilUjiService
 *
 * @package App\Services
 */
class LaporanHasilUjiService
{
    public function createLHU($pengujianId)
    {
        $pengujian = PermintaanPengujianPengujian::find($pengujianId);
        // LaporanHasilUji::
        $pengujian->update([
            'status' => PermintaanPengujian::STATUS_SUBMITTED,
        ]);
        $pengujian->permintaan_pengujian->laporan_hasil_uji()->create([
            'nomor' => $this->generateNomorLaporan(),
            'status' => PermintaanPengujian::STATUS_DRAFT,
        ]);
    }

    public function generateNomorLaporan()
    {
        $permintaan = LaporanHasilUji::orderBy('id', 'desc')->count();
        return 'SLHU/'.date('Ymd').'/'.str_pad($permintaan + 1, 3, "0", STR_PAD_LEFT);
    }
}
