<?php

namespace App\Services;

use App\Models\PermintaanPengujian;
use App\Models\PermintaanPengujianSpk;
use Illuminate\Http\Request;

/**
 * Class SuratPerintahKerjaService
 *
 * @package App\Services
 */
class SuratPerintahKerjaService
{
    public function update(Request $request, PermintaanPengujianSpk $spk)
    {
        $spk->update([
            'nomor' => $request->nomor,
            'tanggal_mulai' => $request->tanggal_mulai,
            'tanggal_selesai' => $request->tanggal_selesai,
            'status' => PermintaanPengujian::STATUS_SUBMITTED,
            'keterangan' => $request->catatan,
        ]);

        $members = [];
        foreach ($request->pegawai as $pegawai) {
            $members[$pegawai] = [
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }
        $spk->members()->sync($members);
    }

    public function approve(PermintaanPengujianSpk $spk)
    {
        $spk->approvals()->create([
            'status' => PermintaanPengujian::STATUS_APPROVED,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        if (auth()->user()->hasRole(['super-admin', 'subkoor'])) {
            $spk->update([
                'status' => PermintaanPengujian::STATUS_APPROVED_BY_SUBKOOR,
                'updated_at' => now(),
            ]);
        }

        if (auth()->user()->hasRole(['super-admin', 'kepala-balai'])) {
            $jadwalService = new JadwalKegiatanPersonilService();
            foreach ($spk->members as $pegawai) {
                $jadwalService->store(collect([
                    'personil' => (string) $pegawai->id,
                    'tanggal_mulai' => date('Y-m-d', strtotime(str_replace('/', '-', $spk->tanggal_mulai))),
                    'tanggal_selesai' => date('Y-m-d', strtotime(str_replace('/', '-', $spk->tanggal_selesai))),
                    'penugasan' => $spk->permintaan_pengujian->master_layanan_uji->pelayanan->nama,
                ]));
            }

            $spk->permintaan_pengujian->pengujian()->create([
                'status' => PermintaanPengujian::STATUS_DRAFT,
            ]);
            $spk->update([
                'status' => PermintaanPengujian::STATUS_APPROVED_BY_KABALAI,
                'updated_at' => now(),
            ]);
        }
    }

    public function reject(PermintaanPengujianSpk $spk, $reason = '')
    {
        $spk->approvals()->create([
            'status' => PermintaanPengujian::STATUS_REJECTED,
            'reason' => $reason,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

    public function generateNomorSPK()
    {
        $permintaan = PermintaanPengujianSpk::orderBy('id', 'desc')->count();
        return 'SPK/'.date('Ymd').'/'.str_pad($permintaan + 1, 3, "0", STR_PAD_LEFT);
    }
}
