<?php

namespace App\Services;

use App\User;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Role;

class UserService
{
    public function store(Collection $data)
    {
        $user = User::create([
            'name' => $data->get('name'),
            'username' => $data->get('username'),
            'password' => bcrypt('password'),
            'email' => $data->get('email'),
            'status' => $data->get('status')
        ]);
        
        $user->pegawai()->create([
            'user_id' => $user->id,
        ]);
    }

    public function destroy(User $user)
    {
        $user->delete();
    }

    public function update(User $user, Collection $data)
    {
        $user->name = $data->get('name');
        $user->username = $data->get('username');
        $user->email = $data->get('email');
        $user->status = $data->get('status');
        $user->save();
    }

    public function findAll()
    {
        $users = User::where("name", '!=', "Super Admin")->get();

        return $users;
    }
}
