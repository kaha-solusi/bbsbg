<?php

namespace App\Services;

/**
 * BaseService
 *
 * @property-read \Illuminate\Database\Eloquent\Builder $model
 * @property-read Illuminate\Http\Resources\Json\JsonResource $resource
 *
 * @category Description
 * @package  Client\Pengujian
 * @author   Agung Kurniawan <agungkes95@gmail.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @version  Release: 0.0.1
 * @access   public
 * @link     http://url.com
 */
class BaseService
{
    protected $model = null;
    protected $resources = null;

    protected function generate_data(
        $select = [],
        $limit = 10,
        $offset = 0,
        $searchable = null,
        $search = null,
        $sort = null,
        $order = null
    )
    {
        $where = [];
        $searchable = collect($searchable);
        if ($search) {
            $contoh = $searchable->map(
                function ($key) use ($search) {
                    return [
                    "$key", '%'.strtolower($search).'%',
                    ];
                }
            );
            $where = $contoh->toArray();
        }
        
        $model = $this->model::select(...$select)
            ->when(
                !is_null($where),
                function ($query) use ($where) {
                    foreach ($where as $value) {
                        $query->orWhere($value[0], 'LIKE', $value[1]);
                    }
                }
            )
            ->when(
                $sort,
                function ($query, $sort) use ($order) {
                    $query->orderBy($sort, $order);
                }
            );

        $total = $model->count();

        return [
            'total' => $total,
            'totalNotFiltered' => $total,
            'rows' => $this->resources::collection(
                $model
                    ->orderBy('created_at', 'desc')
                    ->limit($limit)
                    ->offset($offset)
                    ->get()
            ),
        ];
    }

    public function get_data(...$args)
    {
        return $this->generate_data(...$args);
    }
}
