<?php

namespace App\Services;

use App\Http\Resources\MasterLayananUjiResource;
use App\Models\MasterLayananUji;
use App\Models\Pelayanan;

/**
 * MasterLayananUjiService
 *
 * @method string getJenisPengujianByLab()
 * @method string getPengujianItemByJenisPengujian()
 *
 * @category Service
 * @package  App\Services\MasterLayananUjiService
 * @author   Agung Kurniawan <agungkes95@gmail.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @version  Release: 0.0.1
 * @access   public
 * @link     http://url.com
 */
class MasterLayananUjiService extends BaseService
{
    protected $model = MasterLayananUji::class;
    protected $resources = MasterLayananUjiResource::class;

    /**
     * GetJenisPengujianByLab function
     *
     * @param string $labId lab
     *
     * @return mixed
     */
    public function getJenisPengujianByLab($labId)
    {
        return $this->model::select(
            'master_layanan_ujis.id',
            'master_layanan_ujis.name'
        )
            ->join('pelayanans', 'pelayanans.id', '=', 'pelayanan_id')
            ->where('pelayanans.nama', $labId)
            ->get();
    }

    /**
     * GetPengujianItemByJenisPengujian
     *
     * @param string $pengujianId nama pengujian
     *
     * @return mixed
     */
    public function getPengujianItemByJenisPengujian($pengujianId)
    {
        return $this->model::with('items')
            ->where('master_layanan_ujis.name', $pengujianId)
            ->firstOrFail();
    }

    private function getServiceCategory()
    {
        return Pelayanan::select('id', 'nama')
        ->where('kategori_id', 1);
    }

    public function getServices()
    {
        return $this->getServiceCategory()
            ->with('dataPelayanan')
            ->get();
    }

    public function getCategory()
    {
        return $this->getServiceCategory()->get();
    }

    public function store(string $name, $category, $items = [])
    {
        $master_layanan = MasterLayananUji::create(
            [
                'name' => $name,
                'pelayanan_id' => $category,
            ]
        );

        $childItems = [];
        foreach ($items as $item) {
            $childItems[] = [
                'name' => $item['name'],
                'price' => $item['price'],
                'working_days' => $item['working_days'],
                'standard' => $item['standard']
            ];
        }

        $master_layanan->items()->createMany($childItems);
    }

    public function update(MasterLayananUji $masterLayanan, string $name, $category, $items = [])
    {
        $masterLayanan
            ->update(
                [
                    'name' => $name,
                    'pelayanan_id' => $category,
                ]
            );
        
        $keepId = collect($items)->pluck('index')->filter(function ($m) {
            return !is_null($m);
        });

        // Delete yang tidak ada dalam keepId
        $masterLayanan->items()->whereNotIn('id', $keepId)->delete();

        // Insert atau update
        foreach ($items as $item) {
            $masterLayanan->items()->updateOrCreate([
                'id' => $item['index'] ?? null,
            ], [
                'name' => $item['name'],
                'price' => $item['price'],
                'working_days' => $item['working_days'],
                'standard' => $item['standard'],
                'master_layanan_uji_id' => $masterLayanan->id,
            ]);
        }
    }

    public function destroy(int $id)
    {
        MasterLayananUji::destroy($id);
    }
}
