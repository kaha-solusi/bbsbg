<?php

namespace App\Services;

use App\Models\PeralatanLabUsage;
use Illuminate\Support\Collection;

/**
 * PengujianService
 *
 * @category Description
 * @package  Client\Pengujian
 * @author   Agung Kurniawan <agungkes95@gmail.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @version  Release: 0.0.1
 * @access   public
 * @link     http://url.com
 */
class PeralatanLabUsageService
{
    public function store(Collection $data)
    {
        $alats = $data->get('peralatan');
        foreach ($alats as $alat) {
            PeralatanLabUsage::create([
                'tanggal_pelaksanaan' => $data->get('tanggal'),
                'kategori'            => $data->get('kategori'),
                'keterangan'          => $data->get('keterangan'),
                'peralatan_lab_id'    => $alat,
                'pelaksana'           => $data->get('pelaksana')
            ]);
        }
    }

    public function destroy(PeralatanLabUsage $peralatanLabUsage)
    {
        $peralatanLabUsage->delete();
    }

    public function update(PeralatanLabUsage $usage, Collection $data)
    {
        $usage->update([
            'tanggal_pelaksanaan' => $data->get('tanggal'),
            'kategori'            => $data->get('kategori'),
            'keterangan'          => $data->get('keterangan'),
            'pelaksana'           => $data->get('pelaksana')
        ]);
        $usage->save();
    }
}
