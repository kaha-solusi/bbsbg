<?php

namespace App\Services;

use App\Models\PermintaanPengujian;
use App\Models\PermintaanPengujianKajiUlang;

/**
 * Class KajiUlangService
 *
 * @package App\Services
 */
class KajiUlangService
{
    public function approve(PermintaanPengujianKajiUlang $permintaanPengujianKajiUlang)
    {
        $status = '';
        $approvalStatus = '';
        if (auth()->user()->hasRole(['super-admin', 'subkoor'])) {
            $status = PermintaanPengujian::STATUS_SUBMITTED;
            $approvalStatus = PermintaanPengujian::STATUS_KAJI_ULANG_CHECKED_BY_SUBKOOR;
        }
        if (auth()->user()->hasRole(['super-admin', 'kepala-balai'])) {
            $status = PermintaanPengujian::STATUS_APPROVED;
            $approvalStatus = PermintaanPengujian::STATUS_KAJI_ULANG_APPROVED_BY_KABALAI;

            $permintaanPengujianKajiUlang->permintaan_pengujian->payments()->create([
                'status' => PermintaanPengujian::STATUS_DRAFT,
            ]);
        }
        
        $permintaanPengujianKajiUlang->update([
            'status' => $status,
        ]);
        $permintaanPengujianKajiUlang->approvals()->create([
            'status' => PermintaanPengujian::STATUS_APPROVED,
        ]);
        
        $permintaanPengujianKajiUlang->permintaan_pengujian()->update([
            'status' => $approvalStatus,
        ]);
    }

    public function reject(PermintaanPengujianKajiUlang $permintaanPengujianKajiUlang, string $message)
    {
        $permintaanPengujianKajiUlang->permintaan_pengujian->update([
            'status' => PermintaanPengujian::STATUS_KAJI_ULANG_REJECTED,
        ]);
        
        $permintaanPengujianKajiUlang->update([
            'status' => PermintaanPengujian::STATUS_REJECTED,
        ]);

        $permintaanPengujianKajiUlang->approvals()->create([
            'reason' => $message,
            'status' => PermintaanPengujian::STATUS_REJECTED,
        ]);
    }
}
