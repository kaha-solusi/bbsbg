<?php

namespace App\Services;

use App\Models\Golongan;
use Illuminate\Support\Collection;

class GolonganService
{
    public function store(Collection $data)
    {
        Golongan::create([
            'nama' => $data->get('nama'),
        ]);
    }
    
    public function update(Golongan $golongan, Collection $data)
    {
        $golongan->update([
            'nama' => $data->get('nama'),
        ]);
        $golongan->save();
    }

    public function destroy(Golongan $golongan)
    {
        $golongan->delete();
    }
}
