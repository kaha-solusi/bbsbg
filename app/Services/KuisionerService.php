<?php

namespace App\Services;

use App\Models\Kuisioner;
use Illuminate\Support\Collection;

class KuisionerService
{
    public function store(Collection $data)
    {
        Kuisioner::create([
            'pernyataan' => $data->get('pernyataan'),
            'keterangan' => $data->get('keterangan'),
            'order' => $data->get('order'),
            'kategori_pelayanan_id' => $data->get('kategori'),
        ]);
    }

    public function update(Kuisioner $kuisioner, Collection $data)
    {
        $kuisioner->update([
            'pernyataan' => $data->get('pernyataan'),
            'keterangan' => $data->get('keterangan'),
            'order' => $data->get('order'),
            'kategori_pelayanan_id' => $data->get('kategori'),
        ]);

        $kuisioner->save();
    }

    public function destroy(Kuisioner $kuisioner)
    {
        $kuisioner->delete();
    }
}
