<?php

namespace App\Services;

use App\Http\Resources\ProvinceResource;
use App\Models\District;
use App\Models\Province;
use App\Models\Regency;
use App\Models\Village;
use Cache;
use Illuminate\Support\Collection;

class ProvinceService
{
    public function findAll()
    {
        return Cache::rememberForever('provinces', function () {
            return ProvinceResource::collection(Province::all());
        });
    }

    public function getRegencies(Province $province, Collection $regencies)
    {
        return Cache::rememberForever($province->name.'regencies', function () use ($regencies) {
            return ProvinceResource::collection($regencies);
        });
    }

    public function getDistricts(Province $province, Regency $regency, Collection $districts)
    {
        $cacheKey = $province->name.'regencies'.$regency->name.'districts';
        return Cache::rememberForever($cacheKey, function () use ($districts) {
            return ProvinceResource::collection($districts);
        });
    }

    public function getVillages(Province $province, Regency $regency, District $district, Collection $districts)
    {
        $cacheKey = $province->name.'regencies'.$regency->name.'districts'.$district->name.'villages';
        return Cache::rememberForever($cacheKey, function () use ($districts) {
            return ProvinceResource::collection($districts);
        });
    }
}
