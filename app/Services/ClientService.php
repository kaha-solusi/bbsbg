<?php

namespace App\Services;

use App\Models\Client;
use App\User;
use File;
use Hash;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Role;

class ClientService
{
    public function store(Collection $data)
    {
        $logo = null;
        if (request()->file('logo')) {
            $name = request()->file('logo');
            $logo = time()."_".$name->getClientOriginalName();
            request()->logo->move("upload/logo/client", $logo);
            $logo = '/upload/logo/client/'.$logo;
        }

        $user = User::create([
            'name' => $data->get('name'),
            'email' => $data->get('email'),
            'username'  => $data->get('username'),
            'password'  => Hash::make($data->get('password')),
            'level' => 1,
            'status' => 'aktif',
        ]);
        $user->assignRole('pelanggan');
        $user->client()->create([
            'name' => $data->get('name'),
            'website' => $data->get('website'),
            'alamat' => $data->get('alamat'),
            'phone_number' => $data->get('phone_number'),
            'logo' => $logo
        ]);
    }

    public function destroy(User $user)
    {
        if (!is_null($user->client->logo)) {
            File::delete($user->client->logo);
        }

        $user->delete();
    }

    public function update(User $user, Collection $data)
    {
        if (request()->file('logo')) {
            $name = request()->file('logo');
            $logo = time()."_".$name->getClientOriginalName();
            request()->logo->move("upload/logo/client", $logo);
            $logo = '/upload/logo/client/'.$logo;

            $data->put('logo', $logo);

            File::delete($user->client->logo);
        }

        $data->each(function ($value, $key) use ($user) {
            if (!in_array($key, array_keys($user->getAttributes()))) {
                $user->client->{$key} = $value;
            } else {
                $user->{$key} = $value;
            }
        });
        $user->client->save();
        $user->save();
    }
}
