<?php

namespace App\Services;

use App\Models\PermintaanPengujian;
use App\Models\PermintaanPengujianItem;
use Illuminate\Http\Request;

/**
 * Class ProsesPengujianService
 *
 * @package App\Services
 */
class ProsesPengujianService
{

    public function store(Request $request, PermintaanPengujian $permintaanPengujian, PermintaanPengujianItem $item)
    {
        $item->update([
            'hasil_pengujian' => $request->except([
                'alat_pengujian',
                '_token',
            ]),
        ]);

        $item->peralatan()->sync($request->alat_pengujian);
    }
}
