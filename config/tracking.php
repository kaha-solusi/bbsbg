<?php

return [
    'step_1' => [
        "approved" => "Permohonan Layanan Pengujian Telah Tercatat",
        "rejected" => "",
    ],
    'step_2' => [
        "approved" => "Diskusi pra pengujian",
        "rejected" => "",
    ],
    'step_3' => [
        "approved" => "Menunggu data teknis pemohon",
        "rejected" => "",
    ],
    'step_4' => [
        "approved" => "Permohonan layanan anda diterima, mohon kirim formulir konfirmasi pengujian",
        "rejected" => "Permohonan anda belum dapat dipenuhi",
    ],
    'step_5' => [
        "approved" => "Formulir konfirmasi pengujian telah diterima, mohon lakukan pembayaran",
        "rejected" => "Pelanggan tidak mengirimkan lembar konfirmasi, layanan pengujian dibatalkan",
    ],
    'step_6' => [
        "approved" => "Pembayaran berhasil, mohon lakukan pengiriman benda uji",
        "rejected" => "Pelanggan tidak melanjutkan pembayaran, layanan pengujian dibatalkan",
    ],
    'step_7' => [
        "approved" => "Benda uji diterima",
        "rejected" => "Benda uji tidak sesuai standar, mohon lakukan pengiriman ulang benda uji",
    ],
    'step_8' => [
        "approved" => "Pelaksanaan pengujian",
        "rejected" => "",
    ],
    'step_9' => [
        "approved" => "Penyusunan laporan hasil pengujian",
        "rejected" => "",
    ],
    'step_10' => [
        "approved" => "Laporan hasil uji terbit, silahkan mengisi survei kepuasan pelanggan",
        "rejected" => "",
    ],
    'step_11' => [
        "approved" => "Survei kepuasan pelanggan telah diterima",
        "rejected" => "",
    ],
    'step_12' => [
        "approved" => "Laporan telah terkirim, layanan pengujian selesai",
        "rejected" => "",
    ],
];
