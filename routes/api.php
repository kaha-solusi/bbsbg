<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// singular is just one
// plural is more than one
Route::get(
    '/instansi/get-unit-organisasi',
    'V1\API\InstansiApiController@getUnitOrganisasi'
)->name('api.instansi.get-unit-organisasi');
Route::get(
    '/instansi/get-unit-kerja/{unitOrganisasi?}',
    'V1\API\InstansiApiController@getUnitKerja'
)->name('api.instansi.get-unit-kerja');

Route::resource('/pegawai', 'V1\API\PegawaiApiController', [
    'as' => 'api',
]);
Route::group(['prefix' => 'administrative-area', 'as' => 'api.administrative-area.'], function () {
    Route::get(
        '/provinces',
        'V1\API\ProvinceController@index'
    )->name('provinces');
    Route::get(
        '/provinces/{province}',
        'V1\API\ProvinceController@regencies'
    )->name('province');
    Route::get(
        '/provinces/{province}/{regency}',
        'V1\API\ProvinceController@district'
    )->name('province.district');
    Route::get(
        '/provinces/{province}/{regency}/{district}',
        'V1\API\ProvinceController@village'
    )->name('province.district.village');

    Route::get(
        '/regencies',
        'V1\API\RegencyController@index'
    )->name('regencies');
    Route::get(
        '/regencies/{regency}',
        'V1\API\RegencyController@show'
    )->name('regencie');

    Route::get(
        '/districts',
        'V1\API\DistrictController@index'
    )->name('districts');
    Route::get(
        '/districts/{district}',
        'V1\API\DistrictController@show'
    )->name('district');

    Route::get(
        '/villages',
        'V1\API\VillageController@index'
    )->name('villages');
    Route::get(
        '/villages/{village}',
        'V1\API\VillageController@show'
    )->name('village');
});

Route::get(
    '/tracking',
    'V1\API\OrderController@getOrderByCode'
)->name('api.permintaan-pengujian.track');
