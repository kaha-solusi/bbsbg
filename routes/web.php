<?php

use App\Http\Middleware\EnsureKuisionerFilled;
use App\Mail\CobaMail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear', function () {

    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');

    return "Cleared!";
});
Route::get('/storage-link', function () {

    Artisan::call('storage:link');

    return "Storage Linked!";
});

Route::get('/update', function () {

    Artisan::call('migrate');
    Artisan::call('optimize:clear');

    return "Updated!";
});

Route::get('/update-permission', function () {

    Artisan::call('permission:create-permission "submit.advisi-teknis"');

    return "Permission Updated!";
});

// Homepage Route
Route::get('/', 'HomeController@home')->name('home');
Route::prefix('home')->group(function () {
    Route::get('berita', 'HomeController@berita')->name('berita');

    Route::group(['middleware' => ['auth', 'client']], function () {
        Route::get('permintaan-pengujian/{id}', 'V1\PermintaanPengujian@create')->name('permintaan.pengujian.create');
        Route::post('permintaan-pengujian/{id}/store', 'V1\PermintaanPengujian@store')->name('permintaan.pengujian.store');
    });

    Route::get('pesertabimtek/certificate', 'PesertaBimtekController@cert')->name('pesertabimtek.cert');

    Route::get('pengajar', 'HomeController@pengajar')->name('pengajar');
    Route::get('pejabat', 'HomeController@pejabat')->name('pejabat');
    Route::get('fasilitas', 'HomeController@fasilitas')->name('fasilitas');
    Route::get('sejarah', 'HomeController@sejarah')->name('sejarah');
    Route::get('visimisi', 'HomeController@visimisi')->name('visimisi');
    Route::get('alur-pelayanan', 'HomeController@alurpelayanan')->name('alurpelayanan');
    Route::get('kebijakan-pelayanan', 'HomeController@kebijakanpelayanan')->name('kebijakan-pelayanan');
    Route::get('struktur', 'HomeController@struktur')->name('struktur');
    Route::get('detailberita/{berita}', 'HomeController@detailberita')->name('detailberita');
    Route::get('tugasfungsi', 'HomeController@tugasfungsi')->name('tugasfungsi');
    Route::get('maklumat', 'HomeController@maklumat')->name('maklumat');
    Route::get('kepuasan_pelanggan', 'HomeController@kepuasan_pelanggan')->name('kepuasan_pelanggan');

    Route::get('pengaduan-masyarakat', 'V1\SettingAppController@form_pengaduan')->name('pengaduan-masyarakat.create');
    Route::post('pengaduan-masyarakat/store', 'V1\SettingAppController@laporan_masyarakat')->name('pengaduan-masyarakat.store');

    Route::get('pelapor-gratifikasi', 'V1\SettingAppController@form_pelaporan')->name('pelapor-gratifikasi.create');
    Route::post('pelapor-gratifikasi/store', 'V1\SettingAppController@pelaporan_grafikasi')->name('pelapor-gratifikasi.store');

    Route::get('pelayanan/tracking', 'HomeController@pelayananTracking')->name('pelayanan.tracking');
    Route::get('pelayanan/advisi-teknis', 'V1\AdvisiTeknisController')->name('home.pelayanan.advisi-teknis');
    Route::get('pelayanan/{id}', 'PelayananController@pelayanan')->name('home.pelayanan.extra');

    Route::get('pelayanan', 'HomeController@pelayanan')->name('pelayanan');

    Route::get('jadwal-pengujian', 'V1\KegiatanController@home')->name('jadwal-pengujian');
    Route::get('jadwal-bimtek', 'V1\JadwalBimtekController@home')->name('jadwal-bimtek');

    // Bimtek
    Route::get('daftar-bimtek/{id}', 'BimtekController@register')->name('bimtek.register');
    Route::post('pesertabimtek', 'PesertaBimtekController@store')->name('pesertabimtek.store');

    Route::get('galeri/{kategori}', 'HomeController@galeri')->name('galeri');
    Route::get('maskot', 'HomeController@maskot')->name('maskot');
    Route::get('cariberita', 'HomeController@cariberita')->name('cariberita');

    Route::get('uji-lab-bahan', function () {
        return view('homepage.uji-lab-bahan');
    })->name('uji.lab.bahan');

    Route::get('uji-lab-struktur', function () {
        return view('homepage.uji-lab-struktur');
    })->name('uji.lab.struktur');

    Route::get('faq', 'HomeController@faq')->name('faq');
    // Bimtek
    Route::get('daftar-bimtek/{id}', 'BimtekController@register')->name('bimtek.register');
    Route::post('pesertabimtek', 'PesertaBimtekController@store')->name('pesertabimtek.store');
});

Route::prefix('admin')->group(function () {

    Route::get('create-bimtek', 'BimtekController@create')->name('bimtek.create');

    Route::resource('berita', 'BeritaController');
    Route::group(['middleware' => ['auth']], function () {
        // Route Berita
        //Route Pelayanan
        Route::get('pelayanan/{kategori}', 'PelayananController@index')->name('pelayanan.index');
        Route::get('pelayanan/{kategori}/create/', 'PelayananController@create')->name('pelayanan.create');
        Route::post('pelayanan/store', 'PelayananController@store')->name('pelayanan.store');
        Route::get('pelayanan/{id}/edit/', 'PelayananController@edit')->name('pelayanan.edit');
        Route::patch('pelayanan/{id}/update', 'PelayananController@update')->name('pelayanan.update');
        Route::delete('pelayanan/{id}', 'PelayananController@destroy')->name('pelayanan.destroy');
        //Route Informasi Publik
        Route::resource('informasipublik', 'InformasiPublikController');
        // Maskot
        Route::resource('maskot', 'MaskotController');
        Route::resource('maklumat', 'MaklumatController');

        Route::resource('beritaui', 'BeritaUIController');
        Route::resource('pelayananui', 'PelayananUIController');
        Route::resource('videoui', 'VideoUIController');

        Route::resource('instansi', 'InstansiController', [
            'as' => 'admin',
        ]);

        Route::group(['prefix' => 'instansi/{instansi}'], function () {
            Route::resource('unit-kerja', 'InstansiUnitKerjaController', [
                'as' => 'admin.instansi',
            ]);
        });

        // BIMTEK
        Route::post('bimtek', 'BimtekController@store')->name('bimtek.store');
        Route::get('bimtek/{id}/edit/', 'BimtekController@edit')->name('bimtek.edit');
        Route::post('bimtek/{id}/update', 'BimtekController@update')->name('bimtek.update');
        Route::delete('bimtek/{id}', 'BimtekController@destroy')->name('bimtek.destroy');
        Route::get('pesertabimtek/{bimtek}', 'PesertaBimtekController@index')->name('pesertabimtek.index');
        Route::get('pesertabimtek/{id}/edit/', 'PesertaBimtekController@edit')->name('pesertabimtek.edit');
        Route::post('pesertabimtek/{id}/update/', 'PesertaBimtekController@update')->name('pesertabimtek.update');
        Route::delete('pesertabimtek/{id}', 'PesertaBimtekController@destroy')->name('pesertabimtek.destroy');

        // EXPORT
        Route::get('pegawai/export', 'ExportController@pegawai')->name('pegawai.export');
        Route::get('bimtek-peserta/export', 'ExportController@bimtek_peserta')->name('bimtek-peserta.export');
        Route::get('client/export', 'ExportController@client')->name('client.export');
        Route::get('permintaan-pengujian/export', 'ExportController@permintaan_pengujian')->name('admin.permintaan.export');
        Route::get('struktur-organisasi/export', 'ExportController@struktur_organisasi')->name('struktur-organisasi.export');
        Route::get('jadwal-pengujian/export', 'ExportController@jadwal_pengujian')->name('jadwal-pengujian.export');
        Route::get('jadwal-bimtek/export', 'ExportController@jadwal_bimtek')->name('jadwal-bimtek.export');
        Route::get('umpan-balik/export', 'ExportController@umpan_balik')->name('umpan-balik.export');
        Route::get('kepuasan-pelanggan/export', 'ExportController@kepuasan_pelanggan')->name('kepuasan-pelanggan.export');
        Route::get('indeks-kepuasan/export', 'ExportController@indeks_kepuasan')->name('indeks-kepuasan.export');
        Route::get('akun/export', 'ExportController@akun')->name('akun.export');
        Route::get('pelayanan/{kategori}/export', 'ExportController@pelayanan')->name('pelayanan.export');
        Route::get('peralatan-lab/export', 'ExportController@peralatan_lab')->name('peralatan-lab.export');
        Route::get('jadwal-kegiatan-personil/export', 'ExportController@jadwal-kegiatan-personil')
        ->name('jadwal-kegaiatan-personil.export');


        // IMPORT
        Route::post('pegawai/import', 'ImportController@pegawai')->name('pegawai.import');
        Route::get('pegawai/import.template', function () {
            return response()->download('excel_default/Template Import Data Peralatan.xlsx');
        })->name('pegawai.import.template');
        Route::post('peralatan/import', 'ImportController@peralatan')->name('peralatan.import');
        Route::post('struktur-organisasi/export', 'ImportController@struktur_organisasi')->name('struktur-organisasi.import');
        Route::post('pelayanan/{kategori}/import', 'ImportController@pelayanan')->name('pelayanan.import');
        Route::get('contoh-import-excel', function () {
            return response()->download('excel_default/contoh_import_new.xlsx');
        })->name('contoh-excel');

        //Advisi Teknis
        Route::get('old-advisi-teknis/{status}/detail', 'V1\Admin\AdvisiTeknisController@oldIndex')->name('advisiteknis.index');

        Route::get(
            'advisi-teknis/{advisiTeknis}/kuisioner',
            'V1\Admin\AdvisTeknisKuisionerController@index'
        )
        ->name('admin.advisi-teknis.kuisioner');
        Route::post(
            'advisi-teknis/reject/{advisiTeknis}',
            'V1\Admin\AdvisiTeknisController@reject'
        )->name('admin.advisi-teknis.reject');
        Route::resource(
            'advisi-teknis',
            'V1\Admin\AdvisiTeknisController',
            ['as' => 'admin']
        );
    });

    Route::group(['namespace' => 'V1', 'middleware' => ['auth']], function () {
            Route::get('/dashboard', 'DashboardController@index')->name('adminlteHome.dashboard');

            Route::get(
                '/admin/sejarah',
                'SejarahController@create'
            )->name('admin.sejarah');
            Route::post(
                '/admin/sejarah/store',
                'SejarahController@store'
            )->name('admin.sejarah.store');
            Route::post(
                '/admin/sejarah/img',
                'SejarahController@store_img'
            )->name('admin.sejarah.img');
            Route::post(
                '/admin/sejarah/pdf',
                'SejarahController@store_pdf'
            )->name('admin.sejarah.pdf');
            Route::get(
                '/admin/sejarah/status-homepage/{id}',
                'SejarahController@status_homepage'
            )->name('admin.sejarah.homepage');

            Route::get(
                '/admin/visi-misi',
                'VisiMisiController@create'
            )->name('admin.visi-misi');
            Route::post(
                '/admin/visi-misi/store',
                'VisiMisiController@store'
            )->name('admin.visi-misi.store');
            Route::post(
                '/admin/visi-misi/img',
                'VisiMisiController@store_img'
            )->name('admin.visi-misi.img');
            Route::post(
                '/admin/visi-misi/pdf',
                'VisiMisiController@store_pdf'
            )->name('admin.visi-misi.pdf');
            Route::get(
                '/admin/visi-misi/status-homepage/{id}',
                'VisiMisiController@status_homepage'
            )->name('admin.visi-misi.homepage');

            Route::get(
                '/admin/kebijakan-pelayanan',
                'KebijakanPelayananController@create'
            )->name('admin.kebijakan-pelayanan');
            Route::post(
                '/admin/kebijakan-pelayanan/store',
                'KebijakanPelayananController@store'
            )->name('admin.kebijakan-pelayanan.store');
            Route::post(
                '/admin/kebijakan-pelayanan/img',
                'KebijakanPelayananController@store_img'
            )->name('admin.kebijakan-pelayanan.img');
            Route::post(
                '/admin/kebijakan-pelayanan/pdf',
                'KebijakanPelayananController@store_pdf'
            )->name('admin.kebijakan-pelayanan.pdf');
            Route::get(
                '/admin/kebijakan-pelayanan/status-homepage/{id}',
                'KebijakanPelayananController@status_homepage'
            )->name('admin.kebijakan-pelayanan.homepage');

            Route::get(
                '/admin/alur-pelayanan',
                'AlurPelayananController@create'
            )->name('admin.alur-pelayanan');
            Route::post(
                '/admin/alur-pelayanan/store',
                'AlurPelayananController@store'
            )->name('admin.alur-pelayanan.store');
            Route::post(
                '/admin/alur-pelayanan/img',
                'AlurPelayananController@store_img'
            )->name('admin.alur-pelayanan.img');
            Route::post(
                '/admin/alur-pelayanan/pdf',
                'AlurPelayananController@store_pdf'
            )->name('admin.alur-pelayanan.pdf');
            Route::get(
                '/admin/alur-pelayanan/status-homepage/{id}',
                'AlurPelayananController@status_homepage'
            )->name('admin.alur-pelayanan.homepage');

            Route::get(
                '/admin/front-kepuasan-pelanggan',
                'HomeKepuasanPelangganController@create'
            )->name('admin.front-kepuasan-pelanggan');
            Route::post(
                '/admin/front-kepuasan-pelanggan/img',
                'HomeKepuasanPelangganController@store_img'
            )->name('admin.front-kepuasan-pelanggan.img');
            Route::post(
                '/admin/front-kepuasan-pelanggan/pdf',
                'HomeKepuasanPelangganController@store_pdf'
            )->name('admin.front-kepuasan-pelanggan.pdf');
            Route::get(
                '/admin/front-kepuasan-pelanggan/status-homepage/{id}',
                'HomeKepuasanPelangganController@status_homepage'
            )->name('admin.front-kepuasan-pelanggan.homepage');

            Route::get(
                '/admin/faq',
                'Pegawai@indexfaq'
            )->name('admin.faq.index');
            Route::get(
                '/admin/faq/create',
                'Pegawai@createfaq'
            )->name('admin.faq.create');
            Route::post(
                '/admin/faq/store',
                'Pegawai@storefaq'
            )->name('admin.faq.store');
            Route::get(
                '/admin/faq/{id}/edit',
                'Pegawai@editfaq'
            )->name('admin.faq.edit');
            Route::put(
                '/admin/faq/{id}',
                'Pegawai@updatefaq'
            )->name('admin.faq.update');
            Route::delete(
                '/admin/faq/{id}',
                'Pegawai@destroyfaq'
            )->name('admin.faq.destroy');

            Route::get(
                '/admin/tupoksi',
                'TugasFungsiController@create'
            )->name('admin.tupoksi');
            Route::post(
                '/admin/tupoksi/store',
                'TugasFungsiController@store'
            )->name('admin.tupoksi.store');
            Route::post(
                '/admin/tupoksi/img',
                'TugasFungsiController@store_img'
            )->name('admin.tupoksi.img');
            Route::post(
                '/admin/tupoksi/pdf',
                'TugasFungsiController@store_pdf'
            )->name('admin.tupoksi.pdf');
            Route::get(
                '/admin/tupoksi/status-homepage/{id}',
                'TugasFungsiController@status_homepage'
            )->name('admin.tupoksi.homepage');

            Route::get(
                '/admin/umpan-balik',
                'UmpanBalikController@index'
            )->name('admin.umpan-balik');

            Route::post(
                '/admin/struktur-organisasi/store/img',
                'StrukturController@store_gambar'
            )->name('admin.struktur-organisasi.gambar');
            Route::post(
                '/admin/struktur-organisasi/store/pdf',
                'StrukturController@store_pdf'
            )->name('admin.struktur-organisasi.pdf');
            Route::get(
                '/admin/struktur-organisasi/homepage/{id}',
                'StrukturController@status_homepage'
            )->name('admin.struktur-organisasi.homepage');
            Route::resource('/admin/struktur-organisasi', 'StrukturController');

            Route::get('/admin/no-whatsapp', 'WaController@create')->name('admin.wa');
            Route::post('/admin/no-whatsapp/store', 'WaController@store')->name('admin.wa.store');

            Route::get(
                '/admin/setting-user',
                'SetUserController@create'
            )->name('admin.setting.user');
            Route::post(
                '/admin/setting-user/store',
                'SetUserController@store'
            )->name('admin.setting.user.store');

            Route::post(
                '/admin/password',
                'SetUserController@password'
            )->name('admin.setting.user.password');

            Route::get(
                '/admin/setting-app',
                'SettingAppController@create'
            )->name('admin.setting-app');
            Route::post(
                '/admin/setting-app/store',
                'SettingAppController@store'
            )->name('admin.setting-app.store');

            Route::get(
                '/admin/sosial-media',
                'SosialMediaController@create'
            )->name('admin.sosial-media');
            Route::post(
                '/admin/sosial-media/store',
                'SosialMediaController@store'
            )->name('admin.sosial-media.store');

            // Permintaan Pelayanan
            Route::post(
                '/permintaan-pengujian/kaji-ulang-layanan/reject/{kaji_ulang_layanan}',
                'Admin\PermintaanPengujian\KajiUlangController@reject'
            )->name('admin.permintaan-pengujian.kaji-ulang-layanan.reject');
            Route::post(
                '/permintaan-pengujian/kaji-ulang-layanan/approve/{kaji_ulang_layanan}',
                'Admin\PermintaanPengujian\KajiUlangController@approve'
            )->name('admin.permintaan-pengujian.kaji-ulang-layanan.approve');
            Route::resource(
                '/permintaan-pengujian/kaji-ulang-layanan',
                'Admin\PermintaanPengujian\KajiUlangController',
                ['as' => 'admin.permintaan-pengujian']
            )->only('index', 'show');
            Route::resource(
                '/permintaan-pengujian/{permintaan_pengujian}/histories',
                'Admin\PermintaanPengujianHistoryController',
                ['as' => 'admin.permintaan-pengujian']
            )->except('show');

            // Permintaan Pengujian > Surat Perintah Kerja
            Route::post(
                '/permintaan-pengujian/surat-perintah-kerja/reject',
                'Admin\PermintaanPengujian\SuratPerintahKerjaController@reject'
            )->name('admin.permintaan-pengujian.surat-perintah-kerja.reject');
            Route::post(
                '/permintaan-pengujian/surat-perintah-kerja/approve/{surat_perintah_kerja}',
                'Admin\PermintaanPengujian\SuratPerintahKerjaController@approve'
            )->name('admin.permintaan-pengujian.surat-perintah-kerja.approve');
            Route::resource(
                '/permintaan-pengujian/surat-perintah-kerja',
                'Admin\PermintaanPengujian\SuratPerintahKerjaController',
                ['as' => 'admin.permintaan-pengujian']
            )->except('destroy', 'store', 'create');

            // Permintaan Pengujian > Hasil Pengujian
            Route::get(
                '/permintaan-pengujian/proses-pengujian/{proses_pengujian}/hasil-pengujian/{item}',
                'Admin\PermintaanPengujian\HasilPengujianController@showHasilPengujian'
            )->name('admin.permintaan-pengujian.proses-pengujian.hasil-pengujian.show');
            Route::get(
                '/permintaan-pengujian/proses-pengujian/{proses_pengujian}/hasil-pengujian/{item}/edit',
                'Admin\PermintaanPengujian\HasilPengujianController@editHasilPengujian'
            )->name('admin.permintaan-pengujian.proses-pengujian.hasil-pengujian.edit');
            Route::put(
                '/permintaan-pengujian/proses-pengujian/{proses_pengujian}/hasil-pengujian/{item}',
                'Admin\PermintaanPengujian\HasilPengujianController@updateHasilPengujian'
            )->name('admin.permintaan-pengujian.proses-pengujian.hasil-pengujian.update');
            Route::resource(
                '/permintaan-pengujian/proses-pengujian',
                'Admin\PermintaanPengujian\HasilPengujianController',
                ['as' => 'admin.permintaan-pengujian']
            )->except('destroy', 'store', 'create');

            // Permintaan Pengujian > LHU
            Route::get(
                '/permintaan-pengujian/laporan-hasil-uji/{laporan_hasil_uji}/preview',
                'Admin\PermintaanPengujian\LaporanHasilUjiController@printPreview'
            )->name('admin.permintaan-pengujian.laporan-hasil-uji.printPreview');
            Route::get(
                '/permintaan-pengujian/laporan-hasil-uji/{laporan_hasil_uji}/print',
                'Admin\PermintaanPengujian\LaporanHasilUjiController@print'
            )->name('admin.permintaan-pengujian.laporan-hasil-uji.print');
            Route::resource(
                '/permintaan-pengujian/laporan-hasil-uji',
                'Admin\PermintaanPengujian\LaporanHasilUjiController',
                ['as' => 'admin.permintaan-pengujian']
            )->except('destroy', 'create', 'edit');

            // Permintaan Pelayanan > Pembayaran
            Route::post(
                '/permintaan-pengujian/penerimaan-benda-uji/{sample}/reject',
                'Admin\PermintaanPengujian\PenerimaanBendaUjiController@reject'
            )->name('admin.permintaan-pengujian.penerimaan-benda-uji.reject');
            Route::post(
                '/permintaan-pengujian/penerimaan-benda-uji/{sample}/approve',
                'Admin\PermintaanPengujian\PenerimaanBendaUjiController@approve'
            )->name('admin.permintaan-pengujian.penerimaan-benda-uji.approve');
            Route::resource(
                '/permintaan-pengujian/penerimaan-benda-uji',
                'Admin\PermintaanPengujian\PenerimaanBendaUjiController',
                ['as' => 'admin.permintaan-pengujian']
            )->except('destroy', 'store', 'create');

            Route::get(
                '/permintaan-pengujian/pembayaran/{pembayaran}/download',
                'Admin\PermintaanPengujian\PembayaranController@download'
            )->name('admin.permintaan-pengujian.pembayaran.download');
            Route::post(
                '/permintaan-pengujian/pembayaran/{pembayaran}/reject',
                'Admin\PermintaanPengujian\PembayaranController@reject'
            )->name('admin.permintaan-pengujian.pembayaran.reject');
            Route::post(
                '/permintaan-pengujian/pembayaran/{pembayaran}/approve',
                'Admin\PermintaanPengujian\PembayaranController@approve'
            )->name('admin.permintaan-pengujian.pembayaran.approve');
            Route::resource(
                '/permintaan-pengujian/pembayaran',
                'Admin\PermintaanPengujian\PembayaranController',
                ['as' => 'admin.permintaan-pengujian']
            )->except('destroy', 'store', 'create');

            Route::get(
                'permintaan-pengujian/{permintaan_pengujian}/download-balasan',
                'Admin\PermintaanPengujianController@downloadReply'
            )->name('admin.permintaan-pengujian.download-balasan');
            Route::post(
                'permintaan-pengujian/{permintaan_pengujian}/document-approve',
                'Admin\PermintaanPengujianController@documentApprove'
            )->name('admin.permintaan-pengujian.document-approve');
            Route::post(
                '/permintaan-pengujian/upload/{permintaan_pengujian}',
                'Admin\PermintaanPengujianController@upload'
            )->name('admin.permintaan-pengujian.unggah-form-layanan-konfirmasi');
            Route::resource(
                '/permintaan-pengujian',
                'Admin\PermintaanPengujianController',
                ['as' => 'admin']
            );

            // Route::group([
            //     'prefix' => '/permintaan-pengujian/{permintaan_pengujian}/proses-pengujian/{parameter}',
            //     'as' => 'admin.permintaan-pengujian.proses-pengujian.',
            // ], function () {
            //     Route::get(
            //         '/',
            //         'Admin\ProsesPengujianController@index'
            //     )->name('index');
            //     Route::get(
            //         '/detail',
            //         'Admin\ProsesPengujianController@show'
            //     )->name('show');
            //     Route::post(
            //         '/',
            //         'Admin\ProsesPengujianController@store'
            //     )->name('store');
            // });

            Route::get(
                '/permintaan-pengujian/{permintaan_pengujian}/payment-confirmation',
                'Admin\PermintaanPengujianController@paymentConfirmation'
            )->name('admin.permintaan-pengujian.payment-confirmation');

            Route::post(
                '/permintaan-pengujian/{permintaan_pengujian}/payment-confirmation/approve',
                'Admin\PermintaanPengujianController@paymentConfirmationApprove'
            )->name('admin.permintaan-pengujian.payment-confirmation.approve');
            Route::post(
                '/permintaan-pengujian/{permintaan_pengujian}/payment-confirmation/reject',
                'Admin\PermintaanPengujianController@paymentConfirmationReject'
            )->name('admin.permintaan-pengujian.payment-confirmation.reject');

            // Route::resource(
            //     '/permintaan-pengujian/{permintaan_pengujian}/penerimaan-benda-uji/spk',
            //     'Admin\PermintaanPengujianSpkController',
            //     [
            //         'as' => 'admin.permintaan-pengujian',
            //     ]
            // );
            // Route::resource(
            //     '/permintaan-pengujian/{permintaan_pengujian}/penerimaan-benda-uji',
            //     'Admin\PermintaanPengujianTestSampleController',
            //     [
            //         'as' => 'admin.permintaan-pengujian',
            //     ]
            // );

            // Route::get(
            //     '/admin/permintaan-pengujian/{pengujian}',
            //     'PermintaanPengujian@show'
            // )
            // ->name('admin.permintaan.show');
            // Route::get(
            //     '/admin/permintaan-pengujian/{id}/status/diterima',
            //     'PermintaanPengujian@terimaPermintaan'
            // )->name('admin.permintaan.diterima');
            // Route::get(
            //     '/admin/permintaan-pengujian/{id}/status/ditolak',
            //     'PermintaanPengujian@tolakPermintaan'
            // )->name('admin.permintaan.ditolak');
            // Route::get(
            //     '/admin/permintaan-pengujian/{id}/status/selesai',
            //     'PermintaanPengujian@selesaiPermintaan'
            // )->name('admin.permintaan.selesai');
            // Route::get(
            //     '/admin/permintaan-pengujian',
            //     'PermintaanPengujian@index'
            // )->name('admin.permintaan.index');

            // .Permintaan Pelayanan

            Route::resource('/admin/pejabat', 'PejabatController');

            Route::resource('/admin/pengajar', 'PengajarController');

            Route::resource('/admin/galeri', 'GaleriController');
            Route::get('/admin/galerivideo/create', 'GaleriController@createVideo')->name('galeri.createVideo');
            Route::get('/admin/galerivideo/{$id}/edit', 'GaleriController@editVideo')->name('galeri.editVideo');

            Route::get(
                '/admin/peralatan/barcode/{alat}',
                'PeralatanLabController@showBarcode'
            )->name('peralatan.barcode');

            Route::group(['prefix' => '/admin'], function () {
                Route::get('/get-peralatan-by-name', 'PeralatanLabController@getPeralatanByName')
                    ->name('admin.peralatan.get-peralatan-by-name');
                Route::get('/get-peralatan-by-id', 'PeralatanLabController@getPeralatanById')
                    ->name('admin.peralatan.get-peralatan-by-id');
                Route::resource('/peralatan', 'PeralatanLabController', [
                    'as' => 'admin',
                ]);
                Route::resource('/peralatan/{peralatan}/calibration', 'Admin\PeralatanKalibrasiController', [
                    'as' => 'admin',
                ]);
                Route::resource('/peralatan/usage', 'Admin\PeralatanLabUsageController', [
                    'as' => 'admin',
                ]);
                Route::get('/peralatan/{peralatan}/usage', 'Admin\PeralatanLabUsageController@index')
                    ->name('admin.usage.index');
            });

            Route::resource('/admin/lab', 'LabController');

            Route::resource('/admin/fasilitas', 'SaranaPrasaranaController');

            // Slider
            Route::resource('/admin/slider', 'SliderController');

            Route::resource('/admin/client', 'Admin\ClientController', [
                'as' => 'admin',
            ]);

            Route::resource('/admin/pekerjaan', 'PekerjaanController');

            // Pegawai
            Route::resource('/admin/pegawai/jabatan-laboratorium', 'Admin\LabPositionController', [
                'as' => 'admin.pegawai',
            ]);
            Route::resource('/admin/pegawai/jabatan', 'Admin\JabatanController', [
                'as' => 'admin.pegawai',
            ]);
            Route::resource('/jadwal-kegiatan-personil', 'Admin\JadwalKegiatanPersonilController', [
                'as' => 'admin.pegawai',
            ]);
            Route::resource('/admin/pegawai/golongan', 'Admin\GolonganController', [
                'as' => 'admin.pegawai',
            ]);
            Route::get(
                '/admin/pegawai/get-jabatan-laboratorium',
                'Admin\PegawaiController@getLaboratoriumPositions'
            )
            ->name('admin.pegawai.get-jabatan-laboratorium');
            Route::get(
                '/admin/pegawai/get-jabatan-fungsional',
                'Admin\PegawaiController@getFunctionalPositions'
            )
            ->name('admin.pegawai.get-jabatan-fungsional');
            Route::resource('/admin/pegawai', 'Admin\PegawaiController', [
                'as' => 'admin',
            ]);

            Route::resource('/admin/akun', 'Admin\AkunController', [
                'as' => 'admin',
            ]);

            Route::resource('/admin/roles', 'Admin\RoleController', [
                'as' => 'admin',
            ]);
            Route::resource('/admin/products', 'Admin\ProductController', [
                'as' => 'admin',
            ]);
            Route::resource('/admin/kuisioners', 'Admin\KuisionerController', [
                'as' => 'admin',
            ]);

            Route::resource('/admin/indeks-kepuasan', 'IndeksKepuasanController');

            // Jadwal Pengujian
            Route::get(
                '/admin/jadwal-pengujian/{id}/status/dilaksanakan',
                'KegiatanController@statusDilaksanakan'
            )->name('jadwal-pengujian.status.dilaksanakan');
            Route::get(
                '/admin/jadwal-pengujian/{id}/status/tidak-dilaksanakan',
                'KegiatanController@statusTidakDilaksanakan'
            )->name('jadwal-pengujian.status.tidak');
            Route::get(
                '/admin/jadwal-pengujian/{id}/status/sedang-dilaksanakan',
                'KegiatanController@statusSedangDilaksanakan'
            )->name('jadwal-pengujian.status.sedang');
            Route::get(
                '/admin/jadwal-pengujian/{id}/status/selesai-dilaksanakan',
                'KegiatanController@statusSelesaiDilaksanakan'
            )->name('jadwal-pengujian.status.selesai');
            Route::resource('/admin/jadwal-pengujian', 'KegiatanController');

            // Jadwal Bimtek
            Route::get(
                '/admin/jadwal-bimtek/{id}/status/dilaksanakan',
                'JadwalBimtekController@statusDilaksanakan'
            )->name('jadwal-bimtek.status.dilaksanakan');
            Route::get(
                '/admin/jadwal-bimtek/{id}/status/tidak-dilaksanakan',
                'JadwalBimtekController@statusTidakDilaksanakan'
            )->name('jadwal-bimtek.status.tidak');
            Route::get(
                '/admin/jadwal-bimtek/{id}/status/sedang-dilaksanakan',
                'JadwalBimtekController@statusSedangDilaksanakan'
            )->name('jadwal-bimtek.status.sedang');
            Route::get(
                '/admin/jadwal-bimtek/{id}/status/selesai-dilaksanakan',
                'JadwalBimtekController@statusSelesaiDilaksanakan'
            )->name('jadwal-bimtek.status.selesai');
            Route::resource('/admin/jadwal-bimtek', 'JadwalBimtekController');

            Route::get('/admin/kepuasan-pelanggan', 'KepuasanPelangganController@index')->name('kepuasan-pelanggan.index');

            // Data Master
            Route::resource('/admin/master-pelayanan-uji', 'Admin\MasterLayananUjiController');
    });
});

Route::get(
    '/client/permintaan-pengujian/download',
    'V1\DownloadController@index'
)
->middleware(EnsureKuisionerFilled::class)
->name('permintaan-pengujian.download');
Route::get(
    '/client/permintaan-pengujian/{pengujian}/kuisioner',
    'V1\Client\PermintaanPengujianKuisionerController@index'
)
->name('permintaan-pengujian.kuisioner');

Route::group(
    [
        'middleware' => 'auth',
        'prefix' => 'client',
        'namespace' => 'V1\Client'
    ],
    function () {

        Route::group(['as' => 'client.'], function () {
            Route::resource('permintaan-pengujian', 'PermintaanPengujian');
            Route::get(
                'permintaan-pengujian/{permintaan_pengujian}/payment-confirmation',
                'PermintaanPengujian@paymentConfirmation'
            )->name('permintaan-pengujian.payment-confirmation.get');
            Route::post(
                'permintaan-pengujian/{permintaan_pengujian}/payment-confirmation',
                'PermintaanPengujian@paymentConfirmationPost'
            )->name('permintaan-pengujian.payment-confirmation.post');

            Route::resource(
                'permintaan-pengujian/{permintaan_pengujian}/test-sample',
                'PermintaanPengujianTestSampleController',
                [
                    'as' => 'permintaan-pengujian',
                ]
            );

            Route::get(
                'permintaan-pengujian/{permintaan_pengujian}/laporan-hasil-uji',
                'PermintaanPengujian@laporanHasilUji'
            )->name('permintaan-pengujian.laporan-hasil-uji.get');
        });

    // Client profile
        Route::get('/profile', 'ProfileController@index')->name('client.profile.index');
        Route::put('/profile/{user}', 'ProfileController@update')->name('client.profile.update');

        Route::get(
            '/bimtek',
            'BimtekClientsController@list'
        )->name('client.bimtek.list');

        Route::get(
            '/advisiteknis',
            'AdvisiTeknisController@index'
        )->name('client.advisiteknis.index');


        Route::get('/client/setting-client', 'ClientController@settingClient')->name('client.setting.index');
        Route::post(
            '/setting-client/store',
            'ClientController@settingClientUpdate'
        )->name('client.setting.store');

        Route::post('/password-reset', 'ClientController@password')->name('client.password.reset');

        Route::get('/umpan-balik', 'UmpanBalikController@create')->name('umpan-balik.index');
        Route::post('/umpan-balik/store', 'UmpanBalikController@store')->name('umpan-balik.store');

        Route::get(
            '/kepuasan-pelanggan/create',
            'KepuasanPelangganController@create'
        )->name('kepuasan-pelanggan.create');
        Route::post(
            '/kepuasan-pelanggan/store',
            'KepuasanPelangganController@store'
        )->name('kepuasan-pelanggan.store');
    }
);
// Advisi Teknis

Route::get(
    '/client/advisi-teknis/{advisiTeknis}/kuisioner',
    'V1\Client\AdvisTeknisKuisionerController@index'
)
->name('advisi-teknis.kuisioner');
Route::post(
    '/client/advisi-teknis/{advisiTeknis}/kuisioner',
    'V1\Client\AdvisTeknisKuisionerController@store'
)
->name('advisi-teknis.kuisioner.store');
Route::get(
    '/client/advisi-teknis/download',
    'V1\DownloadController@index'
)->middleware(EnsureKuisionerFilled::class)
->name('advisi-teknis.download');
Route::resource('/client/advisi-teknis', 'V1\Client\AdvisiTeknisController', [
    'as' => 'client',
]);

// Pengujian
Route::group(['prefix' => 'pengujian'], function () {

    Route::group(
        ['prefix' => 'registrasi', 'middleware' => 'auth'],
        function () {
            Route::get(
                '/get_jenis_pengujian/{id?}',
                'V1\PengujianController@getJenisPengujian'
            )->name('pengujian.get-jenis-pengujian');
            Route::get(
                '/get_jenis_pengujian_item/{id?}',
                'V1\PengujianController@getJenisPengujianItem'
            )->name('pengujian.get-jenis-pengujian-item');
            Route::get(
                '/get_product_name/{parameterPengujian?}',
                'V1\PengujianController@getProductName'
            )->name('pengujian.get-product-name');

            Route::name('pengujian.registrasi')
                ->get('/', 'V1\PengujianController@index');
            Route::name('client.pengujian.store')
                ->post('/', 'V1\PengujianController@store');
        }
    );
});

Route::post(
    '/client/permintaan-pengujian/{pengujian}/kuisioner',
    'V1\Client\PermintaanPengujianKuisionerController@store'
)
    ->name('permintaan-pengujian.kuisioner.store');


Route::resource('/client/permintaan-pengujian', 'V1\Client\PermintaanPengujian', [
    'as' => 'client',
]);

//Forgot Password
Route::get('forget-password', 'Auth\ForgotPasswordController@showForgetPasswordForm')->name('forget.password.get');
Route::post('forget-password', 'Auth\ForgotPasswordController@submitForgetPasswordForm')->name('forget.password.post');
Route::get('reset-password/{token}', 'Auth\ForgotPasswordController@showResetPasswordForm')->name('reset.password.get');
Route::post('reset-password', 'Auth\ForgotPasswordController@submitResetPasswordForm')->name('reset.password.post');

Auth::routes(['verify' => true]);
Route::get('maintenance', 'MaintenanceController')->name('maintenance');
Route::get('check-mail', function () {
    Mail::to('cintarembo@gmail.com')->send(new CobaMail('subject', 'content', 'bg'));
});

Route::post('/home/upload', 'V1\FileUploadController@upload')->name('file.upload');
Route::get('/home/download', 'V1\DownloadController@index')->name('file.download');
