<?php // routes/breadcrumbs.php

// Note: Laravel will automatically resolve `Breadcrumbs::` without
// this import. This is nice for IDE syntax and refactoring.

use App\Models\Instansi;
use App\Models\MasterLayananUji;
use App\Models\Product;
use Diglactic\Breadcrumbs\Breadcrumbs;

// This import is also not required, and you could replace `BreadcrumbTrail $trail`
//  with `$trail`. This is nice for IDE type checking and completion.
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

Breadcrumbs::for('home', function (BreadcrumbTrail $trail) {
    $trail->push('Beranda', url('/'));
});

Breadcrumbs::for('pengujian.registrasi', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Registrasi Pengujian');
});

// Dashboard
Breadcrumbs::for('dashboard', function (BreadcrumbTrail $trail) {
    $trail->push('Beranda', route('adminlteHome.dashboard'));
});

Breadcrumbs::for('adminlteHome.dashboard', function ($trail) {
    $trail->push('Beranda', route('adminlteHome.dashboard'));
});

Breadcrumbs::for('admin.instansi.unit-kerja.index', function (BreadcrumbTrail $trail, Instansi $instansi) {
    $trail->parent('dashboard');
    $trail->push('Unit Organisasi', route('admin.instansi.index'));
    $trail->push($instansi->nama_instansi, route('admin.instansi.unit-kerja.index', $instansi->id));
});
Breadcrumbs::for('admin.instansi.unit-kerja.create', function (BreadcrumbTrail $trail, Instansi $instansi) {
    $trail->parent('dashboard');
    $trail->push('Unit Organisasi', route('admin.instansi.index'));
    $trail->push($instansi->nama_instansi, route('admin.instansi.unit-kerja.index', $instansi->id));
    $trail->push('Tambah unit kerja');
});
Breadcrumbs::for('admin.instansi.unit-kerja.edit', function (BreadcrumbTrail $trail, Instansi $instansi, Instansi $unitKerja) {
    $trail->parent('dashboard');
    $trail->push('Unit Organisasi', route('admin.instansi.index'));
    $trail->push($instansi->nama_instansi, route('admin.instansi.unit-kerja.index', $instansi->id));
    $trail->push($unitKerja->nama_instansi);
});

// Products
Breadcrumbs::for('admin.products.index', function (BreadcrumbTrail $trail) {
    $trail->parent('dashboard');
    $trail->push('Data Produk');
});
Breadcrumbs::for('admin.products.create', function (BreadcrumbTrail $trail) {
    $trail->parent('dashboard');
    $trail->push('Data Produk', route('admin.products.index'));
    $trail->push('Tambah Produk Baru');
});
Breadcrumbs::for('admin.products.edit', function (BreadcrumbTrail $trail, Product $product) {
    $trail->parent('dashboard');
    $trail->push('Data Produk', route('admin.products.index'));
    $trail->push($product->name);
});

// Master Pelayanan
Breadcrumbs::for('master-pelayanan-uji.index', function (BreadcrumbTrail $trail) {
    $trail->parent('dashboard');
    $trail->push('Data Pelayanan', route('master-pelayanan-uji.index'));
});
Breadcrumbs::for('master-pelayanan-uji.create', function (BreadcrumbTrail $trail) {
    $trail->parent('master-pelayanan-uji.index');
    $trail->push('Tambah Pelayanan', route('master-pelayanan-uji.create'));
});
Breadcrumbs::for('master-pelayanan-uji.show', function (BreadcrumbTrail $trail, MasterLayananUji $master_pelayanan_uji) {
    $trail->parent('master-pelayanan-uji.index');
    $trail->push($master_pelayanan_uji->name);
});


// Peralatan
Breadcrumbs::for('admin.peralatan.index', function (BreadcrumbTrail $trail) {
    $trail->parent('dashboard');
    $trail->push('Title Here', route('admin.peralatan.index'));
});

// Roles
Breadcrumbs::for('admin.roles.index', function (BreadcrumbTrail $trail) {
    $trail->parent('dashboard');
    $trail->push('Data Role', route('admin.roles.index'));
});

Breadcrumbs::for('admin.roles.create', function (BreadcrumbTrail $trail) {
    $trail->parent('dashboard');
    $trail->push('Tambah Role');
});


// Profile
Breadcrumbs::for('client.profile.index', function (BreadcrumbTrail $trail) {
    $trail->push('Profile');
});
